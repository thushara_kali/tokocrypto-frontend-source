import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import {
    setupCognito,
} from './core/react-cognito/index'
import store from './store';
import configDEV from './config/cognito-dev.json';
import configDEMO from './config/cognito-demo.json';
import configPROD from './config/cognito-production.json';
import 'react-select/dist/react-select.css';
import { PersistGate } from 'redux-persist/integration/react'

let appConfig = {};
let appEnv = process.env.REACT_APP_ENV;
switch(appEnv){
    case 'dev':
        appConfig = configDEV;
        break;
    case 'demo':
        appConfig = configDEMO;
        break;
    case 'production':
        appConfig = configPROD;
        break;
    default :
        appConfig = configDEV;
        break;
}


setupCognito(store, appConfig);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
