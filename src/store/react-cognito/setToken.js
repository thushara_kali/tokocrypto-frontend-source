import React from 'react';
import { connect } from 'react-redux';
import { refreshIdentityCredentials } from './auth';
import { Action } from '../../core/react-cognito/actions';

const BaseSetToken = props =>
  React.cloneElement(props.children, {
    username: props.username,
    email: props.email,
    onSetToken: props.onSetToken,
    clearCache: props.clearCache,
  });

const mapStateToProps = (state) => {
  let username = '';
  if (state.cognito.user) {
    username = state.cognito.user.getUsername();
  } else if (state.cognito.userName) {
    username = state.cognito.cache.userName;
  }
  return {
    username,
    email: state.cognito.cache.email,
    config: state.cognito.config,
    userPool: state.cognito.userPool,
  };
};

const mapDispatchToProps = dispatch => ({
  // authenticator: (username, password, userPool, config) =>
  //   authenticate(username, password, userPool, config, dispatch),
  refreshIdentityCredentials: (username, jwtToken, userPool, config) =>
      refreshIdentityCredentials(username, jwtToken, userPool, config, dispatch),
  clearCache: () => dispatch(Action.clearCache())
});

const mergeProps = (stateProps, dispatchProps, ownProps) =>
  Object.assign({}, ownProps, stateProps, {
    onSubmit: (username, jwtToken) =>
      dispatchProps.refreshIdentityCredentials(username, jwtToken, stateProps.userPool, stateProps.config),
    clearCache: dispatchProps.clearCache
  });

/**
 * Container for login behaviour, wrapping a login form.
 *
 * Magically provides the following props to the wrapped form:
 *
 *  * username
 *  * onSubmit
 *
 * @example
 * <Login>
 *   <LoginForm />
 * </Login>
 */
const setToken = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(BaseSetToken);

export { setToken };
