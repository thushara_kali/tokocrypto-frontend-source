/**
 * container for all the actions
*/
const newAction = {

  login: creds => ({
    type: 'COGNITO_LOGIN',
    creds,
  }),

  
};

export { newAction };
