import { CognitoUser, AuthenticationDetails, CognitoUserPool } from 'amazon-cognito-identity-js';
import { CognitoIdentityCredentials } from 'amazon-cognito-identity-js/dist/aws-cognito-sdk.min.js';
import { Action } from '../../core/react-cognito/actions';
import { mkAttrList, sendAttributeVerificationCode } from '../../core/react-cognito/attributes';
import { buildLogins } from '../../core/react-cognito/utils';
const AWS = require('aws-sdk');

/**
 * logs in to the federated identity pool with a JWT
 * @param {string} username - the username
 * @param {string} jwtToken - a token from the session
 * @param {object} config - the react-cognito config
 * @return {Promise<object>} a promise that resolves to the federated identity credentials
*/
const refreshIdentityCredentials = (username, jwtToken, userPool, config, dispatch) =>
  new Promise((resolve, reject) => {
    const logins = buildLogins(username, jwtToken, config);
    // AWS.config.region = config.region;
    AWS.config= config;
    const creds = new AWS.CognitoIdentityCredentials(logins);


    // 
    // const _creds= new AuthenticationDetails(creds);
    //
    // user.authenticateUser(_creds, {
    //       onFailure: (error) => {
    //           
    //       }
    // })

    // creds.get(function(err) {
    //     if (err) throw err;
    //     
    // })
    //

    // creds.getPromise().then(res=> {
    //     
    // });

    // creds.getId((err)=> {
    //     //if(err) throw err;
    //     
    //
    // })



    creds.refresh((error) => {
        if (error) {
          reject(error.message);
        } else {
          
          dispatch(Action.login(creds));
          resolve(Action);
        }
    });

  });

export {
  refreshIdentityCredentials
};
