import React, {Component} from 'react';
import { BrowserRouter as Router } from 'react-router-dom'
import AppHeader from '../components/header';

class AppLayout extends Component {

    render(){
        return (
            <Router>
                <div className="m-page--wide m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default m-header--minimize-off modal-open">
                    <AppHeader />
                    {this.props.children}
                </div>
            </Router>
        )
    }

}

export default AppLayout;