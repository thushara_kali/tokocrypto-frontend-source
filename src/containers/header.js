import React, { Component } from 'react';
import AvatarDropdown from '../components/avatarDropdown.js';
import HeaderMenu from './../components/headerMenu.js'
// import Notification from './notification.js';


class Header extends Component {
  render() {
    return (
      <header className="an-header">
        <HeaderMenu />
        <div className="header-right">
          <AvatarDropdown />
        </div>
      </header>
    );
  }
}

export default Header;
