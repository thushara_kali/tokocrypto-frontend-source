import axios from 'axios';

var instance = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL
});

var main_instance = axios.create({
    baseURL: process.env.REACT_APP_BASE_MAIN_URL
});

const fetchAddressList = (token, coinType) => {
    return instance.get('/address/' + coinType, {
        headers: { 'Authorization': token }
    });
}

const createAddress = (token, coinType) => {
    return instance.post('/address/' + coinType, null, {
        headers: { 'Authorization': token }
    });
}

const currencyPair = (token, currencyPair) => {
    return instance.get('/rates/changed/'+currencyPair, {
        headers: { 'Authorization': token }
    });
}
const getRates = (token) => {
    return instance.get('/rates', {
        headers: { 'Authorization': token }
    });
}

const fetchDashboard = (token) => {
    return instance.get('/dashboard', {
        headers: { 'Authorization': token }
    });
}

const createConversion = (token, data) => {
    return instance.post('/conversion/create', data, {
        headers: { 'Authorization': token }
    });
}

const confirmConversion = (token, data) => {
    return instance.post('/conversion/accept', data, {
        headers: { 'Authorization': token }
    });
}

const conversionRequest = (token, data) => {
    return instance.post('/conversion/request', data, {
        headers: { 'Authorization': token }
    });
}

const sellCoin = (token, data) => {
    return instance.post('/sell', data, {
        headers: { 'Authorization': token }
    });
}

const fetchConversions = (token, data) => {
    return instance.get('/transactions', {
        headers: { 'Authorization': token }
    });
}

const createBank = (token, data) => {
    return instance.post('/bank', data, {
        headers: { 'Authorization': token }
    });
}

const fetchBanks = (token) => {
    return instance.get('/banks', {
        headers: { 'Authorization': token }
    });
}

const fetchBank = (token, id) => {
    return instance.get('/bank/' + id, {
        headers: { 'Authorization': token }
    });
}

const fetchBankDepositAccounts = (token) => {
    return instance.get('/banks/deposit', {
        headers: { 'Authorization': token }
    });
}

const updateBank = (token, id, data) => {
    return instance.put('/bank/' + id, data, {
        headers: { 'Authorization': token }
    });
}

const updateName = (token, data) => {
    return instance.put('/userUpdate', data, {
        headers: { 'Authorization': token }
    });
}

const updateKYC = (token, data) => {
    return instance.post('/kyc', data, {
        headers: { 'Authorization': token }
    });
}

const fetchKYC = (token) => {
    return instance.get('/kyc', {
        headers: { 'Authorization': token }
    });
}

const fetchDepositHistory = (token) => {
    return instance.get('/deposit', {
        headers: { 'Authorization': token }
    });
}

const fetchWithdrawHistory = (token) => {
    return instance.get('/withdraw', {
        headers: { 'Authorization': token }
    });
}

const sendTranscation = (token, currency, data) => {
    return instance.post('/withdraw/' + currency, data, {
        headers: { 'Authorization': token }
    });
}

const withdraw = (token, data) => {
    return instance.post('/withdraw/idr', data, {
        headers: { 'Authorization': token }
    });
}

const requestOTP = (token, data) => {
    return instance.post('/otp', data, {
        headers: { 'Authorization': token }
    });
}

const checkOTP = (token) => {
    return instance.get('/mfa/config', {
        headers: { 'Authorization': token }
    });
}

const fetchLiveRate = (currencyPair, type) => {
  return instance.get('/rates/history?currencyPair='+currencyPair+'&type='+type);
}

const getNotifications = (token) => {
    return instance.get('/notifications', {
        headers: { 'Authorization': token }
    });
}

const getChartSummary = (currencyPair) => {
    return axios.get('https://tfxf49mpq1.execute-api.us-east-1.amazonaws.com/dev/rates/changed/'+currencyPair);
}

const clientMfa = (email) => {
    return main_instance.post('/otp/login/user', { email });
}

const clientMfaForceEmail = (email) => {
    return main_instance.post('/otp/login/user', { email, forceEmail: true });
}

const clientMfaVerify = (otpId, token) => {
    return  main_instance.post('/otp/login/verification', {
        "otpId": otpId,
        "token": token
     });
}

const fetchUser = (token) => {
    return instance.get('/accessibility', {
        headers: { 'Authorization': token }
    });
}

const getOTPStatus = (token) => {
    return instance.get('/otp/status', {
        headers: { 'Authorization': token }
    });
}

const OTPGAGenerate = (token) => {
    return instance.post('/otp/ga/generate', {}, {
        headers: { 'Authorization': token }
    });
}

const OTPGAEnable = (token, secret, userToken) => {
    return instance.post('/otp/ga/enable', {
        token: userToken,
        secret: secret
    }, {
        headers: { 'Authorization': token }
    });
}

const OTPGADisable = (token) => {
    return instance.delete('/otp/ga/disable', {
        headers: { 'Authorization': token }
    });
}

const getCurrencyUsage = (token, currency) => {
    return instance.post('/withdraw/limits', { currency }, {
        headers: { 'Authorization': token }
    });
}

const fetchPendingConfirmations = (token, data) => {
    return instance.post('/crypto/transactions/confirmations', data, {
        headers: { 'Authorization': token }
    });
}

const getBankNameList = (token) => {
    return instance.get('/list_bank', {
        headers: { 'Authorization': token }
    })
}

const deleteBank = (token, id) => {
    return instance.delete('/bank/'+id, {
        headers: { 'Authorization': token }
    });
}

// const recentTrades = (token,currency) => {
//     return instance.post('/recent-conversions/'+currency,{},{
//         headers: { 'Authorization': token }
//     });
// }

const recentTrades = (token,currency) => {
    return main_instance.post('/reports/recent-conversions/'+currency,{},{
        headers: { 'Authorization': token }
    });
}

const getWithdrawFees = (token) => {
    return instance.get('/getWithdrawFees', {
        headers: { 'Authorization': token }
    });
}

const getWithdrawLimits = (token) => {
    return instance.get('/getWithdrawLimits', {
        headers: { 'Authorization': token }
    });
}

const requestOTPWithdrawFiat = (token, data) => {
    return instance.post('/otp/withdraw-fiat', data, {
        headers: { 'Authorization': token }
    });
}

const requestOTPWithdrawCrypto = (token, data) => {
    return instance.post('/otp/withdraw-crypto', data, {
        headers: { 'Authorization': token }
    });
}

const getCurrencyConfig = (token) => {
    return instance.get('/trading/config', {
        headers: { 'Authorization': token }
    });
}

const getTradingLimit = (token) => {
    return instance.get('/trading/limit', {
        headers: { 'Authorization': token }
    });
}

const OTPLoginAfterCheckPassword = (token) => {
	return main_instance.post('/otp/login/user-request', {},{
		headers: { 'Authorization': token }
	});
}

const OTPLoginVerify = (userToken, otpId, token) => {
	return  main_instance.post('/otp/login/user-verification', {
		"otpId": otpId,
		"token": token
	},{
		headers: { 'Authorization': userToken }
	});
}

const getEmailVersion = (email) => {
    return main_instance.post('/client/getEmailAvailability', {email});
}

const pingRequest = (token) => {
	return instance.get('/ping',{
		headers: { 'Authorization': token }
	});
}

const referralFriendsList = (token) => {
    return instance.get('/referredUsersList', {
        headers: { 'Authorization': token }
    });
}

const commisionTotal = (token) => {
    return instance.get('/commision-Total', {
        headers: { 'Authorization': token }
    });
}

const commisionList = (token) => {
    return instance.get('/commision', {
        headers: { 'Authorization': token }
    });
}

const checkReferralCode = (token, data) => {
    return instance.post('/validateReferealKey', data, {
        headers: { 'Authorization': token }
    });
}

const confirmReferralCode = (token, data) => {
    return instance.post('/createOrUpdateKey', data, {
        headers: { 'Authorization': token }
    });
}

const fetchUserDetails = (token) => {
    return instance.get('/user', {
        headers: { 'Authorization': token }
    });
}

const referralConfigs = (token) => {
    return instance.get('/commision-config', {
        headers: { 'Authorization': token }
    });
}

const kycConfigs = (token) => {
    return instance.get('/getKycLimits', {
        headers: { 'Authorization': token }
    });
}

const sendLoginNotification = (token, email, name, deviceInfo, ipAddress) => {
    return main_instance.post('/otp/login/postEmailNotification', {
        email, name, deviceInfo, ipAddress
    }, {
        headers: { 'Authorization': token }
    });
}

const referralUserDiscount = (token) => {
    return instance.get('/discount', {
        headers: { 'Authorization': token }
    });
}

const fetchProfitLoss = (token) => {
    return main_instance.get('account/profitloss', {
        headers: { 'Authorization': token }
    });
}

const clearProfitLoss = (token) => {
    return main_instance.get('account/clearprofitloss', {
        headers: { 'Authorization': token }
    });
}

const confirmPromoCode = (token, data) => {
    return main_instance.post('promotion/addPromotionToUser', data, {
        headers: { 'Authorization': token }
    });
}

const generateVA = (token, data) => {
    return main_instance.post('bank/generate-va-account', data, {
        headers: { 'Authorization': token }
    });
}

const usableBalance = (token, data) => {
    return main_instance.post(`accounts/getAccountWithUsableBalance`, data, {
        headers: { 'Authorization': token }
    });
}

const getPromoCodeHistory = (token) => {
    return main_instance.get('promotion/getPromoHistory', {
        headers: { 'Authorization': token }
    });
}

// launchpad endpoints

const getFeaturedProjects = (token) => {
    return main_instance.get('/launchpad/client/project/status?document_status=active&pledge_status=featured', {
        headers: { 'Authorization': token }
    });
}

const getFeaturedDraftProjects = (token) => {
    return main_instance.get('/launchpad/client/project/status?document_status=draft&pledge_status=featured', {
        headers: { 'Authorization': token }
    });
}

const getArchivedProjects = (token) => {
    return main_instance.get('/launchpad/client/project/status?document_status=archived', {
        headers: { 'Authorization': token }
    });
}

const getOngoingProject = (token, date) => {
    return main_instance.get('/launchpad/client/project/active/contribution-date?date='+date, {
        headers: { 'Authorization': token }
    });
}

const getCompletedProjects = (token) => {
    return main_instance.get('/launchpad/client/project/status?document_status=completed', {
        headers: { 'Authorization': token }
    });
}

const getReview = (token, id) => {
    return main_instance.get('/launchpad/client/project/'+id+'/review', {
        headers: { 'Authorization': token }
    });
}

const getAllReviews = (token, id) => {
    return main_instance.get('/launchpad/client/review', {
        headers: { 'Authorization': token }
    });
}

const getProject = (token, id) => {
    return main_instance.get('/launchpad/client/project/'+id, {
        headers: { 'Authorization': token }
    });
}

const getAllProjects = (token) => {
    return main_instance.get('/launchpad/client/project', {
        headers: { 'Authorization': token }
    });
}

const getAllProjectRequests = (token) => {
    return main_instance.get('/launchpad/client/project-request', {
        headers: { 'Authorization': token }
    });
}

const getProjectRequest = (token, id) => {
    return main_instance.get('/launchpad/client/project-request/'+id, {
        headers: { 'Authorization': token }
    });
}

const submitProjectRequest = (token, data) => {
    return main_instance.post('/launchpad/client/project-request', data, {
        headers: { 'Authorization': token }
    });
}

const updateProjectRequest = (token, id, data) => {
    return main_instance.put('/launchpad/client/project-request/'+id, data, {
        headers: { 'Authorization': token }
    });
}

const updateProject = (token, id, data) => {
    return main_instance.put('/launchpad/client/project/'+id, data, {
        headers: { 'Authorization': token }
    });
}

const deleteProject = (token, id) => {
    return main_instance.delete('/launchpad/client/project/'+id, {
        headers: { 'Authorization': token }
    });
}

const updateReview = (token, id, data) => {
    return main_instance.put('/launchpad/client/review/'+id, data, {
        headers: { 'Authorization': token }
    });
}

const createProject = (token, data) => {
    return main_instance.post('/launchpad/client/project', data, {
        headers: { 'Authorization': token }
    });
}

const createReview = (token, data) => {
    return main_instance.post('/launchpad/client/review', data, {
        headers: { 'Authorization': token }
    });
}

const createTicket = (token, data) => {
    return main_instance.post('/launchpad/client/project/ticket', data, {
        headers: { 'Authorization': token }
    });
}

const requestLaunchpadUploadUrl = (token, name) => {
    return main_instance.post('/launchpad/client/upload-url', { name }, {
        headers: { 'Authorization': token }
    });
}

const pledgeProject = (token, projectId, amount, currency) => {
    return main_instance.post(`/launchpad/client/project/${projectId}/pledge`, { amount, currency }, {
        headers: { 'Authorization': token }
    });
}

const myPledge = (token) => {
    return main_instance.get(`/launchpad/client/pledge`, {
        headers: { 'Authorization': token }
    });
}

const contributionGroup = (token) => {
    return main_instance.get(`/launchpad/client/contribute/group`, {
        headers: { 'Authorization': token }
    });
}

const myContribution = (token) => {
    return main_instance.get(`/launchpad/client/contribute`, {
        headers: { 'Authorization': token }
    });
}

const contribute = (token, projectId, data) => {
    return main_instance.post(`launchpad/client/project/${projectId}/contribute`,  data, {
        headers: { 'Authorization': token }
    });
}

const getMyApiKeys = (token) => {
    return main_instance.get('apikey/client/apikey/all', {
        headers: { 'Authorization': token }
    });
}

const getSingleApiKey = (token, id) => {
    return main_instance.get(`apikey/client/apikey/${id}`, {
        headers: { 'Authorization': token }
    });
}

const updateSingleApiKey = (token, id, data) => {
    return main_instance.put(`apikey/client/apikey/${id}`, data, {
        headers: { 'Authorization': token }
    });
}

const createApiKey = (token, data) => {
    return main_instance.post(`apikey/client/apikey/create`, data, {
        headers: { 'Authorization': token }
    });
}

const revokeApiKey = (token, id) => {
    return main_instance.delete(`apikey/client/apikey/${id}/deactivate`, {
        headers: { 'Authorization': token }
    });
}

const activateApiKey = (token, id) => {
    return main_instance.put(`apikey/client/apikey/${id}/activate`, {}, {
        headers: { 'Authorization': token }
    });
}

const createGiftCard = (token, promoCode, currency, amount) => {
    return main_instance.post(`promotion/giftCardCreate`, {
        promo_code: promoCode,
        currency: currency,
        amount
    }, {
        headers: { 'Authorization': token }
    });
}

const redeemGiftCard = (token, promoCode) => {
    return main_instance.post(`promotion/addGiftCardToUser`, {
        promo_code: promoCode
    }, {
        headers: { 'Authorization': token }
    });
}

const giftCardHistory = (token) => {
    return main_instance.get(`promotion/getGiftCards`, {
        headers: { 'Authorization': token }
    });
}

const createNewWallet = (token, data) => {
    return main_instance.post(`/accounts/createAccount`, data, {
        headers: { 'Authorization': token }
    });
}

const checkTelegram = (token, data) => {
    return main_instance.post(`/client2/kyc/check-verified-social-media`, data, {
        headers: { 'Authorization': token }
    });
}

const addUsername = (token, data) => {
    return main_instance.post(`/client2/kyc/add-social-media`, data, {
        headers: { 'Authorization': token }
    });
}

const verifyAccount = (token, data) => {
    return main_instance.post(`/client2/kyc/verify-social-media`, data, {
        headers: { 'Authorization': token }
    });
}

const redeemGiftCardByCreated = (token) => {
    return main_instance.get(`promotion/getRedeemedByCreatedUser`, {
        headers: { 'Authorization': token }
    });
}

const redeemGiftCardByUser = (token) => {
    return main_instance.get(`promotion/getRedeemedByRedeemUser`, {
        headers: { 'Authorization': token }
    });
}

const getTopTrading = (token, data) => {
    return main_instance.post(`reports/getTradesByCurrency`, data, {
        headers: { 'Authorization': token }
    });
}

const getLatestNews = (per_page) => {
    return axios.get('https://news.tokocrypto.com/wp-json/wp/v2/posts?categories=6&per_page='+per_page);
}

const buyOrder = (token, data) => {
    return main_instance.post(`orderbook/createOrder`, data, {
        headers: { 'Authorization': token }
    });
}

const cancelOrder = (token, data) => {
    return main_instance.post(`orderbook/cancelOrder`, data, {
        headers: { 'Authorization': token }
    });
}

const orderBook = (token, data) => {
    return main_instance.post(`orderbook/getOrders`, data, {
        headers: { 'Authorization': token }
    });
}

const orderBookRaw = (token, data) => {
    return main_instance.post(`orderbook/getOrderRaw`, data, {
        headers: { 'Authorization': token }
    });
}

const getQuantity = (token, data) => {
    return main_instance.post(`orderbook/estimation/getQuantity`, data, {
        headers: { 'Authorization': token }
    });
}

const getFee = (token, data) => {
    return main_instance.post(`orderbook/estimation/getFee`, data, {
        headers: { 'Authorization': token }
    });
}

const orders = (token, data) => {
    return main_instance.post(`orderbook/getOrdersByUserRaw`, data, {
        headers: { 'Authorization': token }
    });
}

const cancelOrders = (token, data) => {
    return main_instance.post(`orderbook/cancelAllOrders`, data, {
        headers: { 'Authorization': token }
    });
}

const getDepthTrading = (data) => {
    return main_instance.post(`rates/getTradingViewNew`, data, {
        // headers: { 'Authorization': token }
    });
}

const fetchOrderResult = (token, data) => {
    return main_instance.post(`orderbook/orderConversionStatus`, data, {
        headers: { 'Authorization': token }
    });
}

const fetchTradingView = (currencyPair) => {
    // return main_instance.get(`client2/tradingview/history/daily?symbol=`+currencyPair, {});
    return main_instance.get(`client2/tradingview/history?symbol=${currencyPair}&resolution=1D`, {});
}

const getRatesDashboard = (token) => {
    return instance.get(`/rates`, {
        headers: { 'Authorization': token }
    });
}

const getAccountDashboard = (token) => {
    return main_instance.post(`/accounts/getAccountWithUsableBalance`,{}, {
        headers: { 'Authorization': token }
    });
}

const getCurrencyPair = (token) => {
    return instance.get(`/rates/currency_pair`, {
        headers: { 'Authorization': token }
    });
}

const submitDepositFiat = (token, data) => {
    return main_instance.post(`/deposit/submit-deposit`,data, {
        headers: { 'Authorization': token }
    });
}

const getDepositHistory = (token, data) => {
    return main_instance.post(`/deposit/depositPaginate`,data, {
        headers: { 'Authorization': token }
    });
}

const submitDepositAlfa = (token, data) => {
    return main_instance.post(`/bank/create-fixed-payment`,data, {
        headers: { 'Authorization': token }
    });
}

const fetchAllTrasnactions = (token, data) => {
    return main_instance.post(`/transaction/getTransactions`,data, {
        headers: { 'Authorization': token }
    });
}

export default {
    fetchTradingView,
    getRates,
    fetchDashboard,
    checkTelegram,
    addUsername,
    verifyAccount,
    createConversion,
    sellCoin,
    fetchAddressList,
    createAddress,
    confirmConversion,
    conversionRequest,
    submitDepositAlfa,
    contributionGroup,
    fetchConversions,
    createBank,
    fetchBanks,
    fetchBank,
    updateBank,
    currencyPair,
    updateKYC,
    fetchKYC,
    fetchDepositHistory,
    fetchWithdrawHistory,
    sendTranscation,
    withdraw,
    requestOTP,
    checkOTP,
    fetchLiveRate,
    getNotifications,
    getChartSummary,
    clientMfa,
    clientMfaVerify,
    fetchBankDepositAccounts,
    fetchUser,
    getOTPStatus,
    OTPGAGenerate,
    OTPGAEnable,
    OTPGADisable,
    clientMfaForceEmail,
    getCurrencyUsage,
    fetchPendingConfirmations,
    getBankNameList,
    deleteBank,
    recentTrades,
    getWithdrawFees,
    getWithdrawLimits,
    requestOTPWithdrawCrypto,
    requestOTPWithdrawFiat,
    getCurrencyConfig,
    getTradingLimit,
	OTPLoginAfterCheckPassword,
	OTPLoginVerify,
    getEmailVersion,
    pingRequest,
    updateName,
    usableBalance,
    referralFriendsList,
    commisionTotal,
    commisionList,
    checkReferralCode,
    confirmReferralCode,
    generateVA,
    fetchUserDetails,
    referralConfigs,
    kycConfigs,
    sendLoginNotification,
    referralUserDiscount,
    fetchProfitLoss,
    confirmPromoCode,
    getPromoCodeHistory,
    getFeaturedProjects,
    getOngoingProject,
    getCompletedProjects,
    getReview,
    getQuantity,
    getDepositHistory,
    createTicket,
    getProject,
    orderBookRaw,
    updateProject,
    createProject,
    getAllProjects,
    getAllReviews,
    getLatestNews,
    getFee,
    getArchivedProjects,
    createReview,
    updateReview,
    getAllProjectRequests,
    getProjectRequest,
    submitProjectRequest,
    updateProjectRequest,
    requestLaunchpadUploadUrl,
    deleteProject,
    getFeaturedDraftProjects,
    pledgeProject,
    myPledge,
    myContribution,
    contribute,
    clearProfitLoss,
    getMyApiKeys,
    getSingleApiKey,
    updateSingleApiKey,
    createApiKey,
    revokeApiKey,
    createGiftCard,
    redeemGiftCard,
    giftCardHistory,
    createNewWallet,
    activateApiKey,
    redeemGiftCardByCreated,
    redeemGiftCardByUser,
    getTopTrading,
    buyOrder,
    cancelOrder,
    orderBook,
    orders,
    cancelOrders,
    getDepthTrading,
    fetchOrderResult,
    getRatesDashboard,
    getAccountDashboard,
    getCurrencyPair,
    submitDepositFiat,
    fetchAllTrasnactions
};
