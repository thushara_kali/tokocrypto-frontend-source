// this file to set global endpoint function
import axios from "axios";

var instance = axios.create({
  baseURL: process.env.REACT_APP_BASE_ADMIN_URL
});

var main_instance = axios.create({
  baseURL: process.env.REACT_APP_BASE_MAIN_URL
});

const fetchUsers = (token, data) => {
  return instance.post("/users", data, {
    headers: {
      Authorization: token
    }
  });
};

const fetchUser = (token, id) => {
  return instance.get("/user/" + id, {
    headers: {
      Authorization: token
    }
  });
};

const fetchAccountByUser = (token, id) => {
  return instance.get("/user/" + id + "/accounts", {
    headers: {
      Authorization: token
    }
  });
};

const fetchAddress = token => {
  return instance.get("/address/list", {
    headers: {
      Authorization: token
    }
  });
};

const fetchAccount = (token, id) => {
  return instance.get("/account/" + id, {
    headers: {
      Authorization: token
    }
  });
};

const fetchAddressByUser = (token, id) => {
  return instance.get("/address/user/" + id, {
    headers: {
      Authorization: token
    }
  });
};

const fetchWallets = token => {
  return instance.get("/wallets/list", {
    headers: {
      Authorization: token
    }
  });
};

const fetchUSDTWallets = (token, data) => {
  return main_instance.post("/wallets/wallets/getAllOmniBalance", data, {
    headers: {
      Authorization: token
    }
  });
};

const fetchAddressList = (token, id) => {
  return instance.get("wallets/" + id + "/address", {
    headers: {
      Authorization: token
    }
  });
};

const fetchDeposit = token => {
  return instance.get("account/deposit/list", {
    headers: {
      Authorization: token
    }
  });
};

// const fetchDepositFiat = (token, data) => {
//     return main_instance.post('deposit/listDepositByCurrency', data, {
//         headers: {
//             'Authorization': token
//         }
//     });
// }

const fetchDepositFiat = (token, data) => {
  return main_instance.post("deposit/manual-approval", data, {
    headers: {
      Authorization: token
    }
  });
};

const fetchPendingDeposit = token => {
  return instance.get("account/deposit/pending", {
    headers: {
      Authorization: token
    }
  });
};

const fetchWithdraw = token => {
  return instance.get("account/withdraw/list", {
    headers: {
      Authorization: token
    }
  });
};

const fetchWithdrawList = (token, data) => {
  return main_instance.post("withdraw/getWithdrawList", data, {
    headers: {
      Authorization: token
    }
  });
};

const submitWithdraw = (token, data) => {
  return instance.post("account/withdraw", data, {
    headers: {
      Authorization: token
    }
  });
};

const submitDeposit = (token, data) => {
  return instance.post("account/deposit", data, {
    headers: {
      Authorization: token
    }
  });
};

const confirmDeposit = (token, data) => {
  return instance.post("/deposit/confirm", data, {
    headers: {
      Authorization: token
    }
  });
};

const fetchConversions = token => {
  return instance.get("conversion/list", {
    headers: {
      Authorization: token
    }
  });
};

const fetchConversion = (token, id) => {
  return instance.get("conversion/" + id, {
    headers: {
      Authorization: token
    }
  });
};

const fetchTrades = token => {
  return instance.get("trade/list", {
    headers: {
      Authorization: token
    }
  });
};

const fetchTrade = (token, id) => {
  return instance.get("trade/" + id, {
    headers: {
      Authorization: token
    }
  });
};

const fetchKYC = token => {
  return instance.get("kyc", {
    headers: {
      Authorization: token
    }
  });
};

const fetchKYCByUser = (token, id) => {
  return instance.get("kyc/" + id, {
    headers: {
      Authorization: token
    }
  });
};

const approveKYC = (token, id) => {
  return instance.post(
    "kyc/" + id + "/approve",
    {},
    {
      headers: {
        Authorization: token
      }
    }
  );
};

const rejectKYC = (token, id) => {
  return instance.post(
    "kyc/" + id + "/reject",
    {},
    {
      headers: {
        Authorization: token
      }
    }
  );
};

const manualTrades = (token, data) => {
  return instance.post("trade", data, {
    headers: {
      Authorization: token
    }
  });
};

const fetchUserByDepositCode = (token, id) => {
  return instance.get("user/deposit-code/" + id, {
    headers: {
      Authorization: token
    }
  });
};

const fetchAccountByUserId = (token, id) => {
  return instance.get("account/user/" + id + "/currency/IDR", {
    headers: {
      Authorization: token
    }
  });
};

const getSignedUrl = (token, filename, userId, service) => {
  return instance.get(
    "get-signed-url?filename=" +
      filename +
      "&userid=" +
      userId +
      "&service=" +
      service,
    {
      headers: {
        Authorization: token
      }
    }
  );
};

const fetchTradesByConversion = (token, id) => {
  return instance.get("trade/conversion/" + id, {
    headers: {
      Authorization: token
    }
  });
};

const fetchCurrencyPairs = token => {
  return instance.get("currency-pairs", {
    headers: {
      Authorization: token
    }
  });
};

const fetchSingleCurrencyPairs = (token, id) => {
  return instance.get("currency-pairs/" + id, {
    headers: {
      Authorization: token
    }
  });
};

const updateCurrencyPair = (token, id, data) => {
  return instance.put("currency-pairs/" + id, data, {
    headers: {
      Authorization: token
    }
  });
};

const confirmWithdrawals = (token, id) => {
  return instance.put("withdraw/complete/" + id, null, {
    headers: {
      Authorization: token
    }
  });
};

const fetchRates = token => {
  return instance.get("rates/", {
    headers: {
      Authorization: token
    }
  });
};

const fetchExposures = token => {
  return instance.get("exposure", {
    headers: {
      Authorization: token
    }
  });
};

const fetchOpenPositions = token => {
  return instance.get("positions/open", {
    headers: {
      Authorization: token
    }
  });
};

const fetchClosePositions = token => {
  return instance.get("positions/close", {
    headers: {
      Authorization: token
    }
  });
};

const updateTwoFA = (token, userid, data) => {
  return instance.put("/user/" + userid + "/mfa", data, {
    headers: {
      Authorization: token
    }
  });
};

const clientBalances = (token, currency) => {
  return instance.get("/account/" + currency + "/total", {
    headers: {
      Authorization: token
    }
  });
};

const clientBalancesMarket = (token, data) => {
  return main_instance.post("/orderbook/getTotalOrderValues", data, {
    headers: {
      Authorization: token
    }
  });
};

const walletBalances = (token, currency) => {
  return instance.get("/wallets/" + currency, {
    headers: {
      Authorization: token
    }
  });
};

const hotWalletBalances = (token) => {
  return main_instance.get("/wallets/tc/balances", {
    headers: {
      Authorization: token
    }
  });
};

const pendingWithdrawals = (token, currency) => {
  return instance.get("/withdrawal/pending/total/" + currency, {
    headers: {
      Authorization: token
    }
  });
};

const pendingDeposits = (token, currency) => {
  return instance.get("/deposit/pending/total/" + currency, {
    headers: {
      Authorization: token
    }
  });
};

const fetchNotifications = token => {
  return instance.get("/notifications", {
    headers: {
      Authorization: token
    }
  });
};

const createNotifications = (token, data) => {
  return instance.post("/notifications", data, {
    headers: {
      Authorization: token
    }
  });
};

const updateNotification = (token, id, data) => {
  return instance.put("/notifications/" + id, data, {
    headers: {
      Authorization: token
    }
  });
};

const admintMfa = email => {
  return main_instance.post("/otp/login/admin", { email });
};

const adminMfaVerify = (otpId, token) => {
  return main_instance.post("/otp/login/verification", {
    otpId: otpId,
    token: token
  });
};

const fetchBankByUser = (token, id) => {
  return instance.get("/banks/" + id, {
    headers: {
      Authorization: token
    }
  });
};

const submitBankAccount = (token, data) => {
  return instance.post("bank", data, {
    headers: {
      Authorization: token
    }
  });
};

const updateBankAccount = (token, accountNumber, data) => {
  return instance.put("bank/" + accountNumber, data, {
    headers: {
      Authorization: token
    }
  });
};

const fetchBankAccountDetails = (token, id) => {
  return instance.get("bankdetails/" + id, {
    headers: {
      Authorization: token
    }
  });
};

const enableDisableTrade = (token, userId, data) => {
  return instance.put("user/" + userId + "/trading", data, {
    headers: {
      Authorization: token
    }
  });
};

const rejectWithdraw = (token, withdrawId) => {
  return instance.post(
    "/account/rejectWithdraw",
    { withdrawId },
    {
      headers: {
        Authorization: token
      }
    }
  );
};

const geminiBalances = token => {
  return instance.get("/balance/gemini", {
    headers: {
      Authorization: token
    }
  });
};

const requestOTPAfterCheckPassword = token => {
  return main_instance.post(
    "/otp/login/admin-request",
    {},
    {
      headers: {
        Authorization: token
      }
    }
  );
};

const verifytOTPAfterCheckPassword = (userToken, otpId, token) => {
  return main_instance.post(
    "/otp/login/admin-verification",
    {
      otpId: otpId,
      token: token
    },
    {
      headers: {
        Authorization: userToken
      }
    }
  );
};

const rejectDepositIDR = (token, id) => {
  return instance.put(
    "/deposit/reject/" + id,
    {},
    {
      headers: {
        Authorization: token
      }
    }
  );
};

const getConfigs = token => {
  return instance.get("/configs", {
    headers: {
      Authorization: token
    }
  });
};

const getConfigSingle = (token, data) => {
  return instance.post("/configs/single", data, {
    headers: {
      Authorization: token
    }
  });
};

const updateConfigSingle = (token, data) => {
  return instance.put("/configs/update", data, {
    headers: {
      Authorization: token
    }
  });
};

const updateWithdrawalTRXID = (token, id, data) => {
  return instance.put("/withdraw/update/" + id, data, {
    headers: {
      Authorization: token
    }
  });
};

const updateDepositTRXID = (token, id, data) => {
  return instance.put("/deposit/update/idr/txid/" + id, data, {
    headers: {
      Authorization: token
    }
  });
};

const setAsPremium = (token, userId) => {
  return instance.post(
    "/kyc/" + userId + "/premium",
    {},
    {
      headers: {
        Authorization: token
      }
    }
  );
};

const sendToArtemis = (token, userId) => {
  return instance.post(
    "/kyc/" + userId + "/send-artemis",
    {},
    {
      headers: {
        Authorization: token
      }
    }
  );
};

const sendToAsliri = (token, userId) => {
  return main_instance.get("/kyc/asliri/verify-data/" + userId, {
    headers: {
      Authorization: token
    }
  });
};

const getPricingDiscovery = (token, currency) => {
  return main_instance.get("/pricing-discovery/getConfig/" + currency, {
    headers: {
      Authorization: token
    }
  });
};

const submitPricingDiscovery = (token, data, currency) => {
  return main_instance.post(
    "/pricing-discovery/getPricingDiscovery/" + currency,
    data,
    {
      headers: {
        Authorization: token
      }
    }
  );
};

const updatePricingConfigs = (token, data, currency) => {
  return main_instance.post(
    "/pricing-discovery/updatePricingConfigs/" + currency,
    data,
    {
      headers: {
        Authorization: token
      }
    }
  );
};

const editKyc = (token, data) => {
  return main_instance.post("/kyc/updateKycByadmin", data, {
    headers: {
      Authorization: token
    }
  });
};

const rejectKycReason = (token, data) => {
  return main_instance.post("/kyc/reject", data, {
    headers: {
      Authorization: token
    }
  });
};

const getPricingByCurrency = (token, currency) => {
  return main_instance.get(
    "/pricing-discovery/getPricingByCurrency/" + currency,
    {
      headers: {
        Authorization: token
      }
    }
  );
};

const fetchPromo = (token, data) => {
  return main_instance.post("promotion/getAllPromotions", data, {
    headers: {
      Authorization: token
    }
  });
};

const createPromoCode = (token, data) => {
  return main_instance.post("promotion/promotionAdd", data, {
    headers: {
      Authorization: token
    }
  });
};

const getAllProjects = token => {
  return main_instance.get("/launchpad/admin/project", {
    headers: { Authorization: token }
  });
};

const deleteProject = (token, id) => {
  return main_instance.delete("/launchpad/admin/project/" + id, {
    headers: { Authorization: token }
  });
};

const getProject = (token, id) => {
  return main_instance.get("/launchpad/admin/project/" + id, {
    headers: { Authorization: token }
  });
};

const createProject = (token, data) => {
  return main_instance.post("/launchpad/admin/project", data, {
    headers: { Authorization: token }
  });
};

const updateProject = (token, id, data) => {
  return main_instance.put("/launchpad/admin/project/" + id, data, {
    headers: { Authorization: token }
  });
};

const distributeProject = (token, id, data) => {
  return main_instance.post(
    "/launchpad/client/project/" + id + "/distributeCoin",
    data,
    {
      headers: { Authorization: token }
    }
  );
};

const getReview = (token, id) => {
  return main_instance.get("/launchpad/admin/project/" + id + "/review", {
    headers: { Authorization: token }
  });
};

const updateReview = (token, id, data) => {
  return main_instance.put("/launchpad/admin/review/" + id, data, {
    headers: { Authorization: token }
  });
};

const createReview = (token, data) => {
  return main_instance.post("/launchpad/admin/review", data, {
    headers: { Authorization: token }
  });
};

const getOngoingProject = (token, date) => {
  return main_instance.get(
    "/launchpad/admin/project/active/contribution-date?date=" + date,
    {
      headers: { Authorization: token }
    }
  );
};

const getCompletedProject = token => {
  return main_instance.get(
    "/launchpad/admin/project/status?document_status=completed",
    {
      headers: { Authorization: token }
    }
  );
};

const getDraftProject = token => {
  return main_instance.get(
    "/launchpad/admin/project/status?document_status=draft",
    {
      headers: { Authorization: token }
    }
  );
};

const getArchivedProject = token => {
  return main_instance.get(
    "/launchpad/admin/project/status?document_status=archived",
    {
      headers: { Authorization: token }
    }
  );
};

const getAllProjectRequests = token => {
  return main_instance.get("/launchpad/admin/project-request", {
    headers: { Authorization: token }
  });
};

const getProjectRequest = (token, id) => {
  return main_instance.get("/launchpad/admin/project-request/" + id, {
    headers: { Authorization: token }
  });
};

const updateProjectRequest = (token, id, data) => {
  return main_instance.put("/launchpad/admin/project-request/" + id, data, {
    headers: { Authorization: token }
  });
};

const requestLaunchpadUploadUrl = (token, name) => {
  return main_instance.post(
    "/launchpad/admin/upload-url",
    { name },
    {
      headers: { Authorization: token }
    }
  );
};

const getMinimumOrder = token => {
  return main_instance.get("/pricing-discovery/getBitfinexConfigs", {
    headers: { Authorization: token }
  });
};

const submitMinimumOrder = (token, data) => {
  return main_instance.post("/pricing-discovery/updateBitfinexConfigs", data, {
    headers: { Authorization: token }
  });
};

const submitSetConfig = (token, data) => {
  return main_instance.post("/orderbook/bulkConfigUpdate", data, {
    headers: { Authorization: token }
  });
};

const updateActiveOrder = (token, data) => {
  return main_instance.post("/orderbook/changeActiveOrdersDecimals", data, {
    headers: { Authorization: token }
  });
};

const bitfinexBalances = token => {
  return main_instance.get("/pricing-discovery/getBitfinexBalance", {
    headers: {
      Authorization: token
    }
  });
};

const getProjectContribution = (token, projectId) => {
  return main_instance.get(`/launchpad/admin/project/${projectId}/contribute`, {
    headers: {
      Authorization: token
    }
  });
};

const getProjectPledge = (token, projectId) => {
  return main_instance.get(`/launchpad/admin/project/${projectId}/pledge`, {
    headers: {
      Authorization: token
    }
  });
};

const fetchFromArtemis = (token, userId) => {
  return instance.put(
    `/kyc/${userId}/update-artemis-result`,
    {},
    {
      headers: {
        Authorization: token
      }
    }
  );
};

const getUserApiKeys = (token, userId) => {
  return main_instance.get(`/apikey/admin/apikey/users/${userId}`, {
    headers: {
      Authorization: token
    }
  });
};

const revokeApiKey = (token, apiKeyId) => {
  return main_instance.delete(`/apikey/admin/apikey/${apiKeyId}`, {
    headers: {
      Authorization: token
    }
  });
};

const createUserReports = (token, data) => {
  return main_instance.post("/reports/getUsersByFilter", data, {
    headers: { Authorization: token }
  });
};

const createWithdrawalHistoryReports = (token, data) => {
  return main_instance.post("/reports/getWithdrawByDateRange", data, {
    headers: { Authorization: token }
  });
};

const createUserBalanceReports = (token, data) => {
  return main_instance.post("/reports/getUserAccounts", data, {
    headers: { Authorization: token }
  });
};

const userActivityByDateRange = (token, data) => {
    return main_instance.post('/reports/getActivityByDateRange', data, {
        headers: { 'Authorization': token }
    });
}

const getProfitLossByDate = (token, data) => {
  return main_instance.post("/reports/getProfitLossByDate", data, {
    headers: { Authorization: token }
  });
};

const autocompleteUser = (token, data) => {
  return main_instance.post("/users/autocomplete", data, {
    headers: { Authorization: token }
  });
};
const getBankAccountByUser = (token, data) => {
  return main_instance.post("/bank/getAllBankAccountsByUserId", data, {
    headers: { Authorization: token }
  });
};
const manualConversion = (token, data) => {
  return main_instance.post("/otc/manual-conversion", data, {
    headers: { Authorization: token }
  });
};

const manualWithdrawal = (token, data) => {
  return instance.post("/account/withdraw", data, {
    headers: { Authorization: token }
  });
};

const createTradeReports = (token, data) => {
  return main_instance.post("/reports/getTradesByDateRange", data, {
    headers: { Authorization: token }
  });
};

const createMerkleReports = (token, data) => {
  return main_instance.post("/reports/getCsvWalletByDate", data, {
    headers: { Authorization: token }
  });
};

const createKycReports = (token, data) => {
  return main_instance.post("/reports/getKycByFilter", data, {
    headers: { Authorization: token }
  });
};

const createDepositReports = (token, data) => {
  return main_instance.post("/reports/getDepositByDateRange", data, {
    headers: { Authorization: token }
  });
};

const updatePromotion = (token, data) => {
  return main_instance.put("/promotion/promotionEdit", data, {
    headers: {
      Authorization: token
    }
  });
};

const getExposure = token => {
  return main_instance.get("/reports/getExposure", {
    headers: {
      Authorization: token
    }
  });
};
const customerBalance = (token, currency, query) => {
  return instance.get("/account/" + currency + "/total?" + query, {
    headers: {
      Authorization: token
    }
  });
};

const saveProfitLoss = (token, data) => {
  return main_instance.post("/reports/saveProfitLoss", data, {
    headers: {
      Authorization: token
    }
  });
};

const transactionReport = (token, data) => {
  return main_instance.post("/reports/transaction-report", data, {
    headers: {
      Authorization: token
    }
  });
};

const fetchTotalBalancesByUser = (
  token,
  userId,
  startDate,
  endDate,
  currency
) => {
  return instance.get(
    `/account/${currency}/total?start_date=${startDate} 00:00&end_date=${endDate} 23:59&user_id=${userId}&detail_view=1`,
    {
      headers: {
        Authorization: token
      }
    }
  );
};

export default {
  fetchUsers: fetchUsers,
  fetchUser: fetchUser,
  fetchWallets: fetchWallets,
  fetchAddressByUser: fetchAddressByUser,
  fetchAddressList: fetchAddressList,
  fetchDeposit: fetchDeposit,
  fetchWithdraw: fetchWithdraw,
  fetchAccountByUser: fetchAccountByUser,
  fetchAccount: fetchAccount,
  submitWithdraw: submitWithdraw,
  submitDeposit: submitDeposit,
  fetchConversions: fetchConversions,
  fetchTrades: fetchTrades,
  fetchTrade: fetchTrade,
  fetchConversion: fetchConversion,
  manualTrades: manualTrades,
  fetchKYC: fetchKYC,
  fetchKYCByUser: fetchKYCByUser,
  approveKYC: approveKYC,
  rejectKYC,
  customerBalance,
  transactionReport,
  manualConversion,
  createMerkleReports,
  fetchAccountByUserId: fetchAccountByUserId,
  fetchUserByDepositCode: fetchUserByDepositCode,
  fetchPendingDeposit: fetchPendingDeposit,
  confirmDeposit: confirmDeposit,
  fetchAddress: fetchAddress,
  getSignedUrl: getSignedUrl,
  fetchTradesByConversion: fetchTradesByConversion,
  fetchCurrencyPairs: fetchCurrencyPairs,
  fetchSingleCurrencyPairs: fetchSingleCurrencyPairs,
  updateCurrencyPair: updateCurrencyPair,
  confirmWithdrawals: confirmWithdrawals,
  fetchRates: fetchRates,
  fetchExposures: fetchExposures,
  fetchOpenPositions: fetchOpenPositions,
  fetchClosePositions: fetchClosePositions,
  updateTwoFA: updateTwoFA,
  clientBalances,
  walletBalances,
  fetchWithdrawList,
  pendingWithdrawals,
  fetchNotifications,
  fetchDepositFiat,
  getBankAccountByUser,
  createNotifications,
  updateActiveOrder,
  getProfitLossByDate,
  updateNotification,
  manualWithdrawal,
  pendingDeposits,
  editKyc,
  rejectKycReason,
  admintMfa,
  adminMfaVerify,
  fetchBankByUser: fetchBankByUser,
  submitBankAccount: submitBankAccount,
  fetchBankAccountDetails: fetchBankAccountDetails,
  updateBankAccount: updateBankAccount,
  enableDisableTrade: enableDisableTrade,
  rejectWithdraw,
  geminiBalances,
  sendToAsliri,
  requestOTPAfterCheckPassword,
  verifytOTPAfterCheckPassword,
  rejectDepositIDR,
  getConfigs,
  getConfigSingle,
  updateConfigSingle,
  updateWithdrawalTRXID,
  submitSetConfig,
  hotWalletBalances,
  setAsPremium,
  updateDepositTRXID,
  sendToArtemis,
  getPricingDiscovery,
  submitPricingDiscovery,
  fetchPromo,
  createPromoCode,
  distributeProject,
  getAllProjects,
  deleteProject,
  autocompleteUser,
  getProject,
  updateProject,
  getReview,
  updateReview,
  createReview,
  createProject,
  fetchUSDTWallets,
  getOngoingProject,
  getAllProjectRequests,
  getProjectRequest,
  updateProjectRequest,
  requestLaunchpadUploadUrl,
  getMinimumOrder,
  getPricingByCurrency,
  submitMinimumOrder,
  bitfinexBalances,
  getCompletedProject,
  getDraftProject,
  updatePricingConfigs,
  getArchivedProject,
  getProjectPledge,
  getProjectContribution,
  fetchFromArtemis,
  getUserApiKeys,
  revokeApiKey,
  createUserReports,
  createWithdrawalHistoryReports,
  createUserBalanceReports,
  userActivityByDateRange,
  createTradeReports,
  createKycReports,
  createDepositReports,
  updatePromotion,
  getExposure,
  saveProfitLoss,
  clientBalancesMarket,
  fetchTotalBalancesByUser
};
