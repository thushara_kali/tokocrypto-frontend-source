// service API for KYC at Admin
import services from '../../services/admin';

const fetchKYC = (token) => {
    return new Promise((resolve,reject) => {
        services.fetchKYC(token).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

export default {
    fetchKYC: fetchKYC
}