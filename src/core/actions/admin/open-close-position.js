// service API for Open/Close Position at Admin
import services from '../../services/admin';

const fetchOpenPositions = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchOpenPositions(token).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const fetchClosePositions = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchClosePositions(token).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

export default {
    fetchOpenPositions: fetchOpenPositions,
    fetchClosePositions: fetchClosePositions,
}