// service API for Conversion at Admin
import services from '../../services/admin';

const fetchConversions = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchConversions(token).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchConversion = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchConversion(token, id).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchTradesByConversion = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchTradesByConversion(token, id).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}
const autocompleteUser = (token, data) => {
    return new Promise((resolve, reject) => {
        services.autocompleteUser(token, data).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const getBankAccountByUser = (token, data) => {
    return new Promise((resolve, reject) => {
        services.getBankAccountByUser(token, data).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const manualConversion = (token, data) => {
    return new Promise((resolve, reject) => {
        services.manualConversion(token, data).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

export default {
    fetchConversions: fetchConversions,
    fetchConversion: fetchConversion,
    fetchTradesByConversion: fetchTradesByConversion,
    autocompleteUser,
    getBankAccountByUser,
    manualConversion
}