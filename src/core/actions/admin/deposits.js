// service API for Deposite at Admin
import services from '../../services/admin';

const fetchDeposit = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchDeposit(token).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchDepositFiat = (token, data) => {
    return new Promise((resolve, reject) => {
        services.fetchDepositFiat(token, data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchPendingDeposit = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchPendingDeposit(token).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}


const fetchUserByDepositCode = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchUserByDepositCode(token, id).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchAccountByUserId = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchAccountByUserId(token, id).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const submitWithdraw = (token, data) => {
    return new Promise((resolve, reject) => {
        services.submitWithdraw(token, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

const submitDeposit = (token, data) => {
    return new Promise((resolve, reject) => {
        services.submitDeposit(token, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

const confirmDeposit = (token, data) => {
    return new Promise((resolve, reject) => {
        services.confirmDeposit(token, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

const rejectDepositIDR = (token, id) => {
	return new Promise((resolve, reject) => {
		services.rejectDepositIDR(token, id).then(res => {
			resolve(res)
		}).catch(err => {
			reject(err)
		})
	})
}

const updateDepositTRXID = (token, id, data) => {
    return new Promise((resolve, reject) => {
        services.updateDepositTRXID(token, id, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

export default {
    fetchDeposit: fetchDeposit,
    fetchUserByDepositCode: fetchUserByDepositCode,
    fetchAccountByUserId: fetchAccountByUserId,
    submitWithdraw: submitWithdraw,
    submitDeposit: submitDeposit,
    fetchPendingDeposit: fetchPendingDeposit,
    confirmDeposit: confirmDeposit,
    rejectDepositIDR,
    fetchDepositFiat,
    updateDepositTRXID
}
