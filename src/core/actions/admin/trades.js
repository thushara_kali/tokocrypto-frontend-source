// service API for Trades at Admin
import services from '../../services/admin';

const fetchTrades = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchTrades(token).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchTrade = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchTrade(token, id).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const manualTrades = (token, data) => {
    return new Promise((resolve, reject) => {
        services.manualTrades(token, data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

export default {
    manualTrades: manualTrades,
    fetchTrades: fetchTrades,
    fetchTrade: fetchTrade
}