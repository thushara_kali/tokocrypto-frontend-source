// service API for Withdraws at Admin
import services from '../../services/admin';

const fetchWithdraw = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchWithdraw(token).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchWithdrawList = (token, data) => {
    return new Promise((resolve, reject) => {
        services.fetchWithdrawList(token, data).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const confirmWithdrawals = (token, id) => {
    return new Promise((resolve, reject) => {
        services.confirmWithdrawals(token, id).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const manualWithdrawal = (token, data) => {
    return new Promise((resolve, reject) => {
        services.manualWithdrawal(token, data).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const rejectWithdraw = (token, id) => {
    return new Promise((resolve, reject) => {
        services.rejectWithdraw(token, id).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const updateWithdrawalTRXID = (token, id, data) => {
    return new Promise((resolve, reject) => {
        services.updateWithdrawalTRXID(token, id, data).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

updateWithdrawalTRXID

export default {
    fetchWithdraw: fetchWithdraw,
    confirmWithdrawals: confirmWithdrawals,
    rejectWithdraw,
    manualWithdrawal,
    fetchWithdrawList,
    updateWithdrawalTRXID
}
