// service API for Reports at Admin
import services from "../../services/admin";

const createUserReports = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .createUserReports(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const createWithdrawalHistoryReports = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .createWithdrawalHistoryReports(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};
const createUserBalanceReports = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .createUserBalanceReports(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const userActivityByDateRange = (token, data) => {
    return new Promise((resolve,reject) => {
        services.userActivityByDateRange(token, data).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const createBalanceUserReports = (token, data) => {
    return new Promise((resolve,reject) => {
        services
          .createBalanceUserReports(token, data)
          .then(res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
    });
}

const createTradeReports = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .createTradeReports(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const createKycReports = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .createKycReports(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const createDepositReports = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .createDepositReports(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const createMerkleReports = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .createMerkleReports(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getExposure = token => {
  return new Promise((resolve, reject) => {
    services
      .getExposure(token)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getProfitLossByDate = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .getProfitLossByDate(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};
const saveProfitLoss = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .saveProfitLoss(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const transactionReport = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .transactionReport(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

export default {
  createUserReports,
  createWithdrawalHistoryReports,
  createUserBalanceReports,
  userActivityByDateRange,
  createTradeReports,
  createKycReports,
  createDepositReports,
  getExposure,
  createMerkleReports,
  saveProfitLoss,
  transactionReport,
  getProfitLossByDate
};
