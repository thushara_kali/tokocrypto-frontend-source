// service API for Home at Admin
import services from '../../services/admin';

const clientBalances = (token, currency) => {
    return new Promise((resolve, reject) => {
        services.clientBalances(token, currency).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const clientBalancesMarket = (token, data) => {
    return new Promise((resolve, reject) => {
        services.clientBalancesMarket(token, data).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const walletBalances = (token, currency) => {
    return new Promise((resolve, reject) => {
        services.walletBalances(token, currency).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const hotWalletBalances = (token) => {
    return new Promise((resolve, reject) => {
        services.hotWalletBalances(token).then(res => {
            resolve(res)
        })
        .catch(err => {
            reject(err)
        })
    })
}

const pendingWithdrawals = (token, currency) => {
    return new Promise((resolve, reject) => {
        services.pendingWithdrawals(token, currency).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const pendingDeposits = (token, currency) => {
    return new Promise((resolve, reject) => {
        services.pendingDeposits(token, currency).then(res => {
           resolve(res);
        }).catch(err => {
            reject(err);
        });
    })
}

const geminiBalances = (token) => {
    return new Promise((resolve, reject) => {
        services.geminiBalances(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    })
}

const bitfinexBalances = (token) => {
    return new Promise((resolve, reject) => {
        services.bitfinexBalances(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    })
}

const customerBalance = (token, currency, query) => {
    return new Promise((resolve, reject) => {
        services.customerBalance(token, currency, query).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    })
}

export default {
    clientBalances,
    clientBalancesMarket,
    walletBalances,
    customerBalance,
    pendingWithdrawals,
    pendingDeposits,
    hotWalletBalances,
    geminiBalances,
    bitfinexBalances
}
