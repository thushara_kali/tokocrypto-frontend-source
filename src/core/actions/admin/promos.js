// service API for Promosi at Admin
import services from '../../services/admin';

const fetchPromo = (token,data) => {
    return new Promise((resolve,reject) => {
        services.fetchPromo(token,data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const createPromoCode = (token,data) => {
    return new Promise((resolve,reject) => {
        services.createPromoCode(token,data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const updatePromotion = (token,data) => {
    return new Promise((resolve,reject) => {
        services.updatePromotion(token,data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

export default {
    fetchPromo: fetchPromo,
    createPromoCode,
    updatePromotion
}
