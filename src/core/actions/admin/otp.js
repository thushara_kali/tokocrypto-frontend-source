// service API for OTP at Admin
import services from '../../services/admin'

const loginOTP = (email) => {
    return new Promise((resolve, reject) => {
        services.admintMfa(email).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

const requestOTPAfterCheckPassword = (token) => {
	return new Promise((resolve, reject) => {
		services.requestOTPAfterCheckPassword(token).then(res => {
			resolve(res);
		}).catch(err => {
			reject(err);
		})
	});
}

const adminMfaVerify = (otpId, token) => {
    return new Promise((resolve, reject) => {
        services.adminMfaVerify(otpId, token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

const verifytOTPAfterCheckPassword = (userToken, otpId, token) => {
	return new Promise((resolve, reject) => {
		services.verifytOTPAfterCheckPassword(userToken, otpId, token).then(res => {
			resolve(res);
		}).catch(err => {
			reject(err);
		})
	});
}

export default {
    loginOTP,
    adminMfaVerify,
	requestOTPAfterCheckPassword,
	verifytOTPAfterCheckPassword
}
