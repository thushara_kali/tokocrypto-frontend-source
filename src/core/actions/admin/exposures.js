// service API for Exposures at Admin
import services from '../../services/admin';

const fetchExposures = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchExposures(token).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

export default {
    fetchExposures: fetchExposures
}