// service API for Setting Actions  at Admin
import services from '../../services/admin';

const getConfigs = (token) => {
    return services.getConfigs(token);
}

const getConfigSingle = (token,data) => {
    return services.getConfigSingle(token, data);
}

const updateConfigSingle = (token,data) => {
    return services.updateConfigSingle(token, data);
}

const getPricingDiscovery = (token,currency) => {
    return services.getPricingDiscovery(token, currency);
}

const getPricingByCurrency = (token,currency) => {
    return services.getPricingByCurrency(token, currency);
}

const submitPricingDiscovery = (token,data,currency) => {
    return services.submitPricingDiscovery(token, data, currency);
}

const updatePricingConfigs = (token,data,currency) => {
    return services.updatePricingConfigs(token, data, currency);
}

const getMinimumOrder = (token) => {
    return services.getMinimumOrder(token);
}

const submitMinimumOrder = (token,data) => {
    return services.submitMinimumOrder(token, data);
}

const submitSetConfig = (token,data) => {
    return services.submitSetConfig(token, data);
}

const updateActiveOrder = (token,data) => {
    return services.updateActiveOrder(token, data);
}

export default {
    getConfigs,
    getConfigSingle,
    updateConfigSingle,
    getPricingDiscovery,
    updateActiveOrder,
    submitPricingDiscovery,
    getMinimumOrder,
    submitMinimumOrder,
    updatePricingConfigs,
    submitSetConfig,
    getPricingByCurrency
}
