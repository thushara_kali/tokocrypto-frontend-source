// service API for Costumers at Admin
import services from '../../services/admin';

const fetchUsers = (token,data) => {
    return new Promise((resolve, reject) => {
        services.fetchUsers(token,data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchUser = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchUser(token, id).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchAccountByUser = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchAccountByUser(token, id).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchAddressByUser = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchAddressByUser(token, id).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchAccount = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchAccount(token, id).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const submitWithdraw = (token, data) => {
    return new Promise((resolve, reject) => {
        services.submitWithdraw(token, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

const submitDeposit = (token, data) => {
    return new Promise((resolve, reject) => {
        services.submitDeposit(token, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

const fetchKYCByUser = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchKYCByUser(token, id).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

const approveKYC = (token, id) => {
    return new Promise((resolve, reject) => {
        services.approveKYC(token, id).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

const rejectKYC = (token, id) => {
    return new Promise((resolve, reject) => {
        services.rejectKYC(token, id).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

const getSignedUrl = (token, filename, userId, service) => {
    return new Promise((resolve, reject) => {
        services.getSignedUrl(token, filename, userId, service).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const updateTwoFA = (token, userId, data) => {
    return new Promise((resolve, reject) => {
        services.updateTwoFA(token, userId, data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchBankByUser = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchBankByUser(token, id).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const submitBankAccount = (token, data) => {
    return new Promise((resolve, reject) => {
        services.submitBankAccount(token, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

const updateBankAccount = (token, accountNumber, data) => {
    return new Promise((resolve, reject) => {
        services.updateBankAccount(token, accountNumber, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

const fetchBankAccountDetails = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchBankAccountDetails(token, id).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const enableDisableTrade = (token, userId, data) => {
    return new Promise((resolve, reject) => {
        services.enableDisableTrade(token, userId, data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const setAsPremium = (token, userId) => {
    return new Promise((resolve, reject) => {
        services.setAsPremium(token, userId).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const sendToArtemis = (token, userId) => {
    return new Promise((resolve, reject) => {
        services.sendToArtemis(token, userId).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const sendToAsliri = (token, userId) => {
    return new Promise((resolve, reject) => {
        services.sendToAsliri(token, userId).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const editKyc = (token, data) => {
    return new Promise((resolve, reject) => {
        services.editKyc(token, data).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const rejectKycReason = (token, data) => {
    return new Promise((resolve, reject) => {
        services.rejectKycReason(token, data).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const fetchFromArtemis = (token, userId) => {
    return new Promise((resolve, reject) => {
        services.fetchFromArtemis(token, userId).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const fetchTotalBalancesByUser = (token,userId,startDate,endDate,currency) => {
    return new Promise((resolve, reject) => {
        services.fetchTotalBalancesByUser(token,userId,startDate,endDate,currency).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

export default {
    fetchUsers: fetchUsers,
    fetchUser: fetchUser,
    fetchAccountByUser: fetchAccountByUser,
    fetchAddressByUser: fetchAddressByUser,
    fetchAccount: fetchAccount,
    submitWithdraw: submitWithdraw,
    submitDeposit: submitDeposit,
    fetchKYCByUser: fetchKYCByUser,
    approveKYC: approveKYC,
    rejectKYC: rejectKYC,
    getSignedUrl: getSignedUrl,
    updateTwoFA: updateTwoFA,
    fetchBankByUser: fetchBankByUser,
    submitBankAccount: submitBankAccount,
    fetchBankAccountDetails: fetchBankAccountDetails,
    updateBankAccount: updateBankAccount,
    enableDisableTrade: enableDisableTrade,
    setAsPremium,
    editKyc,
    rejectKycReason,
    sendToArtemis,
    sendToAsliri,
    fetchFromArtemis,
    fetchTotalBalancesByUser
}
