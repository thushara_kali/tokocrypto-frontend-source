// service API for Rates at Admin
import services from '../../services/admin';

const fetchRates = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchRates(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

export default {
    fetchRates: fetchRates
}