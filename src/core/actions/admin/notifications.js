// service API for Notifications at Admin
import services from '../../services/admin';

const fetchNotifications = (token) => {
    return new Promise((resolve,reject) => {
        services.fetchNotifications(token).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const createNotifications = (token,data) => {
    return new Promise((resolve,reject) => {
        services.createNotifications(token,data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const updateNotifications = (token,id,data) => {
    return new Promise((resolve,reject) => {
        services.updateNotification(token,id,data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    });
}

export default {
    fetchNotifications,
    createNotifications,
    updateNotifications
}
