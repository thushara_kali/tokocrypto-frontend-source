// service API for Launchpad at Admin
import services from '../../services/admin';

const getAllProjects = (token) => {
    return new Promise((resolve,reject) => {
        services.getAllProjects(token).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const deleteProject = (token, id) => {
    return new Promise((resolve,reject) => {
        services.deleteProject(token, id).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const getProject = (token, id) => {
    return new Promise((resolve,reject) => {
        services.getProject(token, id).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const createProject = (token, data) => {
    return new Promise((resolve,reject) => {
        services.createProject(token, data).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const updateProject = (token, id, data) => {
    return new Promise((resolve,reject) => {
        services.updateProject(token, id, data).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const distributeProject = (token, id, data) => {
    return new Promise((resolve,reject) => {
        services.distributeProject(token, id, data).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const getReview = (token, id) => {
    return new Promise((resolve,reject) => {
        services.getReview(token, id).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const updateReview = (token, id, data) => {
    return new Promise((resolve,reject) => {
        services.updateReview(token, id, data).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const getOngoingProject = (token, date) => {
    return new Promise((resolve,reject) => {
        services.getOngoingProject(token, date).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const getCompletedProject = (token) => {
    return new Promise((resolve,reject) => {
        services.getCompletedProject(token).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const getArchivedProject = (token) => {
    return new Promise((resolve,reject) => {
        services.getArchivedProject(token).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const getDraftProject = (token) => {
    return new Promise((resolve,reject) => {
        services.getDraftProject(token).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const getAllProjectRequests = (token) => {
    return new Promise((resolve,reject) => {
        services.getAllProjectRequests(token).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const getProjectRequest = (token, id) => {
    return new Promise((resolve,reject) => {
        services.getProjectRequest(token, id).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const requestLaunchpadUploadUrl = (token, name) => {
    return new Promise((resolve,reject) => {
        services.requestLaunchpadUploadUrl(token, name).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const updateProjectRequest = (token, id, data) => {
    return new Promise((resolve,reject) => {
        services.updateProjectRequest(token, id, data).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const getProjectPledge = (token, id) => {
    return new Promise((resolve,reject) => {
        services.getProjectPledge(token, id).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const getProjectContribution = (token, id) => {
    return new Promise((resolve,reject) => {
        services.getProjectContribution(token, id).then(res => {
            resolve(res);
        })
        .catch(err => {
            reject(err);
        })
    });
}

const createReview = (token, data) => {
    return new Promise((resolve, reject) => {
      services
        .createReview(token, data)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
};

export default {
    getAllProjects,
    deleteProject,
    getProject,
    updateProject,
    getReview,
    updateReview,
    getOngoingProject,
    getAllProjectRequests,
    getProjectRequest,
    updateProjectRequest,
    distributeProject,
    requestLaunchpadUploadUrl,
    createProject,
    getCompletedProject,
    getDraftProject,
    getArchivedProject,
    getProjectContribution,
    getProjectPledge,
    createReview
}