// service API for Address at Admin
import services from '../../services/admin';

const fetchAddress = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchAddress(token).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const fetchUSDTWallets = (token,data) => {
    return new Promise((resolve, reject) => {
        services.fetchUSDTWallets(token,data).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}


export default {
    fetchAddress: fetchAddress,
    fetchUSDTWallets
}