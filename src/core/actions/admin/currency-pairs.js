// service API for Currency-Pair at Admin
import services from '../../services/admin';

const fetchCurrencyPairs = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchCurrencyPairs(token).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const fetchSingleCurrencyPair = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchSingleCurrencyPairs(token, id).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const updateCurrencyPair = (token, id, data) => {
    return new Promise((resolve, reject) => {
        services.updateCurrencyPair(token, id, data).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    })
}

export default {
    fetchCurrencyPairs: fetchCurrencyPairs,
    fetchSingleCurrencyPair: fetchSingleCurrencyPair,
    updateCurrencyPair: updateCurrencyPair
}