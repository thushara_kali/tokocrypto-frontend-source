// service API for APIKey at Admin
import services from '../../services/admin';

const getUserApiKeys = (token, userId) => {
    return new Promise((resolve,reject) => {
        services.getUserApiKeys(token, userId).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const revokeApiKey = (token, userId) => {
    return new Promise((resolve,reject) => {
        services.revokeApiKey(token, userId).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

export default {
    getUserApiKeys,
    revokeApiKey
}