import services from '../../services/client'

const buyCoin = (token, data) => {
    return new Promise((resolve, reject) => {
        services.createConversion(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const confirmBuy = (token, data) => {
    return new Promise((resolve, reject) => {
        services.confirmConversion(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                resolve(err)
            })
    })
}

const conversionBuy = (token, data) => {
    return new Promise((resolve, reject) => {
        services.conversionRequest(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

export default {
    buyCoin,
    confirmBuy,
    conversionBuy
}
