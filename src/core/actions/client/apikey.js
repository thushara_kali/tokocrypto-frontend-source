import services from '../../services/client'

const getMyApiKeys = (token) => {
    return new Promise((resolve, reject) => {
        services.getMyApiKeys(token).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const getSingleApiKey = (token, id) => {
    return new Promise((resolve, reject) => {
        services.getSingleApiKey(token, id).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const updateSingleApiKey = (token, id, data) => {
    return new Promise((resolve, reject) => {
        services.updateSingleApiKey(token, id, data).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const createApiKey = (token, data) => {
    return new Promise((resolve, reject) => {
        services.createApiKey(token, data).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const revokeApiKey = (token, id) => {
    return new Promise((resolve, reject) => {
        services.revokeApiKey(token, id).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const activateApiKey = (token, id) => {
    return new Promise((resolve, reject) => {
        services.activateApiKey(token, id).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

export default {
    getMyApiKeys,
    getSingleApiKey,
    updateSingleApiKey,
    createApiKey,
    revokeApiKey,
    activateApiKey
}