import services from '../../services/client'

const fetchConversions = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchConversions(token).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const fetchDepositHistory = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchDepositHistory(token).then(res => {
            resolve(res.data);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchWithdrawHistory = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchWithdrawHistory(token).then(res => {
            resolve(res.data);
        })
            .catch(err => {
                reject(err);
            })
    });
}

const fetchPendingConfirmations = (token, data) => {
    return new Promise((resolve, reject) => {
        services.fetchPendingConfirmations(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const pingRequest = (token) => {
	return new Promise((resolve, reject) => {
		services.pingRequest(token).then(res => {
			resolve(res);
		}).catch(err => {
			reject(err);
		})
	});
}

const fetchProfitLoss = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchProfitLoss(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const clearProfitLoss = (token) => {
    return new Promise((resolve, reject) => {
        services.clearProfitLoss(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const fetchAllTrasnactions = (token, data) => {
    return new Promise((resolve, reject) => {
        services.fetchAllTrasnactions(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

export default {
    fetchConversions,
    fetchDepositHistory,
    fetchWithdrawHistory,
    fetchPendingConfirmations,
    pingRequest,
    fetchProfitLoss,
    clearProfitLoss,
    fetchAllTrasnactions
}