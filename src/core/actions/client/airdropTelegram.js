import services from '../../services/client'

const checkTelegram = (token, data) => {
    return new Promise((resolve, reject) => {
        services.checkTelegram(token, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

const addUsername = (token, data) => {
  return new Promise((resolve, reject) => {
      services.addUsername(token, data).then(res => {
          resolve(res)
      }).catch(err => {
          reject(err)
      })
  })
}

const verifyAccount = (token, data) => {
  return new Promise((resolve, reject) => {
      services.verifyAccount(token, data).then(res => {
          resolve(res)
      }).catch(err => {
          reject(err)
      })
  })
}

export default {
  checkTelegram,
  addUsername,
  verifyAccount
};
