import services from '../../services/client'

function createGiftCard(token, promoCode, currency, amount) {
    return new Promise((resolve, reject) => {
        services.createGiftCard(token, promoCode, currency, amount).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

function redeemGiftCard(token, promoCode) {
    return new Promise((resolve, reject) => {
        services.redeemGiftCard(token, promoCode).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

function giftCardHistory(token) {
    return new Promise((resolve, reject) => {
        services.giftCardHistory(token).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

function redeemGiftCardByCreated(token) {
    return new Promise((resolve, reject) => {
        services.redeemGiftCardByCreated(token).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

function redeemGiftCardByUser(token) {
    return new Promise((resolve, reject) => {
        services.redeemGiftCardByUser(token).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

export default {
    createGiftCard,
    redeemGiftCard,
    giftCardHistory,
    redeemGiftCardByCreated,
    redeemGiftCardByUser
}
