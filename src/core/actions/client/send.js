import services from '../../services/client'

const getWalletDetail = (token, coin) => {
    return new Promise((resolve, reject) => {
        services.getWalletDetail(token, coin).then(function (response) {
            resolve(response.data.result)
        })
            .catch(function (error) {
                reject(error);
            });
    });
}


const sendTranscation = (token, otpId, otpToken, coin, address, amount) => {
    return new Promise((resolve, reject) => {
        let data = {};

        if (otpId !== null && otpToken !== null) {
            data = {
                otp: {
                    otpId: otpId,
                    token: Number(otpToken)
                },
                withdraw: {
                    address: address,
                    amount: amount
                }
            }
        } else {
            data = {
                withdraw: {
                    address: address,
                    amount: amount
                }
            }
        }

        services.sendTranscation(token, coin, data).then(function (response) {
            resolve(response.data)
        })
            .catch(function (error) {
                reject(error);
            });
    });
}

const getWithdrawFees = (token) => {
    return new Promise((resolve, reject) => {
        services.getWithdrawFees(token).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const getWithdrawLimits = (token) => {
    return new Promise((resolve, reject) => {
        services.getWithdrawLimits(token).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

export {
    getWalletDetail,
    sendTranscation,
    getWithdrawFees,
    getWithdrawLimits
};
