import services from '../../services/client'

const getWalletDetail = (token, coin) => {
    return new Promise((resolve, reject) => {
        services.getWalletDetail(token, coin).then(function (response) {
            resolve(response.data.result)
        })
            .catch(function (error) {
                reject(error);
            });
    });
}


// const createAdrress = (token, coin) => {
//   return new Promise((resolve, reject) => {
//     services.createAdrress(token, coin).then(function (response) {
//       resolve(response.data.result)
//     })
//       .catch(function (error) {
//         reject(error);
//       });
//   });
// }

const fetchAddressList = (token, coinType) => {
    return new Promise((resolve, reject) => {
        services.fetchAddressList(token, coinType).then(function (response) {
            resolve(response.data)
        })
            .catch(function (err) {
                reject(err)
            })
    })
}

const createAddress = (token, coinType) => {
    return new Promise((resolve, reject) => {
        services.createAddress(token, coinType).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}


export default {
    getWalletDetail,
    createAddress,
    fetchAddressList
};
