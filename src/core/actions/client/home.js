import services from '../../services/client'

const wallets = (token) => {
    return new Promise((resolve, reject) => {
        services.getWallet(token).then(function (response) {
            resolve(response.data)
        })
        .catch(function (error) {
            reject(error);
        });
    });
}


const rates = (token) => {
    return new Promise((resolve, reject) => {
        services.getRates(token).then(function (response) {
            resolve(response.data)
        })
        .catch(function (error) {
            reject(error);
        });
    });
}

const currencyPair = (token, currencyPair) => {
    return new Promise((resolve, reject) => {
        services.currencyPair(token, currencyPair).then(function (response) {
            resolve(response.data)
        })
        .catch(function (error) {
            reject(error);
        });
    });
}

const fetchDashboard = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchDashboard(token).then((res) => {
            resolve(res.data)
        })
        .catch((err) => {
            reject(err)
        })
    })
}

const updateKYC = (token, data) => {
    return new Promise((resolve, reject) => {
        services.updateKYC(token, data).then((res) => {
            resolve(res);
        })
        .catch((err) => {
            reject(err);
        })
    })
}

const fetchKYC = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchKYC(token).then(res => {
            resolve(res.data);
        })
        .catch(err => {
            reject(err);
        })
    })
}

const setKYCStatus = (payload) => {
    return {
        type: 'SET_STATUS',
        payload: payload
    }
}

const setTradingStatus = (payload) => {
    return {
        type: 'SET_STATUS_TRADING',
        payload: payload
    }
}

const fetchLiveRate = (currencyPair, type) => {
    return new Promise((resolve, reject) => {
        services.fetchLiveRate(currencyPair, type).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
}

const getNotifications = (token) => {
    return new Promise((resolve, reject) => {
        services.getNotifications(token).then(res => {
           resolve(res);
        }).catch((err) => {
           reject(err);
        });
    });
}

const getChartSummary = (currencyPair) => {
    return new Promise((resolve, reject) => {
        services.getChartSummary(currencyPair).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const getCurrencyUsage = (token, currency) => {
    return new Promise((resolve, reject) => {
        services.getCurrencyUsage(token, currency).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const recentTrades = (token,currency) => {
    return new Promise((resolve, reject) => {
        services.recentTrades(token,currency).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const getCurrencyConfig = (token) => {
    return new Promise((resolve, reject) => {
        services.getCurrencyConfig(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const getTradingLimit = (token) => {
    return new Promise((resolve, reject) => {
        services.getTradingLimit(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const kycConfigs = (token) => {
    return new Promise((resolve, reject) => {
        services.kycConfigs(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const createNewWallet = (token, data) => {
    return new Promise((resolve, reject) => {
        services.createNewWallet(token, data).then((res) => {
            resolve(res);
        })
        .catch((err) => {
            reject(err);
        })
    })
}

const usableBalance = (token, data) => {
    return new Promise((resolve, reject) => {
        services.usableBalance(token, data).then((res) => {
            resolve(res);
        })
        .catch((err) => {
            reject(err);
        })
    })
}

const getTopTrading = (token, data) => {
    return new Promise((resolve, reject) => {
        services.getTopTrading(token, data).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const getLatestNews = (per_page = 10) => {
    return new Promise((resolve, reject) => {
        services.getLatestNews(per_page).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const getDepthTrading = (data) => {
    return new Promise((resolve, reject) => {
        services.getDepthTrading(data).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const fetchTradingView = (currencyPair) => {
    return new Promise(( resolve, reject ) => {
        services.fetchTradingView(currencyPair).then( res => {
            resolve(res);
        }).catch( err => {
            reject(err);
        })
    })
}

const getRatesDashboard = (token) => {
    return new Promise((resolve, reject) => {
        services.getRatesDashboard(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const getAccountDashboard = (token) => {
    return new Promise((resolve, reject) => {
        services.getAccountDashboard(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const getCurrencyPair = (token) => {
    return new Promise((resolve, reject) => {
        services.getCurrencyPair(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

export default {
    fetchTradingView,
    recentTrades,
    wallets,
    rates,
    fetchDashboard,
    updateKYC,
    fetchKYC,
    setKYCStatus,
    fetchLiveRate,
    getNotifications,
    getLatestNews,
    getChartSummary,
    setTradingStatus,
    getCurrencyUsage,
    getCurrencyConfig,
    currencyPair,
    getTradingLimit,
    kycConfigs,
    usableBalance,
    createNewWallet,
    getTopTrading,
    getDepthTrading,
    getRatesDashboard,
    getAccountDashboard,
    getCurrencyPair
};
