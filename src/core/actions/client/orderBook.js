import services from '../../services/client'

const buyOrder = (token, data) => {
    return new Promise((resolve, reject) => {
        services.buyOrder(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const cancelOrder = (token, data) => {
    return new Promise((resolve, reject) => {
        services.cancelOrder(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const orderBook = (token, data) => {
    return new Promise((resolve, reject) => {
        services.orderBook(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const orderBookRaw = (token, data) => {
    return new Promise((resolve, reject) => {
        services.orderBookRaw(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const getQuantity = (token, data) => {
    return new Promise((resolve, reject) => {
        services.getQuantity(token, data).then(res => {
            resolve(res.data);
        })
        .catch(err => {
            reject(err);
        });
    });
}

const getFee = (token, data) => {
    return new Promise((resolve, reject) => {
        services.getFee(token, data).then(res => {
            resolve(res.data);
        })
        .catch(err => {
            reject(err);
        });
    });
}

const orders = (token, data) => {
    return new Promise((resolve, reject) => {
        services.orders(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const cancelOrders = (token, data) => {
    return new Promise((resolve, reject) => {
        services.cancelOrders(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const fetchOrderResult = (token, data) => {
    return new Promise((resolve, reject) => {
        services.fetchOrderResult(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}


export default {
    buyOrder,
    cancelOrder,
    orderBook,
    getQuantity,
    orderBookRaw,
    getFee,
    orders,
    cancelOrders,
    fetchOrderResult
}
