import services from '../../services/client'

const submitDepositFiat = (token, data) => {
    return new Promise((resolve, reject) => {
        services.submitDepositFiat(token, data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    })
}

const submitDepositAlfa = (token, data) => {
    return new Promise((resolve, reject) => {
        services.submitDepositAlfa(token, data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    })
}

const getDepositHistory = (token, data) => {
    return new Promise((resolve, reject) => {
        services.getDepositHistory(token, data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    })
}

export default {
    submitDepositFiat: submitDepositFiat,
    submitDepositAlfa,
    getDepositHistory
};
