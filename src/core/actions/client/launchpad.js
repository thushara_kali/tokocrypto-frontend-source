import services from "../../services/client";

const getFeaturedProjects = token => {
  return new Promise((resolve, reject) => {
    services
      .getFeaturedProjects(token)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getFeaturedDraftProjects = token => {
  return new Promise((resolve, reject) => {
    services
      .getFeaturedDraftProjects(token)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getAllProjects = token => {
  return new Promise((resolve, reject) => {
    services
      .getAllProjects(token)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getArchivedProjects = token => {
  return new Promise((resolve, reject) => {
    services
      .getArchivedProjects(token)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getAllReviews = token => {
  return new Promise((resolve, reject) => {
    services
      .getAllReviews(token)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getReview = (token, id) => {
  return new Promise((resolve, reject) => {
    services
      .getReview(token, id)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getProject = (token, id) => {
  return new Promise((resolve, reject) => {
    services
      .getProject(token, id)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const deleteProject = (token, id) => {
  return new Promise((resolve, reject) => {
    services
      .deleteProject(token, id)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const updateProject = (token, id, data) => {
  return new Promise((resolve, reject) => {
    services
      .updateProject(token, id, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const updateReview = (token, id, data) => {
  return new Promise((resolve, reject) => {
    services
      .updateReview(token, id, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const createProject = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .createProject(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const createReview = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .createReview(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getOngoingProject = (token, date) => {
  return new Promise((resolve, reject) => {
    services
      .getOngoingProject(token, date)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getCompletedProjects = token => {
  return new Promise((resolve, reject) => {
    services
      .getCompletedProjects(token)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const createTicket = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .createTicket(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getAllProjectRequests = token => {
  return new Promise((resolve, reject) => {
    services
      .getAllProjectRequests(token)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getProjectRequest = (token, id) => {
  return new Promise((resolve, reject) => {
    services
      .getProjectRequest(token, id)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const submitProjectRequest = (token, data) => {
  return new Promise((resolve, reject) => {
    services
      .submitProjectRequest(token, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const updateProjectRequest = (token, id, data) => {
  return new Promise((resolve, reject) => {
    services
      .updateProjectRequest(token, id, data)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const requestLaunchpadUploadUrl = (token, name) => {
  return new Promise((resolve, reject) => {
    services
      .requestLaunchpadUploadUrl(token, name)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const pledgeProject = (token, projectId, amount, currency) => {
  return new Promise((resolve, reject) => {
    services
      .pledgeProject(token, projectId, amount, currency)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const myPledge = (token) => {
  return new Promise((resolve, reject) => {
    services
      .myPledge(token)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const contributionGroup = (token) => {
  return new Promise((resolve, reject) => {
    services
      .contributionGroup(token)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const myContribution = (token) => {
  return new Promise((resolve, reject) => {
    services
      .myContribution(token)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const contribute = (token, projectId, amount) => {
  return new Promise((resolve, reject) => {
    services
      .contribute(token, projectId, amount)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
};

export default {
  getFeaturedProjects,
  getOngoingProject,
  getCompletedProjects,
  createTicket,
  getReview,
  getProject,
  updateProject,
  createProject,
  getAllProjects,
  getAllReviews,
  getArchivedProjects,
  createReview,
  updateReview,
  contributionGroup,
  getProjectRequest,
  getAllProjectRequests,
  submitProjectRequest,
  updateProjectRequest,
  requestLaunchpadUploadUrl,
  deleteProject,
  getFeaturedDraftProjects,
  pledgeProject,
  myContribution,
  myPledge,
  contribute
};
