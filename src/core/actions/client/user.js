import services from '../../services/client'

const fetchUser = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchUser(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};

const updateName = (token, data) => {
    return new Promise((resolve, reject) => {
        services.updateName(token, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

const fetchUserDetails = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchUserDetails(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};

export default {
    fetchUser,
    updateName,
    fetchUserDetails
};
