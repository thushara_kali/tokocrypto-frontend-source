import services from '../../services/client'

const sellCoin = (token, data) => {
    return new Promise((resolve, reject) => {
        services.sellCoin(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const confirmSell = (token, data) => {
    return new Promise((resolve, reject) => {
        services.confirmConversion(token, data).then(res => {
            resolve(res.data)
        })
            .catch(err => {
                reject(err)
            })
    })
}

export default {
    sellCoin,
    confirmSell
}