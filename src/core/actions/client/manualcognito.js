export function setManualCognito(payload) {
	return {
		type: "SET_MANUAL_COGNITO",
		payload: payload
	}
}
