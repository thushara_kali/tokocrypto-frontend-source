import services from '../../services/client'

const createBank = (token, data) => {
    return new Promise((resolve, reject) => {
        services.createBank(token, data).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const fetchBanks = (token, data) => {
    return new Promise((resolve, reject) => {
        services.fetchBanks(token).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const fetchBankDepositAccounts = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchBankDepositAccounts(token).then(res => {
            resolve(res)
        })
        .catch(err => {
            reject(err)
        })
    })
}



const fetchBank = (token, id) => {
    return new Promise((resolve, reject) => {
        services.fetchBank(token, id).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const generateVA = (token, data) => {
    return new Promise((resolve, reject) => {
        services.generateVA(token, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

const updateBank = (token, id, data) => {
    return new Promise((resolve, reject) => {
        services.updateBank(token, id, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

const getBankNameList = (token) => {
    return new Promise((resolve, reject) => {
        services.getBankNameList(token).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

const deleteBank = (token, id) => {
    return new Promise((resolve, reject) => {
        services.deleteBank(token, id).then(res => {
            resolve(res)
        })
            .catch(err => {
                reject(err)
            })
    })
}

export default {
    createBank,
    fetchBanks,
    fetchBank,
    updateBank,
    fetchBankDepositAccounts,
    getBankNameList,
    generateVA,
    deleteBank
}