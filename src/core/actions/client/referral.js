import services from '../../services/client'

const referralFriendsList = (token) => {
    return new Promise((resolve, reject) => {
        services.referralFriendsList(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const commisionTotal = (token) => {
    return new Promise((resolve, reject) => {
        services.commisionTotal(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const commisionList = (token) => {
    return new Promise((resolve, reject) => {
        services.commisionList(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const checkReferralCode = (token, data) => {
    return new Promise((resolve, reject) => {
        services.checkReferralCode(token, data).then((res) => {
            resolve(res);
        })
            .catch((err) => {
                reject(err);
            })
    })
}

const confirmReferralCode = (token, data) => {
    return new Promise((resolve, reject) => {
        services.confirmReferralCode(token, data).then((res) => {
            resolve(res);
        })
            .catch((err) => {
                reject(err);
            })
    })
}

const referralConfigs = (token) => {
    return new Promise((resolve, reject) => {
        services.referralConfigs(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const referralUserDiscount = (token) => {
    return new Promise((resolve, reject) => {
        services.referralUserDiscount(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

const confirmPromoCode = (token, data) => {
    return new Promise((resolve, reject) => {
        services.confirmPromoCode(token, data).then((res) => {
            resolve(res);
        })
            .catch((err) => {
                reject(err);
            })
    })
}

const getPromoCodeHistory = (token) => {
    return new Promise((resolve, reject) => {
        services.getPromoCodeHistory(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

export default {
    referralFriendsList,
    commisionTotal,
    commisionList,
    checkReferralCode,
    confirmReferralCode,
    referralConfigs,
    referralUserDiscount,
    confirmPromoCode,
    getPromoCodeHistory
};
