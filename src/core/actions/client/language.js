export function setLanguage(payload) {
	return {
		type: "SET_LANGUAGE",
		payload: payload
	}
}
