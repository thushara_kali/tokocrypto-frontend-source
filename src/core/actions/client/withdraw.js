import services from '../../services/client'

const fetchBanks = (token) => {
    return new Promise((resolve, reject) => {
        services.fetchBanks(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};

const withdraw = (token, data) => {
    return new Promise((resolve, reject) => {
        services.withdraw(token, data).then(res => {
            resolve(res);
        })
            .catch(err => {
                reject(err);
            })
    })
}

export default {
    fetchBanks: fetchBanks,
    withdraw: withdraw
};
