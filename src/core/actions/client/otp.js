import services from '../../services/client'

const requestOTP = (token, coin) => {
    return new Promise((resolve, reject) => {
        services.requestOTP(token, coin).then(function (res) {
            resolve(res)
        })
            .catch(function (error) {
                reject(error);
            });
    });
}

const requestOTPWithdrawFiat = (token, coin) => {
    return new Promise((resolve, reject) => {
        services.requestOTPWithdrawFiat(token, coin).then(function (res) {
            resolve(res)
        })
            .catch(function (error) {
                reject(error);
            });
    });
}

const requestOTPWithdrawCrypto = (token, coin) => {
    return new Promise((resolve, reject) => {
        services.requestOTPWithdrawCrypto(token, coin).then(function (res) {
            resolve(res)
        })
            .catch(function (error) {
                reject(error);
            });
    });
}

const checkOTP = (token) => {
    return new Promise((resolve, reject) => {
        services.checkOTP(token).then(function (res) {
            resolve(res)
        })
            .catch(function (error) {
                reject(error);
            });
    });
}

const loginOTP = (email) => {
    return new Promise((resolve, reject) => {
        services.clientMfa(email).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

const OTPLoginAfterCheckPassword = (token) => {
	return new Promise((resolve, reject) => {
		services.OTPLoginAfterCheckPassword(token).then(res => {
			resolve(res);
		}).catch(err => {
			reject(err);
		})
	});
}

const loginOTPForceEmail = (email) => {
    return new Promise((resolve, reject) => {
        services.clientMfaForceEmail(email).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

const OTPLoginVerify = (userToken, otpId, token) => {
    return new Promise((resolve, reject) => {
        services.OTPLoginVerify(userToken, otpId, token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

const clientMfaVerify = (otpId, token) => {
	return new Promise((resolve, reject) => {
		services.clientMfaVerify(otpId, token).then(res => {
			resolve(res);
		}).catch(err => {
			reject(err);
		})
	});
}

const getOTPStatus = (token) => {
    return new Promise((resolve, reject) => {
        services.getOTPStatus(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

const OTPGAGenerate = (token) => {
    return new Promise((resolve, reject) => {
        services.OTPGAGenerate(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

const OTPGAEnable = (token, secret, userToken) => {
    return new Promise((resolve, reject) => {
        services.OTPGAEnable(token, secret, userToken).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

const OTPGADisable = (token) => {
    return new Promise((resolve, reject) => {
        services.OTPGADisable(token).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

const sendLoginNotification = (token, email, name, deviceInfo, ipAddress) => {
    return new Promise((resolve, reject) => {
        services.sendLoginNotification(token, email, name, deviceInfo, ipAddress).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
}

export default {
    requestOTP,
    checkOTP,
    loginOTP,
    clientMfaVerify,
    getOTPStatus,
    OTPGAGenerate,
    OTPGAEnable,
    OTPGADisable,
    loginOTPForceEmail,
    requestOTPWithdrawCrypto,
    requestOTPWithdrawFiat,
	OTPLoginAfterCheckPassword,
	OTPLoginVerify,
    sendLoginNotification
}
