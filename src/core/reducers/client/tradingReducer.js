export default function (state = {status: "rejected"}, action) {
    switch (action.type) {
      case 'SET_STATUS_TRADING':
        state.status = action.payload
        return Object.assign({}, state)
      default:
        return state;
    }
}
  