export default function (state = {status: ""}, action) {
    switch (action.type) {
      case 'SET_STATUS':
        state.status = action.payload
        return Object.assign({}, state)
      default:
        return state;
    }
}
  