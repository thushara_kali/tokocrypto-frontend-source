import { combineReducers } from 'redux';
import notificationReducers from './notificationReducers.js';
import toggleMenu from './menuReducers.js';
import tableReducers from './tableReducers.js';
import areaChartReducers from './chart-reducers/chartReducers.js';
import lineChartReducers from './chart-reducers/lineChartReducers.js';
import tinyChartReducers from './chart-reducers/tinyChartReducers.js';
import productTableReducers from './productTableReducers.js';
import productsReducers from './productReducers.js';
import teamReducres from './teamReducres.js';
import serviceReducers from './serviceReducers.js';
import blogReducers from './blogReducers.js';
import { cognito } from '../../react-cognito/index'
import kycReducers from './kycReducers';
import tradingReducer from './tradingReducer.js';
import languageReducer from './languageReducer.js';
import manualCognito from './manualCognito.js';

const allReducers = combineReducers({
    notifications: notificationReducers,
    menu: toggleMenu,
    tableData: tableReducers,
    areaChartData: areaChartReducers,
    lineChartData: lineChartReducers,
    tinyChartData: tinyChartReducers,
    productsTable: productTableReducers,
    products: productsReducers,
    teams: teamReducres,
    services: serviceReducers,
    blogs: blogReducers,
    cognito: cognito,
    kyc: kycReducers,
    trading: tradingReducer,
    language: languageReducer,
    manualCognito: manualCognito
});

export default allReducers;
