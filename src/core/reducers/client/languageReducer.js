export default function (state = "id", action) {
	switch (action.type) {
		case 'SET_LANGUAGE':
			state = action.payload
			return state
		default:
			return state;
	}
}
