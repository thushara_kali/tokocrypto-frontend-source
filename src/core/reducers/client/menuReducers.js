export default function (state = true, action) {
  switch (action.type) {
    case 'MENU_TOGGLE':
      return !state;
    case 'SET_ACTIVE':
      state.active = action.payload
      return Object.assign({}, state)
    default:
      return state;
  }
}
