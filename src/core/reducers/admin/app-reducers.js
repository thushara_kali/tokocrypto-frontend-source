// this function using to set global state for App-Reducers
export default function (state = { active: "dashboard" }, action) {
    switch (action.type) {
        case 'SET_ACTIVE_PAGE':
            state.active = action.payload
            return Object.assign({}, state)
        default:
            return state;
    }
}