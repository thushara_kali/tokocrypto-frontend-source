import { combineReducers } from 'redux';
import { cognito } from '../../react-cognito/index';
import appReducer from './app-reducers';

const allReducers = combineReducers({
    cognito: cognito,
    app: appReducer
});

export default allReducers;