import React, { Component } from 'react';
import PropTypes from 'prop-types';
import countries from './countries';
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ReactFlagsSelect = function (_Component) {

	_inherits(ReactFlagsSelect, _Component);

	function ReactFlagsSelect(props) {

		_classCallCheck(this, ReactFlagsSelect);

		var _this = _possibleConstructorReturn(this, _Component.call(this, props));

		var fullCountries = Object.keys(countries);

		var selectCountries = _this.props.countries && _this.props.countries.filter(function (country) {
			return countries[country];
		});

		//Filter BlackList
		selectCountries = !_this.props.blackList ? selectCountries : fullCountries.filter(function (countryKey) {
			return selectCountries.filter(function (country) {
				return countryKey === country;
			}).length === 0;
		});

		var defaultCountry = countries[_this.props.defaultCountry] && _this.props.defaultCountry;

		_this.state = {
			openOptions: false,
			countries: selectCountries || fullCountries,
			defaultCountry: defaultCountry,
			filteredCountries: []
		};

		_this.toggleOptions = _this.toggleOptions.bind(_this);
		_this.closeOptions = _this.closeOptions.bind(_this);
		_this.onSelect = _this.onSelect.bind(_this);
		_this.filterSearch = _this.filterSearch.bind(_this);
		return _this;
	}

	ReactFlagsSelect.prototype.toggleOptions = function toggleOptions() {
		!this.state.disabled && this.setState({
			openOptions: !this.state.openOptions
		});
	};

	ReactFlagsSelect.prototype.closeOptions = function closeOptions(event) {
		if (event.target !== this.refs.selectedFlag && event.target !== this.refs.flagOptions && event.target !== this.refs.filterText) {
			this.setState({
				openOptions: false
			});
		}
	};

	ReactFlagsSelect.prototype.onSelect = function onSelect(countryCode) {
		this.setState({
			selected: countryCode,
			filter: ''
		});
		this.props.onSelect && this.props.onSelect(countryCode);
	};

	ReactFlagsSelect.prototype.updateSelected = function updateSelected(countryCode) {
		var isValid = countries[countryCode];

		isValid && this.setState({
			selected: countryCode
		});
	};

	ReactFlagsSelect.prototype.filterSearch = function filterSearch(evt) {
		var _this2 = this;

		var filterValue = evt.target.value;
		var filteredCountries = filterValue && this.state.countries.filter(function (key) {
			var label = _this2.props.customLabels[key] || countries[key];
			return label && label.match(new RegExp(filterValue, 'i'));
		});

		this.setState({ filter: filterValue, filteredCountries: filteredCountries });
	};

	ReactFlagsSelect.prototype.componentDidMount = function componentDidMount() {
		!this.props.disabled && window.addEventListener("click", this.closeOptions);
	};

	ReactFlagsSelect.prototype.componentWillUnmount = function componentWillUnmount() {
		!this.props.disabled && window.removeEventListener("click", this.closeOptions);
	};

	ReactFlagsSelect.prototype.render = function render() {
		var _this3 = this;

		var isSelected = this.state.selected || this.state.defaultCountry;
		var selectedSize = this.props.selectedSize;
		var optionsSize = this.props.optionsSize;
		var alignClass = this.props.alignOptions.toLowerCase() === 'left' ? 'to--left' : '';

		return React.createElement(
			'div',
			{ className: 'flag-select ' + (this.props.className ? this.props.className : "") },
			React.createElement(
				'div',
				{ ref: 'selectedFlag', style: { fontSize: selectedSize + 'px' }, className: 'selected--flag--option ' + (this.props.disabled ? 'no--focus' : ''), onClick: this.toggleOptions },
				isSelected && React.createElement(
					'span',
					{ className: 'country-flag', style: { width: selectedSize + 'px', height: selectedSize + 'px' } },
					React.createElement('img', { src: require('../flags/' + isSelected.toLowerCase() + '.svg') }),
					this.props.showSelectedLabel && React.createElement(
						'span',
						{ className: 'country-label' },
						this.props.customLabels[isSelected] || countries[isSelected]
					)
				),
				!isSelected && React.createElement(
					'span',
					{ className: 'country-label' },
					this.props.placeholder
				),
				React.createElement(
					'span',
					{ className: 'arrow-down ' + (this.props.disabled ? 'hidden' : '') },
					'\u25BE'
				)
			),
			this.state.openOptions && React.createElement(
				'div',
				{ ref: 'flagOptions', style: { fontSize: optionsSize + 'px' }, className: 'flag-options ' + alignClass },
				this.props.searchable && React.createElement(
					'div',
					{ className: 'filterBox' },
					React.createElement('input', { type: 'text', placeholder: 'Search', ref: 'filterText', onChange: this.filterSearch })
				),
				(this.state.filter ? this.state.filteredCountries : this.state.countries).map(function (countryCode) {
					return React.createElement(
						'div',
						{ className: 'flag-option ' + (_this3.props.showOptionLabel ? 'has-label' : ''), key: countryCode, onClick: function onClick() {
								return _this3.onSelect(countryCode);
							} },
						React.createElement(
							'span',
							{ className: 'country-flag', style: { width: optionsSize + 'px', height: optionsSize + 'px' } },
							React.createElement('img', { src: require('../flags/' + countryCode.toLowerCase() + '.svg') }),
							_this3.props.showOptionLabel && React.createElement(
								'span',
								{ className: 'country-label' },
								_this3.props.customLabels[countryCode] || countries[countryCode]
							)
						)
					);
				})
			)
		);
	};

	return ReactFlagsSelect;
}(Component);

ReactFlagsSelect.defaultProps = {
	selectedSize: 16,
	optionsSize: 14,
	placeholder: "Select a country",
	showSelectedLabel: true,
	showOptionLabel: true,
	alignOptions: "right",
	customLabels: [],
	disabled: false,
	blackList: false,
	searchable: false
};

ReactFlagsSelect.propsType = {
	countries: PropTypes.array,
	blackList: PropTypes.bool,
	customLabels: PropTypes.array,
	selectedSize: PropTypes.number,
	optionsSize: PropTypes.number,
	defaultCountry: PropTypes.string,
	placeholder: PropTypes.string,
	className: PropTypes.string,
	showSelectedLabel: PropTypes.bool,
	showOptionLabel: PropTypes.bool,
	alignOptions: PropTypes.string,
	onSelect: PropTypes.func,
	disabled: PropTypes.bool,
	searchable: PropTypes.bool
};

export default ReactFlagsSelect;
