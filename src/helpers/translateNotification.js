import localStorage from 'localStorage';
import { NotificationManager } from 'react-notifications';

import LocalizedStrings from 'react-localization';
import enDictionary from '../languages/en.json';
import idDictionary from '../languages/id.json';
import javaDictionary from '../languages/java.json';
import sundaDictionary from '../languages/sunda.json';

export const notifySuccess = (code) => {
    let language = localStorage.getItem('language');
    if(!language) {
        language = 'id'
    }

    

    let strings = new LocalizedStrings({
        en: enDictionary,
        id: idDictionary,
        sunda: sundaDictionary,
        java: javaDictionary
    });

    strings.setLanguage(language);
    if(strings[code.toUpperCase()]) {
        NotificationManager.success(strings[code.toUpperCase()]);
    } else {
        NotificationManager.success(code);
    }
}

export const notifyError = (code) => {
    let language = localStorage.getItem('language');
    if(!language) {
        language = 'id'
    }

    

    let strings = new LocalizedStrings({
        en: enDictionary,
        id: idDictionary,
        sunda: sundaDictionary,
        java: javaDictionary
    });

    strings.setLanguage(language);
    if(strings[code.toUpperCase()]) {
        NotificationManager.error(strings[code.toUpperCase()]);
    } else {
        NotificationManager.error(code);
    }
}
