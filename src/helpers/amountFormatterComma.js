// custom formatter to not rounding decimals while formatting by using the exact number of decimals in format string.
import numeral from 'numeral';

// numeral.register('locale', 'idc', {
//     delimiters: {
//         thousands: ',',
//         decimal: '.'
//     },
//     abbreviations: {
//         thousand: 'k',
//         million: 'm',
//         billion: 'b',
//         trillion: 't'
//     },
//     ordinal : function (number) {
//         return number === 1 ? 'er' : 'ème';
//     },
//     currency: {
//         symbol: 'Rp'
//     }
// });

export default function (amount) {
    if (amount) {
        var format = "0,0";
        // var decimalLength = 0;

        // var stringValue = amount.toString();

        // if (stringValue.split('.').length > 1) {
        //     decimalLength = stringValue.split('.')[1].length;
        // }

        // if (decimalLength > 0 && decimalLength <= 8) {
        //     format = format + ".";
        //     for (var i = 0; i < decimalLength; i++) {
        //         format = format + "0";
        //     }
        // } else if (decimalLength > 0 && decimalLength > 8) {
        //     format = format + ".";
        //     for (var i = 0; i < 8; i++) {
        //         format = format + "0";
        //     }
        // }

        if(numeral(amount).format(format) === 'NaN'){
            return 0;
        } else {
            return numeral(amount).format(format, Math.floor).replace(/\./g,',');
        }
    } else {
        return 0;
    }
}
