import React from 'react';
import amountFormatter from './amountFormatter';

describe('amountFormatter', () => {
  describe('given number less than 1000', () => {
    describe('given 1 digit', () => {
      test('given 4 return "4"', () => {
        expect(amountFormatter(4)).toBe('4');
        expect(amountFormatter(4)).not.toBeUndefined();
        expect(amountFormatter(4)).not.toBeNull();
      });
      test('given "4" return "4"', () => {
        expect(amountFormatter('4')).toBe('4');
        expect(amountFormatter('4')).not.toBeUndefined();
        expect(amountFormatter('4')).not.toBeNull();
      });
    });

    describe('given 2 digit', () => {
      test('given 13 return "13"', () => {
        expect(amountFormatter(13)).toBe('13');
        expect(amountFormatter(13)).not.toBeUndefined();
        expect(amountFormatter(13)).not.toBeNull();
      });
      test('given "13" return "13"', () => {
        expect(amountFormatter('13')).toBe('13');
        expect(amountFormatter('13')).not.toBeUndefined();
        expect(amountFormatter('13')).not.toBeNull();
      });
    });

    describe('given 3 digit', () => {
      test('given 666 return "666"', () => {
        expect(amountFormatter(666)).toBe('666');
        expect(amountFormatter(666)).not.toBeUndefined();
        expect(amountFormatter(666)).not.toBeNull();
      });
      test('given "666" return "666"', () => {
        expect(amountFormatter('666')).toBe('666');
        expect(amountFormatter('666')).not.toBeUndefined();
        expect(amountFormatter('666')).not.toBeNull();
      });
    });
  })
  
  describe('given number 1000 or more', () => {
    describe('given 4 digit', () => {
      test('given 1000 return "1.000"', () => {
        expect(amountFormatter(1000)).toBe('1.000')
        expect(amountFormatter(1000)).not.toBeUndefined();
        expect(amountFormatter(1000)).not.toBeNull();
      })
      test('given "1000" return "1.000"', () => {
        expect(amountFormatter('1000')).toBe('1.000')
        expect(amountFormatter('1000')).not.toBeUndefined();
        expect(amountFormatter('1000')).not.toBeNull();
      })
    })
  })
  
})
