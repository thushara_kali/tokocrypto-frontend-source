import accounting from './accounting/accounting';
import {Decimal} from 'decimal.js';
accounting.settings = {
	currency: {
		symbol : "Rp",   // default currency symbol is '$'
		format: "%s%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
		decimal : ",",  // decimal point separator
		thousand: ".",  // thousands separator
		precision : 2   // decimal places
	},
	number: {
		precision : 0,  // default precision on numbers is 0
		thousand: ".",
		decimal : ","
	}
}

const cryptoFormatter = (amount, offset) => {
  if (offset == undefined) {
    offset = 2;
  }
  if (amount) {
    let firstValueX = Number(amount).toFixed(offset + 1);
    let firstValue = firstValueX.substr(0, firstValueX.length);
    // console.log("firstValue", firstValue, " of ", amount);
    let newValue = firstValue;
    
    // SPLIT AND FORMAT DECIMALS AND THOUSANDS
    let [thousands, decimals] = newValue.split(".");
    // console.log('decimals', decimals)
    // GET RID OF '0' in the end of decimals
    let newDecimals;
    if (offset != 0 && decimals != undefined) {
      newDecimals = decimals.substr(0, offset);
      newDecimals = newDecimals.replace(/0+$/, "");
      // console.log("newDecimals", newDecimals);
    } else {
      newDecimals = "";
    }

    // console.log(
    //   "thousands: ", thousands, "\n",
    //   "decimals: ", newDecimals, "\n"
    // );

    // ADD thousand separator
    let newThousand = thousands.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
    // console.log("new thousand", newThousand);

    let defaultReturn = newThousand;

    // ADD decimals if exist
    if (newDecimals != undefined && newDecimals != "" ) {
      let decimalSeparator = ",";
      defaultReturn = newThousand + decimalSeparator + newDecimals;
    }

    if (
      !defaultReturn ||
      defaultReturn === 0 ||
      Number(amount).toFixed(10) === 0
    ) {
      return "0";
    } else {
      return defaultReturn;
    }
  } else {
    return "0";
  }
};

const cryptoFormaterUnformat = val => {
  return accounting.unformat(val);
};

const cryptoFormatterReplaceFormat = (val, to) => {
  let newValue = val.toString();

  if (to === "comma") {
    return newValue.replace(".", ",");
  }

  return newValue.replace(",", ".");
};

const cryptoFormatterByCoin = (amount, coinName) => {
  switch (coinName) {
    case "BTC":
      return cryptoFormatter(amount, 8);

    case "ETH":
      return cryptoFormatter(amount, 6);

    case "DGX":
      return cryptoFormatter(amount, 4);

    case "XRP":
      return cryptoFormatter(amount, 0);

    case "TTC":
      return cryptoFormatter(amount, 0);

    case "PAX":
      return cryptoFormatter(amount, 4);

    case "TUSD":
      return cryptoFormatter(amount, 4);

    case "GUSD":
      return cryptoFormatter(amount, 4);

    case "USDT":
      return cryptoFormatter(amount, 4);

    case "USDC":
      return cryptoFormatter(amount, 4);

    case "SWIPE":
      return cryptoFormatter(amount, 0);

    default:
      return cryptoFormatter(amount, 2);
      break;
  }
};

export default cryptoFormatter;
export {
  cryptoFormatterByCoin,
  cryptoFormaterUnformat,
  cryptoFormatterReplaceFormat
};
