import { cryptoFormatterByCoin as formatByCoin, cryptoFormatterReplaceFormat as replaceFormat, cryptoFormaterUnformat as unformat } from './cryptoFormatter';
import React from 'react';

describe('cryptoFormatter', () => {
  describe('Formatter By Coin with Replace Format to Indonesian Formatting', () => {
    describe('BTC | max decimals: 8', () => {
      test('given 0.00001 BTC return 0,00001 BTC', () => {
        expect(formatByCoin(0.00001, 'BTC')).toBe('0,00001');
        expect(formatByCoin(0.00001, 'BTC')).toBeDefined();
        expect(formatByCoin(0.00001, 'BTC')).not.toBeFalsy();
      })
      test('given 0.0001 BTC return 0,0001 BTC', () => {
        expect(formatByCoin(0.0001, 'BTC')).toBe('0,0001');
        expect(formatByCoin(0.0001, 'BTC')).toBeDefined();
        expect(formatByCoin(0.0001, 'BTC')).not.toBeFalsy();
      })
      test('given 0.001 BTC return 0,001 BTC', () => {
        expect(formatByCoin(0.001, 'BTC')).toBe('0,001');
        expect(formatByCoin(0.001, 'BTC')).toBeDefined();
        expect(formatByCoin(0.001, 'BTC')).not.toBeFalsy();
      })
      test('given 0.01 BTC return 0,01 BTC', () => {
        expect(formatByCoin(0.01, 'BTC')).toBe('0,01');
        expect(formatByCoin(0.01, 'BTC')).toBeDefined();
        expect(formatByCoin(0.01, 'BTC')).not.toBeFalsy();
      })
      test('given 0.1 BTC return 0,1 BTC', () => {
        expect(formatByCoin(0.1, 'BTC')).toBe('0,1');
        expect(formatByCoin(0.1, 'BTC')).toBeDefined();
        expect(formatByCoin(0.1, 'BTC')).not.toBeFalsy();
      })
      test('given 0 BTC return 0 BTC', () => {
        expect(formatByCoin(0, 'BTC')).toBe('0');
        expect(formatByCoin(0, 'BTC')).toBeDefined();
        expect(formatByCoin(0, 'BTC')).not.toBeFalsy();
      })
      test('given 1 BTC return 1 BTC', () => {
        expect(formatByCoin(1, 'BTC')).toBe('1');
        expect(formatByCoin(1, 'BTC')).toBeDefined();
        expect(formatByCoin(1, 'BTC')).not.toBeFalsy();
      })
      test('given 10 BTC return 10 BTC', () => {
        expect(formatByCoin(10, 'BTC')).toBe('10');
        expect(formatByCoin(10, 'BTC')).toBeDefined();
        expect(formatByCoin(10, 'BTC')).not.toBeFalsy();
      })
      test('given 100 BTC return 100 BTC', () => {
        expect(formatByCoin(100, 'BTC')).toBe('100');
        expect(formatByCoin(100, 'BTC')).toBeDefined();
        expect(formatByCoin(100, 'BTC')).not.toBeFalsy();
      })
      test('given 1000 BTC return 1.000 BTC', () => {
        expect(formatByCoin(1000, 'BTC')).toBe('1.000');
        expect(formatByCoin(1000, 'BTC')).toBeDefined();
        expect(formatByCoin(1000, 'BTC')).not.toBeFalsy();
      })
      test('given 10000 BTC return 10.000 BTC', () => {
        expect(formatByCoin(10000, 'BTC')).toBe('10.000');
        expect(formatByCoin(10000, 'BTC')).toBeDefined();
        expect(formatByCoin(10000, 'BTC')).not.toBeFalsy();
      })
      test('given 100000 BTC return 100.000 BTC', () => {
        expect(formatByCoin(100000, 'BTC')).toBe('100.000');
        expect(formatByCoin(100000, 'BTC')).toBeDefined();
        expect(formatByCoin(100000, 'BTC')).not.toBeFalsy();
      })
      test('given 100001.01 BTC return 100.001,01 BTC', () => {
        expect(formatByCoin(100001.01, 'BTC')).toBe('100.001,01');
        expect(formatByCoin(100001.01, 'BTC')).toBeDefined();
        expect(formatByCoin(100001.01, 'BTC')).not.toBeFalsy();
      })
      test('given 0.123456789 BTC return 0,12345678 BTC', () => {
        expect(formatByCoin(0.123456789, 'BTC')).toBe('0,12345678');
        expect(formatByCoin(0.123456789, 'BTC')).toBeDefined();
        expect(formatByCoin(0.123456789, 'BTC')).not.toBeFalsy();
      })
      test('given 2.59175086e-9 BTC return 0 BTC', () => {
        expect(formatByCoin( 2.59175086e-9, 'BTC')).toBe('0');
        expect(formatByCoin( 2.59175086e-9, 'BTC')).toBeDefined();
        expect(formatByCoin( 2.59175086e-9, 'BTC')).not.toBeFalsy();
      })
      test('given 0.123456789321 BTC, the maximal decimal length must be 8', () => {
        let amount = formatByCoin(0.123456789321, 'BTC');
        let [ num, decimals ] = amount.toString().split(',')
        expect(decimals).toHaveLength(8);
      })
    })

    describe('ETH | max decimals: 6', () => {
      test('given 0.00001 ETH return 0,00001 ETH', () => {
        expect(formatByCoin(0.00001, 'ETH')).toBe('0,00001');
        expect(formatByCoin(0.00001, 'ETH')).toBeDefined();
        expect(formatByCoin(0.00001, 'ETH')).not.toBeFalsy();
      })
      test('given 0.0001 ETH return 0,0001 ETH', () => {
        expect(formatByCoin(0.0001, 'ETH')).toBe('0,0001');
        expect(formatByCoin(0.0001, 'ETH')).toBeDefined();
        expect(formatByCoin(0.0001, 'ETH')).not.toBeFalsy();
      })
      test('given 0.001 ETH return 0,001 ETH', () => {
        expect(formatByCoin(0.001, 'ETH')).toBe('0,001');
        expect(formatByCoin(0.001, 'ETH')).toBeDefined();
        expect(formatByCoin(0.001, 'ETH')).not.toBeFalsy();
      })
      test('given 0.01 ETH return 0,01 ETH', () => {
        expect(formatByCoin(0.01, 'ETH')).toBe('0,01');
        expect(formatByCoin(0.01, 'ETH')).toBeDefined();
        expect(formatByCoin(0.01, 'ETH')).not.toBeFalsy();
      })
      test('given 0.1 ETH return 0,1 ETH', () => {
        expect(formatByCoin(0.1, 'ETH')).toBe('0,1');
        expect(formatByCoin(0.1, 'ETH')).toBeDefined();
        expect(formatByCoin(0.1, 'ETH')).not.toBeFalsy();
      })
      test('given 0 ETH return 0 ETH', () => {
        expect(formatByCoin(0, 'ETH')).toBe('0');
        expect(formatByCoin(0, 'ETH')).toBeDefined();
        expect(formatByCoin(0, 'ETH')).not.toBeFalsy();
      })
      test('given 1 ETH return 1 ETH', () => {
        expect(formatByCoin(1, 'ETH')).toBe('1');
        expect(formatByCoin(1, 'ETH')).toBeDefined();
        expect(formatByCoin(1, 'ETH')).not.toBeFalsy();
      })
      test('given 10 ETH return 10 ETH', () => {
        expect(formatByCoin(10, 'ETH')).toBe('10');
        expect(formatByCoin(10, 'ETH')).toBeDefined();
        expect(formatByCoin(10, 'ETH')).not.toBeFalsy();
      })
      test('given 100 ETH return 100 ETH', () => {
        expect(formatByCoin(100, 'ETH')).toBe('100');
        expect(formatByCoin(100, 'ETH')).toBeDefined();
        expect(formatByCoin(100, 'ETH')).not.toBeFalsy();
      })
      test('given 1000 ETH return 1.000 ETH', () => {
        expect(formatByCoin(1000, 'ETH')).toBe('1.000');
        expect(formatByCoin(1000, 'ETH')).toBeDefined();
        expect(formatByCoin(1000, 'ETH')).not.toBeFalsy();
      })
      test('given 10000 ETH return 10.000 ETH', () => {
        expect(formatByCoin(10000, 'ETH')).toBe('10.000');
        expect(formatByCoin(10000, 'ETH')).toBeDefined();
        expect(formatByCoin(10000, 'ETH')).not.toBeFalsy();
      })
      test('given 100000 ETH return 100.000 ETH', () => {
        expect(formatByCoin(100000, 'ETH')).toBe('100.000');
        expect(formatByCoin(100000, 'ETH')).toBeDefined();
        expect(formatByCoin(100000, 'ETH')).not.toBeFalsy();
      })
      test('given 100001.01 ETH return 100.001,01 ETH', () => {
        expect(formatByCoin(100001.01, 'ETH')).toBe('100.001,01');
        expect(formatByCoin(100001.01, 'ETH')).toBeDefined();
        expect(formatByCoin(100001.01, 'ETH')).not.toBeFalsy();
      })
      test('given 0.123456789 ETH return 0,123456 ETH', () => {
        expect(formatByCoin(0.123456789, 'ETH')).toBe('0,123456');
        expect(formatByCoin(0.123456789, 'ETH')).toBeDefined();
        expect(formatByCoin(0.123456789, 'ETH')).not.toBeFalsy();
      })
      test('given 0.123456789321 ETH, the maximal decimal length must be 6', () => {
        let amount = formatByCoin(0.123456789321, 'ETH');
        let [ num, decimals ] = amount.toString().split(',')
        expect(decimals).toHaveLength(6);
      })
    })
    
    describe('DGX | max decimals: 4', () => {
      test('given 0.00001 DGX return 0 DGX', () => {
        expect(formatByCoin(0.00001, 'DGX')).toBe('0');
        expect(formatByCoin(0.00001, 'DGX')).toBeDefined();
        expect(formatByCoin(0.00001, 'DGX')).not.toBeFalsy();
      })
      test('given 0.0001 DGX return 0,0001 DGX', () => {
        expect(formatByCoin(0.0001, 'DGX')).toBe('0,0001');
        expect(formatByCoin(0.0001, 'DGX')).toBeDefined();
        expect(formatByCoin(0.0001, 'DGX')).not.toBeFalsy();
      })
      test('given 0.001 DGX return 0,001 DGX', () => {
        expect(formatByCoin(0.001, 'DGX')).toBe('0,001');
        expect(formatByCoin(0.001, 'DGX')).toBeDefined();
        expect(formatByCoin(0.001, 'DGX')).not.toBeFalsy();
      })
      test('given 0.01 DGX return 0,01 DGX', () => {
        expect(formatByCoin(0.01, 'DGX')).toBe('0,01');
        expect(formatByCoin(0.01, 'DGX')).toBeDefined();
        expect(formatByCoin(0.01, 'DGX')).not.toBeFalsy();
      })
      test('given 0.1 DGX return 0,1 DGX', () => {
        expect(formatByCoin(0.1, 'DGX')).toBe('0,1');
        expect(formatByCoin(0.1, 'DGX')).toBeDefined();
        expect(formatByCoin(0.1, 'DGX')).not.toBeFalsy();
      })
      test('given 0 DGX return 0 DGX', () => {
        expect(formatByCoin(0, 'DGX')).toBe('0');
        expect(formatByCoin(0, 'DGX')).toBeDefined();
        expect(formatByCoin(0, 'DGX')).not.toBeFalsy();
      })
      test('given 1 DGX return 1 DGX', () => {
        expect(formatByCoin(1, 'DGX')).toBe('1');
        expect(formatByCoin(1, 'DGX')).toBeDefined();
        expect(formatByCoin(1, 'DGX')).not.toBeFalsy();
      })
      test('given 10 DGX return 10 DGX', () => {
        expect(formatByCoin(10, 'DGX')).toBe('10');
        expect(formatByCoin(10, 'DGX')).toBeDefined();
        expect(formatByCoin(10, 'DGX')).not.toBeFalsy();
      })
      test('given 100 DGX return 100 DGX', () => {
        expect(formatByCoin(100, 'DGX')).toBe('100');
        expect(formatByCoin(100, 'DGX')).toBeDefined();
        expect(formatByCoin(100, 'DGX')).not.toBeFalsy();
      })
      test('given 1000 DGX return 1.000 DGX', () => {
        expect(formatByCoin(1000, 'DGX')).toBe('1.000');
        expect(formatByCoin(1000, 'DGX')).toBeDefined();
        expect(formatByCoin(1000, 'DGX')).not.toBeFalsy();
      })
      test('given 10000 DGX return 10.000 DGX', () => {
        expect(formatByCoin(10000, 'DGX')).toBe('10.000');
        expect(formatByCoin(10000, 'DGX')).toBeDefined();
        expect(formatByCoin(10000, 'DGX')).not.toBeFalsy();
      })
      test('given 100000 DGX return 100.000 DGX', () => {
        expect(formatByCoin(100000, 'DGX')).toBe('100.000');
        expect(formatByCoin(100000, 'DGX')).toBeDefined();
        expect(formatByCoin(100000, 'DGX')).not.toBeFalsy();
      })
      test('given 100001.01 DGX return 100.001,01 DGX', () => {
        expect(formatByCoin(100001.01, 'DGX')).toBe('100.001,01');
        expect(formatByCoin(100001.01, 'DGX')).toBeDefined();
        expect(formatByCoin(100001.01, 'DGX')).not.toBeFalsy();
      })
      test('given 0.123456789 DGX return 0,1234 DGX', () => {
        expect(formatByCoin(0.123456789, 'DGX')).toBe('0,1234');
        expect(formatByCoin(0.123456789, 'DGX')).toBeDefined();
        expect(formatByCoin(0.123456789, 'DGX')).not.toBeFalsy();
      })
      test('given 0.123456789321 DGX, the maximal decimal length must be 4', () => {
        let amount = formatByCoin(0.123456789321, 'DGX');
        let [ num, decimals ] = amount.toString().split(',')
        expect(decimals).toHaveLength(4);
      })
    })

    describe('XRP | max decimals: 0', () => {
      test('given 0.00001 XRP return 0 XRP', () => {
        expect(formatByCoin(0.00001, 'XRP')).toBe('0');
        expect(formatByCoin(0.00001, 'XRP')).toBeDefined();
        expect(formatByCoin(0.00001, 'XRP')).not.toBeFalsy();
      })
      test('given 0.0001 XRP return 0 XRP', () => {
        expect(formatByCoin(0.0001, 'XRP')).toBe('0');
        expect(formatByCoin(0.0001, 'XRP')).toBeDefined();
        expect(formatByCoin(0.0001, 'XRP')).not.toBeFalsy();
      })
      test('given 0.001 XRP return 0 XRP', () => {
        expect(formatByCoin(0.001, 'XRP')).toBe('0');
        expect(formatByCoin(0.001, 'XRP')).toBeDefined();
        expect(formatByCoin(0.001, 'XRP')).not.toBeFalsy();
      })
      test('given 0.01 XRP return 0 XRP', () => {
        expect(formatByCoin(0.01, 'XRP')).toBe('0');
        expect(formatByCoin(0.01, 'XRP')).toBeDefined();
        expect(formatByCoin(0.01, 'XRP')).not.toBeFalsy();
      })
      test('given 0.1 XRP return 0 XRP', () => {
        expect(formatByCoin(0.1, 'XRP')).toBe('0');
        expect(formatByCoin(0.1, 'XRP')).toBeDefined();
        expect(formatByCoin(0.1, 'XRP')).not.toBeFalsy();
      })
      test('given 0 XRP return 0 XRP', () => {
        expect(formatByCoin(0, 'XRP')).toBe('0');
        expect(formatByCoin(0, 'XRP')).toBeDefined();
        expect(formatByCoin(0, 'XRP')).not.toBeFalsy();
      })
      test('given 1 XRP return 1 XRP', () => {
        expect(formatByCoin(1, 'XRP')).toBe('1');
        expect(formatByCoin(1, 'XRP')).toBeDefined();
        expect(formatByCoin(1, 'XRP')).not.toBeFalsy();
      })
      test('given 10 XRP return 10 XRP', () => {
        expect(formatByCoin(10, 'XRP')).toBe('10');
        expect(formatByCoin(10, 'XRP')).toBeDefined();
        expect(formatByCoin(10, 'XRP')).not.toBeFalsy();
      })
      test('given 100 XRP return 100 XRP', () => {
        expect(formatByCoin(100, 'XRP')).toBe('100');
        expect(formatByCoin(100, 'XRP')).toBeDefined();
        expect(formatByCoin(100, 'XRP')).not.toBeFalsy();
      })
      test('given 1000 XRP return 1.000 XRP', () => {
        expect(formatByCoin(1000, 'XRP')).toBe('1.000');
        expect(formatByCoin(1000, 'XRP')).toBeDefined();
        expect(formatByCoin(1000, 'XRP')).not.toBeFalsy();
      })
      test('given 10000 XRP return 10.000 XRP', () => {
        expect(formatByCoin(10000, 'XRP')).toBe('10.000');
        expect(formatByCoin(10000, 'XRP')).toBeDefined();
        expect(formatByCoin(10000, 'XRP')).not.toBeFalsy();
      })
      test('given 100000 XRP return 100.000 XRP', () => {
        expect(formatByCoin(100000, 'XRP')).toBe('100.000');
        expect(formatByCoin(100000, 'XRP')).toBeDefined();
        expect(formatByCoin(100000, 'XRP')).not.toBeFalsy();
      })
      test('given 100001.01 XRP return 100.001 XRP', () => {
        expect(formatByCoin(100001.01, 'XRP')).toBe('100.001');
        expect(formatByCoin(100001.01, 'XRP')).toBeDefined();
        expect(formatByCoin(100001.01, 'XRP')).not.toBeFalsy();
      })
      test('given 0.123456789 XRP return 0 XRP', () => {
        expect(formatByCoin(0.123456789, 'XRP')).toBe('0');
        expect(formatByCoin(0.123456789, 'XRP')).toBeDefined();
        expect(formatByCoin(0.123456789, 'XRP')).not.toBeFalsy();
      })
      test('given 0.123456789321 XRP, the maximal decimal length must be 0 or undefined', () => {
        let amount = formatByCoin(0.123456789321, 'XRP');
        let [ num, decimals ] = amount.toString().split(',')

        // Since there's no decimals, it must be return undefined
        expect(decimals).toBeFalsy();
      })
    })

    describe('TTC | max decimals: 0', () => {
      test('given 0.00001 TTC return 0 TTC', () => {
        expect(formatByCoin(0.00001, 'TTC')).toBe('0');
        expect(formatByCoin(0.00001, 'TTC')).toBeDefined();
        expect(formatByCoin(0.00001, 'TTC')).not.toBeFalsy();
      })
      test('given 0.0001 TTC return 0 TTC', () => {
        expect(formatByCoin(0.0001, 'TTC')).toBe('0');
        expect(formatByCoin(0.0001, 'TTC')).toBeDefined();
        expect(formatByCoin(0.0001, 'TTC')).not.toBeFalsy();
      })
      test('given 0.001 TTC return 0 TTC', () => {
        expect(formatByCoin(0.001, 'TTC')).toBe('0');
        expect(formatByCoin(0.001, 'TTC')).toBeDefined();
        expect(formatByCoin(0.001, 'TTC')).not.toBeFalsy();
      })
      test('given 0.01 TTC return 0 TTC', () => {
        expect(formatByCoin(0.01, 'TTC')).toBe('0');
        expect(formatByCoin(0.01, 'TTC')).toBeDefined();
        expect(formatByCoin(0.01, 'TTC')).not.toBeFalsy();
      })
      test('given 0.1 TTC return 0 TTC', () => {
        expect(formatByCoin(0.1, 'TTC')).toBe('0');
        expect(formatByCoin(0.1, 'TTC')).toBeDefined();
        expect(formatByCoin(0.1, 'TTC')).not.toBeFalsy();
      })
      test('given 0 TTC return 0 TTC', () => {
        expect(formatByCoin(0, 'TTC')).toBe('0');
        expect(formatByCoin(0, 'TTC')).toBeDefined();
        expect(formatByCoin(0, 'TTC')).not.toBeFalsy();
      })
      test('given 1 TTC return 1 TTC', () => {
        expect(formatByCoin(1, 'TTC')).toBe('1');
        expect(formatByCoin(1, 'TTC')).toBeDefined();
        expect(formatByCoin(1, 'TTC')).not.toBeFalsy();
      })
      test('given 10 TTC return 10 TTC', () => {
        expect(formatByCoin(10, 'TTC')).toBe('10');
        expect(formatByCoin(10, 'TTC')).toBeDefined();
        expect(formatByCoin(10, 'TTC')).not.toBeFalsy();
      })
      test('given 100 TTC return 100 TTC', () => {
        expect(formatByCoin(100, 'TTC')).toBe('100');
        expect(formatByCoin(100, 'TTC')).toBeDefined();
        expect(formatByCoin(100, 'TTC')).not.toBeFalsy();
      })
      test('given 1000 TTC return 1.000 TTC', () => {
        expect(formatByCoin(1000, 'TTC')).toBe('1.000');
        expect(formatByCoin(1000, 'TTC')).toBeDefined();
        expect(formatByCoin(1000, 'TTC')).not.toBeFalsy();
      })
      test('given 10000 TTC return 10.000 TTC', () => {
        expect(formatByCoin(10000, 'TTC')).toBe('10.000');
        expect(formatByCoin(10000, 'TTC')).toBeDefined();
        expect(formatByCoin(10000, 'TTC')).not.toBeFalsy();
      })
      test('given 100000 TTC return 100.000 TTC', () => {
        expect(formatByCoin(100000, 'TTC')).toBe('100.000');
        expect(formatByCoin(100000, 'TTC')).toBeDefined();
        expect(formatByCoin(100000, 'TTC')).not.toBeFalsy();
      })
      test('given 100001.01 TTC return 100.001 TTC', () => {
        expect(formatByCoin(100001.01, 'TTC')).toBe('100.001');
        expect(formatByCoin(100001.01, 'TTC')).toBeDefined();
        expect(formatByCoin(100001.01, 'TTC')).not.toBeFalsy();
      })
      test('given 0.123456789 TTC return 0 TTC', () => {
        expect(formatByCoin(0.123456789, 'TTC')).toBe('0');
        expect(formatByCoin(0.123456789, 'TTC')).toBeDefined();
        expect(formatByCoin(0.123456789, 'TTC')).not.toBeFalsy();
      })
      test('given 0.123456789321 TTC, the maximal decimal length must be 0 or undefined', () => {
        let amount = formatByCoin(0.123456789321, 'TTC');
        let [ num, decimals ] = amount.toString().split(',')

        // Since there's no decimals, it must be return undefined
        expect(decimals).toBeFalsy();
      })
    })

    describe('TUSD | max decimals: 4', () => {
      test('given 0.00001 TUSD return 0 TUSD', () => {
        expect(formatByCoin(0.00001, 'TUSD')).toBe('0');
        expect(formatByCoin(0.00001, 'TUSD')).toBeDefined();
        expect(formatByCoin(0.00001, 'TUSD')).not.toBeFalsy();
      })
      test('given 0.0001 TUSD return 0,0001 TUSD', () => {
        expect(formatByCoin(0.0001, 'TUSD')).toBe('0,0001');
        expect(formatByCoin(0.0001, 'TUSD')).toBeDefined();
        expect(formatByCoin(0.0001, 'TUSD')).not.toBeFalsy();
      })
      test('given 0.001 TUSD return 0,001 TUSD', () => {
        expect(formatByCoin(0.001, 'TUSD')).toBe('0,001');
        expect(formatByCoin(0.001, 'TUSD')).toBeDefined();
        expect(formatByCoin(0.001, 'TUSD')).not.toBeFalsy();
      })
      test('given 0.01 TUSD return 0,01 TUSD', () => {
        expect(formatByCoin(0.01, 'TUSD')).toBe('0,01');
        expect(formatByCoin(0.01, 'TUSD')).toBeDefined();
        expect(formatByCoin(0.01, 'TUSD')).not.toBeFalsy();
      })
      test('given 0.1 TUSD return 0,1 TUSD', () => {
        expect(formatByCoin(0.1, 'TUSD')).toBe('0,1');
        expect(formatByCoin(0.1, 'TUSD')).toBeDefined();
        expect(formatByCoin(0.1, 'TUSD')).not.toBeFalsy();
      })
      test('given 0 TUSD return 0 TUSD', () => {
        expect(formatByCoin(0, 'TUSD')).toBe('0');
        expect(formatByCoin(0, 'TUSD')).toBeDefined();
        expect(formatByCoin(0, 'TUSD')).not.toBeFalsy();
      })
      test('given 1 TUSD return 1 TUSD', () => {
        expect(formatByCoin(1, 'TUSD')).toBe('1');
        expect(formatByCoin(1, 'TUSD')).toBeDefined();
        expect(formatByCoin(1, 'TUSD')).not.toBeFalsy();
      })
      test('given 10 TUSD return 10 TUSD', () => {
        expect(formatByCoin(10, 'TUSD')).toBe('10');
        expect(formatByCoin(10, 'TUSD')).toBeDefined();
        expect(formatByCoin(10, 'TUSD')).not.toBeFalsy();
      })
      test('given 100 TUSD return 100 TUSD', () => {
        expect(formatByCoin(100, 'TUSD')).toBe('100');
        expect(formatByCoin(100, 'TUSD')).toBeDefined();
        expect(formatByCoin(100, 'TUSD')).not.toBeFalsy();
      })
      test('given 1000 TUSD return 1.000 TUSD', () => {
        expect(formatByCoin(1000, 'TUSD')).toBe('1.000');
        expect(formatByCoin(1000, 'TUSD')).toBeDefined();
        expect(formatByCoin(1000, 'TUSD')).not.toBeFalsy();
      })
      test('given 10000 TUSD return 10.000 TUSD', () => {
        expect(formatByCoin(10000, 'TUSD')).toBe('10.000');
        expect(formatByCoin(10000, 'TUSD')).toBeDefined();
        expect(formatByCoin(10000, 'TUSD')).not.toBeFalsy();
      })
      test('given 100000 TUSD return 100.000 TUSD', () => {
        expect(formatByCoin(100000, 'TUSD')).toBe('100.000');
        expect(formatByCoin(100000, 'TUSD')).toBeDefined();
        expect(formatByCoin(100000, 'TUSD')).not.toBeFalsy();
      })
      test('given 100001.01 TUSD return 100.001,01 TUSD', () => {
        expect(formatByCoin(100001.01, 'TUSD')).toBe('100.001,01');
        expect(formatByCoin(100001.01, 'TUSD')).toBeDefined();
        expect(formatByCoin(100001.01, 'TUSD')).not.toBeFalsy();
      })
      test('given 0.123456789 TUSD return 0,1234 TUSD', () => {
        expect(formatByCoin(0.123456789, 'TUSD')).toBe('0,1234');
        expect(formatByCoin(0.123456789, 'TUSD')).toBeDefined();
        expect(formatByCoin(0.123456789, 'TUSD')).not.toBeFalsy();
      })
      test('given 0.123456789321 TUSD, the maximal decimal length must be 4', () => {
        let amount = formatByCoin(0.123456789321, 'TUSD');
        let [ num, decimals ] = amount.toString().split(',')
        expect(decimals).toHaveLength(4);
      })
    })

    describe('USDT | max decimals: 4', () => {
      test('given 0.00001 USDT return 0 USDT', () => {
        expect(formatByCoin(0.00001, 'USDT')).toBe('0');
        expect(formatByCoin(0.00001, 'USDT')).toBeDefined();
        expect(formatByCoin(0.00001, 'USDT')).not.toBeFalsy();
      })
      test('given 0.0001 USDT return 0,0001 USDT', () => {
        expect(formatByCoin(0.0001, 'USDT')).toBe('0,0001');
        expect(formatByCoin(0.0001, 'USDT')).toBeDefined();
        expect(formatByCoin(0.0001, 'USDT')).not.toBeFalsy();
      })
      test('given 0.001 USDT return 0,001 USDT', () => {
        expect(formatByCoin(0.001, 'USDT')).toBe('0,001');
        expect(formatByCoin(0.001, 'USDT')).toBeDefined();
        expect(formatByCoin(0.001, 'USDT')).not.toBeFalsy();
      })
      test('given 0.01 USDT return 0,01 USDT', () => {
        expect(formatByCoin(0.01, 'USDT')).toBe('0,01');
        expect(formatByCoin(0.01, 'USDT')).toBeDefined();
        expect(formatByCoin(0.01, 'USDT')).not.toBeFalsy();
      })
      test('given 0.1 USDT return 0,1 USDT', () => {
        expect(formatByCoin(0.1, 'USDT')).toBe('0,1');
        expect(formatByCoin(0.1, 'USDT')).toBeDefined();
        expect(formatByCoin(0.1, 'USDT')).not.toBeFalsy();
      })
      test('given 0 USDT return 0 USDT', () => {
        expect(formatByCoin(0, 'USDT')).toBe('0');
        expect(formatByCoin(0, 'USDT')).toBeDefined();
        expect(formatByCoin(0, 'USDT')).not.toBeFalsy();
      })
      test('given 1 USDT return 1 USDT', () => {
        expect(formatByCoin(1, 'USDT')).toBe('1');
        expect(formatByCoin(1, 'USDT')).toBeDefined();
        expect(formatByCoin(1, 'USDT')).not.toBeFalsy();
      })
      test('given 10 USDT return 10 USDT', () => {
        expect(formatByCoin(10, 'USDT')).toBe('10');
        expect(formatByCoin(10, 'USDT')).toBeDefined();
        expect(formatByCoin(10, 'USDT')).not.toBeFalsy();
      })
      test('given 100 USDT return 100 USDT', () => {
        expect(formatByCoin(100, 'USDT')).toBe('100');
        expect(formatByCoin(100, 'USDT')).toBeDefined();
        expect(formatByCoin(100, 'USDT')).not.toBeFalsy();
      })
      test('given 1000 USDT return 1.000 USDT', () => {
        expect(formatByCoin(1000, 'USDT')).toBe('1.000');
        expect(formatByCoin(1000, 'USDT')).toBeDefined();
        expect(formatByCoin(1000, 'USDT')).not.toBeFalsy();
      })
      test('given 10000 USDT return 10.000 USDT', () => {
        expect(formatByCoin(10000, 'USDT')).toBe('10.000');
        expect(formatByCoin(10000, 'USDT')).toBeDefined();
        expect(formatByCoin(10000, 'USDT')).not.toBeFalsy();
      })
      test('given 100000 USDT return 100.000 USDT', () => {
        expect(formatByCoin(100000, 'USDT')).toBe('100.000');
        expect(formatByCoin(100000, 'USDT')).toBeDefined();
        expect(formatByCoin(100000, 'USDT')).not.toBeFalsy();
      })
      test('given 100001.01 USDT return 100.001,01 USDT', () => {
        expect(formatByCoin(100001.01, 'USDT')).toBe('100.001,01');
        expect(formatByCoin(100001.01, 'USDT')).toBeDefined();
        expect(formatByCoin(100001.01, 'USDT')).not.toBeFalsy();
      })
      test('given 0.123456789 USDT return 0,1234 USDT', () => {
        expect(formatByCoin(0.123456789, 'USDT')).toBe('0,1234');
        expect(formatByCoin(0.123456789, 'USDT')).toBeDefined();
        expect(formatByCoin(0.123456789, 'USDT')).not.toBeFalsy();
      })
      test('given 0.123456789321 USDT, the maximal decimal length must be 4', () => {
        let amount = formatByCoin(0.123456789321, 'USDT');
        let [ num, decimals ] = amount.toString().split(',')
        expect(decimals).toHaveLength(4);
      })
    })

    describe('USDC | max decimals: 4', () => {
      test('given 0.00001 USDC return 0 USDC', () => {
        expect(formatByCoin(0.00001, 'USDC')).toBe('0');
        expect(formatByCoin(0.00001, 'USDC')).toBeDefined();
        expect(formatByCoin(0.00001, 'USDC')).not.toBeFalsy();
      })
      test('given 0.0001 USDC return 0,0001 USDC', () => {
        expect(formatByCoin(0.0001, 'USDC')).toBe('0,0001');
        expect(formatByCoin(0.0001, 'USDC')).toBeDefined();
        expect(formatByCoin(0.0001, 'USDC')).not.toBeFalsy();
      })
      test('given 0.001 USDC return 0,001 USDC', () => {
        expect(formatByCoin(0.001, 'USDC')).toBe('0,001');
        expect(formatByCoin(0.001, 'USDC')).toBeDefined();
        expect(formatByCoin(0.001, 'USDC')).not.toBeFalsy();
      })
      test('given 0.01 USDC return 0,01 USDC', () => {
        expect(formatByCoin(0.01, 'USDC')).toBe('0,01');
        expect(formatByCoin(0.01, 'USDC')).toBeDefined();
        expect(formatByCoin(0.01, 'USDC')).not.toBeFalsy();
      })
      test('given 0.1 USDC return 0,1 USDC', () => {
        expect(formatByCoin(0.1, 'USDC')).toBe('0,1');
        expect(formatByCoin(0.1, 'USDC')).toBeDefined();
        expect(formatByCoin(0.1, 'USDC')).not.toBeFalsy();
      })
      test('given 0 USDC return 0 USDC', () => {
        expect(formatByCoin(0, 'USDC')).toBe('0');
        expect(formatByCoin(0, 'USDC')).toBeDefined();
        expect(formatByCoin(0, 'USDC')).not.toBeFalsy();
      })
      test('given 1 USDC return 1 USDC', () => {
        expect(formatByCoin(1, 'USDC')).toBe('1');
        expect(formatByCoin(1, 'USDC')).toBeDefined();
        expect(formatByCoin(1, 'USDC')).not.toBeFalsy();
      })
      test('given 10 USDC return 10 USDC', () => {
        expect(formatByCoin(10, 'USDC')).toBe('10');
        expect(formatByCoin(10, 'USDC')).toBeDefined();
        expect(formatByCoin(10, 'USDC')).not.toBeFalsy();
      })
      test('given 100 USDC return 100 USDC', () => {
        expect(formatByCoin(100, 'USDC')).toBe('100');
        expect(formatByCoin(100, 'USDC')).toBeDefined();
        expect(formatByCoin(100, 'USDC')).not.toBeFalsy();
      })
      test('given 1000 USDC return 1.000 USDC', () => {
        expect(formatByCoin(1000, 'USDC')).toBe('1.000');
        expect(formatByCoin(1000, 'USDC')).toBeDefined();
        expect(formatByCoin(1000, 'USDC')).not.toBeFalsy();
      })
      test('given 10000 USDC return 10.000 USDC', () => {
        expect(formatByCoin(10000, 'USDC')).toBe('10.000');
        expect(formatByCoin(10000, 'USDC')).toBeDefined();
        expect(formatByCoin(10000, 'USDC')).not.toBeFalsy();
      })
      test('given 100000 USDC return 100.000 USDC', () => {
        expect(formatByCoin(100000, 'USDC')).toBe('100.000');
        expect(formatByCoin(100000, 'USDC')).toBeDefined();
        expect(formatByCoin(100000, 'USDC')).not.toBeFalsy();
      })
      test('given 100001.01 USDC return 100.001,01 USDC', () => {
        expect(formatByCoin(100001.01, 'USDC')).toBe('100.001,01');
        expect(formatByCoin(100001.01, 'USDC')).toBeDefined();
        expect(formatByCoin(100001.01, 'USDC')).not.toBeFalsy();
      })
      test('given 0.123456789 USDC return 0,1234 USDC', () => {
        expect(formatByCoin(0.123456789, 'USDC')).toBe('0,1234');
        expect(formatByCoin(0.123456789, 'USDC')).toBeDefined();
        expect(formatByCoin(0.123456789, 'USDC')).not.toBeFalsy();
      })
      test('given 0.123456789321 USDC, the maximal decimal length must be 4', () => {
        let amount = formatByCoin(0.123456789321, 'USDC');
        let [ num, decimals ] = amount.toString().split(',')
        expect(decimals).toHaveLength(4);
      })
    })

  })

  describe('Replace Format', () => {
    test('given 0.01 should return 0,01', () => {
      expect(replaceFormat('0.01', 'comma')).toBe('0,01');
    })
    test('given 0,01 should return 0.01', () => {
      expect(replaceFormat('0,01')).toBe('0.01');
    })
  })

  describe('Unformat', () => {
    test('given 0,0123 should return 0.0123', () => {
      expect(unformat('0,0123')).toBe(0.0123);
    })
    test('given 100.000,0123 should return 100.000,0123', () => {
      expect(unformat('100.000,0123')).toBe(100000.0123);
    })
  })

  describe('Issue on Prod', () => {
    test('given 0.0006 BTC should return 0,0006 BTC', () => {
      expect(formatByCoin(0.0006, 'BTC')).toBe('0,0006');
    })
  })
})



