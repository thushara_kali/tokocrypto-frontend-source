const parse = (queryParams) => {
    if (queryParams.length > 0) {
        const obj = Object.assign({});
        const queryParameter = queryParams.replace('#', '');
        const queryArr = queryParameter.split('&');
        queryArr.forEach((q) => {
            const parsed = q.split('=');
            obj[parsed[0]] = parsed[1];
        });
        return obj;
    }
    return {};
};

export default {
    parse
};
