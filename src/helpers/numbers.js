import math from 'mathjs';

export const toFixedNoRounding = (n, p) => {
  let result = n.toFixed(p);
  return result <= n ? result : (result - Math.pow(0.1, p)).toFixed(p);
}

export const formatNoExponent = (number) => {
    return math.format(number, {notation:"auto", exponential:{lower:0, upper:Infinity}});
}