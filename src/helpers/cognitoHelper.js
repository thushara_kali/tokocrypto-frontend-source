import localStorage from 'localStorage';
import jwtDecode from 'jwt-decode';
import AWS from 'aws-sdk';
import { CognitoUserPool, CognitoUser } from 'amazon-cognito-identity-js';

import devEnv from '../config/cognito-dev.json';
import demoEnv from '../config/cognito-demo.json';
import prodEnv from '../config/cognito-production.json';

const CognitoUserSession = require('amazon-cognito-identity-js').CognitoUserSession;

const cognitoEnv = () => {
    const env = process.env.REACT_APP_ENV;
    switch(env) {
        case 'local':
        case 'dev':
            return devEnv;
        case 'demo':
            return demoEnv;
        case 'production':
            return prodEnv;
        default:
            return devEnv;            
    }
}

const convertTokenIntoCognitoObject = () => {
    AWS.config.region = cognitoEnv().region;
    let cacheToken = localStorage.getItem('CACHE_TOKEN');
    let cacheAccessToken = localStorage.getItem('CACHE_ACCESS_TOKEN');
    let decoded = jwtDecode(cacheToken);

    // const config = {
    //     UserPoolId: 'us-east-1_aQsndC09Q',
    //     ClientId: '3dig65alnu6ptubbmnfeeqccbj',
    //     AppWebDomain: 'localhost:3000',
    //     TokenScopesArray: ['openid', 'profile', 'email'],
    //     RedirectUriSignIn: 'https://localhost:3000/checktoken',
    //     RedirectUriSignOut: 'https://localhost:3000'
    // }

    const poolData = {
        UserPoolId: cognitoEnv().userPool,
        ClientId: cognitoEnv().clientId
    };

    const userPool = new CognitoUserPool(poolData);
    const userData = {
        Username: decoded['cognito:username'],
        Pool: userPool
    };

    let _cognitoUser = new CognitoUser(userData);
    const sessionData = {
        IdToken: cacheToken,
        AccessToken: cacheAccessToken,
        RefreshToken: ''
    };
    
    let clientId = poolData.ClientId
    
    _cognitoUser.email = decoded['email'];
    _cognitoUser.storage['CognitoIdentityServiceProvider.' + clientId + '.' + decoded['cognito:username'] + '.idToken'] = cacheToken;

    let sessionObject = new CognitoUserSession(sessionData);
    _cognitoUser.getSession = () => {
        return (
            null,
            sessionObject
        )
    }

    return _cognitoUser;

    // this.props.authenticatedManual(_cognitoUser);
}

export default {
    convertTokenIntoCognitoObject,
    cognitoEnv
}