const rpFormatter = ( amount ) => {
  if ( amount === '' ) {
    return `Rp${amount}`
  } else {
    return `Rp${amount}`
  }
  
}

const idrFormatter = ( amount ) => {
  if ( amount === '' ) {
    return `${amount}IDR`
  } else {
    return `${amount} IDR`
  }
}

const activeFormatter = rpFormatter;

export default activeFormatter;
export { rpFormatter, idrFormatter };