import numeral from 'numeral';

numeral.register('locale', 'idr', {
    delimiters: {
        thousands: '.',
        million: '.',
        billion: '.',
        decimal: '.'
    },
    abbreviations: {
        thousand: 'K',
        million: 'M',
        billion: 'B',
        trillion: 'T'
    },
    ordinal : function (number) {
        return number === 1 ? 'er' : 'ème';
    },
    currency: {
        symbol: 'IDR'
    }
});

export default function (amount, type='default', order = 'down') {
    numeral.locale('idr');
    if (amount) {
        var format = "0,0";
        var thousand = '0,0 a(k) ';
        var million = '0,0.0 a(m) ';
        var thousandDecimal2 = '0,0.0 a(k) ';
        if(numeral(amount).format(format) === 'NaN'){
            return 0;
        } else {
            if(order === 'up'){
                if(type === 'default') {
                    return numeral(amount).format(format, Math.ceil);
                }
                if(type === 'thousand') {
                    if(amount > 999) {
                        return numeral(amount).format(thousand, Math.ceil);
                    } else {
                        return numeral(amount).format(format, Math.ceil);
                    }
                }
    
                if(type === 'million') {
                    return numeral(amount).format(million, Math.ceil);
                }
                
                if(type === 'thousandDecimal2') {
                    return numeral(amount).format(thousandDecimal2, Math.ceil);
                }
            }else if(order === 'down'){
                if(type === 'default') {
                    return numeral(amount).format(format, Math.floor);
                }
                if(type === 'thousand') {
                    if(amount > 999) {
                        return numeral(amount).format(thousand, Math.floor);
                    } else {
                        return numeral(amount).format(format, Math.floor);
                    }
                }
    
                if(type === 'million') {
                    return numeral(amount).format(million, Math.floor);
                }
                
                if(type === 'thousandDecimal2') {
                    return numeral(amount).format(thousandDecimal2, Math.floor);
                }
            }
        }
    } else {
        return 0;
    }
}
