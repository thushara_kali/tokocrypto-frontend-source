import React, { Component } from 'react';
import history from '../../history';
import numeral from 'numeral';
import actions from '../../core/actions/client/withdraw';
import actionsOTP from '../../core/actions/client/otp';
import { connect } from 'react-redux';
import { NotificationManager } from 'react-notifications';
import { Loader } from 'react-loaders'
import NetworkError from '../../errors/networkError';
import OTPModal from '../../components/OTPModal';

class WithdrawConfirm extends Component {

    constructor() {
        super()
        this.state = {
            amount: 0,
            accountName: "",
            Label: "",
            bank: "",
            bankCountry: "",
            beneficiaryName: "",
            showOTP: false,
            OTPEnable: true
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.value
            this.setState(obj)
        }
    }

    componentDidMount() {
        actionsOTP.checkOTP(this.getToken()).then(res => {
            this.setState({
                OTPEnable: (res.data.data.Withdraw === 'Enable')
            });
        }).catch(err => {
            NetworkError(err);
            // NotificationManager.error('Failed get 2fa config.');
        });

        this.setState({
            accountName: history.location.state.account.Label,
            bankAccountId: history.location.state.account.BankAccountId,
            accountNumber: history.location.state.account.AccountNumber,
            bank: history.location.state.account.BankName,
            beneficiaryName: history.location.state.account.BeneficiaryName
        })
    }

    toggleOTP() {
        this.setState({
            showOTP: !this.state.showOTP
        });
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    withdraw() {

        let amounts = 0;

        if (typeof (this.state.amount) == 'string') {
            amounts = parseFloat(this.state.amount.replace(/,/g, ''))
        } else {
            amounts = this.state.amount;
        }

        let data = {
            bankAccountId: this.state.bankAccountId,
            amount: amounts
        }

        if (amounts < history.location.state.balance && amounts !== 0) {

            if (this.state.OTPEnable) {
                this.toggleOTP();
            } else {
                this.doWithdraw(null, null);
            }

        } else {
            NotificationManager.warning('You don\'t have enough amounts or cannot be 0');
        }
    }

    doWithdraw(otpId, token) {
        let amounts = 0;

        if (typeof (this.state.amount) == 'string') {
            amounts = parseFloat(this.state.amount.replace(/,/g, ''))
        } else {
            amounts = this.state.amount;
        }

        let data = {};

        if (otpId !== null && token !== null) {
            data = {
                otp: {
                    otpId: otpId,
                    token: Number(token)
                },
                withdraw: {
                    bankAccountId: this.state.bankAccountId,
                    amount: amounts
                }

            }
        } else {
            data = {
                withdraw: {
                    bankAccountId: this.state.bankAccountId,
                    amount: amounts
                }
            }
        }



        this.setState({
            loading: true
        });
        actions.withdraw(this.getToken(), data).then(res => {
            this.setState({
                loading: false
            });
            history.push('/');
            NotificationManager.success('Withdrawal transactions success.');
        }).catch(err => {

            this.setState({
                loading: false
            });
            NetworkError(err);
        })
    }

    completeVerification(otpId, token) {
        this.toggleOTP();
        this.doWithdraw(otpId, token);
    }

    render() {
        return (
            <div className="container">
                <OTPModal show={this.state.showOTP} toggleShow={this.toggleOTP.bind(this)} completeVerification={this.completeVerification.bind(this)} />
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <h4 className="center-text">Withdraw to Bank Account</h4>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-4"></div>
                    <div className="col-md-4 col-12">
                        <div className="row">
                            <div className="col-md-6 col-6 center-text">
                                Label
                            </div>
                            <div className="col-md-6 col-6 bold">
                                {this.state.accountName}
                            </div>
                        </div>
                        <hr />
                        <div className="row">
                            <div className="col-md-6 col-6 center-text">
                                Bank
                            </div>
                            <div className="col-md-6 col-6 bold">
                                {this.state.bank}
                            </div>
                        </div>
                        <hr />
                        <div className="row">
                            <div className="col-md-6 col-6 center-text">
                                Account
                            </div>
                            <div className="col-md-6 col-6 bold">
                                {this.state.accountNumber}
                            </div>
                        </div>
                        <hr />
                        <div className="row">
                            <div className="col-md-6 col-6 center-text">
                                Beneficiary Name
                            </div>
                            <div className="col-md-6 col-6 bold">
                                {this.state.beneficiaryName}
                            </div>
                        </div>
                        <hr />
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <h4 className="center-text">Withdrawal Fees</h4>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-2"></div>
                    <div className="col-12 col-md-4">
                        <h5 className="bold">Local Inter Bank Transfer</h5>
                        <ul>
                            <li>Only for Singapore and Malaysia Bank Only</li>
                            <li>Withdrawal Fee is S$2 / RM 6</li>
                            <li>Fee will be deducted from withdraw amount</li>
                        </ul>
                    </div>
                    <div className="col-12 col-md-4">
                        <h5 className="bold">Telegraphic Transfer</h5>
                        <ul>
                            <li>for non Singapore and Malaysia Bank Only</li>
                            <li>Withdrawal Fee is 0.1% of amount this excludes bank charges.</li>
                            <li>Minimum amount is SG$100</li>
                        </ul>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-4"></div>
                    <div className="col-md-4 col-12">
                        <div className="input-group">
                            <span className="input-group-addon" id="basic-addon1">IDR</span>
                            <input value={this.state.amount} onChange={this.handleChange('amount')} type="text" className="form-control" />
                        </div>
                        <br />
                        <p className="center-text">You have <span className="color-primary">IDR {numeral(history.location.state.balance).format('0,0')}</span></p>
                        <br />
                        {this.state.loading ? <Loader type="line-scale" active={true} className="col-12 text-center" /> :
                            <button onClick={() => this.withdraw()} className="btn btn-primary btn-block">Withdraw</button>}
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user
    };
};

export default connect(mapStateToProps, null)(WithdrawConfirm)
