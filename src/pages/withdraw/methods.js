import React, { Component } from 'react'
import history from '../../history.js'

class WithdrawMethod extends Component {

    toBankAccount() {
        history.push('/withdraw/IDR', { balance: history.location.state.balance })
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h4 className="center-text">Withdraw Indonesian Rupiah</h4>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <h6 className="center-text">Choose Withdrawal Method</h6>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-4"></div>
                    <div onClick={() => this.toBankAccount()} className="col-md-4 col-12 boxed" style={{ paddingTop: '15px', paddingBottom: '6px' }}>
                        <div className="container">
                            <div className="row">
                                <h6 className="color-primary withdraw-idr-code bold col-md-10 col-10">Withdraw to Bank Account</h6>
                                <span className="col-md-1 center-text fa fa-chevron-right col-1"></span>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-md-12">
                                    <p>Withdraw to bank account via International wire or inter-bank transfer (For Singapore account holders). Withdrawalsmay take up to <span className="bold">3 Business days</span> to reach your bank account.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default WithdrawMethod