import React, { Component } from 'react'
import _ from 'lodash'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Select from 'react-select';
import countriesPhoneCode from '../../countriesPhoneCode.json'
import history from '../../history.js'
import actions from '../../core/actions/client/withdraw';
import { connect } from 'react-redux';
import { Loader } from 'react-loaders'
import NetworkError from '../../errors/networkError';

class Withdraw extends Component {

    constructor() {
        super()
        this.state = {
            banks: [],
            openModal: false,
            country: "+62",
            label: "",
            bank: "",
            account: "",
            beneficiary: "",
            errLabel: false,
            errAccount: false,
            errBank: false
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.value
            this.setState(obj)
        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    selectAccount(account) {
        history.push('/withdraw/' + account.AccountNumber + '/confirm', { account: account, balance: history.location.state.balance })
    }

    fetchBanks() {
        this.setState({
            loading: true
        });
        actions.fetchBanks(this.getToken()).then(res => {
            this.setState({
                banks: res.data.data,
                loading: false
            });
        }).catch(err => {
            this.setState({
                loading: false
            });
            NetworkError(err);
        })
    }

    gotoBanks() {
        history.push('/banks');
    }

    componentDidMount() {
        this.fetchBanks();
    }

    render() {

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h4 className="center-text">Withdraw to Bank Account</h4>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <h5 className="center-text">Choose Bank Account to Withdraw</h5>
                    </div>
                </div>
                <br />
                <div className="row">
                    {this.state.banks.length > 0 && <div className="col-md-12 boxed">

                        {this.state.banks.map((b, id) => {
                            return (
                                <div key={id} className="row boxed-inner">
                                    <div className="col-md-10">
                                        <p className="color-primary">{b.Label}</p>
                                        <p>{b.BankName} {b.AccountNumber}</p>
                                    </div>
                                    <div className="col-md-2 boxed-inner-action">
                                        <button onClick={() => this.selectAccount(b)} className="btn btn-primary">Select</button>
                                    </div>
                                </div>
                            )
                        })}

                    </div>}
                    <Loader type="line-scale" active={this.state.loading} className="col-12 text-center" />
                </div>
                <br />
                <div className="row">
                    <div className="col-md-4"></div>
                    <div className="col-md-4">
                        <button onClick={() => this.gotoBanks()} className="btn-block btn btn-primary">Add New Bank Account</button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user
    };
};

export default connect(mapStateToProps, null)(Withdraw);