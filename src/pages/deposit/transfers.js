import React, { Component } from 'react'
import classnames from 'classnames'

class Transfers extends Component {

  constructor() {
    super()
    this.state = {
      code: "U78II",
      toggleBox: false
    }
  }

  render() {
    return (
      <div className="container">
        <br />
        <div className="row">
          <div className="col-md-2"></div>
          <div className="col-md-8 col-12 boxed" style={{ paddingTop: '15px', paddingBottom: '6px' }}>
            <div className="container">
              <div className="row" onClick={() => this.setState({ toggleBox: !this.state.toggleBox })}>
                <h6 className="color-primary deposit-idr-code bold col-md-11 col-10">Deposit via local inter-bank transfer</h6>
                <span className={classnames('col-md-1', 'center-text', 'fa', 'col-1', { 'fa-chevron-up': this.state.toggleBox }, { 'fa-chevron-down': !this.state.toggleBox })}></span>
              </div>
              {this.state.toggleBox ?
                <div className="row">
                  <div className="row">
                    <div className="col-md-12">
                      <br />
                      <p>Transfer to out local bank and get it processed in within <span className="bold">1 bussiness day</span></p>
                      <p>Deposit cut-off times are 10am and 4pm everyday.</p>
                      <p>Transfers made before 10am will be processed by <span className="bold">12.30pm</span>. Transfers made before 4pm will be processed by <span className="bold">6.30pm</span></p>
                      <p>Deposit cut-off times are 10am and 4pm everyday.</p>
                      <p>Transfers must be made from bank account that matches your submitted identity card/passport name. Transfers made from a bank account not belongs to you will not be creditted.</p>
                      <p>To avoid delays please enter all details as described above without any missing or extra details.</p>
                      <p className="bold">Cash deposits made via ATM machines will not be accepted.</p>
                      <p className="bold">Please make a bank transfer to this following account.</p>
                    </div>
                    <div className="row">
                      <div className="col-md-12 col-12">
                        <div style={{ marginLeft: '10px' }} className="alert alert-warning" role="alert">
                          Your unique code is {this.state.code}
                        </div>
                      </div>
                    </div>
                    <br />
                    <div className="col-md-12 col-12">
                      <div className="row">
                        <div className="col-md-6 col-6">Bank Name</div>
                        <div className="col-md-6 col-6 bold">Bank Central Asia</div>
                      </div>
                      <hr />
                      <div className="row">
                        <div className="col-md-6 col-6">Recipient Name</div>
                        <div className="col-md-6 col-6 bold">PT Toko Crypto</div>
                      </div>
                      <hr />
                      <div className="row">
                        <div className="col-md-6 col-6">Initials (For DBS/POSB)</div>
                        <div className="col-md-6 col-6 bold">{this.state.code}</div>
                      </div>
                      <hr />
                      <div className="row">
                        <div className="col-md-6 col-6">Initials (For DBS/POSB)</div>
                        <div className="col-md-6 col-6 bold">{this.state.code}</div>
                      </div>
                      <hr />
                      <div className="row">
                        <div className="col-md-6 col-6">Account Number</div>
                        <div className="col-md-6 col-6 bold">454657686899</div>
                      </div>
                      <hr />
                      <div className="row">
                        <div className="col-md-6 col-6">Description / Comment / Purpose / Note </div>
                        <div className="col-md-6 col-6 bold">{this.state.code}</div>
                      </div>
                      <div className="row">
                        <div className="col-md-6 col-6"></div>
                        <div className="col-md-6 col-6 muted">to avoid any delays please enter Description / Comment / Purpose / Note as written above.</div>
                      </div>
                      <hr />
                    </div>
                  </div>
                </div> : null}
            </div>
          </div>
        </div>
      </div>
    )
  }

}

export default Transfers