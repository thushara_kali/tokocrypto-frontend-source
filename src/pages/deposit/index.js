import React, { Component } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import history from '../../history.js'

class Deposit extends Component {

  constructor() {
    super()
    this.state = {
      showModal: false
    }
  }

  openModal() {
    this.setState({
      showModal: true
    })
  }

  goToTransfer() {
    this.setState({
      showModal: false
    })
    // for now I use hardcoded routes since idk which id to retrieve just for UI purposes
    history.push('/deposit/IDR/transfers')
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h4 className="center-text">Deposit Indonesian Rupiah</h4>
          </div>
        </div>
        <br />
        <div className="row">
          <div className="col-md-12">
            <h6 className="center-text">Choose Deposit Method</h6>
          </div>
        </div>
        <br />
        <div className="row">
          <div className="col-md-4"></div>
          <div onClick={() => this.openModal()} className="col-md-4 col-12 boxed" style={{ paddingTop: '15px', paddingBottom: '6px' }}>
            <div className="container">
              <div className="row">
                <h6 className="color-primary deposit-idr-code bold col-md-10 col-10">Unique Deposit Code</h6>
                <span className="col-md-1 center-text fa fa-chevron-right col-1"></span>
              </div>
              <br />
              <div className="row">
                <div className="col-md-12">
                  <p>Deposit to bank account via International wire or inter-bank transfer (For Singapore account holders). Withdrawalsmay take up to <span className="bold">3 Business days</span> to reach your bank account.</p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Modal isOpen={this.state.showModal} className={this.props.className}>
          <ModalHeader toggle={() => this.setState({ showModal: false })}></ModalHeader>
          <ModalBody>
            <div>
              <h5 className="center-text bold">Unique Deposit Code</h5>
              <br />
              <p>Dear Valued Customer,</p>
              <p>Most deposit delay are caused by missing deposit code, we strongly recommend you to replace every recipient/comment field as possible (i.e recipient name,comment,reciever name, beneficiary, purpose) with your <span className="bold">UNIQUE CODE</span> when making your bank transfers.</p>
              <p></p>
            </div>
          </ModalBody>
          <ModalFooter>
            <button onClick={() => this.goToTransfer()} className="btn btn-primary btn-block">I will enter my deposit code only</button>
          </ModalFooter>
        </Modal>

      </div>
    )
  }

}

export default Deposit