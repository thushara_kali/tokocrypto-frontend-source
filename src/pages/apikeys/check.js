import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NotificationManager } from 'react-notifications'
import NetworkError from '../../errors/networkError';
import { bindActionCreators } from 'redux';
import { Loader } from '../../components/tcLoader';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import history from '../../history.js'

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class checkDeveloper extends Component {

    constructor(){
        super();
        this.state = {

        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }


    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    redirectPage(){
        
        history.push('/developer/index.html')
        window.location.reload();

    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    componentWillMount() {
        this.redirectPage();
        this.onSetLanguage();

    }

    render() {

        return (
            <div className="m-grid m-grid--hor m-grid--root m-page">

		        <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body" style={{ backgroundColor: "#f4f7fa"}}>
			    <div className="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container" style={{ paddingBottom: "250px"}}>
				<div className="m-grid__item m-grid__item--fluid m-wrapper">

					<div className="m-content">
                        You Will Redirect to Developers Page
                    </div>
                </div>
                </div>
                </div>

            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        language: state.language
    };
};


export default connect(mapStateToProps) (checkDeveloper);
