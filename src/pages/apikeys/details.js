import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { NotificationManager } from 'react-notifications'
import history from '../../history';
import Actions from '../../core/actions/client/apikey';
import OTPActions from '../../core/actions/client/otp';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import { Loader } from '../../components/tcLoader';
import "react-toggle/style.css"
import Toggle from 'react-toggle'
import { CopyToClipboard } from 'react-copy-to-clipboard';
import ReactTooltip from 'react-tooltip';
import NetworkError from '../../errors/networkError';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label } from 'reactstrap';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class ApiKeyDetail extends Component {

    constructor() {
        super();

        this.state = {
            key: {
                Label: "",
                PostOnly:false
            },
            loading: false,
            sending: false,
            revoking: false,
            activating: false,
            activatePopup: false,
            successPopup: false,
            openOTP: false,
            otpId: '',
            otpToken: '',
            method: 'ga',
            checkGAModal: false
        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    onSetLanguage() {
        strings.setLanguage(this.props.language);
    }

    fetchOTPStatus() {
        this.setState({
            loadingMfa: true
        });

        OTPActions.getOTPStatus(this.getToken()).then(res => {
            // 
            this.setState({
                checkGAModal: !res.data.data.ga,
                loadingMfa: false
            });
        }).catch(err => {
            // 
            NetworkError(err);
            this.setState({
                loadingMfa: false
            });
        });
    }

    /**
     * TCDOC-063 request OTP before updating Apikeys
     */
    requestOTP() {
        let OTPdata = {
            type: 'ga'
        };

        let data = _.clone(this.state.key);
        let formattedData = {}

        _.forOwn(data, function (value, key) {
            
            let newKey = _.camelCase(key);
            formattedData[newKey] = value;
        });

        formattedData.readSTP = formattedData.readStp;
        formattedData.writeSTP = formattedData.writeStp;
        formattedData.withdrawWhitelistIP = formattedData.withdrawWhitelistIp;

        delete formattedData.readStp;
        delete formattedData.writeStp;
        delete formattedData.withdrawWhitelistIp;

        let valid = formattedData.label.length !== 0;

        if (formattedData.writeWithdraw) {
            valid = valid && formattedData.withdrawWhitelistIP.length !== 0;
        }

        if (formattedData.withdrawWhitelistIP.length === 0) {
            formattedData.withdrawWhitelistIP = " ";
        }

        if (valid) {
            this.setState({
                sending: true
            });

            OTPActions.requestOTP(this.getToken(), OTPdata).then(res => {
                let response = res.data;

                this.setState({
                    otpId: response.data.OtpId,
                    method: response.data.Type,
                    loading: false,
                    openOTP: true
                });

            }).catch(err => {
                NotificationManager.error('OTP request Error');
                console.error(err);
                this.setState({ loading: false, loadingResend: false });
                NetworkError(err);
            });
        } else {
            if (formattedData.writeWithdraw) {
                NotificationManager.warning(strings.WITHDRAWAL_WHITELIST_IP_REQUIRED);
            } else {
                NotificationManager.warning(strings.LABEL_REQUIRED);
            }
        }
    }

    /**
     * TCDOC-064 verify OTP before updating Apikeys
     */
    completeVerification() {
        if (this.state.otpToken.length > 0) {
            this.setState({
                loading: true
            });
            OTPActions.clientMfaVerify(this.state.otpId, this.state.otpToken).then(res => {
                this.updateConfig();
            }).catch(err => {
                this.setState({
                    loading: false,
                    openOTP:false,
                    sending:false
                });
                NotificationManager.error(strings.OTP_GA_ERROR);
                
                // NetworkError(err);
            })

        }
    }

    getSingleApiKey() {
        let self = this;
        let id = history.location.pathname.split('/api-keys/')[1];
        this.setState({ loading: true });

        Actions.getSingleApiKey(this.getToken(), id).then(res => {

            if (!res.data.data.WithdrawWhitelistIP) {
                res.data.data.WithdrawWhitelistIP = ""
            }

            if (history.location.search && history.location.search === "?activate=true" && res.data.data.Active === false) {
                self.activateApiKey(res.data.data.ApiKey);
            } else {
                self.setState({
                    key: res.data.data,
                    loading: false,
                    activatePopup: false
                });
            }
        }).catch(err => {
            self.setState({ loading: false });
            
            NetworkError(err);
        });
    }

    /**
     * TCDOC-065 activate pending apikeys
     */
    activateApiKey(apikey) {
        this.setState({
            activatePopup: true,
            activating: true
        });

        Actions.activateApiKey(this.getToken(), apikey).then(res => {
            this.setState({
                activating: false,
                successPopup: true
            });
            NotificationManager.success('Success');
            this.getSingleApiKey();
        }).catch(err => {
            // NotificationManager.error(err.toString());
            NetworkError(err);
            this.setState({
                activating: false
            });
        });
    }

    /**
     * TCDOC-066 update current apikeys config 
     */
    updateConfig() {
        let id = history.location.pathname.split('/api-keys/')[1];
        let self = this;

        let data = _.clone(this.state.key);
        let formattedData = {}

        _.forOwn(data, function (value, key) {
            
            let newKey = _.camelCase(key);
            formattedData[newKey] = value;
        });

        formattedData.readSTP = formattedData.readStp;
        formattedData.writeSTP = formattedData.writeStp;
        formattedData.withdrawWhitelistIP = formattedData.withdrawWhitelistIp;

        delete formattedData.readStp;
        delete formattedData.writeStp;
        delete formattedData.withdrawWhitelistIp;

        let valid = formattedData.label.length !== 0;

        if (formattedData.writeWithdraw) {
            valid = valid && formattedData.withdrawWhitelistIP.length !== 0;
        }

        if (formattedData.withdrawWhitelistIP.length === 0) {
            formattedData.withdrawWhitelistIP = " ";
        }

        if (valid) {
            self.setState({ sending: true });
            Actions.updateSingleApiKey(this.getToken(), id, formattedData).then(res => {
                NotificationManager.success(strings.API_KEY_UPDATED);
                self.getSingleApiKey();
                self.setState({ sending: false, openOTP: false });
            }).catch(err => {
                NotificationManager.error(err.toString());
                // NetworkError(err);
                self.setState({ sending: false });
            });
        } else {
            if (formattedData.writeWithdraw && formattedData.label.length !== 0) {
                NotificationManager.warning(strings.WITHDRAWAL_WHITELIST_IP_REQUIRED);
            } else {
                NotificationManager.warning(strings.LABEL_REQUIRED);
            }
        }

    }

    gotoProfile() {
        history.push('/profile');
    }

    /**
     * TCDOC-067 revoke current apikeys 
     */
    revokeApiKey() {
        let id = history.location.pathname.split('/api-keys/')[1];
        let self = this;

        self.setState({ revoking: true });
        Actions.revokeApiKey(this.getToken(), id).then(res => {
            NotificationManager.success('test 1');
            NotificationManager.success(strings.API_KEY_UPDATED);
            // history.goBack();
            history.push(`/api-keys/`);
        }).catch(err => {
            // 
            // NotificationManager.error(err.toString());
            NetworkError(err);
            self.setState({ revoking: false });
        });
    }

    closeModal() {

    }

    closeCofirm() {
        this.setState({
            successPopup: false
        });
    }

    onCopy() {
        NotificationManager.info(strings.VALUE_COPIED);
    }

    onChangeAccess(access, value) {
        
        let key = _.clone(this.state.key);
        key[access] = value;
        this.setState({
            key
        });
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    componentDidMount() {
        this.fetchOTPStatus();
        this.getSingleApiKey();
    }

    render() {
        return (
            <div className="m-grid m-grid--hor m-grid--root m-page">
                <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body" style={{ backgroundColor: "#f4f7fa" }}>
                    <div style={{ height: '100vh', marginBottom: '50px' }} className="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
                        <div className="m-grid__item m-grid__item--fluid m-wrapper">

                            <div className="m-content">

                                <div className="m-portlet" style={{ marginBottom: "0px" }}>
                                    <div className="m-portlet__body  m-portlet__body--no-padding">
                                        <div className="row m-row--no-padding m-row--col-separator-xl">
                                            <div className="col-12 container">
                                                <div style={{ padding: '3%' }}>
                                                    <div className="row">
                                                        <div className="col-md-4 col-12">
                                                            <p style={{ marginTop: '25px', fontWeight: 'bold' }}>Api Name</p>
                                                            <input disabled={this.state.sending || this.state.loading} onChange={(e) => this.onChangeAccess('Label', e.target.value)} className="form-control" style={{ marginTop: '5px' }} type="text" value={this.state.key.Label} />
                                                        </div>
                                                        {/* <div className="col-md-8 col-12">
                                                            <p style={{ marginTop: '25px', fontWeight: 'bold' }}>Api Key</p>
                                                            <div className="row">
                                                                <div className="col-10">
                                                                    <input value={this.state.key.ApiKey} disabled placeholder={"Api Key will be generated"} className="form-control" />
                                                                </div>
                                                                <div className="col-2 flex-center">
                                                                    <CopyToClipboard text={this.state.key.ApiKey}
                                                                        onCopy={() => this.onCopy()}>
                                                                        <button className="btn btn-outline-info">
                                                                            <i className="fa fa-copy"></i> Copy API
                                                                        </button>
                                                                    </CopyToClipboard>
                                                                </div>
                                                            </div>
                                                        </div> */}
                                                    </div>
                                                    {this.state.loading && <div style={{ marginLeft: '48%' }}>
                                                        <Loader />
                                                    </div>}
                                                    {/* <div className="row">
                                                        <div className="col-md-4 col-12">
                                                            <p style={{ marginTop: '25px', fontWeight: 'bold' }}>{strings.WITHDRAWAL_WHITELIST_IP}</p>
                                                            <input disabled onChange={(e) => this.onChangeAccess('WithdrawWhitelistIP', e.target.value)} className="form-control" style={{ marginTop: '5px' }} type="text" value={this.state.key.WithdrawWhitelistIP} />
                                                        </div>
                                                        <div className="col-md-8 col-12">
                                                            <p style={{ marginTop: '25px', fontWeight: 'bold' }}>Api Secret</p>
                                                            <div className="row">
                                                                <div className="col-10">
                                                                    <input value={this.state.key.SecretKey} disabled placeholder={"Api Secret will be generated"} className="form-control" />
                                                                </div>
                                                                <div className="col-2 flex-center">
                                                                    <CopyToClipboard text={this.state.key.SecretKey}
                                                                        onCopy={() => this.onCopy()}>
                                                                        <button className="btn btn-outline-info">
                                                                            <i className="fa fa-copy"></i> Copy Secret
                                                                    </button>
                                                                    </CopyToClipboard>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> */}

                                                    <br /><br />

                                                    {!this.state.loading && <table className="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Access</th>
                                                                <th>Read</th>
                                                                <th>Write</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <tr>
                                                                <td>{strings.ACCOUNT_INFO}</td>
                                                                <td style={{ display: 'flex', alignItems: 'center' }}>
                                                                    <Toggle
                                                                        checked={this.state.key.ReadAccountInfo}
                                                                        onChange={() => this.onChangeAccess('ReadAccountInfo', !this.state.key.ReadAccountInfo)} />
                                                                    <i style={{ marginLeft: '5px', fontSize: '20px' }} className="fa fa-question-circle" data-tip data-for='apikey-history-info'></i>
                                                                    <ReactTooltip data-border='true' id="apikey-history-info" place="top" effect="solid">
                                                                        <p>{strings.API_INFO_ACCOUNT}</p>
                                                                    </ReactTooltip>
                                                                </td>
                                                                <td>
                                                                    <img data-tip data-for='apikey-no-write' className="blank-opts" src="/assets/blankopts.png" />
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>{strings.TRANSACTION_HISTORY}</td>
                                                                <td style={{ display: 'flex', alignItems: 'center' }}>
                                                                    <Toggle
                                                                        checked={this.state.key.ReadAccountHistory}
                                                                        onChange={() => this.onChangeAccess('ReadAccountHistory', !this.state.key.ReadAccountHistory)} />
                                                                    <i style={{ marginLeft: '5px', fontSize: '20px' }} className="fa fa-question-circle" data-tip data-for='apikey-transaction-info'></i>
                                                                    <ReactTooltip data-border='true' id="apikey-transaction-info" place="top" effect="solid">
                                                                        <p>{strings.API_INFO_HISTORY}</p>
                                                                    </ReactTooltip>
                                                                </td>
                                                                <td>
                                                                    <img data-tip data-for='apikey-no-write' className="blank-opts" src="/assets/blankopts.png" />
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>{strings.DEPOSIT_HISTORY}</td>
                                                                <td style={{ display: 'flex', alignItems: 'center' }}>
                                                                    <Toggle
                                                                        checked={this.state.key.ReadDeposit}
                                                                        onChange={() => this.onChangeAccess('ReadDeposit', !this.state.key.ReadDeposit)} />
                                                                    <i style={{ marginLeft: '5px', fontSize: '20px' }} className="fa fa-question-circle" data-tip data-for='apikey-deposit-info'></i>
                                                                    <ReactTooltip data-border='true' id="apikey-deposit-info" place="top" effect="solid">
                                                                        <p>{strings.API_INFO_DEPOSIT}</p>
                                                                    </ReactTooltip>
                                                                </td>
                                                                <td>
                                                                    <img data-tip data-for='apikey-no-read' className="blank-opts" src="/assets/blankopts.png" />
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>{strings.WITHDRAW_HISTORY}</td>
                                                                <td style={{ display: 'flex', alignItems: 'center' }}>
                                                                    <Toggle
                                                                        checked={this.state.key.ReadWithdraw}
                                                                        onChange={() => this.onChangeAccess('ReadWithdraw', !this.state.key.ReadWithdraw)} />
                                                                    <i style={{ marginLeft: '5px', fontSize: '20px' }} className="fa fa-question-circle" data-tip data-for='apikey-withdraw-info'></i>
                                                                    <ReactTooltip data-border='true' id="apikey-withdraw-info" place="top" effect="solid">
                                                                        <p>{strings.API_INFO_WITHDRAW}</p>
                                                                    </ReactTooltip>
                                                                </td>
                                                                <td>
                                                                    <img data-tip data-for='apikey-no-read' className="blank-opts" src="/assets/blankopts.png" />
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>Orders</td>
                                                                
                                                                <td style={{ alignItems: 'center' }}>
                                                                    <Toggle
                                                                        checked={this.state.key.ReadOrders} 
                                                                        onChange={() => this.onChangeAccess('ReadOrders', !this.state.key.ReadOrders)} />
                                                                    <i style={{ marginLeft: '5px', fontSize: '20px', position: 'absolute', marginTop: '3.5px' }} className="fa fa-question-circle" data-tip data-for='apikey-stp-info-read'></i>
                                                                    <ReactTooltip data-border='true' id="apikey-stp-info-read" place="top" effect="solid">
                                                                        <p>Orders info Read on Tokocrypto</p>
                                                                    </ReactTooltip>
                                                                </td>
                                
                                                                <td>
                                                                    <Toggle
                                                                        checked={this.state.key.WriteOrders} 
                                                                        onChange={() => this.onChangeAccess('WriteOrders', !this.state.key.WriteOrders)} />
                                                                    <i style={{ marginLeft: '5px', fontSize: '20px', position: 'absolute', marginTop: '3.5px' }} className="fa fa-question-circle" data-tip data-for='apikey-stp-info'></i>
                                                                    <Label check style={{marginLeft:"30px", position:"absolute", marginTop:"3px"}} data-tip data-for='apikey-post-only-info'>
                                                                        <Input type="checkbox" checked={this.state.key.PostOnly}  onChange={() => this.onChangeAccess('PostOnly', !this.state.key.PostOnly)} /> post-only
                                                                    </Label>
                                                                    <ReactTooltip data-border='true' id="apikey-stp-info" place="top" effect="solid">
                                                                        <p>Orders info Write on Tokocrypto</p>
                                                                    </ReactTooltip>
                                                                    <ReactTooltip data-border='true' id="apikey-post-only-info" place="top" effect="solid">
                                                                        <p>Activate the checkbox for market maker order</p>
                                                                    </ReactTooltip>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <ReactTooltip data-border='true' id="apikey-no-read" place="top" effect="solid">
                                                            <p>{strings.API_NO_READ}</p>
                                                        </ReactTooltip>
                                                        <ReactTooltip data-border='true' id="apikey-no-write" place="top" effect="solid">
                                                            <p>{strings.API_NO_WRITE}</p>
                                                        </ReactTooltip>
                                                    </table>}
                                                    <br />

                                                    <br />
                                                    <div className="row flex-even">
                                                        <button disabled={this.state.sending} className="btn btn-outline-info" onClick={() => history.push('/api-keys')}>
                                                            {strings.BACK}
                                                        </button>

                                                        <button disabled={this.state.sending || this.state.revoking || this.state.loading} className="btn btn-focus-green m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--success" onClick={() => this.requestOTP()}>{
                                                            this.state.sending ? strings.SAVING : strings.SAVE
                                                        }</button>

                                                        <button disabled={this.state.sending || this.state.revoking || this.state.loading} onClick={() => this.revokeApiKey()} className="btn btn-outline-danger">{strings.REVOKE}</button>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* activate modal */}
                <Modal isOpen={this.state.activatePopup} toggle={() => this.closeModal()}>
                    <ModalHeader toggle={() => this.closeModal()}>Activate Api Key</ModalHeader>
                    <ModalBody>
                        {strings.ACTIVATING_API_KEY}
                    </ModalBody>
                </Modal>

                {/* success modal */}
                <Modal size={'lg'} isOpen={this.state.successPopup} toggle={() => this.closeModal()}>
                    <ModalHeader toggle={() => this.closeModal()}>Succes Activate Api Key</ModalHeader>
                    <ModalBody>
                        <div className="alert alert-warning">
                            <p>{strings.ALERT_APIKEY}</p>
                        </div>
                        <br />
                        <div className="form-group row center-v">
                            <label className="control-label col-md-2">Api Key</label>
                            <div className="col-md-10 row">
                                <input className="form-control col-md-9" disabled value={this.state.key.ApiKey} />
                                <div className="col-3 flex-center">
                                    <CopyToClipboard text={this.state.key.ApiKey}
                                        onCopy={() => this.onCopy()}>
                                        <button className="btn btn-outline-info">
                                            <i className="fa fa-copy"></i> Copy API
                                                                        </button>
                                    </CopyToClipboard>
                                </div>
                            </div>
                        </div>
                        <div className="form-group row center-v">
                            <label className="control-label col-md-2">Secret Key</label>
                            <div className="col-md-10 row">
                                <input className="form-control col-md-9" disabled value={this.state.key.SecretKey} />
                                <div className="col-3 flex-center">
                                    <CopyToClipboard text={this.state.key.SecretKey}
                                        onCopy={() => this.onCopy()}>
                                        <button className="btn btn-outline-info">
                                            <i className="fa fa-copy"></i> Copy Secret
                                                                        </button>
                                    </CopyToClipboard>
                                </div>
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button disabled={this.state.activating} color="success" onClick={() => this.closeCofirm()}>{
                            'Ok'
                        }</Button>
                    </ModalFooter>
                </Modal>

                {/* otp modal */}
                <Modal isOpen={this.state.openOTP}>
                    <ModalHeader>Api Key</ModalHeader>
                    <ModalBody>
                        <div className="form-group">
                            <label className="control-label">{strings.GA_SECURITY_CODE}</label>
                            <input disabled={this.state.loading} onChange={(e) => this.setState({ otpToken: e.target.value })} type="text" className="form-control" />
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button disabled={this.state.loading} color="success" onClick={() => this.completeVerification()}>{
                            this.state.loading ? strings.SENDING_OTP : strings.SEND
                        }</Button>
                    </ModalFooter>
                </Modal>

                {/* require GA modal */}
                <Modal isOpen={this.state.checkGAModal}>
                    <ModalBody>
                        <div>
                            {strings.GA_REQUIRED}
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button disabled={this.state.loading} color="success" onClick={() => this.gotoProfile()}>
                            {strings.GOTO_PROFILE}
                        </Button>
                    </ModalFooter>
                </Modal>
            </div >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        trading: state.trading,
        language: state.language
    };
};

export default connect(mapStateToProps, null)(ApiKeyDetail)
