import React, { Component } from 'react';
import { connect } from 'react-redux';
import Actions from '../../core/actions/client/apikey';
import history from '../../history';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import { Loader } from '../../components/tcLoader';
import NetworkError from '../../errors/networkError';
import ReactTooltip from 'react-tooltip';
import { NotificationManager } from 'react-notifications'
import ReactGA from 'react-ga';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

/**
 * TCDOC-055 this is the main page for apikey, it display the current user api keys (MAX 2)
 */

class ApiKeyList extends Component {

    constructor() {
        super();

        this.state = {
            keys: [],
            loading: false
        }
    }

    callGA(){
        ReactGA.initialize('UA-116825757-2');
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    /**
     * TCDOC-057 this function for remove user api keys
     */
    revokeApiKey(id) {
        let self = this;

        self.setState({ revoking: true });
        Actions.revokeApiKey(this.getToken(), id).then(res => {
            NotificationManager.success(strings.API_KEY_DELETED);
            // history.goBack();
            self.getMyApiKeys();
        }).catch(err => {
            
            // NotificationManager.error(err.toString());
            NetworkError(err);
            self.setState({ revoking: false });
        });
    }

    /**
     * TCDOC-056 this function for fetch user api keys
     */
    getMyApiKeys() {
        this.setState({ loading: true });
        Actions.getMyApiKeys(this.getToken()).then(res => {
            this.setState({
                keys: res.data.data,
                loading: false
            });
        }).catch(err => {
            this.setState({ loading: false });
            
            let details = {
                type: 'COMPONENTS: apikeys | FUNC: getMyApiKeys',
                url: 'apikey/client/apikey/all'
              }
            NetworkError(err, details);
        });
    }

    openDetail(id) {
        history.push(`/api-keys/${id}`);
    }

    /**
     * TCDOC-058 this function for forwarding to create new api keys page, but will check current user apikeys
     */
    createApiKey() {
        if(this.state.keys.length < 2) {
            history.push('/api-keys/create');
        } else {
            NotificationManager.warning(strings.MAXIMUM_KEY);
        }

    }

    onSetLanguage() {
        strings.setLanguage(this.props.language);
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    componentDidMount() {
        this.callGA();
        this.getMyApiKeys();
    }

    render() {
        return (
            <div className="m-grid m-grid--hor m-grid--root m-page">
                <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body" style={{ backgroundColor: "#f4f7fa" }}>
                    <div style={{ height: "100vh" }} className="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
                        <div className="m-grid__item m-grid__item--fluid m-wrapper">

                            <div className="m-content">

                                <div className="m-portlet" style={{ marginBottom: "0px" }}>
                                    <div className="m-portlet__body  m-portlet__body--no-padding">
                                        <div className="row m-row--no-padding m-row--col-separator-xl">
                                            <div className="col-12 container">
                                                <div className="apikey-wrapper">
                                                    <h3 style={{ marginTop: '5px', marginLeft: '5px', fontWeight: 'bold' }}>API KEY</h3>
                                                    <br />
                                                    <div className="apikey-info-wrapper">
                                                        <div className="row">
                                                            <div className="col-md-8 col-12">
                                                                <p>Creating an API key allow you to use a third-party website or mobile application to use currency security quotes, real-time transaction and other services.</p>
                                                            </div>
                                                            <div className="col-md-4 col-12 flex-center">
                                                                <button onClick={() => this.createApiKey()} className="btn btn-focus-green m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">{strings.API_KEY_CREATE}</button>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <br /><br />
                                                    {this.state.loading && <div style={{ marginLeft: '48%' }}>
                                                        <Loader />
                                                    </div>}
                                                    {!this.state.loading && <table style={{ tableLayout: 'fixed' }} className="table">
                                                        <thead style={{ background: '#F7F7F7' }}>
                                                            <tr>
                                                                <th width="20%">Label</th>
                                                                <th width="20%">Api Key</th>
                                                                <th width="20%">Secret Key</th>
                                                                <th width="20%">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {
                                                                this.state.keys.map(k => {
                                                                    return (
                                                                        <tr key={k.ApiKeyId}>
                                                                            <td className="apikey-pointer" onClick={() => this.openDetail(k.ApiKeyId)}>{k.Label}</td>
                                                                            <td style={{ wordWrap: 'break-word' }}>{k.ApiKey}</td>
                                                                            <td style={{ wordWrap: 'break-word' }} className="center-v secret" data-tip data-for={`apikey-secret-alert-${k.ApiKey}`}>
                                                                                <i className="fa fa-exclamation-circle"></i>
                                                                                <span style={{ marginTop: '5px'}}>&nbsp;**************************</span>
                                                                                <ReactTooltip style={{ width: '250px'}} data-border='true' id={`apikey-secret-alert-${k.ApiKey}`} place="top" effect="solid">
                                                                                    <p>{strings.ALERT_APIKEY}</p>
                                                                                </ReactTooltip>
                                                                            </td>
                                                                            <td>
                                                                                <button onClick={() => this.revokeApiKey(k.ApiKeyId)} className="btn btn-outline-danger">
                                                                                    <i className="fa fa-trash"></i>
                                                                                </button>
                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                })
                                                            }
                                                            {
                                                                this.state.keys.length == 0 && !this.state.loading && <tr>
                                                                    <td colSpan="3" style={{ textAlign: 'center', fontSize: '18px' }}>{strings.NO_API_KEY}</td>
                                                                </tr>
                                                            }
                                                        </tbody>
                                                    </table>}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        trading: state.trading,
        language: state.language
    };
};

export default connect(mapStateToProps, null)(ApiKeyList)
