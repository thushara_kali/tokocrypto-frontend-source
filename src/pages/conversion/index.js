import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import classnames from 'classnames';
import buyActions from '../../core/actions/client/buy';
import orderBookActions from '../../core/actions/client/orderBook';
import sellActions from '../../core/actions/client/buy';
import { NotificationManager } from 'react-notifications'
import KYCMessage from '../../components/kycMessage';
import homeActions from '../../core/actions/client/home';
import referralActions from '../../core/actions/client/referral';
import NetworkError from '../../errors/networkError';
import amountFormat from '../../helpers/amountFormatter';
import cryptoFormatter, { cryptoFormatterByCoin, cryptoFormatterReplaceFormat } from '../../helpers/cryptoFormatter';
import currencyFormatter from '../../helpers/currencyFormatter';
import { formatNoExponent } from '../../helpers/numbers';
import history from '../../history.js'
// import { Loader } from 'react-loaders'
import { Loader } from '../../components/tcLoader';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {faEthereum, faBtc} from '@fortawesome/fontawesome-free-brands';
import {faCreditCard, faFighterJet, faExchangeAlt } from '@fortawesome/fontawesome-free-solid';
import NP from 'number-precision';

fontawesome.library.add(faCreditCard, faFighterJet, faEthereum, faBtc, faExchangeAlt)

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class Conversion extends Component {

    constructor(){
        super();
        this.state = {
            BTCRate: {},
            ETHRate: {},
            XRPRate: {},
            DGXRate: {},
            TTCRate: {},
            GUSDRate: {},
            PAXRate: {},
            USDCRate: {},
            TUSDRate: {},
            USDTRate: {},
            IDRAccount: {},
            BTCAccount: {},
            ETHAccount: {},
            XRPAccount: {},
            DGXAccount: {},
            TTCAccount: {},
            GUSDAccount: {},
            PAXAccount: {},
            USDCAccount: {},
            TUSDAccount: {},
            USDTAccount: {},
            activeCurrency: 'BTC',
            activeAction: 'buy', // buy, sell
            amount: "",
            cryptoAmount:"",
            currencyUsed: '',
            fixed_side: 'sell',
            rate: 0,
            fee: 0,
            finalAmount: 0,
            finalFee: 0,
            finalTotal: 0,
            finalRecived: 0,
            finalIDR: 0,
            saveAmount: 0,
            saveFee: 0,
            saveTotal: 0,
            saveRecived: 0,
            saveIDR: 0,
            saveRate: 0,
            btcMinTrade: 0.0002,
            ethMinTrade: 0.002,
            xrpMinTrade: 5,
            dgxMinTrade: 0.002,
            ttcMinTrade: 0.002,
            idrMinTrade: 50000,
            gusdMinTrade: 0.002,
            paxMinTrade: 0.002,
            tusdMinTrade: 0.002,
            usdtMinTrade: 0.002,
            usdcMinTrade: 0.002,
            loading: true,
            completedRecipte: false,
            confirmation: false,
            dailyTradingLimit: 0,
            currentUsage: 0,
            currentUsagePercentage: 0,
            mobileHeight: 3500,
            userDiscount: 0,
            saveDiscount: 0,
            loadingRecipte: false,
            order_id: 0 
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})

            if(fieldName === 'amount'){
                const re = /^[0-9]+$/;
                if (event.target.value == '' || re.test(event.target.value)) {
                    obj[fieldName] = event.target.value
                }
            } else {
                const re = /^(\d+(\,\d*)?)$/;
                if (event.target.value == '' || re.test(event.target.value)) {
                    obj[fieldName] = event.target.value
                }
            }

            if(fieldName==='amount') {

                let convertCryptoAmout = 0

                if (typeof (obj[fieldName]) != 'number' && obj[fieldName] != undefined) {
                    convertCryptoAmout = parseFloat(obj[fieldName].replace(/\,/g, '.'));
                } else {
                    convertCryptoAmout = obj[fieldName];
                }

                obj['cryptoAmount'] = cryptoFormatterByCoin( (convertCryptoAmout) / Number(this.state.rate), this.state.activeCurrency )

                if(isNaN(Number( cryptoFormatterReplaceFormat(obj['cryptoAmount'])))){
                    obj['cryptoAmount'] = 0
                }
                obj['cryptoAmount'] = cryptoFormatterByCoin( obj['cryptoAmount'] , this.state.activeCurrency)
            } else {
                obj['amount'] = (obj[fieldName]*this.state.rate)
                obj['amount'] = amountFormat(obj['amount'])
                if (typeof (obj['amount']) != 'number' && obj[fieldName] != undefined) {
                    obj['amount'] = parseFloat(obj['amount'].replace(/\./g, ''));
                }
            }


            this.setState(
                obj
            )
        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    loadFromURL(){

        if(history.location.search.length > 0){

            // 

            let coin = (history.location.search.split('&')[1].split('=')[1]).toLowerCase()
            let action = (history.location.search.split('?')[1].split('&')[0].split('=')[1]).toLowerCase()

            if(action == "buy"){
                this.setAction('buy')
            } else if ( action == "sell"){
                this.setAction('sell')
            }

            if(coin == "btc"){
                this.setCurrency('BTC')
            } else if (coin == "eth"){
                this.setCurrency('ETH')
            } else if (coin == "xrp"){
                this.setCurrency('XRP')
            } else if (coin == "dgx"){
                this.setCurrency('DGX')
            } else if (coin == "ttc"){
                this.setCurrency('TTC')
            } else if (coin == "gusd"){
                this.setCurrency('GUSD')
            } else if (coin == "pax"){
                this.setCurrency('PAX')
            } else if (coin == "tusd"){
                this.setCurrency('TUSD')
            } else if (coin == "usdt"){
                this.setCurrency('USDT')
            } else if (coin == "usdc"){
                this.setCurrency('USDC')
            }

        }

    }

    conversionBuy(){

        let virtualCurrencyCode = this.state.activeCurrency
        let currencyCode = 'IDR'

        let data = {
            baseCurrency: virtualCurrencyCode,
            counterCurrency: currencyCode
        }
        this.setState({
            loading: true
        })

        let rateSide = 'customer_'+this.state.activeAction;

        buyActions.conversionBuy(this.getToken(), data).then(res => {
            this.setState({
                loading: false,
                review: false,
                reviewData: res.data,
                fee: res.data.fee.percentage,
                rate: res.data.rate[rateSide]

            });

        }).catch(err => {
            NetworkError(err)
            this.setState({
                loading: false
            })
        })
    }

    resetInput() {
        this.setState({
            amount: "",
            cryptoAmount:"",
            loading: false,
            review: false,
            completedRecipte: false,
            spent: 0,
            sell: 0,
            recieved: 0,
            finalFee:0,
            saveAmount: 0,
            saveFee: 0,
            saveTotal: 0,
            saveRecived: 0,
            saveIDR: 0,
            saveDiscount: 0,
            reviewData: {}
        })
        clearTimeout(this.timeout)
    }

    scrollIntoContent(){

        let width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

        if(width < 500){
            this.receipt.scrollIntoView({ behavior: "smooth" });
            if(this.state.completedRecipte == true){
                this.setState({
                    mobileHeight:3000
                })
            }
        }
    }

    buyCoin() {

        let virtualCurrencyCode = this.state.activeCurrency
        let currencyCode = 'IDR'

        let transactionAmount = 0

        if(this.state.currencyUsed == currencyCode){
            transactionAmount = this.state.amount;
        } else {
            transactionAmount = cryptoFormatterReplaceFormat( this.state.cryptoAmount );
        }

        if (this.state.currencyUsed == currencyCode && typeof (transactionAmount) != 'number') {
            transactionAmount = parseFloat(transactionAmount.replace(/\./g, ''))
        }

        let balance = 0;

        if(virtualCurrencyCode === 'BTC'){
            balance = this.state.BTCAccount.Balance;
        } else if(virtualCurrencyCode === 'ETH') {
            balance = this.state.ETHAccount.Balance;
        } else if(virtualCurrencyCode === 'XRP') {
            balance = this.state.XRPAccount.Balance;
        } else if(virtualCurrencyCode === 'DGX') {
            balance = this.state.DGXAccount.Balance;
        } else if(virtualCurrencyCode === 'TTC') {
            balance = this.state.TTCAccount.Balance;
        } else if(virtualCurrencyCode === 'GUSD') {
            balance = this.state.GUSDAccount.Balance;
        } else if(virtualCurrencyCode === 'PAX') {
            balance = this.state.PAXAccount.Balance;
        } else if(virtualCurrencyCode === 'TUSD') {
            balance = this.state.TUSDAccount.Balance;
        } else if(virtualCurrencyCode === 'USDT') {
            balance = this.state.USDTAccount.Balance;
        } else if(virtualCurrencyCode === 'USDC') {
            balance = this.state.USDCAccount.Balance;
        }

        let amountSufficient = true;

        if (this.state.currencyUsed == currencyCode) {
            // transactionAmount = Number(transactionAmount)
            if (Number(transactionAmount) > Number(this.state.IDRAccount.Balance)) {
                NotificationManager.warning(strings.DONT_HAVE_ENOUGH_AMOUNT)
                amountSufficient = false;
            }

        } else {
            transactionAmount = Number(transactionAmount) * Number(this.state.rate)

            let buyAccount = null;
            if(Number(transactionAmount) > Number(this.state.IDRAccount.Balance)) {
                NotificationManager.warning(strings.DONT_HAVE_ENOUGH_AMOUNT)
                amountSufficient = false;
            }
        }

        // amountSufficient = false;

        if(amountSufficient){
            this.setState({
                loading: true
            });

            let convertedAmount;

            if (this.state.currencyUsed == currencyCode) {
                convertedAmount = Number(formatNoExponent(Number( cryptoFormatterReplaceFormat( this.state.finalAmount))));
            } else {
                convertedAmount = Number(formatNoExponent(Number(parseFloat(this.state.amount.replace(/\./g, '')))));
                // convertedAmount = Number(formatNoExponent(Number(this.state.amount))) * Number(this.state.rate)
            }

            // let data = {
            //     buy_currency: virtualCurrencyCode,
            //     sell_currency: currencyCode,
            //     amount: convertedAmount,
            //     fixed_side: this.state.fixed_side
            // };

            let data = {
                quantity: convertedAmount,
                type: 'market',
                buyCurrency: virtualCurrencyCode,
                sellCurrency: currencyCode,
                fixedSide: 'buy'
            };

            orderBookActions.buyOrder(this.getToken(),data).then(res => {
              
                let resData = res.data

                this.setState({
                    loading: false,
                    // saveFee:resData.Fee.Amount,
                    // saveTotal: resData.SellAmount,
                    // saveAmount : resData.TotalBuyAmount,
                    // saveIDR: resData.TotalSellAmount,
                    // saveRate: resData.Rate,
                    // saveDiscount: resData.Fee.Discount,
                    completedRecipte: true,
                    loadingRecipte: true,
                    amount:0,
                    recieved:0,
                    order_id: resData.order.id
                });

                NotificationManager.success(strings.TRANSACTION_SUCCESS);
                this.setState({
                    amount: "",
                    cryptoAmount:"",
                    confirmation: false
                });
                this.scrollIntoContent();
                this.fetchRates();
                this.getTradingLimit();

                if(!this.autofetchOrderResult){
                    this.autofetchOrderResult = setInterval(() => {
                        this.fetchOrderResult('buy');
                    }, 1000 * 10);
        
                    if(window.intervals){
                        window.intervals.push(this.autofetchOrderResult)
                    } else {
                        window.intervals = [this.autofetchOrderResult]
                    }
                }

            }).catch(err => {
                let errors = strings.TRASNACTION_FAILED_TRY_AGAIN;
                // if (err.response && err.response.data.err.errorMessage) {
                //     let msg = Object.assign( {} , JSON.parse(err.response.data.err.errorMessage));
                //     errors = msg["message"];

                //     if (msg["reason"] === 'InsufficientFunds' || !msg["message"]) {
                //         errors = strings.TRASNACTION_NOT_COMPLETE_CONTACT_SUPPORT
                //     }
                //     NotificationManager.error(errors);
                // } else {
                //     NotificationManager.error(errors);
                // }
                this.setState({
                    loading: false,
                    confirmation: false
                });
                NetworkError(err)
                // if (err.response && err.response.data.err) {
            
                //     errors = err.response.data.err.message;
    
                //     NotificationManager.error(errors);
                // } else {
                //     NotificationManager.error(errors);
                // }
            });
        }
    }

    sellCoin() {

        let virtualCurrencyCode = this.state.activeCurrency
        let currencyCode = 'IDR'

        let transactionAmount = 0

        if(this.state.currencyUsed == currencyCode){
            transactionAmount = this.state.amount;
        } else {
            transactionAmount = Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount ));
        }

        if (this.state.currencyUsed == currencyCode && typeof (transactionAmount) != 'number') {
            transactionAmount = parseFloat(transactionAmount.replace(/\./g, ''));
        }


        let amountSufficient = true;

        // 

        let balance = 0;

        if(virtualCurrencyCode === 'BTC'){
            balance = this.state.BTCAccount.Balance;
        } else if(virtualCurrencyCode === 'ETH') {
            balance = this.state.ETHAccount.Balance;
        } else if(virtualCurrencyCode === 'XRP') {
            balance = this.state.XRPAccount.Balance;
        } else if(virtualCurrencyCode === 'DGX') {
            balance = this.state.DGXAccount.Balance;
        } else if(virtualCurrencyCode === 'TTC') {
            balance = this.state.TTCAccount.Balance;
        } else if(virtualCurrencyCode === 'GUSD') {
            balance = this.state.GUSDAccount.Balance;
        } else if(virtualCurrencyCode === 'PAX') {
            balance = this.state.PAXAccount.Balance;
        } else if(virtualCurrencyCode === 'TUSD') {
            balance = this.state.TUSDAccount.Balance;
        } else if(virtualCurrencyCode === 'USDT') {
            balance = this.state.USDTAccount.Balance;
        } else if(virtualCurrencyCode === 'USDC') {
            balance = this.state.USDCAccount.Balance;
        }

        // amount point of view
        if (this.state.currencyUsed == currencyCode) {

            if (Number(balance) < (Number(transactionAmount) / Number(this.state.rate))) {
                NotificationManager.warning(strings.DONT_HAVE_ENOUGH_AMOUNT)
                amountSufficient = false;
            }

        } else {

            if(Number(balance) < (Number(transactionAmount))) {
                NotificationManager.warning(strings.DONT_HAVE_ENOUGH_AMOUNT)
                amountSufficient = false;
            }
        }

        // amountSufficient = false;

        if(amountSufficient){
            this.setState({
                loading: true
            });

            let convertedAmount;

            if (this.state.currencyUsed == currencyCode) {
                // convertedAmount = Number(formatNoExponent(Number(this.state.amount))) / Number(this.state.rate);
                convertedAmount = Number(formatNoExponent(Number(parseFloat(this.state.amount.replace(/\./g, '')))));
            } else {
                // convertedAmount = Number(formatNoExponent(Number(this.state.amount))) * Number(this.state.rate);
                convertedAmount = Number(formatNoExponent(Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount))));
            }

            // let data = {
            //     buy_currency: currencyCode,
            //     sell_currency: virtualCurrencyCode,
            //     amount: convertedAmount,
            //     fixed_side: this.state.fixed_side
            // };

            let data = {
                quantity: convertedAmount,
                type: 'market',
                buyCurrency: currencyCode,
                sellCurrency: virtualCurrencyCode,
                fixedSide: this.state.fixed_side
            };

            orderBookActions.buyOrder(this.getToken(),data).then(res => {
                let resData = res.data

                this.setState({
                    loading: false,
                    // saveFee:resData.Fee.Amount,
                    // saveTotal: resData.TotalBuyAmount,
                    // saveAmount : resData.TotalSellAmount,
                    // saveIDR: resData.BuyAmount,
                    // saveRate: resData.Rate,
                    // saveDiscount: resData.Fee.Discount,
                    completedRecipte: true,
                    amount:0,
                    recieved:0,
                    loadingRecipte: true,
                    order_id: resData.order.id
                });

                NotificationManager.success(strings.TRANSACTION_SUCCESS);
                this.setState({
                    amount: "",
                    cryptoAmount:"",
                    confirmation: false
                });
                this.scrollIntoContent();
                this.fetchRates();
                this.getTradingLimit();
                if(!this.autofetchOrderResult){
                    this.autofetchOrderResult = setInterval(() => {
                        this.fetchOrderResult('sell');
                    }, 1000 * 10);
        
                    if(window.intervals){
                        window.intervals.push(this.autofetchOrderResult)
                    } else {
                        window.intervals = [this.autofetchOrderResult]
                    }
                }
                
            }).catch(err => {
                let errors = strings.TRASNACTION_FAILED_TRY_AGAIN;
                // if (err.response && err.response.data.err.errorMessage) {
                //     let msg = Object.assign( {} , JSON.parse(err.response.data.err.errorMessage));
                //     errors = msg["message"];

                //     if (msg["reason"] === 'InsufficientFunds' || !msg["message"]) {
                //         errors = strings.TRASNACTION_NOT_COMPLETE_CONTACT_SUPPORT
                //     }
                //     NotificationManager.error(errors);
                // } else {
                //     NotificationManager.error(errors);
                // }
                // this.setState({
                //     loading: false
                // });
                this.setState({
                    loading: false,
                    confirmation: false
                });
                NetworkError(err)
                // if (err.response && err.response.data.err) {
            
                //     errors = err.response.data.err.message;
    
                //     NotificationManager.error(errors);
                // } else {
                //     NotificationManager.error(errors);
                // }
            });
        }
    }

    fetchOrderResult(type){

        let data = {
            order_id: this.state.order_id
        };

        orderBookActions.fetchOrderResult(this.getToken(),data).then(res => {
          
            let resData = res.data.result[0]

            if(res.data.result.length > 0){
                if(type == 'buy'){
                    this.setState({
                        loading: false,
                        saveFee:resData.Fee.Amount,
                        saveTotal: resData.SellAmount,
                        saveAmount : resData.TotalBuyAmount,
                        saveIDR: resData.TotalSellAmount,
                        saveRate: resData.Rate,
                        saveDiscount: resData.Fee.Discount,
                        completedRecipte: true,
                        loadingRecipte: false
                    });
                }else if(type == 'sell'){
                    this.setState({
                        loading: false,
                        saveFee:resData.Fee.Amount,
                        saveTotal: resData.TotalBuyAmount,
                        saveAmount : resData.TotalSellAmount,
                        saveIDR: resData.BuyAmount,
                        saveRate: resData.Rate,
                        saveDiscount: resData.Fee.Discount,
                        completedRecipte: true,
                        loadingRecipte: false
                    });
                }
    
                if(window.intervals){
                    window.intervals.map((i, index) => {
                        clearInterval(i)
                        window.intervals.splice(index, 1);
                    });
                }
            }else{
                this.setState({
                    loading: false,
                    completedRecipte: true,
                    loadingRecipte: true
                });
            }
            
        }).catch(err => {
            // let errors = strings.TRASNACTION_FAILED_TRY_AGAIN;
            // if (err.response && err.response.data.err) {
            //     errors = err.response.data.err;

            //     NotificationManager.error(errors);
            // } else {
            //     NotificationManager.error(errors);
            // }
            NetworkError(err);
            this.setState({
                loading: false,
                completedRecipte: true,
                loadingRecipte: true
            });

            if(window.intervals){
                window.intervals.map((i, index) => {
                    clearInterval(i)
                    window.intervals.splice(index, 1);
                });
            }
        });
    }

    fetchRates() {
        let username = this.props.user.username;
        let clientId = this.props.user.pool.clientId;
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];
        homeActions.fetchDashboard(token).then(res => {
            let BTCRate = _.find(res.data.rates, { currencyPair: 'BTCIDR'});
            let ETHRate = _.find(res.data.rates, { currencyPair: 'ETHIDR'});
            let XRPRate = _.find(res.data.rates, { currencyPair: 'XRPIDR'});
            let DGXRate = _.find(res.data.rates, { currencyPair: 'DGXIDR'});
            let TTCRate = _.find(res.data.rates, { currencyPair: 'TTCIDR'});
            let GUSDRate = _.find(res.data.rates, { currencyPair: 'GUSDIDR'})
            let PAXRate = _.find(res.data.rates, { currencyPair: 'PAXIDR'})
            let TUSDRate = _.find(res.data.rates, { currencyPair: 'TUSDIDR'})
            let USDTRate = _.find(res.data.rates, { currencyPair: 'USDTIDR'})
            let USDCRate = _.find(res.data.rates, { currencyPair: 'USDCIDR'})

            let IDRAccount = _.find(res.data.accounts, {CurrencyCode: 'IDR'});
            let BTCAccount = _.find(res.data.accounts, {CurrencyCode: 'BTC'});
            let ETHAccount = _.find(res.data.accounts, {CurrencyCode: 'ETH'});
            let XRPAccount = _.find(res.data.accounts, {CurrencyCode: 'XRP'});
            let DGXAccount = _.find(res.data.accounts, {CurrencyCode: 'DGX'});
            let TTCAccount = _.find(res.data.accounts, {CurrencyCode: 'TTC'});
            let GUSDAccount = _.find(res.data.accounts, {CurrencyCode: 'GUSD'});
            let PAXAccount = _.find(res.data.accounts, {CurrencyCode: 'PAX'});
            let USDCAccount = _.find(res.data.accounts, {CurrencyCode: 'USDC'});
            let TUSDAccount = _.find(res.data.accounts, {CurrencyCode: 'TUSD'});
            let USDTAccount = _.find(res.data.accounts, {CurrencyCode: 'USDT'});

            this.setState({
                BTCRate,
                ETHRate,
                XRPRate,
                DGXRate,
                TTCRate,
                GUSDRate,
                PAXRate,
                TUSDRate,
                USDTRate,
                USDCRate,
                // rate: BTCRate.customer_buy,
                IDRAccount,
                BTCAccount,
                ETHAccount,
                XRPAccount,
                DGXAccount,
                TTCAccount,
                GUSDAccount,
                PAXAccount,
                USDCAccount,
                TUSDAccount,
                USDTAccount
            });

            if(this.state.activeAction == 'buy'){
                if(this.state.activeCurrency == 'BTC'){
                    this.setState({
                        rate: BTCRate.customer_buy
                    })
                } else if (this.state.activeCurrency == 'ETH'){
                    this.setState({
                        rate: ETHRate.customer_buy
                    })
                } else if (this.state.activeCurrency == 'XRP'){
                    this.setState({
                        rate: XRPRate.customer_buy
                    })
                } else if (this.state.activeCurrency == 'DGX'){
                    this.setState({
                        rate: DGXRate.customer_buy
                    })
                } else if (this.state.activeCurrency == 'TTC'){
                    this.setState({
                        rate: TTCRate.customer_buy
                    })
                } else if (this.state.activeCurrency == 'GUSD'){
                    this.setState({
                        rate: GUSDRate.customer_buy
                    })
                } else if (this.state.activeCurrency == 'PAX'){
                    this.setState({
                        rate: PAXRate.customer_buy
                    })
                } else if (this.state.activeCurrency == 'TUSD'){
                    this.setState({
                        rate: TUSDRate.customer_buy
                    })
                } else if (this.state.activeCurrency == 'USDT'){
                    this.setState({
                        rate: USDTRate.customer_buy
                    })
                } else if (this.state.activeCurrency == 'USDC'){
                    this.setState({
                        rate: USDCRate.customer_buy
                    })
                }
            } else if (this.state.activeAction == 'sell'){
                if(this.state.activeCurrency == 'BTC'){
                    this.setState({
                        rate: BTCRate.customer_sell
                    })
                } else if (this.state.activeCurrency == 'ETH'){
                    this.setState({
                        rate: ETHRate.customer_sell
                    })
                } else if (this.state.activeCurrency == 'XRP'){
                    this.setState({
                        rate: XRPRate.customer_sell
                    })
                } else if (this.state.activeCurrency == 'DGX'){
                    this.setState({
                        rate: DGXRate.customer_sell
                    })
                } else if (this.state.activeCurrency == 'TTC'){
                    this.setState({
                        rate: TTCRate.customer_sell
                    })
                } else if (this.state.activeCurrency == 'GUSD'){
                    this.setState({
                        rate: GUSDRate.customer_sell
                    })
                } else if (this.state.activeCurrency == 'PAX'){
                    this.setState({
                        rate: PAXRate.customer_sell
                    })
                } else if (this.state.activeCurrency == 'TUSD'){
                    this.setState({
                        rate: TUSDRate.customer_sell
                    })
                } else if (this.state.activeCurrency == 'USDT'){
                    this.setState({
                        rate: USDTRate.customer_sell
                    })
                } else if (this.state.activeCurrency == 'USDC'){
                    this.setState({
                        rate: USDCRate.customer_sell
                    })
                }
            }


        }).catch(err => {
            // 
            NetworkError(err);
            if(window.intervals){
                window.intervals.map((i, index) => {
                    clearInterval(i)
                    window.intervals.splice(index, 1);
                });
            }
        });
    }

    switchSideTo(currency){
        let virtualCurrencyCode = this.state.activeCurrency;
        let currencyCode = 'IDR';

        if(this.state.activeAction === 'buy'){
            if(currency != currencyCode){
                this.setState({
                    currencyUsed: virtualCurrencyCode,
                    fixed_side: 'buy',
                    // amount: 0
                });
            } else {
                this.setState({
                    currencyUsed: currencyCode,
                    fixed_side: 'sell',
                    // amount: 0
                });
            }
        } else {
            if(currency != currencyCode){
                this.setState({
                    currencyUsed: virtualCurrencyCode,
                    fixed_side: 'sell',
                    // amount: 0
                });
            } else {
                this.setState({
                    currencyUsed: currencyCode,
                    fixed_side: 'buy',
                    // amount: 0
                });
            }
        }
    }

    setCurrency(currency){

        this.resetInput();

        let rate = 0;

        let rateSide = 'customer_'+this.state.activeAction;

        if (currency === 'BTC'){
            rate = this.state.BTCRate[rateSide]
        } else if (currency === 'ETH') {
            rate = this.state.ETHRate[rateSide]
        } else if (currency === 'XRP') {
            rate = this.state.XRPRate[rateSide]
        } else if (currency === 'DGX') {
            rate = this.state.DGXRate[rateSide]
        } else if (currency === 'TTC') {
            rate = this.state.TTCRate[rateSide]
        }  else if (currency === 'GUSD') {
            rate = this.state.GUSDRate[rateSide]
        }  else if (currency === 'PAX') {
            rate = this.state.PAXRate[rateSide]
        }  else if (currency === 'TUSD') {
            rate = this.state.TUSDRate[rateSide]
        }  else if (currency === 'USDT') {
            rate = this.state.USDTRate[rateSide]
        }  else if (currency === 'USDC') {
            rate = this.state.USDCRate[rateSide]
        }

        this.setState({
            activeCurrency: currency,
            rate
        })

        setTimeout(() => {
            this.conversionBuy();
        },100)
    }

    setAction(action) {

        this.resetInput();

        this.setState({
            activeAction: action
        });
        setTimeout(() => {
            this.setCurrency(this.state.activeCurrency);
        },100)
    }

    getTradingLimit() {
        homeActions.getTradingLimit(this.getToken()).then(res => {
            this.setState({
                dailyTradingLimit: res.data.data.maxLimit,
                currentUsage: res.data.data.currentLimit,
                currentUsagePercentage: ( res.data.data.currentLimit / res.data.data.maxLimit) * 100
            });
        }).catch(err => {
            NetworkError(err)
            
        });
    }

    onConfirmation(){
        this.setState({
            confirmation:true
        });
        let width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

        if(width < 500){
            this.confirm.scrollIntoView({ behavior: "smooth" });
        }
    }

    onCancellation(){
        this.setState({
            confirmation:false
        });
        let width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

        if(width < 500){
            this.middle.scrollIntoView({ behavior: "smooth" });
        }
    }

    onKeyDownIDR(e) {
	    let char = String.fromCharCode(e.keyCode);
	    let isDigit = /\d/g.test(char)

    	

    	if(!isDigit && e.keyCode !== 37 && e.keyCode !== 38 && e.keyCode !== 39 && e.keyCode !== 40 && e.keyCode !== 8) {
    		e.preventDefault();
	    }
    }

    selectPrecentageAmount(precentage){

        if(this.state.completedRecipte == false){
            let precentageAmount = ""

            if  (this.state.activeAction == 'buy') {
                this.switchSideTo('IDR');
                precentageAmount = Math.floor((precentage / 100) * this.state.IDRAccount.Balance)

                precentageAmount = precentageAmount.toString();

                this.setState({
                    amount:precentageAmount
                })

            } else if (this.state.activeAction == 'sell'){
                this.switchSideTo(this.state.activeCurrency)

                if(this.state.activeCurrency === 'BTC'){
                    precentageAmount = cryptoFormatter((precentage / 100) * this.state.BTCAccount.Balance)
                } else if (this.state.activeCurrency === 'ETH'){
                    precentageAmount = cryptoFormatter((precentage / 100) * this.state.ETHAccount.Balance)
                } else if (this.state.activeCurrency === 'XRP'){
                    precentageAmount = cryptoFormatter((precentage / 100) * this.state.XRPAccount.Balance)
                } else if (this.state.activeCurrency === 'DGX'){
                    precentageAmount = cryptoFormatter((precentage / 100) * this.state.DGXAccount.Balance)
                } else if (this.state.activeCurrency === 'TTC'){
                    precentageAmount = cryptoFormatter((precentage / 100) * this.state.TTCAccount.Balance)
                } else if (this.state.activeCurrency === 'GUSD'){
                    precentageAmount = cryptoFormatter((precentage / 100) * this.state.GUSDAccount.Balance)
                } else if (this.state.activeCurrency === 'PAX'){
                    precentageAmount = cryptoFormatter((precentage / 100) * this.state.PAXAccount.Balance)
                } else if (this.state.activeCurrency === 'TUSD'){
                    precentageAmount = cryptoFormatter((precentage / 100) * this.state.TUSDAccount.Balance)
                } else if (this.state.activeCurrency === 'USDT'){
                    precentageAmount = cryptoFormatter((precentage / 100) * this.state.USDTAccount.Balance)
                } else if (this.state.activeCurrency === 'USDC'){
                    precentageAmount = cryptoFormatter((precentage / 100) * this.state.USDCAccount.Balance)
                }

                precentageAmount = precentageAmount.toString();

                this.setState({
                    cryptoAmount:precentageAmount
                })

            }
        }else{
            this.resetInput();
        }

    }

    userReferralDiscountRate() {
        referralActions.referralUserDiscount(this.getToken()).then(res => {

            let currentDiscount = 0;
            let totalDiscount = 0;
            let userDiscount = 0;

            if(!_.isEmpty(res.data.data)){

                res.data.data.forEach(u => {
                    currentDiscount = u.DiscountValue.percentage

                    totalDiscount += Number(currentDiscount);
                })

                userDiscount = NP.times(totalDiscount, 100)

                if(userDiscount >= 100){
                    this.setState({userDiscount: 100})
                }else {
                    this.setState({userDiscount: userDiscount})
                }
            }

        }).catch(err => {
            NetworkError(err);
        });
    }

    componentDidMount() {
        this.fetchRates();
        this.conversionBuy();
        this.loadFromURL();
        this.getTradingLimit();
        this.userReferralDiscountRate();
        window.scrollTo(0, 0);
        if(!this.autoFetchRates){
            this.autoFetchRates = setInterval(() => {
                this.fetchRates();
            }, 1000 * 10);

	        if(window.intervals){
		        window.intervals.push(this.autoFetchRates)
	        } else {
		        window.intervals = [this.autoFetchRates]
	        }
        }
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    componentWillUnmount() {
	    window.intervals.map((i, index) => {
		    clearInterval(i)
		    window.intervals.splice(index, 1);
	    });
    }

    render() {

        let deductPrice = 0;
        let deductPriceCrypto = 0;
        let finalAmount = 0;
        let BTCRate = this.state.BTCRate.customer_buy || 0;
        let ETHRate = this.state.ETHRate.customer_buy || 0;
        let XRPRate = this.state.XRPRate.customer_buy || 0;
        let DGXRate = this.state.DGXRate.customer_buy || 0;
        let TTCRate = this.state.TTCRate.customer_buy || 0;
        let GUSDRate = this.state.GUSDRate.customer_buy || 0;
        let PAXRate = this.state.PAXRate.customer_buy || 0;
        let TUSDRate = this.state.TUSDRate.customer_buy || 0;
        let USDTRate = this.state.USDTRate.customer_buy || 0;
        let USDCRate = this.state.USDCRate.customer_buy || 0;

        let BTCRateSell = this.state.BTCRate.customer_sell || 0;
        let ETHRateSell = this.state.ETHRate.customer_sell || 0;
        let XRPRateSell = this.state.XRPRate.customer_sell || 0;
        let DGXRateSell = this.state.DGXRate.customer_sell || 0;
        let TTCRateSell = this.state.TTCRate.customer_sell || 0;
        let GUSDRateSell = this.state.GUSDRate.customer_sell || 0;
        let PAXRateSell = this.state.PAXRate.customer_sell || 0;
        let TUSDRateSell = this.state.TUSDRate.customer_sell || 0;
        let USDTRateSell = this.state.USDTRate.customer_sell || 0;
        let USDCRateSell = this.state.USDCRate.customer_sell || 0;

        let BTCBalance = this.state.BTCAccount.Balance || 0;
        let ETHBalance = this.state.ETHAccount.Balance || 0;
        let XRPBalance = this.state.XRPAccount.Balance || 0;
        let DGXBalance = this.state.DGXAccount.Balance || 0;
        let TTCBalance = this.state.TTCAccount.Balance || 0;
        let IDRBalance = this.state.IDRAccount.Balance || 0;
        let GUSDBalance = this.state.GUSDAccount.Balance || 0;
        let PAXBalance = this.state.PAXAccount.Balance || 0;
        let TUSDBalance = this.state.TUSDAccount.Balance || 0;
        let USDTBalance = this.state.USDTAccount.Balance || 0;
        let USDCBalance = this.state.USDCAccount.Balance || 0;

        // if (this.state.currencyUsed == 'BTC' || this.state.currencyUsed == 'ETH') {
        //     this.state.finalFee = (this.state.amount * this.state.rate) * (this.state.fee) / 100
        // } else {
        //     this.state.finalFee = (this.state.cryptoAmount) * (this.state.fee) / 100
        // }

        let cleanAmount = this.state.amount

        if (typeof (cleanAmount) != 'number') {
            cleanAmount = parseFloat(cleanAmount.replace(/\./g, ''));
        }

        if(this.state.currencyUsed != 'IDR'){
            this.state.finalTotal = Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount ) * this.state.rate);
            this.state.finalFee = (this.state.finalTotal * this.state.fee) / 100;
            this.state.finalIDR = this.state.finalTotal - this.state.finalFee;
        } else if (this.state.currencyUsed === 'IDR'){
            this.state.finalFee = (cleanAmount * this.state.fee) / 100;
            this.state.finalTotal = Number(this.state.amount - this.state.finalFee);
            this.state.finalIDR = amountFormat(cleanAmount);
        }

        // if (this.state.currencyUsed === 'IDR'){
        //     this.state.finalTotal = Number(this.state.amount) - Number(this.state.finalFee);
        // } else {
        //     this.state.finalTotal = Number(this.state.cryptoAmount * this.state.rate) - Number(this.state.finalFee);
        // }

        let price = this.state.amount - this.state.finalFee
        // let recieved = price / Number(this.state.rate);

        let idrFee = (this.state.cryptoAmount) * (this.state.fee) / 100

        let cryptoFee = (this.state.amount * this.state.rate) * (this.state.fee) / 100

        // deduct price on real conversion
        if(this.state.activeAction === 'buy') {
            deductPrice = this.state.finalFee || 0;
            deductPriceCrypto = Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount )) - (this.state.finalFee / this.state.rate);
        } else {
            deductPrice = this.state.finalFee || 0;
        }

        let discountAmount = (this.state.finalFee * this.state.userDiscount) / 100

        if(this.state.currencyUsed === 'IDR'){
            finalAmount = Number(this.state.amount - (this.state.finalFee - discountAmount)) / this.state.rate;
        } else {
            finalAmount = Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount ) );
        }

        if(isNaN( finalAmount )){
            finalAmount = 0
        }

        this.state.finalAmount = cryptoFormatterByCoin( finalAmount, this.state.activeCurrency );

        // if(this.state.currencyUsed === 'IDR'){
        //     this.state.finalRecived = price / Number(this.state.rate);
        // } else {
        //     this.state.finalIDR = Number(this.state.finalAmount * this.state.rate);
        // }

        if(isNaN(cryptoFormatter(this.state.finalRecived))){
            this.state.finalRecived = 0
        }

        let cryptoMinTrade = 1
        let cryptoBalance = 0

        let convertedAmount = 0;

        if(this.state.activeCurrency == 'BTC'){
            cryptoMinTrade = this.state.btcMinTrade
            cryptoBalance = BTCBalance
        } else if (this.state.activeCurrency == 'ETH'){
            cryptoMinTrade = this.state.ethMinTrade
            cryptoBalance = ETHBalance
        } else if (this.state.activeCurrency == 'XRP'){
            cryptoMinTrade = this.state.xrpMinTrade
            cryptoBalance = XRPBalance
        } else if (this.state.activeCurrency == 'DGX'){
            cryptoMinTrade = this.state.dgxMinTrade
            cryptoBalance = DGXBalance
        } else if (this.state.activeCurrency == 'TTC'){
            cryptoMinTrade = this.state.ttcMinTrade
            cryptoBalance = TTCBalance
        } else if (this.state.activeCurrency == 'GUSD'){
            cryptoMinTrade = this.state.gusdMinTrade
            cryptoBalance = GUSDBalance
        } else if (this.state.activeCurrency == 'PAX'){
            cryptoMinTrade = this.state.paxMinTrade
            cryptoBalance = PAXBalance
        } else if (this.state.activeCurrency == 'TUSD'){
            cryptoMinTrade = this.state.tusdMinTrade
            cryptoBalance = TUSDBalance
        } else if (this.state.activeCurrency == 'USDT'){
            cryptoMinTrade = this.state.usdtMinTrade
            cryptoBalance = USDTBalance
        } else if (this.state.activeCurrency == 'USDC'){
            cryptoMinTrade = this.state.usdcMinTrade
            cryptoBalance = USDCBalance
        }

        let buttonDisabled = true;
        let insufficientBalance = false;
        let limitReached = false;

        if(this.state.activeAction == 'buy'){
            buttonDisabled = (this.state.amount < this.state.idrMinTrade) || (this.state.amount > (this.state.dailyTradingLimit - this.state.currentUsage))
            limitReached = (this.state.amount > (this.state.dailyTradingLimit - this.state.currentUsage))
            if(this.state.amount > IDRBalance){
                insufficientBalance = true
                buttonDisabled = true
            }
        } else if (this.state.activeAction == 'sell') {

            if(this.state.activeCurrency === 'BTC') {
                convertedAmount = BTCRate * Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount ) )
            } else if(this.state.activeCurrency === 'ETH') {
                convertedAmount = ETHRate * Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount ) )
            } else if(this.state.activeCurrency === 'XRP') {
                convertedAmount = XRPRate * Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount ) )
            } else if(this.state.activeCurrency === 'DGX') {
                convertedAmount = DGXRate * Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount ) )
            } else if(this.state.activeCurrency === 'TTC') {
                convertedAmount = TTCRate * Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount ) )
            } else if(this.state.activeCurrency === 'GUSD') {
                convertedAmount = GUSDRate * Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount ) )
            } else if(this.state.activeCurrency === 'PAX') {
                convertedAmount = PAXRate * Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount ) )
            } else if(this.state.activeCurrency === 'TUSD') {
                convertedAmount = TUSDRate * Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount ) )
            } else if(this.state.activeCurrency === 'USDT') {
                convertedAmount = USDTRate * Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount ) )
            } else if(this.state.activeCurrency === 'USDC') {
                convertedAmount = USDCRate * Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount ) )
            }

            buttonDisabled = ( Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount )) < cryptoMinTrade) || (convertedAmount > (this.state.dailyTradingLimit - this.state.currentUsage))
            limitReached = (convertedAmount > (this.state.dailyTradingLimit - this.state.currentUsage))
            if ( Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount )) > cryptoBalance) {
                insufficientBalance = true
                buttonDisabled = true
            }
        } else {
            insufficientBalance = false
        }

        let finaDiscount = this.state.finalFee - discountAmount

        this.state.saveDiscount = Number(this.state.saveDiscount)

        return (
            <div className="m-grid m-grid--hor m-grid--root m-page">
                <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body" style={{ backgroundColor: "#f4f7fa"}}>
			    <div className="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container" style={{ height: window.outerHeight < 1700 && window.outerWidth < 500 ? this.state.mobileHeight : window.outerHeight * 1.20}}>
				<div className="m-grid__item m-grid__item--fluid m-wrapper">

                    <KYCMessage status={this.props.kyc.status}/>
                    <br />

                        <div className="row">

                            <div className={classnames({'col-lg-6 buy-sell-main-container': true, 'hide': this.state.confirmation == true})}>
                                <div className="m-portlet m-portlet--tabs  ">
                                    <div className="padding15 col-md-12" style={{paddingTop:"15px",paddingBottom:"15px"}}>
                                        <h6 className="m-subheader__title buy-sell-headingh6" style={{ zIndex: 1}}>
                                            {strings.AVAILABLE_BALANCES}
                                        </h6>

                                        <div className="balances-container-main">
                                            <div className="row width100">
                                                <div className="col-md-12 balances-container">
                                                    <div className="col-md-6 balances-container-boxes mtb25">
                                                        <div>
                                                            <img alt="" src="assets/media/img/btc-wallet.png" style={{ width: "50px" }} />
                                                        </div>
                                                        { this.state.activeCurrency === 'BTC'?
                                                            <div className="balances-container-boxer-balances">
                                                                <span style={{ color: "#575962" }}>Bitcoin</span>
                                                                <br />
                                                                <span style={{ color: "#9b9ca2" }}>{ cryptoFormatterByCoin(BTCBalance, 'BTC')} BTC</span>
                                                            </div> : null
                                                        }
                                                        { this.state.activeCurrency === 'ETH'?
                                                            <div className="balances-container-boxer-balances">
                                                                <span style={{ color: "#575962" }}>Ethereum</span>
                                                                <br />
                                                                <span style={{ color: "#9b9ca2" }}>{ cryptoFormatterByCoin(ETHBalance, 'ETH')} ETH</span>
                                                            </div> : null
                                                        }
                                                        { this.state.activeCurrency === 'XRP'?
                                                            <div className="balances-container-boxer-balances">
                                                                <span style={{ color: "#575962" }}>Ripple</span>
                                                                <br />
                                                                <span style={{ color: "#9b9ca2" }}>{ cryptoFormatterByCoin(XRPBalance, 'XRP')} XRP</span>
                                                            </div> : null
                                                        }
                                                        { this.state.activeCurrency === 'DGX'?
                                                            <div className="balances-container-boxer-balances">
                                                                <span style={{ color: "#575962" }}>DGX</span>
                                                                <br />
                                                                <span style={{ color: "#9b9ca2" }}>{ cryptoFormatterByCoin(DGXBalance, 'DGX')} DGX</span>
                                                            </div> : null
                                                        }
                                                        { this.state.activeCurrency === 'TTC'?
                                                            <div className="balances-container-boxer-balances">
                                                                <span style={{ color: "#575962" }}>TTC</span>
                                                                <br />
                                                                <span style={{ color: "#9b9ca2" }}>{ cryptoFormatterByCoin(TTCBalance, 'TTC')} TTC</span>
                                                            </div> : null
                                                        }
                                                        { this.state.activeCurrency === 'GUSD'?
                                                            <div className="balances-container-boxer-balances">
                                                                <span style={{ color: "#575962" }}>GUSD</span>
                                                                <br />
                                                                <span style={{ color: "#9b9ca2" }}>{ cryptoFormatterByCoin(GUSDBalance, 'GUSD')} GUSD</span>
                                                            </div> : null
                                                        }
                                                        { this.state.activeCurrency === 'PAX'?
                                                            <div className="balances-container-boxer-balances">
                                                                <span style={{ color: "#575962" }}>PAX</span>
                                                                <br />
                                                                <span style={{ color: "#9b9ca2" }}>{ cryptoFormatterByCoin(PAXBalance, 'PAX')} PAX</span>
                                                            </div> : null
                                                        }
                                                        { this.state.activeCurrency === 'TUSD'?
                                                            <div className="balances-container-boxer-balances">
                                                                <span style={{ color: "#575962" }}>TUSD</span>
                                                                <br />
                                                                <span style={{ color: "#9b9ca2" }}>{ cryptoFormatterByCoin(TUSDBalance, 'TUSD')} TUSD</span>
                                                            </div> : null
                                                        }
                                                        { this.state.activeCurrency === 'USDT'?
                                                            <div className="balances-container-boxer-balances">
                                                                <span style={{ color: "#575962" }}>USDT</span>
                                                                <br />
                                                                <span style={{ color: "#9b9ca2" }}>{ cryptoFormatterByCoin(USDTBalance, 'USDT')} USDT</span>
                                                            </div> : null
                                                        }
                                                        { this.state.activeCurrency === 'USDC'?
                                                            <div className="balances-container-boxer-balances">
                                                                <span style={{ color: "#575962" }}>USDC</span>
                                                                <br />
                                                                <span style={{ color: "#9b9ca2" }}>{ cryptoFormatterByCoin(USDCBalance, 'USDC')} USDC</span>
                                                            </div> : null
                                                        }
                                                    </div>
                                                    <div className="col-md-6 balances-container-boxes">
                                                        <div>
                                                            <img alt="" src="assets/media/img/logo/indonesia.png" style={{ width: "50px" }} />
                                                        </div>
                                                        <div className="balances-container-boxer-balances">
                                                            <span style={{ color: "#575962"}}>Rupiah</span>
                                                            <br />
                                                            <span style={{ color: "#9b9ca2" }}>{ currencyFormatter( amountFormat(IDRBalance) ) } </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="m-portlet m-portlet--tabs  ">
                                    <div className="m-portlet__head" style={{paddingTop:"20px",paddingBottom:"20px"}}>
                                        <div className="m-portlet__head-tools col-md12">
                                            <ul className="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary width100" role="tablist">
                                                <li className="nav-item m-tabs__item col-md-6 nomargins" onClick={() => this.setAction('buy')}>
                                                    <a className={classnames({'nav-link m-tabs__link buy-sell-wallet': true,'active-buysell-select': this.state.activeAction === 'buy', 'nav-tabs-borders': this.state.activeAction != 'buy'})} data-toggle="tab" >
                                                        <i className="flaticon-share m--hide"></i>
                                                        {strings.BUY}
                                                    </a>
                                                </li>
                                                <li className="nav-item m-tabs__item col-md-6 nomargins" onClick={() => this.setAction('sell')}>
                                                    <a className={classnames({'nav-link m-tabs__link buy-sell-wallet': true,'active-buysell-select': this.state.activeAction === 'sell', 'nav-tabs-borders': this.state.activeAction != 'sell'})} data-toggle="tab">
                                                        {strings.SELL}
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div className="tab-content">
                                        <div className="tab-pane active" id="m_user_profile_tab_1">
                                            <div className="" style={{ marginTop: "30px", zIndex: 20, position: 'relative' }}>
                                                <div className="m-portlet__head-tools col-md12">
                                                    <ul className="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary buy-sell-wallet-tabs" role="tablist" style={{ borderBottom: "none" }}>
                                                        <li className="mobile-mgbt nav-item m-tabs__item nomargins" onClick={() => this.setCurrency('BTC')}>
                                                            <a className="nav-link m-tabs__link buy-sell-wallet active-bitcoins-select"
                                                                className={classnames({'nav-link m-tabs__link buy-sell-wallet': true,'active-bitcoins-select': this.state.activeCurrency === 'BTC', 'nav-tabs-borders': this.state.activeCurrency != 'BTC'})} data-toggle="tab">
                                                                <i className="cc BTC bitcoin-color" style={{ fontSize:'20px' }}></i>
                                                                <span>BTC</span>
                                                                {this.state.activeAction == "buy" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(BTCRate) ) }
                                                                </span> : null }
                                                                {this.state.activeAction == "sell" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(BTCRateSell) ) }
                                                                </span> : null }

                                                            </a>
                                                        </li>
                                                        <li className="mobile-mgbt nav-item m-tabs__item nomargins" onClick={() => this.setCurrency('ETH')}>
                                                            <a className="nav-link m-tabs__link buy-sell-wallet"
                                                                className={classnames({'nav-link m-tabs__link buy-sell-wallet': true,'active-bitcoins-select': this.state.activeCurrency === 'ETH', 'nav-tabs-borders': this.state.activeCurrency != 'ETH'})} data-toggle="tab" >
                                                                <i className="cc ETH-alt etherium-color" style={{ fontSize:'20px' }}></i>
                                                                <span>ETH</span>
                                                                {this.state.activeAction == "buy" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(ETHRate) ) }
                                                                </span> : null }
                                                                {this.state.activeAction == "sell" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(ETHRateSell) ) }
                                                                </span> : null }
                                                            </a>
                                                        </li>
                                                        <li className="mobile-mgbt nav-item m-tabs__item nomargins" onClick={() => this.setCurrency('XRP')}>
                                                            <a className="nav-link m-tabs__link buy-sell-wallet"
                                                                className={classnames({'nav-link m-tabs__link buy-sell-wallet': true,'active-bitcoins-select': this.state.activeCurrency === 'XRP', 'nav-tabs-borders': this.state.activeCurrency != 'XRP'})} data-toggle="tab" >
                                                                <i className="cc XRP-alt" style={{ color:"#0099d2", fontSize:'20px' }}></i>
                                                                <span>XRP</span>
                                                                {this.state.activeAction == "buy" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(XRPRate) ) }
                                                                </span> : null }
                                                                {this.state.activeAction == "sell" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(XRPRateSell) ) }
                                                                </span> : null }
                                                            </a>
                                                        </li>
                                                        <li className="mobile-mgbt nav-item m-tabs__item nomargins" onClick={() => this.setCurrency('DGX')}>
                                                            <a className="nav-link m-tabs__link buy-sell-wallet"
                                                                className={classnames({'nav-link m-tabs__link buy-sell-wallet': true,'active-bitcoins-select': this.state.activeCurrency === 'DGX', 'nav-tabs-borders': this.state.activeCurrency != 'DGX'})} data-toggle="tab" >
                                                                {/* <i className="cc DGX" style={{ color:"#d8a24a", fontSize:'20px' }}></i> */}
                                                                <img style={{ width: "20px"}} src="assets/media/img/logo/dgx_token.png" alt="" className="custom-icons-nav-tabs-img" />
                                                                <span>DGX</span>
                                                                {this.state.activeAction == "buy" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(DGXRate) ) }
                                                                </span> : null }
                                                                {this.state.activeAction == "sell" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(DGXRateSell) ) }
                                                                </span> : null }
                                                            </a>
                                                        </li>
                                                        <li className="mobile-mgbt nav-item m-tabs__item nomargins" onClick={() => this.setCurrency('TTC')}>
                                                            <a className="nav-link m-tabs__link buy-sell-wallet"
                                                                className={classnames({'nav-link m-tabs__link buy-sell-wallet': true,'active-bitcoins-select': this.state.activeCurrency === 'TTC', 'nav-tabs-borders': this.state.activeCurrency != 'TTC'})} data-toggle="tab" style={{marginTop: '0px'}}>
                                                                {/* <i className="cc DGX" style={{ color:"#d8a24a", fontSize:'20px' }}></i> */}
                                                                <img style={{ width: "20px"}} src="assets/media/img/logo/ttc.png" alt="" className="custom-icons-nav-tabs-img" />
                                                                <span>TTC</span>
                                                                {this.state.activeAction == "buy" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(TTCRate) ) }
                                                                </span> : null }
                                                                {this.state.activeAction == "sell" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(TTCRateSell) ) }
                                                                </span> : null }
                                                            </a>
                                                        </li>
                                                        <li className="mobile-mgbt nav-item m-tabs__item nomargins" onClick={() => this.setCurrency('GUSD')}>
                                                            <a className="nav-link m-tabs__link buy-sell-wallet"
                                                                className={classnames({'nav-link m-tabs__link buy-sell-wallet': true,'active-bitcoins-select': this.state.activeCurrency === 'GUSD', 'nav-tabs-borders': this.state.activeCurrency != 'GUSD'})} data-toggle="tab" style={{marginTop: '15px'}}>
                                                                {/* <i className="cc DGX" style={{ color:"#d8a24a", fontSize:'20px' }}></i> */}
                                                                <img style={{ width: "20px"}} src="assets/media/img/logo/gemini.png" alt="" className="custom-icons-nav-tabs-img" />
                                                                <span>GUSD</span>
                                                                {this.state.activeAction == "buy" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(GUSDRate) ) }
                                                                </span> : null }
                                                                {this.state.activeAction == "sell" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(GUSDRateSell) ) }
                                                                </span> : null }
                                                            </a>
                                                        </li>
                                                        <li className="mobile-mgbt nav-item m-tabs__item nomargins" onClick={() => this.setCurrency('PAX')}>
                                                            <a className="nav-link m-tabs__link buy-sell-wallet"
                                                                className={classnames({'nav-link m-tabs__link buy-sell-wallet': true,'active-bitcoins-select': this.state.activeCurrency === 'PAX', 'nav-tabs-borders': this.state.activeCurrency != 'PAX'})} data-toggle="tab" style={{marginTop: '15px'}}>
                                                                {/* <i className="cc DGX" style={{ color:"#d8a24a", fontSize:'20px' }}></i> */}
                                                                <img style={{ width: "20px"}} src="assets/media/img/logo/pax.png" alt="" className="custom-icons-nav-tabs-img" />
                                                                <span>PAX</span>
                                                                {this.state.activeAction == "buy" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(PAXRate) ) }
                                                                </span> : null }
                                                                {this.state.activeAction == "sell" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(PAXRateSell) ) }
                                                                </span> : null }
                                                            </a>
                                                        </li>
                                                        <li className="mobile-mgbt nav-item m-tabs__item nomargins" onClick={() => this.setCurrency('TUSD')}>
                                                            <a className="nav-link m-tabs__link buy-sell-wallet"
                                                                className={classnames({'nav-link m-tabs__link buy-sell-wallet': true,'active-bitcoins-select': this.state.activeCurrency === 'TUSD', 'nav-tabs-borders': this.state.activeCurrency != 'TUSD'})} data-toggle="tab" style={{marginTop: '15px'}}>
                                                                {/* <i className="cc DGX" style={{ color:"#d8a24a", fontSize:'20px' }}></i> */}
                                                                <img style={{ width: "20px"}} src="assets/media/img/logo/tusd.png" alt="" className="custom-icons-nav-tabs-img" />
                                                                <span>TUSD</span>
                                                                {this.state.activeAction == "buy" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(TUSDRate) ) }
                                                                </span> : null }
                                                                {this.state.activeAction == "sell" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(TUSDRateSell) ) }
                                                                </span> : null }
                                                            </a>
                                                        </li>
                                                        <li className="mobile-mgbt nav-item m-tabs__item nomargins" onClick={() => this.setCurrency('USDT')}>
                                                            <a className="nav-link m-tabs__link buy-sell-wallet"
                                                                className={classnames({'nav-link m-tabs__link buy-sell-wallet': true,'active-bitcoins-select': this.state.activeCurrency === 'USDT', 'nav-tabs-borders': this.state.activeCurrency != 'USDT'})} data-toggle="tab" style={{marginTop: '15px'}}>
                                                                {/* <i className="cc DGX" style={{ color:"#d8a24a", fontSize:'20px' }}></i> */}
                                                                <img style={{ width: "20px"}} src="assets/media/img/logo/usdt.png" alt="" className="custom-icons-nav-tabs-img" />
                                                                <span>USDT</span>
                                                                {this.state.activeAction == "buy" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(USDTRate) ) }
                                                                </span> : null }
                                                                {this.state.activeAction == "sell" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(USDTRateSell) ) }
                                                                </span> : null }
                                                            </a>
                                                        </li>
                                                        <li className="mobile-mgbt nav-item m-tabs__item nomargins" onClick={() => this.setCurrency('USDC')}>
                                                            <a className="nav-link m-tabs__link buy-sell-wallet"
                                                                className={classnames({'nav-link m-tabs__link buy-sell-wallet': true,'active-bitcoins-select': this.state.activeCurrency === 'USDC', 'nav-tabs-borders': this.state.activeCurrency != 'USDC'})} data-toggle="tab" style={{marginTop: '15px'}}>
                                                                {/* <i className="cc DGX" style={{ color:"#d8a24a", fontSize:'20px' }}></i> */}
                                                                <img style={{ width: "20px"}} src="assets/media/img/logo/usdc.png" alt="" className="custom-icons-nav-tabs-img" />
                                                                <span>USDC</span>
                                                                {this.state.activeAction == "buy" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(USDCRate) ) }
                                                                </span> : null }
                                                                {this.state.activeAction == "sell" ?
                                                                    <span style={{ color: "#9b9ca2" }}> <br/> @ { currencyFormatter( amountFormat(USDCRateSell) ) }
                                                                </span> : null }
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div ref={(el) => { this.middle = el; }} className="padding15 col-md-12 mtp20">
                                                <h6 className="m-subheader__title buy-sell-headingh6 mtp20">
                                                    {strings.AMOUNT}
                                                </h6>
                                                <div className="m-widget24__item mb20" style={{ fontWeight: 600, color: "gray" }}>

                                                    <span className="m-widget24__desc">
                                                        {strings.REMANING_DAILY_TRADING_LIMIT}
                                                    </span>
                                                    <span className="m-widget24__stats" style={{ float: "right", color: limitReached? "red" : "black"}}>
                                                        { currencyFormatter( amountFormat(this.state.dailyTradingLimit - this.state.currentUsage) ) }
                                                    </span>
                                                    <div className="m--space-10"></div>
                                                    <div className="progress m-progress--sm">
                                                        <div className="progress-bar m-bg-green" role="progressbar" style={{ width: (100 - this.state.currentUsagePercentage)+"%"}} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span className="m-widget24__change" style={{ fontWeight: 400, fontSize: "12px" }}>
                                                        {strings.DAILY_TRADING_LIMIT}
                                                    </span>
                                                    <span className="m-widget24__number" style={{ float: "right", fontWeight: 400, fontSize: "12px" }}>
                                                        { currencyFormatter( amountFormat(this.state.dailyTradingLimit) ) }
                                                    </span>
                                                </div>

                                                <div className="row">
                                                    { this.state.activeAction === 'buy' ?
                                                        <div style={{width:"100%"}}>
                                                            <div className="col-md-12 input-group-converstion">
                                                                <div className="m-input-icon m-input-icon--right col-md-5">

                                                                    <input onKeyDown={(e) => this.onKeyDownIDR(e)} disabled={this.state.loading} onChange={this.handleChange('amount')} onFocus={this.state.completedRecipte == false ? () => this.switchSideTo('IDR') : () => this.resetInput()} value={this.state.currencyUsed === 'IDR'? this.state.amount : (this.state.amount - deductPrice).toFixed(0)} type="text" className="form-control m-input custom-buy-sell-input" />

                                                                    <span className="m-input-icon__icon m-input-icon__icon--right buy-sell-input-label">
                                                                        <span style={{ marginLeft: "-40px" }}>{currencyFormatter('')}</span>
                                                                    </span>
                                                                </div>
                                                                <div className="col-md-2" style={{ padding: "18px", textAlign: "center" }}>
                                                                    <FontAwesomeIcon style={{ fontSize: "24px", textAlign: "center", color: "#808080", fontWeight: "500" }}  icon={faExchangeAlt} className="fas fa-exchange-alt"/>
                                                                </div>
                                                                <div className="m-input-icon m-input-icon--right col-md-5">

                                                                    <input disabled={this.state.loading} onChange={this.handleChange('cryptoAmount')} disabled value={this.state.currencyUsed !== 'IDR'? this.state.cryptoAmount : this.state.finalAmount} type="text" className="form-control m-input custom-buy-sell-input" />

                                                                    <span className="m-input-icon__icon m-input-icon__icon--right buy-sell-input-label">
                                                                        <span style={{ marginLeft: "-53px"}}>{ this.state.activeCurrency.toUpperCase()}</span>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <div className={classnames({'mb20 buy-sell-precentage-container': true, 'buy-sell-precentage-button-disable': this.state.loading == true})}>

                                                                <a onClick={() => this.selectPrecentageAmount('25')}>25%</a>
                                                                <a onClick={() => this.selectPrecentageAmount('50')}>50%</a>
                                                                <a onClick={() => this.selectPrecentageAmount('75')}>75%</a>
                                                                <a onClick={() => this.selectPrecentageAmount('99.5')}>100%</a>

                                                            </div>

                                                            <div className="m-form__help" style={{fontSize:"12px",textAlign:"center",marginTop:"15px",marginBottom:"-15px",color: this.state.amount < this.state.idrMinTrade ? "red": "#7b7e8a" }}>{strings.MINIMUM_BUY_AMOUNT_IS} <strong>Rp{amountFormat(this.state.idrMinTrade)}</strong></div>

                                                            {insufficientBalance == true ?
                                                                <div className="m-form__help" style={{fontSize:"12px",textAlign:"center",marginTop:"20px",marginBottom:"-15px",color: "red"}}>{strings.INSUFFCIENT_ACCOUNT_BALANCE}</div>:null
                                                            }

                                                        </div>
                                                        :
                                                        <div style={{width:"100%"}}>
                                                            <div className="col-md-12 input-group-converstion">
                                                                <div className="m-input-icon m-input-icon--right col-md-5">

                                                                    <input disabled={this.state.loading} onChange={this.handleChange('cryptoAmount')} onFocus={this.state.completedRecipte == false ? () => this.switchSideTo(this.state.activeCurrency) : () => this.resetInput()} value={this.state.currencyUsed !== 'IDR'? this.state.cryptoAmount : this.state.finalAmount} type="text" className="form-control m-input custom-buy-sell-input" />

                                                                    <span className="m-input-icon__icon m-input-icon__icon--right buy-sell-input-label">
                                                                        <span style={{ marginLeft: "-53px"}}>{ this.state.activeCurrency.toUpperCase()}</span>
                                                                    </span>
                                                                </div>
                                                                <div className="col-md-2" style={{ padding: "18px", textAlign: "center" }}>
                                                                    <FontAwesomeIcon style={{ fontSize: "24px", textAlign: "center", color: "#808080", fontWeight: "500" }}  icon={faExchangeAlt} className="fas fa-exchange-alt"/>
                                                                </div>
                                                                <div className="m-input-icon m-input-icon--right col-md-5">

                                                                    <input onKeyDown={(e) => this.onKeyDownIDR(e)} disabled onChange={this.handleChange('amount')} value={this.state.currencyUsed === 'IDR'? this.state.amount : (this.state.finalIDR).toFixed(0)} type="text" className="form-control m-input custom-buy-sell-input" />

                                                                    <span className="m-input-icon__icon m-input-icon__icon--right buy-sell-input-label">
                                                                        <span style={{ marginLeft: "-40px" }}> {currencyFormatter('')} </span>
                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <div className={classnames({'mb20 buy-sell-precentage-container': true, 'buy-sell-precentage-button-disable': this.state.loading == true})}>

                                                                <a onClick={() => this.selectPrecentageAmount('25')}>25%</a>
                                                                <a onClick={() => this.selectPrecentageAmount('50')}>50%</a>
                                                                <a onClick={() => this.selectPrecentageAmount('75')}>75%</a>
                                                                <a onClick={() => this.selectPrecentageAmount('100')}>100%</a>

                                                            </div>

                                                            <div className="m-form__help" style={{fontSize:"12px",textAlign:"center",marginTop:"15px",marginBottom:"-15px",color: Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount )) < cryptoMinTrade ? "red": "#7b7e8a" }}>{strings.MINIMUM_SELL_AMOUNT_IS} <strong>{this.state.activeCurrency} {cryptoMinTrade}</strong></div>

                                                            {insufficientBalance == true ?
                                                                <div className="m-form__help" style={{fontSize:"12px",textAlign:"center",marginTop:"20px",marginBottom:"-15px",color: "red"}}>{strings.INSUFFCIENT_ACCOUNT_BALANCE}</div>:null
                                                            }
                                                        </div>
                                                    }
                                                    <div className="col-md-12">
                                                        { this.state.activeAction === 'buy'?
                                                            <div>
                                                            { !this.state.loading &&
                                                                <div>
                                                                    <button onClick={() => this.onConfirmation()} type="button" disabled={buttonDisabled} className="btn btn-green btn-block button-padding15 buy-sell-buy-button">{strings.BUY} { this.state.activeCurrency } {strings.INSTANTLY} - { currencyFormatter( amountFormat(this.state.amount) )}</button>
                                                                </div>
                                                            }
                                                            </div> :
                                                            <div>
                                                            { !this.state.loading &&
                                                                <div>

                                                                    <button onClick={() => this.onConfirmation()} type="button" disabled={buttonDisabled} className="btn btn-green btn-block button-padding15 buy-sell-buy-button">{strings.SELL} { this.state.activeCurrency } {strings.INSTANTLY} - {this.state.activeCurrency} { cryptoFormatterByCoin( Number( cryptoFormatterReplaceFormat( this.state.cryptoAmount)), this.state.activeCurrency )}</button>

                                                                </div>
                                                            }
                                                            </div>
                                                        }
                                                        <br />
                                                        {/* <Loader type="line-scale" active={this.state.loading} className="text-center" /> */}
                                                        {this.state.loading == true ?
                                                            <span style={{display:"block", textAlign:"center"}} >
                                                                <Loader type="line-scale" active={this.state.loading} className="text-center" />
                                                            </span>
                                                        : null }

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div className="tab-pane active" id="m_user_profile_tab_2"></div>
                                    </div>
                                </div>
                            </div>
                            {this.state.loadingRecipte == false?
                                <div ref={(el) => { this.receipt = el; }} className={classnames({'col-md-4 buy-sell-receipt-container': true, 'move': this.state.confirmation == true})} style={{margin:"auto",backgroundImage: 'url(assets/media/img/bg/receipt_tokobg.svg)' ,backgroundPositionY:"100%", backgroundRepeat: 'no-repeat', backgroundSize:'cover'}}>
                                    {this.state.userDiscount > 0 ?
                                        <span>
                                            {this.state.completedRecipte == false ?
                                                <div><img alt="" className="buy-sell-receipt-ribbon" src="assets/media/img/receipt_ribbon.svg"/><span className="buy-sell-receipt-discount">{this.state.userDiscount}% <br/>OFF</span></div> :
                                                <div><img alt="" className="buy-sell-receipt-ribbon" src="assets/media/img/receipt_ribbon_green.svg"/><span className="buy-sell-receipt-discount">{this.state.userDiscount}% <br/>OFF</span></div>
                                            }
                                        </span>:
                                        <span>
                                            {this.state.completedRecipte == false ?
                                                <img alt="" className="buy-sell-receipt-ribbon" src="assets/media/img/receipt_ribbon.svg"/>:
                                                <img alt="" className="buy-sell-receipt-ribbon" src="assets/media/img/receipt_ribbon_green.svg"/>
                                            }
                                        </span>
                                    }
                                    <div className="">
                                        <div className="buy-sell-ticket-header">
                                            { this.state.activeAction === 'buy'?
                                                <div className={classnames({'buy-sell-ticket-header-heading': true, 'm--font-green': this.state.completedRecipte == true})}>{this.state.completedRecipte == true ? <span>{strings.YOU_HAVE_BOUGHT}</span>: <span>{strings.YOU_ARE_BUYING}</span>}</div> :
                                                <div className={classnames({'buy-sell-ticket-header-heading': true, 'm--font-green': this.state.completedRecipte == true})}>{this.state.completedRecipte == true ? <span>{strings.YOU_HAVE_SOLD}</span>: <span>{strings.YOU_ARE_SELLING}</span>}</div>
                                            }

                                            <div className={classnames({'buy-sell-ticket-header-subheading': true, 'm--font-green': this.state.completedRecipte == true})}>{ this.state.completedRecipte == true ? cryptoFormatterByCoin(this.state.saveAmount, this.state.activeCurrency ) : this.state.finalAmount } {this.state.activeCurrency}</div>

                                            {this.state.activeCurrency === 'BTC'?
                                                <div className={classnames({'buy-sell-ticket-header-bottom': true, 'm--font-green': this.state.completedRecipte == true})}>@ {this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveRate) )  : currencyFormatter( amountFormat(this.state.rate) )} per BTC</div> :
                                                null
                                            }
                                            {this.state.activeCurrency === 'ETH'?
                                                <div className={classnames({'buy-sell-ticket-header-bottom': true, 'm--font-green': this.state.completedRecipte == true})}>@ {this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveRate) ) : currencyFormatter( amountFormat(this.state.rate) )} per ETH</div> :
                                                null
                                            }
                                            {this.state.activeCurrency === 'XRP'?
                                                <div className={classnames({'buy-sell-ticket-header-bottom': true, 'm--font-green': this.state.completedRecipte == true})}>@ {this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveRate) ) : currencyFormatter( amountFormat(this.state.rate) )} per XRP</div> :
                                                null
                                            }
                                            {this.state.activeCurrency === 'DGX'?
                                                <div className={classnames({'buy-sell-ticket-header-bottom': true, 'm--font-green': this.state.completedRecipte == true})}>@ {this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveRate) ) : currencyFormatter( amountFormat(this.state.rate) )} per DGX</div> :
                                                null
                                            } 
                                            {this.state.activeCurrency === 'TTC'?
                                                <div className={classnames({'buy-sell-ticket-header-bottom': true, 'm--font-green': this.state.completedRecipte == true})}>@ {this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveRate) ) : currencyFormatter( amountFormat(this.state.rate) )} per TTC</div> :
                                                null
                                            } 
                                            {this.state.activeCurrency === 'GUSD'?
                                                <div className={classnames({'buy-sell-ticket-header-bottom': true, 'm--font-green': this.state.completedRecipte == true})}>@ {this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveRate) ) : currencyFormatter( amountFormat(this.state.rate) )} per GUSD</div> :
                                                null
                                            }
                                             {this.state.activeCurrency === 'PAX'?
                                                <div className={classnames({'buy-sell-ticket-header-bottom': true, 'm--font-green': this.state.completedRecipte == true})}>@ {this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveRate) ) : currencyFormatter( amountFormat(this.state.rate) )} per PAX</div> :
                                                null
                                            }
                                             {this.state.activeCurrency === 'TUSD'?
                                                <div className={classnames({'buy-sell-ticket-header-bottom': true, 'm--font-green': this.state.completedRecipte == true})}>@ {this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveRate) ) : currencyFormatter( amountFormat(this.state.rate) )} per TUSD</div> :
                                                null
                                            }
                                             {this.state.activeCurrency === 'USDT'?
                                                <div className={classnames({'buy-sell-ticket-header-bottom': true, 'm--font-green': this.state.completedRecipte == true})}>@ {this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveRate) ) : currencyFormatter( amountFormat(this.state.rate) )} per USDT</div> :
                                                null
                                            }
                                             {this.state.activeCurrency === 'USDC'?
                                                <div className={classnames({'buy-sell-ticket-header-bottom': true, 'm--font-green': this.state.completedRecipte == true})}>@ {this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveRate) ) : currencyFormatter( amountFormat(this.state.rate) )} per USDC</div> :
                                                null
                                            } 
                                        </div>
                                        <div>
                                            <div className={classnames({'ticket-padding buy-sell-ticket-middle-details': true, 'm--font-green': this.state.completedRecipte == true})} style={{ marginTop: "3px" }}>
                                                <FontAwesomeIcon style={{ fontSize: "37px" }} icon={faCreditCard} className={classnames({'i fas fa-credit-card': true, 'm--font-green': this.state.completedRecipte == true})} />
                                                <span className={classnames({'buy-sell-middle-heading': true, 'm--font-green': this.state.completedRecipte == true})}>{strings.WITHDRAW_WALLET}</span>
                                                <br />
                                                {this.state.activeAction == 'buy' ?
                                                    <span className={classnames({'buy-sell-middle-subheading': true, 'm--font-green': this.state.completedRecipte == true})}>Rupiah</span> :
                                                    <span className={classnames({'buy-sell-middle-subheading': true, 'm--font-green': this.state.completedRecipte == true})}>{ this.state.activeCurrency } {strings.WALLET}</span>
                                                }
                                            </div>
                                            <div className={classnames({'ticket-padding buy-sell-ticket-middle-details': true, 'm--font-green': this.state.completedRecipte == true})} style={{ marginTop: "-14px" }}>
                                                <FontAwesomeIcon style={{ fontSize: "37px" }} icon={faFighterJet} className={classnames({'i fas fa-fighter-jet': true, 'm--font-green': this.state.completedRecipte == true})} />
                                                <span className={classnames({'buy-sell-middle-heading': true, 'm--font-green': this.state.completedRecipte == true})}>{strings.AVAILABLE}</span>
                                                <br />
                                                <span className={classnames({'buy-sell-middle-subheading': true, 'm--font-green': this.state.completedRecipte == true})}>{strings.INSTANTLY}</span>
                                            </div>
                                            <div className={classnames({'ticket-padding buy-sell-ticket-middle-details': true, 'm--font-green': this.state.completedRecipte == true})} style={{ marginTop: "-14px" }}>
                                                {this.state.activeCurrency === 'XRP'?
                                                    <i style={{ fontSize: "37px"}} className={classnames({'cc XRP-alt XRP-Wallet-icon': this.state.activeCurrency === 'XRP', 'm--font-green': this.state.completedRecipte == true})}></i>: null
                                                }
                                                {this.state.activeCurrency === 'BTC' || this.state.activeCurrency === 'ETH'?
                                                    <FontAwesomeIcon style={{ fontSize: "37px" }} icon={this.state.activeCurrency === 'ETH' ? faEthereum : faBtc} className={classnames({'i fab fa-ethereum': this.state.activeCurrency === 'ETH', 'i fab fa-btc': this.state.activeCurrency === 'BTC', 'm--font-green': this.state.completedRecipte == true})} /> : null
                                                }
                                                {this.state.activeCurrency === 'DGX'?
                                                    <i style={{ fontSize: "37px"}} className={classnames({'cc DGX DGX-Wallet-icon': this.state.activeCurrency === 'DGX', 'm--font-green': this.state.completedRecipte == true})}></i>: null
                                                }
                                                {this.state.activeCurrency === 'TTC'?
                                                    <i style={{ fontSize: "37px"}} className={classnames({'cc TTC DGX-Wallet-icon': this.state.activeCurrency === 'TTC', 'm--font-green': this.state.completedRecipte == true})}></i>: null
                                                }
                                                 {this.state.activeCurrency === 'GUSD'?
                                                    <i style={{ fontSize: "37px"}} className={classnames({'cc GUSD DGX-Wallet-icon': this.state.activeCurrency === 'GUSD', 'm--font-green': this.state.completedRecipte == true})}></i>: null
                                                }
                                                 {this.state.activeCurrency === 'PAX'?
                                                    <i style={{ fontSize: "37px"}} className={classnames({'cc PAX DGX-Wallet-icon': this.state.activeCurrency === 'PAX', 'm--font-green': this.state.completedRecipte == true})}></i>: null
                                                }
                                                 {this.state.activeCurrency === 'TUSD'?
                                                    <i style={{ fontSize: "37px"}} className={classnames({'cc TUSD DGX-Wallet-icon': this.state.activeCurrency === 'TUSD', 'm--font-green': this.state.completedRecipte == true})}></i>: null
                                                }
                                                 {this.state.activeCurrency === 'USDT'?
                                                    <i style={{ fontSize: "37px"}} className={classnames({'cc USDT DGX-Wallet-icon': this.state.activeCurrency === 'USDT', 'm--font-green': this.state.completedRecipte == true})}></i>: null
                                                }
                                                 {this.state.activeCurrency === 'USDC'?
                                                    <i style={{ fontSize: "37px"}} className={classnames({'cc USDC DGX-Wallet-icon': this.state.activeCurrency === 'USDC', 'm--font-green': this.state.completedRecipte == true})}></i>: null
                                                }
                                                <span className={classnames({'buy-sell-middle-heading': true, 'm--font-green': this.state.completedRecipte == true})}>{strings.DEPOSIT_WALLET}</span>
                                                <br />
                                                {this.state.activeAction == 'buy' ?
                                                    <span className={classnames({'buy-sell-middle-subheading': true, 'm--font-green': this.state.completedRecipte == true})}>{ this.state.activeCurrency } {strings.WALLET}</span> :
                                                    <span className={classnames({'buy-sell-middle-subheading': true, 'm--font-green': this.state.completedRecipte == true})}>Rupiah</span>
                                                }
                                            </div>
                                        </div>
                                        <div className={classnames({'mtb25 buy-sell-middle-details': true, 'm--font-green': this.state.completedRecipte == true})} style={{ marginBottom: "50px" }}>
                                            {this.state.userDiscount > 0?
                                                <span>
                                                    { this.state.currencyUsed !== 'IDR' ?
                                                    <div className={classnames({'ticket-padding buy-sell-middle-details-heading': true, 'm--font-green': this.state.completedRecipte == true})} style={{ paddingTop: "30px" }}>{ this.state.completedRecipte == true ? cryptoFormatterByCoin( this.state.saveAmount, this.state.activeCurrency ) : this.state.finalAmount} { this.state.activeCurrency }
                                                        <span style={{ float: "right"}}>{ this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveTotal) ) : currencyFormatter( amountFormat(this.state.finalTotal) ) }</span>
                                                    </div> :
                                                    <div className={classnames({'ticket-padding buy-sell-middle-details-heading': true, 'm--font-green': this.state.completedRecipte == true})} style={{ paddingTop: "30px" }}>{ this.state.completedRecipte == true ? cryptoFormatterByCoin( this.state.saveAmount, this.state.activeCurrency) : this.state.finalAmount} { this.state.activeCurrency }
                                                        <span style={{ float: "right"}}>{ this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveTotal) ) : currencyFormatter( amountFormat(this.state.finalTotal + discountAmount) ) }</span>
                                                    </div>
                                                    }
                                                </span>:
                                                <span>
                                                    { this.state.currencyUsed !== 'IDR' ?
                                                    <div className={classnames({'ticket-padding buy-sell-middle-details-heading': true, 'm--font-green': this.state.completedRecipte == true})} style={{ paddingTop: "30px" }}>{ this.state.completedRecipte == true ? cryptoFormatterByCoin( this.state.saveAmount, this.state.activeCurrency) : this.state.finalAmount} { this.state.activeCurrency }
                                                        <span style={{ float: "right"}}>{ this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveTotal) ) : currencyFormatter( amountFormat(this.state.finalTotal) ) }</span>
                                                    </div> :
                                                    <div className={classnames({'ticket-padding buy-sell-middle-details-heading': true, 'm--font-green': this.state.completedRecipte == true})} style={{ paddingTop: "30px" }}>{ this.state.completedRecipte == true ? this.state.saveAmount : this.state.finalAmount} { this.state.activeCurrency }
                                                        <span style={{ float: "right"}}>{ this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveTotal) ) : currencyFormatter( amountFormat(this.state.finalTotal) ) }</span>
                                                    </div>
                                                    }
                                                </span>
                                            }
                                            {this.state.userDiscount > 0 ?
                                                <span>
                                                    <div className={classnames({'ticket-padding buy-sell-middle-details-heading': true, 'm--font-green': this.state.completedRecipte == true})} style={{ paddingTop: "15px", paddingTop: "20px" }}>{strings.TOKOCRYPTO_FEE}
                                                        <span className="m--font-warning" style={{ float: "right", color: "#2980b9", marginRight:"-5px"}}>( { this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveFee + this.state.saveDiscount) ) : currencyFormatter( amountFormat(this.state.finalFee) ) } ) </span>
                                                    </div>
                                                    <div className={classnames({'ticket-padding buy-sell-middle-details-heading': true, 'm--font-green': this.state.completedRecipte == true})} style={{ paddingTop: "15px", paddingTop: "20px" }}>Discount ({this.state.userDiscount}%)
                                                        <span className="m--font-warning" style={{ float: "right", color: "#2980b9", marginRight:"-5px"}}>( - { this.state.completedRecipte == true ?  currencyFormatter( amountFormat(this.state.saveDiscount) ) : currencyFormatter( amountFormat(discountAmount) )})</span>
                                                    </div>
                                                </span>:
                                                <div className={classnames({'ticket-padding buy-sell-middle-details-heading': true, 'm--font-green': this.state.completedRecipte == true})} style={{ paddingTop: "15px", paddingTop: "20px" }}>{strings.TOKOCRYPTO_FEE}
                                                    <span className="m--font-warning" style={{ float: "right", color: "#2980b9", marginRight:"-5px"}}>( { this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveFee) ) : currencyFormatter( amountFormat(this.state.finalFee) )})</span>
                                                </div>
                                            }
                                            {this.state.userDiscount > 0 && this.state.currencyUsed !== 'IDR'?
                                                <div className={classnames({'ticket-padding buy-sell-middle-details-heading buy-sell-total': true, 'm--font-green': this.state.completedRecipte == true})} > {this.state.activeAction == 'sell' ? <span>{strings.IDR_RECEIVE}</span>  : <span>{strings.TOTAL_SPENT}</span> }
                                                    <span style={{ float: "right"}}>{ this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveIDR) ) : currencyFormatter( amountFormat(this.state.finalIDR + discountAmount) )}</span>
                                                </div>:
                                                <div className={classnames({'ticket-padding buy-sell-middle-details-heading buy-sell-total': true, 'm--font-green': this.state.completedRecipte == true})} > {this.state.activeAction == 'sell' ? <span>{strings.IDR_RECEIVE}</span>  : <span>{strings.TOTAL_SPENT}</span> }
                                                    <span style={{ float: "right"}}>{ this.state.completedRecipte == true ? currencyFormatter( amountFormat(this.state.saveIDR) ) : currencyFormatter( amountFormat(this.state.finalIDR) )}</span>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                </div>:
                                <div ref={(el) => { this.receipt = el; }} className={classnames({'col-md-4 buy-sell-receipt-container': true, 'move': this.state.confirmation == true})} style={{margin:"auto",backgroundImage: 'url(assets/media/img/bg/receipt_tokobg.svg)' ,backgroundPositionY:"100%", backgroundRepeat: 'no-repeat', backgroundSize:'cover'}}>
                                    <div className="">
                                        <div className="buy-sell-ticket-header" style={{borderBottom:"none"}}>
                                          
                                            <div className={classnames({'buy-sell-ticket-header-subheading': true, 'm--font-green': this.state.completedRecipte == true})} style={{marginTop:"35px"}}>Processing your order</div>

                                        </div>
                                        <div style={{textAlign:"center"}}>
                                            <Loader style={{textAlign:"center"}} type="line-scale" size={"80px"} className="text-center" />
                                            <div>Please wait</div>
                                        </div>
                                        <div className={classnames({'mtb25 buy-sell-middle-details': true, 'm--font-green': this.state.completedRecipte == true})} style={{ marginBottom: "50px", borderTop:"none" }}>
                                            <div style={{textAlign:"center"}}>Closing this page will not cancel your order.<br/>You may return to your wallet if you do not wish to wait</div>
                                        </div>
                                    </div>
                                </div>
                            }

                            <div ref={(el) => { this.confirm = el; }} className={classnames({'col-lg-6 buy-sell-confrimation-container': true, 'active': this.state.confirmation == true})}>
                                <div className="buy-sell-confrimation">
                                    <div className={classnames({'buy-sell-confirmation-inside': true, 'active': this.state.confirmation == true})}>
                                        <div className="buy-sell-confirmation-header">{strings.CONFIRM_YOUR_TRASNACTION}</div>
                                        <div className="buy-sell-confirmation-subheader">{strings.PLEASE_CHECK_RECEIPT}</div>
                                        { this.state.activeAction === 'buy'?
                                            <div>
                                                { !this.state.loading &&
                                                    <button onClick={() => this.buyCoin()} type="button" className="btn btn-green btn-block button-padding15 buy-sell-buy-button">{strings.CONFIRM_BUY}</button>
                                                }
                                            </div>
                                            :
                                            <div>
                                                { !this.state.loading &&
                                                    <button onClick={() => this.sellCoin()} type="button" className="btn btn-green btn-block button-padding15 buy-sell-buy-button">{strings.CONFIRM_SELL}</button>
                                                }
                                            </div>
                                        }
                                        { !this.state.loading &&
                                            <div className="buy-sell-confirmation-cancel">
                                                <a onClick={() => this.onCancellation()}>{strings.CANCEL_CONVERSTION}</a>
                                            </div>
                                        }
                                        {this.state.loading == true ?
                                            <span style={{display:"block", textAlign:"center"}} >
                                                <Loader type="line-scale" active={this.state.loading} className="text-center" />
                                            </span>
                                        : null }
                                        <div style={{marginTop:"25px",backgroundColor:"#fef7e5",padding:"15px",color:"rgb(160, 145, 84)",border:"1px solid #e2c26e",textAlign:"left",borderRadius:"6px"}}>
                                            <div style={{fontSize:"17px"}}>*Price is not Guaranteed</div><br/>
                                            <div style={{fontSize:"12px"}}>Prices may change while your order is being processed, especially for larger orders that may move the market.<br/><br/>
                                                Customers are advised to break large orders into smaller orders to have a better sense of actual market price.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
                </div>
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        trading: state.trading,
        language: state.language
    };
};

export default connect(mapStateToProps, null)(Conversion);
