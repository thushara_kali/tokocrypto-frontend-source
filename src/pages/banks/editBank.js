import React, { Component } from 'react'
import { connect } from 'react-redux';
import actions from '../../core/actions/client/banks';
import { NotificationManager } from 'react-notifications';
import NetworkError from '../../errors/networkError'
import { Loader } from 'react-loaders'
import history from '../../history'
import BackButton from '../../components/backButton'

class EditBank extends Component {

    constructor() {
        super()
        this.state = {
            label: "",
            bankName: "",
            bankAddress: "",
            bankCode: "",
            branchCode: "",
            beneficiaryName: "",
            beneficiaryAddress: "",
            beneficiaryIdCardNumber: "",
            accountNumber: "",
            loading: false,
            sending: false,
            formFilled: false
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.value
            this.setState(obj)
        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    updateBank() {
        this.setState({ formFilled: true })
        let data = {
            label: this.state.label,
            bankName: this.state.bankName,
            bankAddress: this.state.bankAddress,
            bankCode: this.state.bankCode,
            branchCode: this.state.branchCode,
            beneficiaryName: this.state.beneficiaryName,
            beneficiaryAddress: this.state.beneficiaryAddress,
            beneficiaryIdCardNumber: this.state.beneficiaryIdCardNumber
        }
        let valid =
            this.state.label.length > 0 &&
            this.state.bankName.length > 0 &&
            this.state.bankAddress.length > 0 &&
            this.state.bankCode.length > 0 &&
            this.state.branchCode.length > 0 &&
            this.state.beneficiaryName.length > 0 &&
            this.state.beneficiaryAddress.length > 0 &&
            this.state.beneficiaryIdCardNumber.length > 0

        if (valid) {
            this.setState({ sending: true })
            this.props.updateBank(this.getToken(), history.location.pathname.split('/')[history.location.pathname.split('/').length - 1], data).then(res => {
                NotificationManager.success('bank has been updated.')
                this.setState({
                    loading: false,
                    sending: false,
                    formFilled: false
                })
            }).catch(err => {
                NetworkError(err)
                this.setState({
                    loading: false,
                    sending: false,
                    formFilled: false
                })
            })
        } else {
            this.setState({ sending: false })
        }
    }

    fetchBank() {
        this.setState({ loading: true })
        this.props.fetchBank(this.getToken(), history.location.pathname.split('/')[history.location.pathname.split('/').length - 1])
            .then(res => {
                this.setState({
                    loading: false,
                    sending: false,
                    label: res.data.data.Label,
                    bankName: res.data.data.BankName,
                    bankAddress: res.data.data.BankAddress,
                    bankCode: res.data.data.BankCode,
                    branchCode: res.data.data.BranchCode,
                    beneficiaryName: res.data.data.BeneficiaryName,
                    beneficiaryAddress: res.data.data.BeneficiaryAddress,
                    beneficiaryIdCardNumber: res.data.data.BeneficiaryIdCardNumber
                })
            })
            .catch(err => {
                NetworkError(err)
                this.setState({
                    loading: false,
                    sending: false
                })
            })
    }

    componentDidMount() {
        this.fetchBank()
    }

    render() {
        return (
            <div className="container">
                <BackButton back />
                <br />
                <div className="row justify-content-center">
                    <div className="col-10 box-header">
                        <label className="color-white">Edit Bank</label>
                    </div>
                </div>
                {this.state.loading ? <div>
                    <br />
                    <Loader type="line-scale" active={true} className="text-center" />
                </div> :
                    <div>
                        <div className="row justify-content-center">
                            <div className="col-10 box-body">
                                <label>Label</label>
                                <input value={this.state.label} className="form-control" placeholder="label" onChange={this.handleChange('label')} />
                                {this.state.label.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-10 box-body">
                                <label>Bank Name</label>
                                <input value={this.state.bankName} className="form-control" placeholder="bank name" onChange={this.handleChange('bankName')} />
                                {this.state.bankName.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-10 box-body">
                                <label>Bank Address</label>
                                <input value={this.state.bankAddress} className="form-control" placeholder="bank address" onChange={this.handleChange('bankAddress')} />
                                {this.state.bankAddress.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-10 box-body">
                                <label>Bank Code</label>
                                <input value={this.state.bankCode} className="form-control" placeholder="bank code" onChange={this.handleChange('bankCode')} />
                                {this.state.bankCode.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-10 box-body">
                                <label>Branch Code</label>
                                <input value={this.state.branchCode} className="form-control" placeholder="branch code" onChange={this.handleChange('branchCode')} />
                                {this.state.branchCode.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-10 box-body">
                                <label>Beneficiary Name</label>
                                <input value={this.state.beneficiaryName} className="form-control" placeholder="beneficiary name" onChange={this.handleChange('beneficiaryName')} />
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-10 box-body">
                                <label>Beneficiary Address</label>
                                <input value={this.state.beneficiaryAddress} className="form-control" placeholder="beneficiary address" onChange={this.handleChange('beneficiaryAddress')} />
                                {this.state.beneficiaryAddress.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-10 box-body">
                                <label>Beneficiary ID Card Number</label>
                                <input value={this.state.beneficiaryIdCardNumber} className="form-control" placeholder="beneficiary id card number" onChange={this.handleChange('beneficiaryIdCardNumber')} />
                                {this.state.beneficiaryIdCardNumber.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-10 box-body">
                                {this.state.sending ? <Loader type="line-scale" active={true} className="text-center" /> :
                                    <button className="btn btn-primary btn-block" onClick={() => this.updateBank()}>Save</button>}
                            </div>
                        </div>
                    </div>}
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user
    };
};

const mapDispatchToProps = dispatch => ({
    fetchBank: actions.fetchBank,
    updateBank: actions.updateBank
});

export default connect(mapStateToProps, mapDispatchToProps)(EditBank)