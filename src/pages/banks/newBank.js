import React, { Component } from 'react'
import { connect } from 'react-redux';
import actions from '../../core/actions/client/banks';
import { NotificationManager } from 'react-notifications';
import NetworkError from '../../errors/networkError'
import { Loader } from 'react-loaders'
import BackButton from '../../components/backButton'

class NewBank extends Component {

    constructor() {
        super()
        this.state = {
            label: "",
            bankName: "",
            bankAddress: "",
            bankCode: "",
            branchCode: "",
            beneficiaryName: "",
            beneficiaryAddress: "",
            beneficiaryIdCardNumber: "",
            accountNumber: "",
            sending: false,
            formFilled: false
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.value
            this.setState(obj)
        }
    }

    resetField() {
        this.setState({
            label: "",
            bankName: "",
            bankAddress: "",
            bankCode: "",
            branchCode: "",
            beneficiaryName: "",
            beneficiaryAddress: "",
            beneficiaryIdCardNumber: "",
            accountNumber: ""
        })
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    createBank() {
        this.setState({ formFilled: true })
        let data = {
            "label": this.state.label,
            "bankName": this.state.bankName,
            "bankAddress": this.state.bankAddress,
            "bankCode": this.state.bankCode,
            "branchCode": this.state.branchCode,
            "beneficiaryName": this.state.beneficiaryName,
            "beneficiaryAddress": this.state.beneficiaryAddress,
            "beneficiaryIdCardNumber": this.state.beneficiaryIdCardNumber,
            "accountNumber": this.state.accountNumber
        }
        let valid =
            this.state.label.length > 0 &&
            this.state.bankName.length > 0 &&
            this.state.bankAddress.length > 0 &&
            this.state.bankCode.length > 0 &&
            this.state.branchCode.length > 0 &&
            this.state.beneficiaryName.length > 0 &&
            this.state.beneficiaryAddress.length > 0 &&
            this.state.beneficiaryIdCardNumber.length > 0 &&
            this.state.accountNumber.length > 0

        if (valid) {
            this.setState({ sending: true })
            this.props.createBank(this.getToken(), data).then(res => {
                this.setState({
                    sending: false
                })
                NotificationManager.success('bank has been created.')
            }).catch(err => {
                NetworkError(err)
                this.setState({
                    sending: false,
                    formFilled: false
                })
                this.resetField()
            })
        } else {
            this.setState({ sending: false })
        }
    }

    render() {
        return (
            <div className="container">
                <BackButton back />
                <br />
                <div className="row justify-content-center">
                    <div className="col-10 box-header">
                        <label className="color-white">Create New Bank</label>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-10 box-body">
                        <label>Account Number</label>
                        <input value={this.state.accountNumber} className="form-control" placeholder="account number" onChange={this.handleChange('accountNumber')} />
                        {this.state.accountNumber.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-10 box-body">
                        <label>Label</label>
                        <input value={this.state.label} className="form-control" placeholder="label" onChange={this.handleChange('label')} />
                        {this.state.label.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-10 box-body">
                        <label>Bank Name</label>
                        <input className="form-control" placeholder="bank name" onChange={this.handleChange('bankName')} />
                        {this.state.bankName.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-10 box-body">
                        <label>Bank Address</label>
                        <input className="form-control" placeholder="bank address" onChange={this.handleChange('bankAddress')} />
                        {this.state.bankAddress.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-10 box-body">
                        <label>Bank Code</label>
                        <input className="form-control" placeholder="bank code" onChange={this.handleChange('bankCode')} />
                        {this.state.bankCode.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-10 box-body">
                        <label>Branch Code</label>
                        <input className="form-control" placeholder="branch code" onChange={this.handleChange('branchCode')} />
                        {this.state.branchCode.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-10 box-body">
                        <label>Beneficiary Name</label>
                        <input className="form-control" placeholder="beneficiary name" onChange={this.handleChange('beneficiaryName')} />
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-10 box-body">
                        <label>Beneficiary Address</label>
                        <input className="form-control" placeholder="beneficiary address" onChange={this.handleChange('beneficiaryAddress')} />
                        {this.state.beneficiaryAddress.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-10 box-body">
                        <label>Beneficiary ID Card Number</label>
                        <input className="form-control" placeholder="beneficiary id card number" onChange={this.handleChange('beneficiaryIdCardNumber')} />
                        {this.state.beneficiaryIdCardNumber.length < 1 && this.state.formFilled == true ? <span className="color-danger">this field is required</span> : null}
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-10 box-body">
                        {this.state.sending ? <Loader type="line-scale" active={true} className="text-center" /> :
                            <button className="btn btn-primary btn-block" onClick={() => this.createBank()}>Save</button>}
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user
    };
};

const mapDispatchToProps = dispatch => ({
    createBank: actions.createBank
});

export default connect(mapStateToProps, mapDispatchToProps)(NewBank)
