import React, { Component } from 'react'
import { connect } from 'react-redux';
import actions from '../../core/actions/client/banks';
import { NotificationManager } from 'react-notifications';
import NetworkError from '../../errors/networkError'
import { Loader } from 'react-loaders'
import history from '../../history'
import BackButton from '../../components/backButton'

class DepositBanks extends Component {
    constructor() {
        super()
        this.state = {
            banks: [],
            loading: false
        };
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    fetchBankDepositAccounts() {
        this.setState({ loading: true })
        this.props.fetchBankDepositAccounts(this.getToken()).then(res => {
            this.setState({ banks: res.data.data, loading: false })
        })
        .catch(err => {
            NetworkError(err)
        })
    }

    componentDidMount() {
        {this.fetchBankDepositAccounts()}
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h3 className="color-primary">Deposit Bank Accounts</h3>
                    </div>
                </div>
                <br />
                <div className="row">
                    {this.state.banks.length > 0 ? 
                    <table className="table table-banks">
                        <thead>
                            <tr>
                                <th>Account Number</th>
                                <th className="hidden-sm-down">Label</th>
                                <th>Bank Name</th>
                                <th>Account Type</th>
                                {/* <th>View</th> */}
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.banks.map(b => {
                                return (
                                    <tr key={b.id}>
                                        <td>{b.AccountNumber}</td>
                                        <td className="hidden-sm-down">{b.Label}</td>
                                        <td>{b.BankName}</td>
                                        <td>{b.BankAccountType}</td>
                                        {/* <td><button className="btn btn-info" onClick={() => history.push('banks/' + b.AccountNumber)}>Edit</button></td> */}
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                    : !this.state.loading ? <div className="alert alert-danger col-md-12" role="alert">You don't have deposit bank account assign yet.please contact support team.</div>: ""}
                    <Loader type="line-scale" active={this.state.loading} className="col-12 text-center" />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user
    };
};

const mapDispatchToProps = dispatch => ({
    fetchBankDepositAccounts: actions.fetchBankDepositAccounts
});

export default connect(mapStateToProps, mapDispatchToProps)(DepositBanks)