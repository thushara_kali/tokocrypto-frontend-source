import React, { Component } from 'react'
import history from '../../history'
import { connect } from 'react-redux';
import actions from '../../core/actions/client/banks';
import { NotificationManager } from 'react-notifications';
import NetworkError from '../../errors/networkError'
import { Loader } from 'react-loaders'

class Banks extends Component {

    constructor() {
        super()
        this.state = {
            banks: [],
            loading: false
        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    fetchBanks() {
        this.setState({ loading: true })
        this.props.fetchBanks(this.getToken()).then(res => {
            this.setState({ banks: res.data.data, loading: false })
        })
            .catch(err => {
                NetworkError(err)
            })
    }

    componentDidMount() {
        {this.fetchBanks()}
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h3 className="color-primary">Banks</h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <button className="btn btn-primary" onClick={() => history.push('banks/new')}>Add new bank</button>
                    </div>
                </div>
                <br />
                <div className="row">
                    <table className="table table-banks">
                        <thead>
                            <tr>
                                <th>Account Number</th>
                                <th className="hidden-sm-down">Label</th>
                                <th>Bank Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.banks.map(b => {
                                return (
                                    <tr key={b.id}>
                                        <td>{b.AccountNumber}</td>
                                        <td className="hidden-sm-down">{b.Label}</td>
                                        <td>{b.BankName}</td>
                                        <td>
                                            <button className="btn btn-info" onClick={() => history.push('banks/' + b.AccountNumber)}>Edit</button>
                                            <button className="btn btn-danger">Delete</button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                    {/* <Loader type="line-scale" active={this.state.loading} className="col-12 text-center" /> */}
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user
    };
};

const mapDispatchToProps = dispatch => ({
    fetchBanks: actions.fetchBanks
});

export default connect(mapStateToProps, mapDispatchToProps)(Banks)