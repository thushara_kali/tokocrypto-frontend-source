import React, { Component } from 'react';
import KYCMessage from '../../components/kycMessage';
import { connect } from 'react-redux';
import classnames from 'classnames';
import NetworkError from '../../errors/networkError';
import _ from 'lodash';

class TradingOff extends Component {

    constructor(){
        super();
        this.state = {
        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    componentDidMount(){
        // this.fetchAccounts();
    }

    render() {
        return (
            // <div>
            //     <h1>Thank you for register with tokocrypto.</h1>
            // </div>
            <div className="" style={{ marginTop: "15%"}}>
                <div className="text-center">
                <h3 className=""><img style={{ height: "80px"}} alt="" src="assets/media/img/logo/logo-retina.png"/></h3>
                <div className="col-md-6 mx-auto" style={{ marginTop: "25px"}}>
                    <p>
                        Terima kasih atas ketertarikan Anda terhadap Tokocrypto! 
                        Saat ini kami sedang melakukan beta testing dengan beberapa (grup) 
                        beta tester yang terpilih untuk memastikan bahwa kami memiliki 
                        semua mekanisme yang ada dan bertujuan untuk memastikan experience 
                        terbaik bagi pengguna kami. Hubungi kami via &nbsp;<a href="mailto:support@tokocrypto.com">support@tokocrypto.com</a>&nbsp;
                        atau melalui layanan chat kami jika Anda ingin bergabung menjadi beta tester. 
                        Subscribe newsletter kami di situs kami &nbsp;<a href="https://tokocrypto.com/">www.tokocrypto.com</a>&nbsp;
                        untuk mendapatkan akses berita dan update terbaru kami!
                    </p>
                </div>
                <div className="">
                    
                </div>
            
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user
    };
};

export default connect(mapStateToProps, null)(TradingOff);