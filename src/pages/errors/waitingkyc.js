import React, { Component } from 'react';
import KYCMessage from '../../components/kycMessage';
import { connect } from 'react-redux';
import classnames from 'classnames';
import NetworkError from '../../errors/networkError';
import _ from 'lodash';

class WaitingKyc extends Component {

    constructor(){
        super();
        this.state = {
        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    componentDidMount(){
        // this.fetchAccounts();
    }

    render() {
        return (
            // <div>
            //     <h1>Thank you for register with tokocrypto.</h1>
            // </div>
            <div className="login-wrapper" style={{ marginTop: "15%"}}>
                <div className="login-fields text-center">
                <h3 className="title-404"><img alt="" src="assets/media/img/logo/logo.png"/></h3>
                <div className="pt20">
                    <p>Your KYC is waiting for confirmation.</p>
                </div>
                <div className="pt20">
                    
                </div>
            
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user
    };
};

export default connect(mapStateToProps, null)(WaitingKyc);