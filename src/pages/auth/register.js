import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { registerUser } from '../../core/react-cognito';
import { connect } from 'react-redux';
import store from '../../store'
import { Link } from 'react-router-dom';
import { NotificationManager } from 'react-notifications';
import history from '../../history';
import classnames from 'classnames';
import { Loader } from '../../components/tcLoader';
import Capthca from '../../components/captcha';
import PasswordChecker from '../../components/passwordChecker';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import userService from '../../core/services/client';
import { setLanguage } from '../../core/actions/client/language.js';
import { bindActionCreators } from 'redux';
import localStorage from 'localStorage';
import { pixelID } from '../../config/pixel';
import NetworkError from '../../errors/networkError';
import ReactGA from 'react-ga';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

let ReactPixel = null;

class Register extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            loading: false,
            captchaSuccess: process.env.REACT_APP_ENV === 'local'? true : false,
            notAgree: false,
            tnc: false,
            passwordValid: false,
            help: true,
            userDefineLanguage: 'id',
            refer: '',
            disabled: false,
            campaign: '',
            ref: '',
            signUpReference: '',
            signUpReferenceDetail: ''
        };
    }

    callGA(){
        ReactGA.initialize('UA-116825757-2');
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    backAction() {
        if (this.props.back) {
            history.goBack()
        } else {
            history.replace('/login')
        }
    }
    /* 
        TCDOC-002
        determine default referral code from url
    */

    loadFromURL(){

        if(history.location.search.length > 0){

            let mainURL = ((history.location.search.split('?')[1]).split('=')[0]);

            if(mainURL == 'refer'){
                let referid = (history.location.search.split('=')[1]);
                this.setState({
                    refer: referid,
                    disabled: true
                })
            }else if(mainURL == 'campaign'){

                let campaign = ((history.location.search.split('&')[0]).split('=')[1]);
                let ref = ((history.location.search.split('&')[1]).split('=')[1]);

                this.setState({
                    campaign: campaign,
                    ref: ref
                })
            } else if(mainURL === 'email_signup'){
                let email = (history.location.search.split('=')[1]);
                this.setState({
                    email: unescape(email)
                })
            }

        }

    }

    onSubmit = (event) => {
        event.preventDefault();
        const userPool = this.props.userPool;
        const config = this.props.config;
        const email = this.state.email.toLowerCase();
        this.setState({ loading: true });

        /*
            TCDOC-003 
            this function submit to cognito using modified react-cognito in core folders
        */

        userService.getEmailVersion(email).then(res => {
            
            if(res.data.data === null) {
                registerUser(userPool, config, email, this.state.password, {
                    email: email,
                    name: this.state.firstName + ' ' + this.state.lastName,
                    'custom:refer': this.state.refer,
                    'custom:campaign': this.state.campaign,
                    'custom:ref': this.state.ref,
                    'custom:signUpReference': this.state.signUpReference,
                    'custom:signUpRefDetail': this.state.signUpReferenceDetail
                }).then(
                    (action) => {
                        this.trackPixelSubmit();
                        this.setState({ loading: false })
                        store.dispatch(action);

                        /*
                            TCDOC-004
                            every cognito action will have state. after success register is confirmation state
                        */

                        if (this.props.cognitoState === 'CONFIRMATION_REQUIRED') {

                            NotificationManager.info(strings.CONFIRMATION_CODE_SENT);
                            history.push('/confirm');
                        }
                    },
                    error => {
                        this.setState({ loading: false })
                        NotificationManager.error(error);
                    });
            } else {
                this.setState({ loading: false })
                NotificationManager.error('Email Already Exist');
            }
        }).catch(err => {
            this.setState({ loading: false })
            NetworkError(err)
            // NotificationManager.error('Unexpected Error');
        });

    }


    changeFirstName = (event) => {
        const re = /^[a-zA-Z_ ]{1,256}$/;
        if (event.target.value == '' || re.test(event.target.value )) {
            this.setState({ firstName: event.target.value });
        }
    }

    changeLastName = (event) => {
        this.setState({ lastName: event.target.value });
    }

    changeReferFrom = (event) => {
        this.setState({ signUpReference: event.target.value });
    }

    changeReferDetailFrom = (event) => {
        this.setState({ signUpReferenceDetail: event.target.value });
    }

    changeEmail = (event) => {
        this.setState({ email: event.target.value });
    }

    changePassword = (event) => {
        this.setState({ password: event.target.value });
    }

    onChangeRefer = (event) => {
        this.setState({ refer: event.target.value });
    }
    pressReferCode = (event) => {
        const re = /[a-z]/g;
        const keyCode = event.keyCode || event.which;
        const keyValue = String.fromCharCode(keyCode);
        if (!/[a-z]/g.test(keyValue)) event.preventDefault();
    }

    onCapthcaSuccess() {
        this.setState({
            captchaSuccess: true
        });
    }

    onBlurHelp(e) {
        if(!e.target.classList.contains('keep-help')){
            this.setState({
                help: true
            });
        } else {
            this.setState({
                help: true
            });
        }
    }

    forceOpenHelpText(){
        this.setState({
            help: true
        });
    }

    onChangeLanguage(e) {
        
        localStorage.setItem('language', e);

        let currentPath = history.location.pathname.split('/')[2];

    	setTimeout(() => {
            history.push('/' + e + '/' + currentPath)
            window.location.reload();
	    },100)
    }

    componentDidMount(){
        this.callGA();
        this.initFBPixel();
        this.loadFromURL();
    }

    initFBPixel() {
        ReactPixel = require('react-facebook-pixel').default;
        ReactPixel.init(pixelID);
        ReactPixel.pageView();
        ReactPixel.track( 'ViewContent', {content_name: 'register'} );
    }

    trackPixelSubmit() {
        if (ReactPixel) {
            
            ReactPixel.track( 'Lead', {content_name: 'signup completed'} );
        }
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    render() {

        let otherTrue = this.state.signUpReference === "Other Websites (you can state the website under details)" || this.state.signUpReference === "Others (you can state under details)";
        // let letContinue = this.state.signUpReference.length === 0;
        // if(otherTrue){
        //     letContinue = (this.state.signUpReferenceDetail.length > 0) ? false : true;
        // }
        let letContinue = false;
        let submitDisabled = this.state.signUpReference.length === 0 || this.state.firstName.length === 0 || this.state.email.length === 0 || this.state.password.length === 0 || this.state.tnc === false || this.state.captchaSuccess === false || this.state.passwordValid === false || letContinue;

        if(otherTrue){
            if(this.state.signUpReferenceDetail.length === 0){
                submitDisabled = true
            }
        }

        return (
            <div className="m-grid m-grid--hor m-grid--root m-page" style={{ minHeight: "100vh" }}>
                <div onMouseUp={(e) => this.onBlurHelp(e)} className="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style={{ backgroundImage: "url(../assets/media/img/bg/bg-3.jpg)"}}>
                    <div className="m-grid__item m-grid__item--fluid	m-login__wrapper">
                        <div className="m-login__container">
                            <div className="m-login__logo">
                                <a href="#">
                                    <img src="../assets/media/img/logo/TC_new_logo.png"/>
                                </a>
                            </div>
                            <div className="m-login__signin">
                                <div className="m-login__head" style={{textAlign:"center",color:"#9e97aa"}}>
                                    <h3 className="m-login__title" style={{color:"#373d44"}}>{strings.SING_UP}</h3>
                                    <div className="m-login__desc">{strings.DETAILS_CREATE_YOUR_ACCOUNT}</div>
                                </div>
                                <form onSubmit={this.onSubmit} className="m-login__form m-form">

                                    <div>
                                        <div className="form-group m-form__group">
                                            <input className="form-control m-input login-inputs"  type="text" placeholder={strings.FULL_NAME} id="First-name" value={this.state.firstName} onChange={this.changeFirstName}/>
                                        </div>
                                        <div className="form-group m-form__group">
                                            <input className="form-control m-input m-login__form-input--last login-inputs" type="email" placeholder="Email"  id="email" value={this.state.email} onChange={this.changeEmail}/>
                                        </div>
                                        <PasswordChecker forceOpenHelpText={() => this.forceOpenHelpText()} help={this.state.help} value={this.state.password} onChange={this.changePassword} setValid={(valid) => this.setState({ passwordValid: valid})}/>
                                        <div className="form-group m-form__group">
                                            <input disabled={this.state.disabled} className="form-control m-input login-inputs"  type="text" placeholder='Referral Code (Optional)' id="referral-code" value={this.state.refer}  onKeyPress={(e) => this.pressReferCode(e)} onChange={(e)=> this.onChangeRefer(e)}/>
                                        </div>
                                        <div className="form-group m-form__group">
                                            <select name="cars" value={this.state.signUpReference} onChange={this.changeReferFrom} className="form-control form-control-lg login-inputs" style={{fontSize:"1rem", padding:"0 1.3rem 0 1.3rem"}}>
                                                <option value="">Please Select</option>
                                                <option value="Word of mouths (friends, family, collegues etc)">Word of mouths (friends, family, collegues etc)</option>
                                                <option value="Newspaper">Newspaper</option>
                                                <option value="Internet News">Internet News</option>
                                                <option value="Television">Television</option>
                                                <option value="AMG Advertisement">AMG Advertisement</option>
                                                <option value="Youtube">Youtube</option>
                                                <option value="Facebook">Facebook</option>
                                                <option value="Instagram">Instagram</option>
                                                <option value="Twitter">Twitter</option>
                                                <option value="Community Meetups">Community Meetups</option>
                                                <option value="Conference">Conference</option>
                                                <option value="Blockchain Community Groups(Telegram Groups)">Blockchain Community Groups(Telegram Groups)</option>
                                                <option value="Google">Google</option>
                                                <option value="Yahoo">Yahoo</option>
                                                <option value="Other Websites (you can state the website under details)">Other Websites (you can state the website under details)</option>
                                                <option value="Others (you can state under details)">Others (you can state under details)</option>
                                            </select>
                                        </div>
                                        {otherTrue === true &&
                                            <div className="form-group m-form__group">
                                                <textarea className="form-control m-input login-inputs" value={this.state.signUpReferenceDetail} onChange={this.changeReferDetailFrom} rows="3" style={{borderRadius:"0.25rem"}}></textarea>
                                            </div>
                                        }
                                        <div className="form-group m-form__group">
                                            <label className="control-label" style={{ textAlign: "center", width: "100%", marginTop: "20px"}}>
                                                <input checked={this.state.tnc} onChange={() => this.setState({ tnc: !this.state.tnc })} type="checkbox"/>
                                                {
                                                    this.props.language === 'en' ? <a target="_blank" href="https://support.tokocrypto.com/hc/en-us/articles/360004043371-Tokocrypto-User-Agreement" style={{ marginLeft: "5px"}}>{strings.ACCEPT_TERMS_AND_CONDTIONS}</a> : <a target="_blank" href="https://support.tokocrypto.com/hc/id/articles/360004044971-Perjanjian-Pengguna-Tokocrypto" style={{ marginLeft: "5px"}}>{strings.ACCEPT_TERMS_AND_CONDTIONS}</a>
                                                }
                                            </label>
                                        </div>

                                        {/* <div className="row form-group m-form__group m-login__form-sub">
                                            <div className="col m--align-left">
                                                <label className="m-checkbox m-checkbox--focus">
                                                <input type="checkbox" name="agree"/>I Agree the <a href="#" class="m-link m-link--focus">terms and conditions</a>.
                                                <span></span>
                                                </label>
                                                <span className="m-form__help"></span>
                                            </div>
                                        </div> */}
                                    </div>

                                    <div className="m-login__form-action">
                                        { process.env.REACT_APP_ENV !== 'local' && <Capthca
                                            reset={this.state.resetCaptcha}
                                            onCapthcaSuccess={() => this.onCapthcaSuccess()}
                                        /> }
                                        {this.state.loading == false &&
                                            <button onClick={() => this.backAction()} label="Back" style={{marginRight:"30px"}} className="btn btn-focus-green-back m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                                {strings.BACK}
                                            </button>
                                        }
                                        {this.state.loading == false &&
                                            <button type="submit" label="Sign In" primary={true} disabled={submitDisabled} id="m_login_signin_submit" className="btn btn-focus-green m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                                {strings.SING_UP}
                                            </button>
                                        }
                                        {this.state.loading == true &&
                                            <Loader type="line-scale" active={this.state.loading} className="text-center" />
                                        }
                                    </div>
                                </form>
                                <div style={{textAlign:"center"}}>

                                    <a onClick={() => this.onChangeLanguage('en')} value="en" className={classnames({'m-link language-select': true,'active-language-select': this.props.language == 'en'})}>English</a>
                                    <a onClick={() => this.onChangeLanguage('id')} value="id" className={classnames({'m-link language-select': true,'active-language-select': this.props.language == 'id'})}>Indonesian</a>
                                    <a onClick={() => this.onChangeLanguage('sunda')} value="sunda" className={classnames({'m-link language-select': true,'active-language-select': this.props.language == 'sunda'})}>Sundanese</a>
                                    <a onClick={() => this.onChangeLanguage('java')} value="java" className={classnames({'m-link language-select': true,'active-language-select': this.props.language == 'java'})}>Javanese</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            // <div className="col-12 auth-outer">
            //     <form onSubmit={this.onSubmit}>
            //         <div className="login-wrapper">
            //             <div className="login-fields">
            //                 <h3>Register</h3>
            //                 <TextField
            //                     id="firs-name"
            //                     value={this.state.firstName}
            //                     onChange={this.changeFirstName}
            //                     floatingLabelText="First name"
            //                     fullWidth={true}
            //                 />
            //                 <TextField
            //                     id="last-name"
            //                     value={this.state.lastName}
            //                     onChange={this.changeLastName}
            //                     floatingLabelText="Last name"
            //                     fullWidth={true}
            //                 />
            //                 <TextField
            //                     id="email"
            //                     value={this.state.email}
            //                     onChange={this.changeEmail}
            //                     floatingLabelText="Email"
            //                     fullWidth={true}
            //                     type="email"
            //                 />
            //                 <TextField
            //                     id="pass"
            //                     onChange={this.changePassword}
            //                     floatingLabelText="Password"
            //                     type="password"
            //                     fullWidth={true}
            //                 />
            //                 <br />
            //                 <br />
            //                 <Capthca
            //                     onCapthcaSuccess={() => this.onCapthcaSuccess()}
            //                 />
            //                 <div className="pt20">
            //                     {this.state.loading == false &&
            //                         <RaisedButton type="submit" label="Sign Up" primary={true} disabled={!this.state.captchaSuccess} fullWidth={true} />
            //                     }
            //                     {this.state.loading == true &&
            //                         <Loader type="line-scale" active={this.state.loading} className="text-center" />
            //                     }
            //                 </div>
            //                 <div className="pull-right" style={{ margin: 10 }}>
            //                     <Link to="login">back to login</Link>
            //                 </div>
            //             </div>
            //         </div>
            //     </form>
            // </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        config: state.cognito.config,
        cognitoState: state.cognito.state,
        userPool: state.cognito.userPool,
        language: state.language
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Object.assign({}, { setLanguage: setLanguage }), dispatch)
});

export default connect(
    mapStateToProps
)(Register);
