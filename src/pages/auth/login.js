import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { connect } from 'react-redux';
import history from '../../history';
import { Action, authenticate } from '../../core/react-cognito';
import { Link } from 'react-router-dom';
import { Loader } from '../../components/tcLoader';
import { NotificationManager } from 'react-notifications';
import { bindActionCreators } from 'redux';
import Capthca from '../../components/captcha';
import OTPActions from '../../core/actions/client/otp';
import classnames from 'classnames';
import homeActions from '../../core/actions/client/home';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import ReactFlagsSelect from 'react-flags-select';
import localStorage from 'localStorage';
import { setLanguage } from '../../core/actions/client/language.js'
import jwtDecode from 'jwt-decode';
import { updateAttributes } from '../../core/react-cognito/attributes.js'
import deviceInfo from '../../helpers/deviceInfo';
import NetworkError from '../../errors/networkError';

import 'react-flags-select/css/react-flags-select.css';
import axios from 'axios';

import { pixelID } from '../../config/pixel';
import ReactGA from 'react-ga';

var awsIot = require('aws-iot-device-sdk');

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

let ReactPixel = null;

class Login extends React.Component {

    constructor(props, context) {
        super(props);
        this.state = {
            email: props.email,
            username: props.username,
            error: '',
            password: '',
            loading: false,
            captchaSuccess: process.env.REACT_APP_ENV === 'local' ? true : false,
            promptMFA: false,
            otp: '',
            otpId: '',
            resetCaptcha: false,
            otpType: 'email',
            mfaCountDown: 60,
            promptAccess: process.env.REACT_APP_ENV === 'demo' ? true : false, // should be false on prod and dev
            // promptAccess: true,
            pwKeyPrompt: '',
            pwPromptAccess: 'salamtothemoon2019'
        };
    }

    callGA(){
        ReactGA.initialize('UA-116825757-2');
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token;
    }

    onSetLanguage() {
        strings.setLanguage(this.props.language);
    }

    removeCacheStorage() {
        localStorage.removeItem('CACHE_EMAIL');
        localStorage.removeItem('CACHE_TOKEN');
        localStorage.removeItem('CACHE_ACCESS_TOKEN');
        localStorage.removeItem('CACHE_NAME');
        localStorage.removeItem('CACHE_METHOD');
    }

    updateUserLoginInfo() {
        let info = deviceInfo.getDeviceInfo();
        let data = {
            deviceInfo: info.os.name + ' ' + info.os.version + ', ' + info.browser.name + ' ' + info.browser.version
        };

        axios.get("https://ipapi.co/json/").then(res => {
            data["ipAddress"] = res.data.ip;

            // send and forget
            OTPActions.sendLoginNotification(this.getToken(), data.email, data.name, data.deviceInfo, data.ipAddress);
        }).catch(err => {
            NetworkError(err)
            // this.requestOTP();
        });
    }

    onSubmit = (event) => {
        
        event.preventDefault();
        this.props.setTradingStatus('rejected');
        this.setState({
            captchaSuccess: false,
            otp: '',
            loading: true
        });
        if (this.props.user) {
            let username = this.props.user.username
            let clientId = this.props.user.pool.clientId
            let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];
            
            if (token) {
                let decoded = jwtDecode(token);
                let current_time = new Date().getTime() / 1000;
                if (current_time > decoded.exp) {
                    this.props.user.signOut();
                }
            }
        }

        // let splitUsername = this.state.username.split('@');
        // let plainUsername = splitUsername[0].replace(/(\.+)|\+\d+/g, '');
        //
        // let plainLowerCase = plainUsername+'@'+splitUsername[1].toLowerCase();

        // this.requestOTP();
        this.props.onSubmit(this.state.username.toLowerCase(), this.state.password).then(() => {
            // this.setState({ loading: false })
            if (this.props.cognitoState === 'CONFIRMATION_REQUIRED') {
                NotificationManager.info(strings.CONFIRM_YOUR_ACCOUNT);
                history.replace('/confirm');
            } else {
                // this.setState({
                //    promptMFA: true
                // })
                this.requestOTP();
            }
        }).catch((error) => {
            this.setState({ loading: false, resetCaptcha: true })
            //console.dir(error)
            console.dir()
            // NetworkError(error)
            NotificationManager.error(strings.INVALID_USERNAME_PASSWORD);
        });
    }

    requestEmailOTP() {
        this.setState({
            loading: true
        });
        OTPActions.loginOTPForceEmail(this.state.username.toLowerCase()).then(res => {
            this.setState({
                otpId: res.data.data.OtpId,
                otpType: res.data.data.Type,
                promptMFA: true,
                loading: false,
                resetCaptcha: true
            })

            if (res.data.data.Type === 'email') {
                NotificationManager.success(strings.OTP_SENT);
            }

            this.countDownMfa();
        }).catch(err => {
            NetworkError(err)
            // NotificationManager.error(err.response.data.err.message);
            this.setState({
                loading: false,
                resetCaptcha: true,
                username: '',
                password: ''
            });
        });
    }

    /**
     * TCDOC-010 Request OTP
     * call OTP request to get otp id and type
     * make sure the UI are representing the correct message of the otp type
     */
    requestOTP() {
        OTPActions.OTPLoginAfterCheckPassword(this.getToken()).then(res => {
            this.setState({
                otpId: res.data.data.OtpId,
                otpType: res.data.data.Type,
                promptAccess: false,
                promptMFA: true,
                loading: false,
                resetCaptcha: true
            });
            this.countDownMfa();
        }).catch(err => {
            NetworkError(err)
            this.setState({
                promptMFA: false,
                loading: false,
                resetCaptcha: true
            });
        })
    }

    requestOTPOld() {
        this.setState({
            loading: true
        });
        OTPActions.loginOTP(this.state.username.toLowerCase()).then(res => {

            if (res.data.data.user_status === "CONFIRMED") {
                this.setState({
                    otpId: res.data.data.OtpId,
                    otpType: res.data.data.Type,
                    promptMFA: true,
                    loading: false,
                    resetCaptcha: true
                });
                if (res.data.data.Type === 'email') {
                    NotificationManager.success(strings.OTP_SENT);
                }
            } else {
                history.replace('/confirm', { username: this.state.username.toLowerCase(), password: this.state.password });
            }
        }).catch(err => {
            NetworkError(err)
            // NotificationManager.error(err.response.data.err.message);
            this.setState({
                loading: false,
                resetCaptcha: true,
                username: '',
                password: ''
            });
        });
    }

    verifyPrompt() {
        this.setState({
            loading: true
        });

        if (this.state.pwKeyPrompt == this.state.pwPromptAccess) {
            this.setState({
                loading: false,
                promptAccess: false
            });
            // this.requestOTP()
        } else {
            this.setState({
                loading: false
            });
            NotificationManager.error(strings.WRONG_PROMPT);
        }
    }

    /**
     * TCDOC-011
     * verify otp by giving user input and otpId
     */
    verifyMfa() {
        this.setState({
            loading: true
        });

        OTPActions.OTPLoginVerify(this.getToken(), this.state.otpId, this.state.otp).then(res => {
            clearInterval(this.mfaInterval);

            let username = this.props.user.username
            let clientId = this.props.user.pool.clientId
            let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];

            if (token.length > 0) {
                let decoded = jwtDecode(token);

                if (window.Raven) {
                    window.Raven.setUserContext({
                        email: this.props.user.username,
                        id: decoded.sub
                    });
                }
            }

            this.updateUserLoginInfo();

            setTimeout(() => {
                history.replace('/' + history.location.pathname.split('/')[1] + '/dashboard');
            }, 250);
        }).catch(err => {
            this.setState({
                promptMFA: true,
                loading: false,
                password: '',
                resetCaptcha: true
            });
            if(err.response.data.message_code === 'verification_otp_error'){
                NotificationManager.error(strings.INVALID_OTP);
            }else{
                NetworkError(err)
            }
            
        })
    }

    frameLogin(e) {
        try {
            // e.preventDefault();
            // create switch when its demo, dev, app
            if (process.env.REACT_APP_ENV === 'local') {
                document.location.href = "https://tokocrypto-users-dev.auth.us-east-1.amazoncognito.com/login?response_type=token&client_id=3dig65alnu6ptubbmnfeeqccbj&redirect_uri=http://localhost:3000/checktoken";
                // document.location.href = "#";
            } else if (process.env.REACT_APP_ENV === 'dev') {
                document.location.href = "https://tokocrypto-users-dev.auth.us-east-1.amazoncognito.com/login?response_type=token&client_id=3dig65alnu6ptubbmnfeeqccbj&redirect_uri=https://dev.tokocrypto.com/checktoken";
            } else if (process.env.REACT_APP_ENV === 'demo') {
                document.location.href = "https://tokocrypto-users-demo.auth.us-west-2.amazoncognito.com/login?response_type=token&client_id=2j44n9b5jlqcec5iqsapn2mkbr&redirect_uri=https://demo.tokocrypto.com/checktoken";
            } else if (process.env.REACT_APP_ENV === 'prod') {
                document.location.href = "#";
            } else {
                document.location.href = "#";
            }
        } catch (err) {
            
        }

    }

    verifyMfaOld() {
        this.setState({
            loading: true
        });

        OTPActions.clientMfaVerify(this.state.otpId, this.state.otp).then(res => {
            this.props.onSubmit(this.state.username.toLowerCase(), this.state.password).then(() => {
                this.setState({ loading: false })
                if (this.props.cognitoState === 'CONFIRMATION_REQUIRED') {
                    NotificationManager.info(strings.CONFIRM_YOUR_ACCOUNT);
                    history.replace('/confirm');
                } else {
                    history.push(history.location.pathname.split('/')[1] + '/');
                }
            }).catch((error) => {
                this.setState({ loading: false, promptMFA: false, password: '' })
                // NotificationManager.error(error.message);
                NetworkError(error)
            });
        }).catch(err => {
            this.setState({
                promptMFA: false,
                loading: false,
                password: ''
            });
            NetworkError(err)
            // NotificationManager.error(strings.INVALID_OTP);
        })
    }

    countDownMfa() {
        clearInterval(this.mfaInterval);
        this.setState({
            mfaCountDown: 0
        });

        this.mfaInterval = setInterval(() => {
            if (this.state.mfaCountDown < 60) {
                this.setState({
                    mfaCountDown: this.state.mfaCountDown + 1
                });
            } else {
                clearInterval(this.mfaInterval);
            }
        }, 1000);

        if (window.intervals) {
            window.intervals.push(this.mfaInterval)
        } else {
            window.intervals = [this.mfaInterval]
        }
    }

    initFBPixel() {
        ReactPixel = require('react-facebook-pixel').default;
        ReactPixel.init(pixelID);
        ReactPixel.pageView();
        ReactPixel.track('ViewContent', { content_name: 'login' });
    }

    changeUsername = (event) => {
        this.setState({ username: event.target.value, resetCaptcha: false });
    }

    changeOTP = (event) => {
        this.setState({ otp: event.target.value });
    }

    changePrompt = (event) => {
        this.setState({ pwKeyPrompt: event.target.value });
    }

    changePassword = (event) => {
        this.setState({ password: event.target.value, resetCaptcha: false });
    }

    onCapthcaSuccess() {
        this.setState({
            captchaSuccess: true,
            resetCaptcha: false
        });
    }

    onChangeLanguage(e) {
        // this.props.actions.setLanguage(e.target.value);
        localStorage.setItem('language', e);

        let currentPath = history.location.pathname.split('/')[2];

    	setTimeout(() => {
            history.push('/' + e + '/' + currentPath)
            window.location.reload();
	    },100)

        // setTimeout(() => {
        //     this.onSetLanguage();
        //     window.location.reload();
        // }, 100)
    }

    componentWillMount() {
        this.removeCacheStorage();
        this.onSetLanguage();
        if (window.intervals) {
            window.intervals.map((i, index) => {
                clearInterval(i)
                window.intervals.splice(index, 1);
            });
        }
    }

    componentWillUnmount() {
        if (window.intervals) {
            window.intervals.map((i, index) => {
                clearInterval(i)
                window.intervals.splice(index, 1);
            });
        }
    }

    unsubscribeIot() {

        if(window.iotDevice){
            
            var topic = window.iotTopic
            var iotDevice = window.iotDevice


            iotDevice.unsubscribe(topic);
        }

    }

    componentWillMount() {
        this.unsubscribeIot();
    }

    componentDidMount() {
        this.callGA();
        this.initFBPixel();
        this.onSetLanguage();
        localStorage.setItem('notification', JSON.stringify({ enabled: true }));
    }

    render() {

        let loginDisabled = this.state.username.length === 0 || this.state.password.length === 0 || this.state.captchaSuccess === false;
        let mfaDisabled = this.state.otp.length === 0;

        let otpMessage = strings.OTP_SENT_TO_EMAIL;

        if (this.state.otpType.toLowerCase() === 'ga') {
            otpMessage = strings.OTP_GOOGLE_AUTH_APP;
        }

        const tickVerification = (this.state.mfaCountDown / 60) * 100;

        return (
            <div className="m-grid m-grid--hor m-grid--root m-page" style={{ minHeight: "100vh" }}>
                <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style={{ backgroundImage: "url(../assets/media/img/bg/bg-3.jpg)" }}>
                    <div className="m-grid__item m-grid__item--fluid	m-login__wrapper">
                        <div className="m-login__container">
                            <div className="m-login__logo">
                                <a href="#">
                                    <img src="../assets/media/img/logo/TC_new_logo.png" />
                                </a>
                            </div>
                            {history.location.state && history.location.state.verify === true && <div style={{ marginTop: "-20px", marginBottom: "30px" }} className="alert alert-success text-center">{strings.EMAIL_CONFIRMED_SIGN_CONTINUE}</div>}
                            <div className="m-login__signin padding-Bottom-200" className={classnames({ 'm-login__signin': true, 'padding-Bottom-200': this.state.promptMFA })} >
                                <div className="m-login__head">
                                    {!this.state.promptMFA && !this.state.promptAccess &&
                                        <h3 className="m-login__title">
                                            {strings.SING_IN_DASHBOARD}
                                        </h3>
                                    }
                                    {this.state.promptMFA && !this.state.promptAccess &&
                                        <h3 className="m-login__title">
                                            {strings.ONE_TIME_PASSWORD}
                                        </h3>
                                    }
                                    {!this.state.promptMFA && this.state.promptAccess &&
                                        <h3 className="m-login__title">
                                            {strings.PROMPT_BEFORE_DEMO}
                                        </h3>
                                    }
                                </div>
                                <form onSubmit={this.onSubmit} className="m-login__form m-form">
                                    {this.state.promptMFA && !this.state.promptAccess &&
                                        <div style={{ textAlign: "center" }} className="dark-gray">
                                            {otpMessage} {this.state.otpType.toLowerCase() === 'email' && <div className="force-email-login-link">{this.state.username}</div>}
                                        </div>
                                    }
                                    {this.state.promptMFA && !this.state.promptAccess &&
                                        <div className="form-group m-form__group">
                                            <input id="mfa" value={this.state.otp} onChange={this.changeOTP} className="form-control m-input login-inputs" type="text" placeholder={strings.OTP_VERIFICATION_CODE} name="OTP" />
                                        </div>
                                    }
                                    {!this.state.promptMFA && !this.state.promptAccess && <div>

                                        <div className="form-group m-form__group">
                                            <input value={this.state.username} onChange={this.changeUsername} className="form-control m-input login-inputs" type="text" placeholder="Email" name="email" />
                                        </div>
                                        <div className="form-group m-form__group">
                                            <input autocomplete="off" value={this.state.password} onChange={this.changePassword} className="form-control m-input m-login__form-input--last login-inputs" type="password" placeholder={strings.PASSWORD} name="password" />
                                        </div>

                                    </div>}

                                    {!this.state.promptMFA && this.state.promptAccess &&
                                        <div className="form-group m-form__group">
                                            <input id="prompt" value={this.state.pwKeyPrompt} onChange={this.changePrompt} className="form-control m-input login-inputs" type="password" placeholder={strings.PROMPT_BEFORE_DEMO} name="prompt" />
                                        </div>
                                    }

                                    {this.state.promptMFA && this.state.otpType.toLowerCase() === 'email' && !this.state.promptAccess && <div style={{
                                        marginTop: "15px",
                                        paddingLeft: "20px",
                                        paddingRight: "20px"
                                    }}>
                                        {this.state.mfaCountDown !== 60 ?
                                            <div className="progress">
                                                <span className="progress-bar" style={{ width: tickVerification + '%' }} />
                                            </div> :
                                            <div className="progress">
                                                <span className="progress-bar" style={{ width: '100%' }} />
                                            </div>
                                        }
                                    </div>}

                                    <div className="row m-login__form-sub">

                                        {!this.state.promptMFA && !this.state.promptAccess && <div className="col m--align-right m-login__form-right">
                                            <Link to="forgot-password" href="javascript:;" id="m_login_forget_password" className="m-link">{strings.FORGET_PASSWORD}</Link>
                                        </div>}

                                        {this.state.promptMFA && this.state.otpType.toLowerCase() === 'ga' && !this.state.promptAccess && <div style={{ textAlign: "center" }} >
                                            <a>{strings.CONTACT} <a href="mailto:support@tokocrypto.com" className="force-email-login-link">support@tokocrypto.com</a> {strings.SUPPORT_GOOGLE_AUTHENTICATOR}</a>
                                        </div>}

                                        {this.state.promptMFA && this.state.otpType.toLowerCase() === 'email' && this.state.mfaCountDown === 60 && !this.state.promptAccess && <div className="col">
                                            <a className="force-email-login-link" onClick={() => this.requestEmailOTP()}>{strings.RESEND_OTP}</a>
                                        </div>}

                                        {this.state.promptMFA && this.state.otpType.toLowerCase() === 'email' && this.state.mfaCountDown !== 60 && !this.state.promptAccess && <div className="col">
                                            <a className="disabled-link">{strings.RESEND_OTP}</a>
                                        </div>}

                                    </div>
                                    <div className="m-login__form-action">
                                        {!this.state.promptMFA && process.env.REACT_APP_ENV !== 'local' && !this.state.promptAccess && <Capthca
                                            reset={this.state.resetCaptcha}
                                            onCapthcaSuccess={() => this.onCapthcaSuccess()} />
                                        }

                                        {this.state.loading == false && !this.state.promptMFA && !this.state.promptAccess &&
                                            <button type="submit" label="Sign In" disabled={!this.state.captchaSuccess} primary={true} id="m_login_signin_submit" className="btn btn-focus-green m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                                {strings.SING_IN}
                                            </button>
                                        }
                                        <br />
                                        {/* {this.state.loading == false && !this.state.promptMFA && !this.state.promptAccess &&
                                            <div className="m-login__btn" style={{ paddingTop: 0 }}>
                                                <div className="abcRioButton abcRioButtonBlue " style={{ height: 50, width: 240 }}>
                                                    <a className="abcRioButtonContentWrapper" onClick={this.frameLogin}>
                                                        <div className="abcRioButtonIcon custom-icon" style={{ padding: 15 }}>
                                                            <div style={{ width: 20, height: 20 }} className="abcRioButtonSvgImageWithFallback abcRioButtonIconImage abcRioButtonIconImage18" >
                                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 48 48" className="abcRioButtonSvg">
                                                                    <g>
                                                                        <path fill="#EA4335" d="M24 9.5c3.54 0 6.71 1.22 9.21 3.6l6.85-6.85C35.9 2.38 30.47 0 24 0 14.62 0 6.51 5.38 2.56 13.22l7.98 6.19C12.43 13.72 17.74 9.5 24 9.5z"></path>
                                                                        <path fill="#4285F4" d="M46.98 24.55c0-1.57-.15-3.09-.38-4.55H24v9.02h12.94c-.58 2.96-2.26 5.48-4.78 7.18l7.73 6c4.51-4.18 7.09-10.36 7.09-17.65z"></path>
                                                                        <path fill="#FBBC05" d="M10.53 28.59c-.48-1.45-.76-2.99-.76-4.59s.27-3.14.76-4.59l-7.98-6.19C.92 16.46 0 20.12 0 24c0 3.88.92 7.54 2.56 10.78l7.97-6.19z"></path>
                                                                        <path fill="#34A853" d="M24 48c6.48 0 11.93-2.13 15.89-5.81l-7.73-6c-2.15 1.45-4.92 2.3-8.16 2.3-6.26 0-11.57-4.22-13.47-9.91l-7.98 6.19C6.51 42.62 14.62 48 24 48z"></path>
                                                                        <path fill="none" d="M0 0h48v48H0z"></path>
                                                                    </g>
                                                                </svg>
                                                            </div>
                                                        </div>
                                                        <div style={{ fontsize: 26 }} className="abcRioButtonContents">
                                                            <span className="googleButtonString">{strings.SING_IN_GOOGLE}</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        } */}
                                        {this.state.loading == false && this.state.promptMFA && !this.state.promptAccess &&
                                            <button disabled={mfaDisabled} onClick={() => this.verifyMfa()} label="Verify" primary={true} type="submit" label="Sign In" id="m_login_signin_submit" className="btn btn-focus-green m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                                {strings.SUBMIT}
                                            </button>
                                        }
                                        {this.state.loading == false && !this.state.promptMFA && this.state.promptAccess &&
                                            <button onClick={() => this.verifyPrompt()} label="Verify" primary={true} type="button" id="m_prompt_submit" className="btn btn-focus-green m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                                {strings.SUBMIT}
                                            </button>
                                        }
                                        {this.state.loading == true &&
                                            <Loader type="line-scale" active={this.state.loading} className="text-center" />
                                        }
                                    </div>
                                </form>
                            </div>

                            {!this.state.promptMFA && !this.state.promptAccess &&
                                <div className="m-login__account">

                                    <span className="m-login__account-msg" style={{ color: "#847b93" }}>
                                        {strings.DONT_HAVE_ACCOUNT}
                                    </span>
                                    &nbsp;&nbsp;
                                <Link href="javascript:;" id="m_login_signup" className="m-link m-link--light m-login__account-link" to="register">{strings.SING_UP}</Link>

                                    <div style={{ textAlign: "center" }}>

                                        <a onClick={() => this.onChangeLanguage('en')} value="en" className={classnames({ 'm-link language-select': true, 'active-language-select': this.props.language == 'en' })}>English</a>
                                        <a onClick={() => this.onChangeLanguage('id')} value="id" className={classnames({ 'm-link language-select': true, 'active-language-select': this.props.language == 'id' })}>Indonesian</a>
                                        <a onClick={() => this.onChangeLanguage('sunda')} value="id" className={classnames({ 'm-link language-select': true, 'active-language-select': this.props.language == 'sunda' })}>Sundanese</a>
                                        <a onClick={() => this.onChangeLanguage('java')} value="id" className={classnames({ 'm-link language-select': true, 'active-language-select': this.props.language == 'java' })}>Javanese</a>

                                    </div>

                                    {/* <select value={this.props.language} className="form-control m-input" onChange={(e) => this.onChangeLanguage(e)}>
                                    <option value="id">Indonesian</option>
                                    <option value="en">English</option>
                                </select> */}

                                </div>}
                        </div>
                    </div>
                </div>
            </div>

            // <div className="col-12 auth-outer">
            //     <form onSubmit={this.onSubmit}>
            //         <div className="login-wrapper">
            //             <div className="login-fields">
            //                 <h3>Login</h3>
            //                 { this.state.promptMFA && <TextField
            //                     id="mfa"
            //                     value={this.state.otp}
            //                     onChange={this.changeOTP}
            //                     floatingLabelText="OTP Code"
            //                     fullWidth={true}
            //                     type="password"
            //                 /> }
            //                 { !this.state.promptMFA && <div>
            //                     <TextField
            //                         id="name"
            //                         value={this.state.username}
            //                         onChange={this.changeUsername}
            //                         floatingLabelText="Username"
            //                         fullWidth={true}
            //                     />
            //                     <TextField
            //                         id="pass"
            //                         value={this.state.password}
            //                         onChange={this.changePassword}
            //                         floatingLabelText="Password"
            //                         type="password"
            //                         fullWidth={true}
            //                     />
            //                 </div> }
            //                 <br />
            //                 <br />
            //                 { this.state.promptMFA && <div className="alert alert-info">
            //                     We have sent you an email for the one-time password
            //                 </div> }
            //                 { !this.state.promptMFA && <Capthca
            //                     reset={this.state.resetCaptcha}
            //                     onCapthcaSuccess={() => this.onCapthcaSuccess()}
            //                 /> }
            //                 { !this.state.promptMFA && <div className="pull-right" style={{ margin: 10 }}>
            //                     <Link to="register">Register</Link>
            //                 </div>}
            //                 { !this.state.promptMFA && <div className="pull-right" style={{ margin: 10 }}>
            //                     <Link to="forgot-password">Forgot Password</Link>
            //                 </div>}
            //                 <br /><br />
            //                 <div className="pt20">
            //                     {this.state.loading == false && !this.state.promptMFA &&
            //                         <RaisedButton type="submit" label="Log In" disabled={loginDisabled} primary={true} fullWidth={true} />
            //                     }
            //                     {this.state.loading == false && this.state.promptMFA &&
            //                         <RaisedButton disabled={mfaDisabled} onClick={() => this.verifyMfa()} label="Verify" primary={true} fullWidth={true} />
            //                     }
            //                     {this.state.loading == true &&
            //                         <Loader type="line-scale" active={this.state.loading} className="text-center" />
            //                     }
            //                 </div>
            //             </div>
            //         </div>
            //     </form>
            // </div>
        );
    }
}

const mapStateToProps = (state) => {
    let username = '';
    if (state.cognito.user) {
        username = state.cognito.user.getUsername();
    } else if (state.cognito.userName) {
        username = state.cognito.cache.userName;
    }
    return {
        user: state.cognito.user,
        username,
        cognitoState: state.cognito.state,
        email: state.cognito.cache.email,
        config: state.cognito.config,
        userPool: state.cognito.userPool,
        language: state.language
    };
};

const mapDispatchToProps = dispatch => ({
    authenticator: (username, password, userPool, config) => authenticate(username, password, userPool, config, dispatch),
    clearCache: () => dispatch(Action.clearCache()),
    setTradingStatus: (status) => dispatch(homeActions.setTradingStatus(status)),
    actions: bindActionCreators(Object.assign({}, { setLanguage: setLanguage }), dispatch)
});

const mergeProps = (stateProps, dispatchProps, ownProps) =>
    Object.assign({}, ownProps, stateProps, {
        onSubmit: (username, password) =>
            dispatchProps.authenticator(username, password, stateProps.userPool, stateProps.config),
        clearCache: dispatchProps.clearCache,
        setTradingStatus: dispatchProps.setTradingStatus
    });

/**
 * Container for login behaviour, wrapping a login form.
 *
 * Magically provides the following props to the wrapped form:
 *
 *  * username
 *  * onSubmit
 *
 * @example
 * <Login>
 *   <LoginForm />
 * </Login>
 */


export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps,
)(Login);
