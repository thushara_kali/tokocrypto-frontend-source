import React, { Component } from 'react'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import { connect } from 'react-redux'
import { Loader } from '../../components/tcLoader'
import { NotificationManager } from 'react-notifications'
import history from '../../history'
import { Link } from 'react-router-dom'
import GeneralOTP from '../../components/OTPModalGeneral'
import PasswordChecker from '../../components/passwordChecker'
import queryParser from 'query-string-parser'
import LocalizedStrings from 'react-localization'
import enDictionary from '../../languages/en.json'
import idDictionary from '../../languages/id.json'
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import NetworkError from '../../errors/networkError';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class ForgotPasswordForm extends Component {

	constructor() {
		super()
		this.state = {
			username: '',
			verificationCode: '',
			newPassword: '',
			confirmNewPassword: '',
			loading: false,
			mode: 'request', // request || reset,
			passwordValid: false,
			help: true
		}

		this.handleChange = (fieldName) => (event) => {
			let obj = Object.assign({})
			obj[fieldName] = event.target.value
			this.setState(obj)
		}
	}

	onSetLanguage() {
		strings.setLanguage(this.props.language)
	}

	backAction() {
		if (this.props.back) {
			history.goBack()
		} else {
			history.replace('/login')
		}
	}

	/**
	 * TCDOC-008
	 * Send the user email to cognito. please note to lowercase the email because mobile web user might have auto capitalize.
	 */
	requestCode(e) {
		e.preventDefault()
		this.setState({
			loading: true
		})

		this.props.sendVerificationCode(this.state.username, this.props.userPool).then(res => {
			NotificationManager.success(strings.RESET_PASSWORD_INSTRUCTIONS_SENT)
			this.setState({
				loading: false,
				username: ''
			})
		}).catch(err => {
			NetworkError(err)
			NotificationManager.success(strings.RESET_PASSWORD_INSTRUCTIONS_SENT)
			this.setState({
				loading: false,
				username: ''
			})
		})
	}

	/**
	 * TCDOC-009
	 * Send to cognito the new user password
	 */
	resetPassword() {
		this.setState({
			loading: true
		})

		// 
		//
		// let splitUsername = this.state.username.split('@');
		// let plainUsername = splitUsername[0].replace(/(\.+)|\+\d+/g, '');
		//
		// let plainLowerCase = plainUsername+'@'+splitUsername[1].toLowerCase();

		this.props.setPassword(this.state.username.toLowerCase(), this.state.verificationCode, this.state.newPassword).then(res => {
			this.setState({
				loading: false,
				mode: 'request'
			})
			NotificationManager.success(strings.RESET_PASSWORD_SUCCESS)
			history.push('/login')
		}).catch(err => {
			// let message = strings.INVALID_VERIFICATION_CODE
			// if (err.toString().length !== 0) {
			// 	message = err.toString()
			// }
			// NotificationManager.error(message)
			// 
			NetworkError(err)
			this.setState({
				loading: false
			})
		})
	}

	onBlurHelp(e) {
		// 
		if (!e.target.classList.contains('keep-help')) {
			this.setState({
				help: true
			})
		} else {
			this.setState({
				help: true
			})
		}
	}

	forceOpenHelpText() {
		this.setState({
			help: true
		})
	}

	componentDidMount() {
		let location = history.location.pathname.split('/')[2]
		if (location === 'reset-password') {
			let parsed = queryParser.fromQuery(history.location.search)
			this.setState({
				mode: 'reset',
				verificationCode: parsed.code,
				username: parsed.email
			})
		}
	}

	componentWillMount() {
		this.onSetLanguage()
	}

	render() {
		let message = strings.PLEASE_ENTER_YOUR_EMAIL
		if (this.state.mode === 'reset') {
			message = strings.PLEASE_ENTER_YOUR_NEW_PASSWORD
		}

		return (
			<div className="m-grid m-grid--hor m-grid--root m-page" style={{minHeight: '100vh'}}>
				<div onMouseUp={(e) => this.onBlurHelp(e)}
				     className="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2"
				     id="m_login" style={{backgroundImage: 'url(../assets/media/img/bg/bg-3.jpg)'}}>
					<div className="m-grid__item m-grid__item--fluid	m-login__wrapper">
						<div className="m-login__container padding-Bottom-200">
							<div className="m-login__logo">
								<a href="#">
									<img src="../assets/media/img/logo/TC_new_logo.png"/>
								</a>
							</div>
							<div className="m-login__signin">
								<div class="m-login__head" style={{textAlign: 'center', color: '#9e97aa'}}>
									<h3 class="m-login__title"
									    style={{color: '#373d44'}}>{strings.PASSWORD_RESET_FORM}</h3>
									<div class="m-login__desc">{message}</div>
								</div>
								<form onSubmit={this.onSubmit} className="m-login__form m-form">

									{this.state.mode === 'request' &&
									<div>
										<div className="form-group m-form__group">
											<input id="name" floatingLabelText="Email" fullWidth={true} type="email"
											       disabled={this.state.loading}
											       onChange={this.handleChange('username')}
											       className="form-control m-input login-inputs" type="text"
											       placeholder="Email"/>
										</div>
									</div>
									}

									{this.state.mode === 'reset' &&
									<div>
										<PasswordChecker forceOpenHelpText={() => this.forceOpenHelpText()}
										                 help={this.state.help} value={this.state.newPassword}
										                 onChange={this.handleChange('newPassword')}
										                 setValid={(valid) => this.setState({passwordValid: valid})}/>
										<div className="form-group m-form__group">
											<input type="password" onChange={this.handleChange('confirmNewPassword')}
											       type="password" className="form-control m-input login-inputs"
											       placeholder={strings.REPEAT_PASSWORD}/>
										</div>
										{this.state.newPassword !== this.state.confirmNewPassword && this.state.confirmNewPassword.length > 0 &&
										<span style={{marginLeft: '15px'}}>{strings.PASSWORD_MISMATCH}</span>}
										{/* <div className="form-group m-form__group">
                                                <input value={this.state.verificationCode} type="password" onChange={this.handleChange('verificationCode')} type="text" className="form-control m-input login-inputs" placeholder="Verification code"/>
                                            </div> */}
									</div>
									}

									{this.state.loading == false &&
									<div className="m-login__form-action">

										<button onClick={() => this.backAction()} label="Back"
										        id="m_login_signin_submit"
										        className="btn btn-focus-green-back m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
											{strings.BACK}
										</button>

										{this.state.loading == false && this.state.mode === 'request' &&
										<button disabled={this.state.username.length === 0}
										        onClick={(e) => this.requestCode(e)} label="Confirm" primary={true}
										        style={{marginLeft: '30px'}}
										        className="btn btn-focus-green m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
											{strings.SUBMIT}
										</button>
										}

										{this.state.loading == false && this.state.mode === 'reset' &&
										<button
											disabled={this.state.newPassword.length === 0 ||
											this.state.confirmNewPassword.length === 0 ||
											this.state.verificationCode.length == 0 ||
											(this.state.newPassword !== this.state.confirmNewPassword || this.state.newPassword === '') ||
											this.state.passwordValid === false
											}
											onClick={() => this.resetPassword()} label="Reset Password" primary={true}
											style={{marginLeft: '30px'}}
											className="btn btn-focus-green m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
											{strings.SUBMIT}
										</button>
										}

									</div>
									}

									{this.state.loading == true &&
									<div style={{marginLeft: '45%'}}>
										<Loader type="line-scale" active={this.state.loading} className="text-center"/>
									</div>
									}
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}

}

const mapStateToProps = (state) => {
	let username = ''
	if (state.cognito.user) {
		username = state.cognito.user.getUsername()
	} else if (state.cognito.userName) {
		username = state.cognito.cache.userName
	}
	return {
		username,
		cognitoState: state.cognito.state,
		email: state.cognito.cache.email,
		config: state.cognito.config,
		userPool: state.cognito.userPool,
		language: state.language
	}
}

export default connect(mapStateToProps, null)(ForgotPasswordForm)
