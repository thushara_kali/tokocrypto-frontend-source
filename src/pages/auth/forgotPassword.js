import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { PasswordReset } from '../../core/react-cognito';
import PasswordResetForm from './forgotPasswordForm';
import { connect } from 'react-redux';

class ForgotPassword extends Component {

    render(){
        return (
            <PasswordReset>
                <PasswordResetForm />
            </PasswordReset>    
        )
    }

}

export default connect(null, null)(ForgotPassword);
