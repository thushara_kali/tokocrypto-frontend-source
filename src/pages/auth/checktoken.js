import React from 'react';
import { connect } from 'react-redux';
import history from '../../history';
import { refreshIdentityCredentials } from '../../store/react-cognito';
import { Action } from '../../core/react-cognito';
import { setToken } from '../../store/react-cognito';
import { Link } from 'react-router-dom';
import { NotificationManager } from 'react-notifications';
import { bindActionCreators } from 'redux';
import paramsHelper from '../../helpers/params';
import { Loader } from '../../components/tcLoader';
import jwtDecode from 'jwt-decode';
import classnames from 'classnames';
import NetworkError from '../../errors/networkError';
import AWS from 'aws-sdk';

import { CognitoUserPool, CognitoUser } from 'amazon-cognito-identity-js';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import OTPActions from '../../core/actions/client/otp';
import userService from '../../core/services/client';
import localStorage from 'localStorage';
import cognitoHelper from '../../helpers/cognitoHelper';

const CognitoUserSession = require('amazon-cognito-identity-js').CognitoUserSession;

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

this.token = '';

class Checktoken extends React.Component {

    constructor(props, context) {
        super(props);
        this.state = {
            loading: true,
            idToken: '',
            tokens: {},
            otpId: '',
            otpType: '',
            otp: '',
            promptMFA: false,
            mfaCountDown: 60
        }
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    onSetLanguage() {
        strings.setLanguage(this.props.language);
    }

    getToken() {
        let username = this.props.user.username
        
        // if (username.indexOf('Google_') !== -1) {
        //     return localStorage.getItem('CACHE_TOKEN');
        // }
        // 
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        // 
        return token;
    }

    getEmailAvailability(usermail) {
        return new Promise((resolve, reject) => {
            userService.getEmailVersion(usermail.toLowerCase()).then(res => {
                resolve(res.data);
            }).catch(err => {
                NetworkError(err)
                reject(err);
            });
        });
    }

    async convertTokenIntoCognitoObject() {
        const self = this;
        AWS.config.region = cognitoHelper.cognitoEnv().region;
        const queryParsed = history.location.hash;

        const qp = paramsHelper.parse(queryParsed);

        
        let decoded = jwtDecode(qp.id_token);

        // const config = {
        //     UserPoolId: 'us-east-1_aQsndC09Q',
        //     ClientId: '3dig65alnu6ptubbmnfeeqccbj',
        //     AppWebDomain: 'localhost:3000',
        //     TokenScopesArray: ['openid', 'profile', 'email'],
        //     RedirectUriSignIn: 'https://localhost:3000/checktoken',
        //     RedirectUriSignOut: 'https://localhost:3000'
        // }

        const poolData = {
            UserPoolId: cognitoHelper.cognitoEnv().userPool,
            ClientId: cognitoHelper.cognitoEnv().clientId
        };

        const userPool = new CognitoUserPool(poolData);
        const userData = {
            Username: decoded['cognito:username'],
            Pool: userPool
        };

        let _cognitoUser = new CognitoUser(userData);
        const sessionData = {
            IdToken: qp.id_token,
            AccessToken: qp.access_token,
            RefreshToken: ''
        };
        
        let clientId = poolData.ClientId
        
        _cognitoUser.email = decoded['email'];
        _cognitoUser.storage['CognitoIdentityServiceProvider.' + clientId + '.' + decoded['cognito:username'] + '.idToken'] = qp.id_token;

        let sessionObject = new CognitoUserSession(sessionData);
        _cognitoUser.getSession = () => {
            return (
                null,
                sessionObject
            )
        }

        this.props.authenticatedManual(_cognitoUser);

        localStorage.setItem('CACHE_EMAIL', decoded["email"]);
        localStorage.setItem('CACHE_TOKEN', qp.id_token);
        localStorage.setItem('CACHE_ACCESS_TOKEN', qp.access_token);
        localStorage.setItem('CACHE_NAME', decoded['name']);
        localStorage.setItem('CACHE_METHOD', 'social');

        setTimeout(() => {
            
            
            self.requestOTP();
        }, 1000);

        // 

        // 

        // use this to refresh the token. but will need middleware from backend to get user ID and update user cognito properties

        // AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        //     IdentityPoolId: 'us-east-1:72c54bfe-6f58-4edf-930f-4bc7deda68ca',
        //     Logins: {
        //         'cognito-idp.us-east-1.amazonaws.com/us-east-1_aQsndC09Q': qp.id_token
        //     }
        // });

        // AWS.config.credentials.refresh((err, token, session) => {
        //     
        //     if (!err) {
        //         
        //     }
        // });
    }

    requestOTP() {
        OTPActions.OTPLoginAfterCheckPassword(this.getToken()).then(res => {
            this.setState({
                otpId: res.data.data.OtpId,
                otpType: res.data.data.Type,
                promptMFA: true,
                loading: false,
                resetCaptcha: true
            });

            this.countDownMfa();

        }).catch(err => {
            NetworkError(err)
            this.setState({
                promptMFA: false,
                loading: false,
                resetCaptcha: true
            });
        })
    }

    requestEmailOTP() {
        this.setState({
            loading: true
        });
        OTPActions.loginOTPForceEmail(this.props.user.email.toLowerCase()).then(res => {
            this.setState({
                otpId: res.data.data.OtpId,
                otpType: res.data.data.Type,
                promptMFA: true,
                loading: false,
                resetCaptcha: true
            })

            if (res.data.data.Type === 'email') {
                NotificationManager.success(strings.OTP_SENT);
            }

            this.countDownMfa();
        }).catch(err => {
            NetworkError(err)
            // NotificationManager.error(err.response.data.err.message);
            this.setState({
                loading: false,
                resetCaptcha: true,
                username: '',
                password: ''
            });
        });
    }

    verifyMfa() {
        this.setState({
            loading: true
        });

        OTPActions.OTPLoginVerify(this.getToken(), this.state.otpId, this.state.otp).then(res => {
            clearInterval(this.mfaInterval);

            let token = this.getToken();

            if (token.length > 0) {
                let decoded = jwtDecode(token);

                if (window.Raven) {
                    window.Raven.setUserContext({
                        email: this.props.user.username,
                        id: decoded.sub
                    });
                }
            }

            // this.updateUserLoginInfo();

            setTimeout(() => {
                history.replace('/');
            }, 250);
        }).catch(err => {
            
            this.setState({
                promptMFA: true,
                loading: false,
                password: '',
                resetCaptcha: true
            });
            NetworkError(err)
            // NotificationManager.error(strings.INVALID_OTP);
        })
    }

    countDownMfa() {
        clearInterval(this.mfaInterval);
        this.setState({
            mfaCountDown: 0
        });

        this.mfaInterval = setInterval(() => {
            if (this.state.mfaCountDown < 60) {
                this.setState({
                    mfaCountDown: this.state.mfaCountDown + 1
                });
            } else {
                clearInterval(this.mfaInterval);
            }
        }, 1000);

        if (window.intervals) {
            window.intervals.push(this.mfaInterval)
        } else {
            window.intervals = [this.mfaInterval]
        }
    }

    onSubmit = (event) => {
        this.verifyMfa();
    }

    changeOTP = (event) => {
        this.setState({ otp: event.target.value });
    }

    componentWillMount() {

    }

    componentDidMount() {
        
        // check response from lambda
        if (history.location.hash.lengh !== 0 && history.location.hash.indexOf('error_description=') !== -1) {
            NotificationManager.error('Email Already Registered in Tokocrypto User');
            history.replace('/login');
        } else {
            this.convertTokenIntoCognitoObject();
        }
    }

    render() {

        const tickVerification = (this.state.mfaCountDown / 60) * 100;
        let mfaDisabled = this.state.otp.length === 0;

        return (
            <div className="m-grid m-grid--hor m-grid--root m-page" style={{ minHeight: "100vh" }}>
                <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style={{ backgroundImage: "url(assets/media/img/bg/bg-3.jpg)" }}>
                    <div className="m-grid__item m-grid__item--fluid	m-login__wrapper">
                        <div className="m-login__container">
                            <div className="m-login__logo">
                                <a href="#">
                                    <img src="assets/media/img/logo/TC_new_logo.png" />
                                </a>
                            </div>
                            <div className="m-login__signin padding-Bottom-200" className={classnames({ 'm-login__signin': true, 'padding-Bottom-200': this.state.promptMFA })} >
                                <div className="m-login__head">
                                    {!this.state.promptMFA ?
                                        <h3 className="m-login__title">
                                            {strings.SING_IN_GOOGLE}
                                        </h3> :
                                        <h3 className="m-login__title">
                                            {strings.ONE_TIME_PASSWORD}
                                        </h3>
                                    }
                                </div>
                                <form className="m-login__form m-form">
                                    {this.state.promptMFA &&
                                        <div style={{ textAlign: "center" }} className="dark-gray">
                                            {strings.OTP_SENT_TO_EMAIL} {this.state.otpType.toLowerCase() === 'email' && <div className="force-email-login-link">{this.props.user.email}</div>}
                                        </div>
                                    }
                                    {this.state.promptMFA &&
                                        <div className="form-group m-form__group">
                                            <input id="mfa" value={this.state.otp} onChange={this.changeOTP} className="form-control m-input login-inputs" type="text" placeholder={strings.OTP_VERIFICATION_CODE} name="OTP" />
                                        </div>
                                    }

                                    {this.state.promptMFA && <div style={{
                                        marginTop: "15px",
                                        paddingLeft: "20px",
                                        paddingRight: "20px"
                                    }}>
                                        {this.state.mfaCountDown !== 60 ?
                                            <div className="progress">
                                                <span className="progress-bar" style={{ width: tickVerification + '%' }} />
                                            </div> :
                                            <div className="progress">
                                                <span className="progress-bar" style={{ width: '100%' }} />
                                            </div>
                                        }
                                    </div>}

                                    <div className="row m-login__form-sub">

                                        {this.state.promptMFA && this.state.mfaCountDown === 60 && <div className="col">
                                            <a className="force-email-login-link" onClick={() => this.requestEmailOTP()}>{strings.RESEND_OTP}</a>
                                        </div>}

                                        {this.state.promptMFA && this.state.mfaCountDown !== 60 && <div className="col">
                                            <a className="disabled-link">{strings.RESEND_OTP}</a>
                                        </div>}

                                    </div>
                                    <div className="m-login__form-action">
                                        {this.state.loading == false && this.state.promptMFA &&
                                            <button disabled={mfaDisabled} onClick={() => this.verifyMfa()} label="Verify" primary={true} type="submit" label="Sign In" id="m_login_signin_submit" className="btn btn-focus-green m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                                {strings.SUBMIT}
                                            </button>
                                        }
                                        {this.state.loading == true &&
                                            <Loader type="line-scale" active={this.state.loading} className="text-center" />
                                        }
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    let username = '';
    if (state.cognito.user) {
        username = state.cognito.user.getUsername();
    } else if (state.cognito.userName) {
        username = state.cognito.cache.userName;
    }
    return {
        user: state.cognito.user,
        username,
        cognitoState: state.cognito.state,
        email: state.cognito.cache.email,
        config: state.cognito.config,
        userPool: state.cognito.userPool,
        language: state.language
    };
};

const mapDispatchToProps = dispatch => ({
    refreshIdentityCredentials: (username, jwtToken, userPool, config) =>
        refreshIdentityCredentials(username, jwtToken, userPool, config, dispatch),
    clearCache: () => dispatch(Action.clearCache()),
    authenticatedManual: (user) => dispatch(Action.authenticatedManual(user)),
    authenticated: () => dispatch(Action.authenticated())
});


const mergeProps = (stateProps, dispatchProps, ownProps) =>
    Object.assign({}, ownProps, stateProps, {
        onSetToken: (username, jwtToken) =>
            dispatchProps.refreshIdentityCredentials(username, jwtToken, stateProps.userPool, stateProps.config),
        clearCache: dispatchProps.clearCache,
        authenticatedManual: (user) => {
            dispatchProps.authenticatedManual(user)
        },
        authenticated: dispatchProps.authenticated
    });

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps,
)(Checktoken);
