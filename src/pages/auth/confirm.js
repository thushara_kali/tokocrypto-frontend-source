import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { connect } from 'react-redux';
import store from '../../store'
import { Action, authenticate } from '../../core/react-cognito';
import history from '../../history';
import { Loader } from '../../components/tcLoader';
import { NotificationManager } from 'react-notifications';
import { getUserAttributes, sendAttributeVerificationCode } from '../../core/react-cognito/attributes.js';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import NetworkError from '../../errors/networkError';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class Confirm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            verificationCode: '',
            loading: false
        };
    }

    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    componentWillMount() {
        this.onSetLanguage();
        if (this.props.cognitoState !== 'CONFIRMATION_REQUIRED' && history.location.state) {
            // history.replace('/login');
            // 
            this.setState({ loading: true })
            this.props.onSubmit(history.location.state.username, history.location.state.password).then(() => {
                this.setState({ loading: false })
                if (this.props.cognitoState === 'CONFIRMATION_REQUIRED') {
                    NotificationManager.info(strings.CONFIRM_YOUR_EMAIL);
                    // history.replace('/confirm');
                } else {
                    // history.push('/');
                }
            }).catch((error) => {
                this.setState({ loading: false })
                // this.setState({ loading: false, promptMFA: false })
                // NotificationManager.error(error.message);
                NetworkError(error);
                // 
            });
        } else {
            // history.push('/');
        }
    }

    /**
     * TCDOC-005 
     * this function is to resend the verification code, in case user lost or accidentally deleted it.
     */
    resendVerificationCode(){
        this.setState({ loading: true});
        if(this.props.user) {
            this.props.user.resendConfirmationCode((err) => {
                
                NotificationManager.success(strings.CONFIRMATION_CODE_SENT);
                this.setState({ loading: false});
            });
        }
    }

    /**
     * TCDOC-007
     * this function will notifiy google ads when user registered from ads (on condition met)
     */
    mention_google(){
        // thi one for report google ads
        if (process.env.REACT_APP_ENV === 'production') {
            window.gtag_report_conversion('https://tokocrypto.com/'+ this.props.language +'/confirm');
        }
        localStorage.removeItem('partner'); // destroy local storage for google ads partner so user will count just once
    }

    google_ads_mention(){
        // thi one for report google ads
        if (process.env.REACT_APP_ENV === 'production') {
            window.gtag('event', 'conversion', {'send_to': 'AW-726301526/xY7ICNDD2KUBENb2qdoC'});
        }
    }

    /**
     * TCDOC-006
     * this function will send the vericationcode to cognito. please note user cannot lose their cognito session here coz it will be used by cognito 
     * to determine who is the sender
     */
    onSubmit = (event) => {
        event.preventDefault();
        this.setState({ loading: true })
        this.props.user.confirmRegistration(this.state.verificationCode, true, (error) => {
            this.setState({ loading: false })
            if (error) {
                NotificationManager.error(strings.INVALID_CONFIRMATION_CODE);
            } else {
                NotificationManager.success("Verification Successful, Please Login");
                this.google_ads_mention();
                let partner = localStorage.getItem('partner');
                if(partner === 'google' || partner === 'Google'){
                    this.mention_google();
                }
                store.dispatch(Action.partialLogout());
                history.replace('/login', { verify: true });
            }
        });
    }

    changeCode = (event) => {
        this.setState({ verificationCode: event.target.value });
    }

    render() {
        return (
            <div className="m-grid m-grid--hor m-grid--root m-page" style={{ minHeight: "100vh" }}>
                <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style={{ backgroundImage: "url(../assets/media/img/bg/bg-3.jpg)"}}>
                    <div className="m-grid__item m-grid__item--fluid	m-login__wrapper">
                        <div className="m-login__container padding-Bottom-200">
                            <div className="m-login__logo">
                                <a href="#">
                                    <img src="../assets/media/img/logo/TC_new_logo.png"/>
                                </a>
                            </div>
                            <div className="m-login__signin">
                                <div class="m-login__head" style={{textAlign:"center",color:"#9e97aa"}}>
                                    <h3 className="m-login__title" style={{color:"#373d44"}}>{strings.CONFIRM_YOUR_EMAIL}</h3>
                                    <div className="m-login__desc" style={{marginTop:"25px"}}>{strings.CONFIRMAION_CODE_TO_EMAIL_SENT} </div>
                                </div>
                                <form onSubmit={this.onSubmit} className="m-login__form m-form">

                                    <div>
                                        <div className="form-group m-form__group">
                                            <input id="validate_code" value={this.state.verificationCode} onChange={this.changeCode} className="form-control m-input login-inputs"  type="text" placeholder={strings.INPUT_CONFIRMATION_CODE}/>
                                        </div>
                                    </div>
                                    <div style={{ width: "58%", margin:"auto" }}>
                                    <p className="resend-verification mx-auto" onClick={() => this.resendVerificationCode()} style={{marginTop:"40px",marginBottom:"-10px"}}>{strings.LOST_CONFIRMATION_CODE}</p>
                                    </div>


                                    <div className="m-login__form-action">
                                        {this.state.loading == false &&
                                            <button disabled={this.state.verificationCode.length === 0} type="submit" label="Submit" primary={true} id="m_login_signin_submit" className="btn btn-focus-green m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                                {strings.SUBMIT}
                                            </button>
                                        }

                                        {this.state.loading == true &&
                                            <Loader type="line-scale" active={this.state.loading} className="text-center" />
                                        }
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            // <div className="col-12 auth-outer">
            //     <form onSubmit={this.onSubmit}>
            //         <div className="login-wrapper">
            //             <div className="login-fields">
            //                 <h3>Confirm </h3>
            //                 <p>We have sent you a token to your email</p>
            //                 <TextField
            //                     id="name"
            //                     value={this.state.verificationCode}
            //                     onChange={this.changeCode}
            //                     floatingLabelText="Verificaton Code"
            //                     fullWidth={true}
            //                 />

            //                 <div className="pt20">
            //                     {this.state.loading == false &&
            //                         <RaisedButton type="submit" label="Submit" primary={true} fullWidth={true} />
            //                     }
            //                     {this.state.loading == true &&
            //                         <Loader type="line-scale" active={this.state.loading} className="text-center" />
            //                     }
            //                 </div>

            //             </div>
            //         </div>
            //     </form>
            // </div>
        );
    }
}

// const mapStateToProps = (state) => {
//     return {
//         user: state.cognito.user,
//         cognitoState: state.cognito.state
//     };
// };

const mapStateToProps = (state) => {
    let username = '';
    if (state.cognito.user) {
        username = state.cognito.user.getUsername();
    } else if (state.cognito.userName) {
        username = state.cognito.cache.userName;
    }
    return {
        username,
        user: state.cognito.user,
        cognitoState: state.cognito.state,
        email: state.cognito.cache.email,
        config: state.cognito.config,
        userPool: state.cognito.userPool,
        language: state.language
    };
};

const mapDispatchToProps = dispatch => ({
    authenticator: (username, password, userPool, config) => authenticate(username, password, userPool, config, dispatch),
    clearCache: () => dispatch(Action.clearCache()),
});

const mergeProps = (stateProps, dispatchProps, ownProps) =>
    Object.assign({}, ownProps, stateProps, {
        onSubmit: (username, password) =>
            dispatchProps.authenticator(username, password, stateProps.userPool, stateProps.config),
        clearCache: dispatchProps.clearCache,
        setTradingStatus: dispatchProps.setTradingStatus
    });


/**
 * Container for login behaviour, wrapping a login form.
 *
 * Magically provides the following props to the wrapped form:
 *
 *  * username
 *  * onSubmit
 *
 * @example
 * <Login>
 *   <LoginForm />
 * </Login>
 */


export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(Confirm);
