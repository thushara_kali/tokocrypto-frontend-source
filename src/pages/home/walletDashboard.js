import React, { Component } from 'react';
import cryptoFormatter, { cryptoFormatterByCoin } from '../../helpers/cryptoFormatter';
import amountFormatter from '../../helpers/amountFormatter';
import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import {faSignInAlt, faSignOutAlt, faPaperPlane, faQrcode, faPlusCircle, faStar} from '@fortawesome/fontawesome-free-solid';
import EventBus from 'eventing-bus'
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import LocalizedStrings from 'react-localization';
import EllipsisText from "react-ellipsis-text";

let strings = new LocalizedStrings({
	en: enDictionary,
	id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

fontawesome.library.add(faSignInAlt, faSignOutAlt, faPaperPlane, faQrcode, faPlusCircle, faStar)

class WalletDashboard extends Component {

  constructor() {
    super();

    this.state = {
        // Coins' favorite status
        favorite: {
            btc: false,
            eth: false,
            xrp: false,
            dgx: false,
            gusd: false,
            pax: false,
            tusd: false,
            usdt: false,
            swipe: false,
            zil: false,
            favoriteCoin: false,
            favoriteCount: 0
        }
    }
  }

  fetchFavorite() {
    const self = this;
    return new Promise((resolve, reject) => {
        
        let result = localStorage.getItem('userFavorites');
        if(result){
            let resultParsed =  JSON.parse(result);
            
            // self.setState({
            //     favorite: resultParsed
            // }, () => {
            self.setState({
                favorite: {
                    btc: resultParsed.favorite.btc,
                    eth: resultParsed.favorite.eth,
                    xrp: resultParsed.favorite.xrp,
                    dgx: resultParsed.favorite.dgx,
                    gusd: resultParsed.favorite.gusd,
                    pax: resultParsed.favorite.pax,
                    tusd: resultParsed.favorite.tusd,
                    usdc: resultParsed.favorite.usdc,
                    usdt: resultParsed.favorite.usdt,
                    ttc: resultParsed.favorite.ttc,
                    swipe: resultParsed.favorite.swipe,
                    zil: resultParsed.favorite.zil,
                    favoriteCoin: resultParsed.favorite.favoriteCoin,
                    favoriteCount: resultParsed.favorite.favoriteCount
                }
            })
        } else {
            
            this.setState({
                favorite: {
                    btc: false,
                    eth: false,
                    xrp: false,
                    dgx: false,
                    gusd: false,
                    pax: false,
                    tusd: false,
                    usdc: false,
                    usdt: false,
                    ttc: false,
                    swipe: false,
                    zil: false,
                    favoriteCoin: false,
                    favoriteCount: 0
                }
            });
        }
        
    })
  }

  componentDidMount() {
    this.fetchFavorite();
    let self = this;
    EventBus.on('refreshFavorite', () => {
        self.fetchFavorite();
    })
  }

  render() {
    let BTCTotalIDR = 0;
    let BTCBalance = 0;
    let ETHTotalIDR = 0;
    let ETHBalance = 0;
    let XRPTotalIDR = 0;
    let XRPBalance = 0;
    let DGXTotalIDR = 0;
    let DGXBalance = 0;
    let GUSDTotalIDR = 0;
    let GUSDBalance = 0;
    let TUSDTotalIDR = 0;
    let TUSDBalance = 0;
    let USDTTotalIDR = 0;
    let USDTBalance = 0;
    let PAXTotalIDR = 0;
    let PAXBalance = 0;
    let USDCTotalIDR = 0;
    let USDCBalance = 0;
    let TTCTotalIDR = 0;
    let TTCBalance = 0;
    let SWIPETotalIDR = 0;
    let SWIPEBalance = 0;
    let ZILTotalIDR = 0;
    let ZILBalance = 0;

    if(this.props.data.BTCAccount){
        BTCTotalIDR = this.props.data.BTCAccount.Balance * this.props.data.btcEqualRates;
        BTCBalance = this.props.data.BTCAccount.Balance;
    }

    if(this.props.data.ETHAccount){
        ETHTotalIDR = this.props.data.ETHAccount.Balance * this.props.data.ethEqualRates;
        ETHBalance = this.props.data.ETHAccount.Balance;
    }

    if(this.props.data.XRPAccount){
        XRPTotalIDR = this.props.data.XRPAccount.Balance * this.props.data.xrpEqualRates;
        XRPBalance = this.props.data.XRPAccount.Balance;
    }

    if(this.props.data.DGXAccount){
        DGXTotalIDR = this.props.data.DGXAccount.Balance * this.props.data.dgxEqualRates;
        DGXBalance = this.props.data.DGXAccount.Balance;
    }

    if(this.props.data.TUSDAccount){
        TUSDTotalIDR = this.props.data.TUSDAccount.Balance * this.props.data.tusdEqualRates;
        TUSDBalance = this.props.data.TUSDAccount.Balance;
    }

    if(this.props.data.GUSDAccount){
        GUSDTotalIDR = this.props.data.GUSDAccount.Balance * this.props.data.gusdEqualRates;
        GUSDBalance = this.props.data.GUSDAccount.Balance;
    }

    if(this.props.data.PAXAccount){
        PAXTotalIDR = this.props.data.PAXAccount.Balance * this.props.data.paxEqualRates;
        PAXBalance = this.props.data.PAXAccount.Balance;
    }

    if(this.props.data.USDCAccount){
        USDCTotalIDR = this.props.data.USDCAccount.Balance * this.props.data.usdcEqualRates;
        USDCBalance = this.props.data.USDCAccount.Balance;
    }

    if(this.props.data.USDTAccount){
        USDTTotalIDR = this.props.data.USDTAccount.Balance * this.props.data.usdtEqualRates;
        USDTBalance = this.props.data.USDTAccount.Balance;
    }

    if(this.props.data.TTCAccount){
        TTCTotalIDR = this.props.data.TTCAccount.Balance * this.props.data.ttcEqualRates;
        TTCBalance = this.props.data.TTCAccount.Balance;
    }

    if(this.props.data.SWIPEAccount){
        SWIPETotalIDR = this.props.data.SWIPEAccount.Balance * this.props.data.swipeEqualRates;
        SWIPEBalance = this.props.data.SWIPEAccount.Balance;
    }

    if(this.props.data.ZILAccount){
        ZILTotalIDR = this.props.data.ZILAccount.Balance * this.props.data.zilEqualRates;
        ZILBalance = this.props.data.ZILAccount.Balance;
    }

    return (
      
    <div className="col-md-11 dashboard-wallets-accounts-container">
        { (this.state.favorite.favoriteCount < 1 || this.state.favorite.btc ) ? 
        <div className="col-md-12 col-lg-6 col-xl-3">
          <div className="dashboard-wallet-boxes-containers">
            <h4 className="m-widget24__title text-header-dashboard">
                <i className="cc BTC" style={{ float: "left", marginRight: "8px", fontSize: "20px" }}></i>
                Bitcoin {this.state.favorite.btc && <FontAwesomeIcon icon={faStar} color="#f1c40f"/>} 
            </h4>
            <div className="m-widget24">
                <div className="m-widget24__item">
                    <div className="m--space-16"></div>
                    <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                        <EllipsisText text={ cryptoFormatterByCoin( BTCBalance, 'BTC' ).toString() } length={10}/>&nbsp;BTC
                        <span className="dashboard-equalAmounts">(&nbsp; Rp<EllipsisText text={amountFormatter(BTCTotalIDR, 'btc').toString()} length={10}/> &nbsp;)</span>
                    </div>
                    <span className="remaning_daily_limit_container">
                        <span style={{marginLeft:"0px"}} className="m-widget24__change">
                            {strings.REMAINING_DAILY_WITHDRAW_LIMIT}
                        </span>
                        <span className="m-widget24__change remaning_daily_limit_crypto_limit">
                            { cryptoFormatterByCoin(this.props.data.BTCUsageLeft, 'BTC' )} BTC
                        </span>
                    </span>
                </div>
                <div className="row" style={{width:"100%", marginTop:"40px"}}>
                    <div className="col-md-12 wallet-button-container-dashboard">
                        <div className="col-md-6 wallet-button-container-button" style={{borderRight:"1px solid rgba(184, 184, 184, 0.48)"}}>
                            <button disabled={this.props.data.balanceLoading || this.props.data.BTCUsageLeft === 0} onClick={() => this.props.send('btc')} className="btn btn-without-outline-blue" type="button">
                                <FontAwesomeIcon icon={faPaperPlane} />&nbsp;&nbsp; {strings.SEND}
                            </button>
                        </div>
                        <div className="col-md-6 wallet-button-container-button">
                            <button disabled={this.props.data.balanceLoading} onClick={() => this.props.receive('btc')} className="btn btn-without-outline-blue" type="button">
                                <FontAwesomeIcon icon={faQrcode} />&nbsp;&nbsp; {strings.RECEIVE}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
        : null
        }

        { (this.state.favorite.favoriteCount < 1 || this.state.favorite.eth ) ?
        <div className="col-md-12 col-lg-6 col-xl-3">
            <div className="dashboard-wallet-boxes-containers">
                <h4 className="m-widget24__title text-header-dashboard">
                    <i className="cc ETH-alt" style={{ float: "left", marginRight: "8px", fontSize: "20px" }}></i>
                    Ethereum {this.state.favorite.eth && <FontAwesomeIcon icon={faStar} color="#f1c40f"/>} 
                </h4>
                <div className="m-widget24">
                    <div className="m-widget24__item">
                        <div className="m--space-16"></div>
                        <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                            <EllipsisText text={ cryptoFormatterByCoin(ETHBalance, 'ETH').toString() } length={10}/>&nbsp;ETH
                            <span className="dashboard-equalAmounts">(&nbsp; Rp<EllipsisText text={amountFormatter(ETHTotalIDR, 'eth').toString()} length={10}/> &nbsp;)</span>
                        </div>
                        <span className="remaning_daily_limit_container">
                            <span style={{marginLeft:"0px"}} className="m-widget24__change">
                                {strings.REMAINING_DAILY_WITHDRAW_LIMIT}
                            </span>
                            <span className="m-widget24__change remaning_daily_limit_crypto_limit">
                                { cryptoFormatterByCoin(this.props.data.ETHUsageLeft, 'ETH') } ETH
                            </span>
                        </span>
                    </div>
                    <div className="row" style={{width:"100%", marginTop:"40px"}}>
                        <div className="col-md-12 wallet-button-container-dashboard">
                            <div className="col-md-6 wallet-button-container-button" style={{borderRight:"1px solid rgba(184, 184, 184, 0.48)"}}>
                                <button disabled={this.props.data.balanceLoading || this.props.data.ETHUsageLeft === 0} onClick={() => this.props.send('eth')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faPaperPlane} />&nbsp;&nbsp; {strings.SEND}
                                </button>
                            </div>
                            <div className="col-md-6 wallet-button-container-button">
                                <button disabled={this.props.data.balanceLoading} onClick={() => this.props.receive('eth')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faQrcode} />&nbsp;&nbsp; {strings.RECEIVE}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        : null 
        }

        { (this.state.favorite.favoriteCount < 1 || this.state.favorite.xrp ) ?
        <div className="col-md-12 col-lg-6 col-xl-3">
            <div className="dashboard-wallet-boxes-containers">
                <h4 className="m-widget24__title text-header-dashboard">
                    <i className="cc XRP-alt" style={{ float: "left", marginRight: "8px", fontSize: "20px", color:"#0099d2" }}></i>
                    Ripple {this.state.favorite.xrp && <FontAwesomeIcon icon={faStar} color="#f1c40f"/>} 
                </h4>
                <div className="m-widget24">
                    <div className="m-widget24__item">
                        <div className="m--space-16"></div>
                        {this.props.data.XRPAccount?
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                <EllipsisText text={ cryptoFormatterByCoin(XRPBalance, 'XRP').toString() } length={10}/>&nbsp;XRP
                                <span className="dashboard-equalAmounts">(&nbsp; Rp<EllipsisText text={amountFormatter(XRPTotalIDR, 'xrp').toString()} length={10}/> &nbsp;)</span>
                            </div>:
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                0 XRP
                                <span className="dashboard-equalAmounts">(&nbsp; 0 IDR &nbsp;)</span>
                            </div>
                        }
                        <span className="remaning_daily_limit_container">
                            <span style={{marginLeft:"0px"}} className="m-widget24__change">
                                {strings.REMAINING_DAILY_WITHDRAW_LIMIT}
                            </span>
                            <span className="m-widget24__change remaning_daily_limit_crypto_limit">
                                { cryptoFormatterByCoin(this.props.data.XRPUsageLeft, 'XRP') } XRP
                            </span>
                        </span>
                    </div>
                    <div className="row" style={{width:"100%", marginTop:"40px"}}>
                        <div className="col-md-12 wallet-button-container-dashboard">
                            <div className="col-md-6 wallet-button-container-button" style={{borderRight:"1px solid rgba(184, 184, 184, 0.48)"}}>
                                <button disabled={this.props.data.balanceLoading || this.props.data.XRPUsageLeft === 0 || this.props.data.XRPAccountFound} onClick={() => this.props.send('xrp')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faPaperPlane} />&nbsp;&nbsp; {strings.SEND}
                                </button>
                            </div>
                            <div className="col-md-6 wallet-button-container-button">
                                <button disabled={this.props.data.balanceLoading || this.props.data.XRPAccountFound} onClick={() => this.props.receive('xrp')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faQrcode} />&nbsp;&nbsp; {strings.RECEIVE}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        : null 
        }

        { (this.state.favorite.favoriteCount < 1 || this.state.favorite.dgx ) ?
        <div className="col-md-12 col-lg-6 col-xl-3">
            <div className="dashboard-wallet-boxes-containers">
                <h4 className="m-widget24__title text-header-dashboard">
                    <img className="custom-coin-image-dashboard" src="../../assets/media/img/logo/dgx_token.png" alt="" />
                    DGX {this.state.favorite.dgx && <FontAwesomeIcon icon={faStar} color="#f1c40f"/>} 
                </h4>
                <div className="m-widget24">
                    <div className="m-widget24__item">
                        <div className="m--space-16"></div>
                        {this.props.data.DGXAccount?
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                <EllipsisText text={ cryptoFormatterByCoin(DGXBalance, 'DGX').toString() } length={10}/>&nbsp;DGX
                                <span className="dashboard-equalAmounts">(&nbsp; Rp<EllipsisText text={amountFormatter(DGXTotalIDR, 'dgx').toString()} length={10}/> &nbsp;)</span>
                            </div>:
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                0 DGX
                                <span className="dashboard-equalAmounts">(&nbsp; Rp0 &nbsp;)</span>
                            </div>
                        }
                        <span className="remaning_daily_limit_container">
                            <span style={{marginLeft:"0px"}} className="m-widget24__change">
                                {strings.REMAINING_DAILY_WITHDRAW_LIMIT}
                            </span>
                            <span className="m-widget24__change remaning_daily_limit_crypto_limit">
                                { cryptoFormatterByCoin(this.props.data.DGXUsageLeft, 'DGX') } DGX
                            </span>
                        </span>
                    </div>
                    <div className="row" style={{width:"100%", marginTop:"40px"}}>
                        <div className="col-md-12 wallet-button-container-dashboard">
                            <div className="col-md-6 wallet-button-container-button" style={{borderRight:"1px solid rgba(184, 184, 184, 0.48)"}}>
                                <button disabled={this.props.data.balanceLoading || this.props.data.DGXUsageLeft === 0 || this.props.data.DGXAccountFound} onClick={() => this.props.send('dgx')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faPaperPlane} />&nbsp;&nbsp; {strings.SEND}
                                </button>
                            </div>
                            <div className="col-md-6 wallet-button-container-button">
                                <button disabled={this.props.data.balanceLoading || this.props.data.DGXAccountFound} onClick={() => this.props.receive('dgx')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faQrcode} />&nbsp;&nbsp; {strings.RECEIVE}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        : null 
        }

        { this.state.favorite.gusd ?
        <div className="col-md-12 col-lg-6 col-xl-3 walletContainer">
            <div className="dashboard-wallet-boxes-containers">
                <h4 className="m-widget24__title text-header-dashboard">
                    <img className="custom-coin-image-dashboard" src="../../assets/media/img/logo/gemini.png" alt="" />
                    GUSD {this.state.favorite.gusd && <FontAwesomeIcon icon={faStar} color="#f1c40f"/>} 
                </h4>
                <div className="m-widget24">
                    <div className="m-widget24__item">
                        <div className="m--space-16"></div>
                        {this.props.data.GUSDAccount?
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                <EllipsisText text={ cryptoFormatterByCoin(GUSDBalance, 'GUSD').toString() } length={10}/>&nbsp;GUSD
                                <span className="dashboard-equalAmounts">(&nbsp; Rp<EllipsisText text={amountFormatter(GUSDTotalIDR, 'gusd').toString()} length={10}/> &nbsp;)</span>
                            </div>:
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                0 GUSD
                                <span className="dashboard-equalAmounts">(&nbsp; Rp0 &nbsp;)</span>
                            </div>
                        }
                        <span className="remaning_daily_limit_container">
                            <span style={{marginLeft:"0px"}} className="m-widget24__change">
                                {strings.REMAINING_DAILY_WITHDRAW_LIMIT}
                            </span>
                            <span className="m-widget24__change remaning_daily_limit_crypto_limit">
                                { cryptoFormatterByCoin(this.props.data.GUSDUsageLeft, 'GUSD') } GUSD
                            </span>
                        </span>
                    </div>
                    <div className="row" style={{width:"100%", marginTop:"40px"}}>
                        <div className="col-md-12 wallet-button-container-dashboard">
                            <div className="col-md-6 wallet-button-container-button" style={{borderRight:"1px solid rgba(184, 184, 184, 0.48)"}}>
                                <button disabled={this.props.data.balanceLoading || this.props.data.GUSDUsageLeft === 0 || this.props.data.GUSDAccountFound} onClick={() => this.props.send('gusd')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faPaperPlane} />&nbsp;&nbsp; {strings.SEND}
                                </button>
                            </div>
                            <div className="col-md-6 wallet-button-container-button">
                                <button disabled={this.props.data.balanceLoading || this.props.data.GUSDAccountFound} onClick={() => this.props.receive('gusd')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faQrcode} />&nbsp;&nbsp; {strings.RECEIVE}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>       
        : null 
        }

        { this.state.favorite.tusd ?
        <div className="col-md-12 col-lg-6 col-xl-3 walletContainer">
            <div className="dashboard-wallet-boxes-containers">
                <h4 className="m-widget24__title text-header-dashboard">
                    <img className="custom-coin-image-dashboard" src="../../assets/media/img/logo/tusd.png" alt="" />
                    TUSD {this.state.favorite.tusd && <FontAwesomeIcon icon={faStar} color="#f1c40f"/>} 
                </h4>
                <div className="m-widget24">
                    <div className="m-widget24__item">
                        <div className="m--space-16"></div>
                        {this.props.data.TUSDAccount?
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                <EllipsisText text={ cryptoFormatterByCoin(TUSDBalance, 'TUSD').toString() } length={10}/>&nbsp;TUSD
                                <span className="dashboard-equalAmounts">(&nbsp; Rp<EllipsisText text={amountFormatter(TUSDTotalIDR, 'tusd').toString()} length={10}/> &nbsp;)</span>
                            </div>:
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                0 TUSD
                                <span className="dashboard-equalAmounts">(&nbsp; Rp 0 &nbsp;)</span>
                            </div>
                        }
                        <span className="remaning_daily_limit_container">
                            <span style={{marginLeft:"0px"}} className="m-widget24__change">
                                {strings.REMAINING_DAILY_WITHDRAW_LIMIT}
                            </span>
                            <span className="m-widget24__change remaning_daily_limit_crypto_limit">
                                { cryptoFormatterByCoin(this.props.data.TUSDUsageLeft, 'TUSD') } TUSD
                            </span>
                        </span>
                    </div>
                  <div className="row" style={{width:"100%", marginTop:"40px"}}>
                      <div className="col-md-12 wallet-button-container-dashboard">
                          <div className="col-md-6 wallet-button-container-button" style={{borderRight:"1px solid rgba(184, 184, 184, 0.48)"}}>
                              <button disabled={this.props.data.balanceLoading || this.props.data.TUSDUsageLeft === 0 || this.props.data.TUSDAccountFound} onClick={() => this.props.send('tusd')} className="btn btn-without-outline-blue" type="button">
                                  <FontAwesomeIcon icon={faPaperPlane} />&nbsp;&nbsp; {strings.SEND}
                              </button>
                          </div>
                          <div className="col-md-6 wallet-button-container-button">
                              <button disabled={this.props.data.balanceLoading || this.props.data.TUSDAccountFound} onClick={() => this.props.receive('tusd')} className="btn btn-without-outline-blue" type="button">
                                  <FontAwesomeIcon icon={faQrcode} />&nbsp;&nbsp; {strings.RECEIVE}
                              </button>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>  
        : null 
        }

        { this.state.favorite.pax ?
        <div className="col-md-12 col-lg-6 col-xl-3 walletContainer">
            <div className="dashboard-wallet-boxes-containers">
                <h4 className="m-widget24__title text-header-dashboard">
                    <img className="custom-coin-image-dashboard" src="../../assets/media/img/logo/pax.png" alt="" />
                    PAX {this.state.favorite.pax && <FontAwesomeIcon icon={faStar} color="#f1c40f"/>} 
                </h4>
                <div className="m-widget24">
                    <div className="m-widget24__item">
                        <div className="m--space-16"></div>
                        {this.props.data.PAXAccount?
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                <EllipsisText text={ cryptoFormatterByCoin(PAXBalance, 'PAX').toString() } length={10}/>&nbsp;PAX
                                <span className="dashboard-equalAmounts">(&nbsp; Rp<EllipsisText text={amountFormatter(PAXTotalIDR), 'pax'.toString()} length={10}/> &nbsp;)</span>
                            </div>:
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                0 PAX
                                <span className="dashboard-equalAmounts">(&nbsp; 0 IDR &nbsp;)</span>
                            </div>
                        }
                        <span className="remaning_daily_limit_container">
                            <span style={{marginLeft:"0px"}} className="m-widget24__change">
                                {strings.REMAINING_DAILY_WITHDRAW_LIMIT}
                            </span>
                            <span className="m-widget24__change remaning_daily_limit_crypto_limit">
                                { cryptoFormatterByCoin(this.props.data.PAXUsageLeft, 'PAX') } PAX
                            </span>
                        </span>
                    </div>
                    <div className="row" style={{width:"100%", marginTop:"40px"}}>
                        <div className="col-md-12 wallet-button-container-dashboard">
                            <div className="col-md-6 wallet-button-container-button" style={{borderRight:"1px solid rgba(184, 184, 184, 0.48)"}}>
                                <button disabled={this.props.data.balanceLoading || this.props.data.PAXUsageLeft === 0 || this.props.data.PAXAccountFound} onClick={() => this.props.send('pax')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faPaperPlane} />&nbsp;&nbsp; {strings.SEND}
                                </button>
                            </div>
                            <div className="col-md-6 wallet-button-container-button">
                                <button disabled={this.props.data.balanceLoading || this.props.data.PAXAccountFound} onClick={() => this.props.receive('pax')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faQrcode} />&nbsp;&nbsp; {strings.RECEIVE}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        : null 
        }

        { this.state.favorite.usdc ?
        <div className="col-md-12 col-lg-6 col-xl-3 walletContainer">
            <div className="dashboard-wallet-boxes-containers">
                <h4 className="m-widget24__title text-header-dashboard">
                    <img className="custom-coin-image-dashboard" src="../../assets/media/img/logo/usdc.png" alt="" />
                    USDC {this.state.favorite.usdc && <FontAwesomeIcon icon={faStar} color="#f1c40f"/>} 
                </h4>
                <div className="m-widget24">
                    <div className="m-widget24__item">
                        <div className="m--space-16"></div>
                        {this.props.data.USDCAccount?
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                <EllipsisText text={ cryptoFormatterByCoin(USDCBalance, 'USDC').toString() } length={10}/>&nbsp;USDC
                                    <span className="dashboard-equalAmounts">(&nbsp; Rp<EllipsisText text={amountFormatter(USDCTotalIDR, 'usdc').toString()} length={10}/> &nbsp;)</span>
                            </div>:
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                0 USDC
                                <span className="dashboard-equalAmounts">(&nbsp; 0 IDR &nbsp;)</span>
                            </div>
                        }
                        <span className="remaning_daily_limit_container">
                            <span style={{marginLeft:"0px"}} className="m-widget24__change">
                                {strings.REMAINING_DAILY_WITHDRAW_LIMIT}
                            </span>
                            <span className="m-widget24__change remaning_daily_limit_crypto_limit">
                                { cryptoFormatterByCoin(this.props.data.USDCUsageLeft, 'USDC') } USDC
                            </span>
                        </span>
                    </div>
                    <div className="row" style={{width:"100%", marginTop:"40px"}}>
                        <div className="col-md-12 wallet-button-container-dashboard">
                            <div className="col-md-6 wallet-button-container-button" style={{borderRight:"1px solid rgba(184, 184, 184, 0.48)"}}>
                                <button disabled={this.props.data.balanceLoading || this.props.data.USDCUsageLeft === 0 || this.props.data.USDCAccountFound} onClick={() => this.props.send('usdc')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faPaperPlane} />&nbsp;&nbsp; {strings.SEND}
                                </button>
                            </div>
                            <div className="col-md-6 wallet-button-container-button">
                                <button disabled={this.props.data.balanceLoading || this.props.data.USDCAccountFound} onClick={() => this.props.receive('usdc')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faQrcode} />&nbsp;&nbsp; {strings.RECEIVE}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        : null 
        }

        { this.state.favorite.usdt ?
        <div className="col-md-12 col-lg-6 col-xl-3 walletContainer">
            <div className="dashboard-wallet-boxes-containers">
                <h4 className="m-widget24__title text-header-dashboard">
                    <img className="custom-coin-image-dashboard" src="../../assets/media/img/logo/usdt.png" alt="" />
                    USDT {this.state.favorite.usdt && <FontAwesomeIcon icon={faStar} color="#f1c40f"/>} 
                </h4>
                <div className="m-widget24">
                    <div className="m-widget24__item">
                        <div className="m--space-16"></div>
                        {this.props.data.USDTAccount?
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                <EllipsisText text={ cryptoFormatterByCoin(USDTBalance, 'USDT').toString() } length={10}/>&nbsp;USDT
                                    <span className="dashboard-equalAmounts">(&nbsp; Rp<EllipsisText text={amountFormatter(USDTTotalIDR, 'usdt').toString()} length={10}/> &nbsp;)</span>
                            </div>:
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                0 USDT
                                <span className="dashboard-equalAmounts">(&nbsp; 0 IDR &nbsp;)</span>
                            </div>
                        }
                        <span className="remaning_daily_limit_container">
                            <span style={{marginLeft:"0px"}} className="m-widget24__change">
                                {strings.REMAINING_DAILY_WITHDRAW_LIMIT}
                            </span>
                            <span className="m-widget24__change remaning_daily_limit_crypto_limit">
                                { cryptoFormatterByCoin(this.props.data.USDTUsageLeft, 'USDT') } USDT
                            </span>
                        </span>
                    </div>
                    <div className="row" style={{width:"100%", marginTop:"40px"}}>
                        <div className="col-md-12 wallet-button-container-dashboard">
                            <div className="col-md-6 wallet-button-container-button" style={{borderRight:"1px solid rgba(184, 184, 184, 0.48)"}}>
                                <button disabled={this.props.data.balanceLoading || this.props.data.USDTUsageLeft === 0 || this.props.data.USDTAccountFound} onClick={() => this.props.send('usdt')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faPaperPlane} />&nbsp;&nbsp; {strings.SEND}
                                </button>
                            </div>
                            <div className="col-md-6 wallet-button-container-button">
                                <button disabled={this.props.data.balanceLoading || this.props.data.USDTAccountFound} onClick={() => this.props.receive('usdt')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faQrcode} />&nbsp;&nbsp; {strings.RECEIVE}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
        : null 
        }
        { this.state.favorite.ttc ?
        <div className="col-md-12 col-lg-6 col-xl-3 walletContainer">
            <div className="dashboard-wallet-boxes-containers">
                <h4 className="m-widget24__title text-header-dashboard">
                    <img className="custom-coin-image-dashboard" src="../../assets/media/img/logo/ttc.png" alt="" />
                    TTC {this.state.favorite.ttc && <FontAwesomeIcon icon={faStar} color="#f1c40f"/>} 
                </h4>
                <div className="m-widget24">
                    <div className="m-widget24__item">
                        <div className="m--space-16"></div>
                        {this.props.data.TTCAccount?
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                <EllipsisText text={ cryptoFormatterByCoin(TTCBalance, 'TTC').toString() } length={10}/>&nbsp;TTC
                                    <span className="dashboard-equalAmounts">(&nbsp; Rp<EllipsisText text={amountFormatter(TTCTotalIDR, 'ttc').toString()} length={10}/> &nbsp;)</span>
                            </div>:
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                0 TTC
                                <span className="dashboard-equalAmounts">(&nbsp; 0 IDR &nbsp;)</span>
                            </div>
                        }
                        <span className="remaning_daily_limit_container">
                            <span style={{marginLeft:"0px"}} className="m-widget24__change">
                                {strings.REMAINING_DAILY_WITHDRAW_LIMIT}
                            </span>
                            <span className="m-widget24__change remaning_daily_limit_crypto_limit">
                                { cryptoFormatterByCoin(this.props.data.TTCUsageLeft, 'TTC') } TTC
                            </span>
                        </span>
                    </div>
                    <div className="row" style={{width:"100%", marginTop:"40px"}}>
                        <div className="col-md-12 wallet-button-container-dashboard">
                            <div className="col-md-6 wallet-button-container-button" style={{borderRight:"1px solid rgba(184, 184, 184, 0.48)"}}>
                                <button disabled={this.props.data.balanceLoading || this.props.data.TTCUsageLeft === 0 || this.props.data.TTCAccountFound} onClick={() => this.props.send('ttc')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faPaperPlane} />&nbsp;&nbsp; {strings.SEND}
                                </button>
                            </div>
                            <div className="col-md-6 wallet-button-container-button">
                                <button disabled={this.props.data.balanceLoading || this.props.data.TTCAccountFound} onClick={() => this.props.receive('ttc')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faQrcode} />&nbsp;&nbsp; {strings.RECEIVE}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
        : null 
        }

        { this.state.favorite.swipe ?
        <div className="col-md-12 col-lg-6 col-xl-3 walletContainer">
            <div className="dashboard-wallet-boxes-containers">
                <h4 className="m-widget24__title text-header-dashboard">
                    <img className="custom-coin-image-dashboard" src="../../assets/media/img/logo/swipe.png" alt="" />
                    SWIPE {this.state.favorite.swipe && <FontAwesomeIcon icon={faStar} color="#f1c40f"/>} 
                </h4>
                <div className="m-widget24">
                    <div className="m-widget24__item">
                        <div className="m--space-16"></div>
                        {this.props.data.SWIPEAccount?
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                <EllipsisText text={ cryptoFormatterByCoin(SWIPEBalance, 'SWIPE').toString() } length={10}/>&nbsp;SWIPE
                                    <span className="dashboard-equalAmounts">(&nbsp; Rp<EllipsisText text={amountFormatter(SWIPETotalIDR, 'swipe').toString()} length={10}/> &nbsp;)</span>
                            </div>:
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                0 SWIPE
                                <span className="dashboard-equalAmounts">(&nbsp; 0 IDR &nbsp;)</span>
                            </div>
                        }
                        <span className="remaning_daily_limit_container">
                            <span style={{marginLeft:"0px"}} className="m-widget24__change">
                                {strings.REMAINING_DAILY_WITHDRAW_LIMIT}
                            </span>
                            <span className="m-widget24__change remaning_daily_limit_crypto_limit">
                                { cryptoFormatterByCoin(this.props.data.SWIPEUsageLeft, 'SWIPE') } SWIPE
                            </span>
                        </span>
                    </div>
                    <div className="row" style={{width:"100%", marginTop:"40px"}}>
                        <div className="col-md-12 wallet-button-container-dashboard">
                            <div className="col-md-6 wallet-button-container-button" style={{borderRight:"1px solid rgba(184, 184, 184, 0.48)"}}>
                                <button disabled={this.props.data.balanceLoading || this.props.data.SWIPEUsageLeft === 0 || this.props.data.SWIPEAccountFound} onClick={() => this.props.send('swipe')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faPaperPlane} />&nbsp;&nbsp; {strings.SEND}
                                </button>
                            </div>
                            <div className="col-md-6 wallet-button-container-button">
                                <button disabled={this.props.data.balanceLoading || this.props.data.SWIPEAccountFound} onClick={() => this.props.receive('swipe')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faQrcode} />&nbsp;&nbsp; {strings.RECEIVE}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
        : null 
        }

        { this.state.favorite.zil ?
        <div className="col-md-12 col-lg-6 col-xl-3 walletContainer">
            <div className="dashboard-wallet-boxes-containers">
                <h4 className="m-widget24__title text-header-dashboard">
                    <img className="custom-coin-image-dashboard" src="../../assets/media/img/logo/zil.png" alt="" />
                    ZIL {this.state.favorite.zil && <FontAwesomeIcon icon={faStar} color="#f1c40f"/>} 
                </h4>
                <div className="m-widget24">
                    <div className="m-widget24__item">
                        <div className="m--space-16"></div>
                        {this.props.data.ZILAccount?
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                <EllipsisText text={ cryptoFormatterByCoin(ZILBalance, 'ZIL').toString() } length={10}/>&nbsp;ZIL
                                    <span className="dashboard-equalAmounts">(&nbsp; Rp<EllipsisText text={amountFormatter(ZILTotalIDR, 'zil').toString()} length={10}/> &nbsp;)</span>
                            </div>:
                            <div className="dashboard-accounts-header-custom m-widget24__stats m--font-black mobile-balance dashboard-equalAmounts-container">
                                0 ZIL
                                <span className="dashboard-equalAmounts">(&nbsp; 0 IDR &nbsp;)</span>
                            </div>
                        }
                        <span className="remaning_daily_limit_container">
                            <span style={{marginLeft:"0px"}} className="m-widget24__change">
                                {strings.REMAINING_DAILY_WITHDRAW_LIMIT}
                            </span>
                            <span className="m-widget24__change remaning_daily_limit_crypto_limit">
                                { cryptoFormatterByCoin(this.props.data.ZILUsageLeft, 'ZIL') } ZIL
                            </span>
                        </span>
                    </div>
                    <div className="row" style={{width:"100%", marginTop:"40px"}}>
                        <div className="col-md-12 wallet-button-container-dashboard">
                            <div className="col-md-6 wallet-button-container-button" style={{borderRight:"1px solid rgba(184, 184, 184, 0.48)"}}>
                                <button disabled={this.props.data.balanceLoading || this.props.data.ZILUsageLeft === 0 || this.props.data.ZILAccountFound} onClick={() => this.props.send('zil')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faPaperPlane} />&nbsp;&nbsp; {strings.SEND}
                                </button>
                            </div>
                            <div className="col-md-6 wallet-button-container-button">
                                <button disabled={this.props.data.balanceLoading || this.props.data.ZILAccountFound} onClick={() => this.props.receive('zil')} className="btn btn-without-outline-blue" type="button">
                                    <FontAwesomeIcon icon={faQrcode} />&nbsp;&nbsp; {strings.RECEIVE}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
        : null 
        }
    </div>
      
    )
  }
}

export default WalletDashboard;