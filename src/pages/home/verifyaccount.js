import React, { Component } from 'react'
import { NotificationManager } from 'react-notifications'
import Select from 'react-select'
import countriesPhoneCode from '../../countriesPhoneCode.json'
import S3Upload from '../../components/S3Upload'
import DatePicker from 'react-datepicker';
import { connect } from 'react-redux';
import actions from '../../core/actions/client/home';
import moment from 'moment';
import NetworkError from '../../errors/networkError';
import { Loader } from 'react-loaders';
import history from '../../history';
import userActions  from '../../core/actions/client/user.js'
import { bindActionCreators } from 'redux';
import homeActions  from '../../core/actions/client/home.js'
import { pixelID } from '../../config/pixel';

let ReactPixel = null;

class VerifyAccount extends Component {

    constructor() {
        super()
        this.state = {
            countries: [],
            country: 'ID',
            phoneSectionOpen: true,
            phoneSectionVerified: false,
            phone: '',
            idSectionOpen: false,
            idSectionVerified: false,
            fullName: '',
            nationality: 'Indonesia',
            dateOfBirthday: '',
            city: '',
            address: '',
            zipCode: '',
            identityNumber: '',
            fileIdentityPath: '',
            filePhotoWithIdentityPath: '',
            fileProofAddressDocumentPath: '',
            questionaireSectionOpen: false,
            questionaireSectionVerified: false,
            a1: '',
            a2: '',
            a3: '',
            a4: '',
            a5: '',
            incomeSectionOpen: false,
            incomeSectionVerfied: false,
            loading: false
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.value
            this.setState(obj)
        }
    }

    componentDidMount() {
        this.initFBPixel();
        let countries = [];
        countriesPhoneCode.map(c => {
            countries.push({
                label: c.label,
                value: c.label
            });
        });
        this.setState({ countries: countries });
        if (this.props.kyc.status == 'pending') {
            this.fetchKYC();
        }
    }

    verifyPhone() {
        // TODO
        if (this.state.phone.length == 0) {
            NotificationManager.warning('Phone number cannot be blank')
        } else {
            this.setState({
                phoneSectionOpen: false,
                phoneSectionVerified: true,
                idSectionOpen: true
            })
        }
    }

    verifyId() {
        if (this.state.fullName.length == 0 ||
            this.state.nationality.length == 0 ||
            this.state.dateOfBirthday.length == 0 ||
            this.state.city.length == 0 ||
            this.state.address.length == 0 ||
            this.state.zipCode.length == 0 ||
            this.state.identityNumber.length == 0 ||
            this.state.fileIdentityPath.length == 0 ||
            this.state.filePhotoWithIdentityPath.length == 0 ||
            this.state.fileProofAddressDocumentPath.length == 0 ||
            this.state.fileProofAddressDocumentPath.length == 0
        ) {
            NotificationManager.warning('Please answer all questions');
        } else {
            this.setState({
                idSectionVerified: true,
                idSectionOpen: false,
                questionaireSectionOpen: true
            });
        }
    }

    answerQuestions() {
        if (this.state.a1.length == 0 ||
            this.state.a2.length == 0 ||
            this.state.a3.length == 0 ||
            this.state.a4.length == 0
        ) {
            NotificationManager.warning('Please answer all questions');
        } else {
            // TODO
            this.setState({
                questionaireSectionVerified: true,
                questionaireSectionOpen: false,
                incomeSectionOpen: true
            })
        }
    }

    answerIncome() {
        this.setState({
            incomeSectionVerified: true,
            incomeSectionOpen: false
        });
    }

    closeAll() {
        this.setState({
            phoneSectionOpen: false,
            idSectionOpen: false,
            questionaireSectionOpen: false,
            incomeSectionOpen: false
        })
    }

    openSection(section) {
        this.closeAll();
        let obj = Object.assign({});
        obj[section] = true;
        this.setState(obj);
    }

    getToken() {
        let username = this.props.user.username;
        let clientId = this.props.user.pool.clientId;
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];
        return token;
    }

    fetchKYC() {
        this.props.fetchKYC(this.getToken()).then(res => {
            this.setState({
                phone: res.data.phoneNumber,
                fullName: res.data.FullName,
                nationality: res.data.Nationality,
                dateOfBirthday: moment(res.data.DateOfBirthday, "DD-MM-YYYY"),
                city: res.data.City,
                address: res.data.Address,
                zipCode: res.data.ZipCode,
                identityNumber: res.data.IdentityNumber,
                fileIdentityPath: res.data.FileIdentityPath,
                filePhotoWithIdentityPath: res.data.FilePhotoWithIdentityPath,
                fileProofAddressDocumentPath: res.data.FileProofAddressDocumentPath,
                a1: res.data.AdditionalVerification[0].Answer,
                a2: res.data.AdditionalVerification[1].Answer,
                a3: res.data.AdditionalVerification[2].Answer,
                a4: res.data.AdditionalVerification[3].Answer,
                a5: res.data.AdditionalVerification[4].Answer
            })
        })
            .catch(err => {
                NetworkError(err);
            })
    }

    fetchUserPermission(){
        let token = this.getToken();
        userActions.fetchUser(token)
        .then(res => {
            let data = res.data.data;            
            
            
            this.props.actions.setTradingStatus(data.user); 
            if(data.user == "rejected"){
                if(data.kyc == "pending"){
                    history.replace('/verifyaccount');
                }else{
                    history.replace('/trading-unavailable');
                }                
            } else {
                history.replace('/');
            } 
        })
        .catch(err => {
            NetworkError(err);
            // history.replace('/login');
            
        })
    }

    confirmMyAccount() {

        if (
            this.state.phone.length == 0 ||
            this.state.fullName.length == 0 ||
            this.state.nationality.length == 0 ||
            this.state.dateOfBirthday.length == 0 ||
            this.state.city.length == 0 ||
            this.state.address.length == 0 ||
            this.state.zipCode.length == 0 ||
            this.state.identityNumber.length == 0 ||
            this.state.fileIdentityPath.length == 0 ||
            this.state.filePhotoWithIdentityPath.length == 0 ||
            this.state.fileProofAddressDocumentPath.length == 0 ||
            this.state.fileProofAddressDocumentPath.length == 0 ||
            this.state.a1.length == 0 ||
            this.state.a2.length == 0 ||
            this.state.a3.length == 0 ||
            this.state.a4.length == 0 ||
            this.state.a5.length == 0
        ) {
            NotificationManager.warning('Please answer all questions');
        } else {
            let data = {
                country: "ID",
                phoneNumber: '+62' + this.state.phone,
                fullName: this.state.fullName,
                nationality: this.state.nationality,
                dateOfBirthday: this.state.dateOfBirthday.format("DD-MM-YYYY"),
                city: this.state.city,
                address: this.state.address,
                zipCode: this.state.zipCode,
                identityNumber: this.state.identityNumber,
                fileIdentityPath: this.state.fileIdentityPath,
                filePhotoWithIdentityPath: this.state.filePhotoWithIdentityPath,
                fileProofAddressDocumentPath: this.state.fileProofAddressDocumentPath,
                additionalVerification: [
                    {
                        question: "1",
                        answer: this.state.a1
                    },
                    {
                        question: "2",
                        answer: this.state.a2
                    },
                    {
                        question: "3",
                        answer: this.state.a3
                    },
                    {
                        question: "4",
                        answer: this.state.a4
                    },
                    {
                        question: "5",
                        answer: this.state.a5
                    }
                ]
            }
            this.setState({ loading: true });
            this.props.updateKYC(this.getToken(), data).then(res => {
                // history.push('/');
                this.setState({ loading: false });
                this.fetchUserPermission()
                ReactPixel.track( 'Lead', {content_name: 'kyc success', name: this.state.fullName} );
            })
                .catch(err => {
                    NetworkError(err);
                    this.setState({ loading: false });
                });
        }
    }

    initFBPixel() {
        ReactPixel =  require('react-facebook-pixel').default;
        ReactPixel.init(pixelID);
        ReactPixel.pageView(); 
        ReactPixel.track( 'ViewContent', {content_name: 'kyc'} ); 
        ReactPixel.track( 'Lead', {content_name: 'kyc start'} );
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h4 className="text-center">Account Verification</h4>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <h6 className="text-center">This should take you less than 30 minutes</h6>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <div className="bg-muted verify-header">
                            <div className="container">
                                <div className="row">
                                    <h5 className="col-md-11 col-10">Select Your Country</h5>
                                    <i className="color-success fa fa-check-circle col-md-1 col-1 verified-icon"></i>
                                </div>
                            </div>
                        </div>
                        <div className="bg-muted verify-header"
                            onClick={() => this.openSection('phoneSectionOpen')}>
                            <div className="container">
                                <div className="row">
                                    <h5 className="col-md-11 col-8">Phone Number Verification</h5>
                                    {this.state.phoneSectionVerified ?
                                        <i className="color-success fa fa-check-circle col-md-1 verified-icon"></i> :
                                        <div style={{ marginLeft: '-10px' }}>
                                            <label className="label-warning">not verified</label></div>
                                    }
                                </div>
                            </div>
                        </div>
                        {this.state.phoneSectionOpen ? <div className="collapse-container">
                            <br />
                            <p>Enter your phone number</p>
                            <div className="input-group">
                                <span className="input-group-addon" id="sizing-addon2">+62</span>
                                <input style={{ zIndex: 0 }} value={this.state.phone}
                                    onChange={this.handleChange('phone')} type="text"
                                    className="form-control" />
                            </div>
                            <p className="muted smaller-text">An SMS will be sent to verify ownership of this
                                number.</p>
                            <br />
                            <div className="row">
                                <div className="col-md-12">
                                    <button className="btn btn-primary float-right" onClick={() => this.verifyPhone()}>
                                        Next
                                    </button>
                                </div>
                            </div>
                        </div>
                            : null}
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="bg-muted verify-header"
                            onClick={() => this.openSection('idSectionOpen')}>
                            <div className="container">
                                <div className="row">
                                    <h5 className="col-md-11 col-8">Identity Verification</h5>
                                    {this.state.idSectionVerified ?
                                        <i className="color-success fa fa-check-circle col-md-1 verified-icon"></i> :
                                        <div style={{ marginLeft: '-10px' }}>
                                            <label className="label-warning">not verified</label></div>
                                    }
                                </div>
                            </div>
                        </div>
                        {this.state.idSectionOpen ? <div className="collapse-container">
                            <br />
                            <p>Full Name</p>
                            <input className="form-control col-md-12" value={this.state.fullName}
                                onChange={this.handleChange('fullName')} />
                            <p className="muted smaller-text">Full Name must match name provided in Passport/ID.</p>
                            <br />
                            <p>Nationality</p>
                            <Select
                                name="form-field-name"
                                value={this.state.nationality}
                                options={this.state.countries}
                                onChange={(val) => {
                                    this.setState({ nationality: val.value })
                                }}
                            />
                            <p className="muted smaller-text">Full Name must match name provided in Passport/ID.</p>
                            <br />
                            <p>Date of birth (DD-MM-YYYY)</p>
                            <DatePicker style={{ width: "100%" }} className="form-control col-md-12" format={"DD-MM-YYYY"} selected={this.state.dateOfBirthday} onChange={(val) => this.setState({ dateOfBirthday: val })} />
                            <br />
                            <p>City</p>
                            <input className="form-control col-md-12" value={this.state.city}
                                onChange={this.handleChange('city')} />
                            <br />
                            <p>Address</p>
                            <textarea className="form-control" rows={6} value={this.state.address}
                                onChange={this.handleChange('address')}></textarea>
                            <br />
                            <p>Postal/ Zip Code</p>
                            <input className="form-control col-md-12" value={this.state.zipCode}
                                onChange={this.handleChange('zipCode')} />
                            <br />
                            <h5>Upload Identiy Document</h5>
                            <br />
                            <p>Identity Number</p>
                            <input className="form-control col-md-12" value={this.state.identityNumber}
                                onChange={this.handleChange('identityNumber')} />
                            <br />
                            <p>Passport</p>
                            <S3Upload
                                value={this.state.fileIdentityPath}
                                className="form-control"
                                signingUrl={process.env.REACT_APP_SIGNING_URL}
                                onFinish={(file) => { this.setState({ fileIdentityPath: file }) }}
                            />
                            <br />
                            <p>Photo With Passport</p>
                            <S3Upload
                                value={this.state.filePhotoWithIdentityPath}
                                className="form-control col-md-12"
                                signingUrl={process.env.REACT_APP_SIGNING_URL}
                                onFinish={(file) => { this.setState({ filePhotoWithIdentityPath: file }) }}
                            />
                            <div className="col-md-8">
                                <ul className="form-info-list">
                                    <li>Hold the above submitted document and a piece of paper that says "TokoCrypto
                                        *date* " where
                                        *date* is a date not more than 3 days ago.
                                    </li>
                                    <li>Photo must clearly show your face and not covered by passport or ID</li>
                                    <li>Passport number / National ID number must be readable</li>
                                    <li>Image must be in high resolution</li>
                                    <li>Maximum file size is 5MB</li>
                                </ul>
                            </div>
                            <br />
                            <p>Proof of address document</p>
                            <S3Upload
                                value={this.state.fileProofAddressDocumentPath}
                                className="form-control col-md-12"
                                signingUrl={process.env.REACT_APP_SIGNING_URL}
                                onFinish={(file) => { this.setState({ fileProofAddressDocumentPath: file }) }}
                            />
                            <br />
                            <div className="row">
                                <div className="col-md-6">
                                    <p>Submit a full scanned image or photo of either: </p>
                                    <ul className="form-info-list">
                                        <li>Bank statement</li>
                                        <li>Utility Bill</li>
                                        <li>Tax return statement</li>
                                        <li>Government issued letters</li>
                                    </ul>
                                </div>
                                <div className="col-md-6">
                                    <p>Document guidelines: </p>
                                    <ul className="form-info-list">
                                        <li>Address and date must be visible in the document</li>
                                        <li>Document must be dated and be less than 3 months ago</li>
                                        <li>Document must be shown in full, images showing part of the document will not
                                            be accepted
                                        </li>
                                        <li>Screenshots will not be accepted.</li>
                                        <li>Image must be in high resolution.</li>
                                    </ul>
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-md-12">
                                    <button className="btn btn-primary float-right" onClick={() => this.verifyId()}>
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                            : null}
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <h5>Additional Verification (optional)</h5>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="bg-muted verify-header"
                            onClick={() => this.openSection('questionaireSectionOpen')}>
                            <div className="container">
                                <div className="row">
                                    <h5 className="col-md-11 col-8">Questionaire</h5>
                                    {this.state.questionaireSectionVerified ?
                                        <i className="color-success fa fa-check-circle col-md-1 verified-icon"></i> :
                                        <div style={{ marginLeft: '-10px' }}>
                                            <label className="label-warning">not verified</label></div>
                                    }
                                </div>
                            </div>
                        </div>
                        {this.state.questionaireSectionOpen ? <div className="collapse-container">
                            <br />
                            <p>What is the purpose of your trading on TokoCrypto? Please describe in as much detail as
                                possible how
                                you intend to use your trading account.</p>
                            <textarea className="form-control" value={this.state.a1}
                                onChange={this.handleChange('a1')}></textarea>
                            <br />
                            <p>Estimated amount that you would be depositing/withdrawing to/from your TokoCrypto account
                                per month
                                in BTC?</p>
                            <textarea className="form-control" value={this.state.a2}
                                onChange={this.handleChange('a2')}></textarea>
                            <br />
                            <p>What type of trading will you conduct? Buying/selling/both? Estimated trade volume per
                                month?</p>
                            <textarea className="form-control" value={this.state.a3}
                                onChange={this.handleChange('a3')}></textarea>
                            <br />
                            <p>What is the source of the funds which you have so far deposited to your TokoCrypto
                                account?</p>
                            <textarea className="form-control" value={this.state.a4}
                                onChange={this.handleChange('a4')}></textarea>
                            <br />
                            <div className="row">
                                <div className="col-md-12">
                                    <button className="btn btn-primary float-right"
                                        onClick={() => this.answerQuestions()}>Next
                                    </button>
                                </div>
                            </div>
                        </div>
                            : null}
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="bg-muted verify-header"
                            onClick={() => this.openSection('incomeSectionOpen')}>
                            <div className="container">
                                <div className="row">
                                    <h5 className="col-md-11 col-8">Income Verification</h5>
                                    {this.state.incomeSectionVerified ?
                                        <i className="color-success fa fa-check-circle col-md-1 verified-icon"></i> :
                                        <div style={{ marginLeft: '-10px' }}>
                                            <label className="label-warning">not verified</label></div>
                                    }
                                </div>
                            </div>
                        </div>
                        {this.state.incomeSectionOpen ? <div className="collapse-container">
                            <br />
                            <p>Where is your primary source of income from?.</p>
                            <textarea className="form-control" value={this.state.a5}
                                onChange={this.handleChange('a5')}></textarea>
                            <br />
                            <div className="row">
                                <div className="col-md-12">
                                    <button className="btn btn-primary float-right" onClick={() => this.answerIncome()}>
                                        Next
                                    </button>
                                </div>
                            </div>
                        </div>
                            : null}
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        {this.state.loading && <Loader type="line-scale" active={true} className="float-right" />}
                        {this.state.loading == false && <button onClick={() => this.confirmMyAccount()} className="float-right btn btn-primary">confirm my account</button>}
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc
    };
};

const mapDispatchToProps = dispatch => ({
    updateKYC: actions.updateKYC,
    fetchKYC: actions.fetchKYC,
    actions: bindActionCreators(Object.assign({}, { setTradingStatus: homeActions.setTradingStatus }), dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(VerifyAccount)
