import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Loader } from 'react-loaders';
import Coins from '../../components/coins.js';
import BuySellCoins from '../../components/buySellCoins.js';
import actions from '../../core/actions/client/home';
import _ from 'lodash';
import NetworkError from '../../errors/networkError';
import LiveRateChart from '../../components/liveRateLineChart';
import { NotificationManager } from 'react-notifications';
import * as errorMessages from '../../constants/errorMessages'
import VerifyAccount from './verifyaccount'
import Skeleton from 'react-loading-skeleton';
import history from 'history';
import ConfirmAccountBanner from '../../components/confirmAccountBanner';

class Home extends Component {

    constructor() {
        super();
        this.state = {
            liveCurrencyPair: "BTCIDR",
            liveType: "hours",
            liveRates: [],
            accounts: [],
            rates: [],
            verifiying: false,
            accountState: '',
            options:{
                maintainAspectRatio: true,
                fill: false
            },
            notifications: []
        };
    }

    componentDidMount() {
        this.fetchDashboard();
        this.getNotifications();
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    fetchDashboard() {
        let username = this.props.user.username;
        let clientId = this.props.user.pool.clientId;
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];
        this.props.fetchDashboard(token).then(res => {
            let accOrdered = _.orderBy(res.data.accounts, (acc) => {
                return acc.Currency.Type;
            });

            this.setState({
                accounts: accOrdered,
                rates: res.data.rates,
                accountState: res.data.kyc
            });
            this.props.actions.setKYCStatus(res.data.kyc);
        }).catch(err => {
            NetworkError(err);
        });
    }

    onSubmitKYC() {
        this.setState({
            verifiying: false
        });
        this.fetchDashboard();
    }

    renderAccount() {

        return this.state.accounts.map((c, id) => {
            var newaccount = this.state.accounts;

            return (
                <div key={id}>
                    <div className="col-md-12 custom-name">
                        <div className="row">
                            <Coins coin={c} rates={this.state.rates} />
                        </div>
                    </div>
                </div>
            )
        });
    }

    getNotifications(){
        actions.getNotifications(this.getToken()).then(res => {
            this.setState({
                notifications: res.data.data,
                showNotifications: true
            });
        }).catch(err => {
            NetworkError(err);
        });
    }

    hideNotifications(){
        this.setState({
            showNotifications: false
        });
    }

    render() {
        return (

            <div className="col-md-12 main-container">
                <div className="row">
                <div className="col-md-12">

                    {/* <div className="col-md-6 main-account-balance-container">
                        <div className="">
                            <div className="main-account-balance">Account Balance</div>

                            <div className="main-account-balance-amount">
                                Indonesian Rupiah 400,000,000
                            </div>
                        </div>
                        <div className="main-account-buttons">
                            <button>Deposit</button>
                            <button>Withdraw</button>
                        </div>
                    </div> */}

                    {/* <div className="col-md-6 main-account-balance-container">
                       <div>
                            <img className="" src={require('../../img/check-circular-button.png')} />
                            <div>Verify your account to start trading</div>
                       </div>
                    </div> */}

                </div>

                <div className="middle-container">

                    <div className="col-md-4 accountBalance-container">
                        <div className="accountBalance-header-idr"></div>
                        <div className="accountBalance-header">Account Balances</div>
                        {/* <div className="main-account-balance">Your Wallets</div> */}
                        {/* <div className="inside-data"></div> */}

                        {/* <div className="app-headings">Account Balance</div> */}
                        {this.renderAccount()}

                    </div>

                    <div className="col-md-8 information-container">
                        <br /><br /><br />
                        {this.props.kyc.status != 'approved' && this.props.kyc.status != '' && <ConfirmAccountBanner />}
                        <div className="row">
                            <div className="container buy-sell-container">

                                {this.state.rates.map((r, id) => {
                                    return (
                                        <div key={id}>
                                            <div className="col-md-12">
                                                <BuySellCoins
                                                    rate={r}
                                                    sellAccount={_.find(this.state.accounts, { "Currency": { "Code": r.currencyPairHummanize.split('/')[0] } })}
                                                    buyAccount={_.find(this.state.accounts, { "Currency": { "Code": r.currencyPairHummanize.split('/')[1] } })}
                                                />
                                            </div>

                                        </div>
                                    )
                                })}
                            </div>
                        </div>

                        <div className="row">
                            <LiveRateChart />
                        </div>
                    </div>
                </div>

                {/* {this.state.verifiying == false && (this.props.kyc.status != '' && this.props.kyc.status != 'approved') &&
                    <div className="row">
                        <div className="container">
                            <div className="col-md-12 verify-box">
                                {this.props.kyc.status == 'rejected' && <h4>Account Rejected please contact admin.</h4>}
                                {this.props.kyc.status == 'draft' && <h4>Verify your account to start trading</h4>}
                                {this.props.kyc.status == 'pending' && <h4>Your account is being verified</h4>}
                                <br />
                                <div className="row">
                                    <div className="col-md-9">
                                        <p><i className="fa fa-arrow-circle-right color-primary"></i> Phone Number </p>
                                        <p><i className="fa fa-arrow-circle-right color-primary"></i> Identity Verification </p>
                                    </div>
                                    <div className="col-md-1">
                                        <button className="btn btn-primary" onClick={() => history.push('verifyaccount')}>
                                            {this.props.kyc.status == 'draft' || this.props.kyc.status == 'rejected' && <span>Complete Verification </span>}
                                            {this.props.kyc.status == 'pending' && <span>Update verification data </span>}
                                            <i className="fa fa-arrow-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    } */}
                    <br />
                    {/* {this.state.accounts.length > 0 && this.state.verifiying == false ?
                        <div>

                            <br />
                            <div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <h3 className="primary-coin"><span className="fa fa-exchange"></span> Buy / Sell</h3>
                                    </div>
                                </div>
                                <br />
                                <div className="row">
                                    <div className="container">
                                        <hr />
                                        {this.state.rates.map((r, id) => {
                                            return (
                                                <div key={id}>
                                                    <div className="col-md-12">
                                                        <BuySellCoins
                                                            rate={r}
                                                            sellAccount={_.find(this.state.accounts, { "Currency": { "Code": r.currencyPairHummanize.split('/')[0] } })}
                                                            buyAccount={_.find(this.state.accounts, { "Currency": { "Code": r.currencyPairHummanize.split('/')[1] } })}
                                                        />
                                                    </div>
                                                    <hr />
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> : null} */}
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc
    };
};

const mapDispatchToProps = dispatch => ({
    fetchDashboard: actions.fetchDashboard,
    actions: bindActionCreators(Object.assign({}, { setKYCStatus: actions.setKYCStatus }), dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Home)
