import React, { Component } from 'react';
import classnames from 'classnames';
import DatePicker from 'react-datepicker';
import ProvincesID from '../../languages/provincesID.json';
import ProvincesEN from '../../languages/provincesEN.json';
import JobID from '../../languages/jobID.json';
import JobEN from '../../languages/jobEN.json';
import { getUserAttributes, updateAttributes } from '../../core/react-cognito/attributes.js'
import S3Upload from '../../components/S3Upload'
import { connect } from 'react-redux';
import actions from '../../core/actions/client/home';
import NetworkError from '../../errors/networkError';
import { Loader } from '../../components/tcLoader';
import history from '../../history';
import Select from 'react-select'
import moment from 'moment';
import _ from 'lodash';
import userActions from '../../core/actions/client/user.js'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import 'react-phone-number-input/style.css'
import Phone, { formatPhoneNumber, formatPhoneNumberIntl, parsePhoneNumber, isValidPhoneNumber } from 'react-phone-number-input';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import async from 'async';
import { NotificationManager } from 'react-notifications';
import { pixelID } from '../../config/pixel';
import localStorage from 'localStorage';

let ReactPixel = null;

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

const nationalities = ["ABKHAZIAN", "AFGHANI", "ALANDIC", "ALBANIAN", "ALGERIAN", "U.S. TERRITORY", "ANDORRAN", "ANGOLAN", "BRITISH OVERSEAS TERRITORY", "ANTARCTIC", "ANTIGUAN", "ARGENTINE", "ARMENIAN", "ARUBIAN", "ASCENSION", "AUSTRALIAN EXTERNAL TERRITORY", "AUSTRALIAN", "AUSTRALIAN ANTARCTIC TERRITORY", "AUSTRIAN", "AZERBAIJANI", "BAHAMEESE", "BAHRAINIAN", "BAKER ISLAND", "BANGLADESHI", "BARBADIAN", "BELARUSIAN", "BELGIAN", "BELIZEAN", "BENINESE", "BRITISH OVERSEAS TERRITORY", "BHUTANESE", "BOLIVIAN", "BOSNIAN", "MOTSWANA", "BOUVET ISLAND", "BRAZILIAN", "BRITISH ANTARCTIC TERRITORY", "BRITISH INDIAN OCEAN TERRITORY", "BRITISH SOVEREIGN BASE AREAS", "BRITISH OVERSEAS TERRITORY", "BRUNEIAN", "BULGARIAN", "BURKINABE", "MYANMAR", "BURUNDIAN", "CAMBODIAN", "CAMEROONIAN", "CANADIAN", "CAPE VERDEAN", "CARIBBEAN NETHERLANDS", "BRITISH OVERSEAS TERRITORY", "CENTRAL AFRICAN", "CHADIAN", "CHILEAN", "CHINESE", "AUSTRALIAN EXTERNAL TERRITORY", "CLIPPERTON ISLAND", "AUSTRALIAN EXTERNAL TERRITORY", "COLOMBIAN", "COMORAN", "CONGOLESE", "COOK ISLANDER", "AUSTRALIAN EXTERNAL TERRITORY", "COSTA RICAN", "IVORIAN", "CROATIAN", "CUBAN", "CURACAOAN", "CYPRIOT", "CZECH", "CONGOLESE", "DANISH", "DJIBOUTIAN", "DOMINICAN", "DOMINICAN", "ECUADOREAN", "EGYPTIAN", "SALVADOREAN", "EQUATORIAL GUINEAN", "ERITREAN", "ESTONIAN", "ETHIOPIAN", "BRITISH OVERSEAS TERRITORY", "FAROESE", "FIJIAN", "FINNISH", "FRENCH", "FRENCH GUIANESE", "FRENCH OVERSEAS COLLECTIVITY", "FRENCH SOUTHERN AND ANTARCTIC LANDS", "FRENCH SOUTHERN TERRITORIES", "GABONESE", "GAMBIAN", "GEORGIAN", "GERMAN", "GHANAIAN", "GIBRALTARIAN", "GREEK", "GREENLANDER", "GRENADIAN", "GUADELOUPEAN", "GUAMANIAN", "GUATEMALAN", "GUERNSEY", "GUINEAN", "GUINEAN", "GUYANESE", "HAITIAN", "AUSTRALIAN EXTERNAL TERRITORY", "HONDURAN", "HONG KONG", "HOWLAND ISLAND", "HUNGARIAN", "ICELANDER", "INDIAN", "INDONESIAN", "IRANIAN", "IRAQI", "IRISH", "MANX", "ISRAELI", "ITALIAN", "JAMAICAN", "JAPANESE", "JARVIS ISLAND", "JERSEY", "JOHNSTON ATOLL", "JORDANIAN", "KAZAKHSTANI", "KENYAN", "KINGMAN REEF", "I-KIRIBATI", "KOSOVAR", "KUWAITI", "KYRGYZSTANI", "LAOTIAN", "LATVIAN", "LEBANESE", "MOSOTHO", "LIBERIAN", "LIBYAN", "LIECHTENSTEINER", "LITHUNIAN", "LUXEMBOURGER", "MACANESE", "MACEDONIAN", "MALAGASY", "MALAWIAN", "MALAYSIAN", "MALDIVAN", "MALIAN", "MALTESE", "MARSHALLESE", "MARTINICAN", "MAURITANIAN", "MAURITIAN", "MAHORAN", "MEXICAN", "MICRONESIAN", "U.S. TERRITORY", "MOLDOVAN", "MONACAN", "MONGOLIAN", "MONTENEGRIN", "MONTSERRATIAN", "MOROCCAN", "MOZAMBICAN", "NAGORNO-KARABAKH", "NAMIBIAN", "NAURUAN", "NAVASSA ISLAND", "NEPALESE", "DUTCH", "NEW CALEDONIAN", "NEW ZEALANDER", "NICARAGUAN", "NIGERIEN", "NIGERIAN", "NIUEAN", "NORFOLK ISLANDER", "NORTH KOREAN", "U.S. TERRITORY", "U.S. TERRITORY", "NORWEGIAN", "OMANI", "PAKISTANI", "PALAUAN", "PALESTINIAN", "PALMYRA ATOLL", "PANAMANIAN", "PAPUA NEW GUINEAN", "PARAGUAYAN", "PERUVIAN", "PETER I ISLAND", "FILIPINO", "PITCAIRN ISLANDER", "BRITISH OVERSEAS TERRITORY", "POLISH", "PORTUGUESE", "PRIDNESTROVIE (TRANSNISTRIA)", "PUERTO RICAN", "QATARI", "QUEEN MAUD LAND", "FRENCH OVERSEAS REGION", "ROMANIAN", "ROSS DEPENDENCY", "RUSSIAN", "RWANDAN", "FRENCH OVERSEAS COLLECTIVITY", "BRITISH OVERSEAS TERRITORY", "KITTIAN", "SAINT LUCIAN", "FRENCH OVERSEAS COLLECTIVITY", "DUTCH", "SAINT-PIERRAIS", "SAINT VINCENTIAN", "SAMOAN", "SANMARINESE", "SAO TOMEAN", "SAUDI ARABIAN", "SENEGALESE", "SERBIAN", "SEYCHELLOIS", "SIERRA LEONEAN", "SINGAPOREAN", "SLOVAKIAN", "SLOVENIAN", "SOLOMON ISLANDER", "SOMALI", "SOMALILAND", "SOUTH AFRICAN", "BRITISH OVERSEAS TERRITORY", "BRITISH OVERSEAS TERRITORY", "SOUTH KOREAN", "SOUTH OSSETIA", "SOUTH SUDANESE", "SPANISH", "SRI LANKAN", "SUDANESE", "SURINAMER", "SVALBARD AND JAN MAYEN ISLANDS", "SWAZI", "SWEDISH", "SWISS", "SYRIAN", "TAIWANESE", "TAJIKISTANI", "TANZANIAN", "THAI", "TIMORESE", "TOGOLESE", "TOKELAUAN", "TONGAN", "TRINIDADIAN", "TRISTAN DA CUNHA", "TUNISIAN", "TURKISH", "TURKMEN", "BRITISH OVERSEAS TERRITORY", "TUVALUAN", "UGANDAN", "UKRAINIAN", "EMIRIAN", "BRITISH", "U.S. TERRITORY", "AMERICAN", "U.S. TERRITORY", "URUGUAYAN", "UZBEKISTANI", "NI-VANUATU", "VATICAN", "VENEZUELAN", "VIETNAMESE", "WAKE ISLAND", "FRENCH OVERSEAS COLLECTIVITY", "WESTERN SAHARAN", "YEMENI", "ZAMBIAN", "ZIMBABWEAN", "UNKNOWN"];

const countries = ["ABKHAZIA", "AFGHANISTAN", "ALAND ISLANDS", "ALBANIA", "ALGERIA", "AMERICAN SAMOA", "ANDORRA", "ANGOLA", "ANGUILLA", "ANTARCTICA", "ANTIGUA AND BARBUDA", "ARGENTINA", "ARMENIA", "ARUBA", "ASCENSION", "ASHMORE AND CARTIER ISLANDS", "AUSTRALIA", "AUSTRALIAN ANTARCTIC TERRITORY", "AUSTRIA", "AZERBAIJAN", "BAHAMAS", "BAHRAIN", "BAKER ISLAND", "BANGLADESH", "BARBADOS", "BELARUS", "BELGIUM", "BELIZE", "BENIN", "BERMUDA", "BHUTAN", "BOLIVIA", "BOSNIA AND HERZEGOVINA", "BOTSWANA", "BOUVET ISLAND", "BRAZIL", "BRITISH ANTARCTIC TERRITORY", "BRITISH INDIAN OCEAN TERRITORY", "BRITISH SOVEREIGN BASE AREAS", "BRITISH VIRGIN ISLANDS", "BRUNEI", "BULGARIA", "BURKINA FASO", "BURMA (REPUBLIC OF THE UNION OF MYANMAR)", "BURUNDI", "CAMBODIA", "CAMEROON", "CANADA", "CAPE VERDE", "CARIBBEAN NETHERLANDS", "CAYMAN ISLANDS", "CENTRAL AFRICAN REPUBLIC", "CHAD", "CHILE", "CHINA", "CHRISTMAS ISLAND", "CLIPPERTON ISLAND", "COCOS (KEELING) ISLANDS", "COLOMBIA", "COMOROS", "CONGO (REPUBLIC OF)", "COOK ISLANDS", "CORAL SEA ISLANDS", "COSTA RICA", "COTE D'IVOIRE (IVORY COAST)", "CROATIA", "CUBA", "CURACAO", "CYPRUS", "CZECH REPUBLIC", "DEMOCRATIC REPUBLIC OF THE CONGO", "DENMARK", "DJIBOUTI", "DOMINICA", "DOMINICAN REPUBLIC", "ECUADOR", "EGYPT", "EL SALVADOR", "EQUATORIAL GUINEA", "ERITREA", "ESTONIA", "ETHIOPIA", "FALKLAND ISLANDS", "FAROE ISLANDS", "FIJI", "FINLAND", "FRANCE", "FRENCH GUIANA", "FRENCH POLYNESIA", "FRENCH SOUTHERN AND ANTARCTIC LANDS", "FRENCH SOUTHERN TERRITORIES", "GABON", "GAMBIA", "GEORGIA", "GERMANY", "GHANA", "GIBRALTAR", "GREECE", "GREENLAND", "GRENADA", "GUADELOUPE", "GUAM", "GUATEMALA", "GUERNSEY", "GUINEA", "GUINEA-BISSAU", "GUYANA", "HAITI", "HEARD AND MCDONALD ISLANDS", "HONDURAS", "HONG KONG", "HOWLAND ISLAND", "HUNGARY", "ICELAND", "INDIA", "INDONESIA", "IRAN", "IRAQ", "IRELAND", "ISLE OF MAN", "ISRAEL", "ITALY", "JAMAICA", "JAPAN", "JARVIS ISLAND", "JERSEY", "JOHNSTON ATOLL", "JORDAN", "KAZAKHSTAN", "KENYA", "KINGMAN REEF", "KIRIBATI", "KOSOVO", "KUWAIT", "KYRGYZSTAN", "LAOS", "LATVIA", "LEBANON", "LESOTHO", "LIBERIA", "LIBYA", "LIECHTENSTEIN", "LITHUANIA", "LUXEMBOURG", "MACAU", "MACEDONIA", "MADAGASCAR", "MALAWI", "MALAYSIA", "MALDIVES", "MALI", "MALTA", "MARSHALL ISLANDS", "MARTINIQUE", "MAURITANIA", "MAURITIUS", "MAYOTTE", "MEXICO", "MICRONESIA", "MIDWAY ISLANDS", "MOLDOVA", "MONACO", "MONGOLIA", "MONTENEGRO", "MONTSERRAT", "MOROCCO", "MOZAMBIQUE", "NAGORNO-KARABAKH", "NAMIBIA", "NAURU", "NAVASSA ISLAND", "NEPAL", "NETHERLANDS", "NEW CALEDONIA", "NEW ZEALAND", "NICARAGUA", "NIGER", "NIGERIA", "NIUE", "NORFOLK ISLAND", "NORTH KOREA", "NORTHERN CYPRUS", "NORTHERN MARIANA ISLANDS", "NORWAY", "OMAN", "PAKISTAN", "PALAU", "PALESTINE", "PALMYRA ATOLL", "PANAMA", "PAPUA NEW GUINEA", "PARAGUAY", "PERU", "PETER I ISLAND", "PHILIPPINES", "PITCAIRN", "PITCAIRN ISLANDS", "POLAND", "PORTUGAL", "PRIDNESTROVIE (TRANSNISTRIA)", "PUERTO RICO", "QATAR", "QUEEN MAUD LAND", "REUNION", "ROMANIA", "ROSS DEPENDENCY", "RUSSIAN FEDERATION", "RWANDA", "SAINT BARTHELEMY", "SAINT HELENA", "SAINT KITTS AND NEVIS", "SAINT LUCIA", "SAINT MARTIN (FRANCE)", "SAINT MARTIN (NETHERLANDS)", "SAINT PIERRE AND MIQUELON", "SAINT VINCENT AND GRENADINES", "SAMOA", "SAN MARINO", "SAO TOME AND PRINCIPE", "SAUDI ARABIA", "SENEGAL", "SERBIA", "SEYCHELLES", "SIERRA LEONE", "SINGAPORE", "SLOVAKIA", "SLOVENIA", "SOLOMON ISLANDS", "SOMALIA", "SOMALILAND", "SOUTH AFRICA", "SOUTH GEORGIA & SOUTH SANDWICH ISLANDS", "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS", "SOUTH KOREA", "SOUTH OSSETIA", "SOUTH SUDAN", "SPAIN", "SRI LANKA", "SUDAN", "SURINAME", "SVALBARD AND JAN MAYEN ISLANDS", "SWAZILAND", "SWEDEN", "SWITZERLAND", "SYRIA", "TAIWAN", "TAJIKISTAN", "TANZANIA", "THAILAND", "TIMOR-LESTE", "TOGO", "TOKELAU", "TONGA", "TRINIDAD AND TOBAGO", "TRISTAN DA CUNHA", "TUNISIA", "TURKEY", "TURKMENISTAN", "TURKS AND CAICOS ISLANDS", "TUVALU", "UGANDA", "UKRAINE", "UNITED ARAB EMIRATES", "UNITED KINGDOM", "UNITED STATES MINOR OUTLYING ISLANDS", "UNITED STATES OF AMERICA", "UNITED STATES VIRGIN ISLANDS", "URUGUAY", "UZBEKISTAN", "VANUATU", "VATICAN", "VENEZUELA", "VIETNAM", "WAKE ISLAND", "WALLIS AND FUTUNA ISLANDS", "WESTERN SAHARAN", "YEMEN", "ZAMBIA", "ZIMBABWE", "UNKNOWN"];

class KYC extends Component {

    constructor() {
        super();
        this.state = {
            step: 1,
            country: 'INDONESIA',
            countries: [],
            provincesIDs: [],
            provincesID: '',
            provincesEN: '',
            jobEN: '',
            jobID: '',
            jobIDs: [],
            jobENs: [],
            nationalities: [],
            genders: [{ value: "MALE", label: "Male" }, { value: "FEMALE", label: "Female" }],
            loading: false,
            dateOfBirthday: '',
            fullName: '',
            gender: 'MALE',
            nationality: 'INDONESIAN',
            city: '',
            phone: '',
            address: '',
            fileIdentityPath: '',
            filePhotoWithIdentityPath: '',
            fileProofAddressDocumentPath: '',
            identityNumber: '',
            idTypes: [],
            idType: 'KTP',
            zipCode: '',
            underAge: true,
            formTouched: false,
            showForm: false,
            showReference1: false,
            showReference2: false,
            showReference3: false,
            prevName: '',
            isSocial: false,
            placeOfBirth:'',
            kycErrorId: false
        }

        this.handleChange = (fieldName) => (event) => {

            let obj = Object.assign({})

            if (fieldName === 'fullName' || fieldName === 'city') {
                const re = /^[a-zA-Z_ ]{1,256}$/;
                if (event.target.value == '' || re.test(event.target.value)) {
                    obj[fieldName] = event.target.value
                }
            } else if (fieldName === 'identityNumber') {
                const re = /^[a-zA-Z0-9_ ]{1,30}$/;
                if (event.target.value == '' || re.test(event.target.value)) {
                    obj[fieldName] = event.target.value
                }
            } else {
                obj[fieldName] = event.target.value
            }

            this.setState(obj)
        }
    }

    checkIfGoogleSignupOnly() {
        let isSocial = localStorage.getItem('CACHE_METHOD') === 'social';

        if(isSocial) {
            this.setState({
                fullName: localStorage.getItem('CACHE_NAME'),
                isSocial: true
            });
        }
    }

    close() {
        this.setState({
            showReference1: false,
            showReference2: false,
            showReference3: false
        })
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    onSetLanguage() {
        strings.setLanguage(this.props.language);
    }

    fetchUserProfile() {
        
        getUserAttributes(this.props.user).then(attr => {
            
            this.setState({
                fullName: attr.name,
                prevName: attr.name
            })
        })
            .catch(err => {
                
                NetworkError(err);
            })
    }

    onChangeDob(val) {
        this.setState({ dateOfBirthday: val });
        this.checkUserAge(val);
    }

    onKeyDownPhone(e) {

        if (!_.isEmpty(e)) {
            

            if (e.length > 50 || e.length === 0 || (isNaN(parseInt(e[e.length - 1])) && e.length !== 1)) {
                return false;
            } else {
                this.setState({
                    phone: e
                })
            }
        }

    }

    checkUserAge(val) {
        let date = moment(val);
        let now = moment();
        let diff = Math.abs(date.diff(now, 'year'));

        if (diff < 17) {
            this.setState({
                underAge: true
            });
        } else {
            this.setState({
                underAge: false
            });
        }
    }

    nextStep() {        
        let province = false;
        if (this.state.country !== "INDONESIA"){
            province = true;
        }else {
            if(this.state.provincesID.length > 0){
                province = true;
            }else{
                province = false;
            }
        }
        
        let step1Valid =
            this.state.fullName.length > 0 &&
            this.state.nationality.length > 0 &&
            this.state.dateOfBirthday !== "" &&
            this.state.phone.length >= 8 &&
            this.state.jobID.length > 0 &&
            this.state.placeOfBirth.length > 0 &&
            this.state.gender.length > 0 &&
            !this.state.underAge;

        let step2Valid =
            province &&
            this.state.city.length > 0 &&
            this.state.address.length > 0 &&
            this.state.zipCode.length >= 5;

        let step3Valid =
            this.state.idType.length > 0 &&
            this.state.identityNumber.length > 0 &&
            this.state.fileIdentityPath.length > 0;

        let step4Valid =
            this.state.filePhotoWithIdentityPath.length > 0;

        let stepValid = false;

        switch (this.state.step) {
            case 1:
                stepValid = step1Valid;
                break;
            case 2:
                stepValid = step2Valid;
                break;
            case 3:
                stepValid = step3Valid;
                break;
            case 4:
                stepValid = step4Valid;
                break;
            default:
                step1Valid = false;
        }

        if (stepValid) {
            if (this.state.step === 4) {
                
                this.setState({
                    step: this.state.step += 1,
                    formTouched: false
                });
                this.confirmMyAccount();
            } else {
                
                this.setState({
                    step: this.state.step += 1,
                    formTouched: false
                });
            }
        } else {
            this.setState({
                formTouched: true
            })
        }
    }

    prevStep() {
        this.setState({
            step: this.state.step -= 1
        });
    }

    componentDidMount() {
        this.initFBPixel();
        this.checkIfGoogleSignupOnly();

        let provincesDataID = [];
        ProvincesID.map(c => {
            provincesDataID.push({
                label: _.capitalize(c),
                value: c
            });
        });

        let provincesDataEN = [];
        ProvincesEN.map(c => {
            provincesDataEN.push({
                label: _.capitalize(c.label),
                value: c.value
            });
        });

        let jobDataID = [];
        JobID.map(c => {
            jobDataID.push({
                label: c,
                value: c
            });
        });

        let jobDataEN = [];
        JobEN.map(c => {
            jobDataEN.push({
                label: c.label,
                value: c.value
            });
        });
        

        let countriesData = [];
        countries.map(c => {
            countriesData.push({
                label: _.capitalize(c),
                value: c
            });
        });

        let nationalitiesData = [];
        nationalities.map(c => {
            nationalitiesData.push({
                label: _.capitalize(c),
                value: c
            });
        });

        let idTypes = [];
        idTypes = [
            {
                "value": "KTP",
                "label": strings.KTP
            },
        ];

        this.fetchUserProfile()
        this.fetchUserPermission()
        this.setState({ 
            provincesIDs: provincesDataID,
            provincesENs: provincesDataEN,
            jobIDs: jobDataID,
            jobENs: jobDataEN,
            countries: countriesData,
            nationalities: nationalitiesData, 
            idTypes: idTypes 
        });
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    initFBPixel() {
        ReactPixel = require('react-facebook-pixel').default;
        ReactPixel.init(pixelID);
        ReactPixel.pageView();
        ReactPixel.track('ViewContent', { content_name: 'kyc' });
        ReactPixel.track('Lead', { content_name: 'kyc start' });
    }

    getToken() {
        let username = this.props.user.username;
        let clientId = this.props.user.pool.clientId;
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];
        return token;
    }

    /**
     * TCDOC-016
     * fetch current KYC. this will provide kyc page current kyc fills. so user can resume it.
     */
    fetchKYC() {
        this.props.fetchKYC(this.getToken()).then(res => {
            this.setState({
                phone: res.data.phoneNumber,
                fullName: res.data.FullName,
                nationality: res.data.Nationality,
                dateOfBirthday: moment(res.data.DateOfBirthday, "DD-MM-YYYY"),
                job: res.data.jobID,
                province: res.data.provincesID,
                city: res.data.City,
                address: res.data.Address,
                zipCode: res.data.ZipCode,
                identityNumber: res.data.IdentityNumber,
                fileIdentityPath: res.data.FileIdentityPath,
                filePhotoWithIdentityPath: res.data.FilePhotoWithIdentityPath,
                // fileProofAddressDocumentPath: res.data.FileProofAddressDocumentPath,
                a1: res.data.AdditionalVerification[0].Answer,
                a2: res.data.AdditionalVerification[1].Answer,
                a3: res.data.AdditionalVerification[2].Answer,
                a4: res.data.AdditionalVerification[3].Answer,
                a5: res.data.AdditionalVerification[4].Answer
            })
        })
            .catch(err => {
                NetworkError(err);
            })
    }

    /**
     * TCDOC-017
     * send the KYC, just straight forward network request
     */

    confirmMyAccount() {

        // let num = this.state.phone;
        // var maskedPhone = "+"+num;
        this.setState({ loading: true });

        let idType = this.state.idType;
        if (idType.toLocaleLowerCase() === 'ktp') {
            idType = "NRIC";
        }
        let provinceID = this.state.provincesID;
        if (this.state.country !== "INDONESIA"){
            provinceID = '';
        }else {
            provinceID = this.state.provincesID;
        }

        let data = {
            country: this.state.country,
            phoneNumber: this.state.phone,
            fullName: this.state.fullName,
            nationality: this.state.nationality,
            place_of_birth: this.state.placeOfBirth,
            dateOfBirthday: this.state.dateOfBirthday.format("DD/MM/YYYY"),
            job: this.state.jobID,
            province: provinceID ,
            city: this.state.city,
            address: this.state.address,
            zipCode: this.state.zipCode,
            identityNumber: this.state.identityNumber,
            fileIdentityPath: this.state.fileIdentityPath,
            filePhotoWithIdentityPath: this.state.filePhotoWithIdentityPath,
            // fileProofAddressDocumentPath: this.state.fileProofAddressDocumentPath,
            gender: this.state.gender,
            identificationType: idType,
            additionalVerification: []
        }

        if (this.state.fullName === this.state.prevName) {

            this.props.updateKYC(this.getToken(), data).then(res => {
                this.setState({ loading: false, kycErrorId: false });
                ReactPixel.track('Lead', { content_name: 'kyc success', name: this.state.fullName });
            })
                .catch(err => {
                    NetworkError(err);
                    let errorCode = err.response.data.err
                    if(errorCode === 'dublicate identity number'){
                        this.setState({ 
                            kycErrorId: true 
                        });
                    }
                    this.setState({ 
                        loading: false
                    });
                });
        } else {

            let attributes = {
                name: this.state.fullName
            }

            async.parallel({
                updateName: (callback) => {
                    userActions.updateName(this.getToken(), attributes).then(res => {
                        callback(null, res);
                    })
                        .catch(err => {
                            NetworkError(err);
                            // 
                            callback(err, null);
                        });
                },
                updateCognitoName: (callback) => {
                    if(!this.state.isSocial) {
                        getUserAttributes(this.props.user).then((attr) => {
                            // 
                            updateAttributes(this.props.user,attributes).then(attr => {
                                callback(null, attr);
                            }).catch(err => {
                                NetworkError(err);
                                
                                let error = {
                                    response: {data:{message_code:err}}
                                }
    
                                callback(error, null);
                            });
                        }).catch(e => {
                            NetworkError(e);
                            
                            callback(e, null);
                        })
                    } else {
                        callback(null, {});
                    }
                },
                updateKYC: (callback) => {
                    this.props.updateKYC(this.getToken(), data).then(res => {
                        this.setState({ 
                            kycErrorId: false 
                        });
                        callback(null, res);
                    })
                        .catch(err => {
                            let errorCode = err.response.data.err
                            if(errorCode === 'dublicate identity number'){
                                this.setState({ 
                                    kycErrorId: true 
                                });
                            }
                            NetworkError(err);
                            // 
                            callback(err, null);
                        });
                }
            }, (err) => {
                
                if (err) {
                    
                    // NotificationManager.error(strings.KYC_SUBMITTING_ERROR);
                    NetworkError(err);
                    this.setState({ step: 1 });
                    this.setState({ loading: false });
                } else {
                    this.setState({ loading: false });
                }

            });
        }

    }

    // componentWillReceiveProps(props) {
    //     
    //     if(props.kyc) {

    //         let step = 1;
    //         if(props.kyc.status === 'pending'){
    //             step = 2;
    //         }

    //         this.setState({
    //             showForm: true,
    //             step
    //         })
    //     }
    // }

    fetchUserPermission() {
        let token = this.getToken();
        userActions.fetchUser(token)
            .then(res => {
                let data = res.data.data;
                if (data.kyc) {
                    let step = 1;
                    if (data.kyc === 'pending') {
                        step = 4;
                    }

                    this.setState({
                        showForm: true,
                        step
                    })
                }
            })
            .catch(err => {
                NetworkError(err);
                this.setState({ fetchingKYC: false });
            })
    }

    render() {
        let province = false;
        if (this.state.country !== "INDONESIA"){
            province = true;
        }else {
            if(this.state.provincesID.length > 0){
                province = true;
            }else{
                province = false;
            }
        }
        const { phone } = this.state

        let inValidPhoneNumber = strings.INVALID_PHONE_NUMBER;

        let step1Valid =
            this.state.fullName.length > 0 &&
            this.state.nationality.length > 0 &&
            this.state.dateOfBirthday !== "" &&
            this.state.placeOfBirth.length > 0 &&
            this.state.jobID.length > 0 &&
            this.state.phone.length > 0 &&
            this.state.gender.length > 0 &&
            !this.state.underAge;

        let step2Valid =
            province &&
            this.state.city.length > 0 &&
            this.state.address.length > 0 &&
            this.state.zipCode + "".length >= 5;

        let step3Valid =
            this.state.idType.length > 0 &&
            this.state.identityNumber.length > 0 &&
            this.state.fileIdentityPath.length > 0;

        let step4Valid =
            this.state.filePhotoWithIdentityPath.length > 0;

        let isNextValid = false;

        if (this.state.step === 1) {
            isNextValid = step1Valid;
        } else if (this.state.step === 2) {
            isNextValid = step2Valid;
        } else if (this.state.step === 3) {
            isNextValid = step3Valid;
        } else if (this.state.step === 4) {
            isNextValid = step4Valid;
        } else {
            isNextValid = false;
        }

        return (
            <div className="m-grid m-grid--hor m-grid--root m-page">
                <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body" style={{ backgroundColor: "#f4f7fa" }}>
                    <div className="m-grid__item m-grid__item--fluid  m-grid m-grid--ver m-container m-container--responsive m-container--xxl m-page__container" style={{ paddingBottom: window.outerHeight * 0.1 }}>
                        <div className="m-grid__item m-grid__item--fluid m-wrapper">
                            {!this.state.showForm && <div style={{ marginLeft: '50%' }}><Loader /></div>}
                            {this.state.showForm && <div className="m-content">
                                <div className="row">
                                    <div className="col-xl-12">
                                        <div className="m-portlet m-portlet--full-height">

                                            <div className="m-portlet__head">

                                                <div className="m-portlet__head-caption">
                                                    <div className="m-portlet__head-title">
                                                        <h3 className="m-portlet__head-text">
                                                            {strings.COMPLETE_VERIFICATION_DATA}
                                                        </h3>
                                                    </div>
                                                </div>

                                            </div>

                                            <div className="col-md-12">
                                                <div className="row">
                                                    <div className="stepwizard">
                                                        <div className="stepwizard-row setup-panel">
                                                            <div className="stepwizard-step">
                                                                <a href="#step-1" className={classnames({ 'btn btn-circle kyc-progress': true, 'btn-green': this.state.step === 1, 'btn-success': this.state.step > 1 })}>1</a>
                                                                <p>{strings.BASIC_INFO}</p>
                                                            </div>
                                                            <div className="stepwizard-step">
                                                                <a href="#step-2" className={classnames({ 'btn btn-circle': true, 'btn-green': this.state.step === 2, 'btn-default': this.state.step < 2, 'btn-success': this.state.step > 2 })} disabled="disabled">2</a>
                                                                <p>{strings.RESIDENCE}</p>
                                                            </div>
                                                            <div className="stepwizard-step">
                                                                <a href="#step-3" className={classnames({ 'btn btn-circle': true, 'btn-green': this.state.step === 3, 'btn-default': this.state.step < 3, 'btn-success': this.state.step > 3 })} disabled="disabled">3</a>
                                                                <p>{strings.ID_UPLOAD}</p>
                                                            </div>
                                                            <div className="stepwizard-step">
                                                                <a href="#step-4" className={classnames({ 'btn btn-circle': true, 'btn-green': this.state.step === 4, 'btn-default': this.state.step < 4, 'btn-success': this.state.step > 4 })} disabled="disabled">4</a>
                                                                <p>{strings.SELFIE_UPLOAD}</p>
                                                            </div>
                                                            <div className="stepwizard-step">
                                                                <a href="#step-5" className={classnames({ 'btn btn-circle': true, 'btn-success': this.state.step === 5, 'btn-default': this.state.step < 5 })} disabled="disabled">5</a>
                                                                <p>{strings.FINISH}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="m-wizard__form-step m-wizard__form-step--current">
                                                <div className="row">
                                                    {this.state.step === 1 &&
                                                        <div className="col-xl-8 offset-xl-2">
                                                            <div className="m-form__section m-form__section--first">
                                                                <div className="m-form__heading" style={{ marginBottom: "30px" }}>
                                                                    <h3 className="m-form__heading-title">{strings.CUSTOMER_DETAILS}</h3>
                                                                </div>
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.FULL_NAME_DOTS}</b></label>
                                                                    <div className="col-xl-9 col-lg-9">
                                                                        <input type="text" name="name" disabled={this.state.isSocial} value={this.state.fullName} onChange={this.handleChange('fullName')} style={{ marginBottom: "5px" }} className="form-control m-input" />
                                                                        <span className="m-form__help" style={{ fontSize: "12px", color: "#1AA74A" }}>*{strings.INPUT_FULLNAME_INFO}</span><br />
                                                                        {this.state.formTouched && this.state.fullName.length === 0 && <span className="m-form__help" style={{ fontSize: "12px", color: "red" }}>{strings.PLEASE_ENTER_FIRST_LAST_NAMES}</span>}
                                                                    </div>
                                                                </div>
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.NATIONALITY}</b></label>
                                                                    <div className="col-xl-9 col-lg-9">
                                                                        {/* <input type="text" name="name" value={this.state.fullName} onChange={this.handleChange('fullName')} style={{marginBottom:"5px"}} className="form-control m-input" placeholder="Nick Stone"/> */}
                                                                        <Select
                                                                            name="form-field-name"
                                                                            style={{ marginBottom: "5px" }}
                                                                            value={this.state.nationality}
                                                                            options={this.state.nationalities}
                                                                            onChange={(val) => {
                                                                                this.setState({ nationality: val.value })
                                                                            }}
                                                                        />
                                                                        {/* <span className="m-form__help" style={{fontSize:"12px",color:"#7b7e8a"}}>Please select the Nationality</span> */}
                                                                    </div>
                                                                </div>
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.PLACE_OF_BIRTH}</b></label>
                                                                    <div className="col-xl-9 col-lg-9">
                                                                    <input type="text" name="name" value={this.state.placeOfBirth} onChange={this.handleChange('placeOfBirth')} style={{ marginBottom: "5px" }} className="form-control m-input" />
                                                                    {this.state.formTouched && this.state.placeOfBirth.length === 0 && <span className="m-form__help" style={{ fontSize: "12px", color: "red" }}>{strings.PLEASE_ENTER_YOUR_PLACE_OF_BIRTH}</span>}
                                                                    </div>
                                                                </div>
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.DATE_OF_BIRTH}</b></label>
                                                                    <div className="col-xl-9 col-lg-9">
                                                                        <DatePicker
                                                                            dropdownMode="select"
                                                                            showMonthDropdown
                                                                            showYearDropdown
                                                                            style={{ width: "100%", marginBottom: "5px" }}
                                                                            className="form-control col-12"
                                                                            dateFormat={"DD/MM/YYYY"}
                                                                            selected={this.state.dateOfBirthday}
                                                                            placeholderText="dd/mm/yyyy"
                                                                            onChange={(val) => this.onChangeDob(val)}
                                                                            maxDate={moment()}
                                                                        />
                                                                        {this.state.formTouched && this.state.underAge && <span className="m-form__help" style={{ fontSize: "12px", color: "red" }}>{strings.YOU_SHOULD_AT_LEAST_AGE}</span>}
                                                                    </div>
                                                                </div>
                                                                {this.props.language === "id" ?
                                                                    <div className="form-group m-form__group row">
                                                                        <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.JOB}</b></label>
                                                                        <div className="col-xl-9 col-lg-9">
                                                                            <Select
                                                                                name="form-field-name"
                                                                                style={{ marginBottom: "5px" }}
                                                                                value={this.state.jobID}
                                                                                options={this.state.jobIDs}
                                                                                onChange={(val) => {
                                                                                    this.setState({ jobID: val.value })
                                                                                }}
                                                                            />
                                                                            {this.state.formTouched && this.state.jobID.length === 0 && <span className="m-form__help" style={{ fontSize: "12px", color: "red" }}>{strings.PLEASE_INSERT_YOUR_JOB}</span>}
                                                                        </div>
                                                                    </div> :
                                                                    <div className="form-group m-form__group row">
                                                                        <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.JOB}</b></label>
                                                                        <div className="col-xl-9 col-lg-9">
                                                                            <Select
                                                                                name="form-field-name"
                                                                                style={{ marginBottom: "5px" }}
                                                                                value={this.state.jobID}
                                                                                options={this.state.jobENs}
                                                                                onChange={(val) => {
                                                                                    this.setState({ jobID: val.value })
                                                                                }}
                                                                            />
                                                                            {this.state.formTouched && this.state.jobID.length === 0 && <span className="m-form__help" style={{ fontSize: "12px", color: "red" }}>{strings.PLEASE_INSERT_YOUR_JOB}</span>}
                                                                        </div>
                                                                    </div>
                                                                }
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.PHONE_NUMBER}</b></label>
                                                                    <div className="col-xl-9 col-lg-9">
                                                                        <div className="" style={{ marginBottom: "5px" }}>
                                                                            <Phone
                                                                                className="form-control m-input"
                                                                                style={{ zIndex: "0" }}
                                                                                country="ID"
                                                                                placeholder={strings.ENTER_PHONE_NUMBER}
                                                                                value={phone}
                                                                                onChange={(phone) => this.onKeyDownPhone(phone)}
                                                                                error={phone && isValidPhoneNumber(phone) ? undefined : inValidPhoneNumber}
                                                                                indicateInvalid={phone.length == 0 ? false : true}
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.GENDER}</b></label>
                                                                    <div className="col-xl-9 col-lg-9">
                                                                        <Select
                                                                            value={this.state.gender}
                                                                            options={this.state.genders}
                                                                            style={{ marginBottom: "5px" }}
                                                                            onChange={(val) => {
                                                                                this.setState({ gender: val.value })
                                                                            }}
                                                                        />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    }
                                                    {this.state.step === 2 &&
                                                        <div className="col-xl-8 offset-xl-2">
                                                            <div className="m-form__section m-form__section--first">
                                                                <div className="m-form__heading" style={{ marginBottom: "30px" }}>
                                                                    <h3 className="m-form__heading-title">{strings.ADDRESS_DETAILS}</h3>
                                                                </div>
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.COUNTRY}</b></label>
                                                                    <div className="col-xl-9 col-lg-9">
                                                                        {/* <input type="text" name="name" value={this.state.fullName} onChange={this.handleChange('fullName')} style={{marginBottom:"5px"}} className="form-control m-input" placeholder="Nick Stone"/> */}
                                                                        <Select
                                                                            name="form-field-name"
                                                                            style={{ marginBottom: "5px" }}
                                                                            value={this.state.country}
                                                                            options={this.state.countries}
                                                                            onChange={(val) => {
                                                                                this.setState({ country: val.value })
                                                                            }}
                                                                        />
                                                                        {/* <span className="m-form__help" style={{fontSize:"12px",color:"#7b7e8a"}}>Please select the Country</span> */}
                                                                    </div>
                                                                </div>
                                                                {this.state.country === "INDONESIA" &&
                                                                    <div>
                                                                        {this.props.language === "id" ?
                                                                            <div className="form-group m-form__group row">
                                                                                <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.DOMICILE}</b></label>
                                                                                <div className="col-xl-9 col-lg-9">
                                                                                    <Select
                                                                                        name="form-field-name"
                                                                                        style={{ marginBottom: "5px" }}
                                                                                        value={this.state.provincesID}
                                                                                        options={this.state.provincesIDs}
                                                                                        onChange={(val) => {
                                                                                            this.setState({ provincesID: val.value })
                                                                                        }}
                                                                                    />
                                                                                    {this.state.formTouched && this.state.provincesID.length === 0 && <span className="m-form__help" style={{ fontSize: "12px", color: "red" }}>{strings.PLEASE_INSERT_YOUR_DOMICILE}</span>}
                                                                                </div>
                                                                            </div> :
                                                                            <div className="form-group m-form__group row">
                                                                                <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.DOMICILE}</b></label>
                                                                                <div className="col-xl-9 col-lg-9">
                                                                                    <Select
                                                                                        name="form-field-name"
                                                                                        style={{ marginBottom: "5px" }}
                                                                                        value={this.state.provincesID}
                                                                                        options={this.state.provincesENs}
                                                                                        onChange={(val) => {
                                                                                            this.setState({ provincesID: val.value })
                                                                                        }}
                                                                                    />
                                                                                    {this.state.formTouched && this.state.provincesID.length === 0 && <span className="m-form__help" style={{ fontSize: "12px", color: "red" }}>{strings.PLEASE_INSERT_YOUR_DOMICILE}</span>}
                                                                                </div>
                                                                            </div>
                                                                        }
                                                                    </div>
                                                                }
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.CITY} </b></label>
                                                                    <div className="col-xl-9 col-lg-9">
                                                                        <input type="text" name="name" value={this.state.city} onChange={this.handleChange('city')} style={{ marginBottom: "5px" }} className="form-control m-input" />
                                                                        {this.state.formTouched && this.state.city.length === 0 && <span className="m-form__help" style={{ fontSize: "12px", color: "red" }}>{strings.ENTER_YOUR_CITY}</span>}
                                                                    </div>
                                                                </div>
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.ADDRESS} </b></label>
                                                                    <div className="col-xl-9 col-lg-9">
                                                                        <textarea type="text" name="name" value={this.state.address} onChange={this.handleChange('address')} style={{ marginBottom: "5px" }} className="form-control m-input" />
                                                                        {this.state.formTouched && this.state.address.length === 0 && <span className="m-form__help" style={{ fontSize: "12px", color: "red" }}>{strings.STREET_ADDRESS_POBOX_COMPANYNAME}</span>}
                                                                    </div>
                                                                </div>
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-xl-3 col-lg-2 col-form-label"><b style={{ fontSize: '13px' }}>* {strings.POSTAL_CODE}</b></label>
                                                                    <div className="col-xl-9 col-lg-9 postal-code">
                                                                        <input type="text" name="name" value={this.state.zipCode} onChange={this.handleChange('zipCode')} style={{ marginBottom: "5px" }} className="form-control m-input" />
                                                                        {this.state.formTouched && this.state.zipCode.length < 5 && <span className="m-form__help" style={{ fontSize: "12px", color: "red" }}>{strings.PLEASE_ENTER_YOUR_POSTAL_CODE}</span>}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    }
                                                    {this.state.step === 3 &&
                                                        <div className="col-xl-8 offset-xl-2">
                                                            <div className="m-form__section m-form__section--first">
                                                                <div className="m-form__heading" style={{ marginBottom: "30px" }}>
                                                                    <h3 className="m-form__heading-title">{strings.IDENTIFICATION_DOCUMENTS}</h3>
                                                                </div>
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-form-label col-form-label-required">*</label>
                                                                    <label className="col-xl-3 col-lg-3 col-form-label">
                                                                        <b style={{ fontSize: '13px' }}>{strings.IDENTIFICATION_TYPE} </b>
                                                                    </label>
                                                                    <div className="col-xl-8 col-lg-8">
                                                                        {/* <input type="text" name="name" value={this.state.fullName} onChange={this.handleChange('fullName')} style={{marginBottom:"5px"}} className="form-control m-input" placeholder="Nick Stone"/> */}
                                                                        <Select
                                                                            name="form-field-name"
                                                                            value={this.state.idType}
                                                                            options={this.state.idTypes}
                                                                            style={{ marginBottom: "5px" }}
                                                                            onChange={(val) => {
                                                                                this.setState({ idType: val.value })
                                                                            }}
                                                                        />
                                                                        <span className="m-form__help" style={{ fontSize: "12px", color: this.state.formTouched && this.state.idType.length === 0 ? "red" : "#7b7e8a" }}>{strings.SELECT_YOUR_IDENTIFICATION_TYPE}</span>
                                                                    </div>
                                                                </div>
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-form-label col-form-label-required">*</label>
                                                                    <label className="col-xl-3 col-lg-3 col-form-label">
                                                                        <b style={{ fontSize: '13px' }}>{strings.ID_NUMBER}</b>
                                                                    </label>
                                                                    <div className="col-xl-8 col-lg-8">
                                                                        <input type="text" name="name" value={this.state.identityNumber} onChange={this.handleChange('identityNumber')} style={{ marginBottom: "5px" }} className="form-control m-input" />
                                                                        <span className="m-form__help" style={{ fontSize: "12px", color: this.state.formTouched && this.state.identityNumber.length === 0 ? "red" : "#7b7e8a" }}>{strings.PLEASE_ENTER_KTP_PASSPORT_NUMBER}</span>
                                                                    </div>
                                                                </div>
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-form-label col-form-label-required">*</label>
                                                                    <label className="col-xl-3 col-lg-3 col-form-label">
                                                                        <b style={{ fontSize: '13px' }}>{strings.PHOTO_SCAN_ID}</b>
                                                                        {
                                                                            // <p style={{ color: 'grey', fontSize:"13px"}}>{strings.CLICK} <a onClick={() => this.setState({ showReference1: true})} className="is-link">{strings.HERE}</a> {strings.FOR_REFERENCE}</p>
                                                                        }
                                                                    </label>
                                                                    <div className="col-xl-8 col-lg-8">
                                                                        <S3Upload
                                                                            touched={this.state.formTouched}
                                                                            value={this.state.fileIdentityPath}
                                                                            className="custom-file-input"
                                                                            signingUrl={process.env.REACT_APP_SIGNING_URL}
                                                                            onFinish={(file) => { this.setState({ fileIdentityPath: file }) }}
                                                                        />
                                                                        {/* <span className="m-form__help" style={{fontSize:"12px",color:"#7b7e8a"}}>Please enter your Address</span> */}
                                                                    </div>
                                                                </div>
                                                                {
                                                                    this.state.idType === 'KTP' &&
                                                                    <div className="form-group m-form__group row">
                                                                        <label className="col-xl-3 col-lg-3 col-form-label"></label>
                                                                        <div className="col-xl-8 col-lg-8">
                                                                            <p>{strings.EXAMPLE_OF_KTP}</p>
                                                                            <img
                                                                                src="/assets/media/img/KYC-sample-KTP.png"
                                                                                style={{ width: "100%" }}
                                                                            />
                                                                            <br />
                                                                        </div>
                                                                    </div>
                                                                }
                                                                {
                                                                    this.state.idType === 'PASSPORT' &&
                                                                    <div className="form-group m-form__group row">
                                                                        <label className="col-xl-3 col-lg-3 col-form-label"></label>
                                                                        <div className="col-xl-8 col-lg-8">
                                                                            <p>{strings.EXAMPLE_OF_PASSPORT}</p>
                                                                            <img
                                                                                src="/assets/media/img/KYC-sample-Passport.png"
                                                                                style={{ width: "100%" }}
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                }
                                                                {
                                                                    this.state.idType === 'DRIVING LICENSE' &&
                                                                    <div className="form-group m-form__group row">
                                                                        <label className="col-xl-3 col-lg-3 col-form-label"></label>
                                                                        <div className="col-xl-8 col-lg-8">
                                                                            <p>{strings.EXAMPLE_OF_DRIVING_LICENSE}</p>
                                                                            <img
                                                                                src="/assets/media/img/KYC-sample-SIM
                                                                                .png"
                                                                                style={{ width: "100%" }}
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                }
                                                                {/* <div className="form-group m-form__group row">
                                                                    <label className="col-form-label col-form-label-required">*</label>
                                                                    <label className="col-xl-3 col-lg-3 col-form-label">
                                                                        <b style={{ fontSize: '13px'}}>{strings.PHOTO_SCAN_PROOF_ADDRESS}</b>
                                                                        <p style={{ color: 'grey', fontSize:"13px"}}>{strings.CLICK} <a onClick={() => this.setState({ showReference3: true})} className="is-link">{strings.HERE}</a> {strings.FOR_REFERENCE}</p>
                                                                    </label>
                                                                    <div className="col-xl-8 col-lg-8">
                                                                        <S3Upload
                                                                            touched={this.state.formTouched}
                                                                            value={this.state.fileProofAddressDocumentPath}
                                                                            className="custom-file-input"
                                                                            signingUrl={process.env.REACT_APP_SIGNING_URL}
                                                                            onFinish={(file) => { this.setState({ fileProofAddressDocumentPath: file }) }}
                                                                        />
                                                                        <span className="m-form__help" style={{fontSize:"12px",color:"#7b7e8a"}}>Please enter your Address</span>
                                                                    </div>
                                                                </div> */}

                                                            </div>
                                                        </div>
                                                    }
                                                    {this.state.step === 4 &&
                                                        <div className="col-xl-8 offset-xl-2">
                                                            <div className="m-form__section m-form__section--first">
                                                                <div className="m-form__heading" style={{ marginBottom: "30px" }}>
                                                                    <h3 className="m-form__heading-title">{strings.IDENTIFICATION_DOCUMENTS}</h3>
                                                                </div>
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-form-label col-form-label-required">*</label>
                                                                    <label className="col-xl-3 col-lg-3 col-form-label">
                                                                        <b style={{ fontSize: '13px' }}>{strings.SELFIE_HOLDING_ID}</b>
                                                                        {
                                                                            // <p style={{ color: 'grey', fontSize:"13px"}}>{strings.CLICK} <a onClick={() => this.setState({ showReference2: true})} className="is-link">{strings.HERE}</a> {strings.FOR_REFERENCE}</p>
                                                                        }
                                                                    </label>
                                                                    <div className="col-xl-8 col-lg-8">
                                                                        <S3Upload
                                                                            touched={this.state.formTouched}
                                                                            value={this.state.filePhotoWithIdentityPath}
                                                                            className="custom-file-input"
                                                                            signingUrl={process.env.REACT_APP_SIGNING_URL}
                                                                            onFinish={(file) => { this.setState({ filePhotoWithIdentityPath: file }) }}
                                                                        />
                                                                        {/* <span className="m-form__help" style={{fontSize:"12px",color:"#7b7e8a"}}>Please enter your Address</span> */}
                                                                    </div>
                                                                </div>
                                                                <div className="form-group m-form__group row">
                                                                    <label className="col-xl-3 col-lg-3 col-form-label"></label>
                                                                    <div className="col-xl-12 col-lg-12">
                                                                        <p>{strings.EXAMPLE_OF_A_GOOD_SELFIE} </p>
                                                                        {this.props.language == 'id'?
                                                                            <img
                                                                              src="../assets/media/img/selfie-steps-id.jpg"
                                                                              style={{ width: "100%" }}
                                                                            />
                                                                            :
                                                                            <img
                                                                                src="../assets/media/img/selfie-steps.jpg"
                                                                                style={{ width: "100%" }}
                                                                            />
                                                                        }
                                                                    </div>
                                                                    {/* <div className="col-xl-4 col-lg-4">
                                                                        <p style={{ fontSize: '12px' }}>{strings.FACE}</p>
                                                                        <ol type='1' style={{ fontSize: '12px' }}>
                                                                            <li>{strings.DONOT_WEAR_SUNGLASSES_HEADBAND}</li>
                                                                            <li>{strings.LOOKING_STRAIGHT_AT_CAMERA}</li>
                                                                        </ol>
                                                                        <p style={{ fontSize: '12px' }}>{strings.WRITTEN}</p>
                                                                        <ol start='3' style={{ fontSize: '12px' }}>
                                                                            <li>{strings.WORD_VERIFICATION}</li>
                                                                            <li>
                                                                                {strings.WRITE_CLEARLY_YOUR}
                                                                                <ul>
                                                                                    <li>{strings.SIGNATURE}</li>
                                                                                    <li>{strings.FULL_NAME}</li>
                                                                                    <li>{strings.DATE_SUBMISSION}</li>
                                                                                </ul>
                                                                            </li>
                                                                        </ol>
                                                                        <p style={{ fontSize: '12px' }}>{strings.ADDITIONAL}</p>
                                                                        <ul style={{ fontSize: '12px' }}>
                                                                            <li>{strings.RED_EYE_PHOTOGRAPHIC_FLASH}</li>
                                                                            <li>{strings.FACE_WRITING_CLEAR}</li>
                                                                            <li>{strings.PHOTO_COLOUR_UNEDITED}</li>
                                                                        </ul>
                                                                    </div> */}
                                                                </div>
                                                                {/* <div className="form-group m-form__group row">
                                                                    <label className="col-form-label col-form-label-required">*</label>
                                                                    <label className="col-xl-3 col-lg-3 col-form-label">
                                                                        <b style={{ fontSize: '13px'}}>{strings.PHOTO_SCAN_PROOF_ADDRESS}</b>
                                                                        <p style={{ color: 'grey', fontSize:"13px"}}>{strings.CLICK} <a onClick={() => this.setState({ showReference3: true})} className="is-link">{strings.HERE}</a> {strings.FOR_REFERENCE}</p>
                                                                    </label>
                                                                    <div className="col-xl-8 col-lg-8">
                                                                        <S3Upload
                                                                            touched={this.state.formTouched}
                                                                            value={this.state.fileProofAddressDocumentPath}
                                                                            className="custom-file-input"
                                                                            signingUrl={process.env.REACT_APP_SIGNING_URL}
                                                                            onFinish={(file) => { this.setState({ fileProofAddressDocumentPath: file }) }}
                                                                        />
                                                                        <span className="m-form__help" style={{fontSize:"12px",color:"#7b7e8a"}}>Please enter your Address</span>
                                                                    </div>
                                                                </div> */}

                                                            </div>
                                                        </div>
                                                    }
                                                    {this.state.loading && this.state.step === 5 &&
                                                        <div className="mx-auto">
                                                            <Loader type="line-scale" active={true} className="float-right" />
                                                        </div>
                                                    }
                                                    {!this.state.loading && this.state.step === 5 && 
                                                        <div className="mx-auto col-md-3 col-xs-12">
                                                            {!this.state.kycErrorId ?
                                                                <span>
                                                                    <img
                                                                    className="success-image"
                                                                    src={require('../../img/big-success.png')} />

                                                                    <p className="" style={{ marginTop: 20, textAlign: "center", fontWeight: "800" }}>
                                                                        {strings.THANK_YOU_MESSAGE_KYC_PENDING}
                                                                    </p>
                                                                
                                                                    <p className="" style={{ marginTop: 10, textAlign: "center" }}>
                                                                        {strings.CHECL_STATUS_YOUR_KYC_USER_PROFILE}
                                                                    </p>

                                                                    <button onClick={() => { window.location = "/profile"; }} href="/" type="button" style={{ width: "100%" }} className="btn btn-blue m-btn m-btn--custom">{strings.GO_TO_MY_PROFILE}</button>
                                                                </span>:
                                                                <span>
                                                                    <img
                                                                    className="success-image"
                                                                    src={require('../../img/big-error.png')} />
                                                                   
                                                                    <p className="" style={{ marginTop: 20, textAlign: "center", fontWeight: "800" }}>
                                                                        This ID is already registered.<br/>
                                                                        Please submit with your correct ID
                                                                    </p>

                                                                    <button onClick={() => this.prevStep()} href="/" type="button" style={{ width: "100%" }} className="btn btn-blue m-btn m-btn--custom">{strings.BACK}</button>
                                                                    
                                                                </span>
                                                            }
                                                           
                                                            {/* <p
                                                                className="is-link success-kyc-link"
                                                                onClick={() => history.push('/')}
                                                                style={{textAlign: 'center'}} href="/">Back to Dashboard</p> */}
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                            <div className="m-portlet__foot m-portlet__foot--fit m--margin-top-20 kyc-container">
                                                <div className="m-form__actions m-form__actions m--margin-top-30 m--padding-bottom-20">
                                                    <div className="row">
                                                        <div className="col-lg-2"></div>
                                                        <div className="col-lg-4 m--align-left back-button-kyc">
                                                            {this.state.step !== 5 && this.state.step !== 1 &&
                                                                <button onClick={() => this.prevStep()} disabled={this.state.step === 1} className="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                                                    <span>
                                                                        <i className="la la-arrow-left"></i>&nbsp;&nbsp;
                                                                        <span>{strings.BACK}</span>
                                                                    </span>
                                                                </button>
                                                            }
                                                        </div>
                                                        <div className="col-lg-4 m--align-right">
                                                            {this.state.step != 5 &&
                                                                <button onClick={() => this.nextStep()} className="btn btn-warning m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
                                                                    <span>
                                                                        <span>{strings.CONTINUE}</span>&nbsp;&nbsp;
                                                                        <i className="la la-arrow-right"></i>
                                                                    </span>
                                                                </button>
                                                            }
                                                        </div>
                                                        <div className="col-lg-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>}

                        </div>
                    </div>
                </div>

                {
                    /*
    
                    <Modal isOpen={this.state.showReference1} toggle={() => this.close()} className={this.props.className}>
                        <ModalHeader toggle={() => this.close()}>{strings.PHOTO_SCAN_ID}</ModalHeader>
                        <ModalBody className="tc-modal-body">
                            <p>{strings.PHOTO_SCANS_KTP_CARD_NATIONAL_PASSPORT}</p>
                            <p>{strings.EXAMPLE_OF_KTP}</p>
                                <img
                                    src="/assets/media/img/ktp-sample.png"
                                    style={{ width: "100%"}}
                                />
                            <br /><br />
                            <p>{strings.EXAMPLE_OF_PASSPORT}</p>
                                <img
                                    src="/assets/media/img/passport-sample.png"
                                    style={{ width: "100%"}}
                                />
                        </ModalBody>
                    </Modal>
    
                    <Modal isOpen={this.state.showReference2} toggle={() => this.close()} className={this.props.className}>
                        <ModalHeader toggle={() => this.close()}>{strings.SELFIE_HOLDING_ID}</ModalHeader>
                        <ModalBody className="tc-modal-body">
                           <p>{strings.TAKE_SELFIE_HOLDING_DOCUMENT_VERIFICATION_FORM}</p>
                           <ul>
                               <li>{strings.WORD_VERIFICATION}</li>
                               <li>{strings.WRITE_YOUR_IDENTITY}</li>
                           </ul>
                           <p>{strings.PLEASE_FOLLOW_PHOTO_REQUIREMENTS}</p>
                           <ul>
                               <li>{strings.DONOT_WEAR_SUNGLASSES_HEADBAND}</li>
                               <li>{strings.LOOKING_STRAIGHT_AT_CAMERA}</li>
                               <li>{strings.RED_EYE_PHOTOGRAPHIC_FLASH}</li>
                               <li>{strings.FACE_WRITING_CLEAR}</li>
                               <li>{strings.PHOTO_COLOUR_UNEDITED}</li>
                           </ul>
                           <p>{strings.THIS_PHOTO_REQUIRED} </p>
                           <p>{strings.EXAMPLE_OF_A_GOOD_SELFIE} </p>
                           <img
                                    src="/assets/media/img/selfie-holding.jpg"
                                    style={{ width: "100%"}}
                                />
                        </ModalBody>
                    </Modal>
                    */
                }

                <Modal isOpen={this.state.showReference3} toggle={() => this.close()} className={this.props.className}>
                    <ModalHeader toggle={() => this.close()}>{strings.PHOTO_OR_SCAN_PROOF_ADDRESS}</ModalHeader>
                    <ModalBody className="tc-modal-body">
                        <p>{strings.PROVIDE_PHOTO_SCANS_VERIFICATION_FORM_EXAMPLE}</p>
                        <p>{strings.PLEASE_MAKE_SURE_SCAN_CORRESPOND_FOLLOWING_REQUIRENMENT}</p>
                        <ul>
                            <li>{strings.UPLOADED_DOCUMENTS_VALIDITY_DATE_ISSUE}</li>
                            <li>{strings.PROOF_RESIDENCY_DOCUMENT_ADDRESSED}</li>
                            <li>{strings.PROOF_RESIDENCY_DOCUMET_ADDRESSED_HOME_ADDRESS}</li>
                            <li>{strings.PROOF_RESIDENCY_DOCUMENT_PHOTO_OR_IMAGE_OF_PAPER_DOCUMENT}</li>
                            <li>{strings.PROOF_RESIDENCY_DOCUMENT_CONTAIN_DATE_ISSUE}</li>
                        </ul>
                        <p>{strings.EXAMPLE_OF_A_DOCUMENT} </p>
                        <br />
                        <img
                            src="/assets/media/img/bill-sample.png"
                            style={{ width: "100%" }}
                        />
                    </ModalBody>
                </Modal>

            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        trading: state.trading,
        language: state.language
    };
};

const mapDispatchToProps = dispatch => ({
    updateKYC: actions.updateKYC,
    fetchKYC: actions.fetchKYC
});

export default connect(mapStateToProps, mapDispatchToProps)(KYC);