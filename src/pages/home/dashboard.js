import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import homeActions from '../../core/actions/client/home';
import NetworkError from '../../errors/networkError';
import RatesDashboard from '../../components/rates';
import KYCMessage from '../../components/kycMessage';
import NotificationMessage from '../../components/notificationMessage';
import amountFormatter from '../../helpers/amountFormatter';
import RecieveModal from '../../components/recieveModal';
import receiveActions from '../../core/actions/client/recieve';
import SendModal from '../../components/sendModal';
import DepositModal from '../../components/depositModal';
import TradingContest from '../../components/tradingContest';
import WithdrawalModal from '../../components/withdrawalModal';
import { bindActionCreators } from 'redux';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import {faSignInAlt, faSignOutAlt, faPaperPlane, faQrcode, faPlusCircle} from '@fortawesome/fontawesome-free-solid';
import RecentTrades from '../../components/recentTrades';
import WalletAdd from '../../components/home/walletAdd';
import { pixelID } from '../../config/pixel';
import Chart from '../../components/charts';
import { UncontrolledDropdown,Dropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap';
import { NotificationManager } from 'react-notifications';
import { getUserAttributes, updateAttributes } from '../../core/react-cognito/attributes.js'
import async from 'async';
import WalletDashboard from './walletDashboard';
import OrderNow from '../../components/orderBook/orderNow';
import OrderBook from '../../components/orderBook/orderBook';
import Orders from '../../components/orderBook/orders';
import currencyFormatter from '../../helpers/currencyFormatter';
import classnames from 'classnames';
import history from '../../history';
import ReactGA from 'react-ga';

var iotEndpoint
var region
var iotTopic

if(process.env.REACT_APP_ENV === 'demo'){
    iotEndpoint = 'a1sm5n1r7dws4l-ats.iot.us-west-2.amazonaws.com';
    region = 'us-west-2'
}else if(process.env.REACT_APP_ENV === 'local' || process.env.REACT_APP_ENV === 'dev'){
    iotEndpoint = 'a1sm5n1r7dws4l-ats.iot.us-east-1.amazonaws.com';
    region = 'us-east-1'
}else if(process.env.REACT_APP_ENV === 'production'){
    iotEndpoint = 'a1sm5n1r7dws4l-ats.iot.ap-southeast-1.amazonaws.com';
    region = 'ap-southeast-1'
}

var awsIot = require('aws-iot-device-sdk');
var iotDevice = awsIot.device({
    region: region,
    protocol: 'wss',
    keepalive: 600,
    accessKeyId: 'AKIAIVWDMCSFMKW3RGMA',
    secretKey: '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',
    host: iotEndpoint
});

var mainURL = history.location.pathname.split('/')[3];

let ReactPixel = null;
fontawesome.library.add(faSignInAlt, faSignOutAlt, faPaperPlane, faQrcode, faPlusCircle)

let strings = new LocalizedStrings({
	en: enDictionary,
	id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class Dashboard extends Component {
    constructor(){
        super();
        this.state = {
            rates: [],
            activeTab: 'BTC',
            activeCurrency: 'BTC',
            IDRAccount: {},
            BTCAccount: {},
            ETHAccount: {},
            XRPAccount: {},
            DGXAccount: {},
            GUSDAccount: {},
            PAXAccount: {},
            USDCAccount: {},
            TUSDAccount: {},
            USDTAccount: {},
            TTCAccount: {},
            SWIPEAccount: {},
            ZILAccount: {},
            receiveModal: false,
            sendModal: false,
            depositModal: false,
            withdrawalModal: false,
			activeAccount: {},
			IDRLimit: 0,
			IDRUsageText: 0,
            IDRUsage: 0,
            IDRUsageLeft: 0,
            ETHUsage: 0,
            BTCUsage: 0,
            XRPUsage: 0,
            DGXUsage: 0,
            GUSDUsage: 0,
            PAXUsage: 0,
            USDCUsage: 0,
            TUSDUsage: 0,
            USDTUsage: 0,
            TTCUsage: 0,
            SWIPEUsage: 0,
            ZILUsage: 0,
            balanceLoading: false,
            currencyPairData: {},
            rateHistory: {},
            btcEqualRates: 0,
            ethEqualRates: 0,
            dgxEqualRates: 0,
            gusdEqualRates: 0,
            paxEqualRates: 0,
            usdcEqualRates: 0,
            tusdEqualRates: 0,
            usdtEqualRates: 0,
            ttcEqualRates: 0,
            swipeEqualRates: 0,
            zilEqualRates: 0,
            XRPAccountFound: false,
            DGXAccountFound: false,
            GUSDAccountFound: false,
            PAXAccountFound: false,
            USDCAccountFound: false,
            TUSDAccountFound: false,
            USDTAccountFound: false,
            TTCAccountFound: false,
            SWIPEAccountFound: false,
            ZILAccountFound: false,
            contestModal: false, //for trading contest
            refreshDataContest: false, //for trading contest
            dropDown: false,
            walletAdd:false,
            userId:'',
            mostRecentConversion: 0,
            BTCVolume: 0,
            ETHVolume: 0,
            XRPVolume: 0,
            DGXVolume: 0,
            TTCVolume: 0,
            PAXVolume: 0,
            TUSDVolume: 0,
            GUSDVolume: 0,
            USDTVolume: 0,
            USDCVolume: 0,
            SWIPEVolume: 0,
            ZILVolume: 0
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.value
            this.setState(obj)

        }
        this.receive = this.receive.bind(this);
        this.send = this.send.bind(this);
    }

    callGA(){
        ReactGA.initialize('UA-116825757-2');
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    getToken() {
        if(this.props.user){
            let username = this.props.user.username
            let clientId = this.props.user.pool.clientId
            let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
            return token
        }else{
            return 0
        }
    }

    configureIot() {

        async.series({
            getUserid: (callback) => {
                getUserAttributes(this.props.user).then(attr => {
                    let user_id = attr.sub;
        
                    this.setState({
                        userId: user_id
                    })

                    callback(null,attr);
                })
                .catch(err => {
                    // 
                    callback(err, null);
                });
            },
            callIot: (callback) => {

                var iotEndpoint = ''
                var topic = ''
                var region = ''

                if(process.env.REACT_APP_ENV === 'demo'){
                    iotEndpoint = 'a1sm5n1r7dws4l-ats.iot.us-west-2.amazonaws.com';
                    topic = `/pydt/demo/user/${this.state.userId}/orders`;
                    region = 'us-west-2'
                }else if(process.env.REACT_APP_ENV === 'local' || process.env.REACT_APP_ENV === 'dev'){
                    iotEndpoint = 'a1sm5n1r7dws4l-ats.iot.us-east-1.amazonaws.com';
                    topic = `/pydt/dev/user/${this.state.userId}/orders`;
                    region = 'us-east-1'
                }else if(process.env.REACT_APP_ENV === 'production'){
                    iotEndpoint = 'a1sm5n1r7dws4l-ats.iot.ap-southeast-1.amazonaws.com';
                    topic = `/pydt/dev/user/${this.state.userId}/orders`;
                    region = 'ap-southeast-1'
                }
                // const env = PYDT_CONFIG.PROD ? 'prod' : 'dev';
                
                this.iotDevice = awsIot.device({
                  region: region,
                  protocol: 'wss',
                  keepalive: 600,
                  accessKeyId: 'AKIAIVWDMCSFMKW3RGMA',
                  secretKey: '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',
                  host: iotEndpoint
                });
            
                this.iotDevice.on('connect', () => {
                  this.iotDevice.subscribe(topic);
                  
                });
            
                this.iotDevice.on('error', err => {
                  
                  callback(err, null);
                });
            
                this.iotDevice.on('message', (recTopic, message) => {

                    
                 
                    if (recTopic === topic) {
        
                    var string = new TextDecoder("utf-8").decode(message);

                    let msg = JSON.parse(string)
                    
                    NotificationManager.success(msg.msg, msg.title, 8000);
                    this.fetchDashboard();
        
                    }
                });

                callback(null);
            }
        }, (err) => {
            
        });
    }

    componentWillMount() {
        strings.setLanguage(this.props.language);
        this.loadCurrencyFromURL();
    }

    openContest() {
        this.setState({ contestModal: true, refreshDataContest: true })
    }

    fetchCurrencyLimit(){
        homeActions.getCurrencyUsage(this.getToken(), 'idr').then(res => {

            let limit = Number(res.data.data.idr.dailyWithdrawalLimit);
            let usage = limit - Number(res.data.data.idr.availableWithdrawAmountForTheDay);

            let percentage = (usage / limit ) * 100;

            // BTC

            let BTClimit = Number(res.data.data.btc.dailyWithdrawalLimit);
            let BTCusage = Number(res.data.data.btc.availableWithdrawAmountForTheDay);

            let BTCpercentage = (BTCusage / BTClimit);

            // ETH

            let ETHlimit = Number(res.data.data.eth.dailyWithdrawalLimit);
            let ETHusage = Number(res.data.data.eth.availableWithdrawAmountForTheDay);

            let ETHpercentage = (ETHusage / ETHlimit );

            // XRP

            let XRPlimit = Number(res.data.data.xrp.dailyWithdrawalLimit);
            let XRPusage = Number(res.data.data.xrp.availableWithdrawAmountForTheDay);

            let XRPpercentage = (XRPusage / XRPlimit );

            // DGX

            let DGXlimit = Number(res.data.data.dgx.dailyWithdrawalLimit);
            let DGXusage = Number(res.data.data.dgx.availableWithdrawAmountForTheDay);

            let DGXpercentage = (DGXusage / DGXlimit );

            // GUSD
            let GUSDlimit = Number(res.data.data.gusd.dailyWithdrawalLimit);
            let GUSDusage = Number(res.data.data.gusd.availableWithdrawAmountForTheDay);
            let GUSDpercentage = (GUSDusage / GUSDlimit);

            // PAX
            let PAXlimit = Number(res.data.data.pax.dailyWithdrawalLimit);
            let PAXusage = Number(res.data.data.pax.availableWithdrawAmountForTheDay);
            let PAXpercentage = (PAXusage / PAXlimit);

            // USDC
            let USDClimit = Number(res.data.data.usdc.dailyWithdrawalLimit);
            let USDCusage = Number(res.data.data.usdc.availableWithdrawAmountForTheDay);
            let USDCpercentage = (USDCusage / USDClimit);

            // TUSD
            let TUSDlimit = Number(res.data.data.tusd.dailyWithdrawalLimit);
            let TUSDusage = Number(res.data.data.tusd.availableWithdrawAmountForTheDay);
            let TUSDpercentage = (TUSDusage / TUSDlimit);

            // USDT
            let USDTlimit = Number(res.data.data.usdt.dailyWithdrawalLimit);
            let USDTusage = Number(res.data.data.usdt.availableWithdrawAmountForTheDay);
            let USDTpercentage = (USDTusage / USDTlimit);

            // TTC
            let TTClimit = Number(res.data.data.ttc.dailyWithdrawalLimit);
            let TTCusage = Number(res.data.data.ttc.availableWithdrawAmountForTheDay);
            let TTCpercentage = (TTCusage / TTClimit);

            // SWIPE
            let SWIPElimit = Number(res.data.data.swipe.dailyWithdrawalLimit);
            let SWIPEusage = Number(res.data.data.swipe.availableWithdrawAmountForTheDay);
            let SWIPEpercentage = (SWIPEusage / SWIPElimit);

            // ZILIQA
            let ZILlimit = Number(res.data.data.zil.dailyWithdrawalLimit);
            let ZILusage = Number(res.data.data.zil.availableWithdrawAmountForTheDay);
            let ZILpercentage = (ZILusage / ZILlimit);
            
            this.setState({
                IDRUsage: Number(percentage),
                IDRUsageLeft: Number(res.data.data.idr.availableWithdrawAmountForTheDay),
				IDRLimit: limit,
                IDRUsageText: usage,

                BTCUsage: Number(BTCpercentage) * 100,
                BTCUsageLeft: Number(res.data.data.btc.availableWithdrawAmountForTheDay),

                ETHUsage: Number(ETHpercentage) * 100,
                ETHUsageLeft: Number(res.data.data.eth.availableWithdrawAmountForTheDay),

                XRPUsage: Number(XRPpercentage) * 100,
                XRPUsageLeft: Number(res.data.data.xrp.availableWithdrawAmountForTheDay),

                DGXUsage: Number(DGXpercentage) * 100,
                DGXUsageLeft: Number(res.data.data.dgx.availableWithdrawAmountForTheDay),

                GUSDUsage: Number(GUSDpercentage) * 100,
                GSUDUsageLeft: Number(res.data.data.gusd.availableWithdrawAmountForTheDay),

                PAXUsage: Number(PAXpercentage) * 100,
                PAXUsageLeft: Number(res.data.data.pax.availableWithdrawAmountForTheDay),

                USDCUsage: Number(USDCpercentage) * 100,
                USDCUsageLeft: Number(res.data.data.usdc.availableWithdrawAmountForTheDay),

                TUSDUsage: Number(TUSDpercentage) * 100,
                TUSDUsageLeft: Number(res.data.data.tusd.availableWithdrawAmountForTheDay),

                USDTUsage: Number(USDTpercentage) * 100,
                USDTUsageLeft: Number(res.data.data.usdt.availableWithdrawAmountForTheDay),

                TTCUsage: Number(TTCpercentage) * 100,
                TTCUsageLeft: Number(res.data.data.ttc.availableWithdrawAmountForTheDay),

                SWIPEUsage: Number(SWIPEpercentage) * 100,
                SWIPEUsageLeft: Number(res.data.data.swipe.availableWithdrawAmountForTheDay),
                
                ZILUsage: Number(ZILpercentage) * 100,
                ZILUsageLeft: Number(res.data.data.zil.availableWithdrawAmountForTheDay),
            });
        }).catch(err => {
            // 
        });

        // homeActions.getCurrencyUsage(this.getToken(), 'btc').then(res => {
        //     

        //     let limit = Number(res.data.data.dailyWithdrawalLimit);
        //     let usage = limit - Number(res.data.data.availableWithdrawAmountForTheDay);

        //     let percentage = (usage / limit ) * 100;

        //     this.setState({
        //         BTCUsage: percentage,
        //         BTCUsageLeft: Number(res.data.data.availableWithdrawAmountForTheDay)
        //     });
        // }).catch(err => {
        //     
        // });

        // homeActions.getCurrencyUsage(this.getToken(), 'eth').then(res => {
        //     

        //     let limit = Number(res.data.data.dailyWithdrawalLimit);
        //     let usage = limit - Number(res.data.data.availableWithdrawAmountForTheDay);

        //     let percentage = (usage / limit ) * 100;

        //     this.setState({
        //         ETHUsage: percentage,
        //         ETHUsageLeft: Number(res.data.data.availableWithdrawAmountForTheDay)
        //     });
        // }).catch(err => {
        //     
        // });
    }

    /**
     * this function will get the dashboard data. please maintain to filter supported currencies like 
     * let DGXAccount = _.find(accOrdered, {CurrencyCode: 'DGX'});
     */

    fetchDashboard() {
        let username = this.props.user.username;
        let clientId = this.props.user.pool.clientId;
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];

        this.setState({ balanceLoading: true});
        this.fetchCurrencyLimit();
        homeActions.fetchDashboard(token).then(res => {

            this.setState({
                accountState: res.data.kyc,
                rateHistory: res.data.rateHistory,
                balanceLoading: false
            });

            this.props.actions.setKYCStatus(res.data.kyc);
        }).catch(err => {
            NetworkError(err);
            this.setState({ balanceLoading: false});
            if(window.intervals){
                window.intervals.map((i, index) => {
                    clearInterval(i)
                    window.intervals.splice(index, 1);
                });
            }
        });
    }

    /**
     * TCDOC-013
     * get the wallet address for receive function
     */
    fetchAddresses(){
        this.setState({
            loading: true,
            addresses: []
        })
        receiveActions.fetchAddressList(this.getToken(), this.state.activeCurrency.toLowerCase()).then(res => {
            let qRCodeValue = "";

            if (res.data.length > 0) {
                qRCodeValue = res.data[0].Address;
            }
            this.setState({
                receiveAddress: qRCodeValue,
                addresses: res.data,
                loading: false
            })
        })
            .catch(err => {
                this.setState({
                    loading: false
                })
                NetworkError(err)
            })
    }

    receive(currency){
        this.setState({
            activeCurrency: currency.toUpperCase(),
            receiveModal: true,
            loading: true
        })
    }

    onCloseRecieveModal(){
        this.setState({
            receiveModal: false
        })
    }

    onCloseSendModal(){
        this.setState({
            sendModal: false
        })
    }

    send(currency) {
        let activeAccount = {};

        if (currency === 'btc'){
            activeAccount = this.state.BTCAccount;
        } else if(currency === 'eth'){
            activeAccount = this.state.ETHAccount;
        } else if(currency === 'xrp'){
            activeAccount = this.state.XRPAccount;
        } else if(currency === 'dgx'){
            activeAccount = this.state.DGXAccount;
        } else if(currency === 'pax'){
            activeAccount = this.state.PAXAccount;
        } else if(currency === 'tusd'){
            activeAccount = this.state.TUSDAccount;
        } else if(currency === 'usdc'){
            activeAccount = this.state.USDCAccount;
        } else if(currency === 'gusd'){
            activeAccount = this.state.GUSDAccount;
        } else if(currency === 'usdt'){
            activeAccount = this.state.USDTAccount;
        } else if(currency === 'ttc'){
            activeAccount = this.state.TTCAccount;
        } else if(currency === 'swipe'){
            activeAccount = this.state.SWIPEAccount;
        } else if(currency === 'zil'){
            activeAccount = this.state.ZILAccount;
        }

        this.setState({
            activeCurrency: currency.toUpperCase(),
            sendModal: true,
            activeAccount
        })
        this.fetchAddresses();
    }

    changeActiveTab(currency){

       
        setTimeout(() => {
            history.push('/' + localStorage.getItem('language') + '/dashboard/'+ currency.toUpperCase() + 'IDR')
            window.location.reload();
        },100)
        
       
        this.setState({
            activeTab:currency,
            dropDown: false
        })
    }

    loadCurrencyFromURL(){
        if(history.location.pathname.length > 0){
            if(mainURL === 'BTCIDR'){
                this.setState({
                    activeTab:'BTC'
                })
            }else if(mainURL === 'ETHIDR'){
                this.setState({
                    activeTab:'ETH'
                })
            }else if(mainURL === 'XRPIDR'){
                this.setState({
                    activeTab:'XRP'
                })
            }else if(mainURL === 'DGXIDR'){
                this.setState({
                    activeTab:'DGX'
                })
            }else if(mainURL === 'PAXIDR'){
                this.setState({
                    activeTab:'PAX'
                })
            }else if(mainURL === 'TTCIDR'){
                this.setState({
                    activeTab:'TTC'
                })
            }else if(mainURL === 'USDTIDR'){
                this.setState({
                    activeTab:'USDT'
                })
            }else if(mainURL === 'USDCIDR'){
                this.setState({
                    activeTab:'USDC'
                })
            }else if(mainURL === 'TUSDIDR'){
                this.setState({
                    activeTab:'TUSD'
                })
            }else if(mainURL === 'SWIPEIDR'){
                this.setState({
                    activeTab:'SWIPE'
                })
            }else if(mainURL === 'ZILIDR'){
                this.setState({
                    activeTab:'ZIL'
                })
            }
        }
    }

    /**
     * TCDOC-014
     * this function is to track with google pixel. can just change the pixel id if needed
     */
    initFBPixel() {
        const advancedMatching = { em: this.props.user.username };
        const options = {
            autoConfig: true,
            debug: true,
        };
        ReactPixel =  require('react-facebook-pixel').default;
        ReactPixel.init(pixelID, advancedMatching, options);
        ReactPixel.pageView();
        ReactPixel.track( 'ViewContent', {content_name: 'dashboard'} );
    }

    checkCognitoAttr() {
        getUserAttributes(this.props.user).then(res => {
            // 
        }).catch(err => {
            NetworkError(err);
            // 
        })
    }

    getAccountWithBalance(){
        homeActions.getAccountDashboard(this.getToken()).then(res => {

            let accOrdered = _.orderBy(res.data.data, (acc) => {
                return acc.Currency.Type;
            });
            

            let IDRAccount = _.find(accOrdered, {CurrencyCode: 'IDR'});
            let BTCAccount = _.find(accOrdered, {CurrencyCode: 'BTC'});
            let ETHAccount = _.find(accOrdered, {CurrencyCode: 'ETH'});
            let XRPAccount = _.find(accOrdered, {CurrencyCode: 'XRP'});
            let DGXAccount = _.find(accOrdered, {CurrencyCode: 'DGX'});
            let GUSDAccount = _.find(accOrdered, {CurrencyCode: 'GUSD'});
            let PAXAccount = _.find(accOrdered, {CurrencyCode: 'PAX'});
            let USDCAccount = _.find(accOrdered, {CurrencyCode: 'USDC'});
            let TUSDAccount = _.find(accOrdered, {CurrencyCode: 'TUSD'});
            let USDTAccount = _.find(accOrdered, {CurrencyCode: 'USDT'});
            let TTCAccount = _.find(accOrdered, {CurrencyCode: 'TTC'});
            let SWIPEAccount = _.find(accOrdered, {CurrencyCode: 'SWIPE'});
            let ZILAccount = _.find(accOrdered, {CurrencyCode: 'ZIL'});

            if(!XRPAccount){

                let data = {
                    currency: "XRP"
                }

                homeActions.createNewWallet(this.getToken(),data).then(res => {
                }).catch(err => {
                    this.setState({
                        XRPAccountFound: true
                    })
                    NetworkError(err);
                });
            }

            if(!DGXAccount){

                let data = {
                    currency: "DGX"
                }

                homeActions.createNewWallet(this.getToken(),data).then(res => {
                }).catch(err => {
                    this.setState({
                        DGXAccountFound: true
                    })
                    NetworkError(err);
                });
            }

            if(!GUSDAccount) {
                let data = {
                    currency: "GUSD"
                }

                homeActions.createNewWallet(this.getToken(),data).then(res => {
                }).catch(err => {
                    this.setState({
                        GUSDAccountFound: true
                    })
                    NetworkError(err);
                })
            }

            if(!PAXAccount) {
                let data = {
                    currency: "PAX"
                }

                homeActions.createNewWallet(this.getToken(),data).then(res => {
                }).catch(err => {
                    this.setState({
                        PAXAccountFound: true
                    })
                    NetworkError(err);
                })
            }

            if(!USDCAccount) {
                let data = {
                    currency: "USDC"
                }

                homeActions.createNewWallet(this.getToken(),data).then(res => {
                }).catch(err => {
                    this.setState({
                        USDCAccountFound: true
                    })
                    NetworkError(err);
                })
            }
            if(!TUSDAccount) {
                let data = {
                    currency: "TUSD"
                }

                homeActions.createNewWallet(this.getToken(),data).then(res => {
                }).catch(err => {
                    this.setState({
                        TUSDAccountFound: true
                    })
                    NetworkError(err);
                })
            }

            if(!USDTAccount) {
                let data = {
                    currency: "USDT"
                }

                homeActions.createNewWallet(this.getToken(), data).then(res => {
                }).catch(err => {
                    this.setState({
                        USDTAccount: true
                    })
                    NetworkError(err);
                })
            }

            if(!TTCAccount) {
                let data = {
                    currency: "TTC"
                }

                homeActions.createNewWallet(this.getToken(), data).then(res => {
                }).catch(err => {
                    this.setState({
                        TTCAccount: true
                    })
                    NetworkError(err);
                })
            }

            if(!SWIPEAccount) {
                let data = {
                    currency: "SWIPE"
                }

                homeActions.createNewWallet(this.getToken(), data).then(res => {
                }).catch(err => {
                    this.setState({
                        SWIPEAccount: true
                    })
                    NetworkError(err);
                })
            }

            if(!ZILAccount) {
                
                let data = {
                    currency: "ZIL"
                }

                homeActions.createNewWallet(this.getToken(), data).then(res => {
                }).catch(err => {
                    this.setState({
                        ZILAccountFound: true
                    })
                    NetworkError(err);
                })
            }else{
                this.setState({ ZILAccount })
            }

            this.setState({
                accounts: accOrdered,
                IDRAccount,
                BTCAccount,
                ETHAccount,
                XRPAccount,
                DGXAccount,
                GUSDAccount,
                USDCAccount,
                TUSDAccount,
                PAXAccount,
                USDTAccount,
                TTCAccount,
                SWIPEAccount,
                balanceLoading: false,
            });

        }).catch(err => {
            console.log("---------ddd---",NetworkError, err);
            NetworkError(err);
        })
    }

    getCurrencyPair(){
        homeActions.getCurrencyPair(this.getToken()).then(res => {

            let currencyPair = res.data.data

            this.setState({
                currencyPairData: currencyPair
            });

        }).catch(err => {
            NetworkError(err);
        })
    }

    getRates(){
        homeActions.getRatesDashboard(this.getToken()).then(res => {

            let rates = res.data.data

            let btcRatesPair = _.find(rates, { currencyPair: 'BTC'+'IDR'}) 
            let ethRatesPair = _.find(rates, { currencyPair: 'ETH'+'IDR'}) 
            let xrpRatesPair = _.find(rates, { currencyPair: 'XRP'+'IDR'})
            let dgxRatesPair = _.find(rates, { currencyPair: 'DGX'+'IDR'})
            let gusdRatesPair = _.find(rates, { currencyPair: 'GUSD'+'IDR'})
            let paxRatesPair = _.find(rates, { currencyPair: 'PAX'+'IDR'})
            let usdcRatesPair = _.find(rates, { currencyPair: 'USDC'+'IDR'})
            let tusdRatesPair = _.find(rates, { currencyPair: 'TUSD'+'IDR'})
            let usdtRatesPair = _.find(rates, { currencyPair: 'USDT'+'IDR'})
            let ttcRatesPair = _.find(rates, { currencyPair: 'TTC'+'IDR'})
            let swipeRatesPair = _.find(rates, { currencyPair: 'SWIPE'+'IDR'})
            let zilRatesPair = _.find(rates, { currencyPair: 'ZIL'+'IDR'})

            if(!_.isEmpty(rates)){

                if(xrpRatesPair){
                    this.setState({xrpEqualRates: xrpRatesPair.customer_buy})
                }
                
                if(dgxRatesPair){
                    this.setState({dgxEqualRates: dgxRatesPair.customer_buy})
                }

                if(gusdRatesPair){
                    this.setState({gusdEqualRates: gusdRatesPair.customer_buy})
                }

                if(paxRatesPair){
                    this.setState({paxEqualRates: paxRatesPair.customer_buy})
                }

                if(usdcRatesPair){
                    this.setState({usdcEqualRates: usdcRatesPair.customer_buy})
                }

                if(tusdRatesPair){
                    this.setState({tusdEqualRates: tusdRatesPair.customer_buy})
                }

                if(usdtRatesPair) {
                    this.setState({usdtEqualRates: usdtRatesPair.customer_buy})
                }

                if(ttcRatesPair) {
                    this.setState({ttcEqualRates: ttcRatesPair.customer_buy})
                }

                if(swipeRatesPair) {
                    this.setState({swipeEqualRates: swipeRatesPair.customer_buy})
                }

                if(zilRatesPair) {
                    this.setState({zilEqualRates: zilRatesPair.customer_buy})
                }

                this.setState({
                    btcEqualRates: btcRatesPair.customer_buy,
                    ethEqualRates: ethRatesPair.customer_buy
                })
            }

            this.setState({
                rates: rates
            });

        }).catch(err => {
            
            NetworkError(err);
        })
    }

    listenBalanceUpdate(){
        
        async.series({
            getUserid: (callback) => {
                getUserAttributes(this.props.user).then(attr => {
                    let user_id = attr.sub;
        
                    this.setState({
                        userId: user_id
                    })

                    callback(null,attr);
                })
                .catch(err => {
                    let errorDetails = {
                        type: 'COMPONENTS: AppHeader | FUNC: listenBalanceUpdate',
                        url: 'react-cognito'
                    }
                    NetworkError(err, errorDetails);
                    // 
                    callback(err, null);
                });
            },
            callIot: (callback) => {

                let topic
            
                if(process.env.REACT_APP_ENV === 'demo'){
               
                    topic = `/pydt/demo/user/${this.state.userId}/accounts`;
                   
                }else if(process.env.REACT_APP_ENV === 'local' || process.env.REACT_APP_ENV === 'dev'){
                
                    topic = `/pydt/dev/user/${this.state.userId}/accounts`;
                   
                }else if(process.env.REACT_APP_ENV === 'production'){
                  
                    topic = `/pydt/prod/user/${this.state.userId}/accounts`;
                   
                }

                let iotTopic = topic
            
                iotDevice.on('connect', () => {
                    
                    iotDevice.subscribe(topic);
                });

                iotDevice.on('error', err => {
                  callback(err, null);
                });
            
                iotDevice.on('message', (recTopic, message) => {
                    
                    this.getAccountWithBalance();
                });

                callback(null);
            }
        }, (err) => {
            
        });
       
    }

    listenUserOrders(){
        
        async.series({
            getUserid: (callback) => {
                getUserAttributes(this.props.user).then(attr => {
                    let user_id = attr.sub;
        
                    this.setState({
                        userId: user_id
                    })

                    callback(null,attr);
                })
                .catch(err => {
                    let errorDetails = {
                        type: 'COMPONENTS: AppHeader | FUNC: listenUserOrders',
                        url: 'react-cognito'
                    }
                    NetworkError(err, errorDetails);
                    callback(err, null);
                });
            },
            callIot: (callback) => {

                let topic

                if(process.env.REACT_APP_ENV === 'demo'){
                  
                    topic = `/pydt/demo/user/${this.state.userId}/get_user_orders`;
                  
                }else if(process.env.REACT_APP_ENV === 'local' || process.env.REACT_APP_ENV === 'dev'){
              
                    topic = `/pydt/dev/user/${this.state.userId}/get_user_orders`;
                   
                }else if(process.env.REACT_APP_ENV === 'production'){
                   
                    topic = `/pydt/prod/user/${this.state.userId}/get_user_orders`;
                   
                }
            
                let iotTopic = topic
            
                iotDevice.on('connect', () => {
                  
                    iotDevice.subscribe(topic);
                });

                iotDevice.on('error', err => {
                 
                  callback(err, null);
                });
            
                iotDevice.on('message', (recTopic, message) => {
                  
                    this.middleFunc();
                });

                callback(null);
            }
        }, (err) => {
            
        });
       
    }

    listenSocketsServices(){

        async.series({
            callRates: (callback) => {

                let topic

                if(process.env.REACT_APP_ENV === 'demo'){
                    topic = `/pydt/demo/user/global/rates`;
                }else if(process.env.REACT_APP_ENV === 'local' || process.env.REACT_APP_ENV === 'dev'){
                    topic = `/pydt/dev/user/global/rates`;
                }else if(process.env.REACT_APP_ENV === 'production'){
                    topic = `/pydt/prod/user/global/rates`;
                }

                let iotTopic = topic
            
                iotDevice.on('connect', () => {
                  
                    iotDevice.subscribe(topic);
                });

                iotDevice.on('error', err => {
                
                    callback(err, null);
                });
            
                iotDevice.on('message', (recTopic, message) => {
                    this.getRates();
                });

                callback(null);
            },
            callCurrencyPair: (callback) => {

                let topic

                if(process.env.REACT_APP_ENV === 'demo'){
                    topic = `/pydt/demo/user/global/currency_pair`;
                }else if(process.env.REACT_APP_ENV === 'local' || process.env.REACT_APP_ENV === 'dev'){
                    topic = `/pydt/dev/user/global/currency_pair`;
                }else if(process.env.REACT_APP_ENV === 'production'){
                    topic = `/pydt/prod/user/global/currency_pair`
                }
            
                let iotTopic = topic
            
                iotDevice.on('connect', () => {
                  
                    iotDevice.subscribe(topic);
                });

                iotDevice.on('error', err => {
                
                  callback(err, null);
                });
            
                iotDevice.on('message', (recTopic, message) => {
                    this.getCurrencyPair();
                });

                callback(null);
            }
        }, (err) => {
            
        });
    }

    async componentDidMount(){
        this.callGA();
        if(this.props.user){
            this.listenBalanceUpdate();
            // this.listenUserOrders();
            this.listenSocketsServices();
            this.getRates();
            this.getAccountWithBalance();
            this.getCurrencyPair();
            this.checkCognitoAttr();
            this.initFBPixel();
            this.fetchDashboard();
            this.fetchCurrencyLimit();
            await this.fetchAllTradingVolume();
        }else{
            // this.listenUserOrders();
            this.listenSocketsServices();
            this.getRates();
            this.getCurrencyPair();
            this.fetchCurrencyLimit();
            await this.fetchAllTradingVolume();
        }
       
        window.scrollTo(0, 0);
    }

    /**
     * TCDOC-015
     * this is unmount hook from react. we need to unregister every intervals we made so will have mo memory leak
     */
    componentWillUnmount() {
	    window.intervals.map((i, index) => {
		    clearInterval(i)
		    window.intervals.splice(index, 1);
	    });
    }

    openDeposit() {
        this.setState({ depositModal: true })
        if (ReactPixel) {
            ReactPixel.track( 'ViewContent', {content_name: 'deposit'} );
            ReactPixel.track( 'Lead', {content_name: 'deposit_start'} );
        }
    }

    toggle() {
        this.setState({ dropDown: true })
    }

    onOpenWalletAdd(){
        this.setState({
            walletAdd: true
        })
    }

    onCloseWalletClose(){
        this.setState({
            walletAdd: false
        })
    }

    toggle(){
        this.setState({
            dropdownOpen: true
        })
    }

    middleFunc() {
        this.childUpdateTable();
        this.childUpdateTableOrders();
    }

    acceptMethod(childUpdateTable) {
        this.childUpdateTable = childUpdateTable;
    }

    acceptMethodOrders(childUpdateTableOrders) {
        this.childUpdateTableOrders = childUpdateTableOrders;
    }

    getMostRecentConversion = (value) => {
        this.setState({ mostRecentConversion: value })
    }
    
    getTradingVolume = async (currency) => {
        // 
        homeActions.fetchTradingView(currency+"IDR").then( res => {
            // 
            if(res.data.v){
                if (res.data.v.length > 0) {
                    this.setState({
                        [currency+'Volume']: res.data.v[res.data.v.length - 1]
                    })
                    return res.data.v[res.data.v.length - 1]
                } else {
                    return 0
                }
            } else {
                return 0
            }
          
        }).catch( err => {
            NetworkError(err);
            return 0
        })
    };

    async fetchAllTradingVolume() {
        // let BTCVolume,
        //     ETHVolume,
        //     XRPVolume,
        //     DGXVolume,
        //     TTCVolume,
        //     PAXVolume,
        //     TUSDVolume,
        //     GUSDVolume,
        //     USDTVolume,
        //     USDCVolume = 0

        // BTCVolume = await this.getTradingVolume('BTC');
        //  
        // ETHVolume = await this.getTradingVolume('ETH');
        // XRPVolume = await this.getTradingVolume('XRP');
        // DGXVolume = await this.getTradingVolume('DGX');
        // TTCVolume = await this.getTradingVolume('TTC');
        // PAXVolume = await this.getTradingVolume('PAX');
        // TUSDVolume = await this.getTradingVolume('TUSD');
        // GUSDVolume = await this.getTradingVolume('GUSD');
        // USDTVolume = await this.getTradingVolume('USDT');
        // USDCVolume = await this.getTradingVolume('USDC');
        await this.getTradingVolume('BTC'); 
        await this.getTradingVolume('ETH');
        await this.getTradingVolume('XRP');
        await this.getTradingVolume('DGX');
        await this.getTradingVolume('TTC');
        await this.getTradingVolume('PAX');
        await this.getTradingVolume('TUSD');
        await this.getTradingVolume('GUSD');
        await this.getTradingVolume('USDT');
        await this.getTradingVolume('USDC');
        await this.getTradingVolume('SWIPE');
        await this.getTradingVolume('ZIL');
    }
    render(){

        const options = [
            { value: 'BTC/IDR', label: 'BTC/IDR' },
            { value: 'ETH/IDR', label: 'ETH/IDR'},
            { value: 'XRP/IDR', label: 'XRP/IDR'},
            { value: 'DGX/IDR', label: 'DGX/IDR'},
            { value: 'GUSD/IDR', label: 'DGX/IDR'},
            { value: 'PAX/IDR', label: 'PAX/IDR'},
            { value: 'TUSD/IDR', label: 'TUSD/IDR'},
            { value: 'USDC/IDR', label: 'USDC/IDR'},
            { value: 'USDT/IDR', label: 'USDT/IDR'},
            { value: 'TTC/IDR', label: 'TTC/IDR'},
            { value: 'SWIPE/IDR', label: 'SWIPE/IDR'},
            { value: 'ZIL/IDR', label: 'ZIL/IDR'}
        ]

        const defaultOption = options[0]

        let totalPortofolio = 0;
        let cryptoTotalIDR = 0;
        let TotalIDR = 0;
        let UsableBalance = 0;
        let BTCTotalIDR = 0;
        let ETHTotalIDR = 0;
        let XRPTotalIDR = 0;
        let DGXTotalIDR = 0;
        let GUSDTotalIDR = 0;
        let PAXTotalIDR = 0;
        let USDCTotalIDR = 0;
        let TUSDTotalIDR = 0;
        let USDTTotalIDR = 0;
        let TTCTotalIDR = 0;
        let SWIPETotalIDR = 0;
        let ZILTotalIDR = 0;
        
        if(this.state.BTCAccount){
            BTCTotalIDR = this.state.BTCAccount.Balance * this.state.btcEqualRates;
        }

        if(this.state.ETHAccount){
            ETHTotalIDR = this.state.ETHAccount.Balance * this.state.ethEqualRates;
        }

        if(this.state.XRPAccount){
            XRPTotalIDR = this.state.XRPAccount.Balance * this.state.xrpEqualRates;
        }

        if(this.state.DGXAccount){
            DGXTotalIDR = this.state.DGXAccount.Balance * this.state.dgxEqualRates;
        }

        if(this.state.GUSDAccount){
            GUSDTotalIDR = this.state.GUSDAccount.Balance * this.state.gusdEqualRates;
        }
        
        if(this.state.PAXAccount){
            PAXTotalIDR = this.state.PAXAccount.Balance * this.state.paxEqualRates;
        }
        if(this.state.USDCAccount){
            USDCTotalIDR = this.state.USDCAccount.Balance * this.state.usdcEqualRates;
        }
        if(this.state.TUSDAccount){
            TUSDTotalIDR = this.state.TUSDAccount.Balance * this.state.tusdEqualRates;
        }
        if(this.state.USDTAccount){
            USDTTotalIDR = this.state.USDTAccount.Balance * this.state.usdtEqualRates;
        }
        if(this.state.TTCAccount){
            TTCTotalIDR = this.state.TTCAccount.Balance * this.state.ttcEqualRates;
        }
        if(this.state.SWIPEAccount){
            SWIPETotalIDR = this.state.SWIPEAccount.Balance * this.state.swipeEqualRates;
        }
        if(this.state.ZILAccount){
            ZILTotalIDR = this.state.ZILAccount.Balance * this.state.zilEqualRates;
        }
        

        if(this.state.IDRAccount){
            TotalIDR = this.state.IDRAccount.Balance
            UsableBalance = this.state.IDRAccount.UsableBalance
        }

        cryptoTotalIDR = BTCTotalIDR + ETHTotalIDR + XRPTotalIDR + DGXTotalIDR + GUSDTotalIDR + USDCTotalIDR + PAXTotalIDR + TUSDTotalIDR + USDTTotalIDR + TTCTotalIDR + SWIPETotalIDR + ZILTotalIDR

        totalPortofolio = TotalIDR + cryptoTotalIDR

		let classObject = {
			'm-alert m-alert--outline m-alert--square alert alert-dismissible fade show': true,
			'alert-info': true
        }
        
        // ToggleData
        let lastPrice = {};
        let monthlyChangeAmount = {};
        let monthlyChangePercentage = {};

        lastPrice["BTC"] = this.state.currencyPairData['BTCIDR'] != undefined ? this.state.currencyPairData['BTCIDR']['24H'].rates.price : '-';
        lastPrice["ETH"] = this.state.currencyPairData['ETHIDR'] != undefined ? this.state.currencyPairData['ETHIDR']['24H'].rates.price : '-';
        lastPrice["DGX"] = this.state.currencyPairData['DGXIDR'] != undefined ? this.state.currencyPairData['DGXIDR']['24H'].rates.price : '-';
        lastPrice["XRP"] = this.state.currencyPairData['XRPIDR'] != undefined ? this.state.currencyPairData['XRPIDR']['24H'].rates.price : '-';
        lastPrice["PAX"] = this.state.currencyPairData['PAXIDR'] != undefined ? this.state.currencyPairData['PAXIDR']['24H'].rates.price : '-';
        lastPrice["TTC"] = this.state.currencyPairData['TTCIDR'] != undefined ? this.state.currencyPairData['TTCIDR']['24H'].rates.price : '-';
        lastPrice["GUSD"] = this.state.currencyPairData['GUSDIDR'] != undefined ? this.state.currencyPairData['GUSDIDR']['24H'].rates.price : '-';
        lastPrice["USDT"] = this.state.currencyPairData['USDTIDR'] != undefined ? this.state.currencyPairData['USDTIDR']['24H'].rates.price : '-';
        lastPrice["TUSD"] = this.state.currencyPairData['TUSDIDR'] != undefined ? this.state.currencyPairData['TUSDIDR']['24H'].rates.price : '-';
        lastPrice["USDC"] = this.state.currencyPairData['USDCIDR'] != undefined ? this.state.currencyPairData['USDCIDR']['24H'].rates.price : '-';
        lastPrice["SWIPE"] = this.state.currencyPairData['SWIPEIDR'] != undefined ? this.state.currencyPairData['SWIPEIDR']['24H'].rates.price : '-';
        lastPrice["ZIL"] = this.state.currencyPairData['ZILIDR'] != undefined ? this.state.currencyPairData['ZILIDR']['24H'].rates.price : '-';
        
        monthlyChangeAmount['BTC'] = this.state.currencyPairData['BTCIDR'] != undefined ? this.state.currencyPairData['BTCIDR']['24H'].rates['last24hour'] : '-';
        monthlyChangeAmount['ETH'] = this.state.currencyPairData['ETHIDR'] != undefined ? this.state.currencyPairData['ETHIDR']['24H'].rates['last24hour'] : '-';
        monthlyChangeAmount['DGX'] = this.state.currencyPairData['DGXIDR'] != undefined ? this.state.currencyPairData['DGXIDR']['24H'].rates['last24hour'] : '-';
        monthlyChangeAmount['XRP'] = this.state.currencyPairData['XRPIDR'] != undefined ? this.state.currencyPairData['XRPIDR']['24H'].rates['last24hour'] : '-';
        monthlyChangeAmount['PAX'] = this.state.currencyPairData['PAXIDR'] != undefined ? this.state.currencyPairData['PAXIDR']['24H'].rates['last24hour'] : '-';
        monthlyChangeAmount['TTC'] = this.state.currencyPairData['TTCIDR'] != undefined ? this.state.currencyPairData['TTCIDR']['24H'].rates['last24hour'] : '-';
        monthlyChangeAmount['GUSD'] = this.state.currencyPairData['GUSDIDR'] != undefined ? this.state.currencyPairData['GUSDIDR']['24H'].rates['last24hour'] : '-';
        monthlyChangeAmount['USDT'] = this.state.currencyPairData['USDTIDR'] != undefined ? this.state.currencyPairData['USDTIDR']['24H'].rates['last24hour'] : '-';
        monthlyChangeAmount['TUSD'] = this.state.currencyPairData['TUSDIDR'] != undefined ? this.state.currencyPairData['TUSDIDR']['24H'].rates['last24hour'] : '-';
        monthlyChangeAmount['USDC'] = this.state.currencyPairData['USDCIDR'] != undefined ? this.state.currencyPairData['USDCIDR']['24H'].rates['last24hour'] : '-';
        monthlyChangeAmount['SWIPE'] = this.state.currencyPairData['SWIPEIDR'] != undefined ? this.state.currencyPairData['SWIPEIDR']['24H'].rates['last24hour'] : '-';
        monthlyChangeAmount['ZIL'] = this.state.currencyPairData['ZILIDR'] != undefined ? this.state.currencyPairData['ZILIDR']['24H'].rates['last24hour'] : '-';
        
        monthlyChangePercentage['BTC'] = (( monthlyChangeAmount['BTC'] != '-' ) && ( lastPrice['BTC'] != '-' )) ? (( monthlyChangeAmount['BTC'] / lastPrice['BTC'] ) * 100 ).toFixed( 2 ) : '-';
        monthlyChangePercentage['ETH'] = (( monthlyChangeAmount['ETH'] != '-' ) && ( lastPrice['ETH'] != '-' )) ? (( monthlyChangeAmount['ETH'] / lastPrice['ETH'] ) * 100 ).toFixed( 2 ) : '-';
        monthlyChangePercentage['XRP'] = (( monthlyChangeAmount['XRP'] != '-' ) && ( lastPrice['XRP'] != '-' )) ? (( monthlyChangeAmount['XRP'] / lastPrice['XRP'] ) * 100 ).toFixed( 2 ) : '-';
        monthlyChangePercentage['DGX'] = (( monthlyChangeAmount['DGX'] != '-' ) && ( lastPrice['DGX'] != '-' )) ? (( monthlyChangeAmount['DGX'] / lastPrice['DGX'] ) * 100 ).toFixed( 2 ) : '-';
        monthlyChangePercentage['TTC'] = (( monthlyChangeAmount['TTC'] != '-' ) && ( lastPrice['TTC'] != '-' )) ? (( monthlyChangeAmount['TTC'] / lastPrice['TTC'] ) * 100 ).toFixed( 2 ) : '-';
        monthlyChangePercentage['PAX'] = (( monthlyChangeAmount['PAX'] != '-' ) && ( lastPrice['PAX'] != '-' )) ? (( monthlyChangeAmount['PAX'] / lastPrice['PAX'] ) * 100 ).toFixed( 2 ) : '-';
        monthlyChangePercentage['USDT'] = (( monthlyChangeAmount['USDT'] != '-' ) && ( lastPrice['USDT'] != '-' )) ? (( monthlyChangeAmount['USDT'] / lastPrice['USDT'] ) * 100 ).toFixed( 2 ) : '-';
        monthlyChangePercentage['USDC'] = (( monthlyChangeAmount['USDC'] != '-' ) && ( lastPrice['USDC'] != '-' )) ? (( monthlyChangeAmount['USDC'] / lastPrice['USDC'] ) * 100 ).toFixed( 2 ) : '-';
        monthlyChangePercentage['GUSD'] = (( monthlyChangeAmount['GUSD'] != '-' ) && ( lastPrice['GUSD'] != '-' )) ? (( monthlyChangeAmount['GUSD'] / lastPrice['GUSD'] ) * 100 ).toFixed( 2 ) : '-';
        monthlyChangePercentage['TUSD'] = (( monthlyChangeAmount['TUSD'] != '-' ) && ( lastPrice['TUSD'] != '-' )) ? (( monthlyChangeAmount['TUSD'] / lastPrice['TUSD'] ) * 100 ).toFixed( 2 ) : '-';
        monthlyChangePercentage['SWIPE'] = (( monthlyChangeAmount['SWIPE'] != '-' ) && ( lastPrice['SWIPE'] != '-' )) ? (( monthlyChangeAmount['SWIPE'] / lastPrice['SWIPE'] ) * 100 ).toFixed( 2 ) : '-';
        monthlyChangePercentage['ZIL'] = (( monthlyChangeAmount['ZIL'] != '-' ) && ( lastPrice['ZIL'] != '-' )) ? (( monthlyChangeAmount['ZIL'] / lastPrice['ZIL'] ) * 100 ).toFixed( 2 ) : '-';
        
        return (
            <div className="m-grid m-grid--hor m-grid--root m-page">
		        <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body" style={{ backgroundColor: "#f4f7fa"}}>
			    <div className="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
				<div className="m-grid__item m-grid__item--fluid m-wrapper">

                    <KYCMessage status={this.props.kyc.status}/>
					<NotificationMessage />

                    {/* <div onClick={() => this.openContest()} className={classnames(classObject)} role="alert" style={{ marginTop: "20px" }}>
						<div style={{display: 'flex', flexDirection: 'row', justifyContent:'center'}}>
							<a href="#">Tokocrypto Trading Competition</a>
						</div>
					</div> */}

					<div className="m-content">
                        <div style={{marginBottom:"0px"}} className={classnames({"m-portlet": true,"user-disable": this.props.user == null})}>
                            {!this.props.user?<div className="user-unblock-content"><a href='/login'>Please Login or Register Unblock content</a></div>:null}
                            <div className={classnames({"col-md-12 dashboard-top-coin-container": true,"user-disable-opacity": this.props.user == null})}>
                                <div className={classnames({"col-md-4 dashboard-top-coin-profit-container": true})}>
                                    <div className="col-md-6">
                                        <div className="dashboard-top-coin-profit-header-text" style={{color:"#666666"}}>{strings.TOTAL_PORTFOLIO_VALUE}</div>
                                        {/* <div className="dashboard-top-header-area">{strings.ESTIMATED_VALUE}</div> */}
                                    </div>
                                    <div className="col-md-6">
                                        <div className="m--font-black dashboard-top-header-profit"> { currencyFormatter( amountFormatter(totalPortofolio) ) }</div>
                                        {/* <div className="dashboard-top-header-area">Rp. 135,984,736</div> */}
                                    </div>
                                </div>
                                <div className="col-md-4 dashboard-top-coin-idr-container" style={{display:"inline-flex"}}>
                                    <div className="col-md-6">
                                        <img className="custom-img-dashboard" src="../../assets/media/img/logo/indonesia.png" alt="" />
                                        <div className="dashboard-top-coin-profit-header-text" style={{color:"#666666"}}> { currencyFormatter( strings.BALANCE ) }</div>
                                        <div className="dashboard-top-header-area">{strings.DAILY_WITHDRAWAL_LIMIT}</div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="m--font-black dashboard-top-header-profit">{ currencyFormatter( amountFormatter(TotalIDR) ) }</div>
                                        <div className="dashboard-top-header-area">{ currencyFormatter( amountFormatter(this.state.IDRUsageLeft) ) } </div>
                                    </div>
                                </div>
                                <div className="col-md-4 top-header-wallet-button-set">
                                    <div className="row" style={{width:"100%"}}>
                                        <div className="col-md-12 top-header-wallet-button-container">
                                            <button disabled={this.state.balanceLoading} onClick={() => this.openDeposit()} className="btn btn-outline-blue" type="button" style={{marginRight:"30px !important"}}>
                                                <FontAwesomeIcon icon={faSignInAlt} /> &nbsp;Deposit
                                            </button>

                                            <button disabled={this.state.balanceLoading || this.state.IDRUsageLeft == 0} onClick={() => this.setState({ withdrawalModal: true })} className="btn btn-outline-blue" type="button">
                                                <FontAwesomeIcon icon={faSignOutAlt} /> &nbsp;{strings.WITHDRAWAL}
                                            </button>
                                            
                                            {/* <FontAwesomeIcon icon={faPlusCircle} onClick={() => this.onOpenWalletAdd()} style={{fontSize:"25px",color:"#007ea4"}}/> */}
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>

                            </div>
						    <div className="">
                                <div className={classnames({"m-portlet__body dashboard-top-header-wallets": true,"user-disable-opacity": this.props.user == null})}>
                                    <div className="row">
                                    
                                        <WalletDashboard 
                                            data={this.state} 
                                            send={this.send}    
                                            receive={this.receive}
                                        />
                                    
                                    <div 
                                        className="col-md-1 wallet-add-button-container" 
                                        style={{
                                            margin:"auto", 
                                            display:"flex", 
                                            justifyContent: "center",
                                            maxWidth: "5%", flex: "0 0 5%"
                                        }}>
                                        <div 
                                            className="col-md-12 col-lg-6 col-xl-12" 
                                            style={{
                                                padding: 0
                                            }}>
                                            <div 
                                                className="btn-addFavorite-Dashboard-container"
                                                onClick={() => this.onOpenWalletAdd()} >
                                                    <span className="btn-addFavorite-Dashboard" > &#43; </span>
                                            </div>
                                            {/* <FontAwesomeIcon className="btn-addFavorite-Dashboard" icon={faPlusCircle} onClick={() => this.onOpenWalletAdd()}/> */}
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
						</div>

						<div className="row">
							<div className="col-xl-12" style={{ marginBottom: "-30px"}}>
								<div className="m-subheader custom-header-padding">
									<div className="d-flex align-items-center">

									</div>
								</div>

								<div className="m-portlet">
									<div>
										{/* <div className="m-portlet__head dashboard-trading-desk-tabs">
											<div className="m-portlet__head-tools col-md12">
												<ul className="nav nav-tabs m-tabs m-tabs-line m-tabs-line--left m-tabs-line--primary width100" role="tablist">
													<li className="nav-item m-tabs__item col-md-2 nomargins" onClick={() => this.changeActiveTab('BTC')}>
														<a className={classnames({ 'nav-link m-tabs__link dashboard-trading-desk-tabs-links': true, 'active-dashboard-crypto-tabs-btc': this.state.activeTab === 'BTC'})} data-toggle="tab" href="#m_user_profile_tab_1"
														    role="tab">
															<i className="cc BTC" style={{ marginRight: "10px", fontSize: "25px", marginTop: "-5px", color: "#F7931A" }}></i>
															Bitcoin
														</a>
													</li>
													<li className="nav-item m-tabs__item col-md-2 nomargins" onClick={() => this.changeActiveTab('ETH')}>
														<a className={classnames({ 'nav-link m-tabs__link dashboard-trading-desk-tabs-links': true, 'active-dashboard-crypto-tabs-eth': this.state.activeTab === 'ETH'})} data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                                            <i className="cc ETH-alt" style={{ marginRight: "10px", fontSize: "25px", marginTop: "-5px", color: "#575962" }}></i>
                                                            Ethereum
														</a>
													</li>
                                                    <li className="nav-item m-tabs__item col-md-2 nomargins" onClick={() => this.changeActiveTab('XRP')}>
														<a className={classnames({ 'nav-link m-tabs__link dashboard-trading-desk-tabs-links': true, 'active-dashboard-crypto-tabs-xrp': this.state.activeTab === 'XRP'})} data-toggle="tab" href="#m_user_profile_tab_3" role="tab">
                                                            <i className="cc XRP-alt" style={{ marginRight: "10px", fontSize: "25px", marginTop: "-5px", color:"#0099d2" }}></i>
                                                            Ripple
														</a>
													</li>
                                                    <li className="nav-item m-tabs__item col-md-2 nomargins" onClick={() => this.changeActiveTab('DGX')}>
														<a className={classnames({ 'nav-link m-tabs__link dashboard-trading-desk-tabs-links': true, 'active-dashboard-crypto-tabs-xrp': this.state.activeTab === 'DGX'})} data-toggle="tab" href="#m_user_profile_tab_4" role="tab">
                                                            <img className="custom-coin-image-dashboard-middle" src="assets/media/img/logo/dgx_token.png" alt="" />
                                                            DGX
														</a>
													</li>
                                                    <li className="nav-item m-tabs__item col-md-2 nomargins" onClick={() => this.changeActiveTab('TTC')}>
														<a className={classnames({ 'nav-link m-tabs__link dashboard-trading-desk-tabs-links': true, 'active-dashboard-crypto-tabs-xrp': this.state.activeTab === 'TTC'})} data-toggle="tab" href="#m_user_profile_tab_5" role="tab">
                                                            <img className="custom-coin-image-dashboard-middle" src="assets/media/img/logo/ttc.png" alt="" />
                                                            TTC
														</a>
													</li>
												</ul>
											</div>
                                        </div> */}
               
									</div>
                                    <div className="trading-desk-container">
                                        <div className="col-md-3 col-sm-12">
                                            <div className="row">
                                                <div className="trading-desk-inner-container">
                                                    <div className="col-md-12">
                                                        <UncontrolledDropdown>
                                                            <DropdownToggle caret style={{width:"100%",border:"1px solid #B8B8B8",borderRadius:"8px",marginTop:"10px",marginBottom:"15px"}}>
                                                                {this.state.activeTab == "BTC"?
                                                                    <div style={{float:"left",fontWeight:"600",fontSize:"16px"}}>
                                                                        <i className="cc BTC" style={{ marginRight: "10px", fontSize: "25px", marginTop: "-5px", color: "#F7931A" }}></i>BTC/IDR
                                                                    </div>:null
                                                                }
                                                                {this.state.activeTab == "ETH"?
                                                                    <div style={{float:"left",fontWeight:"600",fontSize:"16px"}}>
                                                                        <i className="cc ETH-alt" style={{ marginRight: "10px", fontSize: "25px", marginTop: "-5px", color: "#575962" }}></i>ETH/IDR
                                                                    </div>:null
                                                                }
                                                                {this.state.activeTab == "XRP"?
                                                                    <div style={{float:"left",fontWeight:"600",fontSize:"16px"}}>
                                                                        <i className="cc XRP-alt" style={{ marginRight: "10px", fontSize: "25px", marginTop: "-5px", color:"#0099d2" }}></i>XRP/IDR
                                                                    </div>:null
                                                                }
                                                                {this.state.activeTab == "DGX"?
                                                                    <div style={{float:"left",fontWeight:"600",fontSize:"16px"}}>
                                                                        <img className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/dgx_token.png" alt="" />DGX/IDR
                                                                    </div>:null
                                                                }
                                                                {this.state.activeTab == "GUSD"?
                                                                    <div style={{float:"left",fontWeight:"600",fontSize:"16px"}}>
                                                                        <img className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/gemini.png" alt="" />GUSD/IDR
                                                                    </div>:null
                                                                }
                                                                {this.state.activeTab == "PAX"?
                                                                    <div style={{float:"left",fontWeight:"600",fontSize:"16px"}}>
                                                                        <img className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/pax.png" alt="" />PAX/IDR
                                                                    </div>:null
                                                                }
                                                                {this.state.activeTab == "TUSD"?
                                                                    <div style={{float:"left",fontWeight:"600",fontSize:"16px"}}>
                                                                        <img className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/tusd.png" alt="" />TUSD/IDR
                                                                    </div>:null
                                                                }

                                                                {this.state.activeTab == "USDC"?
                                                                    <div style={{float:"left",fontWeight:"600",fontSize:"16px"}}>
                                                                        <img className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/usdc.png" alt="" />USDC/IDR
                                                                    </div>:null
                                                                }

                                                                {this.state.activeTab == "USDT"?
                                                                    <div style={{float:"left",fontWeight:"600",fontSize:"16px"}}>
                                                                        <img className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/usdt.png" alt="" />USDT/IDR
                                                                    </div>:null
                                                                }

                                                                {this.state.activeTab == "TTC"?
                                                                    <div style={{float:"left",fontWeight:"600",fontSize:"16px"}}>
                                                                        <img className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/ttc.png" alt="" />TTC/IDR
                                                                    </div>:null
                                                                }

                                                                {this.state.activeTab == "SWIPE"?
                                                                    <div style={{float:"left",fontWeight:"600",fontSize:"16px"}}>
                                                                        <img className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/swipe.png" alt="" />SWIPE/IDR
                                                                    </div>:null
                                                                }

                                                                {this.state.activeTab == "ZIL"?
                                                                    <div style={{float:"left",fontWeight:"600",fontSize:"16px"}}>
                                                                        <img className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/zil.png" alt="" />ZIL/IDR
                                                                    </div>:null
                                                                }
                                                                

                                                            </DropdownToggle>
                                                            <DropdownMenu style={{width:"160%",padding:"0"}}>
                                                                <table className="table referral-details-table example-table">
                                                                    <DropdownItem>
                                                                        <thead style={{background:"#F7F7F7", color:"#ffffff"}}>
                                                                            <tr className="referral-details-table-header">
                                                                                <th style={{color:"#636161",fontSize:"13px",fontWeight:"300"}}>Pair</th>
                                                                                <th style={{color:"#636161",fontSize:"13px",fontWeight:"300"}}>Last Price</th>
                                                                                <th style={{color:"#636161",fontSize:"13px",fontWeight:"300"}}>24h Change</th>
                                                                                <th style={{color:"#636161",fontSize:"13px",fontWeight:"300"}}>24h Volume</th>
                                                                            </tr>
                                                                        </thead>
                                                                  
                                                                        {/* BTC */}
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('BTC')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>BTC/IDR</td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['BTC'] > 0 ? <span style={{color:"#1AA74A"}}>  {amountFormatter(lastPrice['BTC'], 'btc')}  </span> : <span style={{color:"#c67900"}}> {amountFormatter(lastPrice['BTC'], 'btc')} </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['BTC'] > 0 ? 
                                                                                    <span style={{color:"#1AA74A"}}>  { monthlyChangePercentage['BTC'] } %   </span> : 
                                                                                    <span style={{color:"#c67900"}}>  { monthlyChangePercentage['BTC'] } %  </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>{ currencyFormatter( amountFormatter( this.state.BTCVolume, 'btc' )) }</td>
                                                                        </tr>

                                                                        {/* ETH */}
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('ETH')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>ETH/IDR</td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['ETH'] > 0 ? <span style={{color:"#1AA74A"}}>  { amountFormatter(lastPrice['ETH'], 'eth') }  </span> : <span style={{color:"#c67900"}}> { amountFormatter(lastPrice['ETH'], 'eth') } </span> }  
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['ETH'] > 0 ? 
                                                                                    <span style={{color:"#1AA74A"}}>  { monthlyChangePercentage['ETH'] } %   </span> : 
                                                                                    <span style={{color:"#c67900"}}>  { monthlyChangePercentage['ETH'] } %  </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>{ currencyFormatter( amountFormatter( this.state.ETHVolume, 'eth' )) }</td>
                                                                        </tr>

                                                                        {/* XRP */}
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('XRP')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>XRP/IDR</td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['XRP'] > 0 ? <span style={{color:"#1AA74A"}}>  { amountFormatter(lastPrice['XRP'], 'xrp') }  </span> : <span style={{color:"#c67900"}}> { amountFormatter(lastPrice['XRP'], 'xrp') } </span> }  
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['XRP'] > 0 ? 
                                                                                    <span style={{color:"#1AA74A"}}>  { monthlyChangePercentage['XRP'] } %   </span> : 
                                                                                    <span style={{color:"#c67900"}}>  { monthlyChangePercentage['XRP'] } %  </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>{ currencyFormatter( amountFormatter( this.state.XRPVolume, 'xrp' )) }</td>
                                                                        </tr>

                                                                        {/* DGX */}
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('DGX')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>DGX/IDR</td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['DGX'] > 0 ? <span style={{color:"#1AA74A"}}>  { amountFormatter(lastPrice['DGX'], 'dgx') }   </span> : <span style={{color:"#c67900"}}> { amountFormatter(lastPrice['DGX'], 'dgx') }  </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['DGX'] > 0 ? 
                                                                                    <span style={{color:"#1AA74A"}}>  { monthlyChangePercentage['DGX'] } %   </span> : 
                                                                                    <span style={{color:"#c67900"}}>  { monthlyChangePercentage['DGX'] } %  </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>{ currencyFormatter( amountFormatter( this.state.DGXVolume, 'dgx' )) }</td>
                                                                        </tr>

                                                                        {/* PAX */}
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('PAX')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>PAX/IDR</td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['PAX'] > 0 ? <span style={{color:"#1AA74A"}}>  { amountFormatter(lastPrice['PAX'], 'pax') }  </span> : <span style={{color:"#c67900"}}> { amountFormatter(lastPrice['PAX'], 'pax') } </span> }  
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['PAX'] > 0 ? 
                                                                                    <span style={{color:"#1AA74A"}}>  { monthlyChangePercentage['PAX'] } %   </span> : 
                                                                                    <span style={{color:"#c67900"}}>  { monthlyChangePercentage['PAX'] } %  </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>{ currencyFormatter( amountFormatter( this.state.PAXVolume, 'pax' )) }</td>
                                                                        </tr>

                                                                        {/* TTC */}
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('TTC')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>TTC/IDR</td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['TTC'] > 0 ? <span style={{color:"#1AA74A"}}>  { amountFormatter(lastPrice['TTC'], 'ttc') }  </span> : <span style={{color:"#c67900"}}> { amountFormatter(lastPrice['TTC'], 'ttc') } </span> }  
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['TTC'] > 0 ? 
                                                                                    <span style={{color:"#1AA74A"}}>  { monthlyChangePercentage['TTC'] } %   </span> : 
                                                                                    <span style={{color:"#c67900"}}>  { monthlyChangePercentage['TTC'] } %  </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>{ currencyFormatter( amountFormatter( this.state.TTCVolume, 'ttc' )) }</td>
                                                                        </tr>

                                                                        {/* USDT */}
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('USDT')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>USDT/IDR</td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['USDT'] > 0 ? <span style={{color:"#1AA74A"}}>  { amountFormatter(lastPrice['USDT'], 'usdt') }  </span> : <span style={{color:"#c67900"}}> { amountFormatter(lastPrice['USDT'], 'usdt') } </span> }  
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['USDT'] > 0 ? 
                                                                                    <span style={{color:"#1AA74A"}}>  { monthlyChangePercentage['USDT'] } %   </span> : 
                                                                                    <span style={{color:"#c67900"}}>  { monthlyChangePercentage['USDT'] } %  </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>{ currencyFormatter( amountFormatter( this.state.USDTVolume, 'usdt' )) }</td>
                                                                        </tr>

                                                                        {/* USDC */}
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('USDC')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>USDC/IDR</td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['USDC'] > 0 ? <span style={{color:"#1AA74A"}}>  { amountFormatter(lastPrice['USDC'], 'usdc') }  </span> : <span style={{color:"#c67900"}}> { amountFormatter(lastPrice['USDC'], 'usdc') } </span> }  
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['USDC'] > 0 ? 
                                                                                    <span style={{color:"#1AA74A"}}>  { monthlyChangePercentage['USDC'] } %   </span> : 
                                                                                    <span style={{color:"#c67900"}}>  { monthlyChangePercentage['USDC'] } %  </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>{ currencyFormatter( amountFormatter( this.state.USDCVolume, 'usdc' )) }</td>
                                                                        </tr>

                                                                        {/* TUSD */}
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('TUSD')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>TUSD/IDR</td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['TUSD'] > 0 ? <span style={{color:"#1AA74A"}}>  { amountFormatter(lastPrice['TUSD'], 'tusd') }  </span> : <span style={{color:"#c67900"}}> { amountFormatter(lastPrice['TUSD'], 'tusd') } </span> }  
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['TUSD'] > 0 ? 
                                                                                    <span style={{color:"#1AA74A"}}>  { monthlyChangePercentage['TUSD'] } %   </span> : 
                                                                                    <span style={{color:"#c67900"}}>  { monthlyChangePercentage['TUSD'] } %  </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>{ currencyFormatter( amountFormatter( this.state.TUSDVolume, 'tusd' )) }</td>
                                                                        </tr>

                                                                        {/* SWIPE */}
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('SWIPE')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>SWIPE/IDR</td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['SWIPE'] > 0 ? <span style={{color:"#1AA74A"}}>  { amountFormatter(lastPrice['SWIPE'], 'swipe') }  </span> : <span style={{color:"#c67900"}}> { amountFormatter(lastPrice['SWIPE'], 'swipe') } </span> }  
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['SWIPE'] > 0 ? 
                                                                                    <span style={{color:"#1AA74A"}}>  { monthlyChangePercentage['SWIPE'] } %   </span> : 
                                                                                    <span style={{color:"#c67900"}}>  { monthlyChangePercentage['SWIPE'] } %  </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>{ currencyFormatter( amountFormatter( this.state.SWIPEVolume, 'swipe' )) }</td>
                                                                        </tr>

                                                                        {/* ZILIQA */}
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('ZIL')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>ZIL/IDR</td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['ZIL'] > 0 ? <span style={{color:"#1AA74A"}}>  { amountFormatter(lastPrice['ZIL'], 'zil') }  </span> : <span style={{color:"#c67900"}}> { amountFormatter(lastPrice['ZIL'], 'zil') } </span> }  
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['ZIL'] > 0 ? 
                                                                                    <span style={{color:"#1AA74A"}}>  { monthlyChangePercentage['ZIL'] } %   </span> : 
                                                                                    <span style={{color:"#c67900"}}>  { monthlyChangePercentage['ZIL'] } %  </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>{ currencyFormatter( amountFormatter( this.state.ZILVolume, 'zil' )) }</td>
                                                                        </tr>

                                                                        {/* GUSD */}
                                                                        {/* <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('GUSD')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>GUSD/IDR</td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['GUSD'] > 0 ? <span style={{color:"#1AA74A"}}>  { amountFormatter(lastPrice['GUSD']) }  </span> : <span style={{color:"#c67900"}}> { amountFormatter(lastPrice['GUSD']) } </span> }  
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>
                                                                                { monthlyChangePercentage['GUSD'] > 0 ? 
                                                                                    <span style={{color:"#1AA74A"}}>  { monthlyChangePercentage['GUSD'] } %   </span> : 
                                                                                    <span style={{color:"#c67900"}}>  { monthlyChangePercentage['GUSD'] } %  </span> } 
                                                                            </td>
                                                                            <td style={{fontSize:"14px"}}>{ currencyFormatter( amountFormatter( this.state.GUSDVolume )) }</td>
                                                                        </tr> */}
                                                                        

                                                                        {/* <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('ETH')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>ETH/IDR</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}> 1 </span>/3432345.32</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>1.38%</span></td>
                                                                            <td style={{fontSize:"14px"}}>345,523,652</td>
                                                                        </tr>
                                                                   
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('XRP')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>XRP/IDR</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>0.0232123</span>/3523235.32</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>9.79%</span></td>
                                                                            <td style={{fontSize:"14px"}}>653,523,652</td>
                                                                        </tr>
                                                                   
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('DGX')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>DGX/IDR</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>0.355032</span>/5432444.23</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>9.79%</span></td>
                                                                            <td style={{fontSize:"14px"}}>754,523,652</td>
                                                                        </tr>
                                                                  
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('GUSD')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>GUSD/IDR</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>0.435032</span>/4532444.23</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>1.79%</span></td>
                                                                            <td style={{fontSize:"14px"}}>654,523,652</td>
                                                                        </tr>
                                                                  
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('PAX')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>PAX/IDR</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>0.855032</span>/6532444.23</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>5.79%</span></td>
                                                                            <td style={{fontSize:"14px"}}>454,523,652</td>
                                                                        </tr>
                                                                    
                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('TUSD')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>TUSD/IDR</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>0.456325</span>/7432444.23</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>3.79%</span></td>
                                                                            <td style={{fontSize:"14px"}}>354,523,652</td>
                                                                        </tr>

                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('USDC')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>USDC/IDR</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>0.456325</span>/7432444.23</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>3.79%</span></td>
                                                                            <td style={{fontSize:"14px"}}>354,523,652</td>
                                                                        </tr>

                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('USDT')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>USDT/IDR</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>0.456325</span>/7432444.23</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>3.79%</span></td>
                                                                            <td style={{fontSize:"14px"}}>354,523,652</td>
                                                                        </tr>

                                                                        <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.changeActiveTab('TTC')}>
                                                                            <td style={{fontSize:"14px",fontWeight:"600"}}>TTC/IDR</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>0.456325</span>/7432444.23</td>
                                                                            <td style={{fontSize:"14px"}}><span style={{color:"#1AA74A"}}>3.79%</span></td>
                                                                            <td style={{fontSize:"14px"}}>354,523,652</td>
                                                                        </tr> */}
                                                                    </DropdownItem>
                                                                </table>
                                                            </DropdownMenu>
                                                        </UncontrolledDropdown>
                                                    </div>
                                       
                                                    { this.state.activeTab === 'BTC' && <RatesDashboard rateHistory={this.state.rateHistory} currencyPairData={this.state.currencyPairData} rates={this.state.rates} currency="BTC" currencyName="Bitcoin"/> }
                                                    { this.state.activeTab === 'ETH' && <RatesDashboard rateHistory={this.state.rateHistory} currencyPairData={this.state.currencyPairData} rates={this.state.rates} currency="ETH" currencyName="Ethereum"/> }
                                                    { this.state.activeTab === 'XRP' && <RatesDashboard rateHistory={this.state.rateHistory} currencyPairData={this.state.currencyPairData} rates={this.state.rates} currency="XRP" currencyName="Ripple"/> }
                                                    { this.state.activeTab === 'DGX' && <RatesDashboard rateHistory={this.state.rateHistory} currencyPairData={this.state.currencyPairData} rates={this.state.rates} currency="DGX" currencyName="DGX"/> }
                                                    { this.state.activeTab === 'GUSD' && <RatesDashboard rateHistory={this.state.rateHistory} currencyPairData={this.state.currencyPairData} rates={this.state.rates} currency="GUSD" currencyName="GUSD"/> }
                                                    { this.state.activeTab === 'TUSD' && <RatesDashboard rateHistory={this.state.rateHistory} currencyPairData={this.state.currencyPairData} rates={this.state.rates} currency="TUSD" currencyName="TUSD"/> }
                                                    { this.state.activeTab === 'PAX' && <RatesDashboard rateHistory={this.state.rateHistory} currencyPairData={this.state.currencyPairData} rates={this.state.rates} currency="PAX" currencyName="PAX"/> }
                                                    { this.state.activeTab === 'USDC' && <RatesDashboard rateHistory={this.state.rateHistory} currencyPairData={this.state.currencyPairData} rates={this.state.rates} currency="USDC" currencyName="USDC"/> }
                                                    { this.state.activeTab === 'USDT' && <RatesDashboard rateHistory={this.state.rateHistory} currencyPairData={this.state.currencyPairData} rates={this.state.rates} currency="USDT" currencyName="USDT"/> }
                                                    { this.state.activeTab === 'TTC' && <RatesDashboard rateHistory={this.state.rateHistory} currencyPairData={this.state.currencyPairData} rates={this.state.rates} currency="TTC" currencyName="TTC"/> }
                                                    { this.state.activeTab === 'SWIPE' && <RatesDashboard rateHistory={this.state.rateHistory} currencyPairData={this.state.currencyPairData} rates={this.state.rates} currency="SWIPE" currencyName="SWIPE"/> }
                                                    { this.state.activeTab === 'ZIL' && <RatesDashboard rateHistory={this.state.rateHistory} currencyPairData={this.state.currencyPairData} rates={this.state.rates} currency="ZIL" currencyName="ZIL"/> }
                                                    
                                                    {/* <TVChartContainer language = {this.props.language}/> */}
                                                    {/* <RecentTrades/> */}
                                                    <OrderNow 
                                                        orderSubmitted={this.middleFunc.bind(this)} 
                                                        currency={this.state.activeTab} 
                                                        user_id={this.state.userId}
                                                        rates={this.state.rates}
                                                        IDRAccount={this.state.IDRAccount} 
                                                        BTCAccount={this.state.BTCAccount}
                                                        ETHAccount={this.state.ETHAccount} 
                                                        XRPAccount={this.state.XRPAccount}
                                                        DGXAccount={this.state.DGXAccount}
                                                        PAXAccount={this.state.PAXAccount}
                                                        TTCAccount={this.state.TTCAccount}
                                                        USDCAccount={this.state.USDCAccount}
                                                        USDTAccount={this.state.USDTAccount}
                                                        TUSDAccount={this.state.TUSDAccount}
                                                        SWIPEAccount={this.state.SWIPEAccount}
                                                        ZILAccount={this.state.ZILAccount}/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-9 col-sm-12" style={{marginTop:"10px"}}>
                                            <div className="row">
                                                <Chart
                                                    rateHistory={this.state.rateHistory}
                                                    currencyPairData={this.state.currencyPairData}
                                                    rates={this.state.rates} 
                                                    currency={this.state.activeTab}
                                                    currencyPair={this.state.activeTab+"IDR"}
                                                    mostRecentConversion={ this.state.mostRecentConversion }
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="trading-desk-container">
                                        <div className="col-md-9 col-sm-12">
                                            <div className="row">
                                                <div className="trading-desk-inner-container">
                                                    <OrderBook 
                                                        orderSubmited={this.acceptMethod.bind(this)} 
                                                        currency={this.state.activeTab} 
                                                        rateHistory={this.state.rateHistory} 
                                                        rates={this.state.rates}
                                                        />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-3 col-sm-12" style={{paddingLeft:"0px"}}>
                                            <div className="row">
                                                <div className="trading-desk-inner-container">
                                                    <RecentTrades 
                                                        currency={this.state.activeTab}
                                                        getMostRecentConversion={ this.getMostRecentConversion.bind(this) }
                                                        />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="trading-desk-container">
                                        <div className="col-md-12 col-sm-12">
                                            <div className="row">
                                                {this.props.user?
                                                    <div className="trading-desk-inner-container">
                                                        <Orders 
                                                        currency={this.state.activeTab}
                                                        orderSubmited={this.acceptMethodOrders.bind(this)}/>
                                                    </div>
                                                :null}
                                            </div>
                                        </div>
                                    </div>
                                </div>

							</div>
						</div>
                    </div>
				</div>
			</div>
		</div>
                {this.props.user?
                    <span>
                    <RecieveModal
                        show={this.state.receiveModal}
                        onClose={() => this.onCloseRecieveModal()}
                        currency={this.state.activeCurrency}
                        address={this.state.receiveAddress}
                        user={this.props.user}
                        loading={this.state.loading}
                    />

                    <SendModal
                        show={this.state.sendModal}
                        onClose={() => this.onCloseSendModal()}
                        currency={this.state.activeCurrency}
                        addresses={this.state.addresses}
                        account={this.state.activeAccount}
                        onNewAddressCreated={() => this.fetchAddresses()}
                        onRefreshBalances={() => this.fetchDashboard()}
                    />

                    <DepositModal
                        show={this.state.depositModal}
                        toggleShow={() => this.setState({ depositModal: !this.state.depositModal})}
                    />

                    <WithdrawalModal
                        accountBalanceIdr = {TotalIDR}
                        availableBalanceIdr = {UsableBalance}
                        idrUsageDayPrecentage = {this.state.IDRUsage}
                        idrUsageDayText = {this.state.IDRUsageText}
                        idrDailyLimit = {this.state.IDRLimit}
                        currency={this.state.activeCurrency}
                        show={this.state.withdrawalModal}
                        toggleShow={() => this.setState({ withdrawalModal: !this.state.withdrawalModal})}
                        onRefreshBalances={() => this.fetchDashboard()}
                    />

                    <WalletAdd
                        show={this.state.walletAdd}
                        onClose={() => this.onCloseWalletClose()}
                        BTCTotalIDR = {BTCTotalIDR}
                        ETHTotalIDR = {ETHTotalIDR}
                        XRPTotalIDR = {XRPTotalIDR}
                        DGXTotalIDR = {DGXTotalIDR}
                        USDCTotalIDR = {USDCTotalIDR}
                        TUSDTotalIDR = {TUSDTotalIDR}
                        PAXTotalIDR = {PAXTotalIDR}
                        GUSDTotalIDR = {GUSDTotalIDR}
                        USDTTotalIDR = {USDTTotalIDR}
                        TTCTotalIDR = {TTCTotalIDR}
                        SWIPETotalIDR = {SWIPETotalIDR}
                        ZILTotalIDR = {ZILTotalIDR}
                        data = {this.state}
                    />
                    <TradingContest
                        show={this.state.contestModal}
                        refreshdataContest={this.state.refreshDataContest}
                        toggleShow={() => this.setState({ contestModal: !this.state.contestModal, refreshDataContest: !this.state.refreshDataContest})}
                    />
                    </span>
                :null}                                                         
                
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        trading: state.trading,
	    language: state.language
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Object.assign({}, { setKYCStatus: homeActions.setKYCStatus }), dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
