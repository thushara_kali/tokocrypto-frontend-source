import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import classnames from 'classnames';
import NetworkError from '../../errors/networkError';
import NotificationMessage from '../../components/notificationMessage';
import amountFormatter from '../../helpers/amountFormatter';
import cryptoFormatter from '../../helpers/cryptoFormatter';
import { bindActionCreators } from 'redux';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import { TVFullChartContainer } from '../../components/TVChartContainer/fullChart';
import {withRouter} from 'react-router';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class TVChart extends Component {
    constructor(){
        super();
        this.state = {

        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    componentWillMount() {
	    strings.setLanguage(this.props.language);
    }

    componentDidMount(){
        window.scrollTo(0, 0);
    }

    componentWillUnmount() {

    }

    render(){

        return (
            <div className="m-grid m-grid--hor m-grid--root m-page">
		        <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body" style={{ backgroundColor: "#f4f7fa"}}>
			    <div className="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
				<div className="m-grid__item m-grid__item--fluid m-wrapper" style={{marginBottom:'70px'}}>

                    <div className="m-content">
						<div className="row">
							<div className="col-xl-12" style={{ marginBottom: "-30px"}}>
								<div className="m-portlet m-portlet--full-height ">
									<div>
										<div className="m-portlet__head">
											<div className="m-portlet__head-caption">
												<div className="m-portlet__head-title">
													<h3 className="m-portlet__head-text">
														{strings.ADVANCED_TRADING_CHART}
													</h3>
												</div>
											</div>
										</div>
									</div>

                                        <TVFullChartContainer language = {this.props.language}/>

								</div>
							</div>
						</div>
                    </div>
				</div>
                </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
	    language: state.language
    };
};

const mapDispatchToProps = dispatch => ({

});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TVChart));
