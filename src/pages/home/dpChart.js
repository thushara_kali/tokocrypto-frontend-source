import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import classnames from 'classnames';
import homeActions from '../../core/actions/client/home';
import NetworkError from '../../errors/networkError';
import RatesDashboard from '../../components/rates';
import KYCMessage from '../../components/kycMessage';
import NotificationMessage from '../../components/notificationMessage';
import amountFormatter from '../../helpers/amountFormatter';
import cryptoFormatter from '../../helpers/cryptoFormatter';
import RecieveModal from '../../components/recieveModal';
import receiveActions from '../../core/actions/client/recieve';
import SendModal from '../../components/sendModal';
import DepositModal from '../../components/depositModal';
import WithdrawalModal from '../../components/withdrawalModal';
import { bindActionCreators } from 'redux';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import {faSignInAlt, faSignOutAlt, faPaperPlane, faQrcode, faPlusCircle} from '@fortawesome/fontawesome-free-solid';
import RecentTrades from '../../components/recentTrades';
import OrderNow from '../../components/orderBook/orderNow';
import OrderBook from '../../components/orderBook/orderBook';
import Orders from '../../components/orderBook/orders';
import NP from 'number-precision';
import { pixelID } from '../../config/pixel';
import Chart from '../../components/charts';
import { UncontrolledDropdown,Dropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap';
import DepthChart from '../../components/charts/depthChart';
import {withRouter} from 'react-router';
import ReactGA from 'react-ga';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class TVChart extends Component {
    constructor(){
        super();
        this.state = {
            rates: [],
            activeTab: 'BTC',
            activeCurrency: 'BTC',
            IDRAccount: {},
            BTCAccount: {},
            ETHAccount: {},
            XRPAccount: {},
            DGXAccount: {},
            GUSDAccount: {},
            PAXAccount: {},
            USDCAccount: {},
            TUSDAccount: {},
            USDTAccount: {},
            TTCAccount: {},
            SWIPEAccount: {},
            ZILAccount: {},
            receiveModal: false,
            sendModal: false,
            depositModal: false,
            withdrawalModal: false,
			activeAccount: {},
			IDRLimit: 0,
			IDRUsageText: 0,
            IDRUsage: 0,
            IDRUsageLeft: 0,
            ETHUsage: 0,
            BTCUsage: 0,
            XRPUsage: 0,
            DGXUsage: 0,
            GUSDUsage: 0,
            PAXUsage: 0,
            USDCUsage: 0,
            TUSDUsage: 0,
            USDTUsage: 0,
            TTCUsage: 0,
            SWIPEUsage: 0,
            ZILUsage: 0,
            balanceLoading: false,
            currencyPairData: {},
            rateHistory: {},
            btcEqualRates: 0,
            ethEqualRates: 0,
            dgxEqualRates: 0,
            gusdEqualRates: 0,
            paxEqualRates: 0,
            usdcEqualRates: 0,
            tusdEqualRates: 0,
            usdtEqualRates: 0,
            ttcEqualRates: 0,
            swipeEqualRates: 0,
            zilEqualRates: 0,
            XRPAccountFound: false,
            DGXAccountFound: false,
			contestModal: false, //for trading contest
            refreshDataContest: false, //for trading contest
            dropDown: false,
            walletAdd:false 
        }
    }

    callGA(){
        ReactGA.initialize('UA-116825757-2');
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    componentWillMount() {
	    strings.setLanguage(this.props.language);
    }

    fetchDashboard() {
        let username = this.props.user.username;
        let clientId = this.props.user.pool.clientId;
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];

        this.setState({ balanceLoading: true});
        homeActions.fetchDashboard(token).then(res => {

            let accOrdered = _.orderBy(res.data.accounts, (acc) => {
                return acc.Currency.Type;
            });

            let IDRAccount = _.find(accOrdered, {CurrencyCode: 'IDR'});
            let BTCAccount = _.find(accOrdered, {CurrencyCode: 'BTC'});
            let ETHAccount = _.find(accOrdered, {CurrencyCode: 'ETH'});
            let XRPAccount = _.find(accOrdered, {CurrencyCode: 'XRP'});
            let DGXAccount = _.find(accOrdered, {CurrencyCode: 'DGX'});
            let GUSDAccount = _.find(accOrdered, {CurrencyCode: 'GUSD'});
            let PAXAccount = _.find(accOrdered, {CurrencyCode: 'PAX'});
            let USDCAccount = _.find(accOrdered, {CurrencyCode: 'USDC'});
            let TUSDAccount = _.find(accOrdered, {CurrencyCode: 'TUSD'});
            let USDTAccount = _.find(accOrdered, {CurrencyCode: 'USDT'});
            let TTCAccount = _.find(accOrdered, {CurrencyCode: 'TTC'});
            let SWIPEAccount = _.find(accOrdered, {CurrencyCode: 'SWIPE'});
            let ZILAccount = _.find(accOrdered, {CurrencyCode: 'ZIL'});

            this.setState({
                accounts: accOrdered,
                rates: res.data.rates,
                accountState: res.data.kyc,
                IDRAccount,
                BTCAccount,
                ETHAccount,
                XRPAccount,
                DGXAccount,
                GUSDAccount,
                USDCAccount,
                TUSDAccount,
                PAXAccount,
                USDTAccount,
                TTCAccount,
                SWIPEAccount,
                balanceLoading: false,
                currencyPairData: res.data.currencyPair,
                rateHistory: res.data.rateHistory
            });

        }).catch(err => {
            NetworkError(err);
            this.setState({ balanceLoading: false});
            if(window.intervals){
                window.intervals.map((i, index) => {
                    clearInterval(i)
                    window.intervals.splice(index, 1);
                });
            }
        });
    }
    
    getRates(){
        homeActions.getRatesDashboard(this.getToken()).then(res => {

            let rates = res.data.data

            let btcRatesPair = _.find(rates, { currencyPair: 'BTC'+'IDR'}) 
            let ethRatesPair = _.find(rates, { currencyPair: 'ETH'+'IDR'}) 
            let xrpRatesPair = _.find(rates, { currencyPair: 'XRP'+'IDR'})
            let dgxRatesPair = _.find(rates, { currencyPair: 'DGX'+'IDR'})
            let gusdRatesPair = _.find(rates, { currencyPair: 'GUSD'+'IDR'})
            let paxRatesPair = _.find(rates, { currencyPair: 'PAX'+'IDR'})
            let usdcRatesPair = _.find(rates, { currencyPair: 'USDC'+'IDR'})
            let tusdRatesPair = _.find(rates, { currencyPair: 'TUSD'+'IDR'})
            let usdtRatesPair = _.find(rates, { currencyPair: 'USDT'+'IDR'})
            let ttcRatesPair = _.find(rates, { currencyPair: 'TTC'+'IDR'})
            let swipeRatesPair = _.find(rates, { currencyPair: 'SWIPE'+'IDR'})
            let zilRatesPair = _.find(rates, { currencyPair: 'ZIL'+'IDR'})

            if(!_.isEmpty(rates)){

                if(xrpRatesPair){
                    this.setState({xrpEqualRates: xrpRatesPair.customer_buy})
                }
                
                if(dgxRatesPair){
                    this.setState({dgxEqualRates: dgxRatesPair.customer_buy})
                }

                if(gusdRatesPair){
                    this.setState({gusdEqualRates: gusdRatesPair.customer_buy})
                }

                if(paxRatesPair){
                    this.setState({paxEqualRates: paxRatesPair.customer_buy})
                }

                if(usdcRatesPair){
                    this.setState({usdcEqualRates: usdcRatesPair.customer_buy})
                }

                if(tusdRatesPair){
                    this.setState({tusdEqualRates: tusdRatesPair.customer_buy})
                }

                if(usdtRatesPair) {
                    this.setState({usdtEqualRates: usdtRatesPair.customer_buy})
                }

                if(ttcRatesPair) {
                    this.setState({ttcEqualRates: ttcRatesPair.customer_buy})
                }

                if(swipeRatesPair) {
                    this.setState({swipeEqualRates: swipeRatesPair.customer_buy})
                }

                if(zilRatesPair) {
                    this.setState({zilEqualRates: zilRatesPair.customer_buy})
                }

                this.setState({
                    btcEqualRates: btcRatesPair.customer_buy,
                    ethEqualRates: ethRatesPair.customer_buy
                })
            }

            this.setState({
                rates: rates
            });

        }).catch(err => {
            
            NetworkError(err);
        })
    }

    componentDidMount(){
        this.callGA();
        window.scrollTo(0, 0);
        this.fetchDashboard();
        this.getRates();

        if(!window.limitWather) {
	        window.limitWather = setInterval(() => {
                // this.fetchCurrencyLimit();
                this.fetchDashboard();
                this.getRates();
            }, (30 * 1000));

	        if(window.intervals){
	        	window.intervals.push(window.limitWather)
	        } else {
		        window.intervals = [window.limitWather]
	        }
        } else {
	        clearInterval(window.limitWather);
	        window.limitWather = 0;
	        window.limitWather = setInterval(() => {
		        // this.fetchCurrencyLimit();
                this.fetchDashboard();
                this.getRates();
	        }, (30 * 1000));
	        if(window.intervals){
		        window.intervals.push(window.limitWather)
	        } else {
		        window.intervals = [window.limitWather]
	        }
        }
        window.scrollTo(0, 0);
    }

    /**
     * TCDOC-015
     * this is unmount hook from react. we need to unregister every intervals we made so will have mo memory leak
     */
    componentWillUnmount() {
	    window.intervals.map((i, index) => {
		    clearInterval(i)
		    window.intervals.splice(index, 1);
	    });
    }

    render(){

        return (
            <div className="m-grid m-grid--hor m-grid--root m-page">
		        <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body" style={{ backgroundColor: "#f4f7fa"}}>
			    <div className="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
				<div className="m-grid__item m-grid__item--fluid m-wrapper" style={{marginBottom:'70px'}}>

                    <div className="m-content">
						<div className="row">
							<div className="col-xl-12" style={{ marginBottom: "-30px"}}>
								<div className="m-portlet m-portlet--full-height ">
									<div>
										<div className="m-portlet__head">
											<div className="m-portlet__head-caption">
												<div className="m-portlet__head-title">
													<h3 className="m-portlet__head-text">
														Depth Chart
													</h3>
												</div>
											</div>
										</div>
									</div>

                                    <DepthChart 
                                        rateHistory={this.state.rateHistory}
                                        currencyPairData={this.state.currencyPairData}
                                        rates={this.state.rates} 
                                        currency="BTC"
                                        currencyPair="BTCIDR"
                                    />

								</div>
							</div>
						</div>
                    </div>
				</div>
                </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
	    language: state.language
    };
};

const mapDispatchToProps = dispatch => ({

});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TVChart));