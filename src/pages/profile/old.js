import React, { Component } from 'react'
import Avatar from 'react-avatar';
import { Nav, NavLink, NavItem, TabContent, TabPane } from 'reactstrap';
import { connect } from 'react-redux';
import { getUserAttributes, updateAttributes } from '../../core/react-cognito/attributes.js'
import { changePassword } from '../../core/react-cognito/utils.js'
import { NotificationManager } from 'react-notifications'
import * as errMessages from '../../constants/errorMessages'
import { Action } from '../../core/react-cognito';
import store from '../../store'
import classnames from 'classnames';
import { Loader } from 'react-loaders'

class Profile extends Component {

    constructor() {
        super()
        this.state = {
            email: "",
            name: "",
            oldPassword: "",
            password: "",
            confirmPassword: "",
            showNotMatch: false,
            activeTab: '1',
            loading: false
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.value
            this.setState(obj)
        }
    }

    resetForm() {
        this.setState({
            oldPassword: "",
            password: "",
            confirmPassword: "",
            showNotMatch: false
        })
    }

    onConfirmPasswordChange(e) {
        if (this.state.password != e.target.value) {
            this.setState({
                showNotMatch: true,
                confirmPassword: e.target.value
            })
        } else {
            this.setState({
                showNotMatch: false,
                confirmPassword: e.target.value
            })
        }
    }

    onUpdateProfile() {
        let valid = false
        let data = {}
        if (this.state.email.length > 0 && this.state.name.length > 0 && (this.state.password == this.state.confirmPassword)) {
            data = {
                name: this.state.name,
                email: this.state.email
            }
            valid = true
        } else {
            valid = false
        }

        if (valid == true) {
            updateAttributes(this.props.user, data).then((user, attr) => {

                // if (this.state.password.length > 0 && this.state.oldPassword.length > 0) {
                //     changePassword(this.props.user, this.state.oldPassword, this.state.password).then(res => {

                //         NotificationManager.success('Password updated')
                //     })
                //         .catch(err => {

                //             NotificationManager.error(err.message)
                //         })
                // }

                NotificationManager.success('Profile Updated')
                store.dispatch(Action.setFullName(this.state.name));
                this.fetchUserProfile()
                this.resetForm()
            }).catch(err => {
                NotificationManager.error(errMessages.MESSAGE_BAD_REQUEST)
                this.resetForm()
            })
        } else {
            NotificationManager.warning('please complete the form')
        }

    }

    onUpdatePassword() {

        this.setState({
            loading: true
        });

        if (this.state.password.length > 0 && this.state.oldPassword.length > 0) {
            changePassword(this.props.user, this.state.oldPassword, this.state.password).then(res => {
                this.setState({
                    loading: false
                });
                NotificationManager.success('Password updated')
                this.resetForm();
            })
                .catch(err => {
                    this.setState({
                        loading: false
                    });
                    NotificationManager.error('Invalid username or password')
                })
        }
    }

    fetchUserProfile() {
        getUserAttributes(this.props.user).then(attr => {

            this.setState({
                name: attr.name,
                email: attr.email
            })
        })
            .catch(err => {

            })
    }

    componentDidMount() {
        this.fetchUserProfile()
    }

    toggle(tab) {
        this.setState({
            activeTab: tab
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <Avatar className="mx-auto d-block" name={this.props.fullName} size={120} round={true} />
                    </div>
                </div>
                <br /><br />
                <div className="row">
                    <div className="col-md-4"></div>
                    <div className="col-md-4">
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '1' })}
                                    onClick={() => { this.toggle('1'); }}>General</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '2' })}
                                    onClick={() => { this.toggle('2'); }}>Reset Password</NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <br />
                                <h6>Email</h6>
                                <input className="form-control" value={this.state.email} onChange={this.handleChange('email')} />
                                <br />
                                <h6>Name</h6>
                                <input className="form-control" value={this.state.name} onChange={this.handleChange('name')} />
                                <br />
                                <button className="btn btn-primary btn-block" onClick={() => this.onUpdateProfile()}>Update Profile</button>
                            </TabPane>
                            <TabPane tabId="2">
                                <br />
                                <h6>Old Password</h6>
                                <input type="password" className="form-control" value={this.state.oldPassword} onChange={this.handleChange('oldPassword')} />
                                <br />
                                <h6>Password</h6>
                                <input type="password" className="form-control" value={this.state.password} onChange={this.handleChange('password')} />
                                {this.state.password.length < 8 && this.state.password.length > 0 ? <p className="warning-text">password must be at least 8 characters</p> : null}
                                <br />
                                <h6>Confirm Password</h6>
                                <input type="password" className="form-control" value={this.state.confirmPassword} onChange={(e) => this.onConfirmPasswordChange(e)} />
                                {this.state.showNotMatch ? <p className="warning-text">password not match</p> : null}
                                {this.state.confirmPassword.length < 8 && this.state.confirmPassword.length > 0 ? <p className="warning-text">password must be at least 8 characters</p> : null}
                                <br />
                                {this.state.loading == true &&
                                    <Loader type="line-scale" active={this.state.loading} className="text-center" />
                                }
                                {this.state.loading == false &&
                                    <button className="btn btn-primary btn-block" onClick={() => this.onUpdatePassword()}>Update Password</button>
                                }
                            </TabPane>
                        </TabContent>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-4"></div>
                    <div className="col-md-4">

                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-4"></div>
                    <div className="col-md-4">

                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-4"></div>
                    <div className="col-md-4">

                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-4"></div>
                    <div className="col-md-4">

                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        fullName: state.cognito.fullName
    };
};

export default connect(
    mapStateToProps,
)(Profile);
