import React, { Component } from 'react';
import { connect } from 'react-redux';
import KYCMessage from '../../components/kycMessage';
import { getUserAttributes, updateAttributes } from '../../core/react-cognito/attributes.js'
import { changePassword } from '../../core/react-cognito/utils.js'
import ChangePasswordModal from '../../components/changePasswordModal';
import { NotificationManager } from 'react-notifications'
import OTPActions from '../../core/actions/client/otp';
import GAModal from '../../components/gaModal';
import NetworkError from '../../errors/networkError';
import { bindActionCreators } from 'redux';
import homeActions from '../../core/actions/client/home';
import userActions from '../../core/actions/client/user';
import telegramActions from '../../core/actions/client/airdropTelegram';
import referralActions from '../../core/actions/client/referral';
import { Loader } from '../../components/tcLoader';
import OTPModalGeneral from '../../components/OTPModalGeneral';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import fontawesome from '@fortawesome/fontawesome'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {faKey, faMobileAlt, faGem, faQrcode, faEdit, faCheck} from '@fortawesome/fontawesome-free-solid'
import {faGoogle} from '@fortawesome/fontawesome-free-brands';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import amountFormatter from '../../helpers/amountFormatter';
import cryptoFormatter from '../../helpers/cryptoFormatter';
import QRModal from '../../components/qrModal';
import history from '../../history.js';
import ReferralCodeInput from '../../components/referralCodeInput';
import PromoCodeInput from '../../components/promoCodeInput';
import classnames from 'classnames';
import ReactTooltip from 'react-tooltip';
import async from 'async';
import _ from 'lodash';
import NP from 'number-precision';
import GiftVoucher from '../../components/GiftVoucher';
import localStorage from 'localStorage';
import airdropTelegram from '../../core/actions/client/airdropTelegram';
import ReactGA from 'react-ga';

fontawesome.library.add(faKey, faMobileAlt, faGoogle, faGem, faQrcode, faEdit, faCheck)

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class Profile extends Component {

    constructor(){
        super();
        this.state = {
            name: '',
            userEmail: '',
            showChangePassword: false,
            showGA: false,
            showOTP: false,
            ga: false,
            email: false,
            level: 0,
            loadingStatus: false,
            loadingMfa: false,
            friendsTotal: 0,
            commisionTotal: 0,
            discountPercentage: 0,
            commissionPercentage: 0,
            userKyc: '',
            userDiscount: 0,
            promoCode: "",
            loading:false,
            error: false,
            errorMessage: "",
            success: false,
            promoHistory: [],
            airdropStatus : "new",
            fieldAccount: true,
            telegramAccount : "",
            levels: [
                {
                    name: "pending",
                    tradingLimit: 0,
                    withdrawalLimitBTC: 0,
                    withdrawalLimitETH: 0,
                    withdrawalLimitXRP: 0,
                    withdrawalLimitDGX: 0
                },
                {
                    name: "approved",
                    tradingLimit: 0,
                    withdrawalLimitBTC: 0,
                    withdrawalLimitETH: 0,
                    withdrawalLimitXRP: 0,
                    withdrawalLimitDGX: 0
                },
                {
                    name: "premium",
                    tradingLimit: 0,
                    withdrawalLimitBTC: 0,
                    withdrawalLimitETH: 0,
                    withdrawalLimitXRP: 0,
                    withdrawalLimitDGX: 0
                }
            ]
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.value
            this.setState(obj)
        }

        this.handleChangeTelegramAccount = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.value.replace(/[|&;$%@"<>()+,]/g, "")
            this.setState(obj)
        }

    }

    callGA(){
        ReactGA.initialize('UA-116825757-2');
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    fetchDashboard() {
        let username = this.props.user.username;
        let clientId = this.props.user.pool.clientId;
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];

        this.setState({ loadingStatus: true })
        homeActions.fetchDashboard(token).then(res => {
            this.props.actions.setKYCStatus(res.data.kyc);
            this.setState({
                level: res.data.level,
                loadingStatus: false
            })
        }).catch(err => {
            NetworkError(err);
            this.setState({ loadingStatus: false })
        });
    }

    /**
     * TCDOC-034
     * this function calling getUserAttributes from cognito directly
     */
    fetchUserProfile() {
        getUserAttributes(this.props.user).then(attr => {

            let user_id = attr.sub;

            let url = process.env.REACT_APP_BASE_MAIN_URL;
            url = url.replace('service-','app-');
            url = url+"register?refer="+user_id

            this.setState({
                name: attr.name,
                userEmail: attr.email,
                // referralLink: url
            })


        })
            .catch(err => {
                NetworkError(err);
            })
    }

    /**
     * TCDOC-035
     * this function will get current user OTP status to give views then user can update thir otp method later
     */
    fetchUserOTPSTatus(){

        this.setState({
            loadingMfa: true
        });

        OTPActions.getOTPStatus(this.getToken()).then(res => {
            // 
            this.setState({
                ga: res.data.data.ga,
                email: res.data.data.email,
                loadingMfa: false
            })
        }).catch(err => {
            // 
            NetworkError(err);
            this.setState({
                loadingMfa: false
            });
        });
    }

    toggleChangePassword(){
        this.setState({
            showChangePassword: !this.state.showChangePassword
        });
    }

    /**
     * TCDOC-036
     * call this function to show google authenticator setup modal
     */
    toggleGA(){
        this.setState({
            showGA: !this.state.showGA
        });

        if(this.state.showGA === false){
            this.fetchUserOTPSTatus();
        }
    }

    /**
     * TCDOC-037
     * call this funtion to disable google authentication OTP
     */
    enableEmail(){
        this.setState({
            loadingMfa: true
        });

        OTPActions.OTPGADisable(this.getToken()).then(res => {
            // 
            this.fetchUserOTPSTatus();
            NotificationManager.success('email one-time password enabled.');
        }).catch(err => {
            // 
            NetworkError(err);
            // NotificationManager.error('failed enabling email one-time password.');
            this.setState({
                loadingMfa: false
            });
        });
    }

    requestEnableEmail() {
        this.setState({
            showOTP: true
        });
    }

    /**
     * TCDOC-040
     * fetch referral friend list data
     */
    fetchReferralFriends() {
        referralActions.referralFriendsList(this.getToken()).then(res => {
            let friendsList = res.data.data.length

            this.setState({
                friendsTotal: friendsList
            })
        }).catch(err => {
            NetworkError(err);
        });
    }

    fetchCommisionTotal() {
        referralActions.commisionTotal(this.getToken()).then(res => {
            this.setState({
                commisionTotal: res.data.data
            })
        }).catch(err => {
            NetworkError(err);
        });
    }

    fetchReferralConfigs() {
        referralActions.referralConfigs(this.getToken()).then(res => {

            let configs = res.data.data

            let commision = configs.commsionRate.customer_profit_percentage * 100
            let discount = configs.discount.percentage * 100

            this.setState({
                discountPercentage: discount,
                commissionPercentage: commision
            })
        }).catch(err => {
            NetworkError(err);
        });
    }

    checkReferralAvailability(){

        userActions.fetchUserDetails(this.getToken()).then(res => {

            let user = res.data.data

            this.setState({userKyc: user.Kyc})

        }).catch(err => {
            NetworkError(err);
        });
    }

    /**
     * TCDOC-041
     * get user kyc status to determine user limitations on using tokocrypto web
     */

    fetchKycConfigs() {

        async.series({
            fetchUserDetails: (callback) => {
                userActions.fetchUserDetails(this.getToken()).then(res => {

                    let user = res.data.data

                    this.setState({
                        userKyc: user.Kyc
                    })

                    callback(null, res);
                })
                .catch(err => {
                    callback(err, null);
                });
            },
            fetchKycConfigs: (callback) => {
                homeActions.kycConfigs(this.getToken()).then(res => {

                    let configs = res.data.data

                    let levels = this.state.levels

                    levels.forEach(u => {

                        let kyc = u.name

                        if(kyc == "pending"){

                            let data = configs.withdrawKycLimits.pending

                            let converstionData = configs.conversionKycLimits.pending

                            u.tradingLimit = converstionData.idr.max_buy_withing_24_hours,
                            u.withdrawalLimitBTC = data.btc.daily_withdrawal_limit,
                            u.withdrawalLimitETH = data.eth.daily_withdrawal_limit,
                            u.withdrawalLimitXRP = data.xrp.daily_withdrawal_limit,
                            u.withdrawalLimitDGX = data.dgx.daily_withdrawal_limit

                            this.setState({
                                levels
                            });
                        } else if(kyc == "approved"){
                            let data = configs.withdrawKycLimits.approved

                            let converstionData = configs.conversionKycLimits.approved

                            u.tradingLimit = converstionData.idr.max_buy_withing_24_hours,
                            u.withdrawalLimitBTC = data.btc.daily_withdrawal_limit,
                            u.withdrawalLimitETH = data.eth.daily_withdrawal_limit,
                            u.withdrawalLimitXRP = data.xrp.daily_withdrawal_limit,
                            u.withdrawalLimitDGX = data.dgx.daily_withdrawal_limit

                            this.setState({
                                levels
                            });
                        } else if(kyc == "premium"){
                            let data = configs.withdrawKycLimits.premium

                            let converstionData = configs.conversionKycLimits.premium

                            u.tradingLimit = converstionData.idr.max_buy_withing_24_hours,
                            u.withdrawalLimitBTC = data.btc.daily_withdrawal_limit,
                            u.withdrawalLimitETH = data.eth.daily_withdrawal_limit,
                            u.withdrawalLimitXRP = data.xrp.daily_withdrawal_limit,
                            u.withdrawalLimitDGX = data.dgx.daily_withdrawal_limit

                            this.setState({
                                levels
                            });
                        }

                    })

                    callback(null, res);
                })
                .catch(err => {
                    callback(err, null);
                });

            }

        }, (err) => {

            if(err) {
                NetworkError(err);
            }

        });
    }

    userReferralDiscountRate() {
        referralActions.referralUserDiscount(this.getToken()).then(res => {

            let currentDiscount = 0;
            let totalDiscount = 0;
            let userDiscount = 0;

            if(!_.isEmpty(res.data.data)){

                res.data.data.forEach(u => {
                    currentDiscount = u.DiscountValue.percentage

                    totalDiscount += Number(currentDiscount);
                })

                userDiscount = NP.times(totalDiscount, 100)

                if(userDiscount >= 100){
                    this.setState({userDiscount: 100})
                }else {
                    this.setState({userDiscount: userDiscount})
                }
            }

        }).catch(err => {
            NetworkError(err);
        });
    }

    toggleOTP() {
        this.setState({ showOTP: !this.state.showOTP})
    }

    confirmPromoCode(){

        this.setState({loading:true})

        let data = {
            promo_code: this.state.promoCode.toUpperCase()
        };

        referralActions.confirmPromoCode(this.getToken(),data).then(res => {

            this.setState({
                loading:false,
                success:true
            })

            this.getPromoCodeHistory();
            this.userReferralDiscountRate();

        }).catch(err => {
            NetworkError(err);
            this.setState({loading:false});

            let errors = "Cannot Add Promo Code, Please Contact Support"
            if (err.response && err.response.data.err) {
                let msg = err.response.data.err;

                this.setState({
                    error: true,
                    errorsMsg: msg
                })

            } else {
                this.setState({
                    error: true,
                    errorsMsg: errors
                })
            }

        });

    }

    getPromoCodeHistory(){
        referralActions.getPromoCodeHistory(this.getToken()).then(res => {

            this.setState({
                    promoHistory: res.data.data.result
            })

        }).catch(err => {
            // let errors = "Cannot Get Promo Code History"
            // if (err.response && err.response.data.err) {
            //     let msg = err.response.data.err;

            //     NotificationManager.error(msg)

            // } else {
            //     NotificationManager.error(errors)
            // }
            NetworkError(err);
        });
    }

    airdropTelegramCheck(){
        let data = {
            "social_type": "telegram"
        }
        airdropTelegram.checkTelegram(this.getToken(), data).then(res => {

            // 

            if(res.data.data !== null){
                let status = "added"
                let enableField = false
                
                if(res.data.data.Status === "verified"){
                    status = 'verified'
                }
                if(res.data.data.Status === "generated"){
                    status = 'generated'
                }
                this.setState({
                    airdropStatus: status,
                    telegramAccount: res.data.data.Username,
                    fieldAccount: enableField
                })
            }
        }).catch(err => {
            NetworkError(err);
        });
    }

    saveTelegramAccount(){
        let data = {
            "social_type": "telegram",
            "username" : this.state.telegramAccount
        }
        airdropTelegram.addUsername(this.getToken(), data).then(res => {
            

            this.airdropTelegramCheck()
            NotificationManager.success('Telegram Account Added.');
            // this.setState({
            //     airdropStatus: "added"
            // })

        }).catch(err => {
            NetworkError(err);
        });
    }

    verifyTelegramAccount(){
        let data = {
            "social_type": "telegram",
            "username" : this.state.telegramAccount,
            "verifier": "ttc-airdrop"
        }
        // NotificationManager.success('Telegram Account Verified.');
        airdropTelegram.verifyAccount(this.getToken(), data).then(res => {

            this.airdropTelegramCheck()
            NotificationManager.success('Telegram Account Verified.');
            // this.setState({
            //     airdropStatus: "added"
            // })

        }).catch(err => {
            // 
            // NotificationManager.error(err.response.data.err);
            NetworkError(err);
        });
    }
    enableFieldTelegram(){
        this.setState({
            fieldAccount : true
        })
    }

    resetInput() {
        this.setState({
            error: false,
            promoCode: "",
            success: false
        })
    }

    componentDidMount() {
        this.callGA();
        this.fetchCommisionTotal();
        this.fetchReferralFriends();
        this.fetchDashboard();
        this.fetchUserProfile()
        this.fetchUserOTPSTatus();
        this.fetchReferralConfigs();
        this.checkReferralAvailability();
        this.fetchKycConfigs();
        this.userReferralDiscountRate();
        this.getPromoCodeHistory();
        this.airdropTelegramCheck();
        

        window.scrollTo(0, 0);
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    render() {

        let applyDisabled = this.state.promoCode.length === 0 || this.state.loading === true || this.state.success === true || this.state.error === true;

        let activeMethod = 'email';

        if(this.state.email === false) {
            activeMethod = 'ga';
        }

        // 
        // 

        let KYCLevel = "Lv.0";
        if(this.props.kyc.status === "approved") {
            KYCLevel = "Lv.1";
        }
        if(this.props.kyc.status === "premium") {
            KYCLevel = "Lv.Premium";
        }

        let customKyc = ''

        if(this.props.kyc.status == 'draft'){
            customKyc = 'pending'
        } else {
            customKyc = this.props.kyc.status
        }

        let urlTeleJoin = ''

        if (process.env.REACT_APP_ENV === 'demo') {
            urlTeleJoin = "https://t.me/joinchat/Dg2yiRMw0KjGVGj_QMxSQQ"
        }
        if (process.env.REACT_APP_ENV === 'production') {
            urlTeleJoin = "https://t.me/ttc_id"
        }
        if (process.env.REACT_APP_ENV === 'local') {
            urlTeleJoin = "https://t.me/joinchat/Dg2yiRMw0KjGVGj_QMxSQQ"
        }

        let width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

        return (
            <div className="m-grid m-grid--hor m-grid--root m-page">

                <OTPModalGeneral
                    show={this.state.showOTP}
                    toggleShow={() => this.toggleOTP()}
                    method={activeMethod}
                    completeVerification={() => this.enableEmail()}
                />

		        <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body" style={{ backgroundColor: "#f4f7fa"}}>
			    <div className="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container" style={{ paddingBottom: "250px"}}>
				<div className="m-grid__item m-grid__item--fluid m-wrapper">

                    <KYCMessage status={this.props.kyc.status} />

					<div className="m-content col-md-12">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="m-portlet" style={{borderRadius:"6px"}}>
                                    <div className="m-portlet__body  m-portlet__body--no-padding">
                                        <div className="row m-row--no-padding m-row--col-separator-xl">
                                            <div className="user-profile-container-header col-md-12">
                                                <div className="user-profile-header-inner-upper">
                                                    <div className="col-md-2">
                                                        <img src="../../assets/app/media/img/users/user-profile.png" className="user-profile-picture m--img-rounded m--img-centered user-profile-header-profile-img" alt="" />
                                                    </div>
                                                    <div className="user-profile-header-inner-middle row">
                                                        <div className="col-md-7 user-profile-header-inner-middle-text">
                                                            <div style={{fontWeight:"600", letterSpacing:"0.1px"}}>{this.state.name}</div>
                                                            <div style={{fontSize:"14px", fontWeight:"200", letterSpacing:"0.1px"}}>{
                                                                this.props.user.username.indexOf('Google_') !== -1? localStorage.getItem('CACHE_NAME') : this.props.user.username
                                                            }</div>
                                                        </div>
                                                        <div className="col-md-5 user-profile-levels-bar-container">
                                                            <div className="col-md-12 user-profile-levels-bar" style={{display:"inline-flex", backgroundColor:"#00000038", textAlign:"center", borderRadius:"20px", fontWeight:"normal"}}>
                                                                <div data-tip data-for='pending' data-place="bottom" className={classnames({'col-md-4 user-profile-levels-bar-inner-container': true, 'user-profile-selected-level-icon': this.props.kyc.status === 'pending' || this.props.kyc.status === 'draft'})}>
                                                                    <FontAwesomeIcon style={{width:"14px"}} icon={faGem} />&nbsp;&nbsp;<span>Level 0</span>
                                                                </div>
                                                                <div data-tip data-for='approved' data-place="bottom" className={classnames({'col-md-4 user-profile-levels-bar-inner-container': true, 'user-profile-selected-level-icon': this.props.kyc.status === 'approved'})}>
                                                                    <FontAwesomeIcon style={{width:"14px"}} icon={faGem} />&nbsp;&nbsp;<span>Level 1</span>
                                                                </div>
                                                                <div data-tip data-for='premium' data-place="bottom" className={classnames({'col-md-4 user-profile-levels-bar-inner-container': true, 'user-profile-selected-level-icon': this.props.kyc.status === 'premium'})}>
                                                                    <FontAwesomeIcon style={{width:"14px"}} icon={faGem} />&nbsp;&nbsp;<span>Premium</span>
                                                                </div>

                                                                {
                                                                    this.state.levels.map(s => {

                                                                        return (
                                                                            <ReactTooltip data-border='true' id={s.name} effect="solid" delayHide={1000}>
                                                                                <div>
                                                                                    {s.name == customKyc?
                                                                                        <div style={{marginBottom:'10px', marginTop:'10px'}}>{strings.CURRENT_ACCOUNT_LEVEL_IS}
                                                                                            <span style={{color:"#232323", fontWeight:"600"}}>
                                                                                                <span>&nbsp;{KYCLevel}</span>
                                                                                                {this.props.kyc.status === 'pending' || this.props.kyc.status === 'draft'?<span>&nbsp;({strings.UNVERIFIED})</span>:<span>&nbsp;({strings.VERIFIED})</span>}
                                                                                            </span>
                                                                                        </div>:
                                                                                        <div style={{marginBottom:'10px', marginTop:'10px'}}>
                                                                                            {s.name === 'pending' ?<span style={{color:"#232323", fontWeight:"600"}}>&nbsp;Lv.0</span>:null}
                                                                                            {s.name === 'approved'?<span style={{color:"#232323", fontWeight:"600"}}>&nbsp;Lv.1</span>:null}
                                                                                            {s.name === 'premium'?<span style={{color:"#232323", fontWeight:"600"}}>&nbsp;Lv.Premium</span>:null}
                                                                                        </div>
                                                                                    }

                                                                                    <ul style={{marginLeft:"-20px", textAlign:"left"}}>
                                                                                        <li>{strings.TRADING_LIMIT_IS_RP} <span style={{color:"#232323", fontWeight:"600"}}>{amountFormatter(s.tradingLimit)}</span></li>
                                                                                        <li>{strings.WITHDRAWAL_LIMIT_IS} <span style={{color:"#232323", fontWeight:"600"}}>{cryptoFormatter(s.withdrawalLimitBTC,4)}</span> BTC</li>
                                                                                        <li>{strings.WITHDRAWAL_LIMIT_IS} <span style={{color:"#232323", fontWeight:"600"}}>{cryptoFormatter(s.withdrawalLimitETH,4)}</span> ETH</li>
                                                                                        <li>{strings.WITHDRAWAL_LIMIT_IS} <span style={{color:"#232323", fontWeight:"600"}}>{cryptoFormatter(s.withdrawalLimitXRP,4)}</span> XRP</li>
                                                                                        <li>{strings.WITHDRAWAL_LIMIT_IS} <span style={{color:"#232323", fontWeight:"600"}}>{cryptoFormatter(s.withdrawalLimitDGX,4)}</span> DGX</li>
                                                                                        {s.name === 'premium'?<li>{strings.PREMIUM_ACCESS}</li>:null}
                                                                                    </ul>

                                                                                    {this.props.kyc.status === 'pending' || this.props.kyc.status === 'draft'?
                                                                                        <span style={{display:"block",textAlign:"left"}}><a href="https://support.tokocrypto.com/hc/en-us/articles/360004044911-How-do-i-increase-my-trading-deposit-and-withdrawal-limits-" target="_blank"> Click here </a> {strings.TO_UPGRADE_YOUR} <br/> {strings.ACCOUNT_ENJOY_PRIVILAGES}</span>
                                                                                    :null}

                                                                                    {this.props.kyc.status === 'approved' && s.name != "pending" ?
                                                                                        <span style={{display:"block",textAlign:"left"}}><a href="https://support.tokocrypto.com/hc/en-us/articles/360004044911-How-do-i-increase-my-trading-deposit-and-withdrawal-limits-" target="_blank"> Click here </a> {strings.TO_UPGRADE_YOUR} <br/> {strings.ACCOUNT_ENJOY_PRIVILAGES}</span>
                                                                                    :null}

                                                                                </div>
                                                                            </ReactTooltip>
                                                                        )
                                                                    })
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 user-profile-header-inner-lower">
                                                <div className="user-profile-header-inner-lower-status col-md-2">
                                                    { this.props.kyc.status === 'approved' || this.props.kyc.status === 'premium' ?
                                                        <span className="user-profile-button-level-verified"><FontAwesomeIcon style={{width:"10px"}} icon={faCheck} />&nbsp;&nbsp;{strings.VERIFIED}</span> : null
                                                    }
                                                    { this.props.kyc.status === 'pending' ?
                                                        <span className="user-profile-button-level-pending">{strings.PENDING}</span> : null
                                                    }
                                                    { this.props.kyc.status === 'draft' ?
                                                        <div onClick={() => history.push('/verifyaccount')}>
                                                            <span className="user-profile-button-level-unverified">{strings.UNVERIFIED}</span>
                                                        </div>: null
                                                    }
                                                    { this.props.kyc.status === 'rejected'?
                                                        <div onClick={() => history.push('/verifyaccount')}>
                                                            <span className="user-profile-button-level-rejected">{strings.REJECTED}</span>
                                                        </div>: null
                                                    }
                                                </div>
                                                <div className="row user-profile-header-inner-lower-container" style={{width:"100%"}}>
                                                    <div className="col-md-12 user-profile-header-inner-lower-authentication">
                                                        <div className="col-md-7" style={{border:"1px solid #EEEEEE", borderRadius: "11px"}}>
                                                            <div style={{padding:"10px"}}>

                                                                <div style={{fontSize:"14px", color:"#222222de", fontWeight: "700"}}>
                                                                    <FontAwesomeIcon icon={faGoogle} />&nbsp; {strings.TWO_FACTOR_AUTHENTICATION}
                                                                </div>
                                                                <div className="user-profile-header-inner-lower-authentication-types">
                                                                    <div className="user-profile-header-inner-lower-authentication-email">
                                                                        {strings.EMAIL_ONE_TIME_PASSWORD}&nbsp;&nbsp;
                                                                        { this.state.email === false?
                                                                            <button disabled className="user-profile-button-login-disabled" type="button">
                                                                                {strings.DISABLE}
                                                                            </button> :
                                                                            <button disabled className="user-profile-button-login-disabled" type="button">
                                                                                {strings.ENABLE}
                                                                            </button>
                                                                        }
                                                                    </div>
                                                                    <div className="user-profile-header-inner-lower-authentication-google">
                                                                        {strings.GOOGLE_AUTHENTICATOR}&nbsp;&nbsp;
                                                                        { this.state.ga === false?
                                                                            <button onClick={() => this.toggleGA()} className="user-profile-button-login-enable" type="button">
                                                                                {strings.ENABLE}
                                                                            </button> :
                                                                            <button onClick={() => this.requestEnableEmail()} className="user-profile-button-login-enable" type="button">
                                                                                {strings.DISABLE}
                                                                            </button>
                                                                        }
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-1"></div>
                                                        <div className="col-md-4 user-profile-header-inner-lower-authentication-password">
                                                            <div style={{padding:"10px"}}>
                                                                <div style={{fontSize:"14px", color:"#222222de", fontWeight: "700"}}>
                                                                    <FontAwesomeIcon icon={faKey} />&nbsp; {strings.CHANGE_PASSWORD}
                                                                </div>
                                                                { localStorage.getItem('CACHE_METHOD') === 'social'? <div>
                                                                    <div className="user-profile-header-inner-lower-authentication-renew-password">
                                                                        {strings.CHANGE_PASSWORD_NOT_SUPPORTED}&nbsp;&nbsp;
                                                                    </div>
                                                                </div> :
                                                                <div>
                                                                    <div className="user-profile-header-inner-lower-authentication-renew-password">
                                                                        {strings.RENEW_YOUR_PASSWORD}&nbsp;&nbsp;
                                                                        <button className="btn btn-outline-blue user-profile-header-change-password" onClick={() => this.toggleChangePassword()} type="button">{strings.CHANGE_PASSWORD}</button>
                                                                    </div>
                                                                </div> }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="m-portlet m-portlet--full-height">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div style={{ padding: "5%"}}>
                                                <div className="referal-discount-container">
                                                    <div className="col-md-6 referal-middle-container">
                                                        <div className="referal-middle-container-text" style={{color:"#222222de"}}>{strings.YOUR_CURRENT_DISCOUNT}</div>
                                                        <div className="referal-middle-values-container" style={{color:"#222222de", fontWeight:"300"}}>
                                                            {strings.CURRENT_DISCOUNT_DISCRIPTION}
                                                        </div>
                                                    </div>
                                                    <div data-tip data-for='promo-history' className="col-md-4 discount-off-box">
                                                        <div style={{fontWeight:"700", fontSize:"25px"}}>{this.state.userDiscount}%</div>
                                                        <div style={{fontSize:"15px", fontWeight:"300", letterSpacing:"5px"}}>OFF</div>
                                                    </div>
                                                    <ReactTooltip data-border='true' id="promo-history" place="bottom" effect="solid">
                                                        {this.state.promoHistory.length > 0?
                                                            <div>
                                                                <table className="table referral-details-table example-table">
                                                                    <thead>
                                                                        <tr className="referral-details-table-header">
                                                                            <th>Promo Code</th>
                                                                            <th>Title</th>
                                                                            <th>Promotion</th>
                                                                            <th>Expire Date</th>
                                                                        </tr>
                                                                    </thead>

                                                                        {
                                                                            this.state.promoHistory.map((r) => {

                                                                                return (
                                                                                    <tr className="referral-details-table-row">
                                                                                        <td>{r.PromoCode}</td>
                                                                                        <td style={{width:"60px"}}>{r.Promotion.Title}</td>
                                                                                        <td>{r.Promotion.DiscountPercentage?
                                                                                                <span>{NP.times(r.Promotion.DiscountPercentage, 100)}%<br/>Discount</span>:
                                                                                                <span>{r.Promotion.FreeGiftAmount}&nbsp;{r.Promotion.FreeGiftCurrency}<br/>Free Gift</span>
                                                                                            }
                                                                                        </td>
                                                                                        <td className="referral-details-table-date">{r.Promotion.EndDate}</td>
                                                                                    </tr>
                                                                                )
                                                                            })
                                                                        }
                                                                </table>
                                                            </div>:
                                                            <div>{strings.NO_PROMO_CODE_ACTIVE}</div>
                                                        }
                                                    </ReactTooltip>
                                                </div>
                                            </div>

                                            <div style={{padding:"0% 5% 5%"}}>
                                                <span style={{ fontWeight: 600 }}>{strings.GOT_PROMO_CODES}?</span><br />
                                                <div className="row referal-link-container">
                                                    <div className="input-group mb-2 col-md-12 col-lg-7">

                                                        <input disabled={this.props.kyc.status === 'pending' || this.props.kyc.status === 'draft' || this.props.kyc.status === 'rejected'} placeholder="enter promo codes" type="text" autoCapitalize="off" className="form-control discount-code-input"
                                                            onChange={this.handleChange('promoCode')}
                                                            onFocus={this.state.error === true || this.state.success == true ?  () => this.resetInput():null }
                                                        />
                                                        <div className="input-group-append referal-code-animation">

                                                            {this.state.success == true ?
                                                                <img src="../../assets/media/img/verified.png" className="referral-code-success-img" alt="" />:null
                                                            }

                                                            {this.state.error == true ?
                                                                <img src="../../assets/media/img/md-remove-circle.png" className="referral-code-success-img" alt="" />:null
                                                            }

                                                        </div>
                                                        <div></div>

                                                    </div>
                                                    <div className="col-md-5">
                                                        <div className="row refferal-confirm-container">

                                                            {this.state.loading == true ?
                                                                <span className="refferal-loader" style={{display:"block", textAlign:"center"}} >
                                                                    <Loader type="line-scale" active={this.state.loading} className="text-center"/>
                                                                </span>
                                                            :   <button onClick={() => this.confirmPromoCode()} disabled={applyDisabled} className="btn btn-outline-blue" style={{fontWeight:"300"}} type="button">
                                                                    {strings.APPLY}
                                                                </button>
                                                            }

                                                        </div>
                                                    </div>
                                                    {this.state.error && <div className="referal-code-check-availability-error">{this.state.errorsMsg}</div>}
                                                    {this.state.success && <div className="referal-code-check-availability-success">{strings.PRMOTION_ADDED}</div>}
                                                    {this.props.kyc.status === 'pending' || this.props.kyc.status === 'draft'? <div className="referal-code-check-availability-error">{strings.COMPLETE_KYC_PROMO_CODE}</div>:null}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="m-portlet m-portlet--full-height">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div style={{ padding: "5%"}}>
                                                <div className="referal-container" style={{width:"100%"}}>
                                                    <div className="col-md-12 col-lg-2">
                                                        <img src="../../assets/media/img/referrals-profile.png" className="referal-icon" alt="" />
                                                    </div>
                                                    <div className="col-md-12 col-lg-7 referal-middle-container">
                                                        <div className="referal-middle-container-text">{strings.REFERRAL_PROGRAM}</div>
                                                        <div className="referal-middle-values-container">
                                                            <ul style={{marginLeft:"-20px", textAlign:"left"}}>
                                                                <li>{strings.ENJOY} <span style={{fontWeight:"500"}}>{this.state.discountPercentage}%</span> {strings.DISCOUNT}</li>
                                                                <li>{strings.EARN} <span style={{fontWeight:"500"}}>{this.state.commissionPercentage}%</span> {strings.COMMISSION}</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12 col-lg-3 referal-button-container">
                                                        <a onClick={() => history.push('/referral')} className="referal-button">{strings.MORE_INFO}</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div style={{ padding: "0% 5% 5%"}}>
                                                <span style={{ fontWeight: 600 }}>{strings.YOUR_REFERRAL_CODE}</span><br />
                                                <ReferralCodeInput
                                                    col1 = "7"
                                                    col2 = "5"
                                                    />
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            {/* <div className="col-md-4 padding-box">
                                <div className="row">
                                    <div className="m-portlet m-portlet--full-height">
                                        <div className="col-md-12">
                                            <div style={{ padding: "4% 4% 2% 4%"}}>
                                                <div className="referal-discount-container">
                                                    <div className="col-md-6 referal-middle-container">
                                                        <div className="referal-middle-container-text" style={{color:"#222222de"}}>Program Promo Telegram</div>
                                                        <div className="referal-middle-values-container" style={{color:"#222222de", fontWeight:"300"}}>
                                                            Dapatkan berbagai promo menarik lainnya dengan memasukkan akun telegram anda
                                                        </div>
                                                    </div>
                                                    {this.state.airdropStatus === 'new' ? <div data-tip data-for='airdrop-telegram' className="col-md-4 grey-box airdrop-box">
                                                        <div style={{fontSize:"15px", fontWeight:"300", padding:"40% 0%"}}>Join TTC<br />Airdrop</div>
                                                    </div> : null }
                                                    {this.state.airdropStatus === 'added' ? <div data-tip data-for='airdrop-telegram' className="col-md-4 green-box airdrop-box" onClick={() => this.verifyTelegramAccount()}>
                                                        <div style={{fontSize:"15px", fontWeight:"300", padding:"40% 0%"}}>Join TTC<br />Airdrop</div>
                                                    </div> : null }
                                                    {this.state.airdropStatus === 'verified' ? <div data-tip data-for='airdrop-telegram' className="col-md-4 verify-box airdrop-box">
                                                        <div style={{fontSize:"15px", fontWeight:"300", padding:"40% 0%"}}>Join TTC<br />Airdrop</div>
                                                    </div> : null }
                                                    {this.state.airdropStatus === 'generated' ? <div data-tip data-for='airdrop-telegram' className="col-md-4 generated-box airdrop-box">
                                                        <div style={{fontSize:"15px", fontWeight:"300", padding:"40% 0%"}}>Join TTC<br />Airdrop</div>
                                                    </div> : null }
                                                    <ReactTooltip data-border='true' id="airdrop-telegram" place="bottom" effect="solid">
                                                        {this.state.airdropStatus === 'new' ?
                                                            <div>Join TTC Airdrop Group to get free 17 TTC</div>:null
                                                        }
                                                        {this.state.airdropStatus === 'added' ?
                                                            <div>
                                                                1 Step To Go, You Need to verify this account telegram
                                                                <br />
                                                                Click me to verify your acoount
                                                            </div>:null
                                                        }
                                                        {this.state.airdropStatus === 'verified' || this.state.airdropStatus === 'generated' ?
                                                            <div>Yey, you got Free 17 TTC from Tokocrypto</div>:null
                                                        }
                                                    </ReactTooltip>
                                                </div>
                                            </div>
                                            <div style={{ padding: "3% 4%"}}>
                                                <div className="background-gradient-toko promo-code-add-container">
                                                    <span style={{ fontWeight: 600, paddingLeft:"50px" }}>Add Telegram (Join <a style={{ color:"#2d3675"}} target="_blank" href={urlTeleJoin} >Here</a> !)</span><br />
                                                    <div className="row referal-link-container">
                                                        <div className="input-group mb-2 col-md-12 col-lg-9">
                                                            <input disabled={this.state.airdropStatus === 'verified' || this.state.airdropStatus === 'generated' || this.state.fieldAccount === false || this.props.kyc.status === 'draft' || this.props.kyc.status === 'rejected'} placeholder="Telegram Account" type="text" autoCapitalize="off" className="input-telegram form-control telegram-account-input"
                                                                onChange={this.handleChangeTelegramAccount('telegramAccount')}
                                                                value={this.state.telegramAccount}
                                                                onFocus={this.state.error === true || this.state.success == true ?  () => this.resetInput():null }
                                                            />
                                                            <div className="input-group-append referal-code-animation">
                                                                <img src="../../assets/media/img/telegram.png" className="tele-img-airdrop" alt="" />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="row refferal-confirm-container">
                                                                {this.state.fieldAccount === true ?
                                                                    <button disabled={this.state.airdropStatus === 'verified' || this.state.airdropStatus === 'generated' || this.props.kyc.status === 'draft' || this.props.kyc.status === 'rejected'} onClick={() => this.saveTelegramAccount()} className="btn btn-secondary btn-save-airdrop" style={{fontWeight:"300"}} type="button">
                                                                        {this.state.airdropStatus === 'new' ? "Save" : null}
                                                                        {this.state.airdropStatus === 'added' ? "Save" : null}
                                                                        {this.state.airdropStatus === 'verified' ? "Done" : null}
                                                                        {this.state.airdropStatus === 'generated' ? "Done" : null}
                                                                    </button> :
                                                                    <button disabled={this.state.airdropStatus === 'verified' || this.state.airdropStatus === 'generated' || this.props.kyc.status === 'draft' || this.props.kyc.status === 'rejected'} onClick={() => this.enableFieldTelegram()} className="btn btn-secondary btn-save-airdrop" style={{fontWeight:"300"}} type="button">
                                                                        {this.state.airdropStatus === 'new' ? "Save" : null}
                                                                        {this.state.airdropStatus === 'added' ? "Edit" : null}
                                                                        {this.state.airdropStatus === 'verified' ? "Done" : null}
                                                                        {this.state.airdropStatus === 'generated' ? "Done" : null}
                                                                    </button>
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> */}
                            {/* <div className="col-md-6 padding-box">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="m-portlet m-portlet--full-height">
                                            <div style={{ padding: "4%"}}>
                                                <div className="referal-container" style={{width:"100%"}}>
                                                    <div className="col-md-12 col-lg-2">
                                                        <img src="../../assets/media/img/referrals-profile.png" className="referal-icon" alt="" />
                                                    </div>
                                                    <div className="col-md-12 col-lg-7 referal-middle-container">
                                                        <div className="referal-middle-container-text">{strings.REFERRAL_PROGRAM}</div>
                                                        <div className="referal-middle-values-container">
                                                            <ul style={{marginLeft:"-20px", textAlign:"left"}}>
                                                                <li>{strings.ENJOY} <span style={{fontWeight:"500"}}>{this.state.discountPercentage}%</span> {strings.DISCOUNT}</li>
                                                                <li>{strings.EARN} <span style={{fontWeight:"500"}}>{this.state.commissionPercentage}%</span> {strings.COMMISSION}</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12 col-lg-3 referal-button-container">
                                                        <a onClick={() => history.push('/referral')} className="referal-button">{strings.MORE_INFO}</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div style={{ padding: "0% 5% 5%"}}>
                                                <span style={{ fontWeight: 600 }}>{strings.YOUR_REFERRAL_CODE}</span><br />
                                                <ReferralCodeInput
                                                    col1 = "9"
                                                    col2 = "3"
                                                    />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> */}
                        </div>

                        <div className="row">
                            <div className="referal-details-container">
                            <div className="col-md-4">
                                <GiftVoucher method={this.state.ga? 'ga' : 'email'}/>
                            </div>
                            <div className="col-md-4">
                                <div className="col-md-12 referal-details-amount m-portlet m-portlet--full-height" style={{display:"inline-flex"}}>
                                    <div className="col-md-3 referal-details-amount-icons">
                                        <div className="user-profile-bottom-container">
                                            <img src="../../assets/media/img/md-people.png" className="" alt="" />
                                        </div>
                                    </div>
                                    <div className="col-md-9" style={{lineHeight:"1.4"}}>
                                        <div style={{ fontWeight: 400, marginBottom: "10px", color:"#666666" }}>{strings.REFERRAL_FRIENDS}</div>
                                        <span style={{fontWeight: 700, color:"#222222de"}}>{this.state.friendsTotal} {strings.USERS}</span><br/>
                                        <div className="btn btn-outline-blue referal-see-details-button">
                                            <a onClick={() => history.push('/referral')}>{strings.SEE_DETAILS}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="col-md-12 referal-details-amount  m-portlet m-portlet--full-height">
                                    <div className="col-md-3 referal-details-amount-icons">
                                        <div className="user-profile-bottom-container">
                                            <img src="../../assets/media/img/md-wallet.png" className="" alt="" />
                                        </div>
                                    </div>
                                    <div className="col-md-9" style={{lineHeight:"1.4"}}>
                                        <div style={{ fontWeight: 400, marginBottom: "10px", color:"#666666" }}>{strings.COMISSION_VALUE}</div>
                                        <span style={{fontWeight: 700, color:"#222222de"}}>{amountFormatter(this.state.commisionTotal)} IDR</span><br/>
                                        <div className="btn btn-outline-blue referal-see-details-button">
                                            <a onClick={() => history.push('/referral')}>{strings.SEE_DETAILS}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>

                <ChangePasswordModal
                    method={this.state.ga? 'ga' : 'email'}
                    show={this.state.showChangePassword}
                    toggleShow={() => this.toggleChangePassword()}
                />

                <GAModal
                    enabled={this.state.ga}
                    show={this.state.showGA}
                    toggleShow={() => this.setState({ showGA: !this.state.showGA})}
                    onFinish={() => this.fetchUserOTPSTatus()}
                 />

            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        trading: state.trading,
        language: state.language
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Object.assign({}, { setKYCStatus: homeActions.setKYCStatus }), dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps) (Profile);
