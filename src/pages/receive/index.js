/**
 * Filename: home.js
 * Home page
 * Component path
 * IconBoxed  'componenet/iconboxes.js'
 * Charts compoenent from 'compoenent/charts'
 * Table from 'containers/table.js'
 */

import React from 'react';
import { List, ListItem } from 'material-ui/List';
import { connect } from 'react-redux';
import { Loader } from 'react-loaders'
import ActionInfo from 'material-ui/svg-icons/action/info';
import QRCode from 'qrcode.react';
import RaisedButton from 'material-ui/RaisedButton';
import { NotificationManager } from 'react-notifications';
import history from '../../history.js'
import numeral from 'numeral'
import BackButton from '../../components/backButton'
import actions from '../../core/actions/client/recieve';
import * as errorMessages from '../../constants/errorMessages'
import NetworkError from '../../errors/networkError'
import ConfirmAccountBanner from '../../components/confirmAccountBanner';

class Receive extends React.Component {

    constructor(props, context) {
        super(props);

        this.state = {
            addresses: [],
            loading: false,
            creatingAddress: false,
            qRCodeValue: ""
        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    fetchAddresses() {
        this.setState({
            loading: true,
            addresses: []
        })
        this.props.fetchAddressList(this.getToken(), history.location.pathname.split("/")[history.location.pathname.split("/").length - 1].toLowerCase()).then(res => {

            let qRCodeValue = "";

            if (res.data.length > 0) {
                qRCodeValue = res.data[0].Address;
            }
            this.setState({
                addresses: res.data,
                qRCodeValue: qRCodeValue,
                loading: false
            })
        })
            .catch(err => {
                this.setState({
                    loading: false
                })

                NetworkError(err)
            })
    }

    createAddress() {
        this.setState({
            creatingAddress: true
        })
        this.props.createAddress(this.getToken(), history.location.pathname.split("/")[history.location.pathname.split("/").length - 1].toLowerCase()).then(res => {
            let all_addresses = this.state.addresses
            NotificationManager.success('Address created');
            this.setState({
                creatingAddress: false
            })
            this.fetchAddresses();
        })
            .catch(err => {
                this.setState({
                    creatingAddress: false
                })
                NetworkError(err)
                // if (!err.response) {
                //     NotificationManager.error(errorMessages.MESSAGE_NOT_AUTHORIZED)
                //     setTimeout(() => {
                //         history.replace('/login')
                //     }, 1500)
                // } else {
                //     if (err.response.status == 400) {
                //         NotificationManager.error(errorMessages.MESSAGE_BAD_REQUEST)
                //     }
                //     if (err.response.status == 401) {
                //         NotificationManager.error(errorMessages.MESSAGE_NOT_AUTHORIZED)
                //         setTimeout(() => {
                //             history.replace('/login')
                //         }, 1500)
                //     }
                //     if (err.response.status == 500) {
                //         NotificationManager.error(errorMessages.MESSAGE_INTERNAL_SERVER_ERROR)
                //     }
                // }
            })
    }

    checkTransactions(address) {
        let coinCode = history.location.pathname.split("/")[history.location.pathname.split("/").length - 1].toLowerCase()
        if (coinCode == 'eth') {
            window.open('https://kovan.etherscan.io/tx/' + address, '_blank')
        } else if (coinCode == 'btc') {
            window.open('https://www.blocktrail.com/tBTC/tx/' + address, '_blank')
        } else {

        }
    }

    componentDidMount() {
        this.fetchAddresses()
    }

    render() {


        return (
            <div className="container">
                <BackButton />
                <br />
                <div className="col-8 mx-auto">
                    {this.props.kyc.status != 'approved' && this.props.kyc.status != '' && <ConfirmAccountBanner />}
                </div>
                <h4 className="center-text">Recieve {history.location.pathname.split("/")[history.location.pathname.split("/").length - 1]}</h4>
                <br />
                {this.state.addresses.length > 0 &&
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <QRCode size={256} className="text-center" value={this.state.qRCodeValue} />
                        </div>
                        <span className="col-md-12 text-center">
                            {this.state.qRCodeValue}
                        </span>

                    </div>
                }
                <br />
                <p className="center-text">You may create as many addresses as you want for different purposes:</p>
                <div className="row">
                    {this.state.creatingAddress == false &&
                        <button className="btn btn-primary mx-auto" onClick={() => this.createAddress()}>
                            Add {history.location.pathname.split("/")[history.location.pathname.split("/").length - 1]} address
            </button>
                    }
                    {this.state.creatingAddress == true &&
                        <div className="col-md-12 text-center">
                            <Loader type="line-scale" active={this.state.creatingAddress} className="text-center" />
                        </div>
                    }
                </div>
                <br />
                {this.state.addresses.length > 0 ?
                    <div className="row">
                        <div className="container">
                            <table className="table table-stripped">
                                <tr className="col-md-12">
                                    <th>Address</th>
                                    <th>Label</th>
                                    <th>Total Recieved</th>
                                </tr>
                                {this.state.addresses.map((item, index) =>
                                    <tr key={index} className="col-md-12" onClick={() => this.setState({ qRCodeValue: item.Address })}>
                                        <td>{item.Address}</td>
                                        <td></td>
                                        <td><p onClick={() => this.checkTransactions(item.Address)}><span className="fa fa-question-circle"></span> check recieved</p></td>
                                    </tr>
                                )}
                            </table>
                        </div>
                    </div> : null}
                {this.state.loading && <Loader type="line-scale" active={true} className="text-center" />}
            </div >
        );
    }
}



const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc
    };
};

const mapDispatchToProps = dispatch => ({
    fetchAddressList: actions.fetchAddressList,
    createAddress: actions.createAddress
});


/**
 * Container for login behaviour, wrapping a login form.
 *
 * Magically provides the following props to the wrapped form:
 *
 *  * username
 *  * onSubmit
 *
 * @example
 * <Login>
 *   <LoginForm />
 * </Login>
 */


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Receive);
