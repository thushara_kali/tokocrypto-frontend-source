import React, { Component } from 'react'
import history from '../../history.js'
import numeral from 'numeral'
import { Loader } from 'react-loaders'
import BackButton from '../../components/backButton'

class ReviewBuy extends Component {

  constructor() {
    super()
  }

  render() {
    return (
      <div className="container">
        <BackButton />
        <div className="row">
          <div className="col-md-3"></div>
          <div className="col-md-6 col-12">
            <div className="row">
              <h4 className="col-md-12 col-12 center-text">{coin.name} successfully purchased</h4>
            </div>
            <br />
            <div className="row">
              <div className="col-md-6 col-6 center-text">Spent</div>
              <div className="col-md-6 col-6 center-text bold">{currency.code} {numeral(this.state.amount).format('0,0')}</div>
            </div>
            <hr />
            <div className="row">
              <div className="col-md-6 col-6 center-text">Recieved</div>
              <div className="col-md-6 col-6 center-text bold">{coin.code} {(1 / this.state.rate) * (this.state.amount - this.state.fee)}</div>
            </div>
            <hr />
            <div className="row">
              <div className="col-md-6 col-6 center-text">Price</div>
              <div className="col-md-6 col-6 center-text bold">{coin.code} {numeral(this.state.amount - this.state.fee).format('0,0')}</div>
            </div>
            <hr />
            <div className="row">
              <div className="col-md-6 col-6 center-text">Fee</div>
              <div className="col-md-6 col-6 center-text bold">{coin.code} {numeral(this.state.fee).format('0,0')}</div>
            </div>
            <br />
            <div className="row">
              <button onClick={() => history.replace('/')} className="col-md-12 col-12 btn btn-primary">Back to wallet</button>
            </div>
          </div>
        </div>
      </div>
    )
  }

}