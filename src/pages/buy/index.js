import React, { Component } from 'react'
import history from '../../history.js'
import numeral from 'numeral'
import { Loader } from 'react-loaders'
import BackButton from '../../components/backButton'
import { NotificationManager } from 'react-notifications';
import actions from '../../core/actions/client/buy';
import { connect } from 'react-redux';
import * as errorMessages from '../../constants/errorMessages'
import NetworkError from '../../errors/networkError';
import ConfirmAccountBanner from '../../components/confirmAccountBanner';
import amountFormatter from '../../helpers/amountFormatter';
import classnames from 'classnames';
import { formatNoExponent } from '../../helpers/numbers';
import Cleave from 'cleave.js/react';

class Buy extends Component {

    constructor() {
        super()
        this.state = {
            amount: 0,
            loading: false,
            review: false,
            fee: 0,
            spent: 0,
            sell: 0,
            recieved: 0,
            reviewData: {},
            conversionId: "",
            timer: 0,
            doingTransaction: false,
            completed: false,
            currencyUsed: "",
            minimumTrade: 0.001,
            maximumTrade: 100000000,
            fixed_side: 'sell',
            rate: 0
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.rawValue
            this.setState(obj)

        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    resetInput() {
        this.setState({
            amount: 0,
            loading: false,
            review: false,
            fee: 0,
            spent: 0,
            sell: 0,
            recieved: 0,
            reviewData: {}
        })
        clearTimeout(this.timeout)
    }

    setReviewTimer() {
        if (this.state.timer >= 0) {
            this.timeout = setTimeout(() => {
                this.setState({
                    timer: this.state.timer - 1
                })
                this.setReviewTimer()
            }, 1000)
        }
    }

    conversionBuy(){

        let virtualCurrencyCode = history.location.state.rate.currencyPairHummanize.split('/')[0]
        let currencyCode = history.location.state.rate.currencyPairHummanize.split('/')[1]

        let data = {
            baseCurrency: virtualCurrencyCode,
            counterCurrency: currencyCode
        }
        this.setState({
            loading: true
        })

        this.props.conversionBuy(this.getToken(), data).then(res => {
            this.setState({
                loading: false,
                review: false,
                reviewData: res.data,
                fee: res.data.fee.percentage,
                rate: res.data.rate.customer_buy

            });
        }).catch(err => {
            NetworkError(err)
            this.setState({
                loading: false
            })
        })
    }

    buyCoin() {

        let virtualCurrencyCode = history.location.state.rate.currencyPairHummanize.split('/')[0]
        let currencyCode = history.location.state.rate.currencyPairHummanize.split('/')[1]

        let transactionAmount = this.state.amount;

        if (typeof (transactionAmount) != 'number') {
            transactionAmount = parseFloat(transactionAmount.replace(/,/g, ''))
        }

        let amountSufficient = true;

        if (this.state.currencyUsed == currencyCode) {
            transactionAmount = Number(transactionAmount)
            if (Number(transactionAmount) > Number(history.location.state.buyAccount.Balance)) {
                NotificationManager.warning('you dont have enough amounts')
                amountSufficient = false;
            }
        } else {
            transactionAmount = Number(transactionAmount) * Number(this.state.rate)
            if(Number(history.location.state.buyAccount.Balance) < transactionAmount) {
                NotificationManager.warning('you dont have enough amounts')
                amountSufficient = false;
            }
        }

        if(amountSufficient){
            this.setState({
                loading: true
            });

            let convertedAmount;

            if (this.state.currencyUsed == currencyCode) {
                convertedAmount = Number(formatNoExponent(Number(this.state.amount)))
            } else {
                convertedAmount = Number(formatNoExponent(Number(this.state.amount)))
                // convertedAmount = Number(formatNoExponent(Number(this.state.amount))) * Number(this.state.rate)
            }

            let data = {
                buy_currency: virtualCurrencyCode,
                sell_currency: currencyCode,
                amount: convertedAmount,
                fixed_side: this.state.fixed_side
            };

            actions.buyCoin(this.getToken(),data).then(res => {
                this.setState({
                    loading: false
                });

                NotificationManager.success('transaction success');
                this.backToWallet();
            }).catch(err => {
                NetworkError(err)
                NotificationManager.danger('transaction failed, please try again later.');
                this.setState({
                    loading: false
                });
            });
        }
    }

    backToWallet() {
        history.replace('/')
    }

    switchSideTo(currency){
        let virtualCurrencyCode = history.location.state.rate.currencyPairHummanize.split('/')[0]
        let currencyCode = history.location.state.rate.currencyPairHummanize.split('/')[1]

        if(currency != currencyCode){
            this.setState({
                currencyUsed: virtualCurrencyCode,
                fixed_side: 'buy',
                amount: 0
            });
        } else {
            this.setState({
                currencyUsed: currencyCode,
                fixed_side: 'sell',
                amount: 0
            });
        }
    }

    componentDidMount() {
        this.setState({
            rate: history.location.state.rate,
            currencyUsed: history.location.state.rate.currencyPairHummanize.split('/')[1]
        })
        this.conversionBuy();
    }

    componentWillUnmount() {
        clearTimeout(this.timeout)
    }

    render() {
        let virtualCurrencyCode = history.location.state.rate.currencyPairHummanize.split('/')[0]
        let currencyCode = history.location.state.rate.currencyPairHummanize.split('/')[1]
        let buyAccount = history.location.state.buyAccount
        let fee;
        if (this.state.currencyUsed == virtualCurrencyCode) {
            fee = (this.state.amount * this.state.rate) * (this.state.fee) / 100
        } else {
            fee = (this.state.amount) * (this.state.fee) / 100
        }

        let price = this.state.amount - fee
        let recieved = price / Number(this.state.rate);

        let buttonDisabled = true;

        if(this.state.currencyUsed == virtualCurrencyCode){
            buttonDisabled = (this.state.amount < this.state.minimumTrade) || ((this.state.amount * this.state.rate) > this.state.maximumTrade)
        } else {
            buttonDisabled = ((this.state.amount > this.state.maximumTrade) || this.state.amount === 0) || recieved < this.state.minimumTrade
        }

        return (
            <div class="container">

                <br />
                {this.state.review == false && this.state.completed == false ?
                    <div className="row">
                        <div className="col-md-2"></div>
                        <div className="col-md-8 col-12">
                            <div className="row buy-sell-header-container">
                                <div className="col-md-12 col-12">
                                    {this.props.kyc.status != 'approved' && this.props.kyc.status != '' && <ConfirmAccountBanner />}
                                    <BackButton />
                                    {/* <h4 className="center-text">Buy {virtualCurrencyCode} with {currencyCode}</h4> */}
                                    <div className="buy-sell-header-text">Buy {virtualCurrencyCode == 'BTC' ? <span>Bitcoin</span> : <span>Ethereum</span> } at <span style={{color:'#2261ef'}}>IDR {numeral(this.state.rate).format('0,0')}</span> per coin</div>
                                    <div className="buy-sell-second-text">Enter amount, Enter the amount and recive your first {virtualCurrencyCode == 'BTC' ? <span>Bitcoins</span> : <span>Ethereum</span> }</div>
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-md-6 col-6">
                                    {/* <p>I want to sell </p> */}
                                    <div className="buy-sell-how-much">How much you need?</div>
                                    <div>Enter amount of {virtualCurrencyCode == 'ETH' ? <span>Ethereum</span> : <span>Bitcoin</span>} to buy or IDR to sell</div>
                                    <p>You have <span className="color-primary">{buyAccount.Currency.Code} {numeral(buyAccount.Balance).format('0,0')}</span></p>
                                </div>

                                <div className="col-md-6 col-6 buy-sell-input-container">
                                    <div className="row">
                                        <button className="buy-sell-swtich-button"><img className="buy-sell-exchnage-icon" src={require('../../img/exchange.png')} /> </button>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="input-group ">
                                            <span className="input-group-addon" id="basic-addon1">When you sell</span>
                                            {this.state.currencyUsed == virtualCurrencyCode?
                                            <Cleave options={{numeral: true, numeralThousandsGroupStyle: 'thousand'}} onFocus={() => this.switchSideTo(currencyCode)} style={{ zIndex: 0,fontWeight:'bold' }} align="right" value={this.state.amount * this.state.rate} onChange={this.handleChange('amount')} type="text" className="form-control buy-sell-inputs" /> :
                                            <Cleave options={{numeral: true, numeralThousandsGroupStyle: 'thousand'}} onFocus={() => this.switchSideTo(currencyCode)} style={{ zIndex: 0,fontWeight:'bold' }} value={this.state.amount} onChange={this.handleChange('amount')} type="text" className="form-control buy-sell-inputs" /> }
                                            <span className="input-group-addon" id="basic-addon1">{currencyCode}</span>
                                        </div>
                                        <div className="input-group ">
                                            <span className="input-group-addon" id="basic-addon1">You will Recive</span>
                                            {this.state.currencyUsed == virtualCurrencyCode?
                                            <Cleave onFocus={() => this.switchSideTo(virtualCurrencyCode)} style={{ zIndex: 0,fontWeight:'bold' }} value={this.state.amount} onChange={this.handleChange('amount')} type="text" className="form-control buy-sell-inputs" /> :
                                            <Cleave onFocus={() => this.switchSideTo(virtualCurrencyCode)} style={{ zIndex: 0,fontWeight:'bold' }} value={amountFormatter(recieved)} onChange={this.handleChange('amount')} type="text" className="form-control buy-sell-inputs" /> }
                                            <span className="input-group-addon" id="basic-addon1">{virtualCurrencyCode}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row buy-sell-maxmin-container">
                                <p className="col-md-12 col-12">
                                    { this.state.currencyUsed == currencyCode?
                                    <span>
                                        Min: <span className={classnames({"color-primary": recieved >= this.state.minimumTrade, "color-danger": recieved < this.state.minimumTrade})}>{virtualCurrencyCode} {amountFormatter(this.state.minimumTrade)}</span>&nbsp;
                                    </span> :
                                    <span>
                                        Min: <span className={classnames({"color-primary": this.state.amount >= this.state.minimumTrade, "color-danger": this.state.amount < this.state.minimumTrade})}>{virtualCurrencyCode} {amountFormatter(this.state.minimumTrade)}</span>&nbsp;
                                    </span> }
                                    { this.state.currencyUsed == currencyCode?
                                    <span>
                                        Max: <span className={classnames({"color-primary": this.state.amount <= this.state.maximumTrade, "color-danger": this.state.amount > this.state.maximumTrade})}>{currencyCode} {amountFormatter(this.state.maximumTrade)}</span>
                                    </span> :
                                    <span>
                                        Max: <span className={classnames({"color-primary": (this.state.amount * this.state.rate) <= this.state.maximumTrade, "color-danger": (this.state.amount * this.state.rate) > this.state.maximumTrade})}>{currencyCode} {amountFormatter(this.state.maximumTrade)}</span>
                                    </span> }
                                </p>

                            </div>
                            <div className="row buy-sell-operation-header">Converstion Details</div>
                            <div className="row buy-sell-operation-details">
                                <div className="col-md-6 buy-sell-fee-container">
                                    <div className="row">
                                        <div className="col-md-12 col-12">Reciving:
                                        { this.state.currencyUsed == virtualCurrencyCode?
                                        <span className="col-md-12 col-12 bold">{virtualCurrencyCode} {amountFormatter(this.state.amount)}</span> :
                                        <span className="col-md-12 col-12 bold">{virtualCurrencyCode} {amountFormatter(recieved)}</span> }</div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12 col-12 ">Rate:
                                        <span className="col-md-12 col-12 bold">{currencyCode} {numeral(this.state.rate).format('0,0')}</span></div>
                                    </div>
                                </div>
                                <div className="col-md-6 buy-sell-fee-container">
                                    <div className="row">
                                        <div className="col-md-12 col-12">Price:
                                        { this.state.currencyUsed == virtualCurrencyCode?
                                        <span className="col-md-12 col-12 bold">{currencyCode} {    numeral(this.state.amount * this.state.rate).format('0,0')}</span> :
                                        <span className="col-md-12 col-12 bold">{currencyCode} {numeral(price).format('0,0')}</span> }</div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12 col-12 ">Fee:
                                        <span className="col-md-12 col-12 bold">{currencyCode} {numeral(fee).format('0,0')}</span></div>
                                    </div>
                                </div>
                            </div>
                            <div className="row buy-sell-operation-details">
                                <div className="col-md-12 buy-sell-operation-notice">Fill in the form and the send the request.The Transfer will be process within 1-5 buisness days.You can track the status of your request at your wallet page.
                                    "Wallet" tab
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12 col-12">
                                    {/* <div className="input-group">
                                        <span className="input-group-addon" id="basic-addon1">{this.state.currencyUsed}</span>
                                        <input style={{ zIndex: 0 }} value={this.state.amount} onChange={this.handleChange('amount')} type="text" className="form-control" />
                                    </div> */}
                                    <br />

                                    {/* <div className="row">
                                        <p className="col-md-8 col-10">
                                            { this.state.currencyUsed == currencyCode?
                                            <span>
                                                minimum: <span className={classnames({"color-primary": recieved >= this.state.minimumTrade, "color-danger": recieved < this.state.minimumTrade})}>{virtualCurrencyCode} {amountFormatter(this.state.minimumTrade)}</span>&nbsp;
                                            </span> :
                                            <span>
                                                minimum: <span className={classnames({"color-primary": this.state.amount >= this.state.minimumTrade, "color-danger": this.state.amount < this.state.minimumTrade})}>{virtualCurrencyCode} {amountFormatter(this.state.minimumTrade)}</span>&nbsp;
                                            </span> }
                                            { this.state.currencyUsed == currencyCode?
                                            <span>
                                                maximum: <span className={classnames({"color-primary": this.state.amount <= this.state.maximumTrade, "color-danger": this.state.amount > this.state.maximumTrade})}>{currencyCode} {amountFormatter(this.state.maximumTrade)}</span>
                                            </span> :
                                            <span>
                                                maximum: <span className={classnames({"color-primary": (this.state.amount * this.state.rate) <= this.state.maximumTrade, "color-danger": (this.state.amount * this.state.rate) > this.state.maximumTrade})}>{currencyCode} {amountFormatter(this.state.maximumTrade)}</span>
                                            </span> }
                                        </p>
                                        <p className="col-md-4 muted col-12">Daily trading limit <span className="fa fa-question-circle"></span></p>
                                    </div> */}

                                    <br />
                                    <Loader type="line-scale" active={this.state.loading} className="text-center" />
                                    {this.state.loading == false ?
                                        <div className="row">
                                            <button
                                                disabled={buttonDisabled}
                                                onClick={() => this.buyCoin()} className="btn btn-primary btn-block">Buy {virtualCurrencyCode}</button>
                                        </div> : null}
                                </div>
                            </div>
                        </div>
                    </div> : null}
                {/* {this.state.review == true ?
                    <div className="row">11.6498
                        <div className="col-md-3"></div>
                        <div className="col-md-6 col-12">
                            <div className="row">
                                <h4 className="col-md-12 col-12 center-text">Review Purchase</h4>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-md-6 col-6 center-text">Spent</div>
                                <div className="col-md-6 col-6 center-text bold">{currencyCode} {numeral(this.state.spent).format('0,0')}</div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-6 col-6 center-text">Recieved</div>
                                <div className="col-md-6 col-6 center-text bold">{virtualCurrencyCode} {this.state.recieved}</div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-6 col-6 center-text">Rate</div>
                                <div className="col-md-6 col-6 center-text bold">{currencyCode} {numeral(this.state.rate).format('0,0')}</div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-6 col-6 center-text">Price</div>
                                <div className="col-md-6 col-6 center-text bold">{currencyCode} {numeral(this.state.sell).format('0,0')}</div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-6 col-6 center-text">Fee</div>
                                <div className="col-md-6 col-6 center-text bold">{currencyCode} {numeral(this.state.fee).format('0,0')}</div>
                            </div>
                            <br />
                            <div className="row">
                                {this.state.timer <= 0 ?
                                    null : <button onClick={() => this.confirmBuy()} className="col-md-12 col-12 btn btn-primary">Confirm in {this.state.timer}</button>}
                                {this.state.timer == 0 && this.state.doingTransaction == false ?
                                    <div className="col-md-12 col-12">
                                        <p>your time is up. please input again.</p>
                                        <button onClick={() => this.resetInput()} className="col-md-12 col-12 btn btn-primary">Input again</button>
                                    </div> : null}
                                {
                                    this.state.doingTransaction == true ?
                                        <Loader type="line-scale" active={true} className="text-center col-md-12" /> : null
                                }
                            </div>
                        </div>
                    </div>
                    : null} */}
                {/* {this.state.completed == true ?
                    <div className="row">
                        <div className="col-md-3"></div>
                        <div className="col-md-6 col-12">
                            <div className="row">
                                <h4 className="col-md-12 col-12 center-text">Purchase Success</h4>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-md-6 col-6 center-text">Spent</div>
                                <div className="col-md-6 col-6 center-text bold">{currencyCode} {numeral(this.state.spent).format('0,0')}</div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-6 col-6 center-text">Recieved</div>
                                <div className="col-md-6 col-6 center-text bold">{virtualCurrencyCode} {this.state.recieved}</div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-6 col-6 center-text">Rate</div>
                                <div className="col-md-6 col-6 center-text bold">{currencyCode} {numeral(this.state.rate).format('0,0')}</div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-6 col-6 center-text">Price</div>
                                <div className="col-md-6 col-6 center-text bold">{currencyCode} {numeral(this.state.sell).format('0,0')}</div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-6 col-6 center-text">Fee</div>
                                <div className="col-md-6 col-6 center-text bold">{currencyCode} {numeral(this.state.fee).format('0,0')}</div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-md-12 col-12">
                                    <button onClick={() => this.backToWallet()} className="col-md-12 col-12 btn btn-primary">Back to wallet</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    : null} */}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc
    };
};

const mapDispatchToProps = dispatch => ({
    buyCoin: actions.buyCoin,
    conversionBuy: actions.conversionBuy,
    confirmBuy: actions.confirmBuy
});

export default connect(mapStateToProps, mapDispatchToProps)(Buy)
