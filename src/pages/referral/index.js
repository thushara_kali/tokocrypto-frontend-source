import React, { Component } from 'react';
import { connect } from 'react-redux';
import KYCMessage from '../../components/kycMessage';
import { NotificationManager } from 'react-notifications'
import NetworkError from '../../errors/networkError';
import { bindActionCreators } from 'redux';
import homeActions from '../../core/actions/client/home';
import userActions from '../../core/actions/client/user';
import referralActions from '../../core/actions/client/referral';
import { Loader } from '../../components/tcLoader';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import fontawesome from '@fortawesome/fontawesome'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {faKey, faMobileAlt, faGem, faQrcode, faEdit} from '@fortawesome/fontawesome-free-solid'
import {faGoogle} from '@fortawesome/fontawesome-free-brands';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import moment from 'moment';
import amountFormatter from '../../helpers/amountFormatter';
import {CSVLink, CSVDownload} from 'react-csv';
import ReferralCodeInput from '../../components/referralCodeInput';
import _ from 'lodash';
import ReactGA from 'react-ga';

fontawesome.library.add(faKey, faMobileAlt, faGoogle, faGem, faQrcode, faEdit)

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class Referral extends Component {

    constructor(){
        super();
        this.state = {
            friendsList: [],
            commisionTotal: 0,
            commisionList: [],
            commissionPercentage: 0,
            discountPercentage: 0
        }

    }

    callGA(){
        ReactGA.initialize('UA-116825757-2');
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }


    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    fetchReferralFriends() {
        referralActions.referralFriendsList(this.getToken()).then(res => {
            this.setState({
                friendsList : res.data.data
            })
        }).catch(err => {
            NetworkError(err);
        });
    }

    fetchCommisionTotal() {
        referralActions.commisionTotal(this.getToken()).then(res => {
            this.setState({
                commisionTotal: res.data.data
            })
        }).catch(err => {
            NetworkError(err);
        });
    }

    fetchCommisionList() {
        referralActions.commisionList(this.getToken()).then(res => {

            let commisions = _.sortBy(res.data.data, function (c) {
                return moment(c.CreatedAt).unix()
            })

            this.setState({
                commisionList: commisions.reverse()
            })
        }).catch(err => {
            NetworkError(err);
        });
    }

    fetchReferralConfigs() {
        referralActions.referralConfigs(this.getToken()).then(res => {

            let configs = res.data.data

            let commision = configs.commsionRate.customer_profit_percentage * 100
            let discount = configs.discount.percentage * 100

            this.setState({
                discountPercentage: discount,
                commissionPercentage: commision
            })
        }).catch(err => {
            NetworkError(err);
        });
    }

    componentDidMount() {
        this.callGA();
        this.fetchReferralFriends();
        this.fetchCommisionTotal();
        this.fetchCommisionList();
        this.fetchReferralConfigs();
        window.scrollTo(0, 0);
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    render() {


        const csvFriends =this.state.friendsList;
        const csvCommission =this.state.commisionList;

        const headersFriends = [
            {label: 'Email', key: 'Email'},
            {label: 'Tier(Kyc)', key: 'Kyc'},
            {label: 'Date', key:  'CreatedAt'}
        ];

        const headersCommission = [
            {label: 'Email', key: 'Email'},
            {label: 'Amount', key: 'Amount'},
            {label: 'Date', key:  'CreatedAt'}
        ];

        return (
            <div className="m-grid m-grid--hor m-grid--root m-page">

		        <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body" style={{ backgroundColor: "#f4f7fa"}}>
			    <div className="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container" style={{ paddingBottom: "250px"}}>
				<div className="m-grid__item m-grid__item--fluid m-wrapper">

                    <KYCMessage status={this.props.kyc.status} />

					<div className="m-content">

                        <div className="col-md-12 referal-details-banner-container">
                            <img src="assets/media/img/referrals-banner.jpg" style={{width:"100%"}} alt=""/>
                        </div>

                        <div className="m-portlet col-md-12">
                            <div className="m-portlet__body  m-portlet__body--no-padding">
                                <div className="row m-row--no-padding m-row--col-separator-xl">
                                    <div className="col-md-6 referal-details-follow-container">

                                        <div className="col-md-12">
                                            <div className="row">
                                                <div className="referal-details-header-titles">
                                                    <div className="col-md-6 referal-details-steps">{strings.YOUR_REFERRAL_CODE}</div>
                                                    <div className="col-md-6 referal-details-commissions-precentage">{strings.YOUR_COMMISSION_RATE}&nbsp; <span>{this.state.commissionPercentage}%</span></div>
                                                </div>
                                            </div>

                                            <ReferralCodeInput/>
                                        </div>

                                    </div>
                                    <div className="col-md-6 referal-details-works-container">
                                        <div className="col-md-12">
                                                <div className="row">

                                                    <div className="col-md-6 referal-details-steps">{strings.TOP_REFERRALS} </div>

                                                </div>

                                               <div style={{textAlign:"center", color:"#aeb1b3"}}>{strings.COMING_SOON}</div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="referal-details--middle-table-container">
                                <div className="col-xl-6">
                                    <div className="col-md-12 m-portlet">
                                        <div className="row">
                                            <div className="m-portlet__head referal-details-header-mobile">
                                                <div className="m-portlet__head-caption">
                                                    <div className="m-portlet__head-title center-title-bar" style={{width:"100%"}}>
                                                        <h3 className="m-portlet__head-text referal-details-header-text">
                                                            {strings.REFERRAL_FRIENDS}
                                                        </h3>
                                                        <div className="referral-details-friends-header-users">{this.state.friendsList.length} {strings.USERS}</div>
                                                        {this.state.friendsList.length > 0 ?
                                                            <div className="referral-details-friedns-header-button" style={{float:"right"}}>
                                                                <div className="referal-details-tabel-button">
                                                                    <CSVLink data={csvFriends} headers={headersFriends} filename={"Tokocrypto-Referral-Friends-List.csv"} style={{fontWeight:"400"}}>{strings.EXPORT_DATA}</CSVLink>
                                                                </div>
                                                            </div>:null
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="m-widget4 m-widget4--chart-bottom referral-details-table-mobile">
                                                {this.state.friendsList.length > 0 ?
                                                    <table className="table referral-details-table">
                                                        <thead>
                                                            <tr className="referral-details-table-header">
                                                                <th>{strings.NO}</th>
                                                                <th>{strings.TIER}</th>
                                                                <th>{strings.EMAIL}</th>
                                                                <th>{strings.DATE}</th>
                                                            </tr>
                                                        </thead>

                                                        {
                                                            this.state.friendsList.map((r,index) => {

                                                                index++

                                                                return (
                                                                    <tr className="referral-details-table-row">
                                                                        <td>{index}</td>
                                                                        <td className="referral-details-table-data">
                                                                            {r.Kyc == 'premium'?
                                                                                <span>
                                                                                    <img src="assets/media/img/tier-platinum.png" className="referral-details-tire-icon" alt="" />
                                                                                    &nbsp;{strings.Premium}
                                                                                </span>:null
                                                                            }
                                                                            {r.Kyc == 'rejected'?
                                                                                <span>
                                                                                    {strings.REJECTED}
                                                                                </span>:null
                                                                            }
                                                                            {r.Kyc == 'approved'?
                                                                                <span>
                                                                                    {strings.BASIC}
                                                                                </span>:null
                                                                            }
                                                                            {r.Kyc == 'pending'?
                                                                                <span>
                                                                                    {strings.PENDING}
                                                                                </span>:null
                                                                            }

                                                                        </td>
                                                                        <td>{r.Email}</td>
                                                                        <td className="referral-details-table-date">{ moment(r.CreatedAt).format("DD-MM-YYYY") }</td>
                                                                    </tr>
                                                                )
                                                            })
                                                        }

                                                    </table>:
                                                        <div className="no-trades-accounts">
                                                            <span>{strings.NO_REFERRAL_FRIENDS_FOUND}</span>
                                                        </div>
                                                }

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-xl-6">
                                    <div className="col-md-12 m-portlet">
                                        <div className="row">
                                            <div className="m-portlet__head referal-details-header-mobile">
                                                <div className="m-portlet__head-caption">
                                                    <div className="m-portlet__head-title center-title-bar" style={{width:"100%"}}>
                                                        <h3 className="m-portlet__head-text referal-details-header-text">
                                                            {strings.COMISSION}
                                                        </h3>
                                                        <div className="referral-details-friends-header-users">{amountFormatter(this.state.commisionTotal)} IDR</div>
                                                        {this.state.commisionList.length > 0 ?
                                                            <div className="referral-details-friedns-header-button" style={{float:"right"}}>
                                                                <div className="referal-details-tabel-button">
                                                                    <CSVLink data={csvCommission} headers={headersCommission} filename={"Tokocrypto-Referral-Commission-List.csv"} style={{fontWeight:"400"}}>{strings.EXPORT_DATA}</CSVLink>
                                                                </div>
                                                            </div>:null
                                                        }

                                                    </div>
                                                </div>
                                            </div>
                                            <div className="m-widget4 m-widget4--chart-bottom referral-details-table-mobile">
                                                {this.state.commisionList.length > 0 ?
                                                    <table className="table referral-details-table">
                                                        <thead>
                                                            <tr className="referral-details-table-header">
                                                                <th>{strings.NO}</th>
                                                                <th>{strings.EMAIL}</th>
                                                                <th>{strings.COMISSION}</th>
                                                                <th>{strings.DATE}</th>
                                                            </tr>
                                                        </thead>

                                                        {
                                                            this.state.commisionList.map((r,index) => {

                                                                index++

                                                                return (
                                                                    <tr className="referral-details-table-row">
                                                                        <td>{index}</td>
                                                                        <td className="referral-details-table-data">{r.Email}</td>
                                                                        <td>{amountFormatter(r.Amount)} {r.Currency}</td>
                                                                        <td className="referral-details-table-date">{ moment(r.CreatedAt).format("DD-MM-YYYY") }</td>
                                                                    </tr>
                                                                )
                                                            })
                                                        }

                                                    </table>:
                                                        <div className="no-trades-accounts">
                                                            <span>{strings.NO_COMMISSION_FOUND}</span>
                                                        </div>
                                                }

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="m-portlet col-md-12">
                            <div className="row">
                                <div className="m-portlet__head referal-details-header-mobile" style={{backgroundColor:"#F5F5F5"}}>
                                    <div className="m-portlet__head-caption">
                                        <div className="m-portlet__head-title center-title-bar" style={{width:"100%"}}>
                                            <h3 className="m-portlet__head-text referal-details-header-text">
                                                {strings.PROGRAM_DETAILS}
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <div className="m-portlet__body  m-portlet__body--no-padding">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="referal-program-details-container">
                                                <div style={{fontWeight:"600", fontSize:"14px", color:"#000"}}>
                                                    {strings.HOW_IT_WORK}
                                                </div><br/>
                                                <div style={{lineHeight:"25px"}}>
                                                    <ol>
                                                        <li>&nbsp;{strings.MORE_DESCRIPTION_STEP_ONE}</li>
                                                        <li>&nbsp;{strings.MORE_DESCRIPTION_STEP_TWO}</li>
                                                        <li>&nbsp;{strings.MORE_DESCRIPTION_STEP_THREE}</li>
                                                        <li>&nbsp;{strings.MORE_DESCRIPTION_STEP_FOUR}</li>
                                                    </ol>
                                                </div><br/>
                                                <div className="m-widget4 m-widget4--chart-bottom col-md-8 referral-details-example-table-container">
                                                    <div className="referral-details-example-table-header">{strings.EXAMPLE_CONTENT}</div>
                                                    <table className="table referral-details-table example-table">
                                                        <thead>
                                                            <tr className="referral-details-table-header">
                                                                <th>{strings.NO}</th>
                                                                <th>{strings.EMAIL}</th>
                                                                <th>{strings.COMISSION}</th>
                                                            </tr>
                                                        </thead>
                                                            <tr className="referral-details-table-row">
                                                                <td>1</td>
                                                                <td>pa****xa@tokocrypto.com</td>
                                                                <td className="referral-details-table-date">1823 IDR</td>
                                                            </tr>
                                                            <tr className="referral-details-table-row">
                                                                <td>2</td>
                                                                <td>su****rt@tokocrypto.com</td>
                                                                <td className="referral-details-table-date">200 IDR</td>
                                                            </tr>
                                                            <tr className="referral-details-table-row">
                                                                <td>3</td>
                                                                <td>jd****ny@tokocrypto.com</td>
                                                                <td className="referral-details-table-date">163 IDR</td>
                                                            </tr>
                                                            <tr className="referral-details-table-row">
                                                                <td>4</td>
                                                                <td>he****oo@tokocrypto.com</td>
                                                                <td className="referral-details-table-date">523 IDR</td>
                                                            </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6 referal-program-details-second-container">
                                            <div className="referal-program-details-container">
                                                <div style={{fontWeight:"600", fontSize:"14px", color:"#000"}}>
                                                    {strings.RULES_OF_REFERRAL}
                                                </div><br/>
                                                <div style={{lineHeight:"25px"}}>
                                                    <ol>
                                                        <li>&nbsp;{strings.REFERRAL_RULES_ONE}</li>
                                                        <li>&nbsp;{strings.REFERRAL_RULES_TWO}</li>
                                                        <li>&nbsp;{strings.REFERRAL_RULES_THREE} {this.state.commissionPercentage}%</li>
                                                        <li>&nbsp;{strings.REFERRAL_RULES_FOUR}</li>
                                                        <li>&nbsp;{strings.REFERRAL_RULES_FIVE}</li>
                                                        <li>&nbsp;{strings.REFERRAL_RULES_SIX}</li>
                                                        <li>&nbsp;{strings.REFERRAL_RULES_SEVEN_FIRST}&nbsp; {this.state.discountPercentage}%&nbsp; {strings.REFERRAL_RULES_SEVEN_SECOND}</li>
                                                    </ol>
                                                </div><br/>

                                                <div style={{color:"#000"}}>{strings.IMPORTANT_NOTICE_REFERRAL}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                </div>
                </div>

            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        trading: state.trading,
        language: state.language
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Object.assign({}, { setKYCStatus: homeActions.setKYCStatus }), dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps) (Referral);
