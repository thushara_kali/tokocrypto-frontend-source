import React, { Component } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap'
import classnames from 'classnames'
import HistoryItem from '../../components/historyItem.js'
import BackButton from '../../components/backButton'
import { connect } from 'react-redux';
import { Loader } from 'react-loaders'
import actions from '../../core/actions/client/histories';
import _ from 'lodash'
import moment from 'moment'
import * as errorMessages from '../../constants/errorMessages'
import { NotificationManager } from 'react-notifications';
import history from '../../history.js'
import NetworkError from '../../errors/networkError'
import DepositWithdrawItem from '../../components/depositWithdrawItem';
import Skeleton from 'react-loading-skeleton';

class Histories extends Component {

    constructor() {
        super()
        this.state = {
            activeTab: '1',
            conversions: [],
            deposits: [],
            withdrawals: [],
            depositWithdraw: "",
            loading: false
        }
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    fetchConversions() {
        this.setState({
            loading: true
        })
        this.props.fetchConversions(this.getToken()).then(res => {
            let conv = _.sortBy(res.data.conversions, function (c) {
                return moment(c.CreatedAt).unix()
            })
            this.setState({
                conversions: conv.reverse(),
                loading: false
            })
        }).catch(err => {
            NetworkError(err)
        })
    }

    fetchDeposits() {
        this.props.fetchDepositHistory(this.getToken()).then(res => {
            let conv = _.sortBy(res.data, function (c) {
                return moment(c.CreatedAt).unix()
            })
            this.setState({
                deposits: conv.reverse(),
                loading: false
            })
        }).catch(err => {
            NetworkError(err)
        })
    }

    fetchWithdraws() {
        this.props.fetchWithdrawHistory(this.getToken()).then(res => {
            let conv = _.sortBy(res.data, function (c) {
                return moment(c.CreatedAt).unix()
            })
            this.setState({
                withdrawals: conv.reverse(),
                TxId: conv.bitgotxId,
                loading: false
            });
        }).catch(err => {
            NetworkError(err)
        })
    }

    clearAll() {
        this.setState({
            conversions: [],
            deposits: [],
            withdrawals: [],
        });
    }

    fetchAllData() {
        this.clearAll();
        this.fetchConversions();
        this.fetchDeposits();
        this.fetchWithdraws();
    }

    componentDidMount() {
        this.fetchAllData();
    }

    render() {
        return (

            <div className="container transactions-main">

                <div className="col-md-12">

                    <BackButton />
                    <div className="row">
                        <div className="col-md-12">
                            <h4 className="left-text header-history">Transaction</h4>
                        </div>
                    </div>
                    <br />
                    <div className="row">
                        <div className="col-md-12 col-12">
                            <Nav tabs className="nav-justified">
                                <NavItem>
                                    <NavLink
                                        className={classnames({ active: this.state.activeTab === '1' })}
                                        onClick={() => { this.toggle('1'); }}
                                    >
                                        Deposits
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        className={classnames({ active: this.state.activeTab === '2' })}
                                        onClick={() => { this.toggle('2'); }}
                                    >
                                        Withdrawals
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        className={classnames({ active: this.state.activeTab === '3' })}
                                        onClick={() => { this.toggle('3'); }}
                                    >
                                        Conversions
                                    </NavLink>
                                </NavItem>
                            </Nav>
                            <TabContent activeTab={this.state.activeTab}>
                                <TabPane tabId="1">
                                    <div className="">
                                        <br />
                                        <div className="col-md-12">
                                            {this.state.deposits.map((h, i) => {
                                                return (
                                                    <DepositWithdrawItem transaction={h} />
                                                )
                                            })}
                                            {this.state.deposits.length == 0 && this.state.loading == false ?
                                                <div className="alert alert-info" role="alert">
                                                    You don't have any deposit yet.
                                            </div> : null}
                                        </div>
                                    </div>
                                </TabPane>
                                <TabPane tabId="2">
                                    <div className="">
                                        <br />
                                        <div className="col-md-12">
                                            {this.state.withdrawals.map((h, i) => {
                                                return (
                                                    <DepositWithdrawItem transaction={h} />
                                                )
                                            })}
                                            {this.state.withdrawals.length == 0 && this.state.loading == false ?
                                                <div className="alert alert-info" role="alert">
                                                    You don't have any withdrawls yet.
                                            </div> : null}
                                        </div>
                                    </div>
                                </TabPane>
                                <TabPane tabId="3">
                                    <div className="">
                                        <br />
                                        <div className="col-md-12">
                                            {this.state.conversions.map((h, i) => {
                                                return (
                                                    <HistoryItem transaction={h} />
                                                )
                                            })}
                                            {this.state.conversions.length == 0 && this.state.loading == false ?
                                                <div className="alert alert-info" role="alert">
                                                    You don't have any conversions yet.
                                            </div> : null}
                                        </div>
                                    </div>
                                </TabPane>
                            </TabContent>
                        </div>
                    </div>
                </div>

            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user
    };
};

const mapDispatchToProps = dispatch => ({
    fetchConversions: actions.fetchConversions,
    fetchDepositHistory: actions.fetchDepositHistory,
    fetchWithdrawHistory: actions.fetchWithdrawHistory
});

export default connect(mapStateToProps, mapDispatchToProps)(Histories)
