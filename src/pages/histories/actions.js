import services from '../../services'
import NetworkError from '../../errors/networkError';

const fetchConversions = (token) => {
  return new Promise((resolve, reject) => {
    services.fetchConversions(token).then(res => {
      resolve(res.data)
    })
      .catch(err => {
        NetworkError(err)
        reject(err)
      })
  })
}

const fetchDepositHistory = (token) => {
  return new Promise((resolve,reject) => {
    services.fetchDepositHistory(token).then(res => {
      resolve(res.data);
    })
    .catch(err => {
      NetworkError(err)
      reject(err);
    })
  });
}

const fetchWithdrawHistory = (token) => {
  return new Promise((resolve,reject) => {
    services.fetchWithdrawHistory(token).then(res => {
      resolve(res.data);
    })
    .catch(err => {
      NetworkError(err)
      reject(err);
    })
  });
}


export default {
  fetchConversions,
  fetchDepositHistory,
  fetchWithdrawHistory
}