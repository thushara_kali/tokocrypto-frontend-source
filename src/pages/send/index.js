/**
 * Filename: home.js
 * Home page
 * Component path
 * IconBoxed  'componenet/iconboxes.js'
 * Charts compoenent from 'compoenent/charts'
 * Table from 'containers/table.js'
 */

import React from 'react';
import { List, ListItem } from 'material-ui/List';
import { sendTranscation } from '../../core/actions/client/send';
import actionsOTP from '../../core/actions/client/otp';
import { connect } from 'react-redux';
import { Loader } from 'react-loaders'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { NotificationManager } from 'react-notifications';
import Panel from '../../components/panel.js';
import BackButton from '../../components/backButton'
import history from '../../history.js'
import classnames from 'classnames'
import numeral from 'numeral'
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap'
import Select from 'react-select';
import countriesPhoneCode from '../../countriesPhoneCode.json'
import ConfirmAccountBanner from '../../components/confirmAccountBanner';
import NetworkError from '../../errors/networkError';
import OTPModal from '../../components/OTPModal';

// var ethereum_address = require('ethereum-address');
const bitcoin_address = require('bitcoin-address');

export const BoxSingle = (props) => {
    // Used a style const for changing icon box
    // color using props.
    const style = {
        backgroundColor: props.iconbg
    }


    return (
        <div className="iconbox-single">
            <div className="box-title">
                <div className="circle-icon" style={style}>
                    <i className="material-icons">{props.icon}</i>
                </div>
                <div>
                    <h5>{props.coin}</h5>
                    <p>{props.label}</p>
                </div>
            </div>

            <div>
                <h5>Balance</h5>
                <p>{props.balance} {props.coin}</p>
            </div>
        </div>
    );
}

class Send extends React.Component {

    constructor(props, context) {
        super(props);

        this.state = {
            wallet: {
                coin: 'btc',
                label: 'BTC',
                balance: 1300
            },
            sendingMethod: "",
            amount: 0,
            coinAddress: "",
            email: "",
            loading: false,
            sendingCoin: false,
            address: '',
            amount: 0,
            notes: "",
            phoneCode: "+62",
            activeTab: '1',
            currencies: [
                {
                    code: "IDR",
                    rate: 23220000
                }
            ],
            showOTP: false,
            OTPEnable: true
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.value
            this.setState(obj)
        }
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    toggleOTP() {
        this.setState({
            showOTP: !this.state.showOTP
        });
    }

    completeVerification(otpId, token) {
        this.toggleOTP();
        this.doSendTransaction(otpId, token);
    }

    componentDidMount() {
        actionsOTP.checkOTP(this.getToken()).then(res => {
            this.setState({
                OTPEnable: (res.data.data.Withdraw === 'Enable')
            });
        }).catch(err => {
            NetworkError(err);
            // NotificationManager.error('Failed get 2fa config.');
        });
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    sendTranscation() {

        let balance = history.location.state.balance;
        let adddress = this.state.coinAddress.trim();
        let virtualCurrency = history.location.pathname.split("/")[history.location.pathname.split("/").length - 1];
        let environment = process.env.REACT_APP_BASE_ADMIN_URL
        let type = "prod";

        if (this.state.coinAddress.trim() == "") {
            NotificationManager.warning("address cannot empty");
        } else if (this.state.amount <= 0) {
            NotificationManager.warning("amount must be greather than 0");
        } else if (this.state.amount > balance) {
            NotificationManager.warning("you dont have enough balance");
        } else {
            if (this.state.OTPEnable) {
                this.toggleOTP();
            } else {
                if ( virtualCurrency == 'BTC'){
                    if( environment == 'dev' || 'demo'){
                        type = 'testnet';
                    }
                    var valid = bitcoin_address.validate(adddress, type);
                    if(valid){

                        this.doSendTransaction(null, null);
                    }
                    else{

                        NotificationManager.warning("Invalid Bitcoin address");
                    }
                } else {
                    // if (ethereum_address.isAddress(adddress)){
                    //     this.doSendTransaction(null, null);
                    // } else {
                    //     NotificationManager.warning("Invalid Ethereum address");
                    // }
                }

            }
        }
    }

    doSendTransaction(otpId, token) {
        this.setState({
            sending: true
        });
        let currency = history.location.pathname.split("/")[history.location.pathname.split("/").length - 1].toLowerCase();

        this.props.sendTranscation(this.getToken(), otpId, token, currency, this.state.coinAddress, parseFloat(this.state.amount)).then(res => {

            this.setState({
                coinAddress: "",
                amount: 0,
                sending: false
            });
            NotificationManager.success('transaction success');
        })
            .catch(err => {
                this.setState({
                    sending: false
                });
                NetworkError(err);

            })
    }

    render() {
        let balance = history.location.state.balance;

        return (
            <div className="container">
                <BackButton />
                <OTPModal show={this.state.showOTP} toggleShow={this.toggleOTP.bind(this)} completeVerification={this.completeVerification.bind(this)} />
                <br />
                <div className="col-8 mx-auto">
                    {this.props.kyc.status != 'approved' && this.props.kyc.status != '' && <ConfirmAccountBanner />}
                </div>
                <Loader type="line-scale" active={this.state.loading} className="text-center" />
                <h4 className="center-text">Send {history.location.pathname.split("/")[history.location.pathname.split("/").length - 1]}</h4>
                <br />
                <div className="row">
                    <div className="col-md-2">
                    </div>
                    <div className="col-md-8">
                        <Nav tabs className="nav-justified">
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '1' })}
                                    onClick={() => { this.toggle('1'); }}
                                >
                                    Send To {history.location.pathname.split("/")[history.location.pathname.split("/").length - 1]} Address
                                </NavLink>
                            </NavItem>
                            {/* <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '2' })}
                  onClick={() => { this.toggle('2'); }}
                >
                  Send By Email
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '3' })}
                  onClick={() => { this.toggle('3'); }}
                >
                  Send By SMS
                </NavLink>
              </NavItem> */}
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <br />
                                <div className="container">
                                    <div className="col-md-12">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <label className="control-label">{history.location.pathname.split("/")[history.location.pathname.split("/").length - 1]} address</label>
                                                <input value={this.state.coinAddress} onChange={this.handleChange('coinAddress')} className="form-control" type="text" />
                                            </div>
                                        </div>
                                        <br />
                                        <div className="row">
                                            <div className="col-md-12">
                                                <label className="control-label">Amount</label>
                                                <div className="input-group">
                                                    <span className="input-group-addon" id="basic-addon1">{history.location.pathname.split("/")[history.location.pathname.split("/").length - 1]}</span>
                                                    <input value={this.state.amount} onChange={this.handleChange('amount')} type="text" className="form-control" />
                                                </div>
                                                <p style={{ marginTop: 10 }}>You have {history.location.pathname.split("/")[history.location.pathname.split("/").length - 1]} {balance}</p>
                                            </div>
                                        </div>
                                        <br />
                                        <div className="row">
                                            <div className="col-md-12">
                                                <label className="control-label">Note for self</label>
                                                <textarea value={this.state.notes} onChange={this.handleChange('notes')} className="form-control" rows={7}></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </TabPane>
                            <TabPane tabId="2">
                                {/* <br />
                <div className="container">
                  <div className="col-md-12">
                    <div className="row">
                      <div className="col-md-12">
                        <label className="control-label">Email</label>
                        <input value={this.state.email} onChange={this.handleChange('email')} className="form-control" type="text" />
                      </div>
                    </div>
                    <br />
                    <div className="row">
                      <div className="col-md-12">
                        <label className="control-label">Amount</label>
                        <div className="input-group">
                          <span className="input-group-addon" id="basic-addon1">{history.location.pathname.split("/")[history.location.pathname.split("/").length - 1]}</span>
                          <input value={this.state.amount} onChange={this.handleChange('amount')} type="text" className="form-control" />
                        </div>
                      </div>
                    </div>
                    <br />
                    <div className="row">
                      <div className="col-md-12">
                        <label className="control-label">Note for self</label>
                        <textarea value={this.state.notes} onChange={this.handleChange('notes')} className="form-control" rows={7}></textarea>
                      </div>
                    </div>
                  </div>
                </div> */}
                            </TabPane>
                            <TabPane tabId="3">
                                {/* <br />
                <div className="container">
                  <div className="col-md-12">
                    <div className="row">
                      <div className="col-md-12">
                        <label className="control-label">Country</label>
                        <Select
                          name="form-field-name"
                          value={this.state.phoneCode}
                          options={countriesPhoneCode}
                          onChange={(val) => { this.setState({ phoneCode: val.value }) }}
                        />
                      </div>
                    </div>
                    <br />
                    <div className="row">
                      <div className="col-md-12">
                        <label className="control-label">Phone</label>
                        <div className="input-group">
                          <span className="input-group-addon" id="basic-addon1">{this.state.phoneCode}</span>
                          <input style={{ zIndex: 0 }} value={this.state.amount} onChange={this.handleChange('amount')} type="text" className="form-control" />
                        </div>
                      </div>
                    </div>
                    <br />
                    <div className="row">
                      <div className="col-md-12">
                        <label className="control-label">Amount</label>
                        <div className="input-group">
                          <span className="input-group-addon" id="basic-addon1">{history.location.pathname.split("/")[history.location.pathname.split("/").length - 1]}</span>
                          <input style={{ zIndex: 0 }} value={this.state.amount} onChange={this.handleChange('amount')} type="text" className="form-control" />
                        </div>
                      </div>
                    </div>
                    <br />
                    <div className="row">
                      <div className="col-md-12">
                        <label className="control-label">Note for self</label>
                        <textarea style={{ zIndex: 0 }} value={this.state.notes} onChange={this.handleChange('notes')} className="form-control" rows={7}></textarea>
                      </div>
                    </div>
                  </div>
                </div> */}
                            </TabPane>
                        </TabContent>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4">
                            </div>
                            <div className="col-md-4">
                                {this.state.sending ?
                                    <Loader type="line-scale" active={true} className="text-center" /> :
                                    <button onClick={() => this.sendTranscation()} className="btn btn-primary btn-block">Next</button>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}



const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc
    };
};

const mapDispatchToProps = dispatch => ({
    sendTranscation: sendTranscation
});


/**
 * Container for login behaviour, wrapping a login form.
 *
 * Magically provides the following props to the wrapped form:
 *
 *  * username
 *  * onSubmit
 *
 * @example
 * <Login>
          *   <LoginForm />
          * </Login>
        */


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Send);
