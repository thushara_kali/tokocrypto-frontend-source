import React, { Component } from 'react'
import BackButton from '../../components/backButton'
import { connect } from 'react-redux'
import { Loader } from '../../components/tcLoader';
import actions from '../../core/actions/client/histories';
import Skeleton from 'react-loading-skeleton';
import homeActions from '../../core/actions/client/home';
import NetworkError from '../../errors/networkError';

class Notifications extends Component {

    constructor() {
        super();
        this.state = {
            notifications: [],
            loading: false
        };
    }

    getToken() {
        let username = this.props.user.username;
        let clientId = this.props.user.pool.clientId;
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];
        return token;
    }

    getNotifications(){

        this.setState({
            loading: true
        });

        homeActions.getNotifications(this.getToken()).then(res => {
            this.setState({
                notifications: res.data.data,
                showNotifications: true,
                loading: false
            });
        }).catch(err => {
            NetworkError(err);
            this.setState({
                loading: false
            });
        });
    }

    componentDidMount() {
        this.getNotifications();
    }

    render() {
        return (

            <div className="col-md-12 transactions-main-container">
                <div className="col-md-6 offset-md-3">

                    <BackButton/>
                    <div className="row">
                        <div className="col-md-12">
                            <h4 className="left-text header-history">Notifications</h4>
                        </div>
                    </div>
                    <br/>
                    <div className="row">
                        <div className="col-md-12 col-12">
                            <div className="">
                                <br/><br/><br/>
                                {/* <Loader type="line-scale" active={this.state.loading} className="text-center" /> */}
                                <div className="row">
                                    <div className="col-md-12 history-conversions-container">
	                                    { this.state.loading && <div style={{ marginLeft: "50%"}}>
	                                        <Loader  type="line-scale" active={this.state.loading} className="text-center" />
	                                    </div> }

                                        {this.state.notifications.map((n, i) => {
                                            return (
                                                <div className="m-alert m-alert--outline m-alert--square alert alert-dismissible fade show alert-info">
                                                    <div className="row">
                                                        <div className="col-md-8 col-6 history-coversions-details hidden-sm-down">
                                                            <div className="notification-title">{ n.Title }</div>
                                                            <p className="notification-body">{ n.Message }</p>
	                                                        <p className="notification-body">{ n.StarDate }</p>
                                                        </div>
                                                    </div>
                                                </div >
                                            )
                                        })}
                                        {this.state.notifications.length == 0 && this.state.loading == false ?
                                            <div className="alert alert-info" role="alert">
                                                You don't have any notifications yet.
                                            </div> : null}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user
    }
}

const mapDispatchToProps = dispatch => ({
    fetchConversions: actions.fetchConversions,
    fetchDepositHistory: actions.fetchDepositHistory,
    fetchWithdrawHistory: actions.fetchWithdrawHistory
})

export default connect(mapStateToProps, mapDispatchToProps)(Notifications)
