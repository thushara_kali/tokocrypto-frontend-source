import React, { Component } from 'react';
import KYCMessage from '../../components/kycMessage';
import { connect } from 'react-redux';
import classnames from 'classnames';
import NetworkError from '../../errors/networkError';
import _ from 'lodash';
import homeActions from '../../core/actions/client/home';
import amountFormatter from '../../helpers/amountFormatter';
import cryptoFormatter, { cryptoFormatterByCoin } from '../../helpers/cryptoFormatter';
import TransactionHistory from '../../components/transactionHistory';
import RecieveModal from '../../components/recieveModal';
import receiveActions from '../../core/actions/client/recieve';
import SendModal from '../../components/sendModal';
import DepositModal from '../../components/depositModal';
import WithdrawalModal from '../../components/withdrawalModal';
import history from '../../history';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import fontawesome from '@fortawesome/fontawesome'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {faSignInAlt, faSignOutAlt, faPaperPlane, faQrcode} from '@fortawesome/fontawesome-free-solid'
import currencyFormatter from '../../helpers/currencyFormatter'
import ReactGA from 'react-ga';

fontawesome.library.add(faSignInAlt, faSignOutAlt, faPaperPlane, faQrcode)

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class AccountsPage extends Component {

    constructor(){
        super();
        this.state = {
            activeTab: 'IDR',
            IDRAccount: {},
            BTCAccount: {},
            ETHAccount: {},
            XRPAccount: {},
            DGXAccount: {},
            GUSDAccount: {},
            PAXAccount: {},
            USDCAccount: {},
            TUSDAccount: {},
            USDTAccount: {},
            TTCAccount: {},
            SWIPEAccount: {},
            ZILAccount: {},
            activeAccount: {},
            receiveModal: false,
            sendModal: false,
            depositModal: false,
            withdrawalModal: false,
            receiveAddress: '',
            addresses: [],
            IDRLimit: 0,
			IDRUsageText: 0,
            IDRUsage: 0,
            ETHUsage: 0,
            BTCUsage: 0,
            XRPUsage: 0,
            DGXUsage: 0,
            GUSDUsage: 0,
            PAXUsage: 0,
            USDCUsage: 0,
            TUSDUsage: 0,
            USDTUsage: 0,
            TTCUsage: 0,
            SWIPEUsage: 0,
            ZILUsage: 0,
            btcEqualRates: 0,
            ethEqualRates: 0,
            xrpEqualRates: 0,
            dgxEqualRates: 0,
            gusdEqualRates: 0,
            paxEqualRates: 0,
            usdcEqualRates: 0,
            tusdEqualRates: 0,
            usdtEqualRates: 0,
            ttcEqualRates: 0,
            swipeEqualRates: 0,
            zilEqualRates: 0
        }
    }

    callGA(){
        ReactGA.initialize('UA-116825757-2');
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    /**
     * TCDOC-024
     * This page is to fetch all currency balance, make sure to add new currency into the filters if have new currency on the system
     * 
     * @param {} currency 
     * currency param is to set the active tab
     */
    fetchAccounts(currency = null){
        let username = this.props.user.username;
        let clientId = this.props.user.pool.clientId;
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];
        homeActions.fetchDashboard(token).then(res => {

            if(currency !== null) {
                this.setState({
                    activeTab: ''
                });

                this.setState({
                    activeTab: currency
                });
            }

            // homeActions.setKYCStatus(res.data.kyc);
            if(this.state.activeTab !== 'IDR' && this.state.activeTab !== 'idr'){
                this.fetchAddresses();
            }
        }).catch(err => {
            NetworkError(err);
            if(window.intervals){
                window.intervals.map((i, index) => {
                    clearInterval(i)
                    window.intervals.splice(index, 1);
                });
            }
        });
    }

    /**
     * TCDOC-025
     * this function is to fetch all of the currency limit
     */
    fetchCurrencyLimit(){
        homeActions.getCurrencyUsage(this.getToken(), 'idr').then(res => {
            // 

            let limit = Number(res.data.data.idr.dailyWithdrawalLimit);
            let usage = limit - Number(res.data.data.idr.availableWithdrawAmountForTheDay);

            let percentage = (usage / limit ) * 100;

            // BTC

            let BTClimit = Number(res.data.data.btc.dailyWithdrawalLimit);
            let BTCusage = limit - Number(res.data.data.btc.availableWithdrawAmountForTheDay);

            let BTCpercentage = (BTCusage / BTClimit ) * 100;

            // ETH

            let ETHlimit = Number(res.data.data.eth.dailyWithdrawalLimit);
            let ETHusage = limit - Number(res.data.data.eth.availableWithdrawAmountForTheDay);

            let ETHpercentage = (ETHusage / ETHlimit ) * 100;

            // XRP

            let XRPlimit = Number(res.data.data.xrp.dailyWithdrawalLimit);
            let XRPusage = limit - Number(res.data.data.xrp.availableWithdrawAmountForTheDay);

            let XRPpercentage = (XRPusage / XRPlimit ) * 100;

            // DGX

            let DGXlimit = Number(res.data.data.dgx.dailyWithdrawalLimit);
            let DGXusage = limit - Number(res.data.data.dgx.availableWithdrawAmountForTheDay);

            let DGXpercentage = (DGXusage / DGXlimit ) * 100;

            // GUSD
            let GUSDlimit = Number(res.data.data.gusd.dailyWithdrawalLimit);
            let GUSDusage = Number(res.data.data.gusd.availableWithdrawAmountForTheDay);
            let GUSDpercentage = (GUSDusage / GUSDlimit);
 
            // PAX
            let PAXlimit = Number(res.data.data.pax.dailyWithdrawalLimit);
            let PAXusage = Number(res.data.data.pax.availableWithdrawAmountForTheDay);
            let PAXpercentage = (PAXusage / PAXlimit);

            // USDC
            let USDClimit = Number(res.data.data.usdc.dailyWithdrawalLimit);
            let USDCusage = Number(res.data.data.usdc.availableWithdrawAmountForTheDay);
            let USDCpercentage = (USDCusage / USDClimit);

            // TUSD
            let TUSDlimit = Number(res.data.data.tusd.dailyWithdrawalLimit);
            let TUSDusage = Number(res.data.data.tusd.availableWithdrawAmountForTheDay);
            let TUSDpercentage = (TUSDusage / TUSDlimit);
 
            // USDT
            let USDTlimit = Number(res.data.data.usdt.dailyWithdrawalLimit);
            let USDTusage = Number(res.data.data.usdt.availableWithdrawAmountForTheDay);
            let USDTpercentage = (USDTusage / USDTlimit);

            // TTC
            let TTClimit = Number(res.data.data.ttc.dailyWithdrawalLimit);
            let TTCusage = Number(res.data.data.ttc.availableWithdrawAmountForTheDay);
            let TTCpercentage = (TTCusage / TTClimit);

            // SWIPE
            let SWIPElimit = Number(res.data.data.swipe.dailyWithdrawalLimit);
            let SWIPEusage = Number(res.data.data.swipe.availableWithdrawAmountForTheDay);
            let SWIPEpercentage = (SWIPEusage / SWIPElimit);

            // ZIL
            let ZILlimit = Number(res.data.data.zil.dailyWithdrawalLimit);
            let ZILusage = Number(res.data.data.zil.availableWithdrawAmountForTheDay);
            let ZILpercentage = (ZILusage / ZILlimit)

            this.setState({
                IDRUsage: percentage,
                IDRUsageLeft: Number(res.data.data.idr.availableWithdrawAmountForTheDay),
				IDRLimit: limit,
                IDRUsageText: usage,

                BTCUsage: BTCpercentage,
                BTCUsageLeft: Number(res.data.data.btc.availableWithdrawAmountForTheDay),

                ETHUsage: ETHpercentage,
                ETHUsageLeft: Number(res.data.data.eth.availableWithdrawAmountForTheDay),

                XRPUsage: XRPpercentage,
                XRPUsageLeft: Number(res.data.data.xrp.availableWithdrawAmountForTheDay),

                DGXUsage: DGXpercentage,
                DGXUsageLeft: Number(res.data.data.dgx.availableWithdrawAmountForTheDay),

                GUSDUsage: Number(GUSDpercentage) * 100,
                GSUDUsageLeft: Number(res.data.data.gusd.availableWithdrawAmountForTheDay),

                PAXUsage: Number(PAXpercentage) * 100,
                PAXUsageLeft: Number(res.data.data.pax.availableWithdrawAmountForTheDay),

                USDCUsage: Number(USDCpercentage) * 100,
                USDCUsageLeft: Number(res.data.data.usdc.availableWithdrawAmountForTheDay),

                TUSDUsage: Number(TUSDpercentage) * 100,
                TUSDUsageLeft: Number(res.data.data.tusd.availableWithdrawAmountForTheDay),

                USDTUsage: Number(USDTpercentage) * 100,
                USDTUsageLeft: Number(res.data.data.usdt.availableWithdrawAmountForTheDay),

                TTCUsage: Number(TTCpercentage) * 100,
                TTCUsageLeft: Number(res.data.data.ttc.availableWithdrawAmountForTheDay),

                SWIPEUsage: Number(SWIPEpercentage) * 100,
                SWIPEUsageLeft: Number(res.data.data.swipe.availableWithdrawAmountForTheDay),

                ZILUsage: Number(ZILpercentage) * 100,
                ZILUsageLeft: Number(res.data.data.zil.availableWithdrawAmountForTheDay)
            });

            // 
        }).catch(err => {
            NetworkError(err);
            // 
        });
    }

    getAccountWithBalance(){
        homeActions.getAccountDashboard(this.getToken()).then(res => {

            let accOrdered = _.orderBy(res.data.data, (acc) => {
                return acc.Currency.Type;
            });

            let IDRAccount = _.find(accOrdered, {CurrencyCode: 'IDR'});
            let BTCAccount = _.find(accOrdered, {CurrencyCode: 'BTC'});
            let ETHAccount = _.find(accOrdered, {CurrencyCode: 'ETH'});
            let XRPAccount = _.find(accOrdered, {CurrencyCode: 'XRP'});
            let DGXAccount = _.find(accOrdered, {CurrencyCode: 'DGX'});
            let GUSDAccount = _.find(accOrdered, {CurrencyCode: 'GUSD'});
            let PAXAccount = _.find(accOrdered, {CurrencyCode: 'PAX'});
            let USDCAccount = _.find(accOrdered, {CurrencyCode: 'USDC'});
            let TUSDAccount = _.find(accOrdered, {CurrencyCode: 'TUSD'});
            let USDTAccount = _.find(accOrdered, {CurrencyCode: 'USDT'});
            let TTCAccount = _.find(accOrdered, {CurrencyCode: 'TTC'});
            let SWIPEAccount = _.find(accOrdered, {CurrencyCode: 'SWIPE'});
            let ZILAccount = _.find(accOrdered, {CurrencyCode: 'ZIL'});

            if(!XRPAccount){

                let data = {
                    currency: "XRP"
                }

                homeActions.createNewWallet(this.getToken(),data).then(res => {
                }).catch(err => {
                    this.setState({
                        XRPAccountFound: true
                    })
                    NetworkError(err);
                });
            }

            if(!DGXAccount){

                let data = {
                    currency: "DGX"
                }

                homeActions.createNewWallet(this.getToken(),data).then(res => {
                }).catch(err => {
                    this.setState({
                        DGXAccountFound: true
                    })
                    NetworkError(err);
                });
            }

            if(!GUSDAccount) {
                let data = {
                    currency: "GUSD"
                }

                homeActions.createNewWallet(this.getToken(),data).then(res => {
                }).catch(err => {
                    this.setState({
                        GUSDAccountFound: true
                    })
                    NetworkError(err);
                })
            }

            if(!PAXAccount) {
                let data = {
                    currency: "PAX"
                }

                homeActions.createNewWallet(this.getToken(),data).then(res => {
                }).catch(err => {
                    this.setState({
                        PAXAccountFound: true
                    })
                    NetworkError(err);
                })
            }

            if(!USDCAccount) {
                let data = {
                    currency: "USDC"
                }

                homeActions.createNewWallet(this.getToken(),data).then(res => {
                }).catch(err => {
                    this.setState({
                        USDCAccountFound: true
                    })
                    NetworkError(err);
                })
            }
            if(!TUSDAccount) {
                let data = {
                    currency: "TUSD"
                }

                homeActions.createNewWallet(this.getToken(),data).then(res => {
                }).catch(err => {
                    this.setState({
                        TUSDAccountFound: true
                    })
                    NetworkError(err);
                })
            }

            if(!USDTAccount) {
                let data = {
                    currency: "USDT"
                }

                homeActions.createNewWallet(this.getToken(), data).then(res => {
                }).catch(err => {
                    this.setState({
                        USDTAccount: true
                    })
                    NetworkError(err);
                })
            }

            if(!TTCAccount) {
                let data = {
                    currency: "TTC"
                }

                homeActions.createNewWallet(this.getToken(), data).then(res => {
                }).catch(err => {
                    this.setState({
                        TTCAccount: true
                    })
                    NetworkError(err);
                })
            }

            if(!SWIPEAccount) {
                let data = {
                    currency: "SWIPE"
                }

                homeActions.createNewWallet(this.getToken(), data).then(res => {
                }).catch(err => {
                    this.setState({
                        SWIPEAccount: true
                    })
                    NetworkError(err);
                })
            }

            if(!ZILAccount) {
                let data = {
                    currency: "ZIL"
                }

                homeActions.createNewWallet(this.getToken(), data).then(res => {
                }).catch(err => {
                    this.setState({
                        ZILAccount: true
                    })
                    NetworkError(err);
                })
            }

            this.setState({
                accounts: accOrdered,
                IDRAccount,
                BTCAccount,
                ETHAccount,
                XRPAccount,
                DGXAccount,
                GUSDAccount,
                USDCAccount,
                TUSDAccount,
                PAXAccount,
                USDTAccount,
                TTCAccount,
                SWIPEAccount,
                ZILAccount,
                balanceLoading: false,
            });

        }).catch(err => {
            NetworkError(err);
            if(window.intervals){
                window.intervals.map((i, index) => {
                    clearInterval(i)
                    window.intervals.splice(index, 1);
                });
            }
        })
    }

    getRates(){
        homeActions.getRatesDashboard(this.getToken()).then(res => {

            let rates = res.data.data

            let btcRatesPair = _.find(rates, { currencyPair: 'BTC'+'IDR'}) 
            let ethRatesPair = _.find(rates, { currencyPair: 'ETH'+'IDR'}) 
            let xrpRatesPair = _.find(rates, { currencyPair: 'XRP'+'IDR'})
            let dgxRatesPair = _.find(rates, { currencyPair: 'DGX'+'IDR'})
            let gusdRatesPair = _.find(rates, { currencyPair: 'GUSD'+'IDR'})
            let paxRatesPair = _.find(rates, { currencyPair: 'PAX'+'IDR'})
            let usdcRatesPair = _.find(rates, { currencyPair: 'USDC'+'IDR'})
            let tusdRatesPair = _.find(rates, { currencyPair: 'TUSD'+'IDR'})
            let usdtRatesPair = _.find(rates, { currencyPair: 'USDT'+'IDR'})
            let ttcRatesPair = _.find(rates, { currencyPair: 'TTC'+'IDR'})
            let swipeRatesPair = _.find(rates, { currencyPair: 'SWIPE'+'IDR'})
            let zilRatesPair = _.find(rates, { currencyPair: 'ZIL'+'IDR'})

            if(!_.isEmpty(rates)){

                if(xrpRatesPair){
                    this.setState({xrpEqualRates: xrpRatesPair.customer_buy})
                }
                
                if(dgxRatesPair){
                    this.setState({dgxEqualRates: dgxRatesPair.customer_buy})
                }

                if(gusdRatesPair){
                    this.setState({gusdEqualRates: gusdRatesPair.customer_buy})
                }

                if(paxRatesPair){
                    this.setState({paxEqualRates: paxRatesPair.customer_buy})
                }

                if(usdcRatesPair){
                    this.setState({usdcEqualRates: usdcRatesPair.customer_buy})
                }

                if(tusdRatesPair){
                    this.setState({tusdEqualRates: tusdRatesPair.customer_buy})
                }

                if(usdtRatesPair) {
                    this.setState({usdtEqualRates: usdtRatesPair.customer_buy})
                }

                if(ttcRatesPair) {
                    this.setState({ttcEqualRates: ttcRatesPair.customer_buy})
                }

                if(swipeRatesPair) {
                    this.setState({swipeEqualRates: swipeRatesPair.customer_buy})
                }

                if(zilRatesPair) {
                    this.setState({zilEqualRates: zilRatesPair.customer_buy})
                }

                this.setState({
                    btcEqualRates: btcRatesPair.customer_buy,
                    ethEqualRates: ethRatesPair.customer_buy
                })
            }

            this.setState({
                rates: res.data.rates
            });

        }).catch(err => {
            NetworkError(err);
            if(window.intervals){
                window.intervals.map((i, index) => {
                    clearInterval(i)
                    window.intervals.splice(index, 1);
                });
            }
        })
    }


    /**
     * TCDOC-026
     * fetch the current tab coin address
     */
    fetchAddresses(){
        this.setState({
            loading: true,
            addresses: []
        })
        receiveActions.fetchAddressList(this.getToken(), this.state.activeTab.toLowerCase()).then(res => {

            let qRCodeValue = "";

            if (res.data.length > 0) {
                qRCodeValue = res.data[0].Address;
            }
            this.setState({
                receiveAddress: qRCodeValue,
                addresses: res.data,
                loading: false
            })
        })
            .catch(err => {
                this.setState({
                    loading: false
                })
                NetworkError(err)
            })
    }

    changeTab(tab){
        this.setState({
            activeTab: tab
        });


        // 

        if(tab !== 'IDR' && tab !== 'idr'){
            setTimeout(() => {
                this.fetchAddresses();
            }, 100)
        }
    }

    onCloseRecieveModal(){
        this.setState({
            receiveModal: false
        })
    }

    openSendModal(currency){
        if(currency === 'BTC'){
            setTimeout(() => {
                this.setState({
                    activeAccount: this.state.BTCAccount,
                    sendModal: true
                });
                // 
            }, 100);
        } else if(currency === 'ETH'){
            setTimeout(() => {
                this.setState({
                    activeAccount: this.state.ETHAccount,
                    sendModal: true
                });
                // 
            }, 100);
        } else if(currency === 'XRP'){
            setTimeout(() => {
                this.setState({
                    activeAccount: this.state.XRPAccount,
                    sendModal: true
                });
                // 
            }, 100);
        } else if(currency === 'DGX'){
            setTimeout(() => {
                this.setState({
                    activeAccount: this.state.DGXAccount,
                    sendModal: true
                });
                // 
            }, 100);
        } else if(currency === 'TTC') {
            setTimeout(() => {
                this.setState({
                    activeAccount: this.state.TTCAccount,
                    sendModal: true
                });
                // 
            }, 100);
        }  else if(currency === 'GUSD') {
            setTimeout(() => {
                this.setState({
                    activeAccount: this.state.GUSDAccount,
                    sendModal: true
                });
                // 
            }, 100);
        } else if(currency === 'TUSD') {
            setTimeout(() => {
                this.setState({
                    activeAccount: this.state.TUSDAccount,
                    sendModal: true
                });
                // 
            }, 100);
        } else if(currency === 'PAX') {
            setTimeout(() => {
                this.setState({
                    activeAccount: this.state.PAXAccount,
                    sendModal: true
                });
                // 
            }, 100);
        } else if(currency === 'USDC') {
            setTimeout(() => {
                this.setState({
                    activeAccount: this.state.USDCAccount,
                    sendModal: true
                });
                // 
            }, 100);
        } else if(currency === 'USDT') {
            setTimeout(() => {
                this.setState({
                    activeAccount: this.state.USDTAccount,
                    sendModal: true
                });
                // 
            }, 100);
        } else if(currency === 'SWIPE') {
            setTimeout(() => {
                this.setState({
                    activeAccount: this.state.SWIPEAccount,
                    sendModal: true
                });
                // 
            }, 100);
        } else if(currency === 'ZIL') {
            setTimeout(() => {
                this.setState({
                    activeAccount: this.state.ZILAccount,
                    sendModal: true
                });
                // 
            }, 100);
        }

    }

    onCloseSendModal(){
        this.setState({
            sendModal: false
        })
    }

    componentDidMount(){
        this.callGA();
        this.fetchAccounts();
        this.getRates();
        this.getAccountWithBalance();
        this.fetchCurrencyLimit();

        let selectedCurrency = 'BTC';
        if(history.location.search.length !== 0) {
            selectedCurrency = history.location.search.replace('?currency=', '');
            if(selectedCurrency.length === 3){
                this.setState({
                    activeTab: selectedCurrency
                });
            }
        }
        window.scrollTo(0, 0);

        this.accountsPolling = setInterval(() => {
            this.fetchAccounts();
            this.getRates();
            this.getAccountWithBalance();
        }, 30 * 1000);

	    if(window.intervals){
		    window.intervals.push(this.accountsPolling)
	    } else {
		    window.intervals = [this.accountsPolling]
	    }
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    componentWillUnmount() {
	    window.intervals.map((i, index) => {
		    clearInterval(i)
		    window.intervals.splice(index, 1);
	    });
    }

    render() {
        return (
            <div className="m-grid m-grid--hor m-grid--root m-page">
                <div className="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body" style={{ backgroundColor: "#f4f7fa"}}>
			    <div className="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container" style={{ paddingBottom: "120px"}}>
				<div className="m-grid__item m-grid__item--fluid m-wrapper">

                    <KYCMessage status={this.props.kyc.status}/>
                    <div className="m-content">
						<div className="row">
							<div className="col-xl-12" style={{ marginBottom: "-30px"}}>
								<div className="m-portlet m-portlet--full-height ">
                                    <div>
                                        <div className="m-portlet__head">
                                            <div className="m-portlet__head-caption">
                                                <div className="m-portlet__head-title">
                                                    <h3 className="m-portlet__head-text">
                                                        {strings.YOUR_ACCOUNTS}
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

									<div className="trading-desk-container col-md-12" style={{ paddingLeft:"0xp", paddingRight:"0px" }}>
										<div className="col-xl-4" style={{ paddingLeft:"0px" }}>

											<div className="m-portlet m-portlet--full-height ">

												<div className="m-portlet__body" style={{ padding: "0.0rem 0.0rem" }}>
													<div className="row m-row--no-padding m-row">
                                                        <div onClick={() => this.setState({ activeTab: 'IDR'})} className={classnames({
                                                            'col-md-12 accounts-wallet-border-bottom': true,
                                                            'accounts-active': this.state.activeTab === 'IDR'
                                                        })}>

															<h4 className="m-widget24__title text-header accounts-header-wallets">
                                                                Rupiah
															</h4>
															<img className="custom-img" src="../../assets/media/img/logo/indonesia.png" alt="" />
															<div className="m-widget24">
																<div className="m-widget24__item">
																	<h4 className="m-widget24__title web-avaialable">
																		{strings.AVAILABLE_BALANCE}
																	</h4>
																	<br />

																	<span className="m-widget24__stats m--font-green web-balance">
																		{ currencyFormatter( amountFormatter(this.state.IDRAccount.Balance) )}
																	</span>
																	<br />
																	<div className="m--space-30"></div>

																</div>
																<div className="row">
																	<div className="col-md-12 wallet-button-container">
																		<button onClick={() => this.setState({ depositModal: !this.state.depositModal})} className="btn btn-outline-green" type="button" data-toggle="modal" data-target="#m_modal_4">
                                                                            <FontAwesomeIcon icon={faSignInAlt} /> {strings.DEPOSIT}
																		</button>
																		<button onClick={() => this.setState({ withdrawalModal: !this.state.withdrawalModal})} className="btn btn-outline-green" type="button" data-toggle="modal" data-target="#m_modal_5">
                                                                            <FontAwesomeIcon icon={faSignOutAlt} /> {strings.WITHDRAWAL}
																		</button>
																	</div>
																</div>

															</div>


														</div>
														<div onClick={() => this.changeTab('BTC')} className={classnames({
                                                            'col-md-12 accounts-wallet-border-bottom': true,
                                                            'accounts-active': this.state.activeTab === 'BTC'
                                                        })}>

															<h4 className="m-widget24__title text-header accounts-header-wallets">
																Bitcoin
																<i className="cc BTC" style={{ float: "right", marginRight: "30px", fontSize: "34px" }}></i>
															</h4>
															<div className="m-widget24">
																<div className="m-widget24__item">
																	<h4 className="m-widget24__title web-avaialable">
                                                                        {strings.AVAILABLE_BALANCE}
																	</h4>
																	<br />

																	<span className="m-widget24__stats bitcoin-color web-balance accounts-equalAmounts-container">
                                                                        { cryptoFormatterByCoin( this.state.BTCAccount.Balance, 'BTC' )} BTC<br/>
                                                                        <span className="accounts-equalAmounts">&asymp; { currencyFormatter( amountFormatter(this.state.BTCAccount.Balance * this.state.btcEqualRates, this.state.activeTab) ) }</span>
																	</span>

																	<br />
																	<div className="m--space-30"></div>

																</div>
																<div className="row">
																	<div className="col-md-12 wallet-button-container accounts-crypto-buttons">
																		<button onClick={() => this.openSendModal('BTC')} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_8">
                                                                            <FontAwesomeIcon icon={faPaperPlane} /> {strings.SEND}
																		</button>

																		<button onClick={() => this.setState({ receiveModal: true, loading: true})} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_6">
                                                                            <FontAwesomeIcon icon={faQrcode} /> {strings.RECEIVE}
																		</button>
																	</div>
																</div>
															</div>

														</div>
														<div onClick={() => this.changeTab('ETH')} className={classnames({
                                                            'col-md-12 accounts-wallet-border-bottom': true,
                                                            'accounts-active': this.state.activeTab === 'ETH'
                                                        })}>

															<h4 className="m-widget24__title text-header accounts-header-wallets">
																Ethereum
															</h4>
															<i className="cc ETH-alt m--font-info" style={{ float: "right", marginRight: "30px", fontSize: "34px", marginTop: "-26px" }}></i>
															<div className="m-widget24">
																<div className="m-widget24__item">
																	<h4 className="m-widget24__title web-avaialable">
                                                                        {strings.AVAILABLE_BALANCE}
																	</h4>
																	<br />

																	<span className="m-widget24__stats m--font-info web-balance accounts-equalAmounts-container">
                                                                        { cryptoFormatterByCoin( this.state.ETHAccount.Balance, 'ETH' )} ETH<br/>
                                                                        <span className="accounts-equalAmounts">&asymp; { currencyFormatter( amountFormatter(this.state.ETHAccount.Balance * this.state.ethEqualRates, this.state.activeTab) ) } </span>
																	</span>

																	<br />
																	<div className="m--space-30"></div>

																</div>
																<div className="row">
																	<div className="col-md-12 wallet-button-container accounts-crypto-buttons">
																		<button onClick={() => this.openSendModal('ETH')} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_8">
                                                                            <FontAwesomeIcon icon={faPaperPlane} /> {strings.SEND}
																		</button>
																		<button onClick={() => this.setState({ receiveModal: true, loading: true})} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_6">
                                                                            <FontAwesomeIcon icon={faQrcode} /> {strings.RECEIVE}
																		</button>
																	</div>
																</div>
															</div>
														</div>
                                                        <div onClick={() => this.changeTab('XRP')} className={classnames({
                                                            'col-md-12 accounts-wallet-border-bottom': true,
                                                            'accounts-active': this.state.activeTab === 'XRP'
                                                        })}>

															<h4 className="m-widget24__title text-header accounts-header-wallets">
																Ripple
															</h4>
															<i className="cc XRP-alt m--font-info" style={{ float: "right", marginRight: "30px", fontSize: "34px", marginTop: "-26px" }}></i>
															<div className="m-widget24">
																<div className="m-widget24__item">
																	<h4 className="m-widget24__title web-avaialable">
                                                                        {strings.AVAILABLE_BALANCE}
																	</h4>
																	<br />

																	<span className="m-widget24__stats m--font-info web-balance accounts-equalAmounts-container">
                                                                        { cryptoFormatterByCoin( this.state.XRPAccount.Balance, 'XRP' )} XRP<br/>
                                                                        <span className="accounts-equalAmounts">&asymp; { currencyFormatter( amountFormatter(this.state.XRPAccount.Balance * this.state.xrpEqualRates, this.state.activeTab) ) } </span>
																	</span>

																	<br />
																	<div className="m--space-30"></div>

																</div>
																<div className="row">
																	<div className="col-md-12 wallet-button-container accounts-crypto-buttons">
																		<button onClick={() => this.openSendModal('XRP')} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_8">
                                                                            <FontAwesomeIcon icon={faPaperPlane} /> {strings.SEND}
																		</button>
																		<button onClick={() => this.setState({ receiveModal: true, loading: true})} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_6">
                                                                            <FontAwesomeIcon icon={faQrcode} /> {strings.RECEIVE}
																		</button>
																	</div>
																</div>
															</div>
														</div>
                                                        <div onClick={() => this.changeTab('DGX')} className={classnames({
                                                            'col-md-12 accounts-wallet-border-bottom': true,
                                                            'accounts-active': this.state.activeTab === 'DGX'
                                                        })}>

															<h4 className="m-widget24__title text-header accounts-header-wallets">
																DGX
															</h4>
                                                            <img style={{ float: "right", marginRight: "30px", width: "34px", marginTop: "-26px"}} src="../../assets/media/img/logo/dgx_token.png" alt="" />
															<div className="m-widget24">
																<div className="m-widget24__item">
																	<h4 className="m-widget24__title web-avaialable">
                                                                        {strings.AVAILABLE_BALANCE}
																	</h4>
																	<br />

																	<span className="m-widget24__stats m--font-info web-balance accounts-equalAmounts-container">
                                                                        { cryptoFormatterByCoin( this.state.DGXAccount.Balance, 'DGX' )} DGX<br/>
                                                                        <span className="accounts-equalAmounts">&asymp; { currencyFormatter( amountFormatter(this.state.DGXAccount.Balance * this.state.dgxEqualRates, this.state.activeTab) ) }  </span>
																	</span>

																	<br />
																	<div className="m--space-30"></div>

																</div>
																<div className="row">
																	<div className="col-md-12 wallet-button-container accounts-crypto-buttons">
																		<button onClick={() => this.openSendModal('DGX')} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_8">
                                                                            <FontAwesomeIcon icon={faPaperPlane} /> {strings.SEND}
																		</button>
																		<button onClick={() => this.setState({ receiveModal: true, loading: true})} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_6">
                                                                            <FontAwesomeIcon icon={faQrcode} /> {strings.RECEIVE}
																		</button>
																	</div>
																</div>
															</div>
														</div>
                                                        
                                                        <div onClick={() => this.changeTab('GUSD')} className={classnames({
                                                            'col-md-12 accounts-wallet-border-bottom': true,
                                                            'accounts-active': this.state.activeTab === 'GUSD'
                                                        })}>

															<h4 className="m-widget24__title text-header accounts-header-wallets">
                                                                GUSD
															</h4>
                                                            <img style={{ float: "right", marginRight: "30px", width: "34px", marginTop: "-26px"}} src="../../assets/media/img/logo/gemini.png" alt="" />
															<div className="m-widget24">
																<div className="m-widget24__item">
																	<h4 className="m-widget24__title web-avaialable">
                                                                        {strings.AVAILABLE_BALANCE}
																	</h4>
																	<br />

																	<span className="m-widget24__stats m--font-info web-balance accounts-equalAmounts-container">
                                                                        { cryptoFormatterByCoin( this.state.GUSDAccount.Balance, 'GUSD' )} GUSD<br/>
                                                                        <span className="accounts-equalAmounts">&asymp; { currencyFormatter( amountFormatter(this.state.GUSDAccount.Balance * this.state.gusdEqualRates, this.state.activeTab) )} </span>
																	</span>

																	<br />
																	<div className="m--space-30"></div>

																</div>
																<div className="row">
																	<div className="col-md-12 wallet-button-container accounts-crypto-buttons">
																		<button onClick={() => this.openSendModal('GUSD')} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_8">
                                                                            <FontAwesomeIcon icon={faPaperPlane} /> {strings.SEND}
																		</button>
																		<button onClick={() => this.setState({ receiveModal: true, loading: true})} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_6">
                                                                            <FontAwesomeIcon icon={faQrcode} /> {strings.RECEIVE}
																		</button>
																	</div>
																</div>
															</div>
														</div>
                                                        <div onClick={() => this.changeTab('TUSD')} className={classnames({
                                                            'col-md-12 accounts-wallet-border-bottom': true,
                                                            'accounts-active': this.state.activeTab === 'TUSD'
                                                        })}>

															<h4 className="m-widget24__title text-header accounts-header-wallets">
                                                                TUSD
															</h4>
                                                            <img style={{ float: "right", marginRight: "30px", width: "34px", marginTop: "-26px"}} src="../../assets/media/img/logo/tusd.png" alt="" />
															<div className="m-widget24">
																<div className="m-widget24__item">
																	<h4 className="m-widget24__title web-avaialable">
                                                                        {strings.AVAILABLE_BALANCE}
																	</h4>
																	<br />

																	<span className="m-widget24__stats m--font-info web-balance accounts-equalAmounts-container">
                                                                        { cryptoFormatterByCoin( this.state.TUSDAccount.Balance, 'TUSD' )} TUSD<br/>
                                                                        <span className="accounts-equalAmounts">&asymp; { currencyFormatter( amountFormatter(this.state.TUSDAccount.Balance * this.state.tusdEqualRates, this.state.activeTab) ) } </span>
																	</span>

																	<br />
																	<div className="m--space-30"></div>

																</div>
																<div className="row">
																	<div className="col-md-12 wallet-button-container accounts-crypto-buttons">
																		<button onClick={() => this.openSendModal('TUSD')} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_8">
                                                                            <FontAwesomeIcon icon={faPaperPlane} /> {strings.SEND}
																		</button>
																		<button onClick={() => this.setState({ receiveModal: true, loading: true})} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_6">
                                                                            <FontAwesomeIcon icon={faQrcode} /> {strings.RECEIVE}
																		</button>
																	</div>
																</div>
															</div>
														</div>
                                                        <div onClick={() => this.changeTab('PAX')} className={classnames({
                                                            'col-md-12 accounts-wallet-border-bottom': true,
                                                            'accounts-active': this.state.activeTab === 'PAX'
                                                        })}>

															<h4 className="m-widget24__title text-header accounts-header-wallets">
                                                                PAX
															</h4>
                                                            <img style={{ float: "right", marginRight: "30px", width: "34px", marginTop: "-26px"}} src="../../assets/media/img/logo/pax.png" alt="" />
															<div className="m-widget24">
																<div className="m-widget24__item">
																	<h4 className="m-widget24__title web-avaialable">
                                                                        {strings.AVAILABLE_BALANCE}
																	</h4>
																	<br />

																	<span className="m-widget24__stats m--font-info web-balance accounts-equalAmounts-container">
                                                                        { cryptoFormatterByCoin( this.state.PAXAccount.Balance, 'PAX' )} PAX<br/>
                                                                        <span className="accounts-equalAmounts">&asymp; { currencyFormatter( amountFormatter(this.state.PAXAccount.Balance * this.state.paxEqualRates, this.state.activeTab) ) } </span>
																	</span>

																	<br />
																	<div className="m--space-30"></div>

																</div>
																<div className="row">
																	<div className="col-md-12 wallet-button-container accounts-crypto-buttons">
																		<button onClick={() => this.openSendModal('PAX')} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_8">
                                                                            <FontAwesomeIcon icon={faPaperPlane} /> {strings.SEND}
																		</button>
																		<button onClick={() => this.setState({ receiveModal: true, loading: true})} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_6">
                                                                            <FontAwesomeIcon icon={faQrcode} /> {strings.RECEIVE}
																		</button>
																	</div>
																</div>
															</div>
														</div>
                                                        <div onClick={() => this.changeTab('USDC')} className={classnames({
                                                            'col-md-12 accounts-wallet-border-bottom': true,
                                                            'accounts-active': this.state.activeTab === 'USDC'
                                                        })}>

															<h4 className="m-widget24__title text-header accounts-header-wallets">
                                                                USDC
															</h4>
                                                            <img style={{ float: "right", marginRight: "30px", width: "34px", marginTop: "-26px"}} src="../../assets/media/img/logo/usdc.png" alt="" />
															<div className="m-widget24">
																<div className="m-widget24__item">
																	<h4 className="m-widget24__title web-avaialable">
                                                                        {strings.AVAILABLE_BALANCE}
																	</h4>
																	<br />

																	<span className="m-widget24__stats m--font-info web-balance accounts-equalAmounts-container">
                                                                        { cryptoFormatterByCoin( this.state.USDCAccount.Balance, 'USDC' )} USDC<br/>
                                                                        <span className="accounts-equalAmounts">&asymp; { currencyFormatter( amountFormatter(this.state.PAXAccount.Balance * this.state.usdcEqualRates, this.state.activeTab) ) } </span>
																	</span>

																	<br />
																	<div className="m--space-30"></div>

																</div>
																<div className="row">
																	<div className="col-md-12 wallet-button-container accounts-crypto-buttons">
																		<button onClick={() => this.openSendModal('USDC')} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_8">
                                                                            <FontAwesomeIcon icon={faPaperPlane} /> {strings.SEND}
																		</button>
																		<button onClick={() => this.setState({ receiveModal: true, loading: true})} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_6">
                                                                            <FontAwesomeIcon icon={faQrcode} /> {strings.RECEIVE}
																		</button>
																	</div>
																</div>
															</div>
														</div>
                                                        <div onClick={() => this.changeTab('USDT')} className={classnames({
                                                            'col-md-12 accounts-wallet-border-bottom': true,
                                                            'accounts-active': this.state.activeTab === 'USDT'
                                                        })}>

															<h4 className="m-widget24__title text-header accounts-header-wallets">
                                                                USDT
															</h4>
                                                            <img style={{ float: "right", marginRight: "30px", width: "34px", marginTop: "-26px"}} src="../../assets/media/img/logo/usdt.png" alt="" />
															<div className="m-widget24">
																<div className="m-widget24__item">
																	<h4 className="m-widget24__title web-avaialable">
                                                                        {strings.AVAILABLE_BALANCE}
																	</h4>
																	<br />

																	<span className="m-widget24__stats m--font-info web-balance accounts-equalAmounts-container">
                                                                        { cryptoFormatterByCoin( this.state.USDTAccount.Balance, 'USDT' )} USDT<br/>
                                                                        <span className="accounts-equalAmounts">&asymp; { currencyFormatter( amountFormatter(this.state.USDCAccount.Balance * this.state.usdtEqualRates, this.state.activeTab) ) } </span>
																	</span>

																	<br />
																	<div className="m--space-30"></div>

																</div>
																<div className="row">
																	<div className="col-md-12 wallet-button-container accounts-crypto-buttons">
																		<button onClick={() => this.openSendModal('USDT')} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_8">
                                                                            <FontAwesomeIcon icon={faPaperPlane} /> {strings.SEND}
																		</button>
																		<button onClick={() => this.setState({ receiveModal: true, loading: true})} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_6">
                                                                            <FontAwesomeIcon icon={faQrcode} /> {strings.RECEIVE}
																		</button>
																	</div>
																</div>
															</div>
                                                        </div> 
                                                        <div onClick={() => this.changeTab('TTC')} className={classnames({
                                                            'col-md-12 accounts-wallet-border-bottom': true,
                                                            'accounts-active': this.state.activeTab === 'TTC'
                                                        })}>

                                                            <h4 className="m-widget24__title text-header accounts-header-wallets">
                                                                TTC
                                                            </h4>
                                                            <img style={{ float: "right", marginRight: "30px", width: "34px", marginTop: "-26px"}} src="../../assets/media/img/logo/ttc.png" alt="" />
                                                            <div className="m-widget24">
                                                                <div className="m-widget24__item">
                                                                    <h4 className="m-widget24__title web-avaialable">
                                                                        {strings.AVAILABLE_BALANCE}
                                                                    </h4>
                                                                    <br />

                                                                    <span className="m-widget24__stats m--font-info web-balance accounts-equalAmounts-container">
                                                                        { cryptoFormatterByCoin( this.state.TTCAccount.Balance, 'TTC' )} TTC<br/>
                                                                        <span className="accounts-equalAmounts">&asymp; { currencyFormatter( amountFormatter(this.state.TTCAccount.Balance * this.state.ttcEqualRates, this.state.activeTab) ) } </span>
                                                                    </span>

                                                                    <br />
                                                                    <div className="m--space-30"></div>

                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-md-12 wallet-button-container accounts-crypto-buttons">
                                                                        <button onClick={() => this.openSendModal('TTC')} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_8">
                                                                            <FontAwesomeIcon icon={faPaperPlane} /> {strings.SEND}
                                                                        </button>
                                                                        <button onClick={() => this.setState({ receiveModal: true, loading: true})} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_6">
                                                                            <FontAwesomeIcon icon={faQrcode} /> {strings.RECEIVE}
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div onClick={() => this.changeTab('SWIPE')} className={classnames({
                                                            'col-md-12 accounts-wallet-border-bottom': true,
                                                            'accounts-active': this.state.activeTab === 'SWIPE'
                                                        })}>

                                                            <h4 className="m-widget24__title text-header accounts-header-wallets">
                                                                SWIPE
                                                            </h4>
                                                            <img style={{ float: "right", marginRight: "30px", width: "34px", marginTop: "-26px"}} src="../../assets/media/img/logo/swipe.png" alt="" />
                                                            <div className="m-widget24">
                                                                <div className="m-widget24__item">
                                                                    <h4 className="m-widget24__title web-avaialable">
                                                                        {strings.AVAILABLE_BALANCE}
                                                                    </h4>
                                                                    <br />

                                                                    <span className="m-widget24__stats m--font-info web-balance accounts-equalAmounts-container">
                                                                        { cryptoFormatterByCoin( this.state.SWIPEAccount.Balance, 'SWIPE' )} SWIPE<br/>
                                                                        <span className="accounts-equalAmounts">&asymp; { currencyFormatter( amountFormatter(this.state.SWIPEAccount.Balance * this.state.swipeEqualRates, this.state.activeTab) ) } </span>
                                                                    </span>

                                                                    <br />
                                                                    <div className="m--space-30"></div>

                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-md-12 wallet-button-container accounts-crypto-buttons">
                                                                        <button onClick={() => this.openSendModal('SWIPE')} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_8">
                                                                            <FontAwesomeIcon icon={faPaperPlane} /> {strings.SEND}
                                                                        </button>
                                                                        <button onClick={() => this.setState({ receiveModal: true, loading: true})} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_6">
                                                                            <FontAwesomeIcon icon={faQrcode} /> {strings.RECEIVE}
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div onClick={() => this.changeTab('ZIL')} className={classnames({
                                                            'col-md-12 accounts-wallet-border-bottom': true,
                                                            'accounts-active': this.state.activeTab === 'ZIL'
                                                        })}>

                                                            <h4 className="m-widget24__title text-header accounts-header-wallets">
                                                                ZIL
                                                            </h4>
                                                            <img style={{ float: "right", marginRight: "30px", width: "34px", marginTop: "-26px"}} src="../../assets/media/img/logo/zil.png" alt="" />
                                                            <div className="m-widget24">
                                                                <div className="m-widget24__item">
                                                                    <h4 className="m-widget24__title web-avaialable">
                                                                        {strings.AVAILABLE_BALANCE}
                                                                    </h4>
                                                                    <br />

                                                                    <span className="m-widget24__stats m--font-info web-balance accounts-equalAmounts-container">
                                                                        { cryptoFormatterByCoin( this.state.ZILAccount.Balance, 'ZIL' )} ZIL<br/>
                                                                        <span className="accounts-equalAmounts">&asymp; { currencyFormatter( amountFormatter(this.state.ZILAccount.Balance * this.state.zilEqualRates, this.state.activeTab) ) } </span>
                                                                    </span>

                                                                    <br />
                                                                    <div className="m--space-30"></div>

                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-md-12 wallet-button-container accounts-crypto-buttons">
                                                                        <button onClick={() => this.openSendModal('ZIL')} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_8">
                                                                            <FontAwesomeIcon icon={faPaperPlane} /> {strings.SEND}
                                                                        </button>
                                                                        <button onClick={() => this.setState({ receiveModal: true, loading: true})} className="btn btn-outline-blue" type="button" data-toggle="modal" data-target="#m_modal_6">
                                                                            <FontAwesomeIcon icon={faQrcode} /> {strings.RECEIVE}
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
													</div>
												</div>
											</div>

										</div>

                                        <TransactionHistory
                                            ETHRate={this.state.ethEqualRates}
                                            BTCRate={this.state.btcEqualRates}
                                            XRPRate={this.state.xrpEqualRates}
                                            DGXRate={this.state.dgxEqualRates}
                                            BTCBalance={this.state.BTCAccount.Balance}
                                            ETHBalance={this.state.ETHAccount.Balance}
                                            XRPBalance={this.state.XRPAccount.Balance}
                                            DGXBalance={this.state.DGXAccount.Balance}
                                            GUSDBalance={this.state.GUSDAccount.Balance}
                                            TUSDBalance={this.state.TUSDAccount.Balance}
                                            USDTBalance={this.state.USDTAccount.Balance}
                                            PAXBalance={this.state.PAXAccount.Balance}
                                            USDCBalance={this.state.USDCAccount.Balance}
                                            SWIPEBalance={this.state.SWIPEAccount.Balance}
                                            ZILBalance={this.state.ZILAccount.Balance}
                                            currency={this.state.activeTab}
                                        />

									</div>
								</div>

							</div>
						</div>

                    </div>
                    </div>
                    </div>
                </div>

                <RecieveModal
                    show={this.state.receiveModal}
                    onClose={() => this.onCloseRecieveModal()}
                    currency={this.state.activeTab}
                    address={this.state.receiveAddress}
                    user={this.props.user}
                    loading={this.state.loading}
                />

                <SendModal
                    show={this.state.sendModal}
                    onClose={() => this.onCloseSendModal()}
                    currency={this.state.activeTab}
                    addresses={this.state.addresses}
                    user={this.props.user}
                    account={this.state.activeAccount}
                    onNewAddressCreated={() => this.fetchAddresses()}
                    onRefreshBalances={(currency) => this.fetchAccounts(currency)}
                />

                <DepositModal
                    show={this.state.depositModal}
                    toggleShow={() => this.setState({ depositModal: !this.state.depositModal})}
                 />

                 <WithdrawalModal
                    accountBalanceIdr = {this.state.IDRAccount.Balance}
                    availableBalanceIdr = {this.state.IDRAccount.UsableBalance}
                    idrUsageDayPrecentage = {this.state.IDRUsage}
					idrUsageDayText = {this.state.IDRUsageText}
					idrDailyLimit = {this.state.IDRLimit}
                    show={this.state.withdrawalModal}
                    toggleShow={() => this.setState({ withdrawalModal: !this.state.withdrawalModal})}
                    onRefreshBalances={(currency) => this.fetchAccounts(currency)}
                 />
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        trading: state.trading,
        language: state.language
    };
};

export default connect(mapStateToProps, null)(AccountsPage);
