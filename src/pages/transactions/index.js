/**
 * Filename: home.js
 * Home page
 * Component path
 * IconBoxed  'componenet/iconboxes.js'
 * Charts compoenent from 'compoenent/charts'
 * Table from 'containers/table.js'
 */

import React from 'react';
import { List, ListItem } from 'material-ui/List';
import { getWalletDetail, getTranscation } from '../../core/actions/client/transactions';
import { connect } from 'react-redux';
import { Loader } from 'react-loaders'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { NotificationManager } from 'react-notifications';
import Panel from '../../components/panel.js';
import NetworkError from '../../errors/networkError';
export const BoxSingle = (props) => {
    // Used a style const for changing icon box
    // color using props.
    const style = {
        backgroundColor: props.iconbg
    }


    return (
        <div className="iconbox-single">
            <div className="box-title">
                <div className="circle-icon" style={style}>
                    <i className="material-icons">{props.icon}</i>
                </div>
                <div>
                    <h5>{props.coin}</h5>
                    <p>{props.label}</p>
                </div>
            </div>

            <div>
                <h5>Balance</h5>
                <p>{props.balance} {props.coin}</p>
            </div>
        </div>
    );
}

class Send extends React.Component {

    constructor(props, context) {
        super(props);

        this.state = {
            wallet: null,
            addresses: [],
            loading: true,
            sendingCoin: false,
            transactions: [],
            address: '',
            amount: 0,

        }

    }

    componentWillMount() {
        let self = this;
        this.props.user.getSession((err, session) => {
            getWalletDetail(session.idToken.jwtToken, this.props.match.params.coin).then(function (result) {
                self.setState({
                    addresses: result.data.addresses,
                    wallet: result.data,
                    loading: false
                })

            })
                .catch(function (error) {
                    NetworkError(error);
                });

            getTranscation(session.idToken.jwtToken, this.props.match.params.coin).then(function (result) {
                let transactions = result.data.transactions.reduce(function (result, value) {
                    var addressTrx = value.entries.filter(function (item) {
                        return item.wallet;
                    });

                    result.push({
                        id: value.id,
                        addressTrx: {
                            address: addressTrx[0].address,
                            value: (self.props.match.params.coin == 'teth') ? Number(addressTrx[0].value) / 1e18 : Number(addressTrx[0].value) / 1e8
                        }
                    })
                    return result;
                }, []);
                self.setState({ transactions: transactions });
            })
                .catch(function (error) {
                    NetworkError(error);
                    
                    
                    
                    
                    
                    // NetworkError(err);
                });

        });

    }




    render() {
        return (
            <div>
                <Loader type="line-scale" active={this.state.loading} className="text-center" />
                {this.state.wallet != null &&

                    <div className="row">
                        <div className="col-md-6">
                            <BoxSingle
                                iconbg="#40c741"
                                icon="monetization_on"
                                coin={this.state.wallet.coin}
                                label={this.state.wallet.label}
                                balance={this.state.wallet.balance}
                            />
                        </div>
                        <div className="col-md-6">
                            <Panel title="Address">
                                {this.state.addresses.map((item, index) =>
                                    <div key={index} >
                                        <ListItem primaryText={item} onClick={() => {

                                        }} />
                                    </div>
                                )}
                            </Panel>
                        </div>
                        <div className="col-md-12">
                            <Panel title="Transcations">
                                {this.state.transactions.map((item, index) =>
                                    <div key={index} >
                                        <ListItem secondaryText={"Transaction Id = " + item.id} primaryText={item.addressTrx.address + "  =  " + item.addressTrx.value + " " + this.props.match.params.coin} onClick={(event) => {
                                            event.preventDefault();
                                            if (this.props.match.params.coin == 'teth') {
                                                window.open('https://kovan.etherscan.io/tx/' + item.id);
                                            } else if (this.props.match.params.coin == 'tbtc') {
                                                window.open('https://www.blocktrail.com/tBTC/tx/' + item.id);
                                            }
                                        }} />
                                    </div>
                                )}
                            </Panel>
                        </div>
                    </div>

                }


            </div>
        );
    }
}



const mapStateToProps = (state) => {
    return {
        user: state.cognito.user
    };
};

const mapDispatchToProps = dispatch => ({

});


/**
 * Container for login behaviour, wrapping a login form.
 *
 * Magically provides the following props to the wrapped form:
 *
 *  * username
 *  * onSubmit
 *
 * @example
 * <Login>
 *   <LoginForm />
 * </Login>
 */


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Send);
