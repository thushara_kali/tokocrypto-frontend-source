import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import { NotificationManager } from "react-notifications";

import FeaturedICO from "../../components/launchpad/featured";
import OnGoingICO from "../../components/launchpad/ongoing";
import CompletedICO from "../../components/launchpad/completed";
import ParticipationICO from "../../components/launchpad/participation";
import LaunchpadInfo from "../../components/launchpad/info";
import LaunchpadService from "../../core/actions/client/launchpad";
import { getUserAttributes } from "../../core/react-cognito/attributes.js";
import LocalizedStrings from "react-localization";
import enDictionary from "../../languages/en.json";
import idDictionary from "../../languages/id.json";
import javaDictionary from "../../languages/java.json";
import sundaDictionary from "../../languages/sunda.json";
import EditorFormatColorReset from "material-ui/SvgIcon";
import classnames from 'classnames';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Cookies from 'universal-cookie';
import { Loader } from '../../components/tcLoader';
import _ from 'lodash';
import NetworkError from '../../errors/networkError';
import dashboardService from '../../core/actions/client/home';
import EventBus from 'eventing-bus';
import ReactGA from 'react-ga';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

/**
 * TCDOC-042 Launchpad main dashboard
 */

class Launchpad extends Component {
  constructor() {
    super();
    this.state = {
      featured: [],
      ongoing: [],
      completed: [],

      ticketProjectName: "",
      ticketMessage: "",
      sendingTicket: false,

      info: "",
      requestProjectName: "",
      requestProjectWebsite: "",
      sendingRequest: false,
      requestSubmit: false,
      ticketSubmit: false,

      openTNC: false,
      loadingFeatured: false,
      loadingOngoing: false,
      loadingCompleted: false,

      BTCBalance: 0,
      ETHBalance: 0,
      USDTBalance: 0,
      USDCBalance: 0,
      IDRBalance: 0,

      myPledge: [],
      myCompletedContribution: []
    };
    
  }

  callGA(){
    ReactGA.initialize('UA-116825757-2');
    ReactGA.pageview(window.location.pathname + window.location.search);
  }

  fetchCompletedProjects() {
    this.setState({
      loadingCompleted: true
    });
    LaunchpadService.getCompletedProjects(this.getToken()).then(res => {
      this.setState({
        completed: res.data.data,
        loadingCompleted: false
      })
    }).catch(err => {
      // NotificationManager.error("Failed to get completed projects");
      this.setState({
        loadingCompleted: false
      });
      NetworkError(err);
    });
  }

  sendProjectRequest() {
    this.setState({
      requestSubmit: true
    });

    let valid =
      this.state.requestProjectName.length !== 0 &&
      this.state.requestProjectWebsite.length !== 0;

    if (valid) {
      this.setState({ sendingRequest: true });
      let data = {
        projectName: this.state.requestProjectName,
        website: this.state.requestProjectWebsite,
        info: this.state.info,
        status: "draft"
      };

      LaunchpadService.submitProjectRequest(this.getToken(), data)
        .then(() => {
          NotificationManager.success("Request sent.");
          this.setState({
            sendingRequest: false,
            info: "",
            requestProjectName: "",
            requestProjectWebsite: "",
            requestSubmit: false
          });
        })
        .catch(err => {
          // NetworkError(err);
          // NotificationManager.error("Failed to send request");
          this.setState({ sEditorFormatColorReset: false });
        });
    } else {
      NotificationManager.error("Please fill the required fields.");
    }
  }

  openQnA() {
    window.open(
      "https://support.tokocrypto.com/hc/en-us/sections/360001122352-FAQ",
      "_blank"
    );
  }

  getToken() {
    if(this.props.user){
      let username = this.props.user.username;
      let clientId = this.props.user.pool.clientId;
      let token = this.props.user.storage[
        "CognitoIdentityServiceProvider." + clientId + "." + username + ".idToken"
      ];
      return token;
    }else{
      return 0
    }
  }

  getFeatured() {
    this.setState({
      loadingFeatured: true
    });

    let self = this;
    LaunchpadService.getFeaturedDraftProjects(this.getToken())
      .then(res => {
        
        this.setState({
          featured: res.data.data,
          loadingFeatured: false
        });

        LaunchpadService.getFeaturedProjects(self.getToken()).then(res2 => {
          let current = self.state.featured;
          let allFeatured = _.concat(current, res2.data.data);

          this.setState({
            featured: allFeatured,
            loadingFeatured: false
          });
        }).catch(err2 => {
          // NetworkError(err2);
          this.setState({
            loadingFeatured: false
          });
        })
      })
      .catch(err => {
        // NetworkError(err);
        this.setState({
          loadingFeatured: false
        });
      });
  }

  getOngoing() {
    this.setState({
      loadingOngoing: true
    });
    let today = moment()
      .format("YYYY-MM-DD");

    LaunchpadService.getOngoingProject(this.getToken(), today)
      .then(res => {
        
        this.setState({
          ongoing: res.data.data,
          loadingOngoing: false
        });
      })
      .catch(err => {
        // NetworkError(err);
        this.setState({
          loadingOngoing: false
        });
      });
  }

  /**
   * TCDOC-043 fetch all projects for launchpad based on their status
   */
  componentDidMount() {
    this.callGA();
    this.getFeatured();
    this.getOngoing();
    this.fetchCompletedProjects();
    this.fetchDashboard();

    let self = this;

    /**
     * TCDOC-044 here you will find event bus for watching success contribution and plegde join
     * after those event we need to refresh user balance because they have successfully pledging
     */
    EventBus.on("contributeSuccess", () => {
      self.fetchDashboard();
    });

    EventBus.on("joinPledge", () => {
      self.fetchDashboard();
    });

    /**
     * TCDOC-045 flag for tnc to be appreared or not by checking cookie value
     */
    const cookies = new Cookies();
    let approved = cookies.get('TC_LAUNCHPAD_APPROVED');
    
    if (approved === undefined || approved !== 'true') {
      this.setState({
        openTNC: true
      });
    }
  }

  agreeTNC() {
    this.setState({ openTNC: false });
    const cks = new Cookies();
    cks.set('TC_LAUNCHPAD_APPROVED', 'true');
  }

  /**
  * TCDOC-046 this function is for user to create ticket for ask or get more info they need from particular project
  */  
  createProjectTicket() {
    let data = {};
    this.setState({
      ticketSubmit: true
    });

    let valid = this.state.ticketProjectName.length !== 0 && this.state.ticketMessage.length !== 0;

    if (valid) {
      this.setState({
        sendingTicket: true
      });

      getUserAttributes(this.props.user)
        .then(attr => {
          data = {
            name: attr.name,
            email: this.props.user.username,
            project_name: this.state.ticketProjectName,
            message: this.state.ticketMessage
          };

          LaunchpadService.createTicket(this.getToken(), data)
            .then(res => {
              NotificationManager.success(strings.TICKET_SUBMITTED);
              this.setState({
                ticketProjectName: "",
                ticketMessage: "",
                sendingTicket: false,
                ticketSubmit: false
              });
            })
            .catch(err => {
              // NotificationManager.error(
              //   "Error submitting ticket, please try again later."
              // );
              NetworkError(err);
              this.setState({
                ticketProjectName: "",
                ticketMessage: "",
                sendingTicket: false,
                ticketSubmit: false
              });
            });
        })
        .catch(err => {
          NetworkError(err);
          
        });
    } else {
      NotificationManager.error("Please fill the required fields.");
    }
  }

  /**
  * TCDOC-047 function for fetch user balance used to validate if user can pledge or not
  */  
  fetchDashboard() {
    // let username = this.props.user.username;
    // let clientId = this.props.user.pool.clientId;
    // let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];

    dashboardService.fetchDashboard(this.getToken()).then(res => {

      let btc = _.find(res.data.accounts, { CurrencyCode: "BTC" });
      let eth = _.find(res.data.accounts, { CurrencyCode: "ETH" });
      let usdt = _.find(res.data.accounts, { CurrencyCode: "USDT" });
      let usdc = _.find(res.data.accounts, { CurrencyCode: "USDC" });
      let idr = _.find(res.data.accounts, { CurrencyCode: "IDR" });

      this.setState({
        BTCBalance: btc.Balance,
        ETHBalance: eth.Balance,
        USDTBalance: usdt.Balance,
        USDCBalance: usdc.Balance,
        IDRBalance: idr.Balance
      });
    }).catch(err => {
      if(this.props.user){
        // NetworkError(err);
      }
    });
  }

  render() {

    let allProjects = [];

    this.state.completed.map(c => {
      allProjects.push(c.projectName);
    });

    this.state.ongoing.map(c => {
      allProjects.push(c.projectName);
    });

    this.state.featured.map(c => {
      allProjects.push(c.projectName);
    });

    let alluProjects = _.uniq(allProjects);

    return (
      <div>
        <div className="container" style={{ marginTop: "10%" }}>
          <h4 className="blue-tc">Featured ICO</h4>
          <br />

          <div className="featured-ico">
            <div className="row">
              {this.state.featured.map(f => {
                return (
                  <div className="col-lg-4 col-12">
                    <FeaturedICO project={f} />
                  </div>
                );
              })}
              {this.state.loadingFeatured && <div style={{ marginLeft: 'auto', marginRight: 'auto' }}>
                <Loader type="line-scale" active={true} className="text-center" />
              </div>}
            </div>
          </div>

          <div className="row">
            <div className="col-lg-9 col-12">
              <br />
              <br />
              <h4 className="blue-tc">On Going Project</h4>
              <br />

              <div className="ongoing-ico">
                <div className="row">
                  {this.state.ongoing.map(o => {
                    return (
                      <div className="col-lg-4 col-12">
                        <OnGoingICO 
                          token={this.getToken()} 
                          project={o} 
                          btcBalance={this.state.BTCBalance} 
                          ethBalance={this.state.ETHBalance} 
                          usdtBalance={this.state.USDTBalance} 
                          usdcBalance={this.state.USDCBalance} 
                          idrBalance={this.state.IDRBalance}/>
                      </div>
                    );
                  })}
                  {this.state.loadingOngoing && <div style={{ marginLeft: 'auto', marginRight: 'auto' }}>
                    <Loader type="line-scale" active={true} className="text-center" />
                  </div>}
                </div>
              </div>

              <br />
              <br />
              <h4 className="blue-tc">Completed Project</h4>
              <br />

              <div className="completed-ico">
                <div className="row">
                  {this.state.completed.map(c => {
                    return (
                      <div className="col-lg-4 col-12">
                        <CompletedICO project={c} />
                      </div>
                    );
                  })}
                  {this.state.loadingCompleted && <div style={{ marginLeft: 'auto', marginRight: 'auto' }}>
                    <Loader type="line-scale" active={true} className="text-center" />
                  </div>}
                </div>
              </div>

              <br />
              <br />
              <h4 className="blue-tc">Your Participation</h4>
              <br />

              <ParticipationICO
                ongoing={this.state.ongoing}
                completed={this.state.completed}
                btcBalance={this.state.BTCBalance}
                ethBalance={this.state.ETHBalance}
                usdtBalance={this.state.USDTBalance} 
                usdcBalance={this.state.USDCBalance} 
                idrBalance={this.state.IDRBalance}
              />

              <br />
              <br />
              <h4 className="blue-tc">More Information</h4>
              <br />

              <LaunchpadInfo />
            </div>

            {/* sidebar  */}
            <div className="col-lg-3 col-12">
              <br />
              <br />
              <div className="m-portlet">
                <div className="m-portlet__head bg-gray">
                  <div className="m-portlet__head-caption">
                    <div className="m-portlet__head-title">
                      <h3 className="m-portlet__head-text">Project</h3>
                    </div>
                  </div>
                </div>
                <div className="m-portlet__body">
                  <p>
                    You can request for project, tokocrypto will review, assess
                    and engage with the project
                  </p>
                  <div className={
                    classnames({
                      "form-group": true,
                      "has-danger": this.state.requestSubmit && this.state.requestProjectName.length === 0
                    })
                  }>
                    <label className="control-label bold">Name of ICO</label>
                    <input
                      disabled={this.state.sendingRequest}
                      value={this.state.requestProjectName}
                      onChange={e =>
                        this.setState({ requestProjectName: e.target.value })
                      }
                      className="form-control"
                    />
                    {this.state.requestSubmit && this.state.requestProjectName.length === 0 && <p className="text-danger">this field required</p>}
                  </div>
                  <div className={
                    classnames({
                      "form-group": true,
                      "has-danger": this.state.requestSubmit && this.state.requestProjectWebsite.length === 0
                    })
                  }>
                    <label className="control-label bold">Website</label>
                    <input
                      disabled={this.state.sendingRequest}
                      value={this.state.requestProjectWebsite}
                      onChange={e =>
                        this.setState({ requestProjectWebsite: e.target.value })
                      }
                      className="form-control"
                    />
                    {this.state.requestSubmit && this.state.requestProjectWebsite.length === 0 && <p className="text-danger">this field required</p>}
                  </div>
                  <div className="form-group">
                    <label className="control-label bold">
                      Additional Information
                    </label>
                    <textarea
                      disabled={this.state.sendingRequest}
                      value={this.state.info}
                      onChange={e => this.setState({ info: e.target.value })}
                      className="form-control"
                    />
                  </div>

                  <button
                    onClick={() => this.sendProjectRequest()}
                    disabled={this.state.sendingRequest}
                    className="btn m-btn--pill btn-sm btn-success btn-block"
                  >
                    {this.state.sendingRequest
                      ? "Submitting..."
                      : "Submit Request"}
                  </button>
                </div>
              </div>

              <div className="m-portlet">
                <div className="m-portlet__head bg-gray">
                  <div className="m-portlet__head-caption">
                    <div className="m-portlet__head-title">
                      <h3 className="m-portlet__head-text">Ask the Teams</h3>
                    </div>
                  </div>
                </div>
                <div className="m-portlet__body">
                  <p>
                    Submit your question, we will conduct Q&A session with the
                    project teams
                  </p>
                  <div className={
                    classnames({
                      "form-group": true,
                      "has-danger": this.state.ticketSubmit && this.state.ticketProjectName.length === 0
                    })
                  }>
                    <label className="control-label bold">Name of ICO</label>
                    <select
                      disabled={this.state.sendingTicket}
                      value={this.state.ticketProjectName}
                      className="form-control"
                      onChange={e =>
                        this.setState({ ticketProjectName: e.target.value })
                      }
                    >
                      <option />
                      {alluProjects.map(c => {
                        return <option>{c}</option>;
                      })}
                    </select>
                    {this.state.ticketSubmit && this.state.ticketProjectName.length === 0 && <p className="text-danger">this field required</p>}
                  </div>
                  <div className={
                    classnames({
                      "form-group": true,
                      "has-danger": this.state.ticketSubmit && this.state.ticketMessage.length === 0
                    })
                  }>
                    <label className="control-label bold">Question</label>
                    <textarea
                      disabled={this.state.sendingTicket}
                      value={this.state.ticketMessage}
                      className="form-control"
                      onChange={e =>
                        this.setState({ ticketMessage: e.target.value })
                      }
                    />
                    {this.state.ticketSubmit && this.state.ticketMessage.length === 0 && <p className="text-danger">this field required</p>}
                  </div>

                  <button
                    disabled={this.state.sendingTicket}
                    onClick={() => this.createProjectTicket()}
                    className="btn m-btn--pill btn-sm btn-success btn-block"
                  >
                    {this.state.sendingTicket
                      ? "Submitting..."
                      : "Submit Request"}
                  </button>
                </div>
              </div>

              <div className="m-portlet">
                <div className="m-portlet__head bg-gray">
                  <div className="m-portlet__head-caption">
                    <div className="m-portlet__head-title">
                      <h3 className="m-portlet__head-text">About Launchpad</h3>
                    </div>
                  </div>
                </div>
                <div className="m-portlet__body">
                  <p>
                    Everything you want to know about tokocrypto launchpad and
                    related informations
                  </p>
                  <button
                    onClick={() => this.openQnA()}
                    className="btn btn-outline-info"
                  >
                    More Info
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* TNC modal */}
        <Modal isOpen={this.state.openTNC} toggle={() => this.setState({ openTNC: false })} className="">
          <ModalHeader toggle={() => this.setState({ openTNC: false })}>Toko Launchpad</ModalHeader>
          <ModalBody>
            <p>Toko Launchpad is a platform from Tokocrypto for our users to contribute into intersting blockchain based projects. Please read our terms and conditions first before continue.</p>
            <br />
            <a href="/Toko_Launchpad_T&C.pdf" target="_blank">see terms and conditions</a>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-success" onClick={() => this.agreeTNC()}>I agree</button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.cognito.user,
    language: state.language
  };
};

export default connect(
  mapStateToProps,
  null
)(Launchpad);
