import React, { Component } from "react";
import moment from "moment";
import classnames from "classnames";
import { TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap";
import { connect } from "react-redux";
import renderHTML from "react-render-html";
import randomColor from "randomcolor";
import { PieChart, Pie, Sector, Cell, Tooltip } from "recharts";
import { NotificationManager } from 'react-notifications';
import EventBus from 'eventing-bus';
import uuid from 'uuid/v1';
import { confirmAlert } from 'react-confirm-alert'; // Import
import LaunchpadService from "../../core/actions/client/launchpad";
import history from "../../history";
import dashboardService from '../../core/actions/client/home';
import _ from 'lodash';
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
import ReactTooltip from 'react-tooltip';
import NetworkError from '../../errors/networkError';

const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];
const RADIAN = Math.PI / 180;

/**
 * TCDOC-054 this component to render project review by admin. all content provided by admin, and just put it here
 */

const CustomTooltip = React.createClass({
  render() {
    const { active } = this.props;

    if (active) {
      const { payload, label } = this.props;
      return (
        <div
          className="custom-tooltip"
          style={{
            backgroundColor: "white",
            padding: 10,
            border: "solid 1px black"
          }}
        >
          <p className="label">{`${payload[0].label} : ${
            payload[0].value
          }%`}</p>
        </div>
      );
    }

    return null;
  }
});

class ReviewICO extends Component {
  constructor() {
    super();
    this.state = {
      activeTab: "1",
      amount: "",
      review: {
        projectSummary: "",
        teamList: [],
        positive: [],
        negative: [],
        team: "",
        conclusion: {
          team: 0,
          investmentRating: 0,
          riskScore: 0,
          productScore: 0,
          totalScore: 0
        },
        product: "",
        tokenSales: "",
        qna: [],
        charts: []
      },
      project: {
        projectName: "",
        logo:
          "https://cdn2.vectorstock.com/i/thumb-large/23/81/default-avatar-profile-icon-vector-18942381.jpg",
        contributionEndDate: ""
      },
      usedCurrency: "",
      BTCBalance: 0,
      ETHBalance: 0,
      USDTBalance: 0,
      USDCBalance: 0,
      IDRBalance: 0,
    };

    this.handleChange = (fieldName) => (event) => {
      let obj = Object.assign({})
      var re
      if(this.state.usedCurrency === 'BTC'){
          re = /^(\d+(\.\d{0,3})?)$/;
      }else if(this.state.usedCurrency === 'USDT'){
          re = /^[1-9]\d*$/;
      }else if(this.state.usedCurrency === 'USDC'){
          re = /^[1-9]\d*$/;
      }else if(this.state.usedCurrency === 'IDR'){
          re = /^[1-9]\d*$/;
      }else{
          re = /^(\d+(\.\d{0,2})?)$/;
      } 
  
      if (event.target.value == '' || re.test(event.target.value)) {
          obj[fieldName] = event.target.value
      }

      this.setState(
          obj
      )
    }
  }

  toggle(activeTab) {
    this.setState({
      activeTab: activeTab
    });
  }

  onChangeCurrency(e) {
    
    this.setState({
        usedCurrency: e.target.value
    });
  }

  getToken() {
    if(this.props.user){
      let username = this.props.user.username;
      let clientId = this.props.user.pool.clientId;
      let token = this.props.user.storage[
        "CognitoIdentityServiceProvider." + clientId + "." + username + ".idToken"
      ];
      return token;
    }else{
      return 0
    }
  }

  getReview(id) {
    LaunchpadService.getReview(this.getToken(), id)
      .then(res => {

        let review = res.data.data;
        if (!review.charts) {
            review.charts = []
        }

        this.setState({
          review
        });

        if(history.location.search === "?qna=true"){
          setTimeout(() => {
            window.location = "#products";
          }, 500);
        }
      })
      .catch((err) => {
        history.goBack();
        NetworkError.handle(err);
        // NotificationManager.error('Review not found');
      });
  }

  getProject(id) {
    LaunchpadService.getProject(this.getToken(), id)
      .then(res => {

        let project = res.data.data;

        this.setState({
          project
        });

        this.addUsedCurrency();
        
      })
      .catch(err => {
        NetworkError.handle(err);
        
      });
  }

  addUsedCurrency(){
    this.setState({
      usedCurrency: this.state.project.currency[0]
    })
  }

  onChangeNavigation(element_id) {
    
    if (element_id === "team") {
      this.toggle("2");
      window.location = "#products";
    } else {
      window.location = "#" + element_id;
    }
  }

  fetchDashboard() {
    // let username = this.props.user.username;
    // let clientId = this.props.user.pool.clientId;
    // let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];

    dashboardService.fetchDashboard(this.getToken()).then(res => {

      let btc = _.find(res.data.accounts, { CurrencyCode: "BTC" });
      let eth = _.find(res.data.accounts, { CurrencyCode: "ETH" });
      let usdt = _.find(res.data.accounts, { CurrencyCode: "USDT" });
      let usdc = _.find(res.data.accounts, { CurrencyCode: "USDC" });
      let idr = _.find(res.data.accounts, { CurrencyCode: "IDR" });

      this.setState({
        btcBalance: btc.Balance,
        ethBalance: eth.Balance,
        usdtBalance: usdt.Balance,
        usdcBalance: usdc.Balance,
        idrBalance: idr.Balance
      });
    }).catch(err => {
      NetworkError.handle(err);
    });
  }

  openLogin() {
    // window.open(this.props.project.website, '_blank');
    history.push('/login');
  }

  doPledge() {
    let balance = 0;
    let valid = false;
    let minimum = 0;

    if (this.state.usedCurrency === 'BTC') {
        balance = this.state.btcBalance;
        minimum = 0.001
    } else if(this.state.usedCurrency === 'ETH') {
        balance = this.state.ethBalance;
        minimum = 0.01
    } else if(this.state.usedCurrency === 'USDT') {
        balance = this.state.usdtBalance;
        minimum = 10
    } else if(this.state.usedCurrency === 'USDC') {
        balance = this.state.usdcBalance;
        minimum = 10
    } else if(this.state.usedCurrency === 'IDR') {
        balance = this.state.idrBalance;
        minimum = 200000
    }

    

    if ((Number(this.state.amount) * (10 / 100)) > balance) {
        NotificationManager.error('At least have 10% balance from pledged amount');
        valid = false;
    } else {
        if (Number(this.state.amount) < minimum) {
            valid = false;
            NotificationManager.error(`Minimum pledged amount is ${minimum} ${this.state.usedCurrency}`);
        } else {
            valid = true;
        }
    }

    if (valid) {
      this.setState({
          loading: true
      });

      let self = this;

      confirmAlert({
        title: 'Submit Pledge',
        message: `Are you sure to pledge ${Number(this.state.amount)} ${this.state.usedCurrency} in project ${this.state.project.projectName}`,
        // buttons: [
        //     {
        //         label: 'Yes',
        //         onClick: () => self.pledge()
        //     },
        //     {
        //         label: 'No',
        //         onClick: () => this.setState({
        //             loading: false
        //         })
        //     }
        // ],
        customUI: ({ title, message, onClose }) => 
          <div className="react-confirm-alert">
            <div className="react-confirm-alert-body">
              <h1>{title}</h1><span style={{fontFamily:"Roboto"}}>{message}</span>
              <div className="react-confirm-alert-button-group">
                  <button onClick={() => {
                      self.pledge();
                      onClose()
                  }}>Yes</button>
                  <button onClick={() => {
                        this.setState({loading: false})
                      onClose()
                  }}>No</button>
              </div>
              <div style={{fontWeight:"600", fontFamily:"Roboto", textDecoration:"underline", fontSize:"11px", marginTop:"12px", cursor:"pointer"}} data-tip data-for='agreement_note'>
                  Agreement note 
              </div>
              <ReactTooltip data-border='true' id="agreement_note" place="bottom" effect="solid">
                  <span style={{fontFamily:"Roboto"}}>10% of what User’s pledged will be deducted from User’s account as deposit.<br/> Deposit will be returned to User once User has contributed as per pledged</span>
              </ReactTooltip>
            </div>
          </div>
        })
      }
  }

  componentDidMount() {
    let id = history.location.pathname.split("/launchpad/review/")[1];
    this.getReview(id);
    this.getProject(id);
    this.fetchDashboard();
    
    if(history.location.search === "?qna=true"){
      window.location = "#products";
      this.setState({
        activeTab: "3"
      })
    }
  }

  renderRating(averageScore) {
    if (averageScore <= 20 && averageScore !== 0) {
      return "Very Low"
    } else if (averageScore > 20 && averageScore <= 40) {
      return "Low"
    } else if (averageScore > 40 && averageScore <= 60) {
      return "Medium"
    } else if (averageScore > 60 && averageScore <= 80) {
      return "High"
    } else if (averageScore > 80 && averageScore <= 100) {
      return "Very High"
    } else {
      return "-"
    }
  }

  pledge() {
    LaunchpadService.pledgeProject(this.getToken(), this.state.project.projectId, Number(this.state.amount), this.state.usedCurrency).then(res => {
        NotificationManager.success('Pledge success, you can view your pledge history under “Your Participation”');
        EventBus.publish("joinPledge");
        this.setState({
            loading: false,
            amount: ""
        });
    }).catch(err => {
        NetworkError.handle(err);
        // if (err.response.data) {
        //     NotificationManager.error(err.response.data.error.message);
        // } else {
        //     NotificationManager.error(err.toString());
        // }
        this.setState({
            loading: false,
            amount: ""
        });
    });
  }

  renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
    index
  }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text
        style={{ fontSize: "8px", border: "solid 1px black" }}
        x={x}
        y={y}
        fill="white"
        textAnchor={x > cx ? "start" : "end"}
        dominantBaseline="central"
      >
        {`${this.state.review.charts[index].label}`}
      </text>
    );
  };

  render() {

    let expired = moment(this.state.project.pledgeEndDate).diff(moment(), 'days') < 0;

    let minimum = 0;
    if (this.state.usedCurrency === 'BTC') {
        minimum = 0.001;
    } else if(this.state.usedCurrency === 'ETH') {
        minimum = 0.01;
    } else if(this.state.usedCurrency === 'USDT') {
        minimum = 10;
    } else if(this.state.usedCurrency === 'USDC') {
        minimum = 10;
    } else if(this.state.usedCurrency === 'IDR') {
        minimum = 200000;
    }

    var full_list = "";
    let currecyList = this.state.project.currency
    if(currecyList){
      for(var i=0; i<currecyList.length; ++i){
        full_list = full_list + currecyList[i]+ "  ";
      } 
    }

    var website = this.state.project.website
   
    return (
      <div>
        <div className="container" style={{ marginTop: "10%" }}>
          <label>/ Launchpad / {this.state.project.projectName} / Review</label>
          <br />
          <br />
          <div className="row">
            <div className="col-lg-8">
              <div className="row">
                <div className="col-lg-2">
                  <img
                    style={{ width: 50, height: 50 }}
                    src={this.state.project.logo}
                  />
                </div>
                <div className="col-lg-8">
                  <h5>{this.state.project.projectName}</h5>
                  {/* <small style={{backgroundColor:"#f0f047",padding:"7px 10px 7px 10px", borderRadius:"10px"}}>
                    <strong>Pre-IEO period</strong>{" "}
                    {moment(this.state.project.contributionEndDate, "YYYY-MM-DD").format("YYYY-MM-DD")}
                  </small> */}
                  {/* <br /> */}
                  <small style={{display:"block",marginTop:"10px"}}>
                    {/* {moment(this.state.project.contributionEndDate, "YYYY-MM-DD").fromNow()} */}
                    Supported currencies: {full_list}
                  </small>
                </div>
                <div className="col-lg-2">
                  <span className="launchpad-review-category">Application</span><br/>
                  <a href={website} target="_blank"><span style={{backgroundColor:"#FFC107"}} className="launchpad-review-category">Website</span></a><br/>
                  {/* <a href="/launchpad"><span className="launchpad-review-category" style={{backgroundColor:"#1AA74A"}}>Join</span></a> */}
                </div>
                
              </div>

              <br />
              <br />
              <div className="row">
                <div className="container">
                  <select
                    className="form-control"
                    onChange={e => this.onChangeNavigation(e.target.value)}
                  >
                    <option disabled selected>
                      Quick Navigation
                    </option>
                    <option value="project-summary">Project Summary</option>
                    { this.state.review.tokenSales.length > 8 && <option value="token-sales">Token Sales</option> }
                    <option value="products">Product</option>
                    <option value="team">Team</option>
                    <option value="positive-negative">Positive Negative</option>
                    <option value="conclusion">Conclusion</option>
                  </select>
                </div>
              </div>

              <br />
              <br />
              <div className="m-portlet" id="project-summary">
                <div className="m-portlet__head bg-gray">
                  <div className="m-portlet__head-caption">
                    <div className="m-portlet__head-title">
                      <h3 className="m-portlet__head-text">Project Summary</h3>
                    </div>
                  </div>
                </div>
                <div className="m-portlet__body">
                  {renderHTML(this.state.review.projectSummary)}
                </div>
              </div>

              <br />
              <br />
              { this.state.review.tokenSales.length > 8 && <div className="m-portlet" id="token-sales">
                <div className="m-portlet__head bg-gray">
                  <div className="m-portlet__head-caption">
                    <div className="m-portlet__head-title">
                      <h3 className="m-portlet__head-text">Token Sales</h3>
                    </div>
                  </div>
                </div>
                <div className="m-portlet__body">
                  {renderHTML(this.state.review.tokenSales)}
                  <br />
                  { this.state.review.charts.length !== 0 && <div className="row">
                    <div className="col-lg-6 col-12">
                      <PieChart
                        width={800}
                        height={200}
                        // onMouseEnter={this.onPieEnter}
                      >
                        <Pie
                          data={this.state.review.charts}
                          cx={100}
                          cy={100}
                          labelLine={false}
                          label={this.renderCustomizedLabel}
                          outerRadius={80}
                          fill="#8884d8"
                        >
                          {this.state.review.charts.map((entry, index) => (
                            <Cell
                              fill={randomColor({
                                luminosity: "dark"
                              })}
                            />
                          ))}
                        </Pie>
                        <Tooltip content={<CustomTooltip />} />
                      </PieChart>
                    </div>
                    <div className="col-lg-6 col-12">
                      {this.state.review.charts.map(c => {
                        return (
                          <p>
                            {c.label} {c.value}%
                          </p>
                        );
                      })}
                    </div>
                  </div> }
                </div>
              </div> }

              <br />
              <br />
              <div className="m-portlet" id="products">
                <div className="m-portlet__head bg-gray">
                  <div className="m-portlet__head-caption">
                    <div className="m-portlet__head-title">
                      <h3 className="m-portlet__head-text">Product</h3>
                    </div>
                  </div>
                </div>
                <div className="m-portlet__body">
                  <Nav tabs>
                    <NavItem>
                      <NavLink
                        className={classnames({
                          active: this.state.activeTab === "1"
                        })}
                        onClick={() => {
                          this.toggle("1");
                        }}
                      >
                        Details&nbsp;
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({
                          active: this.state.activeTab === "2"
                        })}
                        onClick={() => {
                          this.toggle("2");
                        }}
                      >
                        Team&nbsp;
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({
                          active: this.state.activeTab === "3"
                        })}
                        onClick={() => {
                          this.toggle("3");
                        }}
                      >
                        Q&A with Team&nbsp;
                      </NavLink>
                    </NavItem>
                  </Nav>
                  <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                      {renderHTML(this.state.review.product)}
                    </TabPane>
                    <TabPane tabId="2">
                      {
                        this.state.review.team?
                        renderHTML(this.state.review.team) : ""
                      }
                      <br />
                      <br />
                      <div className="row">
                        {this.state.review.teamList.map(t => {
                          return (
                            <div className="col-lg-3 review-team-person">
                              <img
                                style={{ width: 50, height: 50 }}
                                src={t.photo}
                              />
                              <br />
                              <h6 style={{ color: "#007EA5" }}>{t.name}</h6>
                              <p>{t.position}</p>
                            </div>
                          );
                        })}
                      </div>
                    </TabPane>
                    <TabPane tabId="3">
                      {this.state.review.qna ? 
                        <div className="row">
                          {this.state.review.qna.map(q => {
                            return (
                              <div className="col-12">
                                <h6 className="bold">{q.question}</h6>
                                <p>{q.answer}</p>
                                <hr />
                              </div>
                            );
                          })}
                        </div>
                      : <div className="row"><span style={{margin:"auto"}}>No QnA Found</span></div>
                      }
                  
                    </TabPane>
                  </TabContent>
                </div>
              </div>
            </div>

            {/* side bar */}
            <div className="col-lg-4">
              <div className="m-portlet" id="positive-negative">
                <div className="launchpad-ongoing-footer-review">
                    {this.props.user?
                      <div className="row">
                        <div className="col-4" style={{ padding: "2px" }}>
                            <input value={this.state.amount} onChange={this.handleChange('amount')} disabled={expired || this.state.loading} className="form-control" placeholder={minimum} />
                        </div>
                        {currecyList ?
                        <div className="col-4" style={{ padding: "2px" }}>
                          <select value={this.state.usedCurrency} disabled={expired || this.state.loading} className="form-control" onChange={(e) => this.onChangeCurrency(e)}>
                            {
                              currecyList.map(c => {
                                  return (
                                      <option value={c} key={uuid()}>{c}</option>
                                  )
                              })
                            }
                          </select>
                        </div> : null }
                        <div className="col-4" style={{ padding: "2px" }}>
                            <button onClick={() => this.doPledge()} disabled={expired || this.state.loading} className="btn btn-success btn-block">
                                {this.state.loading ? 'Joining' : 'Join'}
                            </button>
                        </div>
                      </div>:
                      <div className="row">
                          <div className="col-12" style={{ padding: "2px" }}>
                              <button onClick={() => this.openLogin()} disabled={this.state.loading} className="btn btn-success btn-block">
                                  Login to Pledge
                              </button>
                          </div>
                      </div>
                    }
                </div>
                <div className="m-portlet__head bg-gray">
                  <div className="m-portlet__head-caption">
                    <div className="m-portlet__head-title">
                      <h3 className="m-portlet__head-text">
                        Positive Negative
                      </h3>
                    </div>
                  </div>
                </div>
                <div className="m-portlet__body">
                  <h6 className="bold">UPSIDES</h6>
                  {this.state.review.positive.map(pos => {
                    
                    return (
                      <div className="row">
                        <div className="col-1">
                          <span className="fa fa-plus launchpad-plus-icon" />
                        </div>
                        <span className="col-10">{pos}</span>
                      </div>
                    );
                  })}

                  <br />
                  <h6 className="bold">DOWNSIDES</h6>
                  {this.state.review.negative.map(min => {
                    return (
                      <div className="row">
                        <div className="col-1">
                          <span className="fa fa-minus launchpad-minus-icon" />
                        </div>
                        <span className="col-10">{min}</span>
                      </div>
                    );
                  })}
                </div>
              </div>

              <div className="m-portlet" id="conclusion">
                <div className="m-portlet__head bg-gray">
                  <div className="m-portlet__head-caption">
                    <div className="m-portlet__head-title">
                      <h3 className="m-portlet__head-text">Conclusion</h3>
                    </div>
                  </div>
                </div>
                <div className="m-portlet__body">
                  <h6>Rating Review and Analytics</h6>
                  <p>tokocrypto review, and rating for the projects</p>
                  <br />
                  <div className="flex-center">
                    <div className="review-score">
                      <p className="score-desc">Total Score</p>
                      <p className="score">
                        {this.renderRating(this.state.review.conclusion.totalScore)}
                      </p>
                    </div>
                  </div>
                  <br />
                  <div className="progress m-progress--sm">
                    <div
                      className="progress-gradient progress-bar m--bg-brand review-progress"
                      role="progressbar"
                      style={{
                        width:
                          this.state.review.conclusion.investmentRating + "%"
                      }}
                      aria-valuenow="50"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                    <span className="review-rating-desc">
                      Investment Rating
                    </span>
                    <span className="review-rating-desc-number">
                      {this.renderRating(this.state.review.conclusion.investmentRating)}
                    </span>
                  </div>
                  <br />
                  <div className="progress m-progress--sm">
                    <div
                      className="progress-gradient progress-bar m--bg-brand review-progress"
                      role="progressbar"
                      style={{ width: this.state.review.conclusion.team + "%" }}
                      aria-valuenow="50"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                    <span className="review-rating-desc">Team Score</span>
                    <span className="review-rating-desc-number">
                      {this.renderRating(this.state.review.conclusion.team)}
                    </span>
                  </div>
                  <br />
                  <div className="progress m-progress--sm">
                    <div
                      className="progress-gradient progress-bar m--bg-brand review-progress"
                      role="progressbar"
                      style={{
                        width: (100 - this.state.review.conclusion.riskScore) + "%"
                      }}
                      aria-valuenow="50"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                    <span className="review-rating-desc">Risk Score</span>
                    <span className="review-rating-desc-number">
                      {this.renderRating(this.state.review.conclusion.riskScore)}
                    </span>
                  </div>
                  <br />
                  <div className="progress m-progress--sm">
                    <div
                      className="progress-gradient progress-bar m--bg-brand review-progress"
                      role="progressbar"
                      style={{
                        width: this.state.review.conclusion.productScore + "%"
                      }}
                      aria-valuenow="50"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                    <span className="review-rating-desc">Product Score</span>
                    <span className="review-rating-desc-number">
                      {this.renderRating(this.state.review.conclusion.productScore)}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.cognito.user,
    language: state.language
  };
};

export default connect(
  mapStateToProps,
  null
)(ReviewICO);
