import React, { Component } from 'react'

class Launchpad extends Component {

    constructor() {
        super()
    }

    render() {
        return (
            <div className="container">
                <div className="row" style={{marginTop: '15%'}}>
                    <div className="col-md-6">
                        <div className="m-portlet m-portlet--full-height">
                            <div className="m-portlet__head" style={{backgroundColor: 'whitesmoke'}}>
                                <div className="m-portlet__head-caption">
                                    <div className="m-portlet__head-title"><h3 className="m-portlet__head-text">Project
                                        List</h3></div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-9">
                                    <div className="col-md-12" style={{padding: '10%'}}>
                                        <div className="launchpad-text flex-center">View current and completed projects
                                            on Toko Lauchpad
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-2 flex-center">
                                    <button className="btn btn-default">View</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="m-portlet m-portlet--full-height">
                            <div className="m-portlet__head" style={{backgroundColor: 'whitesmoke'}}>
                                <div className="m-portlet__head-caption">
                                    <div className="m-portlet__head-title"><h3
                                        className="m-portlet__head-text">Participan History</h3></div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-9">
                                    <div className="col-12" style={{padding: '10%'}}>
                                        <div className="launchpad-text flex-center">View currently and past pledged
                                            project
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-2 flex-center">
                                    <button className="btn btn-default">View</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-6">
                        <div className="m-portlet m-portlet--full-height">
                            <div className="m-portlet__head" style={{backgroundColor: 'whitesmoke'}}>
                                <div className="m-portlet__head-caption">
                                    <div className="m-portlet__head-title"><h3 className="m-portlet__head-text">Q&A</h3>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-9">
                                    <div className="col-md-12" style={{padding: '10%'}}>
                                        <div className="launchpad-text flex-center">Submit enquiries regarding
                                            shortlisted and on-going projects
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-2 flex-center">
                                    <button className="btn btn-default">View</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="m-portlet m-portlet--full-height">
                            <div className="m-portlet__head" style={{backgroundColor: 'whitesmoke'}}>
                                <div className="m-portlet__head-caption">
                                    <div className="m-portlet__head-title"><h3 className="m-portlet__head-text">Project
                                        Request</h3></div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-9">
                                    <div className="col-12" style={{padding: '10%'}}>
                                        <div className="launchpad-text flex-center">Request for projects which you would
                                            like Toko to review and engage
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-2 flex-center">
                                    <button className="btn btn-default">View</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-6">
                        <div className="m-portlet m-portlet--full-height">
                            <div className="m-portlet__head" style={{backgroundColor: 'whitesmoke'}}>
                                <div className="m-portlet__head-caption">
                                    <div className="m-portlet__head-title"><h3
                                        className="m-portlet__head-text">Discussion Group</h3></div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-9">
                                    <div className="col-md-12" style={{padding: '10%'}}>
                                        <div className="launchpad-text flex-center">Join our whatsapp group to be engage
                                            in discussion with our Toko Launchpad Community
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-2 flex-center">
                                    <button className="btn btn-default">View</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="m-portlet m-portlet--full-height">
                            <div className="m-portlet__head" style={{backgroundColor: 'whitesmoke'}}>
                                <div className="m-portlet__head-caption">
                                    <div className="m-portlet__head-title"><h3
                                        className="m-portlet__head-text">Additional Information</h3></div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6" style={{paddingTop: '10%', paddingLeft: "15px", paddingRight: "50px"}}>
                                    <div className="row">
                                        <div className="col-md-9">
                                            <div className="col-md-12">
                                                <div className="launchpad-text flex-center">About Toko Launchpad
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-2 flex-center">
                                            <button className="btn btn-default">View</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6" style={{paddingTop: '10%', paddingLeft: "15px", paddingRight: "50px"}}>
                                    <div className="row">
                                        <div className="col-md-9">
                                            <div className="col-md-12">
                                                <div className="launchpad-text flex-center">How to use Toko Launchpad?
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-2 flex-center">
                                            <button className="btn btn-default">View</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Launchpad
