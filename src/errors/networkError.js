import { NotificationManager } from 'react-notifications';
import history from '../history'
import { notifySuccess, notifyError} from '../helpers/translateNotification';
import store from '../store.js';

let currentMessage = null;
let error = null;
let tempCode
var siteURL

var mainURL = history.location.pathname.split('/')[1];

if(process.env.REACT_APP_ENV === 'dev' || process.env.REACT_APP_ENV === 'local'){
    siteURL = 'https://dev.tokocrypto.com/'
}else if(process.env.REACT_APP_ENV === 'demo'){
    siteURL = 'https://demo.tokocrypto.com/'
}else{
    siteURL = 'https://tokocrypto.com/'
}

const handle = function (err, details, customErr) {
    const state = store.getState();
    
    console.log('=======================================');
    console.log('Error => ', err);
    console.log('Error Response ', err.response)
    if ( details ) {
        console.log(details.type);
        console.log(details.url);
    }
    console.log('=======================================');

    if (!err.response) {

        let errCode = "Something went wrong, Please try again"
        
        // if(errCode != tempCode){
        //     NotificationManager.error(errCode);
        // }

        tempCode = errCode

        setTimeout(() => {
            
            // force clear all interval
            if(window.intervals) {
                window.intervals.map((i, index) => {
                    clearInterval(i)
                    window.intervals.splice(index, 1);
                });
            }

            // history.replace('/')
            
        }, 1500)

    } else {
    if(err.response.status == 400 && err.response.data.err){
        let errCode

        if(err.response.data.err.message){
            errCode = err.response.data.err.message
        }else{
            errCode = err.response.data.err;
        }
        
        NotificationManager.error(errCode);

    } else if(err.response.data.error.statusCode  == 400 ) {
        let errCode

        errCode = err.response.data.error.message;

        NotificationManager.error(errCode);
 
    } else if(err.response.data.error.statusCode  == 401) {
        let errCode = "Session Expire, Please Login again";
        
        if(errCode != tempCode){
            NotificationManager.error(errCode);
        }

        tempCode = errCode

        setTimeout(() => {
            // force clear all interval
            if(window.intervals) {
                window.intervals.map((i, index) => {
                    clearInterval(i)
                    window.intervals.splice(index, 1);
                });
            }
           
            if (state.cognito.user != null) {
                state.cognito.user.signOut();
                if(window.Raven){
                    window.Raven.setUserContext();
                }
    
                // history.replace('/')
                window.location.href = siteURL + mainURL + '/index.html'
            }

        }, 1500)
    }else {
        let errCode = err.response.data.error.code;
        
        // if(errCode != tempCode){
        //     NotificationManager.error("Something went wrong, (Error code: " + errCode + ")");
        // }
        
        tempCode = errCode
    }
  }
}

export default handle;
