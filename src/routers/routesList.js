import React, {Component} from 'react';
import {List, ListItem} from 'material-ui/List';
import {NavLink} from 'react-router-dom'
import history from '../history.js';

// import Divider from 'material-ui/Divider';

class RoutesList extends Component {
  render() {
    // Parent list style
    const style = {
      padding: '16px 16px 16px 55px',
      fontSize: '14px',
    }
    // childern dropdown innerdiv styler
    // const styleChild = {
    //   padding: '12px 13px 12px 40px',
    //   fontSize: '14px'
    // }
    return (
      <List className="nav-menu">
        <ListItem
          primaryText="Dashboard"
          innerDivStyle={style}
          leftIcon={<i className="material-icons">dashboard</i>}
          onClick={() =>{
            history.push('/dashboard');
          }}
        />
      </List>
    );
  }
}

export default RoutesList;
