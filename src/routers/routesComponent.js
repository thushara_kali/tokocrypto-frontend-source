import React, { Component } from 'react';
import { Route, Redirect, Router, Switch } from 'react-router-dom';
import history from '../history';
import localStorage from 'localStorage';
import { Action } from '../core/react-cognito';
import { connect } from 'react-redux';
import cognitoHelper from '../helpers/cognitoHelper';

import Home from '../pages/home/index.js';
import Receive from '../pages/receive/index.js';
import Send from '../pages/send/index.js';
import Withdraw from '../pages/withdraw/index.js';
import WithdrawConfirm from '../pages/withdraw/confirm.js';
import WithdrawMethod from '../pages/withdraw/methods.js';
import Deposit from '../pages/deposit/index.js';
import Transfers from '../pages/deposit/transfers.js';
import Histories from '../pages/histories/index.js';
import Buy from '../pages/buy/index.js';
import Sell from '../pages/sell/index.js';
import Profile from '../pages/profile/index.js';
import Banks from '../pages/banks/index.js';
import NewBanks from '../pages/banks/newBank.js';
import EditBank from '../pages/banks/editBank.js';
import DepositBanks from '../pages/banks/depositBanks.js';
import LoginPage from '../pages/auth/login.js';
import ForgotPasswordPage from '../pages/auth/forgotPassword.js';
import ConfirmPage from '../pages/auth/confirm.js';
import RegisterPage from '../pages/auth/register.js';
import NotificationsPage from '../pages/notifications';
import Page404 from '../pages/auth/page404.js';
import ThankYou from '../pages/errors/index.js';
import TradingOff from '../pages/errors/tradingoff.js';
import WaitingKYC from '../pages/errors/waitingkyc.js';
import store from '../store'
import DashboardLayout from '../containers/dashboardLayout.js'
import { NotificationContainer } from 'react-notifications';
import VerifyAccount from '../pages/home/verifyaccount';
import KYC from '../pages/home/kyc';
import TVChart from '../pages/home/tvChart';
import AppLayout from '../containers/appLayout';
import Dashboard from '../pages/home/dashboard';
import Accounts from '../pages/accounts';
import Conversion from '../pages/conversion';
import Launchpad from '../pages/launchpad';
import Referral from '../pages/referral';
import CheckReferral from '../pages/referral/check';
import ICOReviewPage from '../pages/launchpad/review';
import checkDeveloper from '../pages/apikeys/check';
import ApiKeys from '../pages/apikeys/index';
import ApiKeyDetail from '../pages/apikeys/details';
import ApiKeyCreate from '../pages/apikeys/create';
import checkToken from '../pages/auth/checktoken.js';
import PlainKYC from '../pages/home/plain_kyc.js';
import DPChart from '../pages/home/dpChart';

const PrivateRoute = ({ component: Component, ...rest }) => {
    const state = store.getState();
    let isLoggedIn = state.cognito.user !== null;

    // patch for google signin
    let cacheToken = localStorage.getItem('CACHE_TOKEN');
    
    if(cacheToken) {
        isLoggedIn = true;
    }

    return (
        <Route {...rest} render={props => (
            isLoggedIn ? (
                <AppLayout>
                    <Component {...props} />
                </AppLayout>
            ) : (
                    <Redirect to={{
                        pathname: '/login',
                        state: { from: props.location }
                    }} />
                )
        )} />
    )
}

const PublicRoute = ({ component: Component, ...rest }) => {
    const state = store.getState();

    return (
        <Route {...rest} render={props => (
           
            <AppLayout>
                <Component {...props} />
            </AppLayout>

        )} />
    )
}

const RedirectRoute = ({ ...rest }) => {
    const state = store.getState();

    return (
        <Route {...rest} render={props => (
           
            <Redirect to={{
                pathname: '/' + localStorage.getItem('language') + '/dashboard',
                state: { from: props.location }
            }} />

        )} />
    )
}

const RedirectRouteLogin = ({ ...rest }) => {
    const state = store.getState();

    return (
        <Route {...rest} render={props => (
           
            <Redirect to={{
                pathname: '/' + localStorage.getItem('language') + '/login',
                state: { from: props.location }
            }} />

        )} />
    )
}

const RedirectRouteRegister = ({ ...rest }) => {
    const state = store.getState();

    return (
        <Route {...rest} render={props => (
           
            <Redirect to={{
                pathname: '/' + localStorage.getItem('language') + '/register',
                state: { from: props.location }
            }} />

        )} />
    )
}

const RedirectRouteDashboard = ({ ...rest }) => {
    const state = store.getState();

    return (
        <Route {...rest} render={props => (
           
            <Redirect to={{
                pathname: '/' + localStorage.getItem('language') + '/dashboard',
                state: { from: props.location }
            }} />

        )} />
    )
}

const RedirectRouteKyc = ({ ...rest }) => {
    const state = store.getState();

    return (
        <Route {...rest} render={props => (
           
            <Redirect to={{
                pathname: '/' + localStorage.getItem('language') + '/verifyaccount',
                state: { from: props.location }
            }} />

        )} />
    )
}

const RedirectRouteKycMobile = ({ ...rest }) => {
    const state = store.getState();

    return (
        <Route {...rest} render={props => (
           
            <Redirect to={{
                pathname: '/' + localStorage.getItem('language') + '/verifyaccount_mobile',
                state: { from: props.location }
            }} />

        )} />
    )
}

const RedirectRouteReferral = ({ ...rest }) => {
    const state = store.getState();

    return (
        <Route {...rest} render={props => (
           
            <Redirect to={{
                pathname: '/' + localStorage.getItem('language') + '/referral',
                state: { from: props.location }
            }} />

        )} />
    )
}

const RedirectRouteReferralCode = ({ ...rest }) => {
    const state = store.getState();

    return (
        <Route {...rest} render={props => (
           
            <Redirect to={{
                pathname: '/' + localStorage.getItem('language') + '/referral/:code',
                state: { from: props.location }
            }} />

        )} />
    )
}

const RedirectRouteAll = ({ path, ...rest }) => {
    const state = store.getState();

    return (
        <Route {...rest} render={props => (
           
            <Redirect to={{
                pathname: '/' + localStorage.getItem('language') + path,
                state: { from: props.location }
            }} />

        )} />
    )
}

class RoutesComponent extends Component {

    componentWillMount() {
        if(localStorage.getItem('CACHE_METHOD') === 'social') {
            let _cognitoUser = cognitoHelper.convertTokenIntoCognitoObject();
            this.props.authenticatedManual(_cognitoUser);
        }
    }

    render() {

        return (
            <div>
                <Router history={history}>
                    <Switch>
                        <RedirectRoute exact path="/"/>
                        <RedirectRouteDashboard exact path="/dashboard"/>
                        <RedirectRouteLogin exact path="/login"/>
                        <RedirectRouteRegister exact path="/register"/>
                        <PublicRoute exact path="/:lg/dashboard" component={Dashboard} />
                        <PublicRoute exact path="/:lg/dashboard/:currency" component={Dashboard} />
                        <RedirectRouteKyc exact path="/verifyaccount"/>
                        <RedirectRouteKycMobile exact path="/verifyaccount_mobile"/>
                        <PrivateRoute exact path="/:lg/verifyaccount" component={KYC} />
                        <Route exact path="/:lg/verifyaccount_mobile" component={PlainKYC} />
                        <PrivateRoute exact path="/:lg/accounts" component={Accounts} />
                        {/* <PrivateRoute exact path="/conversions" component={Conversion} /> */}
                        {/* <PrivateRoute exact path="/kyc" component={KYC} /> */}
                        <PrivateRoute exact path="/:lg/history" component={Histories} />
                        <PrivateRoute exact path="/:lg/recieve/:coin" component={Receive} />
                        <PrivateRoute exact path="/:lg/send/:coin" component={Send} />
                        <PrivateRoute exact path="/:lg/deposit/:currency" component={DepositBanks} />
                        <PrivateRoute exact path="/:lg/deposit/:currency/transfers" component={Transfers} />
                        <PrivateRoute exact path="/:lg/withdraw/:currency" component={Withdraw} />
                        <PrivateRoute exact path="/:lg/withdraw/:currency/method" component={WithdrawMethod} />
                        <PrivateRoute exact path="/:lg/withdraw/:accountid/confirm" component={WithdrawConfirm} />
                        <PrivateRoute exact path="/:lg/buy/:coin/" component={Buy} />
                        <PrivateRoute exact path="/:lg/sell/:coin/" component={Sell} />
                        <RedirectRouteAll exact path="/profile"/>
                        <PrivateRoute exact path="/:lg/profile" component={Profile} />
                        <PrivateRoute exact path="/:lg/notifications" component={NotificationsPage} />
                        <PrivateRoute exact path="/:lg/banks" component={Banks} />
                        <PrivateRoute exact path="/:lg/banks/new" component={NewBanks} />
                        <PrivateRoute exact path="/:lg/banks/:accountNumber" component={EditBank} />
                        <PrivateRoute exact path="/:lg/trading-chart" component={TVChart} />
                        <RedirectRouteReferral exact path="/referral"/>
                        <PrivateRoute exact path="/:lg/referral" component={Referral} />
                        <PrivateRoute exact path="/:lg/depth-chart" component={DPChart} />
                        <Route exact path="/:lg/login" component={LoginPage} />
                        <RedirectRouteAll exact path="/checktoken"/>
                        <Route exact path="/:lg/checktoken" component={checkToken} />
                        <RedirectRouteAll exact path="/forgot-password"/>
                        <Route exact path="/:lg/forgot-password" component={ForgotPasswordPage} />
                        <RedirectRouteAll exact path="/reset-password"/>
                        <Route exact path="/:lg/reset-password" component={ForgotPasswordPage} />
                        <Route exact path="/:lg/register" component={RegisterPage} />
                        <RedirectRouteAll exact path="/confirm"/>
                        <Route exact path="/:lg/confirm" component={ConfirmPage} />
                        <Route exact path="/:lg/dashboard" component={Dashboard} />
                        <RedirectRouteAll exact path="/thankyou"/>
                        <Route exact path="/:lg/thankyou" component={ThankYou} />
                        <RedirectRouteReferralCode exact path="/referral/:code"/>
                        <Route exact path="/:lg/referral/:code" component={CheckReferral} />
                        <PrivateRoute exact path="/:lg/trading-unavailable" component={TradingOff} />
                        <PrivateRoute exact path="/:lg/waiting-confirmation" component={WaitingKYC} />
                        <PublicRoute exact path="/:lg/launchpad" component={Launchpad} />
                        <PublicRoute exact path="/:lg/launchpad/review/:id" component={ICOReviewPage} />
                        <Route exact path="/developer" component={checkDeveloper} />
                        <RedirectRouteAll exact path="/api-keys"/>
                        <PrivateRoute exact path="/:lg/api-keys" component={ApiKeys} />
                        <RedirectRouteAll exact path="/api-keys/create"/>
                        <PrivateRoute exact path="/:lg/api-keys/create" component={ApiKeyCreate} />
                        <RedirectRouteAll exact path="/api-keys/:id"/>
                        <PrivateRoute exact path="/:lg/api-keys/:id" component={ApiKeyDetail} />
                        <Route component={Page404} />
                    </Switch>
                </Router>
                <NotificationContainer />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    let username = '';
    if (state.cognito.user) {
        username = state.cognito.user.getUsername();
    } else if (state.cognito.userName) {
        username = state.cognito.cache.userName;
    }
    return {
        user: state.cognito.user,
        username,
        cognitoState: state.cognito.state,
        email: state.cognito.cache.email,
        config: state.cognito.config,
        userPool: state.cognito.userPool,
        language: state.language
    };
};

const mapDispatchToProps = dispatch => ({
    authenticatedManual: (user) => dispatch(Action.authenticatedManual(user)),
});


const mergeProps = (stateProps, dispatchProps, ownProps) =>
    Object.assign({}, ownProps, stateProps, {
        authenticatedManual: (user) => {
            dispatchProps.authenticatedManual(user)
        }
    });

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps,
)(RoutesComponent);

