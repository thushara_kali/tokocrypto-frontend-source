import React, {Component} from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import _ from 'lodash';
import NetworkError from '../../errors/networkError';
import homeActions from '../../core/actions/client/home';
import history from '../../history';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class KYCMessage extends Component {

    constructor(){
        super();
        this.state = {
            status: 'draft'
        }
    }

    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    componentWillReceiveProps(p){

    }

    componentWillMount() {
        this.onSetLanguage();
    }

    render(){

        let message = "";

        if(this.props.status){
            switch(this.props.status){
                case 'draft':
                    message = strings.NOTICE_NOT_COMPLETED_KYC_SUBMISSION;
                    break;
                case 'pending':
                    message = strings.NOTICE_KYC_SUBMISSION_PROCESS_VERIFIYING;
                    break;
                case 'rejected':
                    message = strings.NOTICE_KYC_SUBMISSION_REJECTED;
                    break;
                default:
                    break;
            }
        }

        let classObject = {
            'm-alert m-alert--outline m-alert--square alert alert-dismissible fade show': true,
            'alert-danger': this.props.status === 'rejected',
            'alert-info': this.props.status === 'draft' || this.props.status === 'pending'
        }

        return (
            <div>
                { this.props.status && this.props.status !== 'approved' && this.props.status !== 'premium' &&
                    <div className={classnames(classObject)} role="alert" style={{ marginTop: "20px" }}>
                        {/*<button aria-label="Close" className="close" data-dismiss="alert" type="button"></button>*/}
                        {message}
                        { this.props.status !== 'pending' && <b style={{ cursor: 'pointer'}} onClick={() => history.push('/verifyaccount')}> {strings.CLICK_HERE_TO_PROCEED}</b>}
                    </div>
                }
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        language: state.language
    };
};

export default connect(mapStateToProps, null)(KYCMessage)
