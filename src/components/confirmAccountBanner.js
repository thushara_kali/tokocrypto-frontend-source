import React, {Component} from 'react';
import history from '../history';
import { connect } from 'react-redux';
import classnames from 'classnames';

class ConfirmAccountBanner extends Component {

    constructor(){
        super();
    }

    render(){

        let header = "";

        if(this.props.kyc.status === 'pending') {
            header = "Your account is being verified";
        } else if(this.props.kyc.status === 'rejected') {
            header = "Your account is rejected";
        } else if(this.props.kyc.status === 'draft') {
            header = "Please complete verification data";
        }

        let message = ""
        if(this.props.kyc.status === 'pending') {
            message = "You can deposit and withdraw from IDR bank accounts after your account is verified";
        } else if(this.props.kyc.status === 'rejected') {
            message = "Please update your verification data";
        } else if(this.props.kyc.status === 'draft') {
            message = "You can deposit and withdraw from IDR bank accounts after your account is verified";
        }


        return (
            <div className={classnames({ 'alert': true, 'alert-info': this.props.kyc.status !== 'rejected', 'alert-warning': this.props.kyc.status === 'rejected'})} >
                
                <span style={{ fontWeight: 'bold'}}>{header}</span>
                <br />
                    <div className="row">
                        <div className="col-md-8 col-xs-10">
                            {message}
                        </div>
                        <div className="col-md-4 col-xs-12">
                            <button onClick={() => history.push('/verifyaccount')} className="btn btn-default float-right">update verification data</button>
                        </div>
                    </div>
                    
                
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
      kyc: state.kyc
    };
};

export default connect(mapStateToProps,null)(ConfirmAccountBanner);