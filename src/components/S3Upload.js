import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import classnames from 'classnames';
import NetworkError from '../errors/networkError';
import LocalizedStrings from 'react-localization';
import enDictionary from '../languages/en.json';
import idDictionary from '../languages/id.json';
import javaDictionary from '../languages/java.json';
import sundaDictionary from '../languages/sunda.json';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class S3Upload extends Component {

    constructor() {
        super();
        this.state = {
            fileName: "",
            imageFile: "",
            uploading: false,
            uploadSuccess: false,
            uploadError: false,
            uploadMessage: "",
            invalidFile: false,
            originalFileName: ""
        }
    }

    onSetLanguage() {
        strings.setLanguage(this.props.language);
    }

    getSignedUrl(filename) {
        if(!this.props.mobile) {
            var username = this.props.user.username;
            var clientId = this.props.user.pool.clientId;
            var token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];
        } else {
            var token = this.props.token
        }

        
        
        return new Promise((resolve, reject) => {
            axios.get(this.props.signingUrl + "?filename=" + filename , {
                headers: {
                    'Authorization': token
                }
            }).then(res => {
                
                let signedUrl = res.data.signedUrl;
                this.setState({ filename: res.data.filename });
                this.setState({ imageFile: res.data.signedUrl + "?filename=" + res.data.filename });
                resolve(res.data);
            }).catch(err => {
                NetworkError(err);
                reject(err);
            });
        });
    }

    upload(event) {

        let FileSize = event.target.files[0].size / 1024 / 1024;
        let mime = event.target.files[0].type;
        const rawFile = event.target.files[0];
        const blob = rawFile.slice(0, 4);
        let self = this;

        // 
        

        const getMimetype = (signature) => {
            switch (signature) {
                case '89504E47':
                    return 'image/png'
                // case '47494638':
                //     return 'image/gif'
                // case '25504446':
                //     return 'application/pdf'
                case 'FFD8FFDB':
                case 'FFD8FFE0':
                case 'FFD8FFE1':
                    return 'image/jpeg'
                // case '504B0304':
                //     return 'application/zip'
                default:
                    return 'Unknown filetype'
            }
        }

        self.setState({
            originalFileName: event.target.files[0].name
        });

        const filereader = new FileReader()

        filereader.onloadend = function (evt) {
            if (evt.target.readyState === FileReader.DONE) {
                const uint = new Uint8Array(evt.target.result)
                let bytes = []
                uint.forEach((byte) => {
                    bytes.push(byte.toString(16))
                })
                const hex = bytes.join('').toUpperCase()

                

                mime = getMimetype(hex);

                let acceptedMime = mime === 'image/jpeg' || mime === 'image/png' || mime === 'application/pdf';

                if (FileSize <= 5 && acceptedMime) {

                    self.setState({ uploading: true, uploadMessage: strings.UPLOADING, invalidFile: false });
                    let file = rawFile;
                    self.getSignedUrl(file.name).then(data => {
                        axios.put(data.signedUrl, file).then(res => {
                            self.setState({ uploadSuccess: true, uploadMessage: strings.FILE_UPLOADED });
                            // let self = this;
                            setTimeout(() => {
                                self.setState({ uploading: false })
                            }, 800);

                            self.props.onFinish(data.filename);
                            
                        }).catch(err => {
                            self.setState({ uploadError: true, uploadMessage: strings.PROBLEMS_FILE_UPLOAD });
                            NetworkError(err);
                        });
                    }).catch(err => {
                        NetworkError(err);
                    });
                } else {
                    self.setState({
                        invalidFile: true
                    });
                }
            }

            console.timeEnd('FileOpen')
        }

        filereader.readAsArrayBuffer(blob);
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    render() {
        return (
            <div className="form-group m-form__group">

                <div className="custom-file">
                    <input className={this.props.className} type="file" id="customFile" onChange={(e) => this.upload(e)} />
                    <label className="custom-file-label selected" for="customFile">
                        {this.state.uploading ? <span style={{ fontSize: '10px' }} className={classnames({ 'upload-success': this.state.uploadSuccess }, { 'upload-error': this.state.uploaderror })}>{this.state.uploadMessage}</span> : null}
                        {this.props.value.length !== 0 && this.state.uploading == false && <span className="s3-upload-label upload-success">{this.state.originalFileName} {strings.HAS_BEEN_UPLOADED}</span>}
                    </label>
                    {this.state.invalidFile === true && <span style={{ color: 'red' }} className="s3-upload-label">{strings.FILE_MUST_SIZE_TYPE}</span>}
                    {this.state.invalidFile === false && <span style={{ fontSize: "12px", color: this.props.touched && this.props.value.length === 0 ? "red" : "#7b7e8a" }}>{strings.MAXIMUM_OF_IMAGE_FILE} {this.props.touched && this.props.value.length === 0 ? "is required" : ""}</span>}
                    {/* <input type="file" onChange={this.onImageChange.bind(this)} className="filetype" id="group_image"/> */}

                    {/* { this.state.uploading? <span style={{ fontSize: '10px'}} className={classnames({'upload-success': this.state.uploadSuccess}, {'upload-error': this.state.uploaderror})}>{this.state.uploadMessage}</span> : null }
                    { this.props.value && this.state.uploading == false && <span style={{ fontSize: '10px'}}>uploaded with {this.props.value}</span>} */}
                </div>
                {/* <img style={{width:"100px", marginTop:"15px"}} src={this.state.image} /> */}
            </div>
            // <div className="form-group m-form__group">
            //     <div className="custom-file">
            //         <input type="file" className="custom-file-input" id="customFile"/>
            //         <label className="custom-file-label selected" for="customFile"></label>
            //     </div>
            // </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        fullName: state.cognito.fullName,
        language: state.language
    }
}

export default connect(mapStateToProps)(S3Upload)
