import React, { Component } from 'react';
import QRCode from 'qrcode.react';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import { Loader } from '../tcLoader';
import classnames from 'classnames';
import {Tabs, Tab} from 'material-ui/Tabs';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import { connect } from 'react-redux';
import fontawesome from '@fortawesome/fontawesome'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {faCopy} from '@fortawesome/fontawesome-free-solid'
import receiveActions from '../../core/actions/client/recieve';
import NetworkError from '../../errors/networkError';

fontawesome.library.add(faCopy)

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class RecieveModal extends Component {

    constructor(){
        super();
        this.state = {
            showAddress: false,
            copied: false,
            loading: false,
            copiedTag: false,
            currency: '',
            address: '',
            tabs:'omni'
        }
    }

    handleChange = (value) => {
        this.setState({
          tabs: value,
        });
        this.fetchAddresses(value);
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    async componentWillReceiveProps(props) {
        if(this.state.currency !== props.currency){
            await this.setState({ currency: props.currency });

            this.fetchAddresses(this.state.tabs);
        }
    }

    fetchAddresses(tabs = ''){
        this.setState({
            loading: true
        })
        let currency = this.state.currency.toLowerCase();

        if(this.state.currency === 'USDT' && tabs === 'erc20'){
            currency = 'usdt-erc20';
        }

        receiveActions.fetchAddressList(this.getToken(), currency).then(res => {
            let qRCodeValue = "";

            if (res.data.length > 0) {
                qRCodeValue = res.data[0].Address;
            }
            this.setState({
                address: qRCodeValue,
                loading: false
            })
        }).catch(err => {
            this.setState({
                loading: false
            })
            NetworkError(err)
        })
    }

    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    close(){
        this.setState({
            showAddress: false,
            copied: false,
            copiedTag: false
        });
        this.props.onClose();
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    render(){
        let xrpAddress = '0'
        let destinationTag = '0'
        if(this.state.currency === 'XRP'){
            xrpAddress = this.state.address.split("?")[0]
            destinationTag = this.state.address.split("=")[1]
        }

        return (
            <div>
                <Modal isOpen={this.props.show} toggle={() => this.close()} className="modal-dialog-centered">
                    <ModalHeader toggle={() => this.close()}>
                        { this.state.currency === 'BTC' ?
                            <i className="cc BTC" style={{ fontSize: "25px", verticalAlign:"middle", marginRight:"10px"}}></i> : null
                        }
                        { this.state.currency === 'ETH' ?
                            <i className="cc ETH" style={{ fontSize: "25px", verticalAlign:"middle", marginRight:"10px"}}></i> : null
                        }
                        { this.state.currency === 'XRP' ?
                            <i className="cc XRP" style={{ fontSize: "25px", verticalAlign:"middle", marginRight:"10px"}}></i> : null
                        }
                        { this.state.currency === 'DGX' ?
                            <img style={{ width: "25px", verticalAlign:"middle", marginRight:"10px"}} src="assets/media/img/logo/dgx_token.png" alt="" /> : null
                        }
                        { this.state.currency === 'GUSD' ?
                            <img style={{ width: "25px", verticalAlign:"middle", marginRight:"10px"}} src="assets/media/img/logo/gemini.png" alt="" /> : null
                        }
                        { this.state.currency === 'TUSD' ?
                            <img style={{ width: "25px", verticalAlign:"middle", marginRight:"10px"}} src="assets/media/img/logo/tusd.png" alt="" /> : null
                        }
                        { this.state.currency === 'PAX' ?
                            <img style={{ width: "25px", verticalAlign:"middle", marginRight:"10px"}} src="assets/media/img/logo/pax.png" alt="" /> : null
                        }
                        { this.state.currency === 'USDC' ?
                            <img style={{ width: "25px", verticalAlign:"middle", marginRight:"10px"}} src="assets/media/img/logo/usdc.png" alt="" /> : null
                        }
                        { this.state.currency === 'USDT' ?
                            <img style={{ width: "25px", verticalAlign:"middle", marginRight:"10px"}} src="assets/media/img/logo/usdt.png" alt="" /> : null
                        }
                        { this.state.currency === 'TTC' ?
                            <img style={{ width: "25px", verticalAlign:"middle", marginRight:"10px"}} src="assets/media/img/logo/ttc.png" alt="" /> : null
                        }
                        { this.state.currency === 'SWIPE' ?
                            <img style={{ width: "25px", verticalAlign:"middle", marginRight:"10px"}} src="assets/media/img/logo/swipe.png" alt="" /> : null
                        }
                        { this.state.currency === 'ZIL' ?
                            <img style={{ width: "25px", verticalAlign:"middle", marginRight:"10px"}} src="assets/media/img/logo/zil.png" alt="" /> : null
                        }
                        <span style={{ fontSize: "15px"}}>{strings.RECEIVE}&nbsp;
                            { this.state.currency === 'BTC' ?
                                <span>Bitcoins</span> :null
                            }{ this.state.currency === 'ETH' ?
                                <span>Ethereum</span> :null
                            }{ this.state.currency === 'XRP' ?
                                <span>Ripple</span> :null
                            }{ this.state.currency === 'DGX' ?
                                <span>DGX</span> :null
                            }{ this.state.currency === 'GUSD' ?
                                <span>GUSD</span> :null
                            }{ this.state.currency === 'TUSD' ?
                                <span>TUSD</span> :null
                            }{ this.state.currency === 'PAX' ?
                                <span>PAX</span> :null
                            }{ this.state.currency === 'USDC' ?
                                <span>USDC</span> :null
                            }{ this.state.currency === 'USDT' ?
                                <span>USDT</span> :null
                            }{ this.state.currency === 'TTC' ?
                            <span>TTC</span> :null
                            }{ this.state.currency === 'SWIPE' ?
                            <span>SWIPE</span> :null
                            }{ this.state.currency === 'ZIL' ?
                            <span>ZIL</span> :null
                        }</span>
                    </ModalHeader>
                    <ModalBody className="modal-body tc-modal-body" style={{ textAlign: "center"}}>
                        {this.state.currency === 'USDT' &&
                            <Tabs
                                value={this.state.tabs}
                                onChange={this.handleChange}
                                style={{paddingBottom:"1.5rem"}}
                                inkBarStyle={{backgroundColor:"rgb(214, 183, 29)"}}
                                tabItemContainerStyle={{backgroundColor:"rgb(39, 160, 123)"}}
                            >
                                <Tab 
                                    label="OMNICORE" 
                                    value="omni"
                                    >
                                </Tab>
                                <Tab 
                                    label="ERC20" 
                                    value="erc20"
                                    >
                                </Tab>
                            </Tabs>
                        }
                        { !this.state.loading && <QRCode size={200} className="text-center" value={this.state.address} /> }
                                <br />
                                { this.state.loading && <div style={{ height: 245, display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                                    <Loader type="line-scale" active={this.state.loading} className="text-center" />
                                </div> }
                                {this.state.currency === 'BTC' || this.state.currency === 'ETH' || this.state.currency === 'DGX' || this.state.currency === 'GUSD' || this.state.currency === 'TUSD' || this.state.currency === 'PAX' || this.state.currency === 'USDC' || this.state.currency === 'USDT' || this.state.currency === 'TTC' || this.state.currency === 'SWIPE' || this.state.currency === 'ZIL' ?
                                    <span>
                                        {this.state.loading == false ?
                                        <div className="input-group" style={{ marginBottom: "15px" }}>
                                            <input id="tc-address" value={this.state.address} disabled type="text" className="form-control m-input" aria-describedby="basic-addon2" style={{textAlign:"center",fontSize:"15px",fontWeight:"600"}}/>
                                            <CopyToClipboard
                                                onCopy={() => this.setState({ copied: true})}
                                                text={this.state.address}>
                                                <div className="input-group-append" style={
                                                    { margin: "auto", border: "1px solid gray", padding: "-0.35rem 0.05rem",border: "1px solid rgba(0,0,0,.15)" }
                                                    }>
                                                    <span className="input-group-text" id="basic-addon2"><FontAwesomeIcon style={{ color: "#6f727d" }}  icon={faCopy} /></span>
                                                </div>
                                            </CopyToClipboard>
                                        </div> : null}
                                    </span>
                                : null}
                                {this.state.currency === 'XRP'?
                                    <span>
                                        {this.state.loading == false ?
                                        <span>
                                            <div style={{textAlign:"left",marginTop:"15px",color:"#908e8e"}}>{strings.TC_RIPPLE_DEPOSIT_ADDRESS}:</div>
                                            <div className="input-group" style={{ marginBottom: "15px" }}>
                                                <input id="tc-address" value={xrpAddress} disabled type="text" className="form-control m-input" aria-describedby="basic-addon2" style={{textAlign:"left",fontSize:"15px",fontWeight:"600"}}/>
                                                <CopyToClipboard
                                                    onCopy={() => this.setState({ copied: true, copiedTag: false})}
                                                    text={xrpAddress}>
                                                    <div className="input-group-append" style={
                                                        { margin: "auto", border: "1px solid gray", padding: "-0.35rem 0.05rem",border: "1px solid rgba(0,0,0,.15)" }
                                                        }>
                                                        <span className="input-group-text" id="basic-addon2"><FontAwesomeIcon style={{ color: "#6f727d" }}  icon={faCopy} /></span>
                                                    </div>
                                                </CopyToClipboard>
                                            </div>
                                            <div style={{textAlign:"left", color:"#908e8e"}}>{strings.DEPOSIT_TAG}:</div>
                                            <div className="input-group" style={{ marginBottom: "15px" }}>
                                                <input id="tc-address" value={destinationTag} disabled type="text" className="form-control m-input" aria-describedby="basic-addon2" style={{textAlign:"left",fontSize:"15px",fontWeight:"600"}}/>
                                                <CopyToClipboard
                                                    onCopy={() => this.setState({ copiedTag: true, copied: false})}
                                                    text={destinationTag}>
                                                    <div className="input-group-append" style={
                                                        { margin: "auto", border: "1px solid gray", padding: "-0.35rem 0.05rem",border: "1px solid rgba(0,0,0,.15)" }
                                                        }>
                                                        <span className="input-group-text" id="basic-addon2"><FontAwesomeIcon style={{ color: "#6f727d" }}  icon={faCopy} /></span>
                                                    </div>
                                                </CopyToClipboard>
                                            </div>
                                        </span>
                                        : null}
                                    </span>
                                : null}

                                {/* this text info should only show to deposit coin with 0 decimal accepted (read documentation) */}
                                {this.state.currency === 'XRP' || this.state.currency === 'TTC' || this.state.currency === 'SWIPE' ? <div style={{ fontWeight: 500, fontSize: "11px", lineHeight: 1.9, textAlign:"left" }}>
                                    <b>{strings.IMPORTANT}: Please ensure deposits are in integers. Decimals will be ignored</b>
                                </div> : null}

                                { this.state.copied && <span>{strings.ADDRESS_COPIED_CLIPBOARD}</span> }
                                { this.state.copiedTag && <span>{strings.DESTINATION_TAG_COPIED_TO_CLIPBOARD}</span> }

                                <div className="m-portlet__body" style={{marginTop: "10px", marginBottom:"-15px"}}>
                                    <div className="m-alert m-alert--icon alert alert-warning" role="alert">
                                        <div className="m-alert__icon">
                                            <i className="flaticon-danger"></i>
                                        </div>
                                        {this.state.currency == 'BTC' || this.state.currency == 'ETH' || this.state.currency == 'DGX' || this.state.currency == 'GUSD' || this.state.currency === 'TUSD' || this.state.currency === 'PAX' || this.state.currency === 'USDC' || this.state.currency === 'USDT' || this.state.currency === 'TTC' || this.state.currency === 'SWIPE' || this.state.currency === 'ZIL'?
                                            <div className="m-alert__text">
                                                <strong>{strings.ONLY_SEND} {this.state.currency} {strings.TO_THIS_ADDRESS} </strong>
                                                {strings.SENDING_DIGITAL}
                                                {strings.USDT_RESULT_PERMANENT_LOSS}.
                                            </div> : null
                                        }
                                        {this.state.currency == 'XRP' ?
                                            <div className="m-alert__text">
                                                {strings.ONLY_DEPSOIT_RIPPLE_TO_TAG_MESSAGE}
                                            </div> : null
                                        }
                                    </div>
                                </div>
                    </ModalBody>
                </Modal>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

export default connect(mapStateToProps, null) (RecieveModal);
