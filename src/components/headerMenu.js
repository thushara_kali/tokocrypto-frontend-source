import React, { Component } from 'react'
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import history from '../history.js'
import classnames from 'classnames'

const styles = {
  headerItem: {
    marginRight: '25px'
  }
}

class HeaderMenu extends Component {

  constructor() {
    super()
    this.state = {
      openMenu: false,
      openMenuMobile: false
    }
  }

  handleOnRequestChange = (value) => {
    this.setState({
      openMenu: value,
    });
  }

  handleOnRequestMobileChange = (value) => {
    this.setState({
      openMenuMobile: value,
    });
  }

  changePage(page) {
    history.push(page)
  }

  render() {
    // will do the active page selection with redux at logic phase
    let page = history.location.pathname
    let walletpages = [
      '/', '/recieve/BTC', '/recieve/ETH', '/send/BTC', '/send/ETH', '/deposit/IDR', '/deposit/IDR/transfers', '/deposit/IDR',
      '/withdraw/IDR', '/withdraw/IDR/method', '/withdraw/1/confirm', '/withdraw/2/confirm', '/buy/BTC', '/sell/BTC', '/sell/ETH', '/buy/ETH'
    ]
    let walletMenu = walletpages.includes(page)

    let bankMenu = page.split('/')[1] == 'banks'
    let notificationMenu = page.split('/')[1] == 'notifications'

    return (
      <div className="header-menu-container">
        <div className="header-right">
          <div className={classnames('header-menu-item', { 'active-menu': walletMenu })} onClick={() => this.changePage('/')}>
            {/* <span className={classnames('material-icons', 'menu-icon', { 'menu-icon-active': walletMenu })} >account_balance_wallet</span> */}
            <span style={{ marginBottom: '30px' }}>Wallet</span>
          </div>
          <div className={classnames('header-menu-item', { 'active-menu': page == '/history' })} onClick={() => this.changePage('/history')}>
            {/* <span className={classnames('material-icons', 'menu-icon', { 'menu-icon-active': page == '/history' })}>history</span> */}
            <span style={{ marginBottom: '30px' }}>History</span>
          </div>
          <div className={classnames('header-menu-item', { 'active-menu': bankMenu })} onClick={() => this.changePage('/banks')}>
            {/* <span className={classnames('material-icons', 'menu-icon', { 'menu-icon-active': bankMenu })}>work</span> */}
            <span style={{ marginBottom: '30px' }}>Banks</span>
          </div>
          <div className={classnames('header-menu-item', { 'active-menu': notificationMenu })} onClick={() => this.changePage('/notifications')}>
            {/* <span className={classnames('material-icons', 'menu-icon', { 'menu-icon-active': bankMenu })}>work</span> */}
            <span style={{ marginBottom: '30px' }}>Notifications</span>
          </div>
          {/* <div className="header-menu-item">
            <IconMenu
              className={classnames('header-menu-item', { 'active-menu': page == '/services' })}
              style={{ zIndex: '1000' }}
              anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
              iconButtonElement={
                <div>
                  <span className={classnames('material-icons', 'menu-icon', { 'menu-icon-active': page == 'services' })}>add</span>
                  <span style={{ marginBottom: '30px' }}>Services</span>
                </div>
              }
              open={this.state.openMenu}
              onRequestChange={this.handleOnRequestChange}
            >
              <MenuItem onClick={() => this.changePage('/topup')} primaryText="Top Up Prepaid SIM Card" />
            </IconMenu>
          </div> */}
          {/* <div className="header-menu-item" className={classnames('header-menu-item', { 'active-menu': page == '/reference' })} onClick={() => this.changePage('/reference')}>
            <span className={classnames('material-icons', 'menu-icon', { 'menu-icon-active': page == 'reference' })}>people</span>
            <span style={{ marginBottom: '30px' }}>Refer a friend</span>
          </div> */}
          <div className="header-menu-drop">
            <IconMenu
              style={{ zIndex: '20000' }}
              anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
              iconButtonElement={
                <div>
                  <span className="material-icons menu-icon">menu</span>
                  <span style={{ marginBottom: '30px' }}>Menu</span>
                </div>
              }
              open={this.state.openMenuMobile}
              onRequestChange={this.handleOnRequestMobileChange}
            >
              <MenuItem onClick={() => this.changePage('/')} primaryText="Wallet" />
              <MenuItem onClick={() => this.changePage('history')} primaryText="History" />
              <MenuItem onClick={() => this.changePage('banks')} primaryText="Banks" />
            </IconMenu>
          </div>
        </div>
      </div>
    )
  }

}

export default HeaderMenu