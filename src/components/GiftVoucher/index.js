import React, { Component } from 'react';
import LocalizedStrings from 'react-localization';
import _ from 'lodash';
import homeActions from '../../core/actions/client/home';
import OTPActions from '../../core/actions/client/otp';
import GiftActions from '../../core/actions/client/giftcard';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import { connect } from 'react-redux';
import { NotificationManager } from 'react-notifications';
import ReactHoverObserver from 'react-hover-observer';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ConditionalRender from '../conditionalRender';
import amountFormatter from '../../helpers/amountFormatterComma';
import amountFormatterPoint from '../../helpers/amountFormatter';
import ReactTooltip from 'react-tooltip';
import NetworkError from '../../errors/networkError';
let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class GiftVoucher extends Component {
    constructor() {
        super();
        this.state = {
            modalCreate: false,
            confirmVoucher: false,
            successVoucher: false,
            idrAmount: 0,
            idrBalance: 0,
            recepientEmail: "",
            balanceLoading: false,
            generating: false,
            otpId: "",
            token: "",
            giftCode: "",
            promoCode: "",
            redeeming: false
        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    getBalance() {
        let username = this.props.user.username;
        let clientId = this.props.user.pool.clientId;
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken'];

        this.setState({ balanceLoading: true });
        homeActions.getAccountDashboard(this.getToken()).then(res => {

            let accOrdered = _.orderBy(res.data.data, (acc) => {
                return acc.Currency.Type;
            });
            

            let IDRAccount = _.find(accOrdered, {CurrencyCode: 'IDR'});
            const idrBalance = IDRAccount.Balance


            this.setState({
                idrBalance,
                balanceLoading: false
            });

        }).catch(err => {
            NetworkError(err);
            let errorDetails = {
                type: 'COMPONENTS: GiftVoucher | FUNC: getBalance > fetchDashboard',
                url: '/dashboard'
            }
            NetworkError(err, errorDetails);

            this.setState({ balanceLoading: false });
        })
    }

    onSetLanguage() {
        strings.setLanguage(this.props.language);
    }

    createNewVoucher() {

            this.setState({
                modalCreate: true
            });
    }

    generateVoucher() {
        this.setState({
            generating: true
        });
        this.requestOTP();
    }

    requestOTP() {
        OTPActions.requestOTP(this.getToken(), 'IDR').then(res => {
            this.setState({
                confirmVoucher: true,
                generating: false,
                otpId: res.data.data.OtpId
            });
            NotificationManager.success(strings.OTP_SENT);
        }).catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: GiftVoucher | FUNC: requestOTP',
                url: '/otp'
            }
            NetworkError(err, errorDetails);
            // NotificationManager.error(err.toString());
        })
    }

    setIdrAmount(e) {
        if (isNaN(Number(e.target.value))) {
            this.setState({
                idrAmount: 0
            });
        } else {
            this.setState({
                idrAmount: e.target.value
            });
        }
    }


    confirmGenerateVoucher() {
        this.setState({
            generating: true
        });
        this.validateOTP();
    }

    validateOTP() {
        OTPActions.clientMfaVerify(this.state.otpId, this.state.token).then(res => {
            this.submitGenerateVoucher();
        }).catch(err => {
            this.setState({
                generating: false
            });
            let errorDetails = {
                type: 'COMPONENTS: GiftVoucher | FUNC: validateOTP',
                url: '/otp/login/verification'
            }
            NetworkError(err, errorDetails);

            // NotificationManager.error(strings.INVALID_OTP);
        });

    }

    submitGenerateVoucher() {
        GiftActions.createGiftCard(this.getToken(), this.state.giftCode, 'IDR', Number(this.state.idrAmount)).then(res => {
            
            this.setState({
                generating: false,
                successVoucher: true,
                confirmVoucher: false,
                promoCode: res.data.result.PromoCode
            });
            NotificationManager.success(strings.GIFT_CARD_CREATED);
        }).catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: GiftVoucher | FUNC: submitGenerateVoucher',
                url: 'promotion/giftCardCreate'
            }
            NetworkError(err, errorDetails);

            // if (err.response.data) {
            //     NotificationManager.error(err.response.data.err);
            // } else {
            //     NotificationManager.error(err.toString());
            // }

            this.setState({
                generating: false
            });
        });
    }

    toggleCreate() {
        
        this.setState({
            modalCreate: false,
            confirmVoucher: false,
            generating: false,
            giftCode: "",
            successVoucher:false,
            idrAmount:0,
            token:""
        });
    }

    redeemGiftCard() {
        if (this.state.giftCode.length === 0) {
            NotificationManager.warning(strings.GIFT_CODE_REQUIRED);
        } else {
            this.setState({
                redeeming: true
            });

            GiftActions.redeemGiftCard(this.getToken(), this.state.giftCode).then(res => {
                NotificationManager.success(strings.GIFT_CARD_REDEEMED);
                this.setState({
                    redeeming: false
                });
            }).catch(err => {
                let errorDetails = {
                    type: 'COMPONENTS: GiftVoucher | FUNC: redeemGiftCard',
                    url: 'promotion/addGiftCardToUser'
                }
                NetworkError(err, errorDetails);

                // 
                // NotificationManager.error(strings.GIFT_CARD_REDEEMED_FAIL);
                this.setState({
                    redeeming: false,
                    giftCode: ""
                });
            });
        }
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    componentDidMount() {
        this.getBalance();
    }

    render() {
        return (
            <div className="col-md-12 referal-details-amount  m-portlet m-portlet--full-height">
                <div className="col-md-3 referal-details-amount-icons">
                    <div className="user-profile-bottom-container">
                        <img src="../../assets/voucher.png" className="" alt="" style={{ width: "40px",height: "30px",textAlign: "center", marginLeft: "-5px"}}/>
                    </div>
                </div>
                <div className="col-md-9" style={{ lineHeight: "1.4" }}>
                    <div data-tip data-for='create-gift-tooltip' style={{ fontWeight: 400, marginBottom: "10px", color: "#666666" }}>{strings.GIFT_VOUCHER}</div>
                    <ReactTooltip data-border='true' id={'create-gift-tooltip'} place="bottom" effect="solid" style={{ width: 50}}>
                        <p>{strings.GIFT_DESCRIPTION}</p>
                    </ReactTooltip>
                    <div className="form-group row">
                        <label className="control-label col-4">{strings.VOUCHER_CODE}</label>
                        <input placeholder={strings.ENTER_CODE_REDEEM} disabled={this.state.redeeming} type="text" className="form-control redeem col-8" onChange={(e) => this.setState({ giftCode: e.target.value })} value={this.state.giftCode} />
                    </div>
                    <div className="form-group row" style={{ display: "flex", justifyContent:"center", alignItems:"center"}}>
                        <button disabled={this.state.redeeming} className="btn btn-outline-blue redeem-button" onClick={this.redeemGiftCard.bind(this)}>
                            {this.state.redeeming ? strings.REDEEMING : strings.REDEEM}
                        </button>
                        <button style={{marginLeft: 5}} onClick={this.createNewVoucher.bind(this)} className="btn btn-outline-blue redeem-button">
                            {strings.NEW_VOUCHER}
                        </button>
                    </div>

                </div>

                <Modal isOpen={this.state.modalCreate} toggle={this.toggleCreate.bind(this)} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>{strings.GIFT_VOUCHER}</ModalHeader>
                    <ModalBody>
                        <ConditionalRender when={this.state.confirmVoucher === false && this.state.successVoucher === false}>
                            <div>
                                <p>{strings.NEW_GIFT_VOUCHER_DESC}</p>
                                <div className="form">
                                    <div className="form-group row">
                                        <label className="col-4 control-label">{strings.AMOUNT_RUPIAH}</label>
                                        <div className="col-8">
                                            <input value={this.state.idrAmount} onChange={(e) => this.setIdrAmount(e)} disabled={this.state.generating || this.state.balanceLoading} className="form-control" />
                                            <small>balance {amountFormatterPoint(this.state.idrBalance)}</small>
                                            <br />
                                            <ConditionalRender when={this.state.idrAmount > this.state.idrBalance}>
                                                <small className="error-msg">{strings.INSUFFCIENT_ACCOUNT_BALANCE}</small>
                                            </ConditionalRender>
                                            <ConditionalRender when={Number(this.state.idrAmount) <= 0 || this.state.idrAmount === ''}>
                                                <small className="error-msg">{strings.MINIMUM_GIFT} Rp {amountFormatterPoint(0)}</small>
                                            </ConditionalRender>
                                        </div>
                                    </div>
                                    {
                                        /*
                                        <div className="form-group row">
                                        <label className="col-4 control-label">{strings.RECEPIENT_EMAIL_NOT_REQUIRED}</label>
                                        <div className="col-8">
                                        <input onChange={(e) => this.setState({ recepientEmail: e.target.value })} disabled={this.state.generating || this.state.balanceLoading} className="form-control" />
                                        </div>
                                        </div>
                                        */
                                    }
                                </div>
                            </div>
                        </ConditionalRender>

                        <ConditionalRender when={this.state.confirmVoucher === true}>
                            <div className="form">
                                <div className="form-group row">
                                    <label className="col-4 control-label">{strings.AMOUNT_RUPIAH}</label>
                                    <label className="col-8 control-label">{amountFormatterPoint(this.state.idrAmount)}</label>
                                </div>

                                {
                                    /*
                                    <div className="form-group row">
                                    <label className="col-4 control-label">{strings.RECEPIENT_EMAIL_NOT_REQUIRED}</label>
                                    <label className="col-8 control-label">{this.state.recepientEmail}</label>
                                    </div>
                                    */
                                }
                                <div className="form-group row">
                                    <label className="col-12 control-label">
                                    {
                                        this.props.method === 'email' ? strings.OTP_SENT_TO_EMAIL : strings.OTP_GOOGLE_AUTH_APP
                                    }
                                    </label>
                                    <div className="col-8">
                                        <input disabled={this.state.generating} value={this.state.token} onChange={(e) => this.setState({ token: e.target.value })} className="form-control" />
                                    </div>
                                </div>
                            </div>
                        </ConditionalRender>

                        <ConditionalRender when={this.state.successVoucher === true}>
                            <div className="form">
                                <div className="form-group row">
                                    <label className="col-4 control-label">{strings.AMOUNT_RUPIAH}</label>
                                    <label className="col-8 control-label">{amountFormatterPoint(this.state.idrAmount)}</label>
                                </div>
                                <div className="form-group row">
                                    <label className="col-4 control-label">{strings.VOUCHER_CODE}</label>
                                    <label className="col-8 control-label">{this.state.promoCode}</label>
                                </div>
                            </div>
                        </ConditionalRender>
                    </ModalBody>
                    <ModalFooter>
                        <ConditionalRender when={this.state.confirmVoucher === true}>
                            <Button disabled={this.state.generating} color="primary" onClick={this.confirmGenerateVoucher.bind(this)}>
                                {this.state.generating ?
                                    strings.GENERATING_VOUCHER : strings.GENERATE_VOUCHER
                                }
                            </Button>
                        </ConditionalRender>
                        <ConditionalRender when={this.state.confirmVoucher === false && this.state.successVoucher === false}>
                            <Button disabled={this.state.generating || Number(this.state.idrAmount) <= 0 ||this.state.idrAmount > this.state.idrBalance || this.state.balanceLoading} color="primary" onClick={this.generateVoucher.bind(this)}>
                                {this.state.generating ?
                                    strings.GENERATING_VOUCHER : strings.GENERATE_VOUCHER
                                }
                            </Button>
                        </ConditionalRender>
                        <ConditionalRender when={this.state.successVoucher === true}>
                            <Button disabled={this.state.generating} color="primary" onClick={this.toggleCreate.bind(this)}>
                                Ok
                            </Button>
                        </ConditionalRender>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        trading: state.trading,
        language: state.language
    };
};

export default connect(mapStateToProps, null)(GiftVoucher);
