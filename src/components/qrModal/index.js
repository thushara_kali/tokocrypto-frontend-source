import React, { Component } from 'react';
import QRCode from 'qrcode.react';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import { Loader } from '../tcLoader';
import classnames from 'classnames';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import { connect } from 'react-redux';
import fontawesome from '@fortawesome/fontawesome'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {faCopy} from '@fortawesome/fontawesome-free-solid'

fontawesome.library.add(faCopy)

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class QRModal extends Component {

    constructor(){
        super();
        this.state = {
            loading: false
        }
    }

    // componentWillReceiveProps(props) {
    //     this.setState({ loading: props.loading});
    // }

    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    close(){
        this.props.onClose();
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    render(){
        return (
            <div>
                <Modal isOpen={this.props.show} toggle={() => this.close()} className="modal-dialog-centered">
                    <ModalHeader toggle={() => this.close()}>
                        QRCode
                    </ModalHeader>
                    <ModalBody className="modal-body tc-modal-body" style={{ textAlign: "center"}}>
                        { !this.state.loading && <QRCode size={200} className="text-center" value={this.props.refCode} /> }

                    </ModalBody>
                </Modal>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

export default connect(mapStateToProps, null) (QRModal);
