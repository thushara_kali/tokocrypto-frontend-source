import React, {Component} from 'react';
import actions from '../core/actions/client/home';
import classnames from 'classnames';
import {GoogleCharts} from '../googleCharts';
import _ from 'lodash';
import numeral from 'numeral';
import moment from 'moment';
import NetworkError from '../errors/networkError';

class LiveRateChart extends Component {

  constructor(){
    super();
    this.state = {
      liveCurrencyPair: "BTCIDR",
      liveType: "hours",
      liveRates: [],
      o: 0,
      h: 0,
      l: 0,
      c: 0,
      rising: false
    };
  }

  fetchLiveRate(){
    actions.fetchLiveRate(this.state.liveCurrencyPair, this.state.liveType).then(res => {

      var series = { name: this.state.liveCurrencyPair, values: [] };

      res.data.data.forEach(d => {
        d.UTCx = new Date(d.DateTime);
        d.x = new Date(d.UTCx.getTime() - d.UTCx.getTimezoneOffset()*60*1000);
        series.values.push({ x: d.x, open: d.Open, high: d.High, low: d.Low, close: d.Close});
      });

      let lastTwoOpen = series.values[series.values.length - 2].open
      let lastOpen = series.values[series.values.length - 1].open;
      let lastClose = series.values[series.values.length - 1].close;
      let lastHigh = series.values[series.values.length - 1].high;
      let lastLow = series.values[series.values.length - 1].low;

      let rising = false;

      if (lastTwoOpen < lastOpen) {
          rising = true;
      }

      this.setState({
        liveRates: series,
        o: lastOpen,
        h: lastHigh,
        l: lastLow,
        c: lastClose,
        rising: rising
      });

      this.drawChart(series);
    }).catch(err => {
      let errorDetails = {
        type: 'COMPONENTS: liveRateChart | FUNC: fetchLiveRate',
        url: `/rates/history?currencyPair=${this.state.liveCurrencyPair}&type=${ this.state.liveType }`
      }
      NetworkError(err, errorDetails);
    });
  }

  onChangeCurrencyPair(e){

    let pairs = e.target.value.split('/');

    this.setState({
      liveCurrencyPair: pairs[0]+pairs[1]
    });
    let self = this;
    setTimeout(() => {
      self.fetchLiveRate();
    }, 150);
  }

  onChangeCurrencyType(type){
    this.setState({
      liveType: type
    });

    let self = this;
    setTimeout(() => {
      self.fetchLiveRate();
    }, 150);
  }

  drawChart(series){
    let seriesData = series;

    GoogleCharts.load(() => {
        let ratesData = seriesData.values.map((r) => {

            let dateFormat = moment(r.x).format('MMM Do YYYY, h:mm:ss a');

            return [
                r.x, r.low, r.open, r.close, r.high, ''+dateFormat+'\n'+'O: '+numeral(r.open).format('0,0')+' H: '+numeral(r.high).format('0,0')+' L: '+numeral(r.low).format('0,0')+' C: '+numeral(r.close).format('0,0')
            ];

        });

        let dataTable = new GoogleCharts.api.visualization.DataTable();
        dataTable.addColumn('date', 'Date');
        dataTable.addColumn('number', 'Low');
        dataTable.addColumn('number', 'Open');
        dataTable.addColumn('number', 'Close');
        dataTable.addColumn('number', 'High');
        dataTable.addColumn({type: 'string', role: 'tooltip'});
        dataTable.addRows(ratesData);

        // const data = GoogleCharts.api.visualization.arrayToDataTable(ratesData, true);

        let max = _.maxBy(ratesData, (r) => r[4])[4];
        let min = _.minBy(ratesData, (r) => r[1])[1];

        let tick = 10;
        let range = max - min;
        let step = range / tick;
        let tickRange = [min];

        let current = min;
        while(current < max ){
            current += step;
            tickRange.push(current);
        }

        let options = {
            legend:'none',
            candlestick: {
                fallingColor: { strokeWidth: 1, fill: '#a52714', stroke: '#a52714' }, // red
                risingColor: { strokeWidth: 1, fill: '#0f9d58', stroke: '#0f9d58' }   // green
            },
            bar: { groupWidth: '100%' },
            chartArea: {
                top: 10,
                bottom: 30,
                right: 0,
                left: 100
            },
            tooltip: {isHtml: false}
            // vAxis: {
            //     ticks: tickRange
            // }
        };

        const candle_chart = new GoogleCharts.api.visualization.CandlestickChart(document.getElementById('chart_div'));
        candle_chart.draw(dataTable, options);
    });
  }

  componentDidMount(){
    this.fetchLiveRate();
  }

  render(){

    let unit = 'day';
    let interval = 1;

    if(this.state.liveType == 'hours'){
      unit = 'hour',
      interval = 1;
    }

    return (
      <div className="container col-md-12 chart-container">
        <div className="accountBalance-header">Exchange</div>
        <div className="chart-header">
            <div>
              <select className="form-control" onChange={(e) => this.onChangeCurrencyPair(e)}>
                <option selected={this.state.liveCurrencyPair === "BTCIDR"}>BTC/IDR</option>
                <option selected={this.state.liveCurrencyPair === "ETHIDR"}>ETH/IDR</option>
              </select>
            </div>

            <br/>
            <div className="chart-date-container">
              <div className={classnames({ active: this.state.liveType === 'days'})} onClick={() => this.onChangeCurrencyType('days')}>Days</div>
              <div className={classnames({ active: this.state.liveType === 'hours'})} onClick={() => this.onChangeCurrencyType('hours')}>Hours</div>
            </div>
            <div className="chart-buttons">
              { this.state.liveCurrencyPair === "BTCIDR" && <div><img className="accounts-header-image" src={require('../img/opengraph.png')} />Bitcoins</div> }
              { this.state.liveCurrencyPair === "ETHIDR" && <div><img className="accounts-header-image" src={require('../img/ethereum.png')} />Etherum</div> }
            </div>
          </div>
          <div className="row">
              <div className="col-12 row ohlc-wrapper">
                  <div className="col ohlc-text">
                      <span className="ohlc-indicator indicator-first">O</span>: <span className={classnames({"rising": this.state.rising, "falling": !this.state.rising})}>{numeral(this.state.o).format('0,0')}</span>
                      <span className="ohlc-indicator">H</span>: <span className={classnames({"rising": this.state.rising, "falling": !this.state.rising})}>{numeral(this.state.h).format('0,0')}</span>
                      <span className="ohlc-indicator">L</span>: <span className={classnames({"rising": this.state.rising, "falling": !this.state.rising})}>{numeral(this.state.l).format('0,0')}</span>
                      <span className="ohlc-indicator">C</span>: <span className={classnames({"rising": this.state.rising, "falling": !this.state.rising})}>{numeral(this.state.c).format('0,0')}</span>
                  </div>
              </div>
          </div>
          <br/>
          <div id="chart_div" style={{ height: 500}}>

          </div>

      </div>
    );
  }

}

export default LiveRateChart;
