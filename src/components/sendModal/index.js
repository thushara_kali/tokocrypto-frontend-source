import React, { Component } from 'react';
import QRCode from 'qrcode.react';
import classnames from 'classnames';
import OTPModal from '../OTPModal';
import { sendTranscation,getWithdrawFees,getWithdrawLimits } from '../../core/actions/client/send';
import recieveActions from '../../core/actions/client/recieve';
import { NotificationManager } from 'react-notifications';
import NetworkError from '../../errors/networkError';
import { connect } from 'react-redux';
import { Loader } from '../tcLoader';
import homeActions from '../../core/actions/client/home';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import history from '../../history';
import OTPActions from '../../core/actions/client/otp';
import cryptoFormatter, { cryptoFormatterReplaceFormat, cryptoFormaterUnformat, cryptoFormatterByCoin } from '../../helpers/cryptoFormatter';
import numeral from 'numeral';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import NP from 'number-precision';
import WAValidator from 'wallet-address-validator';
import _ from 'lodash';
const { validation } = require('@zilliqa-js/util');

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class SendModal extends Component {

    constructor(){
        super();
        this.state = {
            showAddress: false,
            selectedAddress: '',
            selectedAddressTag: '',
            sendAmount: '',
            showOTP: false,
            loading: false,
            minimum: 0,
            btcMinimum:0,
            ethMinimum:0,
            xrpMinimum:0,
            dgxMinimum:0,
            tusdMinimum: 0,
            usdtMinimum: 0,
            usdcMinimum: 0,
            paxMinimum: 0,
            gusdMinimum: 0,
            ttcMinimum: 0,
            swipeMinimum: 0,
            zilMinimum: 0,
            btcFee  : 0,
            ethFee  : 0,
            xrpFee  : 0,
            dgxFee  : 0,
            tusdFee : 0,
            usdtFee : 0,
            usdcFee : 0,
            paxFee  : 0,
            gusdFee : 0,
            ttcFee  : 0,
            swipeFee  : 0,
            zilFee  : 0,
            showSubmenu: false,
            loadingAddress: false,
            summary : {
                currency: '',
                totalAmount: 0,
                fee: 0,
                address: ''
            },
            limit: 0,
            available: 0,
            loading: false,
            loadingLimit: false,
            slide: 'withdraw', // withdraw, confirm,
            otpMethod: 'email',
            transactionComplete: false,
            allLimits: '',
            btcLimit: '',
            ethLimit: '',
            xrpLimit: '',
            dgxLimit: '',
            tusdLimit: '',
            usdtLimit: '',
            usdcLimit: '',
            paxLimit: '',
            gusdLimit: '',
            ttcLimit: '',
            zilLimit: ''
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            let currency = this.props.currency.toUpperCase()

            if ( currency === 'SWIPE' ){
                // regex normal
                const re2 = /^[1-9]\d*$/;;
                if (
                    event.target.value == '' || 
                    re2.test(event.target.value)
                ) { 
                    this.setState({ sendAmount: event.target.value })
                }
                
                this.updateAmountNumber();
            } else {
                if ( fieldName === 'sendAmount' ) {
                
                    // regex to prevent start with multiple '0'
                    const re3 = /(^[0]((\,|\.)\d*)?$)/;
                    // regex normal
                    const re2 = /(^[1-9][0-9]*((\,|\.)\d*)?$)/;
                    if (
                        event.target.value == '' || 
                        re2.test(event.target.value) ||
                        re3.test(event.target.value)
                    ) { 
                        this.setState({ sendAmount: event.target.value })
                    }
                    
                    this.updateAmountNumber();
                } 
                else {
                    obj[fieldName] = event.target.value
                    this.setState(obj)
                }
            }
        }

        this.updateAmountNumber = _.debounce(this.updateAmountNumber, 500);
    }

    updateAmountNumber = () => {
        let value = this.state.sendAmount.toString();
        this.setState({
          sendAmount: value.replace(/\./g, ",")
        });
    };

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    doSendTransaction(otpId, token) {
        this.setState({
            loading: true
        });
        let currency = this.props.currency.toLowerCase();
        let address;

        if(this.props.currency == 'XRP'){
            address = this.state.selectedAddress + "?dt=" + this.state.selectedAddressTag;
        } else {
            address = this.state.selectedAddress;
        }

        
        sendTranscation(this.getToken(), otpId, token, currency, address, Number( cryptoFormatterReplaceFormat( this.state.sendAmount )) ).then(res => {

            this.setState({
                selectedAddress: "",
                sendAmount: '0',
                loading: false,
                transactionComplete: true
            });
            if(this.props.onRefreshBalances){
                this.props.onRefreshBalances(this.props.currency.toUpperCase());
            }
            this.getLimit(this.props.currency)
            NotificationManager.success(strings.TRANSACTION_SUCCESS);
        })
            .catch(err => {
                this.setState({
                    loading: false,
                    showOTP: false,
                    selectedAddress: "",
                    sendAmount: '0',
                    loading: false,
                    transactionComplete: false
                });
                this.close(true);
                // if(err.response){
                //     NotificationManager.error(err.response.data.err);
                // } else {
                // }
                let details = {
                    type: 'COMPONENTS: sendModal | FUNC: doSendTransaction',
                    url: ''
                }
                NetworkError(err, details);
            })
    }

    toggleOTP() {
        let newStatus = !this.state.showOTP

        if(newStatus === false && this.state.transactionComplete === true){
            this.setState({
                sendAmount: '0',
                showAddress: false,
                slide: 'withdraw',
                selectedAddress: '',
                transactionComplete: false
            });
            this.close();
        }

        this.setState({
            showOTP: newStatus
        });
    }

    completeVerification(otpId, token) {
        // this.toggleOTP();
        this.doSendTransaction(otpId, token);
    }

    close(closeAll = false){

        let obj = {
            showAddress: false,
            slide: 'withdraw',
            transactionComplete: false,
            selectedAddress: ''
        }

        if(closeAll){
            obj.sendAmount = '0';
            obj.selectedAddress = "";
        }

        this.setState(obj);
        this.props.onClose();
    }

    createAddress() {
        this.setState({
            showSubmenu: false,
            loadingAddress: true
        });

        recieveActions.createAddress(this.getToken(), this.props.currency.toLowerCase()).then(res => {
            let all_addresses = this.state.addresses
            NotificationManager.success(strings.ADDRESS_CREATED);
            this.props.onNewAddressCreated();
            this.setState({
                loadingAddress: false
            });
        })
            .catch(err => {
                let details = {
                    type: 'COMPONENTS: sendModal | FUNC: createAddress',
                    url: ''
                }
                NetworkError(err, details);

                this.close();
                this.setState({
                    loadingAddress: false
                });
            })
    }

    submitAndRequestOTP(){

        if(cryptoFormaterUnformat( this.state.sendAmount ) <= this.state.limit){
            let fee = 0;

            if(this.props.currency == 'BTC' || this.props.currency == 'btc'){
                fee = this.state.btcFee
            } else if (this.props.currency == 'ETH' || this.props.currency == 'eth'){
                fee = this.state.ethFee
            } else if (this.props.currency == 'XRP' || this.props.currency == 'xrp'){
                fee = this.state.xrpFee
            } else if (this.props.currency == 'DGX' || this.props.currency == 'dgx'){
                fee = this.state.dgxFee
            } else if (this.props.currency == 'GUSD' || this.props.currency == 'gusd'){
                fee = this.state.gusdFee
            } else if (this.props.currency == 'TUSD' || this.props.currency == 'tusd'){
                fee = this.state.tusdFee
            } else if (this.props.currency == 'USDC' || this.props.currency == 'usdc'){
                fee = this.state.usdcFee
            } else if (this.props.currency == 'USDT' || this.props.currency == 'usdt'){
                fee = this.state.usdtFee
            } else if (this.props.currency == 'PAX' || this.props.currency == 'pax'){
                fee = this.state.paxFee
            } else if (this.props.currency == 'TTC' || this.props.currency == 'ttc'){
                fee = this.state.ttcFee
            } else if (this.props.currency == 'SWIPE' || this.props.currency == 'SWIPE'){
                fee = this.state.swipeFee
            } else if (this.props.currency == 'ZIL' || this.props.currency == 'ZIL'){
                fee = this.state.zilFee
            }
            

            if (this.props.currency == 'XRP' || this.props.currency == 'xrp'){
                let xrpAddress = this.state.selectedAddress + "?dt=" + this.state.selectedAddressTag;
                this.setState({
                    summary: {
                        currency: this.props.currency,
                        totalAmount: cryptoFormaterUnformat( this.state.sendAmount ),
                        fee: fee,
                        address: xrpAddress
                    }
                })
            } else {
                this.setState({
                    summary: {
                        currency: this.props.currency,
                        totalAmount: cryptoFormaterUnformat( this.state.sendAmount ),
                        fee: fee,
                        address: this.state.selectedAddress
                    }
                })
            }

            setTimeout(() => {
                this.setState({ showOTP: true })
            }, 100)
        } else {
            NotificationManager.error(strings.SEND_AMOUNT_EXCEED_DAILY_LIMIT);
        }

    }

    setLimit(){
        homeActions.getCurrencyUsage(this.getToken()).then(res => {
           
            let limit = res.data.data

            this.setState({
                allLimits: limit
            });

        }).catch(err => {
            
            NetworkError(err);
            let details = {
                type: 'COMPONENTS: sendModal | FUNC: setLimit',
                url: ''
            }
            NetworkError(err, details);

            this.setState({ loadingLimit: false});
        });
    }

    getLimit(currency){
        this.setState({ loadingLimit: true});

        let limits = this.state.allLimits

        if(limits){
            let limit = Number(limits[currency.toLowerCase()].dailyWithdrawalLimit);
            let usage = limit - Number(limits[currency.toLowerCase()].availableWithdrawAmountForTheDay);
    
            let percentage = (usage / limit ) * 100;
    
            this.setState({
                limit: Number(limits[currency.toLowerCase()].dailyWithdrawalLimit),
                available: Number(limits[currency.toLowerCase()].availableWithdrawAmountForTheDay),
                loadingLimit: false
            });
        }

    }

    fetchUserOTPSTatus(){

        this.setState({
            loadingMfa: true
        });

        OTPActions.getOTPStatus(this.getToken()).then(res => {

            

            let otpMethod = 'email';

            if(res.data.data.ga === true) {
                otpMethod = 'ga';
            }

            this.setState({
                otpMethod
            })
        }).catch(err => {
            
            let details = {
                type: 'COMPONENTS: sendModal | FUNC: fetchUserOTPStatus',
                url: ''
            }
            NetworkError(err, details);
            this.setState({
                loadingMfa: false
            });
        });
    }

    gotoAccounts(){
        this.close();
        history.push('/accounts?currency='+this.props.currency.toUpperCase())
    }

    getWithdrawFees(){
        getWithdrawFees(this.getToken()).then(res => {
            

            let fees = res.data.data
            
            
            this.setState({
                btcFee: fees.btc.fee,
                ethFee: fees.eth.fee,
                xrpFee: fees.xrp.fee,
                dgxFee: fees.dgx.fee,
                tusdFee: fees.tusd.fee,
                usdtFee: fees.usdt.fee,
                usdcFee: fees.usdc.fee,
                paxFee: fees.pax.fee,
                gusdFee: fees.gusd.fee,
                ttcFee: fees.ttc.fee,
                swipeFee: fees.swipe.fee,
                zilFee: fees.zil.fee
            })

        }).catch(err => {
            
            let details = {
                type: 'COMPONENTS: sendModal | FUNC: getWithdrawFees',
                url: ''
            }
            NetworkError(err, details);
        });
    }


    getWithdrawLimits(){
        getWithdrawLimits(this.getToken()).then(res => {
            
            let limits = res.data.data.min

            this.setState({
                btcMinimum: limits.btc,
                ethMinimum: limits.eth,
                xrpMinimum: limits.xrp,
                dgxMinimum: limits.dgx,
                tusdMinimum: limits.tusd,
                usdtMinimum: limits.usdt,
                usdcMinimum: limits.usdc,
                paxMinimum: limits.pax,
                gusdMinimum: limits.gusd,
                ttcMinimum: limits.ttc,
                swipeMinimum: limits.swipe,
                zilMinimum: limits.zil
            })

        }).catch(err => {
            let details = {
                type: 'COMPONENTS: sendModal | FUNC: getWithdrawLimits',
                url: ''
            }
            NetworkError(err, details);
        });
    }

    componentWillReceiveProps(newProps){
        if (newProps.currency !== this.props.currency) {
            this.getLimit(newProps.currency)
            this.setLimit()
        } else {
            if(this.state.limit === 0){
                this.getLimit(this.props.currency)
                this.setLimit()
            }
        }
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    componentDidMount(){
        this.setLimit();
        this.fetchUserOTPSTatus();
        this.getWithdrawFees();
        this.getWithdrawLimits();
    }

    onKeyDownAmount(e) {
        let key = e.keyCode;
        // Don't validate the input if below arrow, delete and backspace keys were pressed
        if(key === 37 || key === 38 || key === 39 || key === 40 || key === 8 || key === 46 || key === 188) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
            return;
        } else {
            let string = String.fromCharCode(e.keyCode) || "";

            if (!(/^(\d+(\,\d*)?)$/g).test(string)) {
                e.preventDefault();
            }
        }

    }

    validateTTCAddress(address) {
        let validator = /^t0([a-fA-F\d]){40}$/;
        return validator.test(address)
    }

    validateZILAddress( address ) {
        const validator = validation.isBech32(address);
        
        return validator;
    }

    render(){

        let availablePlaceholder = ""
        let availableWithdrawPlaceholder = ""
        let balance = 0;
        let availableBalance = 0;

        if(this.props.account) {
            availablePlaceholder = strings.AVAILABLE+": "+cryptoFormatterByCoin(this.props.account.Balance,this.props.currency)+" "+this.props.currency;
            availableWithdrawPlaceholder =  strings.AVAILABLE_FOR_WITHDRAW+": "+cryptoFormatterByCoin(this.props.account.UsableBalance,this.props.currency)+" "+this.props.currency;
            balance = this.props.account.Balance;
            availableBalance = this.props.account.UsableBalance;
        }

        let minimum = 0;

        if (this.props.currency == 'BTC' || this.props.currency == 'btc'){
            minimum = this.state.btcMinimum
        } else if (this.props.currency == 'ETH' || this.props.currency == 'eth'){
            minimum = this.state.ethMinimum
        } else if (this.props.currency == 'XRP' || this.props.currency == 'xrp'){
            minimum = this.state.xrpMinimum
        } else if (this.props.currency == 'DGX' || this.props.currency == 'dgx'){
            minimum = this.state.dgxMinimum
        } else if (this.props.currency == 'TUSD' || this.props.currency == 'tusd'){
            minimum = this.state.tusdMinimum
        } else if (this.props.currency == 'GUSD' || this.props.currency == 'gusd'){
            minimum = this.state.gusdMinimum
        } else if (this.props.currency == 'USDC' || this.props.currency == 'usdc'){
            minimum = this.state.usdcMinimum
        } else if (this.props.currency == 'USDT' || this.props.currency == 'usdt'){
            minimum = this.state.usdtMinimum
        } else if (this.props.currency == 'PAX' || this.props.currency == 'pax'){
            minimum = this.state.paxMinimum
        } else if (this.props.currency == 'TTC' || this.props.currency == 'ttc'){
            minimum = this.state.ttcMinimum
        } else if (this.props.currency == 'SWIPE' || this.props.currency == 'swipe'){
            minimum = this.state.swipeMinimum
        } else if (this.props.currency == 'ZIL' || this.props.currency == 'zil'){
            minimum = this.state.zilMinimum
        }

        let fee = 0;

        if (this.props.currency == 'BTC' || this.props.currency == 'btc'){
            fee = this.state.btcFee
        } else if (this.props.currency == 'ETH' || this.props.currency == 'eth'){
            fee = this.state.ethFee
        } else if (this.props.currency == 'XRP' || this.props.currency == 'xrp'){
            fee = this.state.xrpFee
        } else if (this.props.currency == 'DGX' || this.props.currency == 'dgx'){
            fee = this.state.dgxFee
        } else if (this.props.currency == 'TUSD' || this.props.currency == 'tusd'){
            fee = this.state.tusdFee
        } else if (this.props.currency == 'GUSD' || this.props.currency == 'gusd'){
            fee = this.state.gusdFee
        } else if (this.props.currency == 'USDC' || this.props.currency == 'usdc'){
            fee = this.state.usdcFee
        } else if (this.props.currency == 'USDT' || this.props.currency == 'usdt'){
            fee = this.state.usdtFee
        } else if (this.props.currency == 'PAX' || this.props.currency == 'pax'){
            fee = this.state.paxFee
        } else if (this.props.currency == 'TTC' || this.props.currency == 'ttc'){
            fee = this.state.ttcFee
        } else if (this.props.currency == 'SWIPE' || this.props.currency == 'swipe'){
            fee = this.state.swipeFee
        } else if (this.props.currency == 'ZIL' || this.props.currency == 'zil'){
            fee = this.state.zilFee
        }

        // if(this.props.currency === 'ETH'){
        //     fee = 0;
        // }

        let networkType = '';
        let currency = '';
        let invalidAddress = false;
        let invalidAddressTag = false;
        let invalidAddressMessage;

        if(this.props.currency.toUpperCase() != 'USDT'){
            if(this.state.selectedAddress.length > 0){

                if(process.env.REACT_APP_ENV == 'production'){
                    networkType = 'prod'
                } else{
                    networkType = 'testnet'
                }
    
                if(this.props.currency.toUpperCase() == 'BTC'){
                    currency = 'BTC'
                } else if (this.props.currency.toUpperCase() == 'XRP') {
                    currency = 'XRP'
                } else if (this.props.currency.toUpperCase() == 'TTC') {
                    currency = 'TTC'
                } else {
                    currency = 'ETH'
                }
    
                var valid;
                if ( currency == 'TTC' ) {
                    valid = this.validateTTCAddress(this.state.selectedAddress);
                } else if ( this.props.currency === 'ZIL'){
                    valid = this.validateZILAddress(this.state.selectedAddress)
                } else {
                    valid = WAValidator.validate(this.state.selectedAddress, currency, networkType);
                }

                if(valid){
                    invalidAddress = true
                } else {
                    invalidAddress = false
                }
    
            } else {
                invalidAddress = true
            }
        } else {
            invalidAddress = true
        }
        

        if(this.props.currency == 'BTC' || this.props.currency == 'btc'){
            invalidAddressMessage = strings.INVALID_BTC_ADDRESS;
        } else if (this.props.currency == 'ETH' || this.props.currency == 'eth'){
            invalidAddressMessage = strings.INVALID_ETH_ADDRESS;
        } else if (this.props.currency == 'XRP' || this.props.currency == 'xrp'){
            invalidAddressMessage = strings.INVALID_XRP_ADDRESS;
        } else if (this.props.currency == 'DGX' || this.props.currency == 'dgx'){
            invalidAddressMessage = strings.INVALID_DGX_ADDRESS;
        } else if (this.props.currency == 'PAX' || this.props.currency == 'pax'){
            invalidAddressMessage = strings.INVALID_PAX_ADDRESS;
        } else if (this.props.currency == 'GUSD' || this.props.currency == 'gusd'){
            invalidAddressMessage = strings.INVALID_GUSD_ADDRESS;
        } else if (this.props.currency == 'TUSD' || this.props.currency == 'tusd'){
            invalidAddressMessage = strings.INVALID_TUSD_ADDRESS;
        } else if (this.props.currency == 'USDT' || this.props.currency == 'usdt'){
            invalidAddressMessage = strings.INVALID_USDT_ADDRESS;
        } else if (this.props.currency == 'USDC' || this.props.currency == 'usdc'){
            invalidAddressMessage = strings.INVALID_USDC_ADDRESS;
        } else if (this.props.currency == 'TTC' || this.props.currency == 'ttc'){
            invalidAddressMessage = strings.INVALID_TTC_ADDRESS;
        } else if (this.props.currency == 'SWIPE' || this.props.currency == 'swipe'){
            invalidAddressMessage = strings.INVALID_SWIPE_ADDRESS;
        } else if (this.props.currency == 'ZIL' || this.props.currency == 'zil'){
            invalidAddressMessage = strings.INVALID_ZIL_ADDRESS;
        }

        return (
            <div>
                <Modal isOpen={this.props.show} toggle={() => this.close(true)} className="modal-dialog-centered">
                    <ModalHeader toggle={() => this.close(true)}>
                        {strings.WITHDRAWAL}
                    </ModalHeader>
                    <ModalBody className="modal-body tc-modal-body">
                        { this.state.slide === 'withdraw' &&
                        <div>
                            <div style={{ fontWeight: 500, fontSize: "11px", lineHeight: 1.9 }}>
                                    <b>{strings.IMPORTANT}</b><br />
                                    {this.props.currency == 'ETH' ?
                                        <span>
                                            - {strings.MINIMUM_WITHDRAWAL} { minimum } { this.props.currency }.<br />
                                            - {strings.DO_NOT_WITHDRAW_WARNINNG}.<br />
                                            - {strings.PLEASE_NOTE_THAT_ETH_IMPORTANT}.
                                        </span>
                                    :   <span>
                                            - {strings.MINIMUM_WITHDRAWAL} { minimum } { this.props.currency }.<br />
                                            - {strings.DO_NOT_WITHDRAW_WARNINNG}.
                                        </span> 
                                    }
                                </div>
                                {this.props.currency == 'BTC' || this.props.currency == 'ETH' || this.props.currency == 'DGX' || this.props.currency == 'GUSD' || this.props.currency == 'PAX' || this.props.currency == 'TUSD' || this.props.currency == 'USDC' || this.props.currency == 'USDT' || this.props.currency == 'TTC' || this.props.currency == 'SWIPE' || this.props.currency == 'ZIL' ?
                                    <div className="form-group m-form__group">
                                        <label style={{ marginTop: "20px", fontWeight: 600 }} htmlFor="exampleSelect1">{ this.props.currency } {strings.WITHDRAWAL_ADDRESS}</label><br />
                                            <input disabled={this.state.loading || this.state.loadingAddress} className={classnames({
                                                    'form-control': true,
                                                    'is-invalid': invalidAddress == false
                                                })}
                                                onChange={(e) => this.setState({ selectedAddress: e.target.value})}
                                                value={this.state.selectedAddress}
                                            />
                                            { this.state.loadingAddress && <span>{strings.CREATING_ADDRESS} ...</span> }
                                            { invalidAddress == false &&
                                                <div class="invalid-feedback" style={{ color: '#f4516c', fontSize:'13px', fontWeight:'500', marginTop: '7px'}}>
                                                    {invalidAddressMessage}
                                                </div>
                                            }
                                    </div> : null
                                }
                                {this.props.currency == 'XRP' ?
                                    <span>
                                        <div className="form-group m-form__group" style={{marginBottom:"0px"}}>
                                            <label style={{ marginTop: "20px", fontWeight: 600 }} htmlFor="exampleSelect1">{ this.props.currency } {strings.WITHDRAWAL_ADDRESS}</label><br />
                                                <input disabled={this.state.loading || this.state.loadingAddress} className={classnames({
                                                        'form-control': true,
                                                        'is-invalid': invalidAddress == false
                                                    })}
                                                    onChange={(e) => this.setState({ selectedAddress: e.target.value})}
                                                    value={this.state.selectedAddress}
                                                />
                                                { this.state.loadingAddress && <span>{strings.CREATING_ADDRESS} ...</span> }
                                                { invalidAddress == false &&
                                                    <div class="invalid-feedback" style={{ color: '#f4516c', fontSize:'13px', fontWeight:'500', marginTop: '7px'}}>
                                                        {invalidAddressMessage}
                                                    </div>
                                                }
                                        </div>
                                        <div className="form-group m-form__group">
                                            <label style={{ marginTop: "20px", fontWeight: 600 }} htmlFor="exampleSelect1">{strings.DEPOSIT_TAG}</label><br />
                                                <input disabled={this.state.loading || this.state.loadingAddress} className={classnames({
                                                        'form-control': true,
                                                        'is-invalid': invalidAddressTag == true
                                                    })}
                                                    onChange={(e) => this.setState({ selectedAddressTag: e.target.value})}
                                                    value={this.state.selectedAddressTag}
                                                />
                                                { this.state.loadingAddress && <span>{strings.CREATING_ADDRESS} ...</span> }
                                        </div>
                                    </span> : null
                                }
                                <div style={{ fontWeight: 500, fontSize: "13px", marginTop: "24px" }}>
                                    <span>{strings.AMOUNT} </span><span style={{ float: "right" }}><span style={{ fontWeight: 200 }}>{strings.WITHDRAWAL_LIMIT_REMAINING} :&nbsp;</span><b>{cryptoFormatterByCoin(this.state.available, this.props.currency)}/{cryptoFormatterByCoin(this.state.limit, this.props.currency)} {this.props.currency}</b></span>
                                    <div className="input-group" style={{ marginTop: "12px" }}>
                                        <input
                                            type="text"
                                            className={classnames({
                                                'form-control m-input': true,
                                                'is-invalid': Number(cryptoFormatterReplaceFormat( this.state.sendAmount)) > availableBalance
                                            })}
                                            value={ this.state.sendAmount || '' }
                                            // onKeyDown={(e) => this.onKeyDownAmount(e)}
                                            onChange={this.handleChange('sendAmount')}
                                            placeholder={ "Enter Amount Here" }
                                            aria-describedby="basic-addon2"
                                            disabled={this.state.loading || this.state.loadingAddress}
                                        />
                                        <div className="input-group-append" style={{ padding: ".65rem 1.25rem", border: "1px solid rgba(0,0,0,.15)", borderRadius: ".25rem", backgroundColor: "#f4f5f8",
                                        color: "gray" }}><span className="input-group-text" id="basic-addon2">{ this.props.currency }</span></div>
                                    </div>
                                    <div class="balance" style={{ marginTop: "10px"}}><b>{availablePlaceholder}</b></div>
                                    <div class="balance" style={{ marginTop: "5px"}}><b>{availableWithdrawPlaceholder}</b></div>
                                    { Number(cryptoFormatterReplaceFormat( this.state.sendAmount )) > availableBalance && this.state.sendAmount !== ''?
                                        <div class="invalid-feedback" style={{ color: '#f4516c'}}>
                                            {strings.INSUFFCIENT_WITHDRAWAL_BALANCE}
                                        </div> : null
                                    }
                                    { Number(cryptoFormatterReplaceFormat( this.state.sendAmount )) < minimum && this.state.sendAmount !== '' && this.state.sendAmount !== 0 ?
                                        <div class="invalid-feedback" style={{ color: '#f4516c'}}>
                                            {strings.MINIMUM_AMOUNT_IS} { cryptoFormatterByCoin( minimum, this.props.currency ) } { this.props.currency }
                                        </div> : null
                                    }
                                    { Number(cryptoFormatterReplaceFormat( this.state.sendAmount )) > this.state.available && this.state.sendAmount !== '' && this.state.sendAmount !== 0 ?
                                        <div class="invalid-feedback" style={{ color: '#f4516c'}}>
                                            {strings.YOUR_DAILY_LIMIT_REAINING_IS} {cryptoFormatterByCoin(this.state.available, this.props.currency)} {this.props.currency}
                                        </div> : null
                                    }
                                </div>
                                <div style={{ fontWeight: 500, fontSize: "13px", marginTop: "10px", marginBottom: "-12px" }}>
                                    <span><span style={{ fontWeight: 200 }}>{strings.WITHDRAWAL_FEE}:</span>{fee} &nbsp;{this.props.currency}</span>
                                    { 
                                        this.state.sendAmount!=''
                                        && cryptoFormatterReplaceFormat( this.state.sendAmount) != 0 
                                        && cryptoFormatterReplaceFormat( this.state.sendAmount ) != "" 
                                        && (cryptoFormatterReplaceFormat( this.state.sendAmount) - fee) >= 0 ?  
                                        <span style={{ float: "right" }}><span style={{ fontWeight: 200}}>{strings.AMOUNT_SENT}:</span> { cryptoFormatterByCoin( NP.minus(cryptoFormaterUnformat( this.state.sendAmount ), fee), this.props.currency)}&nbsp;{this.props.currency}</span> 
                                        :
                                        <span style={{ float: "right" }}><span style={{ fontWeight: 200}}>{strings.AMOUNT_SENT}:</span> - {this.props.currency} </span>
                                    }
                                </div>
                                {this.state.loading ?
                                    <div style={{ marginLeft: "45%", marginTop: "5%"}}><Loader /></div> :
                                    <button disabled={ Number(cryptoFormatterReplaceFormat( this.state.sendAmount )) < minimum || (cryptoFormatterReplaceFormat( this.state.sendAmount) - fee) < 0 || (Number(cryptoFormatterReplaceFormat( this.state.sendAmount)) > this.state.available) || this.state.loadingLimit || Number(cryptoFormatterReplaceFormat( this.state.sendAmount)) > availableBalance || cryptoFormatterReplaceFormat( this.state.sendAmount) === 0 || this.state.selectedAddress.length === 0 || invalidAddress == false || this.props.currency === 'XRP' && this.state.selectedAddressTag.length === 0 } onClick={() => this.submitAndRequestOTP()} style={{ marginTop: "25px", marginBottom: "20px" }} type="button" className="btn btn-blue btn-block">{strings.SUBMIT}</button>
                                }
                        </div>}
                    </ModalBody>
                </Modal>

                <OTPModal
                    currency={this.props.currency}
                    method={this.state.otpMethod}
                    summary={this.state.summary}
                    show={this.state.showOTP}
                    toggleShow={this.toggleOTP.bind(this)}
                    completeVerification={this.completeVerification.bind(this)}
                    transactionComplete={this.state.transactionComplete}
                />
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        language: state.language
    };
};

export default connect(mapStateToProps, null) (SendModal);
