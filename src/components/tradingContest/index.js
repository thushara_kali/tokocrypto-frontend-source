import React, { Component } from 'react'
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Card,
    CardTitle,
    CardText,
    Row,
    Col
} from 'reactstrap'
import { getUserAttributes, updateAttributes } from '../../core/react-cognito/attributes.js'
import { connect } from 'react-redux'
import actions from '../../core/actions/client/banks'
import LocalizedStrings from 'react-localization'
import enDictionary from '../../languages/en.json'
import idDictionary from '../../languages/id.json'
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import classnames from 'classnames';
import homeActions from '../../core/actions/client/home';
import {faSignInAlt, faSignOutAlt, faPaperPlane, faQrcode} from '@fortawesome/fontawesome-free-solid';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import amountFormatter from '../../helpers/amountFormatter';
import NetworkError from '../../errors/networkError';

// import { Loader } from '../tcLoader';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class tradingContest extends Component {

    constructor() {
        super()
        let n = new Date()
        let date_now = n.getFullYear()+'-'+ (n.getMonth()+1) +'-'+n.getDate();
        this.state = {
            banksdata: [],
            bankName: '',
            fullName: '',
            label: '',
            accountNumber: '',
            instruction: false,
            activeTab: 'mobile',
            refreshDataContest: true,
            listTrader: [],
            listTraderTable: [],
            topTrade:[],
            topThreeTrade:[],
            haveBefore: false,
            date_now : date_now,
            lodingTrading : true
        }

    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    onSetLanguage() {
        strings.setLanguage(this.props.language)
    }

    replaceRange(s, start, end, substitute) {
        return s.substring(0, start) + substitute + s.substring(end);
    }

    get_payload_data(type = 0){
        let data=[];
        const n = new Date()
        const date_now = n.getFullYear()+'-'+ (n.getMonth()+1) +'-'+n.getDate();

        if(type === 0){
            if(date_now < '2020-1-23'){
                data = {
                    "enddate": "2020-01-22 16:59",
                    "startdate": "2020-01-16 17:00",
                    "max_count" : 10,
                    "basecurrency" : "ZIL"
                }
            }else if(date_now < '2020-1-30'){
                data = {
                    "enddate": "2020-01-29 16:59",
                    "startdate": "2020-01-23 17:00",
                    "max_count" : 10,
                    "basecurrency" : "ZIL"
                }
            }else if(date_now < '2020-2-06'){
                data = {
                    "enddate": "2020-02-05 16:59",
                    "startdate": "2020-01-30 17:00",
                    "max_count" : 10,
                    "basecurrency" : "ZIL"
                }
            }

            if(process.env.REACT_APP_ENV === 'demo' || process.env.REACT_APP_ENV === 'local'){
                data = {
                    "enddate": "2020-02-05",
                    "startdate": "2019-01-30",
                    "max_count" : 10,
                    "basecurrency" : "ZIL"
                }
            }
        }
        // else if (type === 1){
        //     if(this.state.date_now < '2019-5-31'){
        //         data = {
        //             "enddate": "2019-05-23",
        //             "startdate": "2019-05-17",
        //             "max_count" : 3
        //         }
        //     }else if(this.state.date_now < '2019-6-7'){
        //         data = {
        //             "enddate": "2019-05-30",
        //             "startdate": "2019-05-24",
        //             "max_count" : 3
        //         }
        //     }else if(this.state.date_now < '2019-6-14'){
        //         data = {
        //             "enddate": "2019-06-06",
        //             "startdate": "2019-05-31",
        //             "max_count" : 3
        //         }
        //     }
            
        //     if(process.env.REACT_APP_ENV === 'dev' || process.env.REACT_APP_ENV === 'local'){
        //         data = {
        //             "enddate": "2019-05-22",
        //             "startdate": "2019-03-16",
        //             "max_count" : 3
        //         }
        //     }
        // }
        
        return data;
    }

    async fetchDataTrading(type = 0) {
        
        let data = await this.get_payload_data(type)        
        
        homeActions.getTopTrading(this.getToken(), data).then(res => {
            if(type === 0){
                this.setState({topTrade: res.data.data, refreshDataContest: false})
            }else if(type === 1){
                this.setState({topThreeTrade: res.data.data, refreshDataContest: false})
            }
        }).catch(err => {
            NetworkError(err);
            
        });
    }

    componentWillReceiveProps(nextProps) {
        // You don't have to do this check first, but it can help prevent an unneeded render
        if (nextProps.show === true) {
            this.fetchDataTrading();
        }
    }

    parsingData(){
        // if(this.state.topTrade.length < 0){
        //     return;
        // }
        let j = 0;
        let user1 = '';
        let total1 = 0;
        let user2 = '';
        let total2 = 0;
        
        return this.state.topTrade.map(function (data){
            let position = 0;
            let email = '';
            email = (data.email) ? data.email : 'mail@domain.com';
            position = email.indexOf("@");
            let replace = '';
            if (position > 3) {
                for (var i = 3; i < position; i++) {
                    replace = replace + '*';
                }
            }
            let user = email.substring(0, 3) + replace + email.substring(position);
            j++;
            
            if (j === 3) {
                return (
                    <div className="col-md-12" style={{display: 'flex', flexDirection: 'row', paddingBottom:"30px"}}>
                        <div className="col-md-4">
                            <div className="row">
                                <div className="col-md-12" style={{display: 'flex',  justifyContent:'center',flexDirection: 'column',alignItems:"center", padding:"0px 10px"}}>
                                    <img className="img-winner-competition" src="../../assets/media/img/competition/1.png" alt="" />
                                    <div>
                                        {user1}
                                    </div>
                                    <span className="winner-span">Rp 3.000.000</span>
                                    <div>
                                        <div className="col-md-12 idr-winner">
                                            IDR {amountFormatter(total1)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="row">
                                <div className="col-md-12" style={{display: 'flex',  justifyContent:'center',flexDirection: 'column',alignItems:"center", padding:"0px 10px"}}>
                                    <img className="img-winner-competition" src="../../assets/media/img/competition/2.png" alt="" />
                                    <div>
                                        {user2}
                                    </div>
                                    <span className="winner-span">Rp 1.500.000</span>
                                    <div>
                                        <div className="col-md-12 idr-winner">
                                            IDR {amountFormatter(total2)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="row">
                                <div className="col-md-12" style={{display: 'flex',  justifyContent:'center',flexDirection: 'column',alignItems:"center", padding:"0px 10px"}}>
                                    <img className="img-winner-competition" src="../../assets/media/img/competition/3.png" alt="" />
                                    <div>
                                        {user}
                                    </div>
                                    <span className="winner-span">Rp 500.000</span>
                                    <div>
                                        <div className="col-md-12 idr-winner">
                                            IDR {amountFormatter(data.totalTrade)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }
            if(j===1){
                user1 = user;
                total1 = data.totalTrade;
            }
            if(j===2){
                user2 = user;
                total2 = data.totalTrade;
            }
            if (j > 3) {
                return (
                    <div className="col-md-12 tc-competition-mid">
                        <div className="col-md-1" style={{ textAlign: "center", lineHeight: "1.4" }}>
                            <span className="" style={{ fontSize: "12px" }}>
                                {j}
                            </span> <br />
                        </div>
                        <div className="col-xl-5" style={{ paddingLeft: "0px" }}>
                            <span>
                                {user}
                            </span>
                        </div>
                        <div className="col-xl-5" style={{ paddingLeft: "0px", textAlign: "right" }}>
                            <span>
                                IDR {amountFormatter(data.totalTrade)}
                            </span>
                        </div>
                    </div>
                )
            }
        });
    }

    parsingData_top(){
        if(this.state.topThreeTrade.length < 0){
            return;
        }
        let j = 0;
        
        return this.state.topThreeTrade.map(function (data){
            let position = 0;
            let email = '';
            email = (data.email) ? data.email : 'mail@domain.com';
            position = email.indexOf("@");
            let replace = '';
            if (position > 3) {
                for (var i = 3; i < position; i++) {
                    replace = replace + '*';
                }
            }
            let user = email.substring(0, 3) + replace + email.substring(position);
            j++;
            let img_path = "assets/media/img/competition/p-"+ j +".png"
            return (
                <div className="col-md-4">
                    <div className="row">
                        <div className="col-md-12" style={{display: 'flex',  justifyContent:'center',flexDirection: 'column',alignItems:"center"}}>
                            <img className="img-winner-competition" src={img_path} alt="" />
                            <div>
                                {user}
                            </div>
                            <span className="winner-span">Rp 1.000.000</span>
                            <div>
                                <div className="col-md-12 idr-winner">
                                    IDR {amountFormatter(data.totalTrade)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        });
    }
    componentDidMount() {
        
        this.fetchDataTrading()
        // let n = new Date()
        // let date_now = n.getFullYear()+'-'+ (n.getMonth()+1) +'-'+n.getDate();
        // if(date_now > '2019-5-23'){
        //     this.fetchDataTrading(1)
        // }
    }

    close() {
        this.props.toggleShow()
    }

    toggle(activeTab) {
        this.setState({
            activeTab
        })
    }

    render() {
        const n = new Date()
        const date_now = n.getFullYear()+'-'+ (n.getMonth()+1) +'-'+n.getDate();
        let subheader = '';
        if(date_now < '2020-1-23'){
            subheader = '16 Jan - 22 Jan 2020';
        }else if(date_now < '2020-1-30'){
            subheader = '23 Jan - 29 Jan 2020';
        }else if(date_now < '2020-2-06'){
            subheader = '30 Jan - 5 Feb 2020';
        }
        return (
            <div>
                <Modal isOpen={this.props.show} toggle={() => this.close()}
                       className={classnames({'modal-dialog-centered': !this.state.instruction})} size="lg">
                    <ModalHeader className="tc-modal-header-competition" toggle={() => this.close()}>
                        <span className="tc-modal-title-competition">
                            {/* Zilliqa Trading Competion */}
                            <img style={{ width: "40%" }} src="../../assets/media/img/competition/header.png" alt="" />
                        </span>
                    </ModalHeader>
                    {this.props.show === true && <ModalBody className="deposit-modal-idr tc-modal-body-competition" >
                        {/* <div className="row" style={{ width: "700px", justifyContent:"center" }}>
                            <img src="../../assets/media/img/competition/header.png" alt="" />
                        </div> */}
                        <div className="row" style={{ width: "700px", justifyContent:"center" }}>
                            <div className="row top-title-competition">
                                <img className="blink-image" style={{ width: "8%" }} src="../../assets/media/img/competition/radio.png" alt="" />
                                {subheader}
                            </div>
                        </div>
                        <div className="row" style={{ width: "700px" }}>
                            {
                                this.parsingData()
                            }
                        </div>
                        {/* <div className="row" style={{ width: "700px", justifyContent:"center", paddingTop:"20px" }}>
                            <div className="row top-title-competition">
                                Previous Winner
                            </div>
                        </div>
                        <div className="row" style={{ width: "700px" }}>
                            <div className="col-md-12" style={{display: 'flex', flexDirection: 'row', paddingBottom:"30px"}}>
                            {
                                this.parsingData_top()
                            }
                            </div>
                        </div> */}
                    </ModalBody>}
                </Modal>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        language: state.language
    }
}

const mapDispatchToProps = dispatch => ({
    fetchBankDepositAccounts: actions.fetchBankDepositAccounts
})

export default connect(mapStateToProps, mapDispatchToProps, null)(tradingContest)
