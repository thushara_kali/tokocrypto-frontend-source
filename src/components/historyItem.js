import React, { Component } from 'react'
import moment from 'moment'
import numeral from 'numeral'

class HistoryItem extends Component {

  render() {

    let userCurrency = ""
    let dealerCurrency = ""
    if (this.props.transaction.Action == 'Sell') {
      userCurrency = this.props.transaction.SellCurrency
      dealerCurrency = this.props.transaction.BuyCurrency
    } else {
      userCurrency = this.props.transaction.BuyCurrency
      dealerCurrency = this.props.transaction.SellCurrency
    }

    let userAmount = 0
    let dealerAmount = 0
    if (this.props.transaction.Action == 'Sell') {
      userAmount = this.props.transaction.SellAmount
      dealerAmount = this.props.transaction.BuyAmount
    } else {
      userAmount = this.props.transaction.BuyAmount
      dealerAmount = this.props.transaction.SellAmount
    }

    let userRate = 0
    if (this.props.transaction.Action == 'Sell') {
      userRate = this.props.transaction.Rate
    } else {
      userRate = this.props.transaction.Rate
    }

    return (
      <div className="history-item-conversions">
        <div className="row">
         
          <div className="col-md-8 col-6 history-coversions-details hidden-sm-down">
            <div>{userCurrency} {userAmount}</div>
            <div className="history-coversions-dealer"> {dealerCurrency} {numeral(dealerAmount).format('0,0')} at 1 {userCurrency} =  {dealerCurrency} {numeral(userRate).format('0,0')}</div>
            <div className="btn-history history-conversions-date">{moment(this.props.transaction.CreatedAt).format('lll')}</div>
          </div>
          <div className="col-md-2 col-3 history-conversions-buttons">
          {this.props.transaction.Action == 'Sell' ?
            <div className="btn-history-sell">Sell</div> :
            <div className="btn-history-buy">Buy</div>}
          </div>
          <div className="col-md-8 col-12 hidden-sm-up">
            <br />
            <h5>{userCurrency} {userAmount}</h5>
            <div className="history-coversions-dealer"> {dealerCurrency} {numeral(dealerAmount).format('0,0')} at 1 {userCurrency} =  {dealerCurrency} {numeral(userRate).format('0,0')}</div>
          </div>
        </div>
      </div >
    )
  }

}

export default HistoryItem