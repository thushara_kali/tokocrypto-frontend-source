import React, { Component } from 'react';
import history from '../history'
import numeral from 'numeral'
import _ from 'lodash'
import { connect } from 'react-redux';
import { toFixedNoRounding } from '../helpers/numbers';
import Skeleton from 'react-loading-skeleton';
import QRCode from 'qrcode.react';

class Coin extends Component {

    recieveCoin(coin) {
        history.push('/recieve/' + coin)
    }

    sendCoin(coin) {
        history.push('/send/' + coin, { balance: this.props.coin.Balance })
    }

    depositMoney(coin) {
        if (this.props.kyc.status == 'approved') {
            history.push('/deposit/' + coin)
        } else {
            history.push('/verifyaccount');
        }
    }

    withdrawMoney(coin) {
        if (this.props.kyc.status == 'approved') {
            history.push('/withdraw/' + coin + '/method', { balance: this.props.coin.Balance })
        } else {
            history.push('/verifyaccount');
        }
    }

    render() {

        let coinImage = null
        switch (this.props.coin.Currency.Code) {
            case 'BTC':
                coinImage = (<img className="accounts-header-image" src={require('../img/opengraph.png')} />)
                break;
            case 'ETH':
                coinImage = (<img className="accounts-header-image" src={require('../img/ethereum.png')} />)
                break;
            default:
                break;
        }

        let rate = _.find(this.props.rates, { currencyPair: this.props.coin.Currency.Code + 'IDR' })

        return (
            <div style={{width:'100%'}}>
               

                {this.props.coin.Currency.Code == 'IDR' ?
                <div className="idr-balance-container">
                
                    <div className="inside-container">
                    <div className="idr-balance">
                        <img className="idr-flag-img" src={require('../img/id.png')} /> <span style={{color:'#b8c0cd'}}>Indonesian Rupiah</span> <br/> <div>IDR {numeral(this.props.coin.Balance).format('0,0')}</div>
                    </div>
                    {/* <div className="buttons-container-hr">
                        <div onClick={() => this.depositMoney(this.props.coin.Currency.Code)} className="buttons-trasnactions"> 
                            <img className="" src={require('../img/inbox.png')} />
                            <div>Deposit</div>
                        </div>
                        <div onClick={() => this.withdrawMoney(this.props.coin.Currency.Code)}  className="buttons-trasnactions">
                            <img className="" src={require('../img/paper-plane.png')} />
                            <div>Withdraw</div>
                        </div>    
                    </div> */}
                    <div className="main-account-buttons">
                        <button  onClick={() => this.depositMoney(this.props.coin.Currency.Code)}>Deposit</button>
                        <button onClick={() => this.withdrawMoney(this.props.coin.Currency.Code)}>Withdraw</button>
                    </div>
                    </div> 
                </div>: 

                <div className="balance-container">
                <div className="virtual-inside-container">
                    <div className="row">
                    <div className="col-md-7">
                        <div className="virtual-two-containers">
                            <div className="col-md-12">

                                <div className="data-container row">

                                    <div className="accounts-header">
                                        {coinImage}
                                        <div className="accounts-header-text">{this.props.coin.Currency.Name}</div>
                                    </div>

                                    <div className="account-balance">{toFixedNoRounding(this.props.coin.Balance, 4)}<span style={{fontWeight:500}}> {this.props.coin.Currency.Code}</span></div>
                                    <div className="account-rate-balance"><span style={{color:"#b2b2b2"}}>IDR</span> <span style={{color:"#8e8d8d"}}>{numeral(this.props.coin.Balance * rate.buy).format('0,0')}</span></div>
                            
                                </div>

                            </div>
                            <div className="col-md-12">

                                <div className="data-container row">

                                    <div onClick={() => this.recieveCoin(this.props.coin.Currency.Code)} className="wallet-buttons"> 
                                       
                                        {/* <img className="" src={require('../img/inbox.png')} /> */}
                                        Recieve
                                        
                                    </div>

                                    <div onClick={() => this.sendCoin(this.props.coin.Currency.Code)} className="wallet-buttons"> 
                                       
                                        {/* <img className="" src={require('../img/paper-plane.png')} /> */}
                                        Send
                                         
                                    </div>

                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <QRCode value={this.props.coin.AccountId} />
                    </div>
                    </div>
                </div> 
                </div> }
               

                {/* {this.props.coin.Currency.Type == 'virtual' ?
                <div className="balance-container">
                    <div className="virtual-inside-container">
                        <div className="col-md-12">
                            <div className="virtual-two-containers">
                                <div className="col-md-8">

                                    <div className="data-container row">

                                        <div className="accounts-header">
                                            {coinImage}
                                            <div className="accounts-header-text">{this.props.coin.Currency.Name}</div>
                                        </div>

                                        <div className="account-balance">{toFixedNoRounding(this.props.coin.Balance, 4)}<span style={{fontWeight:500}}> {this.props.coin.Currency.Code}</span></div>
                                        <div className="account-rate-balance"><span style={{color:"#b2b2b2"}}>IDR</span> <span style={{color:"#8e8d8d"}}>{numeral(this.props.coin.Balance * rate.buy).format('0,0')}</span></div>
                                   
                                    </div>

                                </div>
                                <div className="col-md-4">

                                    <div className="data-container row">

                                        <div onClick={() => this.recieveCoin(this.props.coin.Currency.Code)} className="virtual-buttons-trasnactions"> 
                                            <img className="" src={require('../img/inbox.png')} />
                                            <div>Recieve</div>
                                        </div>

                                        <div onClick={() => this.sendCoin(this.props.coin.Currency.Code)} className="virtual-buttons-trasnactions"> 
                                            <img className="" src={require('../img/paper-plane.png')} />
                                            <div>Send</div>
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> : null} */}

                {/* <div className="row coin-row">
                    <div className="col-md-4 col-12">
                        <div className="row">
                            {coinImage}
                            <div className="coin-name">
                                <h5>{this.props.coin.Currency.Code}</h5>
                                <h6>{this.props.coin.Currency.Name}</h6>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 col-12">
                        <div className="row">
                            <div className="col-md-12 col-12">
                                <h6>{this.props.coin.code} Balance</h6>
                                {this.props.coin.Currency.Type == 'fiat' ?
                                    <h6> <span className="primary-coin">{coinIcon} {numeral(this.props.coin.Balance).format('0,0')}</span></h6> :
                                    <h6> <span className="primary-coin">{coinIcon} {toFixedNoRounding(this.props.coin.Balance, 4)}</span> = IDR {numeral(this.props.coin.Balance * rate.buy).format('0,0')}</h6>}
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 col-12">
                        <div className="row">
                            {this.props.coin.Currency.Type == 'virtual' ?
                                <div onClick={() => this.recieveCoin(this.props.coin.Currency.Code)} className="send-recieve-btn">
                                    <i className="fa fa-download" aria-hidden="true"></i> Recieve {this.props.coin.Currency.Code}
                                </div> : null}
                            {this.props.coin.Currency.Type == 'virtual' ?
                                <div onClick={() => this.sendCoin(this.props.coin.Currency.Code)} className="send-recieve-btn">
                                    <i className="fa fa-send" aria-hidden="true"></i> Send {this.props.coin.Currency.Code}
                                </div> : null}

                            {this.props.coin.Currency.Type == 'fiat' ?
                                <div onClick={() => this.depositMoney(this.props.coin.Currency.Code)} className="send-recieve-btn">
                                    <i className="fa fa-download" aria-hidden="true"></i> Deposit {this.props.coin.Currency.Code}
                                </div> : null}
                            {this.props.coin.Currency.Type == 'fiat' ?
                                <div onClick={() => this.withdrawMoney(this.props.coin.Currency.Code)} className="send-recieve-btn">
                                    <i className="fa fa-send" aria-hidden="true"></i> Withdraw {this.props.coin.Currency.Code}
                                </div> : null}
                        </div>
                    </div>
                </div> */}
            </div >
        )
    }

}

const mapStateToProps = (state) => {
    return {
        kyc: state.kyc
    };
};

export default connect(mapStateToProps, null)(Coin)
