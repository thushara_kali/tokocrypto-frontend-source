import React from 'react';

const Footer = () => (
  // <footer className="readmin-footer">
  //   <p>Copyright © Re Toko Crypto 2017</p>
  // </footer>
  <footer className="m-grid__item m-footer ">
    <div className="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
      <div className="m-footer__wrapper">
        <div className="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
          <div className="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
            <span className="m-footer__copyright">
              2017 © Made with love
              <a href="#" class="m-link">
                by Tokocrypto
              </a>
            </span>
          </div>
          <div className="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
            <ul className="m-footer__nav m-nav m-nav--inline m--pull-right">
              <li className="m-nav__item">
                <a href="#" className="m-nav__link">
                  <span className="m-nav__link-text">
                    About
                  </span>
                </a>
              </li>
              <li className="m-nav__item">
                <a href="#" className="m-nav__link">
                  <span className="m-nav__link-text">
                    Privacy
                  </span>
                </a>
              </li>
              <li className="m-nav__item">
                <a href="#" className="m-nav__link">
                  <span className="m-nav__link-text">
                    T&amp;C
                  </span>
                </a>
              </li>

              <li className="m-nav__item m-nav__item--last">
                <a href="#" className="m-nav__link" data-toggle="m-tooltip" title="" data-placement="left" data-original-title="Support Center">
                  <i className="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
);

export default Footer;
