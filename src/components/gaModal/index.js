import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import OTPActions from '../../core/actions/client/otp';
import { NotificationManager } from 'react-notifications';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import queryString from 'querystring';
import urlencode from 'urlencode';
import QRCode from 'qrcode.react';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import NetworkError from '../../errors/networkError';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class GAModal extends Component {

  constructor() {
    super();
    this.state = {
        qr: "",
        secret: "",
        userToken: "",
        enabling: false,
        requested: false,
        copied: false
    }
  }

  getToken() {
    let username = this.props.user.username
    let clientId = this.props.user.pool.clientId
    let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
    return token
  }

  onSetLanguage(){
    strings.setLanguage(this.props.language);
  }

  /**
   * TCDOC-038
   * this function will request to enable OTP with google athenticator to fetch the qrcode and secret code
   */
  requestGA(){

    this.setState({ loading: true, requested: true });

    OTPActions.OTPGAGenerate(this.getToken()).then(res => {
        let parsed = queryString.parse(res.data.data.qr);

        let baseQR = res.data.data.qr.split('chl=');

        let qr = baseQR[0]+"chl="+urlencode(parsed.chl);

        let simpleQRString = parsed.chl
        .replace(' ', '-')
        .replace(/@[a-z]+\.[a-z]+\.?[a-z]+/g, '')
        .replace('(', '')
        .replace(')', '');

	    let appEnv = "-"+process.env.REACT_APP_ENV;

	    if(process.env.REACT_APP_ENV === 'production') {
		    appEnv = "";
	    }

	    let splits = simpleQRString.split('TokoCrypto');

	    let newFormat = splits[0] + 'TokoCrypto'+appEnv + splits[1];

        // 
        // 

        this.setState({
            // qr: res.data.data.uri,
            qr: newFormat,
            secret: res.data.data.secret,
            loading: false
        });
    }).catch(err => {
      let errorDetails = {
        type: 'COMPONENTS: GAModal | FUNC: requestGA > OTGPAGenerate',
        url: '/otp/ga/generate'
      }
      NetworkError(err, errorDetails);
      this.setState({ loading: false });
    })
  }

  /**
   * TCDOC-039 
   * enable and confirm the google authenticator
   */
  enableGA(){
      this.setState({
          enabling: true
      });
      OTPActions.OTPGAEnable(this.getToken(), this.state.secret, this.state.userToken).then(res => {
        
        this.props.toggleShow()
        this.props.onFinish();
        NotificationManager.success('success setup google authenticator');
        this.setState({
            enabling: false
        });
      }).catch(err => {
        
        this.setState({
          enabling: false
        });
        let errorDetails = {
          type: 'COMPONENTS: GAModal | FUNC: enableGA > OTPGAEnable',
          url: '/otp/ga/enable'
        }
        NetworkError(err, errorDetails);
        
        // NotificationManager.error('failed setup google authenticator');
        
      });
  }

  componentWillReceiveProps(props){
    if(props.show === true && !this.state.requested){
        this.requestGA();
    }
    if(this.props.show === false) {
        this.setState({
            qr: "",
            secret: "",
            userToken: "",
            enabling: false,
            requested: false
        })
    }
  }

  componentWillMount() {
    this.onSetLanguage();
  }

  render() {
    return (
      <div>
        <Modal isOpen={this.props.show} toggle={() => this.props.toggleShow()} className={"modal-dialog-centered"} size="lg">
          <ModalHeader toggle={() => this.props.toggleShow()}>{strings.SET_UP_GOOGLE_AUTHENTICATOR}</ModalHeader>
          <ModalBody className="deposit-modal-idr">
            <div className="row">
                <div className="col-md-6">
                    { !this.state.loading && <QRCode size={200} className="text-center" value={this.state.qr} /> }
                </div>
                <div className="col-md-6">
                    <h5>{strings.SCAN_QRCODE_GOOGLE_AUTHENTICATOR}</h5>
                    <br />
                    <input value={this.state.userToken} onChange={e => this.setState({ userToken: e.target.value })} type="text" className="form-control" placeholder={strings.ENTER_TOKEN_FROM_GOOGLE_AUTHENTICATOR}/>
                    <br />
                    <CopyToClipboard
                        onCopy={() => this.setState({ copied: true})}
                        text={this.state.secret}>
                        <button className="btn btn-primary">{strings.COPY_SCRET_CODE_MANUALLY}</button>
                    </CopyToClipboard>
                    <br />
                    { this.state.copied && <span>{strings.SECRET_CODE_COPIED}</span> }
                </div>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggleShow()}>{strings.CANCEL}</Button>
            {
                this.state.enabling?
                <Button disabled={true} color="primary" onClick={() => this.enableGA()}>{strings.ENABLING_GOOGLE_AUTHENTICATOR}...</Button> :
                <Button disabled={this.state.loading || this.state.userToken.length === 0} color="primary" onClick={() => this.enableGA()}>{strings.ENABLE}</Button>
            }
          </ModalFooter>
        </Modal>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    user: state.cognito.user,
    kyc: state.kyc,
    language: state.language
  };
};

export default connect(mapStateToProps, null)(GAModal);
