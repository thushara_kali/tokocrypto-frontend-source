import React, { Component } from 'react';
import historyActions from '../../core/actions/client/histories';
import _ from 'lodash';
import moment from 'moment';
import { connect } from 'react-redux';
import NetworkError from '../../errors/networkError';
import amountFormatter from '../../helpers/amountFormatter';
import { cryptoFormatterByCoin } from '../../helpers/cryptoFormatter';
import currencyFormatter from '../../helpers/currencyFormatter';
import { Loader } from '../tcLoader';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import fontawesome from '@fortawesome/fontawesome'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import { faExchangeAlt, faArrowCircleUp, faArrowCircleDown } from '@fortawesome/fontawesome-free-solid';
import InfiniteScroll from "react-infinite-scroll-component";

fontawesome.library.add(faExchangeAlt, faArrowCircleUp, faArrowCircleDown)

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

var itemsTrasnactions = []

/**
 * TCDOC-027
 * TransactionHistory component is the component to fetch all the history and merge it
 */
class TransactionHistory extends Component {

    constructor() {
        super();
        this.state = {
            loading: true,
            txid: "",
            lastKey: null,
            currency: '',
            hasMoreItems: false,
            transactionsAll: [],
        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    onSetLanguage() {
        strings.setLanguage(this.props.language);
    }

    fetchAllTrasnactions = (currency) => {

        let lastKey = this.state.lastKey
        let data

        if(currency != this.state.currency){
            lastKey = null
            itemsTrasnactions = []
        }

        if(this.state.lastKey !== null){
            data = {
                limit: '50',
                currency: this.state.currency,
                lastKey: this.state.lastKey
            }
        } else {
            data = {
                limit: '50',
                currency: currency || this.state.currency,
            }
        }

        this.setState({
            hasMoreItems : false
        })

        historyActions.fetchAllTrasnactions(this.getToken(),data).then(res => {

            let keyObj = res.data.result.lastKey || null;
            let transactionsAll = res.data.result.data || [];

            itemsTrasnactions = itemsTrasnactions.concat(transactionsAll)

            if(keyObj !== null){
                this.setState({
                    hasMoreItems : true
                })
            }
            this.setState({
                transactionsAll: itemsTrasnactions,
                lastKey: keyObj,
                currency: currency
            })

        }).catch(err => {
            NetworkError(err);
        });
        
    }
    
    componentWillReceiveProps(props) {
        if (props.currency) {

            this.changeCurrency(props.currency)

            if(props.currency != this.state.currency){
                this.fetchAllTrasnactions(props.currency);
            }
            
            if(props.currency === this.state.currency){
                this.setState({
                    lastKey: null
                })
            }
        }
    }

    changeCurrency(currency){
        this.setState({
            currency: currency
        })
    }

    async componentDidMount() {
        this.fetchAllTrasnactions();
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    componentWillUnmount() {
        this.limitWather = null;
        window.intervals.map((i, index) => {
            clearInterval(i)
            window.intervals.splice(index, 1);
        });
    }

    /**
     * TCDOC-033
     * this is the template render for each different type of history
     * @param {} c 
     */
    renderHistory() {

        let dw = this.state.transactionsAll;

        dw = _.sortBy(dw, function (c) {
            if(c.CreatedAt){
                return moment(c.CreatedAt).unix()
            }else if(!c.CreatedAt){
                return moment(c.createdAt).unix()
            }
        })

        dw.reverse();
        let dwLength = 0;
        if(dw){
            dwLength = dw.length
        }

        if (dwLength > 0) {
            return dw.map(c => {
                if (c.ReferenceType) {
                    return (
                        <div key={c.ConversionId} className="chart-header col-md-12" style={{ display: "inline-flex", marginTop: "0px", borderBottom: "2px solid #ebedf2", paddingBottom: "20px" }}>
                            <div className="col-md-1" style={{ textAlign: "center", lineHeight: "1.4" }}>
                                <span className="" style={{ fontSize: "12px" }}>
                                    {moment(c.CreatedAt).format("YYYY")}
                                </span> <br />
                                <span className="" style={{ fontSize: "17px" }}>
                                    {moment(c.CreatedAt).format("DD")}
                                </span> <br />
                                <span className="accounts-trasnactions-tabale-header" style={{ fontSize: "11px", textAlign: "center" }}>
                                    {moment(c.CreatedAt).format("MMMM")}
                                </span>
                            </div>
                            <div className="col-md-1">
                                {c.ReferenceType === "Deposit" &&
                                    <FontAwesomeIcon style={{ fontSize: "27px", marginTop: "10px" }} icon={faArrowCircleDown} className="fas fa-arrow-circle-down etherium-color" />
                                }
                                {c.ReferenceType === "Withdraw" &&
                                    <FontAwesomeIcon style={{ fontSize: "27px", marginTop: "10px" }} icon={faArrowCircleUp} className="fas fa-arrow-circle-up etherium-color" />
                                }
                                {(c.ReferenceType === "Contribute" || c.ReferenceType === "Pledge") &&
                                    <FontAwesomeIcon style={{ fontSize: "27px", marginTop: "10px" }} icon={faArrowCircleUp} className="fas fa-arrow-circle-up etherium-color" />
                                }
                                {c.ReferenceType === "Conversion" &&
                                    <FontAwesomeIcon style={{ fontSize: "27px", marginTop: "10px" }} icon={faExchangeAlt} className="fas fa-exchange-alt bitcoin-color" />
                                }
                                {c.ReferenceType === "GiftCard" && c.RedeemUserEmail === "Out" &&
                                    <img src="../../assets/voucher-orange.png" style={{ width: "40px", height: "30px" }} />
                                }
                                {c.ReferenceType === "GiftCard" && c.RedeemUserEmail !== "Out" &&
                                    <img src="../../assets/voucher.png" style={{ width: "40px", height: "30px" }} />
                                }
                            </div>
                            <div className="col-xl-6" style={{ paddingLeft: "0px" }}>
                                {(c.ReferenceType === "Deposit" || c.ReferenceType === "Withdraw") &&
                                    <span className="accounts-trasnactions-tabale-header">
                                        {c.ReferenceType} {c.Currency}
                                    </span>
                                }
                                {(c.ReferenceType === "Contribute" || c.ReferenceType === "Pledge") &&
                                    <span className="accounts-trasnactions-tabale-header">
                                        {c.ReferenceType} {c.Currency}
                                    </span>
                                }
                                {c.ReferenceType === "GiftCard" &&
                                    <span> </span>
                                }
                                {c.ReferenceType === "Conversion" && c.Action === "Out" &&
                                    <span className="accounts-trasnactions-tabale-header">
                                        Sell {c.Currency}
                                    </span>
                                }
                                {c.ReferenceType === "Conversion" && c.Action === "In" &&
                                    <span className="accounts-trasnactions-tabale-header">
                                        Buy {c.Currency}
                                    </span>
                                }
                                <br />
                                {c.ReferenceType === "Conversion" &&
                                    <span className="" style={{ fontSize: "12px" }}>
                                        {strings.USING_IDR_WALLET}
                                    </span>
                                }
                                {(c.ReferenceType === "Deposit" || c.ReferenceType === "Withdraw") && c.Currency !== "SWIPE" &&
                                    <a href={"https://testnet.smartbit.com.au/tx/" + c.TxId} target="_blank">
                                        <strong>TxID:</strong>&nbsp;{c.TxId}
                                    </a>
                                }
                                {(c.ReferenceType === "Contribute" || c.ReferenceType === "Pledge") &&
                                    <span> </span>
                                }
                                {(c.ReferenceType === "Deposit" || c.ReferenceType === "Withdraw") && c.Currency === "SWIPE" &&
                                    <span className="" style={{ fontSize: "12px" }}>
                                        TxID:&nbsp;{c.TxId}
                                    </span>
                                }
                                {c.ReferenceType === "GiftCard" &&
                                    <span> </span>
                                }
                                <br />
                                <span className="" style={{ fontSize: "12px" }}>
                                    <span style={{ color: "#737479" }}>{moment(c.CreatedAt).format("hh:mm:ss A")}</span>
                                </span>
                            </div>
                            <div className="col-xl-4" style={{ paddingLeft: "0px", textAlign: "right" }}>
                                {(c.ReferenceType === "Deposit" || c.ReferenceType === "Withdraw") ?
                                    <span className="accounts-trasnactions-tabale-header">
                                        {c.Currency} {cryptoFormatterByCoin(c.Amount, c.Currency)}
                                    </span> : <span className="accounts-trasnactions-tabale-header">
                                        {cryptoFormatterByCoin(c.Amount, c.Currency)} {c.Currency}
                                    </span>
                                }
                                <br />
                                {c.ReferenceType === "Conversion" &&
                                    <span className="" style={{ fontSize: "12px" }}>
                                        { currencyFormatter( amountFormatter(c.BuyAmount) ) }
                                    </span>
                                }
                                {(c.ReferenceType === "Deposit" || c.ReferenceType === "Withdraw") &&
                                    <span>{c.Status == "Success" ?
                                        <span style={{ fontSize: "13px" }}>
                                            <span style={{ color: "#27ae60", fontWeight: "bold" }}>
                                                <strong>{c.Status}</strong>
                                            </span>
                                        </span> :
                                        <span style={{ fontSize: "13px" }}>
                                            <span style={{ color: "#f89927", fontWeight: "bold" }}>
                                                <strong>{c.Status}</strong>
                                            </span>
                                        </span>
                                    }</span>
                                }
                                {c.ReferenceType === "GiftCard" &&
                                    <span style={{ fontSize: "13px" }}>
                                        {c.ReferenceId}
                                    </span>
                                }
                                <br />
                                {(c.ReferenceType === "Conversion" && c.Fee && c.FeePercentage) &&
                                    <span style={{ fontSize: "12px" }}>
                                        {strings.FEE}:&nbsp;{ currencyFormatter( amountFormatter(c.Fee) ) }&nbsp; ({c.FeePercentage}%)
                                    </span>
                                }
                                {(c.ReferenceType === "Conversion" && !c.Fee && !c.FeePercentage) &&
                                    <span style={{ fontSize: "12px" }}>
                                        {strings.FEE}:&nbsp;0&nbsp; (0%)
                                    </span>
                                }
                            </div>
                        </div>
                    )
                }
            })
        } else {
            return (
                <div className="no-trades-accounts">
                    {this.state.loading == false ? <span>{strings.NO_TRASNACTIONS_FOUND}</span> : null}
                </div>
            )
        }
    }

    /**
     * TCDOC-033
     * this is the template render for each different type of history
     * @param {} c 
     */
    renderDepositAndWithdrawals() {

        let dw = this.state.transactionsAll;

        dw = _.sortBy(dw, function (c) {
            if(c.CreatedAt){
                return moment(c.CreatedAt).unix()
            }else if(!c.CreatedAt){
                return moment(c.createdAt).unix()
            }
        })

        dw.reverse();
        let dwLength = 0;

        if(dw){
            dwLength = dw.length
        }

        if (dwLength > 0) {
            return dw.map(c => {
                if (c.ReferenceType !== "Conversion") {
                    return (
                        <div key={c.ConversionId} className="chart-header col-md-12" style={{ display: "inline-flex", marginTop: "0px", borderBottom: "2px solid #ebedf2", paddingBottom: "20px" }}>
                            <div className="col-md-1" style={{ textAlign: "center", lineHeight: "1.4" }}>
                                <span className="" style={{ fontSize: "12px" }}>
                                    {moment(c.CreatedAt).format("YYYY")}
                                </span> <br />
                                <span className="" style={{ fontSize: "17px" }}>
                                    {moment(c.CreatedAt).format("DD")}
                                </span> <br />
                                <span className="accounts-trasnactions-tabale-header" style={{ fontSize: "11px", textAlign: "center" }}>
                                    {moment(c.CreatedAt).format("MMMM")}
                                </span>
                            </div>
                            <div className="col-md-1">
                                {c.ReferenceType === "Deposit" &&
                                    <FontAwesomeIcon style={{ fontSize: "27px", marginTop: "10px" }} icon={faArrowCircleDown} className="fas fa-arrow-circle-down etherium-color" />
                                }
                                {c.ReferenceType === "Withdraw" &&
                                    <FontAwesomeIcon style={{ fontSize: "27px", marginTop: "10px" }} icon={faArrowCircleUp} className="fas fa-arrow-circle-up etherium-color" />
                                }
                                {(c.ReferenceType === "Contribute" || c.ReferenceType === "Pledge") &&
                                    <FontAwesomeIcon style={{ fontSize: "27px", marginTop: "10px" }} icon={faArrowCircleUp} className="fas fa-arrow-circle-up etherium-color" />
                                }
                                {c.ReferenceType === "GiftCard" && c.RedeemUserEmail === "Out" &&
                                    <img src="../../assets/voucher-orange.png" style={{ width: "40px", height: "30px" }} />
                                }
                                {c.ReferenceType === "GiftCard" && c.RedeemUserEmail !== "Out" &&
                                    <img src="../../assets/voucher.png" style={{ width: "40px", height: "30px" }} />
                                }
                            </div>
                            <div className="col-xl-5" style={{ paddingLeft: "0px" }}>
                                {(c.ReferenceType === "Deposit" || c.ReferenceType === "Withdraw") &&
                                    <span className="accounts-trasnactions-tabale-header">
                                        {c.ReferenceType}
                                    </span>
                                }
                                {(c.ReferenceType === "Contribute" || c.ReferenceType === "Pledge") &&
                                    <span className="accounts-trasnactions-tabale-header">
                                        {c.ReferenceType} {c.Currency}
                                    </span>
                                }
                                {c.ReferenceType === "GiftCard" && c.RedeemUserEmail === "Out" &&
                                    <span className="accounts-trasnactions-tabale-header">
                                        {strings.VOUCHER_CODE_CREATED}
                                    </span>
                                }
                                {c.ReferenceType === "GiftCard" && c.RedeemUserEmail !== "Out" &&
                                    <span className="accounts-trasnactions-tabale-header">
                                        {strings.VOUCHER_CODE_REDEEMED}
                                    </span>
                                }
                                <br />
                                {(c.ReferenceType === "Deposit" || c.ReferenceType === "Withdraw") &&
                                    <span className="" style={{ fontSize: "12px" }}>
                                        {strings.USING_IDR_WALLET}
                                    </span>
                                }
                                {(c.ReferenceType === "Contribute" || c.ReferenceType === "Pledge") &&
                                    <span> </span>
                                }
                                {c.ReferenceType === "GiftCard" &&
                                    <span> </span>
                                }
                                <br />
                                <span className="" style={{ fontSize: "12px" }}>
                                    <span style={{ color: "#737479" }}>{moment(c.CreatedAt).format("hh:mm:ss A")}</span>
                                </span>
                            </div>
                            <div className="col-xl-5" style={{ paddingLeft: "0px", textAlign: "right" }}>
                                {c.Amount <= 100000 ?
                                    <span className="accounts-trasnactions-tabale-header">
                                        { currencyFormatter( amountFormatter(c.Amount) ) }
                                    </span> : <span>
                                        { currencyFormatter( amountFormatter(c.Amount) ) }
                                    </span>
                                }
                                <br />
                                {(c.ReferenceType === "Deposit" || c.ReferenceType === "Withdraw") &&
                                    <span>{c.Status == "Success" ?
                                        <span style={{ fontSize: "13px" }}>
                                            <span style={{ color: "#27ae60", fontWeight: "bold" }}>
                                                <strong>{c.Status}</strong>
                                            </span>
                                        </span> :
                                        <span style={{ fontSize: "13px" }}>
                                            <span style={{ color: "#f89927", fontWeight: "bold" }}>
                                                <strong>{c.Status}</strong>
                                            </span>
                                        </span>
                                    }</span>
                                }
                                {c.ReferenceType === "GiftCard" &&
                                    <span style={{ fontSize: "13px" }}>
                                        {c.ReferenceId}
                                    </span>
                                }
                            </div>
                        </div>
                    )
                }
            })
        } else {
            return (
                <div className="no-trades-accounts">
                    {this.state.loading == false ? <span>{strings.NO_TRASNACTIONS_FOUND}</span> : null}
                </div>
            )
        }
    }

    render() {
        return (
            <div className="col-xl-8" style={{paddingLeft:"0xp",paddingRight:"0px"}}>
			    <div className="m-portlet m-portlet--full-height">
				    <div className="m-portlet__head" style={{ backgroundColor: "whitesmoke" }}>
					    <div className="row" style={{height:"100%"}}>
						    <div className="m-portlet__head-caption col-md-12" style={{ display: "inline-flex", height:"5.1rem"}}>
							    <div className="m-portlet__head-title col-md-4">
								    <h3 className="m-portlet__head-text">
									    {strings.TRANSACTIONS}
									</h3>
								</div>
                            </div>
                        </div>
                    </div>
                    <div id="historyList" style={{ height: "2368", overflow: 'auto' }}>
                        <InfiniteScroll
                            pageStart={0}
                            next={() => this.fetchAllTrasnactions(this.props.currency)}
                            hasMore={this.state.hasMoreItems}
                            loader={<span style={{ display: "block", textAlign: "center", marginTop: "20px" }} ><Loader type="line-scale" active={this.state.loading} className="text-center" /></span>}
                            height={2268}
                        >
                            {this.props.currency !== 'IDR' && this.renderHistory()}
                            {this.props.currency === 'IDR' && this.renderDepositAndWithdrawals()}
                        </InfiniteScroll>
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        language: state.language
    };
};

export default connect(mapStateToProps, null)(TransactionHistory);