import React, { Component } from 'react';
import moment from 'moment';

class DepositWithdrawItem extends Component {

    render() {
        return (
            <div className="history-item">
                <div className="row" >
                    {/* <div className="col-md-3 col-12">
                        <div className="btn btn-info btn-history">{moment(this.props.transaction.CreatedAt).format('lll')}</div>
                    </div> */}
                    <div className="col-md-8 col-8">
                        <div className="row" style={{ padding: "10px" }}>
                            <div className="col-12 history-transactions-details">
                                {/* <h5>{this.props.transaction.Currency} <span className="color-primary">{this.props.transaction.Amount}</span></h5> */}
                                {/* <div>{this.props.transaction.WDUser.Name}</div> */}
                                {this.props.transaction.Currency == 'IDR' ?
                                <div>Deposit Code: <span style={{fontWeight:'bold'}}>{this.props.transaction.WDUser.DepositCode}</span></div>:
                                <div>TxID: 
                                    {this.props.transaction.Currency == 'BTC' ? 
                                        <a href={'https://testnet.smartbit.com.au/tx/' + this.props.transaction.bitgotxId} style={{fontWeight:'bold'}} className="history-transactions-details-id" target="_blank" > {this.props.transaction.bitgotxId}</a>:
                                        <a href={'https://kovan.etherscan.io/tx/' + this.props.transaction.bitgotxId} style={{fontWeight:'bold'}} className="history-transactions-details-id" target="_blank"> {this.props.transaction.bitgotxId}</a>
                                    }
                                    </div>
                                 }
                                <div className="btn-history-container">
                                    {this.props.transaction.Status == 'Pending' ?
                                    <div className="btn-history-pending">{this.props.transaction.Status}</div> :
                                    <div className="btn-history-success">{this.props.transaction.Status}</div>}
                                    <div className="btn-history-date">{moment(this.props.transaction.CreatedAt).format('lll')}</div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 col-4 status-item-history">
                        {this.props.transaction.WDType == 'Deposit' ?
                            <span><img className="" src={require('../img/add.png')} /> {this.props.transaction.Amount} {this.props.transaction.Currency}</span> :
                            <span><img className="" src={require('../img/minus.png')} /> {this.props.transaction.Amount} {this.props.transaction.Currency}</span>
                        }
                    </div>
                </div>
            </div >
        )
    }

}

export default DepositWithdrawItem;