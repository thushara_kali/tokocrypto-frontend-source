import React, { Component } from 'react';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import Avatar from 'react-avatar';
import { connect } from 'react-redux';
import { getUserAttributes } from '../core/react-cognito/attributes.js'
import { Action } from '../core/react-cognito';
import avatarImage from '../img/user12.jpg';
import history from '../history';
import store from '../store'
import NetworkError from '../errors/networkError'
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

class AvatarDropdown extends Component {
    state = {
        valueSingle: '3',
        valueMultiple: ['3', '5'],
    };

    handleOnRequestChange = (value) => {
        this.setState({
            openMenu: value,
        });
    }

    onLogout = (event) => {
        event.preventDefault();
        if (this.props.user != null) {
            this.props.user.signOut();
            history.replace('/login');
        }
        //     const { store } = this.context;
        //  //   const state = store.getState();
        //     // state.cognito.user.signOut();
        //     // event.preventDefault();
        //     // store.dispatch(Action.logout());
        //     // this.props.onLogout();
    }

    fetchUserProfile() {
        getUserAttributes(this.props.user).then(attr => {
            this.setState({
                name: attr.name,
                email: attr.email
            })
            store.dispatch(Action.setFullName(attr.name));
        })
        .catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: avatarDropdown | FUNC: fetchUserProfile',
                url: '-'
            }
            NetworkError(err, errorDetails);
        })
    }

    componentDidMount() {
        this.fetchUserProfile();
    }

    render() {
        const style = {
            zIndex: '1500'
        }

        return (
            <div>
                <IconMenu
                    style={style}
                    anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                    iconButtonElement={<IconButton>
                        <Avatar
                            name={this.props.fullName}
                            round={true}
                            size={40}
                        />
                    </IconButton>}
                    open={this.state.openMenu}
                    onRequestChange={this.handleOnRequestChange}
                >
                    <MenuItem onClick={() => history.push('/profile')} primaryText="View Profile" leftIcon={<i className="material-icons">account_circle</i>} />
                    {/*<Divider />*/}
                    <MenuItem primaryText="Sign Out" leftIcon={<i className="material-icons">power_settings_new</i>}
                        onClick={this.onLogout}

                    />
                </IconMenu>
                <div className="header-fullname">{this.props.fullName}</div>
            </div>

        );
    }
}



const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        fullName: state.cognito.fullName
    };
};

export default connect(
    mapStateToProps,
)(AvatarDropdown);
