import React, { Component } from 'react';
import history from '../history'
import numeral from 'numeral'
import classnames from 'classnames';
import actions from '../core/actions/client/home';
import NetworkError from '../errors/networkError';
class Coin extends Component {

  constructor(){
      super();
      this.state = {
          changesPercent: 0
      }
  }

  buyCoin(coin) {
    history.push('/buy/' + coin, { rate: this.props.rate, buyAccount: this.props.buyAccount })
  }

  sellCoin(coin) {
    history.push('/sell/' + coin, { rate: this.props.rate, sellAccount: this.props.sellAccount })
  }

  getLastDayUpDown(currencyPair){
    actions.getChartSummary(currencyPair).then(res => {
        this.setState({
            changesPercent: res.data.data['24H'].rates.changes.percentage
        })
    }).catch((err) => {
      let details = {
        type: 'COMPONENTS: buySellCoin | FUNC: getLastDayUpDown',
        url: 'https://tfxf49mpq1.execute-api.us-east-1.amazonaws.com/dev/rates/changed/' + currencyPair
      }
      NetworkError(err, details);
    });
  }

  componentDidMount(){
      this.getLastDayUpDown(this.props.rate.currencyPairHummanize.replace('/',''));
  }

  render() {

    let coinIcon = ""
    let coinImage = null
    let moneyImage = null

    let currencyCode = this.props.rate.currencyPairHummanize.split('/')[1]
    let virtualCurrencyCode = this.props.rate.currencyPairHummanize.split('/')[0]

    switch (virtualCurrencyCode) {
      case 'BTC':
        coinIcon = (<i className="fa fa-btc" aria-hidden="true"></i>)
        coinImage = (<img width="50" height="50" src={require('./../img/btc.svg')} />)
        break;
      case 'ETH':
        coinIcon = 'ETH'
        coinImage = (<img width="50" height="50" src={require('./../img/eth.png')} />)
        break;
      default:
        break;
    }

    switch (currencyCode) {
      case 'IDR':
        moneyImage = (<img className="flag-circle" width="50" height="50" src={require('./../img/id.png')} />)
        break;
      default:
        moneyImage = null
    }

    return (
      <div>
        <div className="col-md-12">
            <div className="row">
              <div className="Buy-sell-header">
                {virtualCurrencyCode == 'BTC' ?
                <div style={{marginBottom:"10px"}}>Bitcoin</div>:
                <div style={{marginBottom:"10px"}}>Ethereum</div>
                }
                <div className="ratings-updates">
                  {/* <h5>{this.props.rate.currencyPairHummanize}</h5> */}
                  {/* <h6>{this.props.rate.label}</h6> */}
                  {currencyCode} {numeral(this.props.rate.customer_buy).format('0,0')}
                  <span>&nbsp;</span>
                  { this.state.changesPercent >= 0 ? <i className="fa fa-arrow-up rates-up"></i> : <i className="fa fa-arrow-down rates-down"></i> }
                  <span className={classnames({ "rates-up": this.state.changesPercent >= 0, "rates-down": this.state.changesPercent < 0})}>{ this.state.changesPercent } %</span>
                </div>
              </div>
              <div>
                <div className="buy-sell-button-container">
                  <div className="buy-sell-buttons" onClick={() => { this.buyCoin(virtualCurrencyCode) }}>
                    <div>Buy</div>
                    <div>{currencyCode} {numeral(this.props.rate.customer_buy).format('0,0')}</div>
                  </div>
                  <div  className="buy-sell-buttons" onClick={() => { this.sellCoin(virtualCurrencyCode) }}>
                    <div>Sell</div>
                    <div>{currencyCode} {numeral(this.props.rate.customer_sell).format('0,0')}</div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        {/* <div className="row coin-row">
          <div className="col-md-4">
            <div className="row">
              <div className="col-md-12">
                {coinImage}
                <i className="fa fa-exchange exchange-icon" aria-hidden="true" style={{ width: 50, height: 50 }}></i>
                {moneyImage}
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="row">
              <h5 className="col-md-12">{this.props.rate.currencyPairHummanize}</h5>

              <h6 className="col-md-12">{this.props.rate.label}</h6>
            </div>
          </div>
          <div className="col-md-4">
            <div className="row">
              <div className="col-12 col-md-6">
                <p>Buy {virtualCurrencyCode} with {currencyCode} at</p>
                <div className="row">
                  <p className="coin-buy col-md-12 col-8">{currencyCode} {numeral(this.props.rate.customer_buy).format('0,0')}</p>
                  <div className="col-md-12 col-3">
                    <button className="btn btn-success" onClick={() => { this.buyCoin(virtualCurrencyCode) }}>
                      Buy
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-6">
                <p className="">Sell {virtualCurrencyCode} with IDR at</p>
                <div className="row">
                  <p className="coin-sell col-md-12 col-8">{currencyCode} {numeral(this.props.rate.customer_sell).format('0,0')}</p>
                  <div className="col-md-12 col-3">
                    <button className="btn btn-danger" onClick={() => { this.sellCoin(virtualCurrencyCode) }}>
                      Sell
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> */}
      </div >
    )
  }

}

export default Coin
