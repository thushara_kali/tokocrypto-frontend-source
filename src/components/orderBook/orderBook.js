import React, { Component } from 'react';
import _ from 'lodash';
import moment, { max } from 'moment';
import { connect } from 'react-redux';
import amount from '../../helpers/amountFormatter';
import amountFormatter from '../../helpers/amountFormatter';
import cryptoFormatter, { cryptoFormatterByCoin } from '../../helpers/cryptoFormatter';
import history from '../../history';
import orderBookActions from '../../core/actions/client/orderBook';
import homeActions from '../../core/actions/client/home';
import NetworkError from '../../errors/networkError';
import { Loader } from '../tcLoader';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import { NotificationManager } from 'react-notifications';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import async from 'async';
import classnames from 'classnames';
import OrderBookModal from '../../components/orderBook/orderBookModel';
import currencyFormatter from '../../helpers/currencyFormatter'
import OrderNow from './orderNow';
import { ClapSpinner } from "react-spinners-kit";

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

var fetchOrderAskTime
var fetchOrderBidTime

class OrderBook extends Component {

    constructor(){
        super();
        this.state = {
           orderBookBid: [],
           orderBookAsk: [],
           orderBookModel:false,
           buyRate: 0,
           sellRate: 0,
           Buyprice: "",
           Sellprice: "",
           joinTable: [],
           currency: '',
           loading: true
        }
    }

    getToken() {
        if(this.props.user){
            let username = this.props.user.username
            let clientId = this.props.user.pool.clientId
            let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
            return token
        }else{
            return 0
        }
    }

    fetchOrderBookBid = () => {

        let _self = this
        let currency

        if(this.props.currency){
            currency = this.props.currency
        }else{
            currency = 'BTC'
        }

        let data = {
            currency:currency,
            sort: "desc",
            fixedSide: "buy",
            group: "true",
            limit: 100
        }
        // orderBookActions.orderBook(this.getToken(),data).then(res => {
        //     this.setState({
        //         orderBookBid : _.uniq(res.data.result.result)
        //     })

        //     fetchOrderBidTime =  setTimeout(function(){ _self.fetchOrderBookBid(); }, 5000);
        // }).catch(err => {
        //     let errorDetails = {
        //         type: 'COMPONENTS: orderBook | FUNC: fetchOrderBookBid',
        //         url: `orderbook/createOrder`
        //     }
        //     NetworkError(err, errorDetails);

        //     // NotificationManager.error("Error Fetch Order Book");
        // });
        orderBookActions.orderBookRaw(this.getToken(),data).then(res => {
            this.setState({
                orderBookBid : _.uniq(res.data.result)
            })

            fetchOrderBidTime =  setTimeout(function(){ _self.fetchOrderBookBid(); }, 5000);
        }).catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: orderBook | FUNC: fetchOrderBookBid',
                url: `orderbook/createOrder`
            }
            NetworkError(err, errorDetails);

            // NotificationManager.error("Error Fetch Order Book");
        });
    }

    fetchOrderBookAsk() {

        let _self = this
        let currency

        if(this.props.currency){
            currency = this.props.currency
        }else{
            currency = 'BTC'
        }

        let data = {
            currency:currency,
            sort: "asc",
            fixedSide: "sell",
            group: "true",
            limit: 100
        }
        // orderBookActions.orderBook(this.getToken(),data).then(res => {
        //     this.setState({
        //         orderBookAsk : _.uniq(res.data.result.result),
        //         loading: false
        //     })

        //     fetchOrderAskTime = setTimeout(function(){ _self.fetchOrderBookAsk(); }, 5000);
        // }).catch(err => {
        //     let errorDetails = {
        //         type: 'COMPONENTS: orderBook | FUNC: fetchOrderBookAsk',
        //         url: `orderbook/createOrder`
        //     }
        //     NetworkError(err, errorDetails);

        //     // NotificationManager.error("Error Fetch Order book");
        //     // NetworkError(err);
        // });
        orderBookActions.orderBookRaw(this.getToken(),data).then(res => {
            this.setState({
                orderBookAsk : _.uniq(res.data.result),
                loading: false
            })
            

            fetchOrderAskTime = setTimeout(function(){ _self.fetchOrderBookAsk(); }, 5000);
        }).catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: orderBook | FUNC: fetchOrderBookAsk',
                url: `orderbook/createOrder`
            }
            NetworkError(err, errorDetails);

            // NotificationManager.error("Error Fetch Order book");
            // NetworkError(err);
        });
    }

    componentDidMount(){
        this.fetchOrderBookBid();
        this.fetchOrderBookAsk();
        // this.props.orderSubmited(this.updateTable.bind(this));
        // if(!this.autoOrderBook){
        //     this.autoOrderBook = setInterval(() => {
        //         this.fetchOrderBookBid();
        //         this.fetchOrderBookAsk();
        //     }, 1000 * 10);

        //     if(window.intervals3){
        //         window.intervals3.push(this.autoOrderBook)
        //     } else {
        //         window.intervals3 = [this.autoOrderBook]
        //     }
        // }
    }

    componentWillUnmount() {
	    // window.intervals3.map((i, index) => {
		//     clearTimeout(i)
		//     window.intervals3.splice(index, 1);
        // });
        clearTimeout(fetchOrderAskTime);
        clearTimeout(fetchOrderBidTime);
    }

    onOpenOrderBookModal(){
        this.setState({
            orderBookModel: true
        })
    }

    onCloseOrderBookModal(){
        this.setState({
            orderBookModel: false
        })
    }

    updateTable() {
        this.fetchOrderBookBid();
        this.fetchOrderBookAsk();
    }

    tableGetMax(array) {
        let max = Math.max(array[0], array[1])
        return max
    }

    tableSumObject(array) {
        var sum = 0;
        for (var x in array) { 
            if( array[x].hasOwnProperty( "AvailableQuantity" )) {
                sum += ( array[x].AvailableQuantity );
            }
        }
        return sum;
    }

    tableCalculateWidth(current, max) {
        return ((current / max) * 100).toString() + '%';
    }

    componentDidUpdate( prevProps, prevState ) {
        if ( prevProps.currency != this.state.currency ) {
            this.setState({ currency: prevProps.currency, loading: true })
        }
    }

    componentWillReceiveProps(props){

        let price;

        price = _.find(props.rates, { currencyPair: props.currency.toUpperCase()+"IDR"})

        if(price){
            this.setState({
                buyRate: price.customer_buy,
                sellRate: price.customer_sell,
            });
        }
    }

    render(){

        var totalBuy = 0
        var totalAsk = 0
        var currentSellPrice = 0
        var currentBuyPrice = 0
        var maxTotal = this.tableGetMax([this.tableSumObject(this.state.orderBookAsk.slice(0,30)), this.tableSumObject(this.state.orderBookBid.slice(0,30))])
        let orderBookBid = this.state.orderBookBid[0]
        let orderBookAsk = this.state.orderBookAsk[0]
        if(orderBookBid){
            var orderBookBidPrice =  0;
            if(this.state.orderBookBid.length > 1){
                orderBookBidPrice =  this.state.orderBookBid[0].Price    
            }
        }

        if(orderBookAsk){
            var orderBookAskPrice =  0;
            if(this.state.orderBookAsk.length > 1){
                orderBookAskPrice =  this.state.orderBookAsk[0].Price    
            }
        }

        var listNumber = [];
        for (var i = 0; i <= 30; i++) {
            listNumber.push(i);
        }
        var orderBookBidData = this.state.orderBookBid;
        var orderBookAskData = this.state.orderBookAsk;

        var listNumberObject = Object.assign({}, ...listNumber);

        // let x = this.tableRenderer();
        return (

            <div className="col-md-12">
                <div className="m-portlet m-portlet--full-height m-portlet--fit combined-chart-recent">
                    {/* <div className="m-portlet__head" style={{height:"3.1rem"}}>
                        <div className="m-portlet__head-caption">
                            <div className="m-portlet__head-title">
                                <h3 className="m-portlet__head-text">
                                    Order Book
                                </h3>
                            </div>
                        </div>
                    </div> */}
                    <div className="m-portlet__body" 
                        style={{
                            display:"inline-flex",
                            width:"100%",
                            paddingTop:"15px",
                            paddingLeft:"0px",
                            paddingRight:"0px",
                            height:"100%"
                            // overflow: 'hidden'
                        }}>
                        
                        <div className="col-md-12" style={{ paddingLeft:"3%", overflowY: 'scroll'}}>
                            <table className="table referral-details-table example-date">
                                <thead>
                                    <tr className="referral-details-table-header">
                                        <th style={{ textAlign: 'center', background:'#1AA74A', color:"#ffffff", fontSize: "13px" }}>Bid</th>
                                        <th style={{ textAlign: 'center', background:'#1AA74A', color:"#ffffff", fontSize: "13px", textAlign: 'left' }} colSpan={2}>Current Bid : { currencyFormatter( amountFormatter(orderBookBidPrice,this.props.currency) )} </th>
                                        <th style={{ textAlign: 'center', background:'#C67900', color:"#ffffff", fontSize: "13px"}}>Ask</th>
                                        <th style={{ textAlign: 'center', background:'#C67900', color:"#ffffff", fontSize: "13px", textAlign: 'left'}} colSpan={ 2 }>Current Ask : { currencyFormatter( amountFormatter(orderBookAskPrice,this.props.currency) )} </th>
                                    </tr>
                                    <tr className="referral-details-table-row" style={{background:"#F7F7F7"}}>
                                        <th width={ `${100 / 6}%` } style={{ textAlign: 'center', color:"#666666", fontWeight:"700", textAlign: 'center'}}>Total</th>
                                        <th width={ `${100 / 6}%` } style={{ textAlign: 'center', color:"#666666", fontWeight:"700", textAlign: 'center'}}>Quantity</th>
                                        <th width={ `${100 / 6}%` } style={{ textAlign: 'center', color:"#666666", fontWeight:"700", textAlign: 'center'}}>Price</th>
                                        <th width={ `${100 / 6}%` } style={{ textAlign: 'center', color:"#666666", fontWeight:"700", textAlign: 'center'}}>Price</th>
                                        <th width={ `${100 / 6}%` } style={{ textAlign: 'center', color:"#666666", fontWeight:"700", textAlign: 'center'}}>Quantity</th>
                                        <th width={ `${100 / 6}%` } style={{ textAlign: 'center', color:"#666666", fontWeight:"700", textAlign: 'center'}}>Total</th>
                                    </tr>
                                </thead>
                                
                                <tbody style={{ overflowY: 'scroll'}}>
                                    
                                    { this.state.loading ? 
                                    <tr >
                                        <td colSpan={ 6 } >
                                        <div style={{ flex: 1 , display: 'flex', paddingTop: 10, justifyContent: 'center' }}>
                                            <ClapSpinner
                                                size={30}
                                                color="#686769"
                                                loading={ this.state.loading }
                                            />
                                            </div>
                                        </td>
                                        
                                    </tr>
                                    : (this.state.orderBookBid.length > 0) || (this.state.orderBookAsk.length > 0 && (orderBookBidData != undefined && orderBookAskData != undefined)) ? 

                                        listNumber.map( (r,index) => {
                                            if( orderBookBidData[index] || orderBookAskData[index] ) {

                                                let r_Bid = orderBookBidData[index];
                                                let r_Ask = orderBookAskData[index];
                                                
                                                // Bid
                                                let currentBidOrderStatusSum = (this.state.orderBookBid.length > 1 && this.state.orderBookBid[index]) ? this.state.orderBookBid[index].AvailableQuantity : 0
                                                let currentBidPrice = (this.state.orderBookBid.length > 1 && this.state.orderBookBid[index]) ? this.state.orderBookBid[index].Price : 0;
                                                totalBuy = totalBuy + currentBidOrderStatusSum;
                                                currentBuyPrice = currentBuyPrice + currentBidPrice;

                                                // Ask
                                                let currentAskOrderStatusSum = (this.state.orderBookAsk.length > 1 && this.state.orderBookAsk[index]) ? this.state.orderBookAsk[index].AvailableQuantity : 0
                                                let currentAskPrice = (this.state.orderBookAsk.length > 1 && this.state.orderBookAsk[index]) ? this.state.orderBookAsk[index].Price : 0;
                                                totalAsk = totalAsk + currentAskOrderStatusSum;
                                                currentSellPrice = currentSellPrice + currentAskPrice;

                                                return (
                                                    <tr
                                                        className="referral-details-table-row referral-details-table-row-no-border" 
                                                        colSpan={ 6}>

                                                        { r_Bid && r_Bid.hasOwnProperty('Price') ?
                                                            <td colSpan={3} style={{ padding: 0}}>
                                                                <div 
                                                                    style={{ 
                                                                        display: 'flex', 
                                                                        flexDirection: 'row',
                                                                        fontSize:"10px",
                                                                        background: `linear-gradient(-90deg, rgba(158, 240, 185, 0.75)${this.tableCalculateWidth(totalBuy, maxTotal)},  #fff ${this.tableCalculateWidth(totalBuy, maxTotal)})`,
                                                                        borderWidth: 0,
                                                                        borderBottomWidth: 0,}}
                                                                    >
                                                                    <div style={{flex: 1, color:"#1AA74A", padding: "0.4rem"}}>{cryptoFormatterByCoin( totalBuy, this.props.currency )}</div>
                                                                    <div style={{flex: 1, padding: "0.4rem"}}>{cryptoFormatterByCoin( r_Bid.AvailableQuantity, this.props.currency )}</div>
                                                                    <div style={{flex: 1, padding: "0.4rem"}}>{ currencyFormatter( amountFormatter(r_Bid.Price,this.props.currency) )}</div>
                                                                </div>
                                                            </td> 
                                                            
                                                            :
                                                            <td colSpan={3} style={{ padding: 0}}>
                                                                <div 
                                                                    style={{ 
                                                                        display: 'flex', 
                                                                        flexDirection: 'row',
                                                                        fontSize:"10px",
                                                                        borderWidth: 0,
                                                                        borderBottomWidth: 0,}}
                                                                    >
                                                                    <div style={{flex: 1, color:"#1AA74A", padding: "0.4rem"}}></div>
                                                                    <div style={{flex: 1, padding: "0.4rem"}}></div>
                                                                    <div style={{flex: 1, padding: "0.4rem"}}></div>
                                                                </div>
                                                            </td>
                                                        }
                                                        { r_Ask && r_Ask.hasOwnProperty('Price') ?
                                                            <td colSpan={3} style={{ padding: 0}}>
                                                                <div 
                                                                    style={{ 
                                                                        display: 'flex', 
                                                                        flexDirection: 'row',
                                                                        fontSize:"10px",
                                                                        background: `linear-gradient(90deg, rgba(255, 200, 112, 0.75) ${this.tableCalculateWidth(totalAsk, maxTotal)},  #fff ${this.tableCalculateWidth(totalAsk, maxTotal)})`,
                                                                        borderTopWidth: 0,
                                                                        borderBottomWidth: 0,}}
                                                                    >
                                                                    <div style={{flex: 1, padding: "0.4rem"}}>{ currencyFormatter( amountFormatter(r_Ask.Price,this.props.currency) )}</div>
                                                                    <div style={{flex: 1, padding: "0.4rem"}}>{cryptoFormatterByCoin( r_Ask.AvailableQuantity, this.props.currency )}</div>
                                                                    <div style={{flex: 1, color:"#C67900", padding: "0.4rem"}}>{cryptoFormatterByCoin( totalAsk, this.props.currency )}</div>
                                                                </div>
                                                            </td> 
                                                            
                                                            :
                                                            <td colSpan={3} style={{ padding: 0}}>
                                                                <div 
                                                                    style={{ 
                                                                        display: 'flex', 
                                                                        flexDirection: 'row',
                                                                        fontSize:"10px",
                                                                        borderTopWidth: 0,
                                                                        borderBottomWidth: 0,}}
                                                                    >
                                                                    <div style={{flex: 1, padding: "0.4rem"}}></div>
                                                                    <div style={{flex: 1, padding: "0.4rem"}}></div>
                                                                    <div style={{flex: 1, color:"#C67900", padding: "0.4rem"}}></div>
                                                                </div>
                                                            </td> 
                                                        }
                                                        
                                                        
                                                        
                                                    </tr>
                                                )
                                            }
                                        })
                                        : null
                                    }
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <OrderBookModal
                    show={this.state.orderBookModel}
                    onClose={() => this.onCloseOrderBookModal()}
                />
            </div>

        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
	    language: state.language
    };
};

export default connect(mapStateToProps, null)(OrderBook);