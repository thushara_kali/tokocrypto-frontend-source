import React, { Component } from 'react';
import QRCode from 'qrcode.react';
import classnames from 'classnames';
import OTPModal from '../OTPModal';
import { sendTranscation,getWithdrawFees,getWithdrawLimits } from '../../core/actions/client/send';
import recieveActions from '../../core/actions/client/recieve';
import { NotificationManager } from 'react-notifications';
import NetworkError from '../../errors/networkError';
import { connect } from 'react-redux';
import { Loader } from '../tcLoader';
import homeActions from '../../core/actions/client/home';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import history from '../../history';
import OTPActions from '../../core/actions/client/otp';
import cryptoFormatter from '../../helpers/cryptoFormatter';
import numeral from 'numeral';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import orderBookActions from '../../core/actions/client/orderBook';
import NP from 'number-precision';
import WAValidator from 'wallet-address-validator';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class OrderBookModel extends Component {

    constructor(){
        super();
        this.state = {
            orderBook: [],
            loading: false,
            limit:10,
            offSet:0
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.value
            this.setState(obj)

        }
    }

    getToken() {
        if(this.props.user){
            let username = this.props.user.username
            let clientId = this.props.user.pool.clientId
            let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
            return token
        }else{
            return 0
        }
    }

    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    close(closeAll = false){
        this.props.onClose();
    }

    componentWillReceiveProps(newProps){
        if(this.props.user){
            if (newProps.currency !== this.props.currency) {
                this.getLimit(newProps.currency)
            } else {
                if(this.state.limit === 0){
                    this.getLimit(this.props.currency)
                }
            }
        }
    }
    
    fetchOrderBook() {

        let tempList = []
        let currency

        if(this.props.currency){
            currency = this.props.currency
        }else{
            currency = 'BTC'
        }

        let data = {
            currency:currency,
            limit:this.state.limit,
            offSet:this.state.offSet
        }
        orderBookActions.orderBookRaw(this.getToken(),data).then(res => {

            tempList = res.data.result

            let concatedArray = tempList.concat(this.state.orderBook);
            
            this.setState({
                orderBook:concatedArray
            })

        }).catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: orderBookModel | FUNC: fetchOrderBook',
                url: `orderbook/getOrders`
            }
            NetworkError(err, errorDetails);
        });
    }

    componentWillMount() {
        this.onSetLanguage();
        this.fetchOrderBook();
    }

    
    loadMore(){
        let offsetSet = this.state.offSet + 10
        this.setState({
            offSet:offsetSet
        })
        this.fetchOrderBook();
    }

    render(){

        var totalBuy = 0
        var totalAsk = 0

        var countBuy = 0
        var countAsk = 0

        return (
            <div>
                <Modal isOpen={this.props.show} toggle={() => this.close(true)} className="modal-dialog-centered order-book-model" style={{maxWidth:"1000px"}}>
                    <ModalHeader toggle={() => this.close(true)}>
                        Order Book
                    </ModalHeader>
                    <ModalBody className="modal-body tc-modal-body">
                        <div className="m-portlet__body" style={{display:"inline-flex",width:"100%",paddingTop:"15px",paddingLeft:"0px",paddingRight:"0px"}}>
                            <div className="col-md-6">
                                
                                    <table className="table referral-details-table example-table">
                                        <thead style={{background:"#1AA74A"}}>
                                            <tr className="referral-details-table-header">
                                                <th style={{color:"#ffffff"}}>Buy</th>
                                                <th style={{color:"#ffffff"}}></th>
                                                <th style={{color:"#ffffff"}}></th>
                                            </tr>
                                        </thead>
                                        <tr className="referral-details-table-row" style={{background:"#F7F7F7"}}>
                                            <td style={{color:"#666666", fontWeight:"700"}}>Price</td>
                                            <td style={{color:"#666666", fontWeight:"700"}}>Quantity</td>
                                            <td style={{color:"#666666", fontWeight:"700"}}>Total</td>
                                        </tr>
                                        {
                                            this.state.orderBook.map((r,index) => {

                                                if(r.BidAsk == "buy"){

                                                    countBuy++

                                                    totalBuy = totalBuy + this.state.orderBook[index].AvailableQuantity 

                                                    return (
                                                        <tr className="referral-details-table-row" style={{fontSize:"10px"}}>
                                                            <td>{r.Price}</td>
                                                            <td>{r.Quantity}</td>
                                                            <td style={{color:"#1AA74A"}}>{totalBuy}</td>
                                                        </tr>
                                                    )

                                                }
                                            
                                            })
                                        }
                                    </table>
                            
                            </div>
                            <div className="col-md-6">
                                
                                    <table className="table referral-details-table example-table">
                                        <thead style={{background:"#C67900", color:"#ffffff"}}>
                                            <tr className="referral-details-table-header">
                                                <th style={{color:"#ffffff"}}>Ask</th>
                                                <th style={{color:"#ffffff"}}></th>
                                                <th style={{color:"#ffffff"}}></th>
                                            </tr>
                                        </thead>
                                        <tr className="referral-details-table-row" style={{background:"#F7F7F7"}}>
                                            <td style={{color:"#666666", fontWeight:"700"}}>Price</td>
                                            <td style={{color:"#666666", fontWeight:"700"}}>Quantity</td>
                                            <td style={{color:"#666666", fontWeight:"700"}}>Total</td>
                                        </tr>
                                        {
                                            this.state.orderBook.map((r,index) => {
                                            
                                                if(r.BidAsk == "sell"){

                                                    countAsk++

                                                    totalAsk = totalAsk + this.state.orderBook[index].Quantity 

                                                    return (
                                                        <tr className="referral-details-table-row" style={{fontSize:"10px"}}>
                                                            <td>{r.Price}</td>
                                                            <td>{r.Quantity}</td>
                                                            <td style={{color:"#C67900"}}>{totalAsk}</td>
                                                        </tr>
                                                    )
                                                
                                                }
                
                                            })
                                        }
                            
                                    </table>
                            </div>
                        </div>
                        <button onClick={() => this.loadMore()} type="button" className="btn btn-outline-blue" style={{fontSize:"12px",padding:"1px 8px 1px",marginTop:"10px"}}>
                            Load More
                        </button>
                    </ModalBody>
                </Modal>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        language: state.language
    };
};

export default connect(mapStateToProps, null) (OrderBookModel);
