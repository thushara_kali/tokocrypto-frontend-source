import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import { connect } from 'react-redux';
import amountFormatter from '../../helpers/amountFormatter';
import cryptoFormatter, { cryptoFormatterByCoin } from '../../helpers/cryptoFormatter';
import amountFormatterRound from '../../helpers/amountFormatterRound';
import history from '../../history';
import orderBookActions from '../../core/actions/client/orderBook';
import homeActions from '../../core/actions/client/home';
import NetworkError from '../../errors/networkError';
import { Loader } from '../tcLoader';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { NotificationManager } from 'react-notifications';
import async from 'async';
import classnames from 'classnames';
import Pagination from "react-js-pagination";
import { ClapSpinner } from "react-spinners-kit";
import { getUserAttributes, updateAttributes } from '../../core/react-cognito/attributes.js'

var awsIot = require('aws-iot-device-sdk');

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class Orders extends Component {

    constructor(){
        super();
        this.state = {
            ordersList: [],
            limit:5,
            offset:0,
            count:0,
            activePage:1,
            loading: false
        }
    }

    getToken() {
        if(this.props.user){
            let username = this.props.user.username
            let clientId = this.props.user.pool.clientId
            let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
            return token
        }else{
            return 0
        }
    }

    fetchOrders() {

        let data = {
            limit:this.state.limit,
            offset:this.state.offset,
            count:true,
            sort:"asc",
            sortBy:"createdAt"
        }
        orderBookActions.orders(this.getToken(),data).then(res => {
           
            this.setState({
                ordersList:res.data.result.result,
                count:res.data.result.count
            })
            
        }).catch(err => {
            NetworkError(err);
            // let error = 'Error Fetch Orders'
            // NetworkError(err,error);
        });

    }

    cancelOrder(orderId) {

        this.setState({
            loading:true
        });

        let data = {
            order_id: orderId
        }
        orderBookActions.cancelOrder(this.getToken(),data).then(res => {
            this.fetchOrders();
            NotificationManager.success("Order Canceled");
            this.setState({
                loading: false
            });
            // this.setState({
            //     ordersList : res.data.result
            // })
        }).catch(err => {
            // let errors = "Error Order Cancelling";
            this.setState({
                loadingBuy:false,
                loadingAsk:false,
                loading:false
            });
            // NotificationManager.error(errors);
            NetworkError(err);
        });
    }

    cancelOrders() {

        this.setState({
            loading:true
        });

        let data = {
            
        }
        orderBookActions.cancelOrders(this.getToken(),data).then(res => {
            this.fetchOrders();
            NotificationManager.success("All Orders Canceled");
            // this.setState({
            //     ordersList : res.data.result
            // })
            this.setState({
                loading:false
            });
        }).catch(err => {
            // NotificationManager.error("Error Cancelling Orders");
            NetworkError(err);
            this.setState({
                loading:false
            });
           
        });
    }

    changePage(pagenumber){

        let offsetSet = (pagenumber - 1) * this.state.limit

        this.setState({
            offset:offsetSet
        })

        this.fetchOrders();

        this.setState({
            activePage:pagenumber
        })

    }

    componentDidMount(){
        var  _self = this;
        this.fetchOrders();
        // this.fetchOrdersByGroup();
        // this.props.orderSubmited(this.updateTable.bind(this));
        // if(!this.autoFetchOrders){
        //     this.autoFetchOrders = setInterval(function()  {
        //         _self.fetchOrders();
        //     }, 1000 * 5);

	    //     if(window.intervals2){
		//         window.intervals2.push(this.autoFetchOrders)
	    //     } else {
		//         window.intervals2 = [this.autoFetchOrders]
        //     }
        // }
    }

    updateTable() {
        this.fetchOrders();
    }

    componentWillUnmount() {
        
	    // window.intervals2.map((i, index) => {
		//     clearInterval(i)
		//     window.intervals2.splice(index, 1);
	    // });
    }

    render(){

        return (

            <div className="col-md-12">
                <div className="m-portlet m-portlet--full-height m-portlet--fit">
                    <div className="m-portlet__head" style={{height:"3.1rem"}}>
                        <div className="m-portlet__head-caption">
                            <div className="m-portlet__head-title">
                                <h3 className="m-portlet__head-text">
                                    Orders
                                </h3>
                            </div>
                        </div>
                    </div>
                    {this.state.ordersList.length > 0 ?
                        <div className="m-portlet__body" style={{display:"inline-flex",width:"100%",padding:"0"}}>
                            <div className="col-md-12" style={{padding:"0",overflowX:"scroll"}}>
                                
                                <table className="table referral-details-table">
                                    <thead>
                                        <tr className="referral-details-table-header">
                                            <th style={{color:"#007EA5",fontSize:"12px"}}>Pair</th>
                                            <th style={{color:"#007EA5",fontSize:"12px"}}>Type</th>
                                            <th style={{color:"#007EA5",fontSize:"12px"}}>Buy/Sell</th>
                                            <th style={{color:"#007EA5",fontSize:"12px"}}>Quantity</th>
                                            <th style={{color:"#007EA5",fontSize:"12px"}}>Price</th>
                                            <th style={{color:"#007EA5",fontSize:"12px"}}>Status</th>
                                            <th style={{color:"#007EA5",fontSize:"12px"}}>Placed</th>
                                            <th style={{color:"#007EA5",fontSize:"12px"}}>Action</th>
                                        </tr>
                                    </thead>
                                    {/* <tr className="referral-details-table-row" style={{background:"#F7F7F7"}}>
                                        <td style={{color:"#666666", fontWeight:"700"}}>Buy</td>
                                        <td style={{color:"#666666", fontWeight:"700"}}></td>
                                        <td style={{color:"#666666", fontWeight:"700"}}></td>
                                        <td style={{color:"#666666", fontWeight:"700"}}></td>
                                        <td style={{color:"#666666", fontWeight:"700"}}></td>
                                        <td style={{color:"#666666", fontWeight:"700"}}></td>
                                        <td style={{color:"#666666", fontWeight:"700"}}></td>
                                    </tr>
                                    <tr className="referral-details-table-row" style={{background:"#F7F7F7"}}>
                                        <td style={{color:"#666666", fontWeight:"700"}}>Sell</td>
                                        <td style={{color:"#666666", fontWeight:"700"}}></td>
                                        <td style={{color:"#666666", fontWeight:"700"}}></td>
                                        <td style={{color:"#666666", fontWeight:"700"}}></td>
                                        <td style={{color:"#666666", fontWeight:"700"}}></td>
                                        <td style={{color:"#666666", fontWeight:"700"}}></td>
                                        <td style={{color:"#666666", fontWeight:"700"}}></td>
                                    </tr> */}

                                    { this.state.loading ? 
                                        <tr >
                                            <td colSpan={ 12 } >
                                            <div style={{ flex: 1 , display: 'flex', paddingTop: 10, justifyContent: 'center' }}>
                                                <ClapSpinner
                                                    size={30}
                                                    color="#686769"
                                                    loading={ this.state.loading }
                                                />
                                                </div>
                                            </td>
                                            
                                        </tr> 
                                    :
                                        <tbody>
                                        {
                                            this.state.ordersList.map((r) => {

                                                if(r.Status === "active"){
                                                    
                                                    return (
                                                        <tr className="referral-details-table-row" style={{fontSize:"10px"}}>
                                                            <td>{r.BuyCurrency}/{r.SellCurrency}</td>
                                                            <td>{r.Type}</td>
                                                            <td>{r.BidAsk}</td>
                                                            <td>{ cryptoFormatterByCoin(r.AvailableQuantity, r.BuyCurrency.toString() ) }</td>
                                                            <td>{ amountFormatter(r.Price, this.props.currency) }</td>
                                                            <td>{r.Status}</td>
                                                            <td>{moment(r.createdAt).format("DD-MM-YYYY")}</td>
                                                            <td>  
                                                                <button disabled={this.state.loading} onClick={() => this.cancelOrder(r.id)} type="button" className="btn btn-outline-blue" style={{fontSize:"10px",padding:"1px 8px 1px"}}>
                                                                    X
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    )
                                                
                                                }
                                            })
                                        } 
                                        </tbody> 
                                    }
                                </table>
                                <div style={{display:"inline-flex"}}>
                                    <Pagination
                                        activePage={this.state.activePage}
                                        itemsCountPerPage={this.state.limit}
                                        totalItemsCount={this.state.count}
                                        pageRangeDisplayed={5}
                                        onChange={(page) => this.changePage(page)}
                                    />
                                    {this.state.loading ?
                                        <div style={{ textAlign:"center"}}><Loader size="40px" /></div> :
                                        <button onClick={() => this.cancelOrders()} type="button" className="btn btn-outline-danger" style={{fontSize:"10px", marginLeft:"15px"}}>
                                            Cancel All
                                        </button>  
                                    }
                                </div>
                            </div>
                        </div> :
                      
                        <div className="no-trades-accounts">
                            <span>No Orders Found</span>
                        </div>
                     
                    }

                </div>
            </div>

        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
	    language: state.language
    };
};

export default connect(mapStateToProps, null)(Orders);
