import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { cryptoFormatterReplaceFormat, cryptoFormatterByCoin } from '../../helpers/cryptoFormatter';
import amountFormatter from '../../helpers/amountFormatter';
import currencyFormatter from '../../helpers/currencyFormatter';
import orderBookActions from '../../core/actions/client/orderBook';
import homeActions from '../../core/actions/client/home';
import { getUserAttributes } from '../../core/react-cognito/attributes.js'
import NetworkError from '../../errors/networkError';
import { Loader } from '../tcLoader';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import classnames from 'classnames';
import { NotificationManager } from 'react-notifications';
import ReactTooltip from 'react-tooltip';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

var fetchOrderAskTime
var fetchOrderBidTime

class OrderNow extends Component {

    constructor(){
        super();
        this.state = {
           quantity: '',
           limitPrice: '',
           buffer : 0.001,
           type: "limit",
           activeTab: "limit",
           submit: true,
           rates: [],
           loadingBuy:false,
           loadingAsk:false,
           orderBookModel:false,
           usableIDR:0,
           usableCrypto:0,
           curr:'BTC',
            orderBookBid: [],
            orderBookAsk: [],
            currentAskPrice:0,
            currentBidPrice:0,
            totalEstimatedBuy:'',
            totalEstimatedSell:'',
            fee:0.2,
            discount_fixed:0,
            discount_percentage:0,
            loadingPercentage:false,
            bidProcess:false,
            askProcess:false
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})

            const re = /^(\d+(\,\d*)?)$/;
            if (event.target.value == '' || re.test(event.target.value)) {
                obj[fieldName] = event.target.value
            }

            this.setState(
                obj
            )
        }

        this.updateNumber = _.debounce(this.updateNumber, 500);
        this.updateAmountNumber = _.debounce(this.updateAmountNumber, 500);
    }

    onChangeHandler = e => {
        const re = /^(\d+(\.\d*)*?)$/;
        if (e.target.value == "" || re.test(e.target.value)) {
          this.setState({ limitPrice: e.target.value });
        }
    
        this.updateNumber();
    };

    amountChangeHandler = e => {

        let currency = this.props.currency.toUpperCase()

        if ( currency === 'SWIPE' ){
            // regex normal
            const re2 = /^[1-9]\d*$/;;
            
            if (
            e.target.value == "" ||
            re2.test(e.target.value)
            ) {
            this.setState({ quantity: e.target.value });
            }
        
            this.updateAmountNumber();
        } else {
            // regex untuk mencegah '00'
            const re3 = /(^[0]((\,|\.)\d*)?$)/;
            // regex normal
            const re2 = /(^[1-9][0-9]*((\,|\.)\d*)?$)/;
        
            
            if (
            e.target.value == "" ||
            re2.test(e.target.value) ||
            re3.test(e.target.value)
            ) {
            this.setState({ quantity: e.target.value });
            }
        
            this.updateAmountNumber();
        }
    };

    updateAmountNumber = () => {
        let value = this.state.quantity.toString();
        
        this.setState({
          quantity: value.replace(/\./g, ",")
        });
    };

    updateNumber = () => {
        let amount = this.state.limitPrice;
        // 
        let newAmount = 0;
        if (amount.replace(/\./g, "").length > 6) {
          newAmount = amount.replace(/\d{3}$/, "000");
          // amount.replace(/\d{3}$/, "000");
          this.setState({ limitPrice: amountFormatter(newAmount) }, () => {
          });
        } else {
          this.setState({ limitPrice: amountFormatter(amount) }, () => {
          });
        }
    };
    getToken() {
        if(this.props.user){
            let username = this.props.user.username
            let clientId = this.props.user.pool.clientId
            let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
            return token
        }else{
            return 0
        }
    }

    getBuyEstimate(){
        let limitPrice = this.state.limitPrice.replace(/\./g, '');
        let quantity = Number(cryptoFormatterReplaceFormat(this.state.quantity));
        let fee = 0;
        let result = 0;

        fee = limitPrice * quantity * this.state.fee / 100;

        if(this.state.activeTab === 'limit'){
            result = (limitPrice * quantity) + fee;
        }else if(this.state.activeTab === 'market'){
        
            result = this.state.currentAskPrice * quantity + fee;
        }

        return result;
    }

    getSellEstimate(){
        let limitPrice = this.state.limitPrice.replace(/\./g, '');
        let quantity = Number(cryptoFormatterReplaceFormat(this.state.quantity));
        let fee = 0;
        let result = 0;

        fee = limitPrice * quantity * this.state.fee / 100;

        if(this.state.activeTab === 'limit'){
            result = limitPrice * quantity - fee;
        }else if(this.state.activeTab === 'market'){
            result = this.state.currentBidPrice * quantity - fee;
        }

        return result;
    }

    getQuantity(percent){
        if(this.state.limitPrice === ''){
            NotificationManager.warning("Please input your limit price!");
            return;
        }

        this.setState({
            loadingPercentage: true
        });

        let data = {};
        if(this.state.activeTab === 'limit'){
            data = {
                currency: this.state.curr.toUpperCase(),
                percentage: percent,
                fixed_side: 'buy',
                limit_price: this.state.limitPrice.replace(/\./g, '')
            };
        }else{
            data = {
                currency: this.state.curr.toUpperCase(),
                percentage: percent,
                fixed_side: 'buy'
            };
        }
        

        orderBookActions.getQuantity(this.getToken(),data).then(res => {
            let data = res.data;
            let quantity;
            
            if(data){
                quantity = cryptoFormatterByCoin(data.qty, this.state.curr.toUpperCase());
                this.setState({
                    quantity: quantity,
                    fee: data.fee,
                    totalEstimatedBuy: data.total,
                    loadingPercentage: false
                });

            }

        }).catch(err => {
            this.setState({
                loadingPercentage: false
            });
            // NotificationManager.error("Error Fetch Percentage");
            NetworkError(err);
        });
    }

    getFee(){
        let data = {};

        orderBookActions.getFee(this.getToken(),data).then(res => {
            this.setState({
                fee:res.data.fee,
                discount_fixed:res.data.discount_fixed,
                discount_percentage: res.data.discount_percentage
            });
        }).catch(err => {
            // NotificationManager.error("Error Fetch Your Fee");
            NetworkError(err);
        });
    }

    submitOrder(type) {

        let buyCurrency
        let sellCurrency
        let fixedSide
        let limitPrice = this.state.limitPrice.replace(/\./g, '')

        if(type == 'buy'){
            buyCurrency = this.props.currency
            sellCurrency = "IDR"
            fixedSide = "buy"

            this.setState({
                loadingBuy:true
            });
        } else if (type == 'sell'){
            buyCurrency = "IDR"
            sellCurrency = this.props.currency
            fixedSide = "sell"

            this.setState({
                loadingAsk:true
            });
        }

        let data

        if(this.state.type == "market"){
            data = {
                quantity: Number(cryptoFormatterReplaceFormat( this.state.quantity )) ,
                type: this.state.type,
                buyCurrency: buyCurrency,
                sellCurrency: sellCurrency,
                fixedSide: fixedSide
            };
        } else {
            data = {
                quantity: Number(cryptoFormatterReplaceFormat( this.state.quantity)),
                price: Number(limitPrice),
                type: this.state.type,
                buyCurrency: buyCurrency,
                sellCurrency: sellCurrency,
                fixedSide: fixedSide
            };
        }

        orderBookActions.buyOrder(this.getToken(),data).then(res => {
          
            this.setState({
                // TC-938: Remove Auto Clear
                // To prevent Auto Clear after submit, quantity and limitPrice are commented
                // Uncomment to make Auto Clear available. 
                // quantity: '',
                // limitPrice: '',

                loadingBuy:false,
                loadingAsk:false
            });

            this.props.orderSubmitted();

            NotificationManager.success("Order Placed");
           
        }).catch(err => {
            NetworkError(err);
            this.setState({
                loadingBuy:false,
                loadingAsk:false
            });
            // let errors = "Order Place Failed";
            // if (err.response && err.response.data.err.message) {
        
            //     errors = err.response.data.err.message;

            //     NotificationManager.error(errors);
            // } else {
            //     NotificationManager.error(errors);
            // }
         
        });
    }

    changeActiveTab(tab){
        if(tab == 'limit'){
            this.setState({
                activeTab:'limit',
                type: "limit"
            })
        } else if (tab == 'market'){
            this.setState({
                activeTab:'market',
                type: "market"
            })
        }
    }

    FetchUser(curr){
        getUserAttributes(this.props.user).then(attr => {
            let user_id = attr.sub;
            this.getAvailableTrade(user_id, this.state.curr.toUpperCase());
        })
        .catch(err => {
            NetworkError(err);
            // NotificationManager.error("Error Fetch User");
        });
    }

    getAvailableTrade(userId, currency){
        let data = {
            "user_id" : userId
        }
        
        homeActions.usableBalance(this.getToken(),data).then(res => {
            let a = 0;
            let b = 0;
            
            res.data.data.map(function (i) {
                if (i.Currency.Code === 'IDR') {
                    a = i.UsableBalance
                }
                if (i.Currency.Code === currency) {
                    b = i.UsableBalance
                }
            })
            this.setState({
                usableIDR: a,
                usableCrypto: b
            })
           
        }).catch(err => {
            // 
            NetworkError(err);
            // NotificationManager.error("Error Fetch Avaialable Orders");
        });
    }

    resetInput() {
        this.setState({
            quantity: "",
            limitPrice: ""
        })
    }

    getrates() {
        let RatesPair = _.find(this.state.rates, { currencyPair: this.state.curr+'IDR'})
        if(RatesPair){
            let limit = amountFormatter((1.001 * RatesPair.customer_buy),'default','down');
            this.setState({
                currentAskPrice: RatesPair.customer_buy,
                currentBidPrice: RatesPair.customer_sell,
                limitPrice: limit 
            });
        }
    }

    async componentWillReceiveProps(nextProps) {
        // You don't have to do this check first, but it can help prevent an unneeded render
        if (nextProps.currency !== this.state.curr) {
            this.resetInput();
            await this.setState({ curr: nextProps.currency });
            this.getrates();
        }
        if (nextProps.rates !== this.state.rates) {
            await this.setState({
                rates: nextProps.rates,
            });
            this.getrates();
        }
    }

    orderFullList() {
        this.setState({
            orderBookModel: true
        })
    }

    componentDidMount(){
        if(this.props.user){
            this.FetchUser();
            this.getFee();
        }
    }

    render(){

        let applyDisabled
        let img_coin_wallet = "../../assets/media/img/wallet/crypto-wallet.png";
        let UsableBalance = 0
        if(this.state.activeTab == "limit"){
            applyDisabled = this.state.limitPrice.length === 0 || this.state.quantity.length === 0 ;
        } else {
            applyDisabled = this.state.quantity.length === 0 ;
        }

        if(this.props.IDRAccount.UsableBalance){
            UsableBalance = this.props.IDRAccount.UsableBalance
        }

        return (


            <div className={classnames({"col-md-12": true,"user-disable": this.props.user == null})}>
                {!this.props.user?<div className="user-unblock-content-ordernow"><a href="/login">Please Login or Register Unblock content</a></div>:null}
                <div className={classnames({"m-portlet m-portlet--full-height m-portlet--fit order-now-widget": true,"user-disable-opacity": this.props.user == null})}>
                    <div className="m-portlet__head" style={{height:"3.1rem"}}>
                        <div className="m-portlet__head-caption">
                            <div className="m-portlet__head-title">
                                <h3 className="m-portlet__head-text">
                                    Order Now
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div className="m-portlet__body" style={{ paddingLeft: "0px", paddingRight: "0px", paddingTop:"0px"}}>
                        {this.state.submit == true}
                        <div className="m-widget4 m-widget4--chart-bottom">
                            <div>
                                <div className="m-portlet__head dashboard-trading-desk-tabs">
                                    <div className="m-portlet__head-tools col-md12">
                                        <ul className="nav nav-tabs m-tabs m-tabs-line m-tabs-line--left m-tabs-line--primary width100" role="tablist">
                                            <li className="nav-item m-tabs__item col-md-4 nomargins" onClick={() => this.changeActiveTab('limit')}>
                                                <a style={{fontSize:"12px", textAlign:"center", padding:"5px"}} className={classnames({ 'nav-link m-tabs__link ': true, 'active-dashboard-crypto-tabs-btc': this.state.activeTab === 'limit'})} data-toggle="tab" href="#m_user_profile_tab_1"
                                                    role="tab">
                                                    Limit
                                                </a>
                                            </li>
                                            <li className="nav-item m-tabs__item col-md-4 nomargins" onClick={() => this.changeActiveTab('market')}>
                                                <a style={{fontSize:"12px", textAlign:"center", padding:"5px"}} className={classnames({ 'nav-link m-tabs__link ': true, 'active-dashboard-crypto-tabs-eth': this.state.activeTab === 'market'})} data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                                    Market
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12 mobile-usable-balance">
                            <div className="row mobile-usable-balance-inline">
                                <ReactTooltip id="walletIdr" place="top">
                                    {/* <span>Available IDR: {amountFormatter(this.state.usableIDR)}</span>  */}
                                    <span>Usable Balance</span>
                                </ReactTooltip>
                                <ReactTooltip id="coinIdr" place="top">
                                    {/* <span>Available {this.props.currency}: {cryptoFormatterByCoin</span>  */}
                                    <span>Usable {this.state.curr.toUpperCase()}</span>
                                </ReactTooltip>
                                <div className="col-md-6 limit-market-field" data-tip data-for="walletIdr">
                                    <img className="custom-img-limit-maker" src="../../assets/media/img/wallet/wallet-IDR.png" alt="" />
                                    {amountFormatter(UsableBalance)}
                                </div>
                                
                                <div className="col-md-6 limit-market-field" data-tip data-for="coinIdr">
                                    <img className="custom-img-limit-maker" src={img_coin_wallet} alt="" />
                                    {cryptoFormatterByCoin(this.state.usableCrypto, this.state.curr.toUpperCase())}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12" style={{marginBottom:"15px"}}>
                            <div className="row">
                                <div className="col-md-12" style={{display:"inline-flex", marginTop:"15px"}}>
                                    <div className="col-md-4" style={{fontSize:"12px", letterSpacing:"0", color:"#777777", padding:"9px"}}>
                                         Quantity
                                    </div>
                                    <div className="col-md-8">
                                        <div className="row">
                                                {this.state.loadingPercentage ?
                                                    <div style={{ textAlign:"center"}}><Loader size="40px" /></div> :
                                                    <div className="m-input-icon m-input-icon--right col-md-12">

                                                        <span className="m-input-icon__icon m-input-icon__icon--left buy-sell-input-label-order">
                                                            <span style={{ marginLeft: "15px", color:"#B8B8B8" }}>{this.props.currency}</span>
                                                        </span>
                                                        <input 
                                                            // onChange={this.handleChange('quantity')} 
                                                            onChange={this.amountChangeHandler}
                                                            // TC-938: Remove Auto Clear
                                                            // To prevent Auto Clear after submit, quantity commented
                                                            // onFocus={() => this.resetInput("quantity")} 
                                                            value={this.state.quantity} 
                                                            type="text" 
                                                            className="form-control m-input custom-buy-sell-input-order" 
                                                            style={{fontSize:"12px", textAlign:"right", background:"#F7F7F7", color:"#222222de"}}
                                                        />
                                                    </div>
                                                }
                                        </div>
                                    </div>
                                </div>
                                {this.state.type == "limit"?
                                    <div className="col-md-12" style={{display:"inline-flex", marginTop:"15px"}}>
                                        <div className="col-md-4" style={{fontSize:"12px", letterSpacing:"0", color:"#777777", padding:"9px"}}>
                                            Limit Price
                                        </div> 
                                        <div className="col-md-8">
                                            <div className="row">
                                                <div className="m-input-icon m-input-icon--right col-md-12">
    
                                                    <span className="m-input-icon__icon m-input-icon__icon--left buy-sell-input-label-order">
                                                        <span style={{ marginLeft: "15px", color:"#B8B8B8" }}> {currencyFormatter('')}</span>
                                                    </span>
    
                                                    <input 
                                                        onChange={ this.onChangeHandler }
                                                        // TC-938: Remove Auto Clear
                                                        // To prevent Auto Clear after submit, limitPrice commented 
                                                        // onFocus={() => this.resetInput("limitPrice")} 
                                                        value={ this.state.limitPrice } 
                                                        type="text" 
                                                        className="form-control m-input custom-buy-sell-input-order" 
                                                        style={{fontSize:"12px", textAlign:"right", background:"#F7F7F7", color:"#222222de"}}
                                                    />
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>:null
                                }
                            </div>
                        </div>

                        {/* <div className="col-md-12" style={{display:"inline-flex",padding:"10px"}}>
                                <div className="col-md-3">
                                    <button className="btn-percent-blue" type="button" onClick={() => this.getQuantity(25)}>25%</button>
                                </div>
                                <div className="col-md-3">
                                    <button className="btn-percent-blue" type="button" onClick={() => this.getQuantity(50)}>50%</button>
                                </div>
                                <div className="col-md-3">
                                    <button className="btn-percent-blue" type="button" onClick={() => this.getQuantity(75)}>75%</button>
                                </div>
                                <div className="col-md-3">
                                    <button className="btn-percent-blue" type="button" onClick={() => this.getQuantity(100)}>100%</button>
                                </div>
                        </div> */}
                        <div className="col-md-12" style={{display:"inline-flex"}}>
                            <div className="buy-estimated col-md-5">
                                Total IDR(Buy estimated)
                            </div>
                            {this.state.loadingPercentage || this.state.askProcess ?
                                <div style={{ textAlign:"center"}}><Loader size="40px" /></div> :
                                <div className="col-md-7" style={{paddingTop:"9px"}}>
                                    { currencyFormatter( amountFormatter(this.getBuyEstimate()) ) }
                                </div>
                            }
                        </div>
                        <div className="col-md-12" style={{display:"inline-flex"}}>
                            <div className="buy-estimated col-md-5">
                                Total IDR(Sell estimated)
                            </div>
                            {this.state.loadingPercentage || this.state.bidProcess ?
                                <div style={{ textAlign:"center"}}><Loader size="40px" /></div> :
                                <div className="col-md-7">
                                    { currencyFormatter( amountFormatter(this.getSellEstimate()) ) }
                                </div>
                            }
                        </div>
                        <div className="col-md-12" style={{marginTop:"17px"}}>
                            <div className="row">
                                <div className="col-md-6">
                                    {this.state.loadingBuy ?
                                        <div style={{ textAlign:"center"}}><Loader size="40px" /></div> :
                                        <button onClick={() => this.submitOrder("buy")} disabled={applyDisabled} type="button" className="btn btn btn-green btn-block" style={{fontSize:"14px"}}>
                                            Buy / Bid
                                        </button>
                                    }
                                </div>
                                <div className="col-md-6">
                                    {this.state.loadingAsk ?
                                        <div style={{ textAlign:"center"}}><Loader size="40px" /></div> :
                                        <button onClick={() => this.submitOrder("sell")} disabled={applyDisabled} type="button" className="btn btn-brown btn-block" style={{fontSize:"14px"}}>
                                            Sell / Ask
                                        </button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
	    language: state.language
    };
};

export default connect(mapStateToProps, null)(OrderNow);
