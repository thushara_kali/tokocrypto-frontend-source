import React, { Component } from 'react';
import classnames from 'classnames';
import commonPassword from 'common-password';
import ReactTooltip from 'react-tooltip'
import history from '../history';
import LocalizedStrings from 'react-localization';
import enDictionary from '../languages/en.json';
import idDictionary from '../languages/id.json';
import javaDictionary from '../languages/java.json';
import sundaDictionary from '../languages/sunda.json';
import { connect } from 'react-redux';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

/*
    TCDOC-001 
    components for password checker
*/
class PasswordChecker extends Component {

    constructor(){
        super();
        this.state = {
            help: false,
            show: false
        }
    }

    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    /*
        TCDOC-001 
        password validated on change text
    */
    onChangePassword(e) {
        let minimumEight = e.target.value.length >=8;
        // let patt = /[^a-zA-Z0-9\s]/g;
        // let specialChar = patt.test(e.target.value);
        // let pattDigits = /[^\sa-zA-Z]+/g;
        // let digits = pattDigits.test(e.target.value);
	    let pattNumbers = /\d+/g;
	    let numbers = pattNumbers.test(e.target.value);
        let pattUpper = /[A-Z]/g;
        let uppers = pattUpper.test(e.target.value);
        let pattLower = /[a-z]/g;
        let lowers = pattLower.test(e.target.value);
        let hasBadPasswords = commonPassword(e.target.value) || e.target.value === '';

	    let upLoNum = uppers && lowers && numbers;

        this.props.setValid(minimumEight && upLoNum && !hasBadPasswords);
        this.props.onChange(e);
    }

    onBlurHelp(e) {
        
        if(!e.target.classList.contains('keep-help')){
            this.setState({
                help: false
            });
        }
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    /*
        TCDOC-001 
        password also checked on render to give visible clue for user of what his password strength
    */
    render(){

        let minimumEight = this.props.value.length >=8;
        // let patt = /[^a-zA-Z0-9\s]/g;
        // let specialChar = patt.test(this.props.value);
        // let pattDigits = /[^\sa-zA-Z]+/g;
        // let digits = pattDigits.test(this.props.value);
        let pattUpper = /[A-Z]/g;
	    let pattNumbers = /\d+/g;
	    let numbers = pattNumbers.test(this.props.value);
        let uppers = pattUpper.test(this.props.value);
        let pattLower = /[a-z]/g;
        let lowers = pattLower.test(this.props.value);
        let hasBadPasswords = commonPassword(this.props.value) || this.props.value === '';

	    let upLoNum = uppers && lowers && numbers;

        let formType = "password";

        if(this.state.show) {
            formType = "text";
        }

	    let marginBottom = 0.5;
	    let heightRight = 73;
	    let isReset = history.location.pathname === '/reset-password';

	    if(this.props.help && !isReset) {
		    heightRight = 74
		    marginBottom = 0.5
	    } else if(!isReset && !this.props.help) {
		    heightRight = 72
		    marginBottom = 0
	    } else if(this.props.help && isReset) {
		    heightRight = 74
		    marginBottom = 0.5
	    } else if(!this.props.help && isReset){
		    marginBottom = 1;
	    }

        return (
            <div>
                <div onFocus={() => this.setState({ help: true })} className="form-group m-form__group form-password">
                    <div className="input-group keep-help">
							<input autocomplete="off" onFocus={() => this.props.forceOpenHelpText()} type="text" className="form-control m-input keep-help" aria-describedby="basic-addon2" type={formType} placeholder={strings.PASSWORD}  id="pass" onChange={(e) => this.onChangePassword(e)} />
							<div className="input-group-append keep-help" onMouseDown={() => this.setState({ show: true})} onMouseUp={() => this.setState({ show: false})}><span style={{ height: heightRight + "%", marginBottom: marginBottom+"px"}} className="input-group-text input-group-password keep-help">SHOW</span></div>
					</div>
                    {/* <input className="form-control m-input m-login__form-input--last login-inputs" type={formType} placeholder="Password"  id="pass" onChange={(e) => this.onChangePassword(e)}/>
                    <i className="fa fa-eye" onClick={() => this.setState({ show: !this.state.show})}></i> */}
                </div>
                { this.props.help &&
                    <div className="password-strength mx-auto">
                        <div className="row">
                            <div className="col-12">
                                <p>{strings.PASSWORD_INCLUDING_PROPERTIES} </p>
                                <div className="row">
                                    <div className="col-md-7">
                                        <span className={classnames({ 'valid': minimumEight, 'invalid': !minimumEight })}><i className="fa fa-circle fa-fw"></i>{strings.EIGHT_MORE_CHARACTER}</span><br />
                                        <span className={classnames({ 'valid': upLoNum, 'invalid': !upLoNum })}><i className="fa fa-circle fa-fw"></i>{strings.UPPER_CASE_LETTER_STYMBOL_NUMBER}</span>
                                    </div>
                                    <div className="col-md-5">
                                        <span className={classnames({ 'valid': !hasBadPasswords, 'invalid': hasBadPasswords })}><i className="fa fa-circle fa-fw"></i>{strings.NOT_COMMON_PASSWORD}</span><br />
                                        <a data-tip="Passwords such as ‘password1’ or ‘Password’ are not permitted." style={{marginLeft: "15px"}}>{strings.LEARN_MORE}</a>
                                        <ReactTooltip />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

export default connect(mapStateToProps, null)(PasswordChecker);
