import React, {Component} from 'react';
import actions from '../core/actions/client/home';
import classnames from 'classnames';
import {GoogleCharts} from '../googleCharts';
import _ from 'lodash';
import numeral from 'numeral';
import moment from 'moment';
import amountFormatter from '../helpers/amountFormatter';
import NetworkError from '../errors/networkError';

class LiveRateLineChart extends Component {

    constructor(){
        super();
        this.state = {
            liveCurrencyPair: "BTCIDR",
            liveType: "hours",
            liveRates: [],
            o: 0,
            h: 0,
            l: 0,
            c: 0,
            rising: false,
            currentBTC: 0,
            currentETH: 0,
            changesPercentage: 0,
            changesAmount: 0
        };
    }

    fetchInitialRate(){
        actions.fetchLiveRate("BTCIDR", this.state.liveType).then(res => {
            let last_item = res.data.data[res.data.data.length -1 ];
            this.setState({
              currentBTC: last_item.Close
            });
        });

        actions.fetchLiveRate("ETHIDR", this.state.liveType).then(res => {
            let last_item = res.data.data[res.data.data.length -1 ];
            this.setState({
                currentETH: last_item.Close
            });
        });
    }

    fetchLiveRate(){
        actions.fetchLiveRate(this.state.liveCurrencyPair, this.state.liveType).then(res => {

            var series = { name: this.state.liveCurrencyPair, values: [] };

            res.data.data.forEach(d => {
                d.UTCx = new Date(d.DateTime);
                d.x = new Date(d.UTCx.getTime() - d.UTCx.getTimezoneOffset()*60*1000);
                series.values.push({ x: d.x, open: d.Open, high: d.High, low: d.Low, close: d.Close});
            });

            let lastTwoOpen = series.values[series.values.length - 2].open;
            let lastOpen = series.values[series.values.length - 1].open;
            let lastClose = series.values[series.values.length - 1].close;
            let lastHigh = series.values[series.values.length - 1].high;
            let lastLow = series.values[series.values.length - 1].low;

            let rising = false;

            if (lastTwoOpen < lastOpen) {
                rising = true;
            }

            this.setState({
                liveRates: series,
                o: lastOpen,
                h: lastHigh,
                l: lastLow,
                c: lastClose,
                rising: rising
            });

            this.drawChart(series);
        }).catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: liveRateLineChart | FUNC: fetchLiveRate',
                url: `/rates/history?currencyPair=${this.state.liveCurrencyPair}&type=${ this.state.liveType }`
            }
            NetworkError(err, errorDetails);
        });
    }

    onChangeCurrencyPair(pair){
        this.setState({
            liveCurrencyPair: pair
        });

        let self = this;
        setTimeout(() => {
            self.fetchLiveRate();
            self.getSummary();
        }, 150);
    }

    onChangeCurrencyType(type){
        this.setState({
            liveType: type
        });

        let self = this;
        setTimeout(() => {
            self.fetchLiveRate();
        }, 150);
    }

    drawChart(series){
        let seriesData = series;

        GoogleCharts.load(() => {
            let ratesData = seriesData.values.map((r) => {

                let dateFormat = moment(r.x).format('MMM Do YYYY, h:mm:ss a');

                return [
                    r.x, r.close, ''+dateFormat+'\n'+numeral(r.close).format('0,0')
                ];

            });

            let dataTable = new GoogleCharts.api.visualization.DataTable();
            dataTable.addColumn('date', 'Date');
            // dataTable.addColumn('number', 'Low');
            // dataTable.addColumn('number', 'Open');
            dataTable.addColumn('number', 'Close');
            // dataTable.addColumn('number', 'High');
            dataTable.addColumn({type: 'string', role: 'tooltip'});
            dataTable.addRows(ratesData);

            // const data = GoogleCharts.api.visualization.arrayToDataTable(ratesData, true);

            // let max = _.maxBy(ratesData, (r) => r[4])[4];
            // let min = _.minBy(ratesData, (r) => r[1])[1];

            // let tick = 10;
            // let range = max - min;
            // let step = range / tick;
            // let tickRange = [min];

            // let current = min;
            // while(current < max ){
            //     current += step;
            //     tickRange.push(current);
            // }

            let options = {
                legend:'none',
                vAxis: {
                    gridlines: {
                        color: 'transparent',
                    },
                    textPosition: 'none'
                },
                hAxis: {
                    gridlines: {
                        color: 'transparent'
                    },
                    // textPosition: 'none'
                },
                bar: { groupWidth: '100%' },
                chartArea: {
                    top: 10,
                    bottom: 30,
                    right: 0,
                    left: 0
                },
                tooltip: {isHtml: false},
                colors:['#EA973D']
                // vAxis: {
                //     ticks: tickRange
                // }
            };

            const candle_chart = new GoogleCharts.api.visualization.AreaChart(document.getElementById('chart_div'));
            candle_chart.draw(dataTable, options);
        });
    }

    getSummary(){
        actions.getChartSummary(this.state.liveCurrencyPair).then(res => {

            this.setState({
                changesPercentage: res.data.data.month.rates.changes.percentage,
                changesAmount: res.data.data.month.rates.price - res.data.data.month.rates.lastMonth
            })
        }).catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: liveRateLineChart | FUNC: getSummary',
                url: `https://tfxf49mpq1.execute-api.us-east-1.amazonaws.com/dev/rates/changed/${this.state.liveCurrencyPair}`
            }
            NetworkError(err, errorDetails);
        });
    }

    componentDidMount(){
        this.fetchInitialRate();
        this.fetchLiveRate();
        this.getSummary();
    }

    render(){

        let unit = 'day';
        let interval = 1;

        if(this.state.liveType == 'hours'){
            unit = 'hour',
                interval = 1;
        }

        return (
            <div className="container col-md-12 chart-container">
                <div className="chart-header row">
                    <div className="col-3">
                        <p
                            onClick={() => this.onChangeCurrencyPair("BTCIDR")}
                            className={classnames({"color-primary selected-pair-chart": this.state.liveCurrencyPair === "BTCIDR"})}>Bitcoin IDR {amountFormatter(this.state.currentBTC)}</p>
                    </div>
                    <div className="col-3">
                        <p
                            onClick={() => this.onChangeCurrencyPair("ETHIDR")}
                            className={classnames({"color-primary selected-pair-chart": this.state.liveCurrencyPair === "ETHIDR"})}>Ethereum IDR {amountFormatter(this.state.currentETH)}</p>
                    </div>
                    <div className="col-6 chart-intervals">
                        <div className="chart-date-container col-8">
                            <div className={classnames({ active: this.state.liveType === 'days'})} onClick={() => this.onChangeCurrencyType('days')}>Days</div>
                            <div className={classnames({ active: this.state.liveType === 'hours'})} onClick={() => this.onChangeCurrencyType('hours')}>Hours</div>
                        </div>
                        {/* <div className="chart-buttons col-4">
                            { this.state.liveCurrencyPair === "BTCIDR" && <div><img className="accounts-header-image" src={require('../img/opengraph.png')} />Bitcoins</div> }
                            { this.state.liveCurrencyPair === "ETHIDR" && <div><img className="accounts-header-image" src={require('../img/ethereum.png')} />Etherum</div> }
                        </div> */}
                    </div>
                </div>
                <div className="row chart-insight">
                    <div className="col-4 chart-insight-item">
                        { this.state.liveCurrencyPair === "BTCIDR"?
                            <h3>IDR {amountFormatter(this.state.currentBTC)}</h3> :
                            <h3>IDR {amountFormatter(this.state.currentETH)}</h3>
                        }

                        { this.state.liveCurrencyPair === "BTCIDR"?
                            <h6>Bitcoin Price</h6> :
                            <h6>Ethereum Price</h6>
                        }
                    </div>
                    <div className="col-4 chart-insight-item">
                    {this.state.changesAmount >= 0 ?
                        <h3 className="color-success">IDR + {amountFormatter(this.state.changesAmount)}</h3> :
                        <h3 className="color-danger">IDR - {amountFormatter(Math.abs(this.state.changesAmount))}</h3>
                    }
                        <h6>Since Last month</h6>
                    </div>
                    <div className="col-4 chart-insight-item">
                        {this.state.changesPercentage >= 0 ?
                            <h3 className="color-success">+ {this.state.changesPercentage} %</h3> :
                            <h3 className="color-danger">- {Math.abs(this.state.changesPercentage)} %</h3>
                        }
                        <h6>Since Last month (%)</h6>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 row ohlc-wrapper">
                        <div className="col ohlc-text">
                            <span className="ohlc-indicator indicator-first">O</span>: <span className={classnames({"rising": this.state.rising, "falling": !this.state.rising})}>{numeral(this.state.o).format('0,0')}</span>
                            <span className="ohlc-indicator">H</span>: <span className={classnames({"rising": this.state.rising, "falling": !this.state.rising})}>{numeral(this.state.h).format('0,0')}</span>
                            <span className="ohlc-indicator">L</span>: <span className={classnames({"rising": this.state.rising, "falling": !this.state.rising})}>{numeral(this.state.l).format('0,0')}</span>
                            <span className="ohlc-indicator">C</span>: <span className={classnames({"rising": this.state.rising, "falling": !this.state.rising})}>{numeral(this.state.c).format('0,0')}</span>
                        </div>
                    </div>
                </div>
                <br/>
                <div id="chart_div" style={{ height: 300}}>

                </div>

            </div>
        );
    }

}

export default LiveRateLineChart;
