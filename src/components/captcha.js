import React, { Component } from 'react';
// import Recaptcha from 'react-recaptcha';
import config from '../config/recaptcha.json'
import Recaptcha from 'react-grecaptcha';

class Captcha extends Component {

    constructor() {
        super();
    }asd

    verifyCallback(callback) {
        if (callback) {
            
            this.props.onCapthcaSuccess();
        }
    }

    componentWillReceiveProps(props){
        if(props.reset === true) {
            window.grecaptcha.reset();
        }
    }

    render() {
        return (
            <div className="captcha-container">
                {/* <Recaptcha
                    ref={(e) => this.captcha = e}
                    sitekey={config.siteKey}
                    // render="explicit"
                    verifyCallback={(callback) => this.verifyCallback(callback)}
                /> */}
                <Recaptcha 
                    sitekey={config.siteKey}
                    callback={(callback) => this.verifyCallback(callback)}
                />
            </div>
        )
    }

}

export default Captcha;
