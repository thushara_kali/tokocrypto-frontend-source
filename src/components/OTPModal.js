import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import actions from '../core/actions/client/otp';
import { Loader } from './tcLoader';
import NetworkError from '../errors/networkError';
import { NotificationManager } from 'react-notifications';
import classnames from 'classnames';
import amountFormatter from '../helpers/amountFormatter';
import history from '../history';
import cryptoFormatter, { cryptoFormatterByCoin } from '../helpers/cryptoFormatter';
import LocalizedStrings from 'react-localization';
import enDictionary from '../languages/en.json';
import idDictionary from '../languages/id.json';
import javaDictionary from '../languages/java.json';
import sundaDictionary from '../languages/sunda.json';
import NP from 'number-precision';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class OTPModal extends Component {

  constructor() {
    super();
    this.state = {
      method: "email",
      token: "",
      maskedToken: "",
      verifyRequested: false,
      loading: false,
      loadingOTP:false,
      loadingResend: false,
      OTPSent: false
    }
  }

  componentWillReceiveProps(nextProps) {
        // 
        if(this.props.show === false) {
            this.setState({
            token: "",
            maskedToken: "",
            verifyRequested: false,
            loading: false,
            loadingOTP:false,
            loadingResend: false,
            OTPSent: false
            });
        }
      if(nextProps.show === true && this.state.verifyRequested === false){
          // on open
          this.requestVerification();
      }
  }

  getToken() {
    let username = this.props.user.username
    let clientId = this.props.user.pool.clientId
    let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
    return token
  }

  onSetLanguage(){
    strings.setLanguage(this.props.language);
  }

  changeMethod(method) {
    if (method === 'sms') {
      this.setState({
        maskedToken: "",
        method: method
      });
    } else {
      this.setState({
        method: method
      });
    }

  }

  requestVerification(loadingResend = false) {
    let data = {
      type: this.state.method
    }

    this.setState({ loadingOTP: true, loadingResend, OTPSent: false, verifyRequested: true });

    if(loadingResend === false) {
        this.setState({ token: ""});
    }

    let requestOTP = actions.requestOTP;
    
    
    if(this.props.summary) {
        requestOTP = actions.requestOTPWithdrawCrypto;
        data = {
            withdraw_amount: Number(this.props.summary.totalAmount),
            currency:this.props.summary.currency.toUpperCase(),
            address: this.props.summary.address
        }
    } else {
        requestOTP = actions.requestOTPWithdrawFiat;
        data = {
            withdraw_amount: Number(this.props.summaryFiat.totalAmount),
            bank: this.props.summaryFiat.bank,
            account_number: this.props.summaryFiat.accountNumber
        }
    }

    requestOTP(this.getToken(), data).then(res => {
      let response = res.data;
      let OTPSent = false;
      if(loadingResend) {
          // is resend
        OTPSent = true;
      }

      

      if(this.props.method === 'email') {
        NotificationManager.success(strings.OTP_SENT);
      }

      if (this.state.method === 'misscall') {
        this.setState({
          maskedToken: response.data.MaskedToken,
          otpId: response.data.OtpId,
          verifyRequested: true,
          method: response.data.Type,
          loadingResend: false,
          OTPSent
        });
      } else {
        this.setState({
          otpId: response.data.OtpId,
          verifyRequested: true,
          loadingOTP: false,
          method: response.data.Type,
          loadingResend: false,
          OTPSent
        });
      }

    }).catch(err => {
      console.error(err);
      this.setState({ loading: false, loadingResend: false });
      NetworkError(err);
    });
  }

  completeVerification() {
    if (this.state.token.length > 0) {
        this.setState({
            loading: true
        });

        actions.clientMfaVerify(this.state.otpId, this.state.token).then(res => {
            this.props.completeVerification(this.state.otpId, this.state.token);
            // this.setState({
            //     loading: false
            // });
            // NotificationManager.success("Verification success");
        }).catch(err => {
            this.setState({
                loading: false,
                invalidToken: true
            });
            // NotificationManager.error(strings.INVALID_VERIFICATION_CODE);
            NetworkError(err);
            // this.requestVerification();
        })

    }
  }

  close(){
    this.props.toggleShow()
    setTimeout(() => {
        this.setState({
            method: "email",
            token: "",
            maskedToken: "",
            verifyRequested: false,
            loading: false,
            loadingOTP:false,
            invalidToken: false
          });
    }, 50);

  }

  gotoAccounts(){
    this.close();
    if(this.props.summary) {
        history.push('/accounts?currency='+this.props.summary.currency.toUpperCase());
    } else {
        history.push('/accounts?currency=IDR');
    }

  }

  componentWillMount() {
    this.onSetLanguage();
  }

  render() {
    return (
      <div>
        <Modal size="lg" isOpen={this.props.show} toggle={() => this.close()} className={this.props.className}>
          <ModalHeader toggle={() => this.close()}>
            { this.props.transactionComplete? 'Success' : 'Verification' }
          </ModalHeader>
          <ModalBody className="tc-modal-body">
            <div className="row">
              <div className="col-md-12">
                <div className="form">
                { this.props.summary && <div>
                  <h4>{strings.SUMMARY_WITHDRAWAL}</h4>
                  <div className="">
                    <label className="control-label col-md-4">{strings.WITHDRAWAL_ADDRESS}: </label>
                    <label className="control-label col-md-8">{ this.props.summary.address }</label>
                  </div>
                  <div className="">
                    <label className="control-label col-4">{strings.TOTAL_AMOUNT}: </label>
                    <label className="control-label col-6">{ cryptoFormatterByCoin(this.props.summary.totalAmount, this.props.summary.currency) } { this.props.summary.currency }</label>
                  </div>
                  <div className="">
                    <label className="control-label col-4">{strings.FEE}: </label>
                    <label className="control-label col-6">{ cryptoFormatterByCoin(this.props.summary.fee, this.props.summary.currency) } { this.props.summary.currency }</label>
                  </div>
                  <div className="" style={{ paddingTop: "5px", backgroundColor: "#dcdcdc78" }}>
                    <label className="control-label col-4">{strings.YOU_WILL_GET}: </label>
                    <label className="control-label col-6">{ cryptoFormatterByCoin(NP.minus(this.props.summary.totalAmount, this.props.summary.fee), this.props.summary.currency) } { this.props.summary.currency }</label>
                  </div>
                </div>}
                { this.props.summaryFiat && <div>
                  <h4>{strings.SUMMARY_WITHDRAWAL}</h4>
                  <div className="">
                    <label className="control-label col-4">{strings.BANK}: </label>
                    <label className="control-label col-6">{ this.props.summaryFiat.bank }</label>
                  </div>
                  <div className="">
                    <label className="control-label col-4">{strings.ACCOUNT_NUMBER}: </label>
                    <label className="control-label col-6">{ this.props.summaryFiat.accountNumber }</label>
                  </div>
                  <div className="">
                    <label className="control-label col-4">{strings.TOTAL_AMOUNT}: </label>
                    <label className="control-label col-6">Rp{ amountFormatter(this.props.summaryFiat.totalAmount) }</label>
                  </div>
                  <div className="">
                    <label className="control-label col-4">{strings.FEE}: </label>
                    <label className="control-label col-6">Rp{ amountFormatter(this.props.summaryFiat.fee) }</label>
                  </div>
                  <div className="" style={{ paddingTop: "5px", backgroundColor: "#dcdcdc78" }}>
                    <label className="control-label col-4">{strings.YOU_WILL_GET}: </label>
                    <label className="control-label col-6">Rp{ amountFormatter(this.props.summaryFiat.totalAmount - this.props.summaryFiat.fee) }</label>
                  </div>
                </div>}
                  <br />
                  <div className="form-group">
                    {
                      this.props.method === 'email' && this.props.transactionComplete === false &&
                      <p> {strings.YOU_WILL_EMAIL_VERIFICATION_TOKEN}.</p>
                    }
                    {
                      this.props.method === 'ga' && this.props.transactionComplete === false &&
                      <p> {strings.ENTER_VERIFICATION_GOOGLE_AUTHENTICATOR_APP}.</p>
                    }
                  </div>
                  <br />
                  { this.props.transactionComplete === false && <div className="form-group">
                        <input type="text" className={classnames({ "form-control": true, "is-invalid": this.state.invalidToken })} value={this.state.token} onChange={(e) => this.setState({ token: e.target.value })} placeholder={strings.YOUR_VERIFICATION_TOKEN} />
                    </div> }
                  <div style={{ fontWeight: 500, fontSize: "11px", lineHeight: 1.9 }}>
                      Note: {strings.WITHDRAWALS_CAN_TAKE_UP_TO}
                  </div>
                  { this.props.transactionComplete === false && !this.state.loadingResend && !this.state.OTPSent && this.props.method === 'email' && <a className="float-right is-link" onClick={() => this.requestVerification(true)}>
                      {strings.RESEND_OTP}
                    </a> }
                  { this.props.transactionComplete === false && this.state.loadingResend && this.props.method === 'email' && <a className="float-right is-link" href="#">
                      {strings.SENDING_OTP}...
                  </a>}
                  { this.props.transactionComplete === false && this.state.OTPSent && this.props.method === 'email' && <a className="float-right is-link" onClick={() => this.requestVerification(true)}>
                      {strings.OTP_SENT}. {strings.CLICK_TO_RESEND}
                  </a>}
                </div>
              </div>
            </div>
          </ModalBody>
          { this.props.transactionComplete ?
            <ModalFooter className="tc-modal-footer">
                <p style={{ width: "100%"}}>{strings.TRACK_TRANSACTION_STATUS_UNDER} <a href="#" onClick={() => this.gotoAccounts()}> {strings.ACCOUNTS} </a> {strings.TAB}</p>
            </ModalFooter> :
            <ModalFooter className="tc-modal-footer">
                {this.state.loading ? <div className="mx-auto"><Loader type="line-scale" active={true} className="text-center" /></div> :
                <button disabled={this.state.token.length === 0} className="btn-block btn btn-blue" onClick={() => this.completeVerification()}>{strings.VERIFY}</button>}
            </ModalFooter>
          }
        </Modal>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    user: state.cognito.user,
    kyc: state.kyc,
    language: state.language
  };
};

export default connect(mapStateToProps, null)(OTPModal);
