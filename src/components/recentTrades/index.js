import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import { connect } from 'react-redux';
import amount from '../../helpers/amountFormatter';
import cryptoFormatter, {cryptoFormatterByCoin} from '../../helpers/cryptoFormatter';
import history from '../../history';
import historyActions from '../../core/actions/client/histories';
import homeActions from '../../core/actions/client/home';
import NetworkError from '../../errors/networkError';
import { Loader } from '../tcLoader';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import async from 'async';
import currencyFormatter from '../../helpers/currencyFormatter';
import { ClapSpinner } from "react-spinners-kit";

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class RecentTrades extends Component {

    constructor(){
        super();
        this.state = {
            recentConversions: [],
            loadingRecents: false,
            currentTrade: "",
            mostRecentConversion: 0,
            currentCurrency: "",
            loading: true,
            currency: 'BTC'
        }
    }

    getToken() {
        if(this.props.user){
            let username = this.props.user.username
            let clientId = this.props.user.pool.clientId
            let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
            return token
        }else{
            return 0
        }
    }

    pingRequest(){
        historyActions.pingRequest(this.getToken()).then(res => {
        }).catch(err => {
            NetworkError(err)
            if(window.intervals){
                window.intervals.map((i, index) => {
                    clearInterval(i)
                    window.intervals.splice(index, 1);
                });
            }
        })
    }

    /**
     * TCDOC-018 Fetch recent trades
     * this function will get the recent trades from backend
     */
    fetchRecentTrades() {
        let _self = this;

        this.setState({ loadingRecents: true});
        homeActions.recentTrades(this.getToken(),this.state.currency).then(res => {
            var tempCurrency = this.state.currency
            let recents = [];
            var randomRecentTrades = 0;

            let dataArray = res.data.data
            
            if(dataArray.length > 25){
                dataArray.length = 25
            }

            randomRecentTrades = (Math.floor(Math.random() * 5) + 1);

            if(this.state.recentConversions.length !== 0 ){

                    if(this.state.currentCurrency == this.state.currency){
                        recents = this.state.recentConversions
                        this.setState({
                            loading:false
                        })
                    }else{
                        recents = []
                        this.setState({
                            loading: true
                        })
                    }

                    let dataset = _.orderBy(dataArray, ['CreatedAt'], ['asc'])

                    _self.asyncLoop(dataset.length, function(loopMain){

                        let value = dataset[loopMain.iteration()];
                        let add = true;
                        let tempArr = [];

                        _self.asyncLoop(_self.state.recentConversions.length, function(loop){
                            let item = _self.state.recentConversions[loop.iteration()];

                            if(item._source.ConversionId == value._source.ConversionId){
                                    add = false;
                                    loop.break();
                            }else{
                                
                                loop.next();
                            }
                        }, function(){
                            if(add){
                                let currentTradeStatus = value._source.Action;

                                if(currentTradeStatus === "Buy"){
                                    _self.setState({
                                        currentTrade: "tradesBuy"
                                    })
                                } else if (currentTradeStatus === "Sell"){
                                    _self.setState({
                                        currentTrade: "tradesSell"
                                    })
                                }

                                recents.unshift(value);

                                loopMain.next();
                            }else{
                                loopMain.next()
                            }
                        })

                    },function(){
                        if(_self.state.recentConversions.length > 50){
                            _self.state.recentConversions.slice(0, 50);
                        }
                    })

                } else {
                    if(dataArray) {
                        recents = dataArray;
                    }
                }

            setTimeout(() => {

                let recentsUnique = _.uniqBy(recents, '_source.ConversionId');
                
                recentsUnique = _.orderBy(recentsUnique, ['_source.CreatedAt'], ['desc'])

                let mostRecentConversionValue = 0
                if (!recents) {
                    mostRecentConversionValue = recents[0]._source.Action === 'Sell' ? recents[0]._source.SellRate : recents[0]._source.BuyRate;
                }
                
                this.setState({
                    recentConversions: recentsUnique,
                    loading: false,
                    loadingRecents: false,
                    mostRecentConversion: mostRecentConversionValue,
                    currentCurrency: tempCurrency
                })
            },randomRecentTrades * 1000);

        }).catch(err => {
            
            this.setState({ loadingRecents: false, loading: false });
            NetworkError(err);
            if(window.intervals){
                window.intervals.map((i, index) => {
                    clearInterval(i)
                    window.intervals.splice(index, 1);
                });
            }
        });
    }

    onGetMostRecentTrades = () => {
        this.props.getMostRecentConversion( this.state.mostRecentConversion )
    }

    asyncLoop(iterations, func, callback) {
        var index = 0;
        var done = false;
        var loop = {
            next: function() {
                if (done) {
                    return;
                }

                if (index < iterations) {
                    index++;
                    func(loop);

                } else {
                    done = true;
                    callback();
                }
            },

            iteration: function() {
                return index - 1;
            },

            break: function() {
                done = true;
                callback();
            }
        };
        loop.next();
        return loop;
    }

    /**
     * TCDOC-019
     * recentTradesAuto and pingRequestAuto are container variable to hold the intervals. they need to be cleared in componentWillunMount
     */
    componentDidMount(){
        this.fetchRecentTrades();
        // this.fetchConversions(this.props.currency);
        // let rate;
        // if(this.props.rates.length > 0 && this.props.currency){
        //     rate = _.find(this.props.rates, { currencyPair: this.props.currency+"IDR"})
        //     this.setState({
        //         buyRate: rate.customer_buy,
        //         sellRate: rate.customer_sell
        //     });
        // }
	    this.recentTradesAuto = setInterval(() => {
            this.fetchRecentTrades();
            this.onGetMostRecentTrades();
        }, 5 * 1000);

        // this.pingRequestAuto = setInterval(() => {
        //     this.pingRequest();
        // }, 10 * 1000);

	    if(window.intervals){
            window.intervals.push(this.recentTradesAuto)
            // window.intervals.push(this.pingRequestAuto)
	    } else {
            window.intervals = [this.recentTradesAuto]
	    }
    }

    componentWillMount() {
        clearInterval(this.recentTradesAuto);
        // clearInterval(this.pingRequestAuto);
	    strings.setLanguage(this.props.language);
    }

    async componentWillReceiveProps(nextProps){
        if(this.state.currency !== nextProps.currency){
            await this.setState({
                currency: nextProps.currency,
                loading: true
            })
            this.fetchRecentTrades();
        }
    }

    render(){

        return (

            <div className="col-md-12">
                <div className="m-portlet m-portlet--full-height m-portlet--fit combined-chart-recent">
                    <div className="m-portlet__head" style={{height:"4.1rem"}}>
                        <div className="m-portlet__head-caption">
                            <div className="m-portlet__head-title">
                                <h3 className="m-portlet__head-text">
                                    {strings.RECENT_TRADES}
                                </h3>
                            </div>
                        </div>
                        <i className="fa fa-refresh float-right refresh-recents" onClick={() => this.fetchRecentTrades()}></i>
                    </div>
                    <div className="m-portlet__body" style={{ overflowY: 'scroll', paddingLeft: "15px", paddingRight: "15px"}}>
                        <div className="m-widget4 m-widget4--chart-bottom">
                            <table className="table table-recents" style={{ marginTop: "-30px"}} >
                                <thead style={{background:"#F7F7F7"}}>
                                    <tr>
                                        <th>{strings.TIME}</th>
                                        <th>{strings.RATE}</th>
                                        <th>{strings.QUANTITY}</th>
                                    </tr>
                                </thead>
                                {this.state.loading == true ? 
                                    <tr >
                                        <td colSpan={ 6 } >
                                            <div style={{ flex: 1 , display: 'flex', paddingTop: 10, justifyContent: 'center' }}>
                                                <ClapSpinner
                                                    size={30}
                                                    color="#686769"
                                                    loading={ this.state.loading }
                                                />
                                            </div>
                                        </td>
                                    </tr>:
                                    <ReactCSSTransitionGroup component={'tbody'} transitionName={this.state.currentTrade} transitionEnterTimeout={2000} transitionLeaveTimeout={2000}>
                                    {
                                        this.state.recentConversions.map(r => {
                                            if(r){
                                                return (
                                                    <tr key={r._source.ConversionId}>
                                                        <td>{ moment(r._source.CreatedAt).local().format('h:mm:ss a') }</td>
                                                        <td>
                                                            { r._source.Action === 'Sell'?
                                                                <span>
                                                                    {currencyFormatter( amount(r._source.SellRate, this.state.currency) )}
                                                                </span>:
                                                                <span>
                                                                    {currencyFormatter( amount(r._source.BuyRate, this.state.currency) )}
                                                                </span>}
                                                        </td>
                                                        <td>
                                                            { r._source.SellCurrency !== 'IDR'?
                                                                <span style={{ color: "#FBAF5D"}}>
                                                                    {cryptoFormatterByCoin(r._source.TotalSellAmount, this.state.currentCurrency)} { r._source.SellCurrency }
                                                                </span>:
                                                                <span style={{ color: "#1AA64A"}}>
                                                                    {cryptoFormatterByCoin(r._source.TotalBuyAmount, this.state.currentCurrency)} { r._source.BuyCurrency }
                                                                </span>}
                                                        </td>
                                                    </tr>
                                                )
                                            }
                                        })
                                    }
                                    </ReactCSSTransitionGroup>
                                }
                            </table>
                            {
                                this.state.recentConversions.length === 0 && this.state.loadingRecents === false ?
                                    <p style={{ textAlign: 'center'}}>No recent conversions</p> : null
                            }

                        </div>
                    </div>
                </div>
            </div>

        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
	    language: state.language
    };
};

export default connect(mapStateToProps, null)(RecentTrades);
