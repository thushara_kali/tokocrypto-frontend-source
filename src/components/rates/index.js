import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import { connect } from 'react-redux';
import amount from '../../helpers/amountFormatter';
import cryptoFormatter from '../../helpers/cryptoFormatter';
import history from '../../history';
import historyActions from '../../core/actions/client/histories';
import homeActions from '../../core/actions/client/home';
import HistoricalChart from '../charts';
import NetworkError from '../../errors/networkError';
import { Loader } from '../tcLoader';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import async from 'async';
import currencyFormatter from '../../helpers/currencyFormatter';
import ReactTooltip from 'react-tooltip';
let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class RatesDashboard extends Component {

    constructor(){
        super();
        this.state = {
            buyRate: 0,
            sellRate: 0,
            recentConversions: [],
            loadingRecents: false,
            currentTrade: "",
            ratesBuyStatus: "",
            ratesSellStatus: "",
            Buyprice: "",
            Sellprice: ""
        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    BuySelltimer(){

        var randomTimeBuy = 0;
        var randomTimeSell = 0;

        randomTimeBuy = (Math.floor(Math.random() * 10) + 1);

        setTimeout(() => {
            this.setState({
                buyRate: this.state.Buyprice
            });
        },randomTimeBuy * 1000);

        randomTimeSell = (Math.floor(Math.random() * 10) + 1);

        setTimeout(() => {
            this.setState({
                sellRate: this.state.Sellprice
            });
        },randomTimeSell * 1000);
    }

    componentWillReceiveProps(props){

        let price;
        if(props.rates.length !== 0){
            price = _.find(props.rates, { currencyPair: props.currency.toUpperCase()+"IDR"})
            this.setState({
                Buyprice:price.customer_buy,
                Sellprice:price.customer_sell
            })
        }

        if(this.state.buyRate == 0){
            this.setState({
                buyRate: price.customer_buy,
                sellRate: price.customer_sell
            });
        }

        if(this.state.buyRate < price.customer_buy){
            this.setState({
                ratesBuyStatus: "flashingRatesUp"
            })
        } else if (this.state.buyRate > price.customer_buy){
            this.setState({
                ratesBuyStatus: "flashingRatesDown"
            })
        }

        if (this.state.sellRate < price.customer_sell){
            this.setState({
                ratesSellStatus: "flashingRatesUp"
            })
        } else if (this.state.sellRate > price.customer_sell){
            this.setState({
                ratesSellStatus: "flashingRatesDown"
            })
        }

    }

    componentDidMount(){
        this.BuySelltimer();
        // this.fetchConversions(this.props.currency);
        // let rate;
        // if(this.props.rates.length > 0 && this.props.currency){
        //     rate = _.find(this.props.rates, { currencyPair: this.props.currency+"IDR"})
        //     this.setState({
        //         buyRate: rate.customer_buy,
        //         sellRate: rate.customer_sell
        //     });
        // }

        this.BuySelltimerAuto = setInterval(() => {
            this.BuySelltimer();
        }, 10 * 1000);

        // this.pingRequestAuto = setInterval(() => {
        //     this.pingRequest();
        // }, 10 * 1000);

	    if(window.intervals){
            window.intervals.push(this.BuySelltimerAuto)
	    } else {
            window.intervals = [this.BuySelltimerAuto]
	    }
    }

    componentWillMount() {
        clearInterval(this.BuySelltimerAuto);
        // clearInterval(this.pingRequestAuto);
	    strings.setLanguage(this.props.language);
    }

    // componentWillUnmount() {
    //     window.intervals.map((i, index) => {
    //         clearInterval(i)
    //         window.intervals.splice(index, 1);
    //     });
    // }

    render(){

        // let buy_price = 0;
        // let sell_price = 0;

        // let currentSaveSatate = '';

        // if(this.props.rates.length !== 0){
        //     var price = _.find(this.props.rates, { currencyPair: this.props.currency.toUpperCase()+'IDR'});

        //     buy_price = price.customer_buy;
        //     sell_price = price.customer_sell;

        // }

        // 

        // 

        return (

            <div data-tip data-for="instantBuy" className="col-md-12">
                {/* <ReactTooltip id="instantBuy" place="right">
                    <span>Prices may change while your order is being processed, <br /> especially for larger orders that may move the market.</span> 
                </ReactTooltip> */}
                <div className="m-portlet dashboard-buy-sell-custom-shadow">
                    <div className="m-portlet__head" style={{height:"3.1rem"}}>
                        <div className="m-portlet__head-caption">
                            <div className="m-portlet__head-title" style={{width:"100%", textAlign:"center"}}>
                                <h3 className="m-portlet__head-text">
                                    {/* <span style={{color:"#222222"}}>Instant Buy</span> | { this.props.currency } */}
                                    <span style={{color:"#222222"}}>VIP PRIVATE TRADING</span>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div className="m-portlet__body" style={{paddingTop:"0px", paddingBottom:"0px"}}>
                        <div className="tab-content" style={{height:"100%"}}>
                            <div className="tab-pane active" id="m_widget4_tab1_content" style={{height:"100%"}}>
                                <div className="m-widget4" style={{height:"100%"}}>
                                    <div className="m-widget4__item" style={{ display: "block", paddingBottom:"0.55rem"}}>
                                        <div className="dashboard-custom-buy-sell" style={{textAlign:"center"}}>
                                            {/* <div className="row" onClick={() => history.push('/conversions?action=buy&currency='+this.props.currency)}>
                                                <div className="col-md-12">
                                                    <button disabled={this.state.buyRate === 0} style={{ fontWeight: 500, marginTop: "0px", padding: "18px", borderRadius: "8px" }} type="button" className="btn btn btn-green btn-block dashboard-trading-desk-buy-sell-buttons">
                                                        <a style={{ color: "#fff", textDecoration: "unset", fontWeight: "600" }}>{strings.BUY} @ { currencyFormatter( amount(this.state.buyRate) ) }</a>
                                                    </button>
                                                </div>
                                            </div> */}
                                            <img src="../../assets/media/img/vip-buy-sell.png" className="dashboard-custom-buy-sell-vip" alt="" />
                                        </div>
                                    </div>
                                    {/* <div className="m-widget4__item" style={{ display: "block", paddingTop: "1px"}}>
                                        <div className="dashboard-custom-buy-sell">
                                            <div className="row" onClick={() => history.push('/conversions?action=sell&currency='+this.props.currency)}>
                                                <div className="col-md-12">
                                                    <button disabled={this.state.sellRate === 0} style={{ fontWeight: 500, marginTop: "0px", padding: "18px", borderRadius: "8px" }} type="button" className="btn btn-brown btn-block dashboard-trading-desk-buy-sell-buttons">
                                                        <a style={{ color: "#fff", textDecoration: "unset", fontWeight: "600" }}>{strings.SELL} @ { currencyFormatter( amount(this.state.sellRate) ) }</a>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div> */}
                                </div>
                            </div>
                            <div className="tab-pane active" id="m_widget4_tab1_content">
                            </div>
                            <div className="tab-pane" id="m_widget4_tab2_content">
                                <div className="m-widget4">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <HistoricalChart currency={this.props.currency} {...this.props}/> */}
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
	    language: state.language
    };
};

export default connect(mapStateToProps, null)(RatesDashboard);
