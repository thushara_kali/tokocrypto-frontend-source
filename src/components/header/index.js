import React, { Component } from 'react';
import classnames from 'classnames';
import enhanceWithClickOutside from 'react-click-outside';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserAttributes, updateAttributes } from '../../core/react-cognito/attributes.js'
import actions  from '../../core/actions/client/user.js'
import homeActions  from '../../core/actions/client/home.js'
import { setLanguage } from '../../core/actions/client/language.js'
import history from '../../history';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import ReactFlagsSelect from 'react-flags-select';
import ReactFlags from '../../languages/react-flags/es/index.js';
import localStorage from 'localStorage';
import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {faEthereum, faBtc} from '@fortawesome/fontawesome-free-brands';
import {faHome,faChartBar,faCreditCard,faUser,faTasks,faKey} from '@fortawesome/fontawesome-free-solid';
import 'react-flags-select/css/react-flags-select.css';
import async from 'async';
import { NotificationManager } from 'react-notifications';
import NetworkError from '../../errors/networkError';

var awsIot = require('aws-iot-device-sdk');
var mainURL = history.location.pathname.split('/')[1];
var siteURL

if(process.env.REACT_APP_ENV === 'dev' || process.env.REACT_APP_ENV === 'local'){
    siteURL = 'https://dev.tokocrypto.com/'
}else if(process.env.REACT_APP_ENV === 'demo'){
    siteURL = 'https://demo.tokocrypto.com/'
}else{
    siteURL = 'https://tokocrypto.com/'
}

fontawesome.library.add(faHome,faChartBar,faCreditCard,faUser,faTasks)

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class AppHeader extends Component {

    constructor(){
        super();
        this.state = {
            profileOpen: false,
            notificationOpen: false,
            name: "",
            username: "",
            email: "",
            location: "/",
            openMobileMenu: false,
            selectedLang: "",
            topic: ""
        }

    }

    configureIot() {

        async.series({
            getUserid: (callback) => {
                getUserAttributes(this.props.user).then(attr => {
                    let user_id = attr.sub;
        
                    this.setState({
                        userId: user_id
                    })

                    callback(null,attr);
                })
                .catch(err => {
                    let errorDetails = {
                        type: 'COMPONENTS: AppHeader | FUNC: configureIot',
                        url: 'react-cognito'
                    }
                    NetworkError(err, errorDetails);
                    // 
                    callback(err, null);
                });
            },
            callIot: (callback) => {

                var iotEndpoint = ''
                var topic = ''
                var region = ''

                if(process.env.REACT_APP_ENV === 'demo'){
                    iotEndpoint = 'a1sm5n1r7dws4l-ats.iot.us-west-2.amazonaws.com';
                    topic = `/pydt/demo/user/${this.state.userId}/orders`;
                    region = 'us-west-2'
                }else if(process.env.REACT_APP_ENV === 'local' || process.env.REACT_APP_ENV === 'dev'){
                    iotEndpoint = 'a1sm5n1r7dws4l-ats.iot.us-east-1.amazonaws.com';
                    topic = `/pydt/dev/user/${this.state.userId}/orders`;
                    region = 'us-east-1'
                }else if(process.env.REACT_APP_ENV === 'production'){
                    iotEndpoint = 'a1sm5n1r7dws4l-ats.iot.ap-southeast-1.amazonaws.com';
                    topic = `/pydt/prod/user/${this.state.userId}/orders`;
                    region = 'ap-southeast-1'
                }
                // const env = PYDT_CONFIG.PROD ? 'prod' : 'dev';
                
                window.iotDevice = awsIot.device({
                  region: region,
                  protocol: 'wss',
                  keepalive: 600,
                  accessKeyId: 'AKIAIVWDMCSFMKW3RGMA',
                  secretKey: '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',
                  host: iotEndpoint
                });

                window.iotTopic = topic

                // this.setState({
                //     iotDevice: iotDevice,
                //     topic: topic
                // })

                // 
            
                window.iotDevice.on('connect', () => {
                    window.iotDevice.subscribe(topic);
                });

                window.iotDevice.on('error', err => {
                  callback(err, null);
                });
            
                window.iotDevice.on('message', (recTopic, message) => {
                 
                    if (recTopic === topic) {
        
                    var string = new TextDecoder("utf-8").decode(message);

                    let msg = JSON.parse(string)
                    
                    NotificationManager.success(msg.msg, msg.title, 8000);
        
                    }
                });

                callback(null);
            }
        }, (err) => {
            
        });
    }

    getToken() {
        if(this.props.user){
            let username = this.props.user.username
            let clientId = this.props.user.pool.clientId
            let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
            return token
        }else{
            return 0
        }
    }

    onSetLanguage(){

        if(history.location.pathname.length > 0){

            let localLang = localStorage.getItem('language')

            if(localLang != mainURL){
                setTimeout(() => {
                    window.location.reload();
                },100)
            }

            strings.setLanguage(this.props.language);
            if(mainURL){
                if(mainURL == "en"){
                    this.setState({
                        selectedLang: "US"
                    })
                    localStorage.setItem('language', mainURL);
                } else if (mainURL == "id"){
                    this.setState({
                        selectedLang: "ID"
                    })
                    localStorage.setItem('language', mainURL);
                } else if (mainURL == "sunda"){
                    this.setState({
                        selectedLang: "SUNDA"
                    })
                    localStorage.setItem('language', mainURL);
                } else if (mainURL == "java"){
                    this.setState({
                        selectedLang: "JAVA"
                    })
                    localStorage.setItem('language', mainURL)
                }
            }
        }else{
            strings.setLanguage(this.props.language);
            if(this.props.language){
                if(this.props.language == "en"){
                    this.setState({
                        selectedLang: "US"
                    })
                } else if (this.props.language == "id"){
                    this.setState({
                        selectedLang: "ID"
                    })
                } else if (this.props.language == "sunda"){
                    this.setState({
                        selectedLang: "SUNDA"
                    })
                } else if (this.props.language == "java"){
                    this.setState({
                        selectedLang: "JAVA"
                    })
                }
            }
        }
    }

    fetchUserPermission(){
        let token = this.getToken();
        actions.fetchUser(token)
        .then(res => {
            let data = res.data.data;
            // 
            this.setState({ fetchingKYC: false});
            // 
            // 
            this.props.actions.setTradingStatus(data.trading);
            this.props.actions.setKYCStatus(data.kyc);
            if(history.location.pathname !== '/profile') {
                if(data.trading == "rejected"){
                    if(data.kyc == "pending" || data.kyc == "draft"){
                        // history.replace('/verifyaccount');
                    }else{
                            if(data.kyc === "approved") {
                                history.replace('/trading-unavailable');
                            } else {
                                // history.replace('/profile');
                            }
                    }
                } else {
                    if(data.kyc == "pending"){
                        // history.replace('/waiting-confirmation');
                    }
                }
            }
        })
        .catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: AppHeader | FUNC: fetchUserPermission',
                url: '/accessibility'
            }
            // NetworkError(err, errorDetails);
            this.setState({ fetchingKYC: false});
            // history.replace('/login');
            // 
        })
    }

    handleClickOutside() {
        this.setState({
            profileOpen: false,
            notificationOpen: false
        })
    }

    fetchUserProfile() {

        
        // this one for login with google
        if(this.props.user){
            this.fetchUserPermission();
            if (!this.props.user.username.includes('@')) {
                this.setState({
                    name: localStorage.getItem('CACHE_NAME'),
                    username: localStorage.getItem('CACHE_EMAIL')
                })
            }else{
                getUserAttributes(this.props.user).then(attr => {
                    this.setState({
                        name: attr.name,
                        email: attr.email,
                        username: attr.email
                    })
                })
                .catch(err => {
                    let errorDetails = {
                        type: 'COMPONENTS: AppHeader | FUNC: configureIot',
                        url: 'react-cognito'
                    }
                    // NetworkError(err, errorDetails);
                    
                })
            }
        }

        // if not using google will run this function
    }

    componentDidMount() {
        if(this.props.user){
            this.configureIot();
            this.setState({ fetchingKYC: true});
        }
        this.setState({ fetchingKYC: false});
        this.fetchUserProfile()
        this.setState({
            location: history.location.pathname
        });
        // this.getUserDetails();
        // 
    }

    componentDidUpdate(){
        // 
        // 

        setTimeout(() => {
            // 

            if(this.state.location !== history.location.pathname){
                this.setState({
                    location: history.location.pathname
                })
            }
        }, 50)
    }

    navigate(route){
        history.push('/' + mainURL + route);
        this.fetchUserProfile();

        this.setState({
            location: route,
            profileOpen: false,
            notificationOpen: false,
            openMobileMenu: false
        });
    }

    navigateLogo(){
        if(process.env.REACT_APP_ENV == 'production'){
            window.location.href = 'https://tokocrypto.com/' + mainURL + '/index.html'
        } else if (process.env.REACT_APP_ENV == 'demo'){
            window.location.href = 'https://demo.tokocrypto.com/' + mainURL + '/index.html'
        } else {
            window.location.href = 'https://dev.tokocrypto.com/' + mainURL + '/index.html'
        }
    }

    onLogout = (event) => {
        // this.state.iotDevice.unsubscribe(this.state.topic);
        // 
        event.preventDefault();
        if (this.props.user != null) {
            this.props.user.signOut();
            if(window.Raven){
                window.Raven.setUserContext();
            }
            // history.replace('/');
            window.location.href = siteURL + mainURL + '/index.html'
        }
    }

	// onChangeLanguage(e) {
    // 	
    // 	this.props.actions.setLanguage(e.target.value);
	// 	localStorage.setItem('language', e.target.value);

    // 	setTimeout(() => {
	// 	    
	// 	    this.onSetLanguage();
	// 	    window.location.reload();
	//     },100)
    // }

    onChangeLanguage(e) {
        

        let countryCode
        if(e == "ID"){
            countryCode = "id"
        } else if (e == "US"){
            countryCode = "en"
        } else if (e == "SUNDA"){
            countryCode = "sunda"
        } else if (e == "JAVA"){
            countryCode = "java"
        }
        localStorage.setItem('language', countryCode);
        
        let currentPath = history.location.pathname.split('/')[2];

    	setTimeout(() => {
            history.push('/' + countryCode + '/' + currentPath)
            window.location.reload();
	    },100)
	}

    componentWillMount() {
        this.onSetLanguage();
    }

    render() {

        let location = this.state.location;
        // 
        // 

        return (

            <div className="m-grid m-grid--hor m-grid--root m-page">

                <header className="m-grid__item	m-header " data-minimize="minimize" data-minimize-offset="200"
                        data-minimize-mobile-offset="200">
                    <div className="m-header__top">
                        <div
                            className="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
                            <div className="m-stack m-stack--ver m-stack--desktop">

                                <div className="m-stack__item m-brand">
                                    <div className="m-stack m-stack--ver m-stack--general m-stack--inline">
                                        <div className="m-stack__item m-stack__item--middle m-brand__logo">
                                            <a onClick={() => this.navigateLogo()} className="m-brand__logo-wrapper">
                                                <img style={{width:"180px"}} alt="" src="/assets/media/img/logo/TC_new_logo.png"/>
                                            </a>
                                        </div>
                                        <div className="m-stack__item m-stack__item--middle m-brand__tools">

                                            <a id="m_aside_header_menu_mobile_toggle" onClick={() => this.setState({ openMobileMenu: true })}
                                            className="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                                <span></span>
                                            </a>

                                            {/*<a id="m_aside_header_topbar_mobile_toggle" onClick={() => this.setState({ openMobileMenu: true })}*/}
                                            {/*className="m-brand__icon m--visible-tablet-and-mobile-inline-block">*/}
                                                {/*<i className="flaticon-more"></i>*/}
                                            {/*</a>*/}

                                        </div>
                                    </div>

                                </div>
                                
                                <div className={classnames({"m-stack__item m-stack__item--middle m-stack__item--fluid": true,"user-disable user-disable-opacity": this.props.user == null})}>
                                    <button
                                        onClick={() => this.setState({ openMobileMenu: false })}
                                        className={classnames({
                                            "m-aside-header-menu-mobile-close--skin-light": true,
                                            "m-aside-header-menu-mobile-close": !this.state.openMobileMenu,
                                            "m-aside-header-menu-mobile-open": this.state.openMobileMenu
                                        })}
                                        id="m_aside_header_menu_mobile_close_btn">
                                        <i className="fa fa-times"></i>
                                    </button>
                                    <div id="m_header_menu"
                                        className={classnames({
                                            "m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light": true,
                                            "m-aside-header-menu-mobile--on": this.state.openMobileMenu
                                        })}>
                                        { this.state.fetchingKYC === false && <ul className="m-menu__nav  m-menu__nav--submenu-arrow ">
                                            <li className={classnames({ 'm-menu__item': true, 'm-menu__item--active': location === '/' + mainURL + '/dashboard'})} aria-haspopup="true">
                                                <a onClick={() => this.navigate('/dashboard')} className="m-menu__link ">
                                                    <span className="m-menu__item-here"></span>
                                                    <span className="m-menu__link-text">
                                                    {location === '/'?
                                                        <FontAwesomeIcon style={{ color: "#007EA5" }}  icon={faHome} className="fas fa-home header-menu-chat-icon"/>:
                                                        <FontAwesomeIcon style={{ color: "#a7a9b3" }}  icon={faHome} className="fas fa-home header-menu-chat-icon"/>
                                                    }
                                                    &nbsp; {strings.DASHBOARD}
                                                </span>
                                                </a>
                                            </li>
                                            {/* <li className={classnames({ 'm-menu__item': true, 'm-menu__item--active': location === '/conversions'})}
                                                aria-haspopup="true">
                                                <a onClick={() => this.navigate('/conversions')} className="m-menu__link m-menu__toggle">
                                                    <span className="m-menu__item-here"></span>
                                                    <span className="m-menu__link-text">
                                                    {location === '/conversions'?
                                                        <FontAwesomeIcon style={{ color: "#007EA5" }} icon={faChartBar} className="fas fa-chart-bar header-menu-chat-icon"/>:
                                                        <FontAwesomeIcon style={{ color: "#a7a9b3" }} icon={faChartBar} className="fas fa-chart-bar header-menu-chat-icon"/>
                                                    }
                                                    &nbsp; {strings.BUY_SELL}
                                                </span>
                                                </a>

                                            </li> */}
                                            <li className={classnames({ 'm-menu__item': true, 'm-menu__item--active': location === '/' + mainURL + '/accounts'})}
                                                aria-haspopup="true">
                                                <a onClick={() => this.navigate('/accounts')} className="m-menu__link m-menu__toggle">
                                                    <span className="m-menu__item-here"></span>
                                                    <span className="m-menu__link-text">
                                                    {location === '/accounts'?
                                                        <FontAwesomeIcon style={{ color: "#007EA5" }} icon={faCreditCard} className="fas fa-credit-card header-menu-chat-icon"/>:
                                                        <FontAwesomeIcon style={{ color: "#a7a9b3" }} icon={faCreditCard} className="fas fa-credit-card header-menu-chat-icon"/>
                                                    }
                                                    &nbsp; {strings.ACCOUNTS}
                                                </span>
                                                </a>
                                            </li>
                                            <li className={classnames({ 'm-menu__item': true, 'm-menu__item--active': location === '/' + mainURL + '/profile'})}
                                                aria-haspopup="true">
                                                <a onClick={() => this.navigate('/profile')} className="m-menu__link m-menu__toggle">
                                                    <span className="m-menu__item-here"></span>
                                                    <span className="m-menu__link-text">
                                                    {location === '/profile'?
                                                        <FontAwesomeIcon style={{ color: "#007EA5" }} icon={faUser} className="fas fa-user header-menu-chat-icon"/>:
                                                        <FontAwesomeIcon style={{ color: "#a7a9b3" }} icon={faUser} className="fas fa-user header-menu-chat-icon"/>
                                                    }
                                                    &nbsp; {strings.USER_PROFILE}
                                                </span>
                                                </a>
                                            </li>
                                            <li className={classnames({ 'm-menu__item': true, 'm-menu__item--active': location === '/' + mainURL + '/launchpad'})}
                                                aria-haspopup="true">
                                                <a onClick={() => this.navigate('/launchpad')} className="m-menu__link m-menu__toggle">
                                                    <span className="m-menu__item-here"></span>
                                                    <span className="m-menu__link-text">
                                                    {location === '/launchpad'?
                                                        <FontAwesomeIcon style={{ color: "#007EA5" }} icon={faTasks} className="fas fa-tasks header-menu-chat-icon"/>:
                                                        <FontAwesomeIcon style={{ color: "#a7a9b3" }} icon={faTasks} className="fas fa-tasks header-menu-chat-icon"/>
                                                    }
                                                    &nbsp; {strings.LAUNCHPAD}
                                                </span>
                                                </a>
                                            </li>
                                            { window.outerWidth < 768 && <li className={classnames({ 'm-menu__item': true, 'm-menu__item--active': location === '/' + mainURL + '/api-keys'})}
                                                aria-haspopup="true">
                                                <a onClick={() => this.navigate('/api-keys')} className="m-menu__link m-menu__toggle">
                                                    <span className="m-menu__item-here"></span>
                                                    <span className="m-menu__link-text">
                                                    {location === '/api-keys'?
                                                        <FontAwesomeIcon style={{ fontSize: "1.07rem", textAlign: "center", color: "#007EA5", fontWeight: "500", verticalAlign: "baseline" }} icon={faKey} className="fas fa-keys"/>:
                                                        <FontAwesomeIcon style={{ fontSize: "1.07rem", textAlign: "center", color: "#a7a9b3", fontWeight: "500", verticalAlign: "baseline" }} icon={faKey} className="fas fa-keys"/>
                                                    }
                                                    &nbsp; {strings.API_KEYS}
                                                </span>
                                                </a>
                                            </li> }
                                            { window.outerWidth < 768 && <li className={classnames({ 'm-menu__item': true, 'm-menu__item--active': location === '/' + mainURL + '/profile'})}
                                                aria-haspopup="true">
                                                <a onClick={(e) => this.onLogout(e)} className="m-menu__link m-menu__toggle">
                                                    <span className="m-menu__item-here"></span>
                                                    <span className="m-menu__link-text">
                                                    {strings.LOGOUT}
                                                </span>
                                                </a>
                                            </li> }
                                            { window.outerWidth < 768 && <li className="m-nav__item" style={{display:"inline-flex",marginTop:"-2px", padding:"9px 30px"}}>

                                                <span className="m-nav__link-title">
                                                    <span className="m-nav__link-wrap">
                                                        <span className="m-nav__link-text">
                                                            {strings.LANGUAGE} :
                                                        </span>
                                                    </span>
                                                </span>

                                                <ReactFlags
                                                    defaultCountry={this.state.selectedLang}
                                                    countries={["US", "ID", "SUNDA", "JAVA"]}
                                                    customLabels={{"US": "English","ID": "Indonesian", "SUNDA":"Sundanese", "JAVA":"Javanese"}}
                                                    onSelect={this.onChangeLanguage}
                                                />
                                            </li> }
                                        </ul> }
                                    </div>
                                </div>
                                
                                <div className="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                                    <div id="m_header_topbar" className="m-topbar  m-stack m-stack--ver m-stack--general">
                                        <div className="m-stack__item m-topbar__nav-wrapper">
                                            <ul className="m-topbar__nav m-nav m-nav--inline">

                                                <li className={classnames({
                                                    "m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light": true,
                                                    "m-dropdown--open": this.state.profileOpen
                                                })}
                                                    >
                                                    <a href="#" className="m-nav__link m-dropdown__toggle" onClick={() => this.setState({ profileOpen: !this.state.profileOpen, notificationOpen: false })}>
                                                    <span className="m-topbar__userpic">
                                                        {/* <img src="assets/app/media/img/users/user4.jpg"
                                                            className="m--img-rounded m--img-centered" alt=""/> */}
                                                    </span>
                                                        <span className="m-topbar__welcome">
                                                        {strings.HELLO},&nbsp;
                                                        
                                                    </span>
                                                        <span className="m-topbar__username">
                                                        { this.state.name }
                                                        {!this.props.user?
                                                            <span><a style={{color:"#007ea4"}} onClick={() => this.navigate('/login')}>Please Login or Register</a></span>
                                                        :null}
                                                    </span>
                                                    </a>
                                                    <div className="m-dropdown__wrapper">
                                                        <span
                                                            className="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                        <div className="m-dropdown__inner">
                                                            {this.props.user?
                                                                 <div className="m-dropdown__header m--align-center"
                                                                    style={{ background: "url(assets/app/media/img/misc/user_profile_bg.jpg)", backgroundSize: "cover"}}>
                                                                    <div className="m-card-user m-card-user--skin-dark">
                                                                        <div className="m-card-user__pic">
                                                                            {/* <img src="assets/app/media/img/users/user4.jpg"
                                                                                className="m--img-rounded m--marginless"
                                                                                alt=""/> */}
                                                                        </div>
                                                                        <div className="m-card-user__details">
                                                                        <span className="m-card-user__name m--font-weight-500">
                                                                            { this.state.name }
                                                                        </span>
                                                                            <a href=""
                                                                            className="m-card-user__email m--font-weight-300 m-link">
                                                                                { this.state.username }
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            :null}
                                                           
                                                            <div className="m-dropdown__body">
                                                                <div className="m-dropdown__content">
                                                                    <ul className="m-nav m-nav--skin-light">
                                                                        <li className="m-nav__section m--hide">
                                                                        <span className="m-nav__section-text">
                                                                            {strings.SECTION}
                                                                        </span>
                                                                        </li>
                                                                        {this.props.user?
                                                                            <li className="m-nav__item" style={{display:"inline-flex"}}>
                                                                                <a onClick={() => this.navigate('/profile')} className="m-nav__link">
                                                                                    <i className="m-nav__link-icon flaticon-profile-1"></i>
                                                                                    <span className="m-nav__link-title">
                                                                                        <span className="m-nav__link-wrap">
                                                                                            <span className="m-nav__link-text">
                                                                                                {strings.MY_PROFILE}
                                                                                            </span>
                                                                                        </span>
                                                                                    </span>
                                                                                </a>
                                                                                {/* <select className="form-control m-input">
                                                                                    <option>Indonesian</option>
                                                                                    <option>English</option>
                                                                                </select> */}
                                                                            </li>
                                                                        :null}
                                                                        {this.props.user?
                                                                            <li className="m-nav__item" style={{display:"inline-flex"}}>
                                                                                <a onClick={() => this.navigate('/notifications')} className="m-nav__link">
                                                                                    <i className="m-nav__link-icon fa fa-envelope"></i>
                                                                                    <span className="m-nav__link-title">
                                                                                        <span className="m-nav__link-wrap">
                                                                                            <span className="m-nav__link-text">
                                                                                                {strings.NOTIFICATIONS}
                                                                                            </span>
                                                                                        </span>
                                                                                    </span>
                                                                                </a>
                                                                            </li>
                                                                        :null}
	                                                                   
                                                                        {this.props.user?
                                                                            <li className="m-nav__item" style={{display:"inline-flex"}}>
                                                                                <a onClick={() => this.navigate('/api-keys')} className="m-nav__link">
                                                                                    <i className="m-nav__link-icon fa fa-key"></i>
                                                                                    <span className="m-nav__link-title">
                                                                                        <span className="m-nav__link-wrap">
                                                                                            <span className="m-nav__link-text">
                                                                                                {strings.API_KEYS}
                                                                                            </span>
                                                                                        </span>
                                                                                    </span>
                                                                                </a>
                                                                            </li>
                                                                        :null}

                                                                        <li className="m-nav__item" style={{display:"inline-flex",marginTop:"12px"}}>

                                                                            <span className="m-nav__link-title">
                                                                                <i className="m-nav__link-icon flaticon-pin" style={{paddingRight:"15px",color:"#c1bfd0"}}></i>
                                                                                <span className="m-nav__link-wrap">
                                                                                    <span className="m-nav__link-text">
                                                                                        {strings.LANGUAGE} :
                                                                                    </span>
                                                                                </span>
                                                                            </span>
                                                                            <ReactFlags
                                                                                defaultCountry={this.state.selectedLang}
                                                                                countries={["US", "ID", "SUNDA", "JAVA"]}
                                                                                customLabels={{"US": "English","ID": "Indonesian", "SUNDA":"Sundanese", "JAVA":"Javanese"}}
                                                                                onSelect={this.onChangeLanguage}
                                                                            />
                                                                            {/* <select value={this.props.language} className="form-control m-input" onChange={(e) => this.onChangeLanguage(e)}>
                                                                                <option value="id">Indonesian</option>
                                                                                <option value="en">English</option>
                                                                            </select> */}
                                                                        </li>

                                                                        <li className="m-nav__item" style={{display:"inline-flex"}}>
                                                                            <a href="https://support.tokocrypto.com" target="_blank" className="m-nav__link">
                                                                                <i className="m-nav__link-icon fa fa-handshake-o"></i>
                                                                                <span className="m-nav__link-title">
                                                                                    <span className="m-nav__link-wrap">
                                                                                        <span className="m-nav__link-text">
                                                                                            {strings.SUPPORT}
                                                                                        </span>
                                                                                    </span>
                                                                                </span>
                                                                            </a>
                                                                        </li>

                                                                        <li className="m-nav__separator m-nav__separator--fit"></li>
                                                                        {this.props.user?
                                                                            <li className="m-nav__item" onClick={(e) => this.onLogout(e)}>
                                                                                <a href="#"
                                                                                className="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                                                                    {strings.LOGOUT}
                                                                                </a>
                                                                            </li>
                                                                        :null}
                                                                      
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                {/* <li className={
                                                    classnames({
                                                        "m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width": true,
                                                        "m-dropdown--open": this.state.notificationOpen
                                                    })
                                                }
                                                    data-dropdown-toggle="click" data-dropdown-persistent="true">
                                                    <a href="#" className="m-nav__link m-dropdown__toggle"
                                                    id="m_topbar_notification_icon"
                                                    onClick={() => this.setState({ notificationOpen: !this.state.notificationOpen, profileOpen: false })}
                                                    >
                                                        <span
                                                            className="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>
                                                        <span className="m-nav__link-icon">
                                                        <span className="m-nav__link-icon-wrapper">
                                                            <i className="flaticon-music-2"></i>
                                                        </span>
                                                    </span>
                                                    </a>
                                                    <div className="m-dropdown__wrapper">
                                                        <span
                                                            className="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                                        <div className="m-dropdown__inner">
                                                            <div className="m-dropdown__header m--align-center"
                                                                style={{ background: "url(assets/app/media/img/misc/notification_bg.jpg)", backgroundSize: "cover" }}>
                                                            <span className="m-dropdown__header-title">
                                                                9 New
                                                            </span>
                                                                <span className="m-dropdown__header-subtitle">
                                                                User Notifications
                                                            </span>
                                                            </div>
                                                            <div className="m-dropdown__body">
                                                                <div className="m-dropdown__content">
                                                                    <ul className="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand"
                                                                        role="tablist">
                                                                        <li className="nav-item m-tabs__item">
                                                                            <a className="nav-link m-tabs__link active"
                                                                            data-toggle="tab"
                                                                            href="#topbar_notifications_notifications"
                                                                            role="tab">
                                                                                Alerts
                                                                            </a>
                                                                        </li>
                                                                        <li className="nav-item m-tabs__item">
                                                                            <a className="nav-link m-tabs__link"
                                                                            data-toggle="tab"
                                                                            href="#topbar_notifications_events"
                                                                            role="tab">
                                                                                Events
                                                                            </a>
                                                                        </li>
                                                                        <li className="nav-item m-tabs__item">
                                                                            <a className="nav-link m-tabs__link"
                                                                            data-toggle="tab"
                                                                            href="#topbar_notifications_logs" role="tab">
                                                                                Logs
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                    <div className="tab-content">
                                                                        <div className="tab-pane active"
                                                                            id="topbar_notifications_notifications"
                                                                            role="tabpanel">
                                                                            <div className="m-scrollable"
                                                                                data-scrollable="true"
                                                                                data-max-height="250"
                                                                                data-mobile-max-height="200">
                                                                                <div
                                                                                    className="m-list-timeline m-list-timeline--skin-light">
                                                                                    <div className="m-list-timeline__items">
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
                                                                                            <span
                                                                                                className="m-list-timeline__text">
                                                                                            12 new users registered
                                                                                        </span>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            Just now
                                                                                        </span>
                                                                                        </div>
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge"></span>
                                                                                            <span
                                                                                                className="m-list-timeline__text">
                                                                                            System shutdown
                                                                                            <span
                                                                                                className="m-badge m-badge--success m-badge--wide">
                                                                                                pending
                                                                                            </span>
                                                                                        </span>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            14 mins
                                                                                        </span>
                                                                                        </div>
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge"></span>
                                                                                            <span
                                                                                                className="m-list-timeline__text">
                                                                                            New invoice received
                                                                                        </span>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            20 mins
                                                                                        </span>
                                                                                        </div>
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge"></span>
                                                                                            <span
                                                                                                className="m-list-timeline__text">
                                                                                            DB overloaded 80%
                                                                                            <span
                                                                                                className="m-badge m-badge--info m-badge--wide">
                                                                                                settled
                                                                                            </span>
                                                                                        </span>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            1 hr
                                                                                        </span>
                                                                                        </div>
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge"></span>
                                                                                            <span
                                                                                                className="m-list-timeline__text">
                                                                                            System error -
                                                                                            <a href="#" className="m-link">
                                                                                                Check
                                                                                            </a>
                                                                                        </span>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            2 hrs
                                                                                        </span>
                                                                                        </div>
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge"></span>
                                                                                            <span
                                                                                                className="m-list-timeline__text">
                                                                                            Production server down
                                                                                        </span>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            3 hrs
                                                                                        </span>
                                                                                        </div>
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge"></span>
                                                                                            <span
                                                                                                className="m-list-timeline__text">
                                                                                            Production server up
                                                                                        </span>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            5 hrs
                                                                                        </span>
                                                                                        </div>
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge"></span>
                                                                                            <span href=""
                                                                                                className="m-list-timeline__text">
                                                                                            New order received
                                                                                            <span
                                                                                                className="m-badge m-badge--danger m-badge--wide">
                                                                                                urgent
                                                                                            </span>
                                                                                        </span>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            7 hrs
                                                                                        </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="tab-pane"
                                                                            id="topbar_notifications_events"
                                                                            role="tabpanel">
                                                                            <div className="m-scrollable"
                                                                                data-max-height="250"
                                                                                data-mobile-max-height="200">
                                                                                <div
                                                                                    className="m-list-timeline m-list-timeline--skin-light">
                                                                                    <div className="m-list-timeline__items">
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                                                                            <a href=""
                                                                                            className="m-list-timeline__text">
                                                                                                New order received
                                                                                            </a>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            Just now
                                                                                        </span>
                                                                                        </div>
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge m-list-timeline__badge--state1-danger"></span>
                                                                                            <a href=""
                                                                                            className="m-list-timeline__text">
                                                                                                New invoice received
                                                                                            </a>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            20 mins
                                                                                        </span>
                                                                                        </div>
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                                                                            <a href=""
                                                                                            className="m-list-timeline__text">
                                                                                                Production server up
                                                                                            </a>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            5 hrs
                                                                                        </span>
                                                                                        </div>
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                                            <a href=""
                                                                                            className="m-list-timeline__text">
                                                                                                New order received
                                                                                            </a>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            7 hrs
                                                                                        </span>
                                                                                        </div>
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                                            <a href=""
                                                                                            className="m-list-timeline__text">
                                                                                                System shutdown
                                                                                            </a>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            11 mins
                                                                                        </span>
                                                                                        </div>
                                                                                        <div
                                                                                            className="m-list-timeline__item">
                                                                                            <span
                                                                                                className="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                                            <a href=""
                                                                                            className="m-list-timeline__text">
                                                                                                Production server down
                                                                                            </a>
                                                                                            <span
                                                                                                className="m-list-timeline__time">
                                                                                            3 hrs
                                                                                        </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="tab-pane"
                                                                            id="topbar_notifications_logs"
                                                                            role="tabpanel">
                                                                            <div
                                                                                className="m-stack m-stack--ver m-stack--general"
                                                                                style={{ minHeight: "180px"}}>
                                                                                <div
                                                                                    className="m-stack__item m-stack__item--center m-stack__item--middle">
                                                                                <span className="">
                                                                                    All caught up!
                                                                                    <br /> No new logs.
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li> */}
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </header>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        trading: state.trading,
        cacheUser: state.cacheUser,
        language: state.language
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Object.assign({}, { setTradingStatus: homeActions.setTradingStatus, setKYCStatus: homeActions.setKYCStatus, setLanguage: setLanguage }), dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps) (enhanceWithClickOutside(AppHeader));
