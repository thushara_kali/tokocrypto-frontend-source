import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, ModalHeader, ModalBody, Input, Label} from 'reactstrap';
import cryptoFormatter, { cryptoFormatterByCoin } from '../../helpers/cryptoFormatter';
import amountFormatter from '../../helpers/amountFormatter';
import "react-toggle/style.css" // for ES6 modules
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import {faStar} from '@fortawesome/fontawesome-free-solid';
import localStorage from 'localStorage';
import EventBus from 'eventing-bus';


fontawesome.library.add(faStar)

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class WalletAdd extends Component {

    constructor(){
        super();
        this.state = {
            loading: false,

            // Coins' favorite status
            favorite: {
                btc: false,
                eth: false,
                xrp: false,
                dgx: false,
                gusd: false,
                pax: false,
                tusd: false,
                usdc: false,
                usdt: false,
                ttc: false,
                swipe: false,
                zil: false,
                // favoriteCoin: false,
                favoriteCount: 0
            },

            show: {
                btc: true,
                eth: true,
                xrp: true,
                dgx: true,
                gusd: true,
                pax: true,
                tusd: true,
                usdc: true,
                usdt: true,
                ttc: true,
                swipe: true,
                zil: true
            },

            hideSmallBalance: false
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.value
            this.setState(obj)

        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    close(closeAll = false){
        if (this.state.hideSmallBalance) {
            this.handleHideSmallBalance()
        }
        this.props.onClose();
    }

    componentWillReceiveProps(newProps){
        // console.log('rerender')
        // EventBus.publish('refreshFavorite')
        if (newProps.currency !== this.props.currency) {
            this.getLimit(newProps.currency)
        } else {
            if(this.state.limit === 0){
                this.getLimit(this.props.currency)
            }
        }
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    componentDidMount() {
        this.fetchFavorite();
        let self = this;
        EventBus.on('refreshFavorite', () => {
            self.fetchFavorite();
        })
    }

    fetchFavorite() {
        const self = this;
        return new Promise((resolve, reject) => {
            console.log('Fetch Favorite invoke')
            let result = localStorage.getItem('userFavorites');
            if(result){
                let resultParsed =  JSON.parse(result);
                console.log('resultParsedJSON', resultParsed)
                // self.setState({
                //     favorite: resultParsed
                // }, () => {console.log('fetchFavorite > this.state.favorite', this.state.favorite)})
                self.setState({
                    favorite: {
                        btc: resultParsed.favorite.btc,
                        eth: resultParsed.favorite.eth,
                        xrp: resultParsed.favorite.xrp,
                        dgx: resultParsed.favorite.dgx,
                        gusd: resultParsed.favorite.gusd,
                        pax: resultParsed.favorite.pax,
                        tusd: resultParsed.favorite.tusd,
                        usdc: resultParsed.favorite.usdc,
                        usdt: resultParsed.favorite.usdt,
                        ttc: resultParsed.favorite.ttc,
                        swipe: resultParsed.favorite.swipe,
                        zil: resultParsed.favorite.zil,
                        // favoriteCoin: resultParsed.favorite.favoriteCoin,
                        favoriteCount: resultParsed.favorite.favoriteCount
                    }
                })
            } else {
                console.log('user Favorite empty')
            }
            
        })
    }

    addToFavorite(coin) {
        console.log('add Favorite invoke')
        if(this.state.favorite.favoriteCount < 4){
            switch (coin) {
                case 'BTC':
                    this.setState({
                        favorite: {
                            btc: true,
                            eth: this.state.favorite.eth,
                            xrp: this.state.favorite.xrp,
                            dgx: this.state.favorite.dgx,
                            gusd: this.state.favorite.gusd,
                            pax: this.state.favorite.pax,
                            tusd: this.state.favorite.tusd,
                            usdc: this.state.favorite.usdc,
                            usdt: this.state.favorite.usdt,
                            ttc: this.state.favorite.ttc,
                            swipe: this.state.favorite.swipe,
                            zil: this.state.favorite.zil,
                            // favoriteCoin: true,
                            favoriteCount: this.state.favorite.favoriteCount + 1
                        }
                    }, () => {
                        
                        this.saveFavorite()
                    })
                    break;
                
                case 'ETH':
                    this.setState({
                        favorite: {
                            btc: this.state.favorite.btc,
                            eth: true,
                            xrp: this.state.favorite.xrp,
                            dgx: this.state.favorite.dgx,
                            gusd: this.state.favorite.gusd,
                            pax: this.state.favorite.pax,
                            tusd: this.state.favorite.tusd,
                            usdc: this.state.favorite.usdc,
                            usdt: this.state.favorite.usdt,
                            ttc: this.state.favorite.ttc,
                            swipe: this.state.favorite.swipe,
                            zil: this.state.favorite.zil,
                            // favoriteCoin: true,
                            favoriteCount: this.state.favorite.favoriteCount + 1
                        }
                    }, () => {
                        
                        this.saveFavorite()
                    })
                    break;
    
                case 'XRP':
                    this.setState({
                        favorite: {
                            btc: this.state.favorite.btc,
                            eth: this.state.favorite.eth,
                            xrp: true,
                            dgx: this.state.favorite.dgx,
                            gusd: this.state.favorite.gusd,
                            pax: this.state.favorite.pax,
                            tusd: this.state.favorite.tusd,
                            usdc: this.state.favorite.usdc,
                            usdt: this.state.favorite.usdt,
                            ttc: this.state.favorite.ttc,
                            swipe: this.state.favorite.swipe,
                            zil: this.state.favorite.zil,
                            // favoriteCoin: true,
                            favoriteCount: this.state.favorite.favoriteCount + 1
                        }
                    }, () => {
                        
                        this.saveFavorite();
                    })
                    break;
    
                case 'DGX':
                    this.setState({
                        favorite: {
                            btc: this.state.favorite.btc,
                            eth: this.state.favorite.eth,
                            xrp: this.state.favorite.xrp,
                            dgx: true,
                            gusd: this.state.favorite.gusd,
                            pax: this.state.favorite.pax,
                            tusd: this.state.favorite.tusd,
                            usdc: this.state.favorite.usdc,
                            usdt: this.state.favorite.usdt,
                            ttc: this.state.favorite.ttc,
                            swipe: this.state.favorite.swipe,
                            zil: this.state.favorite.zil,
                            // favoriteCoin: true,
                            favoriteCount: this.state.favorite.favoriteCount + 1
                        }
                    }, () => {
                        console.log('this.state.favorite', this.state.favorite)
                        this.saveFavorite();
                    })
                    break;
    
                case 'GUSD':
                    this.setState({
                        favorite: {
                            btc: this.state.favorite.btc,
                            eth: this.state.favorite.eth,
                            xrp: this.state.favorite.xrp,
                            dgx: this.state.favorite.dgx,
                            gusd: true,
                            pax: this.state.favorite.pax,
                            tusd: this.state.favorite.tusd,
                            usdc: this.state.favorite.usdc,
                            usdt: this.state.favorite.usdt,
                            ttc: this.state.favorite.ttc,
                            swipe: this.state.favorite.swipe,
                            zil: this.state.favorite.zil,
                            // favoriteCoin: true,
                            favoriteCount: this.state.favorite.favoriteCount + 1
                        }
                    }, () => {
                        console.log('this.state.favorite', this.state.favorite)
                        this.saveFavorite();
                    })
                    break;
    
                case 'PAX':
                    this.setState({
                        favorite: {
                            btc: this.state.favorite.btc,
                            eth: this.state.favorite.eth,
                            xrp: this.state.favorite.xrp,
                            dgx: this.state.favorite.dgx,
                            gusd: this.state.favorite.gusd,
                            pax: true,
                            tusd: this.state.favorite.tusd,
                            usdc: this.state.favorite.usdc,
                            usdt: this.state.favorite.usdt,
                            ttc: this.state.favorite.ttc,
                            swipe: this.state.favorite.swipe,
                            zil: this.state.favorite.zil,
                            // favoriteCoin: true,
                            favoriteCount: this.state.favorite.favoriteCount + 1
                        }
                    }, () => {
                        console.log('this.state.favorite', this.state.favorite)
                        this.saveFavorite();
                    })
                    break;
    
                case 'TUSD':
                    this.setState({
                        favorite: {
                            btc: this.state.favorite.btc,
                            eth: this.state.favorite.eth,
                            xrp: this.state.favorite.xrp,
                            dgx: this.state.favorite.dgx,
                            gusd: this.state.favorite.gusd,
                            pax: this.state.favorite.pax,
                            tusd: true,
                            usdc: this.state.favorite.usdc,
                            usdt: this.state.favorite.usdt,
                            ttc: this.state.favorite.ttc,
                            swipe: this.state.favorite.swipe,
                            zil: this.state.favorite.zil,
                            // favoriteCoin: true,
                            favoriteCount: this.state.favorite.favoriteCount + 1
                        }
                    }, () => {
                        
                        this.saveFavorite();
                    })
                    break;


                case 'USDC':
                    this.setState({
                        favorite: {
                            btc: this.state.favorite.btc,
                            eth: this.state.favorite.eth,
                            xrp: this.state.favorite.xrp,
                            dgx: this.state.favorite.dgx,
                            gusd: this.state.favorite.gusd,
                            pax: this.state.favorite.pax,
                            tusd: this.state.favorite.tusd,
                            usdc: true,
                            usdt: this.state.favorite.usdt,
                            ttc: this.state.favorite.ttc,
                            swipe: this.state.favorite.swipe,
                            zil: this.state.favorite.zil,
                            // favoriteCoin: true,
                            favoriteCount: this.state.favorite.favoriteCount + 1
                        }
                    }, () => {
                        
                        this.saveFavorite();
                    })
                    break;
    
                case 'USDT':
                    this.setState({
                        favorite: {
                            btc: this.state.favorite.btc,
                            eth: this.state.favorite.eth,
                            xrp: this.state.favorite.xrp,
                            dgx: this.state.favorite.dgx,
                            gusd: this.state.favorite.gusd,
                            pax: this.state.favorite.pax,
                            tusd: this.state.favorite.tusd,
                            usdc: this.state.favorite.usdc,
                            usdt: true,
                            ttc: this.state.favorite.ttc,
                            swipe: this.state.favorite.swipe,
                            zil: this.state.favorite.zil,
                            // favoriteCoin: true,
                            favoriteCount: this.state.favorite.favoriteCount + 1
                        }
                    }, () => {
                        
                        this.saveFavorite();
                    })
                    break;

                case 'TTC':
                    this.setState({
                        favorite: {
                            btc: this.state.favorite.btc,
                            eth: this.state.favorite.eth,
                            xrp: this.state.favorite.xrp,
                            dgx: this.state.favorite.dgx,
                            gusd: this.state.favorite.gusd,
                            pax: this.state.favorite.pax,
                            tusd: this.state.favorite.tusd,
                            usdc: this.state.favorite.usdc,
                            usdt: this.state.favorite.usdt,
                            ttc: true,
                            swipe: this.state.favorite.swipe,
                            zil: this.state.favorite.zil,
                            // favoriteCoin: true,
                            favoriteCount: this.state.favorite.favoriteCount + 1
                        }
                    }, () => {
                        
                        this.saveFavorite();
                    })
                    break;
                
                case 'SWIPE':
                    this.setState({
                        favorite: {
                            btc: this.state.favorite.btc,
                            eth: this.state.favorite.eth,
                            xrp: this.state.favorite.xrp,
                            dgx: this.state.favorite.dgx,
                            gusd: this.state.favorite.gusd,
                            pax: this.state.favorite.pax,
                            tusd: this.state.favorite.tusd,
                            usdc: this.state.favorite.usdc,
                            usdt: this.state.favorite.usdt,
                            ttc: this.state.favorite.ttc,
                            swipe: true,
                            zil: this.state.favorite.zil,
                            // favoriteCoin: true,
                            favoriteCount: this.state.favorite.favoriteCount + 1
                        }
                    }, () => {
                        
                        this.saveFavorite();
                    })
                    break;

                case 'ZIL':
                    this.setState({
                        favorite: {
                            btc: this.state.favorite.btc,
                            eth: this.state.favorite.eth,
                            xrp: this.state.favorite.xrp,
                            dgx: this.state.favorite.dgx,
                            gusd: this.state.favorite.gusd,
                            pax: this.state.favorite.pax,
                            tusd: this.state.favorite.tusd,
                            usdc: this.state.favorite.usdc,
                            usdt: this.state.favorite.usdt,
                            ttc: this.state.favorite.ttc,
                            swipe: this.state.favorite.swipe,
                            zil: true,
                            // favoriteCoin: true,
                            favoriteCount: this.state.favorite.favoriteCount + 1
                        }
                    }, () => {
                        
                        this.saveFavorite();
                    })
                    break;
                    
                default:
                    break;
            }
    
            console.log('JSON', JSON.stringify({
                favorite: {
                    btc: this.state.favorite.btc,
                    eth: this.state.favorite.eth,
                    xrp: this.state.favorite.xrp,
                    dgx: this.state.favorite.dgx,
                    gusd: this.state.favorite.gusd,
                    pax: this.state.favorite.pax,
                    tusd: this.state.favorite.tusd,
                    usdc: this.state.favorite.usdc,
                    usdt: this.state.favorite.usdt,
                    ttc: this.state.favorite.ttc,
                    swipe: this.state.favorite.swipe,
                    zil: this.state.favorite.zil,
                    // favoriteCoin: this.state.favorite.favoriteCoin,
                    favoriteCount: this.state.favorite.favoriteCount
                }
            }))
    
        } else {
            alert('You can only select maximum 4 favorites');
        }
        
    }

    resetAllFavorite() {
        
        this.setState({
            favorite: {
                btc: false,
                eth: false,
                xrp: false,
                dgx: false,
                gusd: false,
                pax: false,
                tusd: false,
                usdc: false,
                usdt: false,
                ttc: false,
                swipe: false,
                zil: false,
                // favoriteCoin: false,
                favoriteCount: 0
            }
        }, () => {
            
            localStorage.removeItem('userFavorites')
            EventBus.publish('refreshFavorite')
            
        })
        
    }

    saveFavorite() {
        localStorage.setItem('userFavorites', JSON.stringify({
            favorite: {
                btc: this.state.favorite.btc,
                eth: this.state.favorite.eth,
                xrp: this.state.favorite.xrp,
                dgx: this.state.favorite.dgx,
                gusd: this.state.favorite.gusd,
                pax: this.state.favorite.pax,
                tusd: this.state.favorite.tusd,
                usdc: this.state.favorite.usdc,
                usdt: this.state.favorite.usdt,
                ttc: this.state.favorite.ttc,
                swipe: this.state.favorite.swipe,
                zil: this.state.favorite.zil,
                // favoriteCoin: this.state.favorite.favoriteCoin,
                favoriteCount: this.state.favorite.favoriteCount
            }
        }))
        EventBus.publish('refreshFavorite')
    }

    removeFromFavorite(coin) {
        switch (coin) {
            case 'BTC':
                this.setState({
                    favorite: {
                        btc: false,
                        eth: this.state.favorite.eth,
                        xrp: this.state.favorite.xrp,
                        dgx: this.state.favorite.dgx,
                        gusd: this.state.favorite.gusd,
                        pax: this.state.favorite.pax,
                        tusd: this.state.favorite.tusd,
                        usdc: this.state.favorite.usdc,
                        usdt: this.state.favorite.usdt,
                        ttc: this.state.favorite.ttc,
                        swipe: this.state.favorite.swipe,
                        zil: this.state.favorite.zil,
                        // favoriteCoin: true,
                        favoriteCount: this.state.favorite.favoriteCount - 1
                    }
                }, () => {
                    
                    if(this.state.favorite.favoriteCount>0){
                        this.saveFavorite()
                    } else {
                        this.resetAllFavorite()
                    }
                })
                break;
            
            case 'ETH':
                this.setState({
                    favorite: {
                        btc: this.state.favorite.btc,
                        eth: false,
                        xrp: this.state.favorite.xrp,
                        dgx: this.state.favorite.dgx,
                        gusd: this.state.favorite.gusd,
                        pax: this.state.favorite.pax,
                        tusd: this.state.favorite.tusd,
                        usdc: this.state.favorite.usdc,
                        usdt: this.state.favorite.usdt,
                        ttc: this.state.favorite.ttc,
                        swipe: this.state.favorite.swipe,
                        zil: this.state.favorite.zil,
                        // favoriteCoin: true,
                        favoriteCount: this.state.favorite.favoriteCount - 1
                    }
                }, () => {
                    
                    if(this.state.favorite.favoriteCount>0){
                        this.saveFavorite()
                    } else {
                        this.resetAllFavorite()
                    }
                })
                break;

            case 'XRP':
                this.setState({
                    favorite: {
                        btc: this.state.favorite.btc,
                        eth: this.state.favorite.eth,
                        xrp: false,
                        dgx: this.state.favorite.dgx,
                        gusd: this.state.favorite.gusd,
                        pax: this.state.favorite.pax,
                        tusd: this.state.favorite.tusd,
                        usdc: this.state.favorite.usdc,
                        usdt: this.state.favorite.usdt,
                        ttc: this.state.favorite.ttc,
                        swipe: this.state.favorite.swipe,
                        zil: this.state.favorite.zil,
                        // favoriteCoin: true,
                        favoriteCount: this.state.favorite.favoriteCount - 1
                    }
                }, () => {
                    
                    if(this.state.favorite.favoriteCount>0){
                        this.saveFavorite()
                    } else {
                        this.resetAllFavorite()
                    }
                })
                break;

            case 'DGX':
                this.setState({
                    favorite: {
                        btc: this.state.favorite.btc,
                        eth: this.state.favorite.eth,
                        xrp: this.state.favorite.xrp,
                        dgx: false,
                        gusd: this.state.favorite.gusd,
                        pax: this.state.favorite.pax,
                        tusd: this.state.favorite.tusd,
                        usdc: this.state.favorite.usdc,
                        usdt: this.state.favorite.usdt,
                        usdt: this.state.favorite.usdt,
                        ttc: this.state.favorite.ttc,
                        swipe: this.state.favorite.swipe,
                        zil: this.state.favorite.zil,
                        // favoriteCoin: true,
                        favoriteCount: this.state.favorite.favoriteCount - 1
                    }
                }, () => {
                    if(this.state.favorite.favoriteCount>0){
                        this.saveFavorite()
                    } else {
                        this.resetAllFavorite()
                    }
                })
                break;

            case 'GUSD':
                this.setState({
                    favorite: {
                        btc: this.state.favorite.btc,
                        eth: this.state.favorite.eth,
                        xrp: this.state.favorite.xrp,
                        dgx: this.state.favorite.dgx,
                        gusd: false,
                        pax: this.state.favorite.pax,
                        tusd: this.state.favorite.tusd,
                        usdc: this.state.favorite.usdc,
                        usdt: this.state.favorite.usdt,
                        usdt: this.state.favorite.usdt,
                        ttc: this.state.favorite.ttc,
                        swipe: this.state.favorite.swipe,
                        zil: this.state.favorite.zil,
                        // favoriteCoin: true,
                        favoriteCount: this.state.favorite.favoriteCount - 1
                    }
                }, () => {
                    if(this.state.favorite.favoriteCount>0){
                        this.saveFavorite()
                    } else {
                        this.resetAllFavorite()
                    }
                })
                break;

            case 'PAX':
                this.setState({
                    favorite: {
                        btc: this.state.favorite.btc,
                        eth: this.state.favorite.eth,
                        xrp: this.state.favorite.xrp,
                        dgx: this.state.favorite.dgx,
                        gusd: this.state.favorite.gusd,
                        pax: false,
                        tusd: this.state.favorite.tusd,
                        usdc: this.state.favorite.usdc,
                        usdt: this.state.favorite.usdt,
                        ttc: this.state.favorite.ttc,
                        swipe: this.state.favorite.swipe,
                        zil: this.state.favorite.zil,
                        // favoriteCoin: true,
                        favoriteCount: this.state.favorite.favoriteCount - 1
                    }
                }, () => {
                    if(this.state.favorite.favoriteCount>0){
                        this.saveFavorite()
                    } else {
                        this.resetAllFavorite()
                    }
                })
                break;

            case 'TUSD':
                this.setState({
                    favorite: {
                        btc: this.state.favorite.btc,
                        eth: this.state.favorite.eth,
                        xrp: this.state.favorite.xrp,
                        dgx: this.state.favorite.dgx,
                        gusd: this.state.favorite.gusd,
                        pax: this.state.favorite.pax,
                        tusd: false,
                        usdc: this.state.favorite.usdc,
                        usdt: this.state.favorite.usdt,
                        ttc: this.state.favorite.ttc,
                        swipe: this.state.favorite.swipe,
                        zil: this.state.favorite.zil,
                        // favoriteCoin: true,
                        favoriteCount: this.state.favorite.favoriteCount - 1
                    }
                }, () => {
                    if(this.state.favorite.favoriteCount>0){
                        this.saveFavorite()
                    } else {
                        this.resetAllFavorite()
                    }
                })
                break;
            
            case 'USDC':
                this.setState({
                    favorite: {
                        btc: this.state.favorite.btc,
                        eth: this.state.favorite.eth,
                        xrp: this.state.favorite.xrp,
                        dgx: this.state.favorite.dgx,
                        gusd: this.state.favorite.gusd,
                        pax: this.state.favorite.pax,
                        tusd: this.state.favorite.tusd,
                        usdc: false,
                        usdt: this.state.favorite.usdt,
                        ttc: this.state.favorite.ttc,
                        swipe: this.state.favorite.swipe,
                        zil: this.state.favorite.zil,
                        // favoriteCoin: true,
                        favoriteCount: this.state.favorite.favoriteCount - 1
                    }
                }, () => {
                    if(this.state.favorite.favoriteCount>0){
                        this.saveFavorite()
                    } else {
                        this.resetAllFavorite()
                    }
                })
                break;

            case 'USDT':
                this.setState({
                    favorite: {
                        btc: this.state.favorite.btc,
                        eth: this.state.favorite.eth,
                        xrp: this.state.favorite.xrp,
                        dgx: this.state.favorite.dgx,
                        gusd: this.state.favorite.gusd,
                        pax: this.state.favorite.pax,
                        tusd: this.state.favorite.tusd,
                        usdc: this.state.favorite.usdc,
                        usdt: false,
                        ttc: this.state.favorite.ttc,
                        swipe: this.state.favorite.swipe,
                        zil: this.state.favorite.zil,
                        // favoriteCoin: true,
                        favoriteCount: this.state.favorite.favoriteCount - 1
                    }
                }, () => {
                    if(this.state.favorite.favoriteCount>0){
                        this.saveFavorite()
                    } else {
                        this.resetAllFavorite()
                    }
                })
                break;

            case 'TTC':
                this.setState({
                    favorite: {
                        btc: this.state.favorite.btc,
                        eth: this.state.favorite.eth,
                        xrp: this.state.favorite.xrp,
                        dgx: this.state.favorite.dgx,
                        gusd: this.state.favorite.gusd,
                        pax: this.state.favorite.pax,
                        tusd: this.state.favorite.tusd,
                        usdc: this.state.favorite.usdc,
                        usdt: this.state.favorite.usdt,
                        swipe: this.state.favorite.swipe,
                        zil: this.state.favorite.zil,
                        ttc: false,
                        // favoriteCoin: true,
                        favoriteCount: this.state.favorite.favoriteCount - 1
                    }
                }, () => {
                    if(this.state.favorite.favoriteCount>0){
                        this.saveFavorite()
                    } else {
                        this.resetAllFavorite()
                    }
                })
                break;
            
            case 'SWIPE':
                this.setState({
                    favorite: {
                        btc: this.state.favorite.btc,
                        eth: this.state.favorite.eth,
                        xrp: this.state.favorite.xrp,
                        dgx: this.state.favorite.dgx,
                        gusd: this.state.favorite.gusd,
                        pax: this.state.favorite.pax,
                        tusd: this.state.favorite.tusd,
                        usdc: this.state.favorite.usdc,
                        usdt: this.state.favorite.usdt,
                        ttc: this.state.favorite.ttc,
                        zil: this.state.favorite.zil,
                        swipe: false,
                        // favoriteCoin: true,
                        favoriteCount: this.state.favorite.favoriteCount - 1
                    }
                }, () => {
                    
                    if(this.state.favorite.favoriteCount>0){
                        this.saveFavorite()
                    } else {
                        this.resetAllFavorite()
                    }
                })
                break;
            
            case 'ZIL':
                this.setState({
                    favorite: {
                        btc: this.state.favorite.btc,
                        eth: this.state.favorite.eth,
                        xrp: this.state.favorite.xrp,
                        dgx: this.state.favorite.dgx,
                        gusd: this.state.favorite.gusd,
                        pax: this.state.favorite.pax,
                        tusd: this.state.favorite.tusd,
                        usdc: this.state.favorite.usdc,
                        usdt: this.state.favorite.usdt,
                        ttc: this.state.favorite.ttc,
                        swipe: this.state.favorite.swipe,
                        zil: false,
                        // favoriteCoin: true,
                        favoriteCount: this.state.favorite.favoriteCount - 1
                    }
                }, () => {
                    
                    if(this.state.favorite.favoriteCount>0){
                        this.saveFavorite()
                    } else {
                        this.resetAllFavorite()
                    }
                })
                break;

            default:
                break;
        }

        console.log('JSON', JSON.stringify({
            favorite: {
                btc: this.state.favorite.btc,
                eth: this.state.favorite.eth,
                xrp: this.state.favorite.xrp,
                dgx: this.state.favorite.dgx,
                gusd: this.state.favorite.gusd,
                pax: this.state.favorite.pax,
                tusd: this.state.favorite.tusd,
                usdc: this.state.favorite.usdc,
                usdt: this.state.favorite.usdt,
                ttc: this.state.favorite.ttc,
                swipe: this.state.favorite.swipe,
                zil: this.state.favorite.zil,
                // favoriteCoin: this.state.favorite.favoriteCoin,
                favoriteCount: this.state.favorite.favoriteCount
            }
        }))

    }

    handleHideSmallBalance() {
        console.log('hidesmallbalance invoked')
        this.setState({hideSmallBalance: !this.state.hideSmallBalance},() => {
            console.log('current hidesmallbalance', this.state.hideSmallBalance)
            if (this.state.hideSmallBalance) {
                this.hideSmallBalance()
            } else {
                console.log('return hidesmallbalance')
                this.setState({
                    show: {
                        btc: true,
                        eth: true,
                        xrp: true,
                        dgx: true,
                        gusd: true,
                        pax: true,
                        tusd: true,
                        usdc: true,
                        usdt: true,
                        ttc: true,
                        swipe: true,
                        zil: true
                    },
                })
            }
        })
    }

    hideSmallBalance() {
        var showBTC, showETH, showXRP, showDGX, showPAX, showTUSD, showGUSD, showUSDC, showUSDT, showTTC, showSWIPE, showZIL;
        
        ((!this.props.BTCTotalIDR || this.props.BTCTotalIDR < 5000 ) && this.state.favorite.btc == false  ) ? showBTC = false : showBTC = true;
        ((!this.props.ETHTotalIDR || this.props.ETHTotalIDR < 5000 ) && this.state.favorite.eth == false  ) ? showETH = false : showETH = true;
        ((!this.props.XRPTotalIDR || this.props.XRPTotalIDR < 5000 ) && this.state.favorite.xrp  == false ) ? showXRP = false : showXRP = true;
        ((!this.props.DGXTotalIDR || this.props.DGXTotalIDR < 5000 ) && this.state.favorite.dgx == false  ) ? showDGX = false : showDGX = true;
        ((!this.props.PAXTotalIDR || this.props.PAXTotalIDR < 5000 ) && this.state.favorite.pax  == false ) ? showPAX = false : showPAX = true;
        ((!this.props.TUSDTotalIDR || this.props.TUSDTotalIDR < 5000 ) && this.state.favorite.tusd == false  ) ? showTUSD = false : showTUSD = true;
        ((!this.props.GUSDTotalIDR || this.props.GUSDTotalIDR < 5000 ) && this.state.favorite.gusd  == false ) ? showGUSD = false : showGUSD = true;
        ((!this.props.USDCTotalIDR || this.props.USDCTotalIDR < 5000 ) && this.state.favorite.usdc == false  ) ? showUSDC = false : showUSDC = true;
        ((!this.props.USDTTotalIDR || this.props.USDTTotalIDR < 5000 ) && this.state.favorite.usdt == false  ) ? showUSDT = false : showUSDT = true;
        ((!this.props.TTCTotalIDR || this.props.TTCTotalIDR < 5000 ) && this.state.favorite.ttc == false  )  ? showTTC = false : showTTC = true;
        ((!this.props.SWIPETotalIDR || this.props.SWIPETotalIDR < 5000 ) && this.state.favorite.swipe == false  )  ? showSWIPE = false : showSWIPE = true;
        ((!this.props.ZILTotalIDR || this.props.ZILTotalIDR < 5000 ) && this.state.favorite.zil == false  )  ? showZIL = false : showZIL = true;

        this.setState({
            show: {
                btc: showBTC,
                eth: showETH,
                xrp: showXRP,
                dgx: showDGX,
                gusd: showGUSD,
                pax: showPAX,
                tusd: showTUSD,
                usdc: showUSDC,
                usdt: showUSDT,
                ttc: showTTC,
                swipe: showSWIPE,
                zil: showZIL
            }
        }, () => {})

    }

    render(){
        let BTCBalance = this.props.data.BTCAccount.Balance ? this.props.data.BTCAccount.Balance : null;
        let ETHBalance = this.props.data.ETHAccount.Balance ? this.props.data.ETHAccount.Balance : null;
        let XRPBalance = this.props.data.XRPAccount.Balance ? this.props.data.XRPAccount.Balance : null;
        let DGXBalance = this.props.data.DGXAccount.Balance ? this.props.data.DGXAccount.Balance : null;
        let PAXBalance = this.props.data.PAXAccount.Balance ? this.props.data.PAXAccount.Balance : null;
        let TUSDBalance = this.props.data.TUSDAccount.Balance ? this.props.data.TUSDAccount.Balance : null;
        let GUSDBalance = this.props.data.GUSDAccount.Balance ? this.props.data.GUSDAccount.Balance : null;
        let USDCBalance = this.props.data.USDCAccount.Balance ? this.props.data.USDCAccount.Balance : null;
        let USDTBalance = this.props.data.USDTAccount.Balance ? this.props.data.USDTAccount.Balance : null;
        let TTCBalance = this.props.data.TTCAccount.Balance ? this.props.data.TTCAccount.Balance : null;
        let SWIPEBalance = this.props.data.SWIPEAccount.Balance ? this.props.data.SWIPEAccount.Balance : null;
        let ZILBalance = this.props.data.ZILAccount.Balance || 0;

        return (
            <div>
                <Modal isOpen={this.props.show} toggle={() => this.close(true)} className="modal-dialog-centered">
                    <ModalHeader toggle={() => this.close(true)}>
                        Favourites
                    </ModalHeader>
                    <ModalBody className="modal-body tc-modal-body">
                        <div>
                            <Label check className="float-right" >   
                                <Input type="checkbox" onChange={() =>  this.handleHideSmallBalance()}/>{'   '}
                                <span style={{marginLeft: 5, fontSize: 10}}> Hide small balances </span>
                            </Label>
                        </div>
                        
                        <div className="m-portlet__body" style={{display:"inline-flex",width:"100%",paddingTop:"15px",paddingLeft:"0px",paddingRight:"0px"}}>
                            <table className="table referral-details-table example-table">
                                <thead style={{background:"#F7F7F7", color:"#ffffff"}}>
                                    <tr className="referral-details-table-header">
                                        <th style={{color:"#636161",fontSize:"13px",fontWeight:"300"}}></th>
                                        <th style={{color:"#636161",fontSize:"13px",fontWeight:"300"}}>Pair</th>
                                        <th style={{color:"#636161",fontSize:"13px",fontWeight:"300"}}>Quantity</th>
                                        <th style={{color:"#636161",fontSize:"13px",fontWeight:"300"}}>Estimated value IDR</th>
                                    </tr>
                                </thead>

                                {/* After Favorite */}
                                {this.state.favorite.btc && this.state.show.btc  && 
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.removeFromFavorite('BTC')}>
                                        <td style={{fontSize:"14px", color:"#F49600"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>BTC/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( BTCBalance, 'BTC' ) }</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.BTCTotalIDR, 'btc')}</td>
                                    </tr>
                                }
                                
                                {this.state.favorite.eth && this.state.show.eth && 
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.removeFromFavorite('ETH')}>
                                        <td style={{fontSize:"14px", color:"#F49600"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>ETH/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( ETHBalance, 'ETH' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.ETHTotalIDR, 'eth')}</td>
                                    </tr>
                                }

                                {this.state.favorite.xrp && this.state.show.xrp && 
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.removeFromFavorite('XRP')}>
                                        <td style={{fontSize:"14px", color:"#F49600"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>XRP/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( XRPBalance, 'XRP' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.XRPTotalIDR, 'xrp')}</td>
                                    </tr>
                                }

                                {this.state.favorite.dgx && this.state.show.dgx && 
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.removeFromFavorite('DGX')}>
                                        <td style={{fontSize:"14px", color:"#F49600"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>DGX/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( DGXBalance, 'DGX' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.DGXTotalIDR, 'dgx')}</td>
                                    </tr>
                                }
                                
                                {this.state.favorite.gusd && this.state.show.gusd  && 
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.removeFromFavorite('GUSD')}>
                                        <td style={{fontSize:"14px", color:"#F49600"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>GUSD/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( GUSDBalance, 'GUSD' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.GUSDTotalIDR, 'gusd')}</td>
                                    </tr>
                                }

                                {this.state.favorite.pax && this.state.show.pax && 
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.removeFromFavorite('PAX')}>
                                        <td style={{fontSize:"14px", color:"#F49600"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>PAX/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( PAXBalance, 'PAX' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.PAXTotalIDR, 'pax')}</td>
                                    </tr>
                                }

                                {this.state.favorite.tusd && this.state.show.tusd &&
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.removeFromFavorite('TUSD')}>
                                        <td style={{fontSize:"14px", color:"#F49600"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>TUSD/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( TUSDBalance, 'TUSD' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.TUSDTotalIDR, 'tusd')}</td>
                                    </tr>
                                }

                                {this.state.favorite.usdc && this.state.show.usdc &&
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.removeFromFavorite('USDC')}>
                                        <td style={{fontSize:"14px", color:"#F49600"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>USDC/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( USDCBalance, 'USDC' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.USDCTotalIDR, 'usdc')}</td>
                                    </tr>
                                }

                                {this.state.favorite.usdt && this.state.show.usdt &&
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.removeFromFavorite('USDT')}>
                                        <td style={{fontSize:"14px", color:"#F49600"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>USDT/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( USDTBalance, 'USDT' ) }</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.USDTTotalIDR, 'usdt')}</td>
                                    </tr>
                                } 
                            

                                {this.state.favorite.ttc && this.state.show.ttc &&
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.removeFromFavorite('TTC')}>
                                        <td style={{fontSize:"14px", color:"#F49600"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>TTC/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( TTCBalance, 'TTC' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.TTCTotalIDR, 'ttc')}</td>
                                    </tr>
                                }

                                {this.state.favorite.swipe && this.state.show.swipe &&
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.removeFromFavorite('SWIPE')}>
                                        <td style={{fontSize:"14px", color:"#F49600"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>SWIPE/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( SWIPEBalance, 'SWIPE' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.SWIPETotalIDR, 'swipe')}</td>
                                    </tr>
                                }

                                {this.state.favorite.zil && this.state.show.zil &&
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.removeFromFavorite('ZIL')}>
                                        <td style={{fontSize:"14px", color:"#F49600"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>ZIL/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( ZILBalance, 'ZIL' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.ZILTotalIDR,'zil')}</td>
                                    </tr>
                                }

                                {/* Before Favorite */}
                                {!this.state.favorite.btc && this.state.show.btc && 
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.addToFavorite('BTC')}>
                                        <td style={{fontSize:"14px"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>BTC/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( BTCBalance, 'BTC' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.BTCTotalIDR, 'btc')}</td>
                                    </tr>
                                }
                                
                                {!this.state.favorite.eth && this.state.show.eth && 
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.addToFavorite('ETH')}>
                                        <td style={{fontSize:"14px"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>ETH/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( ETHBalance, 'ETH' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.ETHTotalIDR, 'eth')}</td>
                                    </tr>
                                }

                                {!this.state.favorite.xrp && this.state.show.xrp && 
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.addToFavorite('XRP')}>
                                        <td style={{fontSize:"14px"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>XRP/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( XRPBalance, 'XRP' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.XRPTotalIDR,'xrp')}</td>
                                    </tr>
                                }

                                {!this.state.favorite.dgx && this.state.show.dgx && 
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.addToFavorite('DGX')}>
                                        <td style={{fontSize:"14px"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>DGX/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>{ cryptoFormatterByCoin( DGXBalance, 'DGX' )}</td>
                                        <td style={{fontSize:"14px"}}>{amountFormatter(this.props.DGXTotalIDR, 'dgx')}</td>
                                    </tr>
                                }

                                
                                {!this.state.favorite.gusd && this.state.show.gusd && 
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.addToFavorite('GUSD')}>
                                        <td style={{fontSize:"14px"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>GUSD/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}> { cryptoFormatterByCoin( GUSDBalance, 'GUSD' )} </td>
                                        <td style={{fontSize:"14px"}}> {amountFormatter(this.props.GUSDTotalIDR, 'gusd')} </td>
                                    </tr>
                                }

                                {!this.state.favorite.pax && this.state.show.pax && 
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.addToFavorite('PAX')}>
                                        <td style={{fontSize:"14px"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>PAX/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}> { cryptoFormatterByCoin( PAXBalance, 'PAX' )} </td>
                                        <td style={{fontSize:"14px"}}> {amountFormatter(this.props.PAXTotalIDR, 'pax')} </td>
                                    </tr>
                                }

                                {!this.state.favorite.tusd && this.state.show.tusd &&
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.addToFavorite('TUSD')}>
                                        <td style={{fontSize:"14px"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>TUSD/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}> { cryptoFormatterByCoin( TUSDBalance, 'TUSD' )} </td>
                                        <td style={{fontSize:"14px"}}> {amountFormatter(this.props.TUSDTotalIDR, 'tusd')} </td>
                                    </tr>
                                }

                                {!this.state.favorite.usdc && this.state.show.usdc &&
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.addToFavorite('USDC')}>
                                        <td style={{fontSize:"14px"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>USDC/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}> { cryptoFormatterByCoin( USDCBalance, 'USDC' )} </td>
                                        <td style={{fontSize:"14px"}}> {amountFormatter(this.props.USDCTotalIDR, 'usdc')} </td>
                                    </tr>
                                }

                                {!this.state.favorite.usdt && this.state.show.usdt &&
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.addToFavorite('USDT')}>
                                        <td style={{fontSize:"14px"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>USDT/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}> { cryptoFormatterByCoin( USDTBalance, 'USDT' )} </td>
                                        <td style={{fontSize:"14px"}}> {amountFormatter(this.props.USDTTotalIDR, 'usdt')} </td>
                                    </tr>
                                } 
                            
                                {!this.state.favorite.ttc && this.state.show.ttc &&
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.addToFavorite('TTC')}>
                                        <td style={{fontSize:"14px"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>TTC/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}> { cryptoFormatterByCoin( TTCBalance, 'TTC' ) } </td>
                                        <td style={{fontSize:"14px"}}> {amountFormatter(this.props.TTCTotalIDR, 'ttc')} </td>
                                    </tr>
                                } 

                                {!this.state.favorite.swipe && this.state.show.swipe &&
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.addToFavorite('SWIPE')}>
                                        <td style={{fontSize:"14px"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>SWIPE/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}> { cryptoFormatterByCoin( SWIPEBalance, 'SWIPE' ) } </td>
                                        <td style={{fontSize:"14px"}}> {amountFormatter(this.props.SWIPETotalIDR, 'swipe')} </td>
                                    </tr>
                                }

                                {!this.state.favorite.zil && this.state.show.zil &&
                                    <tr className="referral-details-table-row currency-dropdown-selector" style={{fontSize:"10px",cursor:"pointer"}} onClick={() => this.addToFavorite('ZIL')}>
                                        <td style={{fontSize:"14px"}}><FontAwesomeIcon icon={faStar} /></td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}>ZIL/IDR</td>
                                        <td style={{fontSize:"14px",fontWeight:"600"}}> { cryptoFormatterByCoin( ZILBalance, 'ZIL' ) } </td>
                                        <td style={{fontSize:"14px"}}> {amountFormatter(this.props.ZILTotalIDR, 'zil')} </td>
                                    </tr>
                                }

                            </table>
                        </div>
                    </ModalBody>
                </Modal>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        language: state.language
    };
};

export default connect(mapStateToProps, null) (WalletAdd);
