import * as React from 'react';
import './index.css';
import history from '../../history.js';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import { UncontrolledDropdown,Dropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap';
import async from 'async';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

// function getSymbolFromURL() {
// 	const regex = new RegExp('[\\?&]symbol=([^&#]*)');
// 	const results = regex.exec(window.location.search);
// 	return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, ' '));
// }

export class TVChartAdvancedContainer extends React.PureComponent {

	static defaultProps = {
		symbol: 'BTCIDR',
		interval: '15',
		containerId: 'tv_chart_container',
		datafeedUrl: process.env.REACT_APP_BASE_URL + '/tradingview',
		libraryPath: '/charting_library/',
		chartsStorageUrl: 'https://saveload.tradingview.com',
		chartsStorageApiVersion: '1.1',
		clientId: 'tradingview.com',
		userId: 'public_user_id',
		fullscreen: false,
		autosize: true,
		studiesOverrides: {}
	};

	constructor(){
        super();
        this.state = {
						resolution: '15',
						symbol: 'BTCIDR'
        }

    }

	getLanguage() {
		let results
		if (this.props.language == 'en') {
			return results = 'en'
		} else if (this.props.language == 'id') {
			return results = 'id_ID'
		}
	}

	// componentWillMount() {
	//     strings.setLanguage(this.props.language);
	// }

	componentDidMount() {
		this.setupChart();
	}

	async componentWillReceiveProps(props){
		
		if(this.state.symbol !== props.symbol){
				await this.setState({ 
						symbol: props.symbol
				})
				// window.tvWidget.remove();
				this.setupChart();
		}
}

	async setupChart(res) {
		if(res){
			res = res
		}else {
			res = "15"
		}
		const widgetOptions = {
			symbol: this.state.symbol,
			// BEWARE: no trailing slash is expected in feed URL
			datafeed: new window.Datafeeds.UDFCompatibleDatafeed(this.props.datafeedUrl),
			interval: res,
			container_id: this.props.containerId,
			library_path: this.props.libraryPath,
			timezone: "Asia/Bangkok",
			locale: this.getLanguage() || 'en',
			drawings_access: { type: 'black', tools: [{ name: "Regression Trend" }] },
			enabled_features: ['study_templates'],
			disabled_features: ["header_widget"],
			charts_storage_url: this.props.chartsStorageUrl,
			charts_storage_api_version: this.props.chartsStorageApiVersion,
			client_id: this.props.clientId,
			user_id: this.props.userId,
			fullscreen: this.props.fullscreen,
			autosize: this.props.autosize,
			studies_overrides: this.props.studiesOverrides,
			time_frames: [
				{ text: "50y", resolution: "6M", description: "50 Years" },
				{ text: "3y", resolution: "W", description: "3 Years", title: "3yr" },
				{ text: "8m", resolution: "D", description: "8 Month" },
				{ text: "3d", resolution: "5", description: "3 Days" },
				{ text: "1000y", resolution: "W", description: "All", title: "All" },
			],
			overrides: {
				"mainSeriesProperties.style": 1,
				"mainSeriesProperties.areaStyle.color1": "#86d29b",
				"mainSeriesProperties.areaStyle.color2": "#7bfcf8",
				"mainSeriesProperties.areaStyle.linecolor": "#1ba64b"
			}
		};
		window.tvWidget = new window.TradingView.widget(widgetOptions);
		

		window.tvWidget.onChartReady(() => {
			const button = window.tvWidget.createButton()
				.attr('title', 'Click to show a notification popup')
				.addClass('apply-common-tooltip')
				.on('click', () => window.tvWidget.showNoticeDialog({
					title: strings.NOTIFICATION,
					body: strings.ADVANCED_CHART_WORK_CORRECTLY,
					callback: () => {
						
					},
				}));

			button[0].innerHTML = strings.CHECK_STATUS;
		});
	}

	componentWillMount() {
		let self = this;
		strings.setLanguage(this.props.language);
		window.TradingView.onready(() => {
			
			self.setupChart();
		});
	}

	render() {

		return (
			<div>
				<div
				id={this.props.containerId}
				className={'TVChartContainer'}
				/>
				
				<div className="form-group" style={{position:"absolute",top:"72px",left:"170px"}}>
				
				<select onChange={(e) => this.setupChart(e.target.value)} class="form-control" style={{opacity:"0",cursor:"pointer"}}>
					<option selected="selected" hidden value="5">5</option>
					<option value="15">15</option>
					<option value="30">30</option>
					<option value="60">60</option>
					<option value="120">120</option>
					<option value="240">240</option>
					<option value="360">360</option>
					<option value="720">720</option>
				</select>
				</div>
			</div>
			
		);
	}
}

const mapStateToProps = (state) => {
	return {
		language: state.language
	};
};

const mapDispatchToProps = dispatch => ({

});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TVChartAdvancedContainer))
