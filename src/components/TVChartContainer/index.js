import * as React from 'react';
import './index.css';
import history from '../../history.js';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

// function getSymbolFromURL() {
// 	const regex = new RegExp('[\\?&]symbol=([^&#]*)');
// 	const results = regex.exec(window.location.search);
// 	return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, ' '));
// }

export class TVChartContainer extends React.PureComponent {

	static defaultProps = {
		symbol: 'BTCIDR',
		interval: '15',
		containerId: 'tv_chart_container',
		datafeedUrl: process.env.REACT_APP_BASE_URL + '/tradingview',
		libraryPath: '/charting_library/',
		chartsStorageUrl: 'https://saveload.tradingview.com',
		chartsStorageApiVersion: '1.1',
		clientId: 'tradingview.com',
		userId: 'public_user_id',
		fullscreen: false,
		autosize: true,
		studiesOverrides: {}
	};

	getLanguage() {
		let results
		if (this.props.language == 'en') {
			return results = 'en'
		} else if (this.props.language == 'id') {
			return results = 'id_ID'
		}
	}

	// componentWillMount() {
	//     strings.setLanguage(this.props.language);
	// }

	componentDidMount() {
		this.setupChart();
	}

	setupChart() {
		const widgetOptions = {
			symbol: this.props.symbol,
			// BEWARE: no trailing slash is expected in feed URL
			datafeed: new window.Datafeeds.UDFCompatibleDatafeed(this.props.datafeedUrl),
			interval: this.props.interval,
			container_id: this.props.containerId,
			library_path: this.props.libraryPath,
			timezone: "Asia/Bangkok",
			locale: this.getLanguage() || 'en',
			drawings_access: { type: 'black', tools: [{ name: "Regression Trend" }] },
			disabled_features: [
				"header_interval_dialog_button",
				'header_saveload',
				'header_compare',
				"left_toolbar",
				"header_widget",
				"control_bar",
				"border_around_the_chart",
				"save_chart_properties_to_local_storage"
			],
			enabled_features: ['study_templates'],
			charts_storage_url: this.props.chartsStorageUrl,
			charts_storage_api_version: this.props.chartsStorageApiVersion,
			client_id: this.props.clientId,
			user_id: this.props.userId,
			fullscreen: this.props.fullscreen,
			autosize: this.props.autosize,
			studies_overrides: this.props.studiesOverrides,
			favorites: {
				chartTypes: ["Area", "Candles"]
			},
			overrides: {
				"mainSeriesProperties.style": 3,
				"mainSeriesProperties.areaStyle.color1": "#86d29b",
				"mainSeriesProperties.areaStyle.color2": "#7bfcf8",
				"mainSeriesProperties.areaStyle.linecolor": "#1ba64b"
			}
		};

		const widget = window.tvWidget = new window.TradingView.widget(widgetOptions);

		widget.onChartReady(() => {
			const button = widget.createButton()
				.attr('title', 'Click to show a notification popup')
				.addClass('apply-common-tooltip')
				.on('click', () => widget.showNoticeDialog({
					title: strings.NOTIFICATION,
					body: strings.ADVANCED_CHART_WORK_CORRECTLY,
					callback: () => {
						
					},
				}));

			button[0].innerHTML = strings.CHECK_STATUS;
		});
	}

	componentWillMount() {
		let self = this;
		strings.setLanguage(this.props.language);
		window.TradingView.onready(() => {
			
			self.setupChart();
		});
	}

	render() {

		return (
			<div
				id={this.props.containerId}
				className={'TVChartContainer'}
			/>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		language: state.language
	};
};

const mapDispatchToProps = dispatch => ({

});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TVChartContainer))
