import React, {Component} from 'react';
import actions from '../../core/actions/client/home';
import history from '../../history'
import classnames from 'classnames'
import { connect } from 'react-redux';
import localStorage from 'localStorage';

class NotificationMessage extends Component {

	constructor() {
		super();
		this.state = {
			title: "",
			message: "",
			notificationEnabled: false
		}
	}

	getToken() {
		if(this.props.user){
			let username = this.props.user.username
			let clientId = this.props.user.pool.clientId
			let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
			return token
		}else{
			return 0
		}
	}

	getMessage() {
		actions.getNotifications(this.getToken()).then(res => {

			if(res.data.data.length !== 0 ){
				this.setState({
					title: res.data.data[res.data.data.length-1].Title,
					message: res.data.data[res.data.data.length-1].Message
				})
			}

			let setting = localStorage.getItem('notification');

			if(setting){
				let parsed = JSON.parse(setting);
				this.setState({
					notificationEnabled: parsed.enabled
				});
			} else {
				localStorage.setItem('notification', JSON.stringify({ enabled: true }));
				this.setState({
					notificationEnabled: true
				});
			}

		});
	}

	componentDidMount() {
		this.getMessage();
	}

	disableNotification() {
		localStorage.setItem('notification', JSON.stringify({ enabled: false }));
		this.setState({
			notificationEnabled: false
		});
	}

	render() {

		let classObject = {
			'm-alert m-alert--outline m-alert--square alert alert-dismissible fade show': true,
			'alert-info': true
		}

		return (
			<div>
				{
					this.state.notificationEnabled && this.state.message && <div className={classnames(classObject)} role="alert" style={{ marginTop: "20px" }}>
						<button onClick={() => this.disableNotification()} aria-label="Close" className="close" data-dismiss="alert" type="button"></button>
						<b>{this.state.title}</b> { this.state.message }
					</div>
				}
			</div>
		)
	}

}

const mapStateToProps = (state) => {
	return {
		user: state.cognito.user,
		kyc: state.kyc,
		language: state.language
	};
};

export default connect(mapStateToProps, null) (NotificationMessage);
