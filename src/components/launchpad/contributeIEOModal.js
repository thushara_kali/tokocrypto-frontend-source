import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from "react-redux";
import LaunchpadService from "../../core/actions/client/launchpad";
import { NotificationManager } from "react-notifications";
import EventBus from 'eventing-bus';
import uuid from 'uuid/v1';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
import ReactTooltip from 'react-tooltip';
import NetworkError from '../../errors/networkError'

/**
 * TCDOC-049 this component used for rendering modal for contribute
 */

class ContributeModal extends Component {

    constructor() {
        super();
        this.state = {
            contributeAmount: "",
            loading: false,
            usedCurrency: '',
            project: {},
            curr_list: [],
            modalShow: false
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            var re
            if(this.state.usedCurrency === 'BTC'){
                re = /^(\d+(\.\d{0,3})?)$/;
            }else if(this.state.usedCurrency === 'USDT'){
                re = /^[1-9]\d*$/;
            }else if(this.state.usedCurrency === 'USDC'){
                re = /^[1-9]\d*$/;
            }else if(this.state.usedCurrency === 'IDR'){
                re = /^[1-9]\d*$/;
            }else{
                re = /^(\d+(\.\d{0,2})?)$/;
            } 
        
            if (event.target.value == '' || re.test(event.target.value)) {
                obj[fieldName] = event.target.value
            }
            

            this.setState(
                obj
            )
        }
    }

    getToken() {
        let username = this.props.user.username;
        let clientId = this.props.user.pool.clientId;
        let token = this.props.user.storage[
            "CognitoIdentityServiceProvider." + clientId + "." + username + ".idToken"
        ];
        return token;
    }

    contribute() {
        let balance = 0;
        let minimum = 0;
        
        if (this.state.usedCurrency === "BTC") {
            minimum = 0;
            balance = this.props.btcBalance;
        } else if(this.state.usedCurrency === "ETH") {
            minimum = 0;
            balance = this.props.ethBalance;
        } else if(this.state.usedCurrency === "USDT") {
            minimum = 0;
            balance = this.props.usdtBalance;
        } else if(this.state.usedCurrency === "USDC") {
            minimum = 0;
            balance = this.props.usdcBalance;
        } else if(this.state.usedCurrency === "IDR") {
            minimum = 1;
            balance = this.props.idrBalance;
        }        

        if (Number(this.state.contributeAmount) > balance) {
            
            NotificationManager.error(`Insufficient amount, your balance is ${balance} ${this.state.usedCurrency}`);
        } else if (Number(this.state.contributeAmount) === 0) {
            NotificationManager.error(`Cannot contribute 0 ${this.state.usedCurrency}`);
        } else if (Number(this.state.contributeAmount) < minimum) {
            NotificationManager.error(`Minimum contribute amount is ${minimum} ${this.state.usedCurrency}`);
        }else {
            this.setState({
                loading: true,
                modalShow: false
            });

            let self = this;
            // this.props.toggle();
            
            confirmAlert({
                title: 'Submit Contribute',
                // message: 'test',
                message: `Are you sure to contribute ${Number(this.state.contributeAmount)} ${this.state.usedCurrency} in project ${this.props.contributedProject.projectName}`,
                buttons: [
                    {
                        label: 'Yes',
                        onClick: () => self.contribute_action()
                    },
                    {
                        label: 'No',
                        onClick: () => this.setState({
                            loading: false,
                            modalShow: true
                        })
                    }
                ]
            })

            
        }
    }

    contribute_action(){
        this.setState({
            modalShow: true
        });
        let data = {
            "amount" : Number(this.state.contributeAmount),
            "projectType" : this.props.contributedProject.projectType,
            "currency" : this.state.usedCurrency
        }
        LaunchpadService.contribute(this.getToken(), this.props.contributedProject.projectId, data).then(res => {
            this.setState({
                loading: false,
                // modalShow: false
            });
            this.props.toggle();

            NotificationManager.success("Contribution success");
            EventBus.publish("contributeSuccess");
        }).catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: contributeIEOModal | FUNC: contributeAction',
                url: `launchpad/client/project/${this.props.contributedProject.projectId}/contribute`
            }
            NetworkError(err, errorDetails);

            this.setState({
                loading: false
            });
            
            // if(err.response.data.error.message){
            //     let error = err.response.data.error.message
            //     NotificationManager.error("Error: " + error);
            // }else {
            //     NotificationManager.error(err.toString());
            // }
            
            EventBus.publish("contributeFailed");
        });
    }

    componentWillReceiveProps(newProps){
        if (newProps.contributedProject !== this.props.contributedProject) {
            this.setState({
                curr_list: newProps.contributedProject.currency,
                usedCurrency: newProps.contributedProject.currency[0],
                modalShow:true
            });
        }
    }

    onChangeCurrency(e) {
        this.setState({
            usedCurrency: e.target.value
        });
    }

    componentDidMount(){
        
    }

    render() {
        let balance = 0;

        let minimum = 0;
        
        if (this.state.usedCurrency === "BTC") {
            balance = this.props.btcBalance;
            // minimum = 0.001;
        } else if(this.state.usedCurrency === "ETH") {
            balance = this.props.ethBalance;
            // minimum = 0.01;
        } else if(this.state.usedCurrency === "USDT") {
            balance = this.props.usdtBalance;
            // minimum = 10;
        } else if(this.state.usedCurrency === "USDC") {
            balance = this.props.usdcBalance;
            // minimum = 10;
        } else if(this.state.usedCurrency === "IDR") {
            balance = this.props.idrBalance;
            // minimum = 200000;
        }
        
        return (
            <div>
                <Modal isOpen={this.props.open && this.state.modalShow} toggle={() => this.props.toggle()} className={this.props.className}>
                    <ModalHeader toggle={() => this.props.toggle()}>Contribute</ModalHeader>
                    <ModalBody>
                        <div className="m-alert m-alert--outline m-alert--square alert alert-dismissible fade show alert-info">
                            Your balance {balance} {this.state.usedCurrency}
                        </div>
                        <div className="form-group">
                            <label className="control-label">
                                Contribute Currency
                            </label>
                            <div className>
                                <select value={this.state.usedCurrency} disabled={this.state.loading} className="form-control" onChange={(e) => this.onChangeCurrency(e)}>
                                    {
                                        this.state.curr_list.map(c => {
                                            return (
                                                <option value={c} key={uuid()}>{c}</option>
                                            )
                                        })
                                    }
                                </select>
                            </div>
                            <div className="feedback">
                                You have contributed {this.props.contributedProject.currentContributeAmount} {this.props.contributedProject.selectedCurrecy} <br />
                                {/* {this.props.contributedProject.currentContributeAmount - this.props.contributedProject.contributeAmount} {this.props.contributedProject.selectedCurrecy} remaining
                                to complete the pledge. */}
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label">
                                Contribute Amount
                            </label>
                            <div className="input-group mb-3">
                                <input value={this.state.contributeAmount} onChange={this.handleChange('contributeAmount')} type="text" className="form-control" placeholder={minimum} aria-label="Recipient's username" aria-describedby="basic-addon2" />
                            </div>
                            <div className="feedback">
                                {/* changed */}
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button disabled={this.state.loading} color="secondary" onClick={() => this.props.toggle()}>Cancel</Button>{' '}
                        <Button disabled={this.state.loading} color="success" onClick={() => this.contribute()}>
                            {this.state.loading ? 'Contributing...' : 'Contribute'}
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

}

const mapStateToProps = state => {
    return {
        user: state.cognito.user,
        language: state.language
    };
};

export default connect(
    mapStateToProps,
    null
)(ContributeModal);