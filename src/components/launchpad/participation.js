import React, { Component } from 'react';
import classnames from 'classnames';
import { connect } from "react-redux";
import LaunchpadService from "../../core/actions/client/launchpad";
import networkError from '../../errors/networkError';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import moment from 'moment';
import _ from 'lodash';
import ContributeModal from './contributeModal';
import EventBus from 'eventing-bus';
import uuid from 'uuid/v1';
import cryptoFormatter, { cryptoFormatterByCoin } from '../../helpers/cryptoFormatter';
import amountFormatter from '../../helpers/amountFormatter';
import currencyFormatter from '../../helpers/currencyFormatter';
import { getUserAttributes } from '../../core/react-cognito/attributes.js'
import { resolve } from 'url';

/**
 * TCDOC-053 this component used for rendering participated ICO by user we split them into paging system
 * use the PER_PAGE const to change page setup
 */

const PER_PAGE = 5;

class Participation extends Component {

    constructor() {
        super();
        this.state = {
            activeTab: '1',
            openContribute: false,
            contributedProject: {},
            myPledge: [],
            myCompletedPledge: [],
            ongoingPage: 1,
            completedPage: 1,
            myCompletedPledgeMaster: [],
            myOngoingPledgeMaster: [],
            user_id:'',
            myOngoingPledgeTotal: 0,
            myCompletedPledgeTotal: 0,
            indexStart: 1,
            indexComplete: 1
        }
    }

    nextOngoing() {
        let newPage = this.state.ongoingPage + 1;
        let newIndex = this.state.indexStart + 5
        this.setState({
            myPledge: this.state.myOngoingPledgeMaster[newPage - 1],
            ongoingPage: newPage,
            indexStart: newIndex
        });
    }

    prevOngoing() {
        let newPage = this.state.ongoingPage - 1;
        let newIndex = this.state.indexStart - 5
        this.setState({
            myPledge: this.state.myOngoingPledgeMaster[newPage - 1],
            ongoingPage: newPage,
            indexStart: newIndex
        });
    }

    nextCompleted() {
        let newPage = this.state.completedPage + 1;
        let newIndex = this.state.indexComplete + 5
        this.setState({
            myCompletedPledge: this.state.myCompletedPledgeMaster[newPage - 1],
            completedPage: newPage,
            indexComplete:newIndex
        });
    }

    prevCompleted() {
        let newPage = this.state.completedPage - 1;
        let newIndex = this.state.indexComplete - 5
        this.setState({
            myCompletedPledge: this.state.myCompletedPledgeMaster[newPage - 1],
            completedPage: newPage,
            indexComplete:newIndex
        });
    }

    toggle(activeTab) {
        this.setState({
            activeTab: activeTab
        })
    }

    openContribute(project) {
        this.setState({
            openContribute: true,
            contributedProject: project
        });
    }

    closeContribute() {
        this.setState({
            openContribute: false
        });
    }

    async fetchMyPledge() {
        return new Promise((resolve, reject) => {

            LaunchpadService.myPledge(this.getToken()).then(res => {

                let myPledge = [];
                let combPledge = [];
                let user_id = ""

                // user_id = getUserAttributes(this.props.user).then(attr => {
                //     
                    
                //     return attr.sub;
                // })

                res.data.data.map(pl => {
                    let project = _.find(this.props.ongoing, { projectId: pl.projectId });
                    pl.selectedCurrecy = pl.currency || null
                    project = _.extend(pl, project);
                    myPledge.push(project);
                });

                myPledge.forEach(function (el) {
                    var findIndex = combPledge.findIndex(function (item) {
                        return item.projectId === el.projectId;
                    });
                    if (findIndex === -1) {
                        combPledge.push(el);
                    } else {
                        combPledge[findIndex].contributeAmount += el.contributeAmount;
                    }
                });
                
                combPledge.map(plx => {

                    if (_.has(plx, 'documentStatus') === false) {
                        let projectC = _.find(this.props.completed, { projectId: plx.projectId });
                        projectC = _.extend(projectC, plx);
                        let index = _.findIndex(combPledge, { projectId: plx.projectId });
                        combPledge[index] = projectC;
                    }
                });

                

                combPledge = _.filter(combPledge, { userId: this.state.user_id });
                let myCompletedPledge = _.filter(combPledge, { documentStatus: "completed" });
                // console.log('All',combPledge);
                // console.log('completed',myCompletedPledge);
                
                let myOngoingPledge = _.filter(combPledge, { documentStatus: "active" });

                // paginate the pledge
                let myOngoingPledgeMaster = myOngoingPledge;
                let myCompletedPledgeMaster = myCompletedPledge;
                if (myOngoingPledge.length !== 0) {
                    myOngoingPledgeMaster = _.chunk(myOngoingPledge, PER_PAGE);
                }
                if (myCompletedPledge.length !== 0) {
                    myCompletedPledgeMaster = _.chunk(myCompletedPledge, PER_PAGE);
                }

                let ongoingpage = [];
                if (myOngoingPledgeMaster.length !== 0) {
                    ongoingpage = myOngoingPledgeMaster[0]
                }
                let completedPage = [];
                if (myCompletedPledgeMaster.length !== 0) {
                    completedPage = myCompletedPledgeMaster[0]
                }

                resolve({
                    // allPledge: combPledge,
                    allPledge: combPledge,
                    myOngoingPledge : myOngoingPledge,
                    myCompletedPledge: myCompletedPledge,
                    // myPledge: ongoingpage,
                    // // myCompletedPledge: completedPage,
                    // myOngoingPledgeMaster,
                    // myCompletedPledgeMaster,
                    // myOngoingPledgeTotal: myOngoingPledge.length,
                    // myCompletedPledgeTotal: myCompletedPledge.length
                })
            }).catch(err => {
                let errorDetails = {
                    type: 'COMPONENTS: participation | FUNC: fetchMyPledge',
                    url: `/launchpad/client/pledge`
                }
                // NetworkError(err, errorDetails);

                reject(err);
            });
        });
    }

    async fetchMyContribution(){
        return new Promise((resolve, reject) => {

            LaunchpadService.contributionGroup(this.getToken()).then(res => {
                // 
                let myPledge = [];
                let allContribute = [];
                let myContribute = [];
                let compContribute = [];
                
                
                res.data.data.map(pl => {
                    let project = _.find(this.props.completed, { projectId: pl.projectId });
                    let projectOngoing = _.find(this.props.ongoing, { projectId: pl.projectId });
                    project = _.extend(pl, project);
                    projectOngoing = _.extend(project,projectOngoing);
                    allContribute.push(projectOngoing);
                });

                myContribute = _.filter(allContribute, { projectType: "IEO" });
                
                
                let myCompletedContribute = _.filter(myContribute, { documentStatus: "completed" });
                let myOngoingContrubute = _.filter(myContribute, { documentStatus: "active" });
                
                resolve({
                    allContribute: allContribute,
                    myContribute: myOngoingContrubute,
                    myCompletedContribute: myCompletedContribute
                })
                
            }).catch(err => {
                let errorDetails = {
                    type: 'COMPONENTS: participation | FUNC: fetchMyContribution',
                    url: `/launchpad/client/contribute/group`
                }
                if(this.props.user){
                    // NetworkError(err, errorDetails);
                }
                
                reject(err);
            });
        });
    }

    getToken() {
        if(this.props.user){
          let username = this.props.user.username;
          let clientId = this.props.user.pool.clientId;
          let token = this.props.user.storage[
            "CognitoIdentityServiceProvider." + clientId + "." + username + ".idToken"
          ];
          return token;
        }else{
          return 0
        }
      }

    componentWillReceiveProps(newProps) {
        
        if (Array.isArray(newProps.ongoing) && newProps.ongoing.length !== 0) {
            this.fetchOnGoing();
        }
        if (Array.isArray(newProps.completed) && newProps.completed.length !== 0) {
            this.fetchOnGoing();
        }
    }

    async fetchOnGoing(){
        let contribute = await this.fetchMyContribution();
        let pledge = await this.fetchMyPledge();
        let n_pledge = [];

        
        

        pledge.myOngoingPledge.forEach(e => {
            
            let project = _.find(contribute.allContribute, { projectId: e.projectId });
            
            project = _.extend(e, project);
            
            n_pledge.push(project);
        });

        pledge.myCompletedPledge.map(e => {
            
            let project = _.find(contribute.allContribute, { projectId: e.projectId });
            
            project = _.extend(e, project);
            return project
        });
        
        let ongoingList = pledge.myOngoingPledge.concat(contribute.myContribute)
        let completeList = pledge.myCompletedPledge.concat(contribute.myCompletedContribute)
        
        
        

        // pagination
        let myOngoingPledgeMaster = ongoingList.sort(function(a, b) {
            let dateA = new Date(a.contributionEndDate), dateB = new Date(b.contributionEndDate);
            return dateA - dateB;
        });
        let myCompletedPledgeMaster = completeList.sort(function(a, b) {
            let dateA = new Date(a.contributionEndDate), dateB = new Date(b.contributionEndDate);
            return dateA - dateB;
        });
        if (ongoingList.length !== 0) {
            myOngoingPledgeMaster = _.chunk(ongoingList, PER_PAGE);
        }
        if (completeList.length !== 0) {
            myCompletedPledgeMaster = _.chunk(completeList, PER_PAGE);
        }

        let ongoingpage = [];
        if (myOngoingPledgeMaster.length !== 0) {
            ongoingpage = myOngoingPledgeMaster[0]
        }
        let completedPage = [];
        if (myCompletedPledgeMaster.length !== 0) {
            completedPage = myCompletedPledgeMaster[0]
        }

        this.setState({
            myPledge: ongoingpage,
            myCompletedPledge: completedPage,
            myOngoingPledgeMaster,
            myCompletedPledgeMaster
        });

        this.setState({
            myOngoingPledgeTotal: ongoingList.length,
            myCompletedPledgeTotal: completeList.length
        })
    }

    componentDidMount() {
        let self = this;

        if(this.props.user){
            getUserAttributes(this.props.user).then(attr => {
                self.setState({
                    user_id: attr.sub
                })
            })
        }
        
        // this.fetchMyContribution();
        EventBus.on("contributeSuccess", () => {
            self.fetchOnGoing();
            self.setState({
                openContribute: false
            })
        });

        EventBus.on("joinPledge", () => {
            self.fetchOnGoing();
            self.setState({
                openContribute: false
            })
        });

        EventBus.on("contributeFailed", () => {
            self.setState({
                openContribute: false
            })
        });
    }

    render_pre(o, index,type) {

        let pledgeAmount = 0;
        if (o.selectedCurrecy === 'BTC') {
            pledgeAmount = cryptoFormatter(o.contributeAmount,3);
        } else if(o.selectedCurrecy === 'ETH') {
            pledgeAmount = cryptoFormatter(o.contributeAmount,2);
        } else if(o.selectedCurrecy === 'USDT') {
            pledgeAmount = cryptoFormatter(o.contributeAmount,0);
        } else if(o.selectedCurrecy === 'USDC') {
            pledgeAmount = cryptoFormatter(o.contributeAmount,0);
        } else if(o.selectedCurrecy === 'IDR') {
            pledgeAmount = currencyFormatter( amountFormatter(o.contributeAmount) );
        }

        let contributeAmount = 0;
        if (o.selectedCurrecy === 'BTC') {
            contributeAmount = cryptoFormatter(o.totalContributeAmountGroup,3);
        } else if(o.selectedCurrecy === 'ETH') {
            contributeAmount = cryptoFormatter(o.totalContributeAmountGroup,2);
        } else if(o.selectedCurrecy === 'USDT') {
            contributeAmount = cryptoFormatter(o.totalContributeAmountGroup,0);
        } else if(o.selectedCurrecy === 'USDC') {
            contributeAmount = cryptoFormatter(o.totalContributeAmountGroup,0);
        } else if(o.selectedCurrecy === 'IDR') {
            contributeAmount = currencyFormatter( amountFormatter(o.totalContributeAmountGroup) );
        }
        
        return (
            <tr key={uuid()}>
                {type === 1 ?
                    <td>{index + this.state.indexStart}.</td> :
                    <td>{index + this.state.indexComplete}.</td>
                }
                <td>
                    <div className="row">
                        <img style={{ width: 25, height: 25 }} src={o.logo} />
                        <span style={{ marginLeft: "5px" }}>{o.projectName}</span>
                    </div>
                </td>
                <td>{
                    moment(o.contributionEndDate, "YYYY-MM-DD").format("YYYY-MM-DD") === "Invalid date" ? '-' :
                        moment(o.contributionEndDate, "YYYY-MM-DD").format("YYYY-MM-DD")
                }</td>
                {o.selectedCurrecy === 'BTC' ?
                    <td>{pledgeAmount} {o.selectedCurrecy}</td>
                : null }
                {o.selectedCurrecy === 'ETH' ?
                    <td>{pledgeAmount} {o.selectedCurrecy}</td>
                : null }
                {o.selectedCurrecy === 'USDT' ||  o.selectedCurrecy === 'USDC'?
                    <td>{pledgeAmount} {o.selectedCurrecy}</td>
                : null }
                {o.selectedCurrecy === 'IDR'?
                    <td>{pledgeAmount}</td>
                : null }
                {/* <td>{cryptoFormatterByCoin(o.contributeAmount, o.selectedCurrecy)} {o.selectedCurrecy}</td> */}
                <td>{
                    moment(o.releaseEndDate, "YYYY-MM-DD").format("YYYY-MM-DD") === "Invalid date" ? '-' :
                        moment(o.releaseEndDate, "YYYY-MM-DD").format("YYYY-MM-DD")
                }</td>
                {/* <td>{o.currentContributeAmount} {o.currency[0]}</td> */}
                {o.selectedCurrecy === 'BTC' ?
                    <td>{contributeAmount} {o.selectedCurrecy}</td>
                : null }
                {o.selectedCurrecy === 'ETH' ?
                    <td>{contributeAmount} {o.selectedCurrecy}</td>
                : null }
                {o.selectedCurrecy === 'USDT' ||  o.selectedCurrecy === 'USDC'?
                    <td>{contributeAmount} {o.selectedCurrecy}</td>
                : null }
                {o.selectedCurrecy === 'IDR'?
                    <td>{ contributeAmount }</td>
                : null }
                <td>{cryptoFormatter(o.totalContributeAmountUSD,0)} USD</td> 
                <td>{cryptoFormatter(o.totalTokensToBeReceivedGroup,0)}</td>
                {type === 1 && <td>
                    {o.allowContributing && contributeAmount !== pledgeAmount ? <button onClick={() => this.openContribute(o)} className="btn m-btn--pill btn-sm btn-outline-info">
                        Contribute
                    </button> : "-"}
                </td>}
            </tr>

        )
    }

    render_ieo(o, index,type) {
        
        return (
            <tr key={uuid()}>
                {type === 1 ?
                    <td>{index + this.state.indexStart}.</td> :
                    <td>{index + this.state.indexComplete}.</td>
                }
                <td>
                    <div className="row">
                        <img style={{ width: 25, height: 25 }} src={o.logo} />
                        <span style={{ marginLeft: "5px" }}>{o.projectName}</span>
                    </div>
                </td>
                <td>{
                    moment(o.contributionEndDate, "YYYY-MM-DD").format("YYYY-MM-DD") === "Invalid date" ? '-' :
                        moment(o.contributionEndDate, "YYYY-MM-DD").format("YYYY-MM-DD")
                }</td>
                <td>-</td>
                <td>{
                    moment(o.releaseEndDate, "YYYY-MM-DD").format("YYYY-MM-DD") === "Invalid date" ? '-' :
                        moment(o.releaseEndDate, "YYYY-MM-DD").format("YYYY-MM-DD")
                }</td>
                {/* <td>{o.currentContributeAmount} {o.currency[0]}</td> */}
                <td></td>
                <td>{cryptoFormatter(o.totalContributeAmountUSD,0)} USD</td> 
                <td>{cryptoFormatter(o.totalTokensToBeReceivedGroup,0)}</td>
                {type === 1 && <td>
                    -
                </td>}
            </tr>

        )
    }

    render() {
        return (
            <div>
                <div className="m-portlet m-portlet--mobile">
                    <div className="m-portlet__body launchpad-tab">
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '1' })}
                                    onClick={() => { this.toggle('1'); }}
                                >
                                    Ongoing Projects&nbsp;
                                <span className={classnames({ 'm-badge': true, 'm-badge--info': this.state.activeTab === '1', 'm-badge--default': this.state.activeTab !== '1' })}>{this.state.myOngoingPledgeTotal}</span>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === '2' })}
                                    onClick={() => { this.toggle('2'); }}
                                >
                                    Completed Projects&nbsp;
                                <span className={classnames({ 'm-badge': true, 'm-badge--info': this.state.activeTab === '2', 'm-badge--default': this.state.activeTab !== '2' })}>{this.state.myCompletedPledgeTotal}</span>
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab} style={{overflowX : 'auto'}}>
                            <TabPane tabId="1">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th style={{minWidth : '140px'}}>ICO Name</th>
                                            <th>Ending Date</th>
                                            <th>Pledge</th>
                                            <th>Release Date</th>
                                            <th>Contributed</th>
                                            <th style={{minWidth : '100px'}}>Contributed<br />(USD Value)</th>
                                            <th style={{minWidth : '80px'}}>Tokens Receiving</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.myPledge.map((o, index) => {
                                                if(o.projectType === "IEO"){
                                                    return this.render_ieo(o,index,1)
                                                }else{
                                                    return this.render_pre(o,index,1)
                                                }
                                            })
                                        }
                                    </tbody>
                                </table>

                                <div className="row">
                                    <div className="col-12">
                                        <button onClick={() => this.prevOngoing()} disabled={this.state.ongoingPage === 1} className="btn btn-sm btn-success">Prev</button>
                                        <button disabled={this.state.myOngoingPledgeMaster.length === this.state.ongoingPage} onClick={() => this.nextOngoing()} style={{ marginLeft: 10 }} className="btn btn-sm btn-success">Next</button>
                                    </div>
                                </div>
                            </TabPane>
                            <TabPane tabId="2">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th style={{minWidth : '140px'}}>ICO Name</th>
                                            <th>Ending Date</th>
                                            <th>Pledge</th>
                                            <th>Release Date</th>
                                            <th>Contributed</th>
                                            <th style={{minWidth : '100px'}}>Contributed<br />(USD Value)</th>
                                            <th style={{minWidth : '80px'}}>Tokens Receiving</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.myCompletedPledge.map((c, cIndex) => {
                                                if(c.projectType === "IEO"){
                                                    return this.render_ieo(c,cIndex,2)
                                                }else{
                                                    return this.render_pre(c,cIndex,2)
                                                }
                                            })
                                        }
                                    </tbody>
                                </table>

                                <div className="row">
                                    <div className="col-12">
                                        <button onClick={() => this.prevCompleted()} disabled={this.state.completedPage === 1} className="btn btn-sm btn-success">Prev</button>
                                        <button disabled={this.state.myCompletedPledgeMaster.length === this.state.completedPage} onClick={() => this.nextCompleted()} style={{ marginLeft: 10 }} className="btn btn-sm btn-success">Next</button>
                                    </div>
                                </div>
                            </TabPane>
                        </TabContent>
                    </div>
                </div>

                <ContributeModal
                    btcBalance={this.props.btcBalance}
                    ethBalance={this.props.ethBalance}
                    usdtBalance={this.props.usdtBalance}
                    usdcBalance={this.props.usdcBalance}
                    idrBalance={this.props.idrBalance}
                    open={this.state.openContribute}
                    toggle={() => this.closeContribute()}
                    contributedProject={this.state.contributedProject}
                />

            </div>
        )
    }

}

const mapStateToProps = state => {
    return {
        user: state.cognito.user,
        language: state.language
    };
};

export default connect(
    mapStateToProps,
    null
)(Participation);
