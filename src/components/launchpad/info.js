import React, { Component } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';

/**
 * TCDOC-051 this component used for rendering info footer there is not much going on here
 * just view separation for easier debugging
 */

class LaunchpadInfo extends Component {

    constructor() {
        super();
        this.state = {
            activeTab: '1'
        }
    }

    toggle(activeTab) {
        this.setState({
            activeTab: activeTab
        })
    }

    openQnA() {
        window.open('https://support.tokocrypto.com/hc/en-us/sections/360001122352-FAQ', '_blank');
    }

    render() {
        return (
            <div>
                <div className="m-portlet m-portlet--mobile">
                    <div className="m-portlet__body launchpad-tab">
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                className={classnames({ active: this.state.activeTab === '1' })}
                                onClick={() => { this.toggle('1'); }}
                                >
                                Contact Us
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                className={classnames({ active: this.state.activeTab === '2' })}
                                onClick={() => this.openQnA()}
                                >
                                FAQ
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-lg-8">
                                            <p>Here you can find more information, Whether you are an experienced user or just starting out, we invite you to consider joining our conversation channel.</p>
                                        </div>
                                        <div className="col-lg-4">
                                            <h6 className="blue-tc">Join Our Conversation</h6>
                                            <div className="container">
                                                <a href="https://t.me/joinchat/FhHIjBIBaIhaWe3qFE1ddA" target="_blank">
                                                    <div className="row">
                                                        <img style={{ width: 20, height: 20}} src="/assets/media/img/logo/telegram.png" />&nbsp;
                                                        <span className="blue-tc">tokocrypto_launchpad</span>
                                                    </div>
                                                </a>
                                                {/* <div className="row" style={{ marginTop: "5px"}}>
                                                    <img style={{ width: 20, height: 20}} src="/assets/media/img/logo/wa.png" />&nbsp;
                                                    <span className="blue-tc">tokocrypto_launchpad</span>
                                                </div> */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </TabPane>
                        </TabContent>
                    </div>
                </div>
            </div>
        )
    }

}

export default LaunchpadInfo;