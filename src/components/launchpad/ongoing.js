import React, { Component } from 'react';
import moment from 'moment';
import { connect } from "react-redux";
import { NotificationManager } from 'react-notifications'
import history from '../../history';
import launchpadService from '../../core/actions/client/launchpad';
import EventBus from 'eventing-bus';
import uuid from 'uuid/v1';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
import ReactTooltip from 'react-tooltip';
import ContributeModal from './contributeIEOModal';
import NetworkError from '../../errors/networkError';
import cryptoFormatter, { cryptoFormatterByCoin } from '../../helpers/cryptoFormatter';
import amountFormatter from '../../helpers/amountFormatter';

/**
 * TCDOC-052 this component used for rendering ongoing ICO there is not much going on here
 * just view separation for easier debugging
 */

class OnGoingICO extends Component {

    constructor() {
        super();
        this.state = {
            usedCurrency: "",
            loading: false,
            amount: "",
            currencies: [],
            disabled: false,
            openContribute: false,
            contributedProject: {}
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            var re
            if(this.state.usedCurrency === 'BTC'){
                re = /^(\d+(\.\d{0,3})?)$/;
            }else if(this.state.usedCurrency === 'USDT'){
                re = /^[1-9]\d*$/;
            }else if(this.state.usedCurrency === 'USDC'){
                re = /^[1-9]\d*$/;
            }else if(this.state.usedCurrency === 'IDR'){
                re = /^[1-9]\d*$/;
            }else{
                re = /^(\d+(\.\d{0,2})?)$/;
            } 
        
            if (event.target.value == '' || re.test(event.target.value)) {
                obj[fieldName] = event.target.value
            }

            this.setState(
                obj
            )
        }
    }

    openReview(projectId) {
        history.push('/launchpad/review/' + projectId);
    }

    openQnA(projectId) {
        history.push('/launchpad/review/' + projectId + '?qna=true');
    }

    componentDidMount() {
        this.setState({
            usedCurrency: this.props.project.currency[0],
            currencies: this.props.project.currency
        })
        this.getReview();
    }

    // getToken() {
    //     let username = this.props.user.username;
    //     let clientId = this.props.user.pool.clientId;
    //     let token = this.props.user.storage[
    //         "CognitoIdentityServiceProvider." + clientId + "." + username + ".idToken"
    //     ];
    //     return token;
    // }

    getToken() {
        if(this.props.user){
          let username = this.props.user.username;
          let clientId = this.props.user.pool.clientId;
          let token = this.props.user.storage[
            "CognitoIdentityServiceProvider." + clientId + "." + username + ".idToken"
          ];
          return token;
        }else{
          return 0
        }
      }

    doPledge() {
        let balance = 0;
        let valid = false;
        let minimum = 0;

        if (this.state.usedCurrency === 'BTC') {
            balance = this.props.btcBalance;
            minimum = 0.001
        } else if(this.state.usedCurrency === 'ETH') {
            balance = this.props.ethBalance;
            minimum = 0.01
        } else if(this.state.usedCurrency === 'USDT') {
            balance = this.props.usdtBalance;
            minimum = 10
        } else if(this.state.usedCurrency === 'USDC') {
            balance = this.props.usdcBalance;
            minimum = 10
        } else if(this.state.usedCurrency === 'IDR') {
            balance = this.props.idrBalance;
            minimum = 200000
        }

        if ((Number(this.state.amount) * (10 / 100)) > balance) {
            NotificationManager.error('At least have 10% balance from pledged amount');
            valid = false;
        } else {
            if (Number(this.state.amount) < minimum) {
                valid = false;
                NotificationManager.error(`Minimum pledged amount is ${minimum} ${this.state.usedCurrency}`);
            } else {
                valid = true;
            }
        }

        if (valid) {
            this.setState({
                loading: true
            });

            let self = this;

            confirmAlert({
                title: 'Submit Pledge',
                message: `Are you sure to pledge ${Number(this.state.amount)} ${this.state.usedCurrency} in project ${this.props.project.projectName}`,
                // buttons: [
                //     {
                //         label: 'Yes',
                //         onClick: () => self.pledge()
                //     },
                //     {
                //         label: 'No',
                //         onClick: () => this.setState({
                //             loading: false
                //         })
                //     }
                // ],
                customUI: ({ title, message, onClose }) => 
                    <div className="react-confirm-alert">
                        <div className="react-confirm-alert-body">
                            <h1>{title}</h1><span style={{fontFamily:"Roboto"}}>{message}</span>
                            <div className="react-confirm-alert-button-group">
                                <button onClick={() => {
                                    self.pledge();
                                    onClose()
                                }}>Yes</button>
                                <button onClick={() => {
                                     this.setState({loading: false})
                                    onClose()
                                }}>No</button>
                            </div>
                            <div style={{fontWeight:"600", fontFamily:"Roboto", textDecoration:"underline", fontSize:"11px", marginTop:"12px", cursor:"pointer"}} data-tip data-for='agreement_note'>
                                Agreement note 
                            </div>
                            <ReactTooltip data-border='true' id="agreement_note" place="bottom" effect="solid">
                                <span style={{fontFamily:"Roboto"}}>10% of what User’s pledged will be deducted from User’s account as deposit.<br/> Deposit will be returned to User once User has contributed as per pledged</span>
                            </ReactTooltip>
                        </div>
                    </div>
            })
        }
    }

    openLogin() {
        // window.open(this.props.project.website, '_blank');
        history.push('/login');
    }

    pledge() {
        launchpadService.pledgeProject(this.props.token, this.props.project.projectId, Number(this.state.amount), this.state.usedCurrency).then(res => {
            NotificationManager.success('Pledge success, you can view your pledge history under “Your Participation”');
            EventBus.publish("joinPledge");
            this.setState({
                loading: false,
                amount: ""
            });
        }).catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: contributeIEOModal | FUNC: contributeAction',
                url: `launchpad/client/project/${this.props.contributedProject.projectId}/pledge`
            }
            NetworkError(err, errorDetails);

            // if (err.response.data) {
            //     NotificationManager.error(err.response.data.error.message);
            // } else {
            //     NotificationManager.error(err.toString());
            // }
            this.setState({
                loading: false,
                amount: ""
            });
        });
    }

    onChangeCurrency(e) {
        
        this.setState({
            usedCurrency: e.target.value
        });
    }

    openContribute(project) {
        
        this.setState({
            openContribute: true,
            contributedProject: project
        });
    }

    closeContribute() {
        this.setState({
            openContribute: false
        });
    }

    getReview() {

        if(this.props.project.projectId){
            let id = this.props.project.projectId

            launchpadService.getReview(this.getToken(), id)
              .then(res => {

                let reviewData = res.data.data
              
              })
              .catch((err) => {
                let errorDetails = {
                    type: 'COMPONENTS: contributeIEOModal | FUNC: contributeAction',
                    url: `launchpad/client/project/${id}/review`
                }
                NetworkError(err, errorDetails);

                // NotificationManager.error('Review not found');
                this.setState({
                    disabled: true
                });
                
              });
        }else{
            
        }
    }

    render() {

        let expired = moment(this.props.project.pledgeEndDate).diff(moment(), 'days') < 0;
        let disable_Contribute = this.props.project.allowContributing && Number(this.props.project.contributeAmount) !== Number(this.props.project.currentContributeAmount)

        let minimum = 0;
        if (this.state.usedCurrency === 'BTC') {
            minimum = 0.001;
        } else if(this.state.usedCurrency === 'ETH') {
            minimum = 0.01;
        } else if(this.state.usedCurrency === 'USDT') {
            minimum = 10;
        } else if(this.state.usedCurrency === 'USDC') {
            minimum = 10;
        } else if(this.state.usedCurrency === 'IDR') {
            minimum = 200000;
        }

        let percent = 0;
        if(this.props.project.progress){
            percent = this.props.project.progress
        }

        // let hardCapText = this.props.project.hardCapAmount +' '+ this.props.project.hardCapCurrency
        let hardCapText = ''
        let hardCapAmount = this.props.project.hardCapAmount || 0;
        let hardCapCurrency = this.props.project.hardCapCurrency || "USD";
        if(hardCapCurrency === "IDR" || hardCapCurrency === "USD"){
            hardCapText = amountFormatter(hardCapAmount) +' '+ hardCapCurrency
        }else{
            hardCapText = cryptoFormatterByCoin(hardCapAmount,hardCapCurrency) +' '+ hardCapCurrency
        }


        return (
            <div style={{ marginTop: "6%" }}>
                <div className="launchpad-ongoing-wrapper">

                    <div className="launchpad-ongoing-category">
                        Application
                    </div>

                    {(Number(this.props.project.bonus) !== 0 && !isNaN(Number(this.props.project.bonus))) && <div className="launchpad-ongoing-ribbon">
                        <div className="launchpad-ongoing-ribbon-text-wrapper">
                            <p className="bold">{Number(this.props.project.bonus)} %</p>
                            <p>BONUS</p>
                        </div>
                    </div>}
                    {(Number(this.props.project.bonus) !== 0 && !isNaN(Number(this.props.project.bonus))) && <div className="arrow-up"></div>}
                    <div className="container flex-center" style={{ marginTop: "35px" }}>
                        <img style={{ maxWidth: '100%', height: 'auto' }} src={this.props.project.logo} />
                    </div>

                    <div className="launchpad-ongoing-info">
                        <h6 className="bold blue-tc">{this.props.project.projectName}</h6>
                        <p className="">{this.props.project.shortDescription}</p>
                        <div className="row">
                            <div className="col-5" style={{ marginTop: "15px" }}>
                                <p className="muted smaller-text lh-small">{moment(this.props.project.pledgeEndDate, "YYYY-MM-DD").format("YYYY-MM-DD")}</p>
                                <p className="muted smaller-text lh-small">{moment(this.props.project.pledgeEndDate, "YYYY-MM-DD").fromNow()}</p>
                            </div>
                            <div className="col-7 launchpad-ongoing-btn-row-info">
                                <button className="btn btn-default" disabled={this.state.disabled} onClick={() => this.openReview(this.props.project.projectId)}>Review</button>
                                <button className="btn btn-default" disabled={this.state.disabled} onClick={() => this.openQnA(this.props.project.projectId)}>Q&A</button>
                            </div>
                        </div>
                    </div>

                    {this.props.project.projectType === 'IEO' && <div className="launchpad-ongoing-info">
                        <div style={{display:"flex"}}>
                            <div className="col-6"></div>
                            <div className="col-6" style={{display:"flex", justifyContent:"flex-end", padding:"0"}}>
                                <p className="muted smaller-text lh-small" style={{marginBottom:"8px"}}>Hard Cap</p>
                            </div>
                        </div>
                        <div style={{border:"1px solid #5cb85c",padding:"3px", borderRadius:".5rem"}}>
                            <div className="progress">
                                <div className="progress-bar-medium bg-success progress-bar-animated progress-bar-striped" style={{ width: percent + '%' }} >
                                    <p className="smaller-text" style={{color:"#fff"}}>{percent}%</p>
                                </div>
                            </div>
                        </div>
                        <div style={{display:"flex"}}>
                            <div className="col-6"></div>
                            <div className="col-6" style={{display:"flex", justifyContent:"flex-end", padding:"0"}}>
                                <p className="muted smaller-text lh-small" style={{marginTop:"8px"}}>{hardCapText}</p>
                            </div>
                        </div>
                    </div>}
                </div>
                <div className="launchpad-ongoing-footer">
                    {this.props.user?
                        <div className="row">
                            {this.props.project.projectType != 'IEO' ?
                                <div style={{display:"inline-flex"}}>
                                    <div className="col-4" style={{ padding: "2px" }}>
                                        <input value={this.state.amount} onChange={this.handleChange('amount')} disabled={expired || this.state.loading} className="form-control" placeholder={minimum} />
                                    </div>
                                    <div className="col-4" style={{ padding: "2px" }}>
                                        <select value={this.state.usedCurrency} disabled={expired || this.state.loading} className="form-control" onChange={(e) => this.onChangeCurrency(e)} style={{fontSize:"0.85rem"}} >
                                            {
                                                this.state.currencies.map(c => {
                                                    
                                                    return (
                                                        <option value={c} key={uuid()}>{c}</option>
                                                    )
                                                })
                                            }
                                        </select>
                                    </div>
                                    <div className="col-4" style={{ padding: "2px" }}>
                                        <button onClick={() => this.doPledge()} disabled={expired || this.state.loading} className="btn btn-success btn-block">
                                            {this.state.loading ? 'Joining' : 'Join'}
                                        </button>
                                    </div>
                                </div>:
                                <div style={{width:"100%"}}>
                                    <div className="col-12" style={{ padding: "2px" }}>
                                        <button onClick={() => this.openContribute(this.props.project)} disabled={!disable_Contribute || this.state.loading} className="btn btn-success btn-block">
                                            {this.state.loading ? 'Contributing' : 'Contribute'}
                                        </button>
                                    </div>
                                </div>
                            }
                           
                        </div>:
                        <div className="row">
                            <div className="col-12" style={{ padding: "2px" }}>
                            {this.props.project.projectType === 'IEO' ?
                                <button onClick={() => this.openLogin()} disabled={this.state.loading} className="btn btn-success btn-block">
                                    Login to Contribute
                                </button> :
                                <button onClick={() => this.openLogin()} disabled={this.state.loading} className="btn btn-success btn-block">
                                    Login to Pledge
                                </button>
                            }
                            </div>
                        </div>
                    }
                   
                </div>
                <ContributeModal
                    btcBalance={this.props.btcBalance}
                    ethBalance={this.props.ethBalance}
                    usdtBalance={this.props.usdtBalance}
                    usdcBalance={this.props.usdcBalance}
                    idrBalance={this.props.idrBalance}
                    open={this.state.openContribute}
                    toggle={() => this.closeContribute()}
                    contributedProject={this.state.contributedProject}
                />
            </div>
        )
    }

}
const mapStateToProps = state => {
    return {
        user: state.cognito.user,
        language: state.language
    };
};

export default connect(
    mapStateToProps,
    null
)(OnGoingICO);

// export default OnGoingICO;