import React, { Component } from 'react';
import moment from 'moment';
import history from '../../history';

/**
 * TCDOC-048 this component used for rendering completed ICO there is not much going on here
 * just view separation for easier debugging
 */

class CompletedICO extends Component {

    constructor() {
        super();
    }

    openReview(projectId) {
        history.push(`/launchpad/review/${projectId}`);
    }

    openQnA(projectId) {
        history.push(`/launchpad/review/${projectId}?qna=true`);
    }

    render() {
        return (
            <div style={{ marginTop: "6%" }}>
                <div className="launchpad-ongoing-wrapper">

                    <div className="launchpad-ongoing-category">
                        Application
                    </div>

                    {this.props.project.bonus !== 0 && <div className="launchpad-ongoing-ribbon">
                        <div className="launchpad-ongoing-ribbon-text-wrapper">
                            <p className="bold">{this.props.project.bonus} %</p>
                            <p>DISC</p>
                        </div>
                    </div>}
                    {this.props.project.bonus !== 0 && <div className="arrow-up"></div>}
                    <div className="container flex-center" style={{ marginTop: "35px" }}>
                        <img style={{ width: 50, height: 50 }} src={this.props.project.logo} />
                    </div>

                    <div className="launchpad-ongoing-info">
                        <h6 className="bold blue-tc">{this.props.project.projectName}</h6>
                        <p className="">{this.props.project.shortDescription}</p>
                        <div className="row">
                            <div className="col-5" style={{ marginTop: "15px" }}>
                                <p className="muted smaller-text lh-small">{moment(this.props.project.contributionEndDate, "YYYY-MM-DD").format('YYYY-MM-DD ')}</p>
                                <p className="muted smaller-text lh-small">completed</p>
                            </div>
                            <div className="col-7 launchpad-ongoing-btn-row-info">
                                <button className="btn btn-default" onClick={() => this.openReview(this.props.project.projectId)}>Review</button>
                                <button className="btn btn-default" onClick={() => this.openQnA(this.props.project.projectId)}>Q&A</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="launchpad-ongoing-footer">
                    <div className="row">
                        <div className="col-12 flex-center">
                            <span>Release Date {
                                moment(this.props.project.releaseEndDate, "YYYY-MM-DD").format('YYYY-MM-DD ') === 'Invalid date' ?
                                    '-' : moment(this.props.project.releaseEndDate, "YYYY-MM-DD").format('YYYY-MM-DD ')
                            }
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default CompletedICO;