import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from "react-redux";
import LaunchpadService from "../../core/actions/client/launchpad";
import { NotificationManager } from "react-notifications";
import EventBus from 'eventing-bus';
import ReactTooltip from 'react-tooltip';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
import cryptoFormatter, { cryptoFormatterByCoin } from '../../helpers/cryptoFormatter';
import {Decimal} from 'decimal.js';
import NP from 'number-precision';
import NetworkError from '../../errors/networkError';

/**
 * TCDOC-049 this component used for rendering modal for contribute
 */

class ContributeModal extends Component {

    constructor() {
        super();
        this.state = {
            contributeAmount: "",
            loading: false,
            modalShow: false
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            var re
            if(this.props.contributedProject.selectedCurrecy === 'BTC'){
                re = /^(\d+(\.\d{0,3})?)$/;
            }else if(this.props.contributedProject.selectedCurrecy === 'USDT'){
                re = /^[1-9]\d*$/;
            }else if(this.props.contributedProject.selectedCurrecy === 'USDC'){
                re = /^[1-9]\d*$/;
            }else if(this.props.contributedProject.selectedCurrecy === 'IDR'){
                re = /^[1-9]\d*$/;
            }else{
                re = /^(\d+(\.\d{0,2})?)$/;
            } 
        
            if (event.target.value == '' || re.test(event.target.value)) {
                obj[fieldName] = event.target.value
            }
            

            this.setState(
                obj
            )
        }
    }

    getToken() {
        let username = this.props.user.username;
        let clientId = this.props.user.pool.clientId;
        let token = this.props.user.storage[
            "CognitoIdentityServiceProvider." + clientId + "." + username + ".idToken"
        ];
        return token;
    }

    contribute() {
        let balance = 0;
        let minimum = 0;
        
        if (this.props.contributedProject.selectedCurrecy === "BTC") {
            balance = this.props.btcBalance;
            minimum = 0;
        } else if(this.props.contributedProject.selectedCurrecy === "ETH") {
            balance = this.props.ethBalance;
            minimum = 0;
        } else if(this.props.contributedProject.selectedCurrecy === "USDT") {
            balance = this.props.usdtBalance;
            minimum = 0;
        } else if(this.props.contributedProject.selectedCurrecy === "USDC") {
            balance = this.props.usdcBalance;
            minimum = 0;
        } else if(this.props.contributedProject.selectedCurrecy === "IDR") {
            balance = this.props.idrBalance;
            minimum = 1;
        }

        let contributeAmount = this.props.contributedProject.contributeAmount || 0
        let totalContributeAmount = this.props.contributedProject.totalContributeAmountGroup || 0
        let contributeAmountFinal =  contributeAmount || 0

        let tempAmount = NP.plus(contributeAmount,totalContributeAmount)
        let tempAmount1 = NP.minus(tempAmount,contributeAmountFinal)

        if (Number(this.state.contributeAmount) > balance) {
            
            NotificationManager.error(`Insufficient amount, your balance is ${balance} ${this.props.contributedProject.selectedCurrecy}`);
        } else if (Number(this.state.contributeAmount) === 0) {
            NotificationManager.error(`Cannot contribute 0 ${this.props.contributedProject.selectedCurrecy}`);
        } else if (tempAmount1 < 0) {
            NotificationManager.error('Cannot contribute more than your pledge');
        } else {
            this.setState({
                loading: true,
                modalShow: false
            });

            let self = this;
            // this.props.toggle();
            
            confirmAlert({
                title: 'Submit Contribute',
                // message: 'test',
                message: `Are you sure to contribute ${Number(this.state.contributeAmount)} ${this.props.contributedProject.selectedCurrecy} in project ${this.props.contributedProject.projectName}`,
                buttons: [
                    {
                        label: 'Yes',
                        onClick: () => self.contribute_action()
                    },
                    {
                        label: 'No',
                        onClick: () => this.setState({
                            loading: false,
                            modalShow: true
                        })
                    }
                ]
            })

            
        }
    }

    contribute_action(){
        this.setState({
            modalShow: true
        });

        let data = {
            "amount" : Number(this.state.contributeAmount),
            "projectType" : this.props.contributedProject.projectType
        }

        LaunchpadService.contribute(this.getToken(), this.props.contributedProject.projectId, data).then(res => {
            this.setState({
                loading: false
            });
            NotificationManager.success("Contribution success");
            EventBus.publish("contributeSuccess");
        }).catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: contributeModal | FUNC: contributeAction',
                url: `launchpad/client/project/${this.props.contributedProject.projectId}/contribute`
            }
            NetworkError(err, errorDetails);

            this.setState({
                loading: false
            });
            // if(err.response.data.error.message){
            //     let error = err.response.data.error.message
            //     NotificationManager.error("Error: " + error);
            // }else {
            //     NotificationManager.error(err.toString());
            // }
        });
    }

    componentWillReceiveProps(newProps){
        if (newProps.contributedProject !== this.props.contributedProject) {
            this.setState({
                modalShow:true
            });
        }
    }

    render() {
        let balance = 0;
        let minimum = 0;
        let contributed = 0;
        let left_contributed = 0;
        let contributeAmount = 0;
        let totalContributeAmount = 0;
        if(this.props.contributedProject.contributeAmount){
            contributeAmount = this.props.contributedProject.contributeAmount
        }
        
        if(this.props.contributedProject.totalContributeAmountGroup){
            totalContributeAmount = this.props.contributedProject.totalContributeAmountGroup
        }
        left_contributed = contributeAmount - totalContributeAmount
        
        if (this.props.contributedProject.selectedCurrecy === "BTC") {
            balance = this.props.btcBalance;
            // minimum = 0.001;
            left_contributed = cryptoFormatterByCoin(left_contributed,this.props.contributedProject.selectedCurrecy)
        } else if(this.props.contributedProject.selectedCurrecy === "ETH") {
            // minimum = 0.01;
            balance = this.props.ethBalance;
            left_contributed = cryptoFormatterByCoin(left_contributed,this.props.contributedProject.selectedCurrecy)
        } else if(this.props.contributedProject.selectedCurrecy === "USDT") {
            // minimum = 10;
            balance = this.props.usdtBalance;
            left_contributed = cryptoFormatterByCoin(left_contributed,this.props.contributedProject.selectedCurrecy)
        } else if(this.props.contributedProject.selectedCurrecy === "USDC") {
            // minimum = 10;
            balance = this.props.usdcBalance;
            left_contributed = cryptoFormatterByCoin(left_contributed,this.props.contributedProject.selectedCurrecy)
        } else if(this.props.contributedProject.selectedCurrecy === "IDR") {
            // minimum = 200000;
            balance = this.props.idrBalance;
        }
        

        return (
            <div>
                <Modal isOpen={this.props.open && this.state.modalShow} toggle={() => this.props.toggle()} className={this.props.className}>
                    <ModalHeader toggle={() => this.props.toggle()}>Contribute</ModalHeader>
                    <ModalBody>
                        <div className="m-alert m-alert--outline m-alert--square alert alert-dismissible fade show alert-info">
                            Your balance {balance} {this.props.contributedProject.selectedCurrecy}
                        </div>
                        <div className="form-group">
                            <label className="control-label">
                                Contribute Amount
                            </label>
                            <div className="input-group mb-3">
                                <input value={this.state.contributeAmount} onChange={this.handleChange('contributeAmount')} type="text" className="form-control" placeholder={minimum} aria-label="Recipient's username" aria-describedby="basic-addon2" />
                                <div className="input-group-append">
                                    <span className="input-group-text" id="basic-addon2">{this.props.contributedProject.selectedCurrecy}</span>
                                </div>
                            </div>
                            <div className="feedback">
                                You have contributed {cryptoFormatterByCoin(this.props.contributedProject.totalContributeAmountGroup,this.props.contributedProject.selectedCurrecy)} {this.props.contributedProject.selectedCurrecy} <br />
                                {left_contributed} {this.props.contributedProject.selectedCurrecy} remaining
                                to complete the pledge.
                            </div>
                        </div>
                        <div style={{fontWeight:"600", fontFamily:"Roboto", textDecoration:"underline", fontSize:"11px", marginTop:"12px", cursor:"pointer"}} data-tip data-for='agreement_note'>
                                Agreement note 
                        </div>
                        <ReactTooltip data-border='true' id="agreement_note" place="bottom" effect="solid">
                            <span style={{fontFamily:"Roboto"}}>When User has contributed as per pledged, 10% deposit will be automatically deposited back into account.</span>
                        </ReactTooltip>
                    </ModalBody>
                    <ModalFooter>
                        <Button disabled={this.state.loading} color="secondary" onClick={() => this.props.toggle()}>Cancel</Button>{' '}
                        <Button disabled={this.state.loading} color="success" onClick={() => this.contribute()}>
                            {this.state.loading ? 'Contributing...' : 'Contribute'}
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

}

const mapStateToProps = state => {
    return {
        user: state.cognito.user,
        language: state.language
    };
};

export default connect(
    mapStateToProps,
    null
)(ContributeModal);