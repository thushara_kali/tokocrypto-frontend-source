import React, { Component } from 'react';
import moment from 'moment';
import history from '../../history';

/**
 * TCDOC-050 this component used for rendering featured ICO there is not much going on here
 * just view separation for easier debugging
 */

class FeaturedICO extends Component {

    constructor() {
        super();

    }

    openReview() {
        // window.open(this.props.project.website, '_blank');
        history.push('/launchpad/review/' + this.props.project.projectId);
    }

    render() {

        let bg = this.props.project.featuredImage;

        return (
            <div>
                <div onClick={() => this.openReview()} className="launchpad-featured-wrapper" style={{ backgroundImage: `url(${bg})`, backgroundSize: 'cover', height: '200px' }}>
                    <div className="container">
                        <div className="row">
                            {/* <img style={{ width: 50, height: 50}} src="/assets/media/img/logo/indonesia.png" />
                            <div className="launchpad-featured-info">
                                <p className="bold">Name</p>
                                <p>Short Desc</p>
                            </div> */}
                        </div>
                    </div>
                </div>
                {/* <div className="container launchpad-featured-footer">
                    <div className="row">
                        <div className="launchpad-featured-pricing col-8">
                            <p>
                                <span className="green">$0</span>
                                <span className="muted">/ $0</span>
                            </p>
                            <p>
                                <span className="green">{this.props.project.bonus} %</span>
                                <span className="muted">&nbsp;{moment(this.props.project.contributionEndDate).fromNow()}</span>
                            </p>
                        </div>
                        <div className="col-4">
                            <button className="btn btn-default" onClick={() => this.openReview(this.props.project.projectId)}>Details</button>
                        </div>
                    </div>
                </div> */}
            </div>
        )
    }

}

export default FeaturedICO;