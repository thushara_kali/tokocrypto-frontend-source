import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUserAttributes, updateAttributes } from '../core/react-cognito/attributes.js'
import { changePassword } from '../core/react-cognito/utils.js'
import { NotificationManager } from 'react-notifications'
import NetworkError from '../errors/networkError';
import { bindActionCreators } from 'redux';
import homeActions from '../core/actions/client/home';
import userActions from '../core/actions/client/user';
import referralActions from '../core/actions/client/referral';
import { Loader } from '../components/tcLoader';
import LocalizedStrings from 'react-localization';
import enDictionary from '../languages/en.json';
import idDictionary from '../languages/id.json';
import javaDictionary from '../languages/java.json';
import sundaDictionary from '../languages/sunda.json';
import fontawesome from '@fortawesome/fontawesome'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {faKey, faMobileAlt, faGem, faQrcode, faEdit, faCopy, faShareAlt} from '@fortawesome/fontawesome-free-solid'
import {faGoogle, faSupple} from '@fortawesome/fontawesome-free-brands';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import amountFormatter from '../helpers/amountFormatter';
import QRModal from '../components/qrModal';
import history from '../history.js'

fontawesome.library.add(faKey, faMobileAlt, faGoogle, faGem, faQrcode, faEdit, faCopy, faShareAlt)

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class PromoCodeInput extends Component {

    constructor(){
        super();
        this.state = {
            referralLink: "",
            copied: false,
            referralCode: "",
            disabledInput: false,
            codeLoading: false,
            codeSuccess: false,
            codeFailed: false,
            buttonDisabled: true,
            referralConfirm: false,
            availability: false,
            kycError: false,
            timeout: 0,
            referralCopyCode: '',
            loading:false,
            referralConfirmed: '',
            baseReferralURL: '',
            QRModal: false,
            inviteButton: false
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            const re = /^[a-z]{0,20}$/;
            if (event.target.value == '' || re.test(event.target.value)) {
                obj[fieldName] = event.target.value
                this.setState(obj);
                if(this.state.timeout) clearTimeout(this.state.timeout);
                this.state.timeout = setTimeout(() => {
                    this.checkReferralCode(obj);
                }, 300);
            }
            // setTimeout(() => {
            //     this.checkReferralCode(obj);
            // },1000);
        }

    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    setReferralURL(){
        if(process.env.REACT_APP_ENV == 'production'){
            this.setState({
                baseReferralURL: 'https://tokocrypto.com/'+ this.props.language +'/referral/'
            })
        } else if (process.env.REACT_APP_ENV == 'demo'){
            this.setState({
                baseReferralURL: 'https://demo.tokocrypto.com/'+ this.props.language +'/referral/'
            })
        } else {
            this.setState({
                baseReferralURL: 'https://dev.tokocrypto.com/'+ this.props.language +'/referral/'
            })
        }
    }

    onSetLanguage(){
        strings.setLanguage(this.props.language);
    }

    checkReferralCode(code){

        let referral = code.referralCode

        this.setState({codeLoading: true, codeFailed:false, codeSuccess: false})

        let data = {
            key: referral
        };

        referralActions.checkReferralCode(this.getToken(),data).then(res => {

            this.setState({
                codeLoading: false,
                buttonDisabled: false,
                codeSuccess: true,
                codeFailed: false
            })

        }).catch(err => {
            NetworkError(err);
            if(referral){
                this.setState({
                    codeLoading: false,
                    buttonDisabled: true,
                    codeSuccess: false,
                    codeFailed: true
                })
            } else {
                this.setState({
                    codeSuccess: false,
                    codeFailed: false,
                    buttonDisabled: true,
                    codeLoading: false
                })
            }

        });

    }

    confirmReferralCode(){

        this.setState({
            loading:true,
            referralConfirm: true
        })

        let url = this.state.baseReferralURL;
        url = url+this.state.referralCode

        let data = {
            key: this.state.referralCode
        };

        referralActions.confirmReferralCode(this.getToken(),data).then(res => {

            this.setState({
                loading:false,
                disabledInput:true,
                codeSuccess: false,
                referralCopyCode: url,
                referralConfirmed: true
            })
            NotificationManager.success(strings.REFERRAL_CODE_ADDED);

        }).catch(err => {
            this.setState({
                loading:false,
                referralConfirmed: false
            });
            NetworkError(err);

            // let errors = strings.REFFERAL_CODE_NOT_CONFIRMED;
            // if (err.response && err.response.data.err.errorMessage) {
            //     let msg = Object.assign( {} , JSON.parse(err.response.data.err.errorMessage));
            //     errors = msg["message"];

            //     NotificationManager.error(errors);
            // } else {
            //     NotificationManager.error(errors);
            // }

        });

    }

    checkReferralAvailability(){

        userActions.fetchUserDetails(this.getToken()).then(res => {

            let user = res.data.data

            if(user.Kyc == 'pending' || user.Kyc == 'rejected'){
                this.setState({
                    availability:false,
                    disabledInput:true,
                    buttonDisabled:true,
                    kycError:true
                });
            } else if (user.ReferKey != '' && user.ReferKey != undefined){

                let url = this.state.baseReferralURL;
                url = url+user.ReferKey

                this.setState({
                    disabledInput:true,
                    referralConfirmed: true,
                    referralConfirm: true,
                    referralCode: user.ReferKey,
                    referralCopyCode: url
                })
            }

        }).catch(err => {
            // let errors = strings.REFFERAL_CODE_AVAILABILITY_ERROR;
            // if (err.response && err.response.data.err.errorMessage) {
            //     let msg = Object.assign( {} , JSON.parse(err.response.data.err.errorMessage));
            //     errors = msg["message"];

            //     NotificationManager.error(errors);
            // } else {
            //     NotificationManager.error(errors);
            // }
            NetworkError(err);
        });
    }

    showQRCode(){
        this.setState({
            QRModal: true
        })
    }

    onCloseQRModal(){
        this.setState({
            QRModal: false
        })
    }

    checkNavigatorSupport(){
        if (navigator.share) {
            this.setState({
                inviteButton: true
            })
        } else {
            this.setState({
                inviteButton: false
            })
        }
    }

    inviteFriends(){
        if (navigator.share) {
            navigator.share({
                url: this.state.referralCopyCode
            })
            .then(() => console.log('Successful share'))
            .catch((error) => {
                NetworkError(error);
            });
        }
    }

    componentDidMount() {
        this.setReferralURL();
        this.checkReferralAvailability();
    }

    componentWillMount() {
        this.checkNavigatorSupport();
        this.onSetLanguage();
    }

    render() {

        return (
            <div className="row referal-link-container">
                <div className="input-group mb-2 col-md-12 col-lg-7">

                    <input placeholder="enter promo codes" type="text" autoCapitalize="off" className="form-control discount-code-input"
                            onChange={this.handleChange('referralCode')}/>
                        <div className="input-group-append referal-code-animation">
                            {this.state.codeLoading == true?
                                <img src="assets/media/img/loading-refferal.gif" className="referral-code-loading-img" alt="" />:null
                            }
                            {this.state.codeSuccess == true?
                                <img src="assets/media/img/verified.png" className="referral-code-success-img" alt="" />:null
                            }
                            {this.state.codeFailed == true?
                                <img src="assets/media/img/md-remove-circle.png" className="referral-code-success-img" alt="" />:null
                            }
                        </div>
                        <div></div>

                </div>
                <div className="col-md-5">
                    <div className="row refferal-confirm-container">

                        <button onClick={() => this.confirmReferralCode()} disabled={this.state.buttonDisabled} className="btn btn-outline-blue" style={{fontWeight:"300"}} type="button">
                            Apply
                        </button>

                        {this.state.loading == true ?
                            <span className="refferal-loader" style={{display:"block", textAlign:"center"}} >
                                <Loader type="line-scale" active={this.state.loading} className="text-center"/>
                            </span>
                        : null }

                    </div>
                </div>
                {this.state.codeLoading && <div className="referal-code-check-availability">{strings.PLEASE_WAIT_WILL_CHECK_AVAILABILITY}</div>}

                {this.state.codeFailed && <div className="referal-code-check-availability-error">{strings.SORRY_NAME_TAKEN}</div>}

                {this.state.copied &&  <div className="referal-code-check-availability">{strings.COPIED}</div> }

                {this.state.kycError &&  <div className="referal-code-check-availability-error">{strings.APPROVE_KYC_PROCEED_REFERRAL}</div>}

                <QRModal
                    show={this.state.QRModal}
                    onClose={() => this.onCloseQRModal()}
                    refCode={this.state.referralCopyCode}
                    loading={this.state.loading}
                />

            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        language: state.language
    };
};


export default connect(mapStateToProps) (PromoCodeInput);
