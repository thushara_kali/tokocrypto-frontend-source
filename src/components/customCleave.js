import React, { Component } from 'react';
import amountFormatter from '../helpers/amountFormatter';
import numeral from 'numeral';

class CustomCleave extends Component {

    constructor(){
        super();
        this.state = {
            currentValue: ""
        }
    }

    onChangeFormat(e) {

        let formatted = amountFormatter(e.target.value) || "0";

        this.setState({
            currentValue: formatted
        });
        let eventobj = {
            target: {
                rawValue: Number(formatted.replace(/\./g, ''))
            }
        }
        this.props.onChange(eventobj);
    }

    onKeyDown(e) {
        if(e.keyCode === 189){
            e.preventDefault();
        }
    } 

    render () {
        return (
            <input 
                value={this.state.currentValue}
                className={this.props.className}
                onChange={(e) => this.onChangeFormat(e)}
                onKeyDown={(e) => this.onKeyDown(e)}
            />
        )
    }

}

export default CustomCleave;