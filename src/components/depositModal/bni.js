import React, {Component} from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class BNI extends Component {

    constructor() {
        super();
    }

    componentWillMount() {
        strings.setLanguage(this.props.language);
    }

    render() {
        return (
            <div className="col-12">
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/bni/bni-01.png"/>
                            <p className="col-9">{ strings.DEPOSIT_BNI_1 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/bni/bni-02.png"/>
                            <p className="col-9">{ strings.DEPOSIT_BNI_2 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/bni/bni-03.png"/>
                            <p className="col-9">{ strings.DEPOSIT_BNI_3 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/bni/bni-04.png"/>
                            <div className="col-9">
                                <p>{ strings.DEPOSIT_BNI_4 }</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/bni/bni-05.png"/>
                            <div className="col-9">
                                <p>{ strings.DEPOSIT_BNI_5 } <b className="bold">{this.props.accountNumber}</b> {strings.DEPOSIT_BNI_51}</p>
                                <p>{ strings.DEPOSIT_BNI_52 }</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/bni/bni-06.png"/>
                            <div className="col-9">
                                <p>{ strings.DEPOSIT_BNI_6 }</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-6" style={{width: "180px"}} src="/assets/app/media/img/deposit/bni/bni-07.png"/>
                            <br /><br />
                            <div className="col-12" style={{marginTop: "20px"}}>
                                <p>{ strings.DEPOSIT_BNI_7 }</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-6" style={{width: "180px"}} src="/assets/app/media/img/deposit/bni/bni-08.png"/>
                            <br /><br />
                            <div className="col-12" style={{marginTop: "20px"}}>
                                <p>{ strings.DEPOSIT_BNI_8 }</p>
                                <p>{ strings.ESTIMATED_TIME } <b className="bold">{strings.FIVE_MINUTES}</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}


const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

export default connect(mapStateToProps, null) (BNI);
