import React, {Component} from 'react';
import classnames from 'classnames';
import MandiriOnline from './mandiriOnline';
import CIMB from './cimb';
import BCA from './bca';
import BNI from './bni';

import { connect } from 'react-redux';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class MobileApp extends Component {

    constructor() {
        super();
        this.state = {
            provider: 'mandiri'
        }
    }

    componentWillMount() {
        strings.setLanguage(this.props.language);
    }

    setProvider(provider) {
        this.setState({
            provider
        });
    }

    render() {
        return (
            <div>
                <br /><br />
                <div className="row margin-0">
                    <div onClick={() => this.setProvider('mandiri')} className={classnames({ 'col-3 provider': true, 'mandiri-checked': this.state.provider === 'mandiri'})}>
                        <input type="radio" name="provider" checked={this.state.provider === 'mandiri'}/> Mandiri Online
                    </div>
                    <div  onClick={() => this.setProvider('cimb')} className={classnames({ 'col-3 provider': true, 'gm-checked': this.state.provider === 'cimb'})}>
                        <input type="radio" name="provider" checked={this.state.provider === 'cimb'}/> Go Mobile
                    </div>
                    <div onClick={() => this.setProvider('bca')} className={classnames({ 'col-3 provider': true, 'bca-checked': this.state.provider === 'bca'})}>
                        <input type="radio" name="provider" checked={this.state.provider === 'bca'}/> m-BCA
                    </div>
                    <div onClick={() => this.setProvider('bni')} className={classnames({ 'col-3 provider': true, 'bni-checked': this.state.provider === 'bni'})}>
                        <input type="radio" name="provider" checked={this.state.provider === 'bni'}/> BNI
                    </div>
                </div>
                <br /><br />
                <div className="row margin-0">
                    { this.state.provider === 'mandiri' &&
                        <MandiriOnline
                            fullName={this.props.fullName}
                            bankName={this.props.bankName}
                            accountNumber={this.props.accountNumber}
                        />
                    }
                    { this.state.provider === 'cimb' &&
                        <CIMB
                            fullName={this.props.fullName}
                            bankName={this.props.bankName}
                            accountNumber={this.props.accountNumber}
                        />
                    }
                    { this.state.provider === 'bca' &&
                        <BCA
                            fullName={this.props.fullName}
                            bankName={this.props.bankName}
                            accountNumber={this.props.accountNumber}
                        />
                    }
                    { this.state.provider === 'bni' &&
                    <BNI
                        fullName={this.props.fullName}
                        bankName={this.props.bankName}
                        accountNumber={this.props.accountNumber}
                    />
                    }
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

export default connect(mapStateToProps, null) (MobileApp);
