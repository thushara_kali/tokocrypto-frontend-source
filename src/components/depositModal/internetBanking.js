import React, {Component} from 'react';
import classnames from 'classnames';
import KBCA from './klikbca';

import { connect } from 'react-redux';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class InternetBanking extends Component {

    constructor() {
        super();
        this.state = {
            provider: 'kbca'
        }
    }

    componentWillMount() {
        strings.setLanguage(this.props.language);
    }

    setProvider(provider) {
        this.setState({
            provider
        });
    }

    render() {
        return (
            <div>
                <br /><br />
                <div className="row margin-0">
                    <div className="col-3">
                        <div onClick={() => this.setProvider('kbca')} className={classnames({ 'provider': true, 'bca-checked': this.state.provider === 'kbca'})}>
                            <input type="radio" name="provider" checked={this.state.provider === 'kbca'}/> Klik BCA
                        </div>
                    </div>
                </div>
                <br /><br />
                <div className="row margin-0">
                    { this.state.provider === 'kbca' &&
                    <KBCA
                        fullName={this.props.fullName}
                        bankName={this.props.bankName}
                        accountNumber={this.props.accountNumber}
                    />
                    }
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

export default connect(mapStateToProps, null) (InternetBanking);
