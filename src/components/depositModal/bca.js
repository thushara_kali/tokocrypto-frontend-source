import React, {Component} from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class BCA extends Component {

    constructor() {
        super();
    }

    componentWillMount() {
        strings.setLanguage(this.props.language);
    }

    render() {
        return (
            <div className="col-12">
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/mbca/bca-01.png"/>
                            <p className="col-9">{ strings.DEPOSIT_BCA_1 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/mbca/bca-02.png"/>
                            <p className="col-9">{ strings.DEPOSIT_BCA_2 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/mbca/bca-03.png"/>
                            <p className="col-9">{ strings.DEPOSIT_BCA_3 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/mbca/bca-04.png"/>
                            <div className="col-9">
                                <p>{ strings.DEPOSIT_BCA_4 } <b className="bold">{this.props.accountNumber}</b></p>
                                <p>{ strings.DEPOSIT_BCA_41 } </p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/mbca/bca-05.png"/>
                            <p className="col-9">{ strings.DEPOSIT_BCA_5 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/mbca/bca-06.png"/>
                            <div className="col-9">
                                <p>{ strings.DEPOSIT_BCA_6 }</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/mbca/bca-07.png"/>
                            <p className="col-9">{ strings.DEPOSIT_BCA_7 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/mbca/bca-08.png"/>
                            <div className="col-9">
                                <p>{ strings.DEPOSIT_BCA_8 }</p>
                                <p>{ strings.DEPOSIT_BCA_81 } <b className="bold">{this.props.accountNumber}</b></p>
                                <p>{ strings.DEPOSIT_BCA_82 }</p>
                                <p>{ strings.DEPOSIT_BCA_83 }</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/mbca/bca-09.png"/>
                            <p className="col-9">{ strings.DEPOSIT_BCA_9 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/mbca/bca-10.png"/>
                            <div className="col-9">
                                <p>{ strings.DEPOSIT_BCA_10 }</p>
                                <p>{ strings.ESTIMATED_TIME } <b className="bold">{strings.FIVE_MINUTES}</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}


const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

export default connect(mapStateToProps, null) (BCA);
