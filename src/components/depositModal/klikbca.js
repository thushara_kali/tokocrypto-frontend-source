import React, {Component} from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class KBCA extends Component {

    constructor() {
        super();
    }

    componentWillMount() {
        strings.setLanguage(this.props.language);
    }

    render() {
        return (
            <div className="col-12">
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-5" src="/assets/app/media/img/deposit/kbca/KBCA-01.png"/>
                            <p className="col-7">{ strings.DEPOSIT_KBCA_1 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-5" src="/assets/app/media/img/deposit/kbca/KBCA-02.png"/>
                            <p className="col-7">{ strings.DEPOSIT_KBCA_2 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-5" src="/assets/app/media/img/deposit/kbca/KBCA-03.png"/>
                            <p className="col-7">{ strings.DEPOSIT_KBCA_3 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-5" src="/assets/app/media/img/deposit/kbca/KBCA-04.png"/>
                            <div className="col-7">
                                <p>{ strings.DEPOSIT_KBCA_4 }</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-5" src="/assets/app/media/img/deposit/kbca/KBCA-05.png"/>
                            <div className="col-7">
                                <p>{ strings.DEPOSIT_KBCA_5 }</p>
                                <p>{ strings.DEPOSIT_KBCA_51 } <b className="bold">{this.props.fullName}</b></p>
                                <p>{ strings.DEPOSIT_KBCA_52 } <b className="bold">{this.props.accountNumber}</b></p>
                                <p>{ strings.DEPOSIT_KBCA_53 }</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-5" src="/assets/app/media/img/deposit/kbca/KBCA-06.png"/>
                            <div className="col-7">
                                <p>{ strings.DEPOSIT_KBCA_6 }</p>
                                <p>{ strings.DEPOSIT_KBCA_61 }</p>
                                <p>{ strings.DEPOSIT_KBCA_62 } <b className="bold">{this.props.fullName}</b></p>
                                <p>{ strings.DEPOSIT_KBCA_63 } <b className="bold">{this.props.accountNumber}</b></p>
                                <p>{ strings.DEPOSIT_KBCA_64 }</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-5" src="/assets/app/media/img/deposit/kbca/KBCA-07.png"/>
                            <p className="col-7">{ strings.DEPOSIT_KBCA_7 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-5" src="/assets/app/media/img/deposit/kbca/KBCA-08.png"/>
                            <div className="col-7">
                                <p>{ strings.DEPOSIT_KBCA_8 }</p>
                                <p>{ strings.DEPOSIT_KBCA_81 }</p>
                                <p>{ strings.DEPOSIT_KBCA_82 }</p>
                                <p>{ strings.DEPOSIT_KBCA_83 }</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-5" src="/assets/app/media/img/deposit/kbca/KBCA-10.png"/>
                            <div className="col-7">
                                <p>{ strings.DEPOSIT_KBCA_10 }</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-5" src="/assets/app/media/img/deposit/kbca/KBCA-11.png"/>
                            <div className="col-7">
                                <p>{ strings.DEPOSIT_KBCA_11 }</p>
                                <p>{ strings.DEPOSIT_KBCA_111 }</p>
                                <p>{ strings.DEPOSIT_KBCA_112 } <b className="bold">{this.props.fullName}</b></p>
                                <p>{ strings.DEPOSIT_KBCA_113 } <b className="bold">{this.props.accountNumber}</b></p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-5" src="/assets/app/media/img/deposit/kbca/KBCA-12.png"/>
                            <div className="col-7">
                                <p>{ strings.DEPOSIT_KBCA_12 }</p>
                                <p>{ strings.ESTIMATED_TIME } <b className="bold">{strings.FIVE_MINUTES}</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}


const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

export default connect(mapStateToProps, null) (KBCA);
