import React, {Component} from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class CIMB extends Component {

    constructor() {
        super();
    }

    componentWillMount() {
        strings.setLanguage(this.props.language);
    }

    render() {
        return (
            <div className="col-12">
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/cimb/cimb-01.png"/>
                            <p className="col-9">{ strings.DEPOSIT_CIMB_1 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/cimb/cimb-02.png"/>
                            <p className="col-9">{ strings.DEPOSIT_CIMB_2 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/cimb/cimb-03.png"/>
                            <p className="col-9">{ strings.DEPOSIT_CIMB_3 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/cimb/cimb-04.png"/>
                            <p className="col-9">{ strings.DEPOSIT_CIMB_4 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/cimb/cimb-05.png"/>
                            <p className="col-9">{ strings.DEPOSIT_CIMB_5 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/cimb/cimb-06.png"/>
                            <div className="col-9">
                                <p>{ strings.DEPOSIT_CIMB_6 } <b className="bold">{this.props.accountNumber}</b></p>
                                <p>{ strings.DEPOSIT_CIMB_61 }</p>
                                <p>{ strings.DEPOSIT_CIMB_62 }</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/cimb/cimb-07.png"/>
                            <div className="col-9">
                                <p>{ strings.DEPOSIT_CIMB_7 }</p>
                                <div className="row">
                                    <div className="col-12">
                                        <label className="control-label col-5"><b>{strings.NAME}</b></label>
                                        <label className="control-label col-7 bold">{this.props.fullName}</label>
                                    </div>
                                    <div className="col-12">
                                        <label className="control-label col-5"><b>{strings.BANK_NAME}</b></label>
                                        <label className="control-label col-7 bold">{this.props.bankName}</label>
                                    </div>
                                    <div className="col-12">
                                        <label className="control-label col-5"><b>{strings.ACCOUNT_NUMBER}</b></label>
                                        <label className="control-label col-7 bold">{this.props.accountNumber}</label>
                                    </div>
                                </div>
                                <br />
                                <p>{ strings.DEPOSIT_CIMB_71 }</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-even col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/cimb/cimb-08.png"/>
                            <p className="col-9">{ strings.DEPOSIT_CIMB_8 }</p>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="step-wrapper-odd col-12">
                        <div className="row">
                            <img className="col-3" src="/assets/app/media/img/deposit/cimb/cimb-09.png"/>
                            <div className="col-9">
                                <p>{ strings.DEPOSIT_CIMB_9 }</p>
                                <p>{ strings.ESTIMATED_TIME } <b className="bold">{strings.FIVE_MINUTES}</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        language: state.language
    };
};

export default connect(mapStateToProps, null) (CIMB);
