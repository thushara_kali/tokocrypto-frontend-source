import React, { Component } from 'react'
import {
    Modal,
    ModalHeader,
    ModalBody,
    Collapse,
    // Button,
    // CardBody, 
    Card
} from 'reactstrap'
import { NotificationManager } from "react-notifications";

import { getUserAttributes, updateAttributes } from '../../core/react-cognito/attributes.js'
import { connect } from 'react-redux'
import actions from '../../core/actions/client/banks'
import { Loader } from '../tcLoader'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import LocalizedStrings from 'react-localization'
import enDictionary from '../../languages/en.json'
import idDictionary from '../../languages/id.json'
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import classnames from 'classnames'
import MobileAppGuide from './mobileApp'
import InternetBankingGuide from './internetBanking'
import _ from 'lodash';
import S3UploadModel from '../../components/S3UploadModel';
import userActions from '../../core/actions/client/user';
import depositActions from '../../core/actions/client/deposit';
import Cleave from '../../components/customCleave';
import NetworkError from '../../errors/networkError';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class DepositModal extends Component {

    constructor() {
        super()
        this.state = {
            banksdata: [],
            bankName: '',
            fullName: '',
            label: '',
            accountNumber: '',
            instruction: false,
            activeTab: 'mobile',
            expandedBca: false,
            expandedAlfa: false,
            expandedBri: false,
            expandedMandiri: false,
            expandedPermata: false,
            successAlfa:false,
            expandedBni: false,
            loadingGenerate:false,
            loadingDepositFiat:false,
            fileProofScreenshotPath: '',
            depositCode: '',
            amount: '',
            amountAlfa: '',
            bankBni: {
                BankName: 'BNI',
                AccountNumber: '',
            },
            bankBri: {
                BankName: 'BRI',
                AccountNumber: '',
            },
            bankMandiri: {
                BankName: 'MANDIRI',
                AccountNumber: '',
            },
            bankPermata: {
                BankName: 'PERMATA',
                AccountNumber: '',
            },
            bankBca: {
                BankName: 'BCA',
                AccountNumber: '',
                AccountNumberStatic: 2613305777
            },

        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            if (fieldName === 'amount' || fieldName === 'amountAlfa') {
                obj[fieldName] = event.target.rawValue;
            } else {
                obj[fieldName] = event.target.value
            }

            this.setState(obj)
        }

    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    onSetLanguage() {
        strings.setLanguage(this.props.language)
    }

    fetchUserProfile() {
        // console.log('fetching');
        getUserAttributes(this.props.user).then(attr => {
            // console.log('fetching ok', attr);
            this.setState({
                fullName: attr.name
            })
        })
        .catch(err => {
            // console.log('fetching err', err);
        })
    }

    fetchBankDepositAccounts() {
        this.setState({loading: true})
        this.props.fetchBankDepositAccounts(this.getToken()).then(async res => {

            let banks = res.data.data[0];
            let bni = [];
            let bca = [];
            let mandiri = [];
            let bri = [];
            let permata = [];

            bni = _.find(res.data.data,{BankName : "BNI"})

            bca = _.find(res.data.data,{BankName : "BCA"})

            mandiri = _.find(res.data.data,{BankName : "MANDIRI"})

            bri = _.find(res.data.data,{BankName : "BRI"})

            permata = _.find(res.data.data,{BankName : "PERMATA"})

            // console.log("Bank-inside", res.data, bri, mandiri, permata);

            if(bni){                
                this.setState({
                    bankBni : bni
                })
            }else{
                this.generateVA('BNI');
            }

            if(permata){
                this.setState({
                    bankPermata : permata
                })
            } else {
                this.generateVA('PERMATA');
            }

            if(bri){
                this.setState({
                    bankBri : bri
                })
            } else {
                this.generateVA('BRI');
            }

            if(mandiri){
                this.setState({
                    bankMandiri : mandiri
                })
            } else {
                this.generateVA('MANDIRI');
            }

            if(bca){
                this.setState({
                    bankBca : bca
                })
            }
            
            this.setState({
                banksdata: res.data.data,
                bankName: banks.BankName,
                label: banks.Label,
                accountNumber: banks.AccountNumber,
                bankBca: {
                    BankName: 'BCA',
                    AccountNumberStatic: 2613305777
                },
                loading: false
            })

        })
        .catch(err => {
            // NetworkError(err)
        })
    }

    componentDidMount() {
        this.fetchUserProfile()
        this.fetchUserDetails()
        {this.fetchBankDepositAccounts()}
    }

    componentWillMount() {
        this.onSetLanguage()
    }

    generateVA(bankName = ''){
        this.setState({
            loadingGenerate: true,
        });
        let data = {
            bankName
        }
        actions.generateVA(this.getToken(),data).then(res => {
            
            this.setState({
                loadingGenerate: false,
            })
            // NotificationManager.success("Generate VA "+bankName+" Success");
            this.fetchBankDepositAccounts();
        })
        .catch(err => {
            this.setState({
                loadingGenerate: false,
            })
            // NotificationManager.error("Failed to generate VA, Please try again");
            // NetworkError(err)
        })
    }

    close() {
        this.setState({
            copied: false,
            instruction: false
        })
        this.props.toggleShow()
    }

    toggle(activeTab) {
        this.setState({
            activeTab
        })
    }

    expandeToogleBca() {
        this.setState({ expandedBca: !this.state.expandedBca })
    }

    expandeToogleAlfa() {
        this.setState({ expandedAlfa: !this.state.expandedAlfa })
    }

    expandeToogleBri() {
        this.setState({ expandedBri: !this.state.expandedBri })
    }

    expandeTooglemandiri() {
        this.setState({ expandedMandiri: !this.state.expandedMandiri })
    }

    expandeTooglePermata() {
        this.setState({ expandedPermata: !this.state.expandedPermata })
    }

    expandeToogleBni() {
        this.setState({ expandedBni: !this.state.expandedBni })
    }

    
    fetchUserDetails(){

        userActions.fetchUserDetails(this.getToken()).then(res => {

            this.setState({
                depositCode: res.data.data.DepositCode
            })

        }).catch(err => {
            NetworkError(err);
        });
    }

    submitDepositFiat() {

        this.setState({loadingDepositFiat:true})

        let data

        data = {
            amount: this.state.amount ,
            currencyCode: "IDR",
            referencePath: this.state.fileProofScreenshotPath
        };

        depositActions.submitDepositFiat(this.getToken(),data).then(res => {
          
            this.setState({
                loadingDepositFiat:false,
                amount: '',
                referencePath: ''
            });

            NotificationManager.success("Deposit Placed");

            this.close();
           
        }).catch(err => {
            NetworkError(err);
            this.setState({
                loadingDepositFiat:false
            });

        });
    }

    submitDepositAlfa() {

        this.setState({loadingDepositFiat:true})

        const data = {
            amount: this.state.amountAlfa,
            retailOutletName: "ALFAMART"
        };

        depositActions.submitDepositAlfa(this.getToken(),data).then(res => {
          
            this.setState({
                loadingDepositFiat:false,
                amountAlfa: '',
                successAlfa: true
            });

            NotificationManager.success("Successfully submitted, please check your email for invoice");
           
        }).catch(err => {
            NetworkError(err);
            this.setState({
                loadingDepositFiat:false
            });

        });
    }

    render() {

        var depositButton

        if(this.state.fileProofScreenshotPath.length == 0 || this.state.amount < 1000000 || this.state.amount != ""){
            depositButton = true
        } else {
            depositButton = false
        }

        return (
            <div>
                <Modal isOpen={this.props.show} toggle={() => this.close()}
                       className={classnames({'modal-dialog-centered': !this.state.instruction})} size="lg">
                    <ModalHeader toggle={() => this.close()}>{strings.DEPOSIT_INDONESIAN_RUPIAH}</ModalHeader>
                    <ModalBody className="tc-modal-body-deposit">
                        {this.state.instruction === false && <div style={{padding:"25px"}}>
                            <ul>
                                <li>
                                    <p>{strings.DEPOSIT_INSTRUCTION_1}</p>
                                </li>
                                <li>
                                    <p>
                                        {strings.DEPOSIT_INSTRUCTION_2_1}&nbsp;
                                        <span style={{color: 'red'}}>{strings.MUST}</span>&nbsp;
                                        {strings.DEPOSIT_INSTRUCTION_2_2}<span>{this.state.fullName}</span>
                                    </p>
                                </li>
                                <li>
                                    <p>{strings.DEPOSIT_INSTRUCTION_3} Rp 50.000</p>
                                </li>
                            </ul>
                            <br/>
                            <span style={{color: 'red'}}>{strings.DEPOSIT_WARNING}</span>&nbsp;
                            <br/><br/>
                            <button className="btn btn-blue" style={{marginLeft: '45%'}}
                                    onClick={() => this.setState({instruction: true})}>
                                <i className="fa fa-check"></i> {strings.ACK}
                            </button>
                        </div>}
                        {this.state.instruction === true && <div>
                            <div className="col-12">
                                <div className="row">

                                    <a href="#" onClick={() => this.expandeToogleBca()}>
                                        <div className="col-12">
                                            <div className="row" style={{borderBottom:"1px solid #b8b8b87a", margin:"10px 0px", paddingBottom:"20px"}}>
                                                <img className="col-3" src="/assets/media/img/bank/mbank-06.png"  />
                                                <div className="col-7"></div>
                                                {this.state.expandedBca === false ?
                                                    <img className="col-2" src="/assets/media/img/bank/mb-ar-down.png" />
                                                    :
                                                    <img className="col-2" src="/assets/media/img/bank/mb-ar-up.png"  />
                                                }
                                            </div>
                                        </div>
                                    </a>
                                    
                                    <Collapse isOpen={this.state.expandedBca} style={{paddingTop:"10px", paddingBottom:"15px"}}>
                                        <div className="step-wrapper-odd">
                                            <div className="row">
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.NAME}</b></label>
                                                    <label className="control-label col-8 bold">PT Crypto Indonesia Berkat</label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.BANK_NAME}</b></label>
                                                    <label className="control-label col-8 bold">{this.state.bankBca.BankName}</label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>Deposit code</b></label>
                                                    <label className="control-label col-8 bold">
                                                        {this.state.depositCode} &nbsp;
                                                        <CopyToClipboard text={this.state.depositCode} onCopy={() => this.setState({ copiedDepositCode: true})}>
                                                            <i className="fa fa-copy is-link"></i>
                                                        </CopyToClipboard>
                                                        &nbsp; {this.state.copiedDepositCode && <span>{strings.COPIED}</span>}
                                                    </label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.ACCOUNT_NUMBER}</b></label>
                                                    <label className="control-label col-8 bold">
                                                        {this.state.bankBca.AccountNumberStatic} &nbsp;
                                                        <CopyToClipboard text={this.state.bankBca.AccountNumberStatic} onCopy={() => this.setState({ copied: true})}>
                                                            <i className="fa fa-copy is-link"></i>
                                                        </CopyToClipboard>
                                                        &nbsp; {this.state.copied && <span>{strings.COPIED}</span>}
                                                    </label>
                                                </div>
                                                <div className="col-12" style={{display:"inline-flex"}}>
                                                    <label className="control-label col-4"><b>Amount</b></label>
                                                    {/* <input type="text" value="" name="name" style={{ marginBottom: "5px", width:"40%" }} onChange={this.handleChange('amount')} value={this.state.amount} className={ "form-control m-input" } /> */}
                                                    <div className="input-group" style={{ marginBottom: "5px", width: "40.3%" }}>
                                                        <div className="input-group-append" style={{ padding: "-0.65rem 1.25rem", border: "1px solid rgba(0,0,0,.15)", borderRadius: ".25rem", backgroundColor: "#f4f5f8", color: "gray" }} >
                                                            <span className="input-group-text" id="basic-addon2">Rp</span></div>
                                                        {/* <input onChange={this.handleChange('amount')} type="number" className="form-control m-input" placeholder="" aria-describedby="basic-addon2" /> */}
                                                        <Cleave onChange={this.handleChange('amount')} type="text" className="form-control m-input" placeholder="" aria-describedby="basic-addon2" disabled={this.state.loading} />
                                                    </div>
                                                </div>
                                                <div style={{ color: this.state.amount < 1000000 && (this.state.amount != "") ? 'red' : 'black', fontSize: "10px", margin: "auto", marginBottom: "10px" }}>{strings.MINIMUM_AMOUNT_IS} Rp1.000.000</div>
                                                <div className="col-12" style={{display:"inline-flex"}}>
                                                    <label className="control-label col-4"><b>Screenshot</b></label>
                                                    <S3UploadModel
                                                        value={this.state.fileProofScreenshotPath}
                                                        service="deposits"
                                                        className="form-control col-md-12"
                                                        signingUrl={process.env.REACT_APP_SIGNING_URL}
                                                        onFinish={(file) => { this.setState({ fileProofScreenshotPath: file }) }}
                                                    />
                                                </div><br/><br/>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>User Guide</b></label>
                                                    <a href="https://support.tokocrypto.com/hc/id/articles/360036549432-Cara-Deposit-Melalui-BCA" target="_blank" style={{cursor:"pointer", textDecoration:"none"}}><span className="control-label col-8 bold">Link</span></a>
                                                </div>
                                                <div className="col-12">
                                                    <b style={{fontSize:"10px", marginLeft:"15px"}}>{strings.IMPORTANT}: Amount and screenshots are a requirement</b>
                                                </div>

                                                <div className="col-12" style={{paddingTop:"15px", paddingBottom:"5px"}}>
                                                    {this.state.loadingDepositFiat ?
                                                        <div style={{ textAlign: "center" }}><Loader /></div> :
                                                        <button disabled={this.state.fileProofScreenshotPath.length === 0 || this.state.amount < 1000000 } onClick={() => this.submitDepositFiat()} style={{width:"25%", margin:"auto"}} className="btn btn-primary btn-block">Submit</button>
                                                    }
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                    </Collapse>
                                </div>
                                <br/>
                                <div className="row">
                                    <a href="#" onClick={() => this.expandeToogleAlfa()}>
                                        <div className="col-12">
                                            <div className="row" style={{borderBottom:"1px solid #b8b8b87a", margin:"10px 0px", paddingBottom:"20px"}}>
                                                <img className="col-3" src="/assets/media/img/bank/Alfamart.png"  />
                                                <div className="col-7"></div>
                                                {this.state.expandedAlfa === false ?
                                                    <img className="col-2" src="/assets/media/img/bank/mb-ar-down.png" />
                                                    :
                                                    <img className="col-2" src="/assets/media/img/bank/mb-ar-up.png"  />
                                                }
                                            </div>
                                        </div>
                                    </a>
                                    <Collapse isOpen={this.state.expandedAlfa} style={{paddingTop:"10px", paddingBottom:"15px"}}>
                                        <div className="step-wrapper-odd">
                                            <div className="row">
                                                <div className="col-12" style={{display:"inline-flex"}}>
                                                    <label className="control-label col-4"><b>Amount</b></label>
                                                    <div className="input-group" style={{ marginBottom: "5px", width: "40.3%" }}>
                                                        <div className="input-group-append" style={{ padding: "-0.65rem 1.25rem", border: "1px solid rgba(0,0,0,.15)", borderRadius: ".25rem", backgroundColor: "#f4f5f8", color: "gray" }} >
                                                            <span className="input-group-text" id="basic-addon2">Rp</span></div>
                                                        <Cleave onChange={this.handleChange('amountAlfa')} type="text" className="form-control m-input" placeholder="" aria-describedby="basic-addon2" disabled={this.state.loading} />
                                                    </div>
                                                </div>
                                                <br/><br/>
                                                <div className="col-12" style={{textAlign:"justify", paddingRight:"60px", paddingTop:"30px"}}>
                                                    <ul>
                                                        <li>
                                                            <p>{strings.DEPOSIT_ALFA_INSTRUCTION_1}</p>
                                                        </li>
                                                        <li>
                                                            <p>{strings.DEPOSIT_ALFA_INSTRUCTION_2}</p>
                                                        </li>
                                                        <li>
                                                            <p>{strings.DEPOSIT_ALFA_INSTRUCTION_3}</p>
                                                        </li>
                                                    </ul>
                                                </div>

                                            {this.state.successAlfa && <div style={{ color: 'green', fontSize: "10px", margin: "auto", marginBottom: "10px" }}>Successfully submitted, please check your email for invoice</div> }

                                                <div className="col-12" style={{paddingTop:"15px", paddingBottom:"5px"}}>
                                                    {this.state.loadingDepositFiat ?
                                                        <div style={{ textAlign: "center" }}><Loader /></div> :
                                                        <button disabled={this.state.amountAlfa < 10000 || this.state.amountAlfa > 5000000} onClick={() => this.submitDepositAlfa()} style={{width:"25%", margin:"auto"}} className="btn btn-primary btn-block">Submit</button>
                                                    }
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </Collapse>
                                    <div className="col-12 row" style={{padding:"10px 25px 10px 50px"}}>
                                        <button className={classnames({
                                            'btn col-5': true,
                                            'btn-gradient': this.state.activeTab === 'mobile',
                                            'btn-gradient-inactive': this.state.activeTab !== 'mobile',
                                        })} onClick={() => this.toggle('mobile')}>
                                            Mobile App
                                        </button>
                                        <div className="col-2"></div>
                                        <button className={classnames({
                                            'btn col-5': true,
                                            'btn-gradient': this.state.activeTab !== 'mobile',
                                            'btn-gradient-inactive': this.state.activeTab === 'mobile',
                                        })} onClick={() => this.toggle('ib')}>
                                            Internet Banking
                                        </button>
                                    </div>
                                </div>
                                <div className="col-12" style={{padding:"0px"}}>
                                    <a href="#" onClick={() => this.expandeToogleBni()}>
                                        <div className="col-12">
                                            <div className="row" style={{borderBottom:"1px solid #b8b8b87a", margin:"10px 0px", paddingBottom:"20px"}}>
                                                <img className="col-3" src="/assets/media/img/bank/mbank-05.png"  />
                                                <div className="col-7"></div>
                                                {this.state.expandedBni == false ?
                                                    <img className="col-2" src="/assets/media/img/bank/mb-ar-down.png" />
                                                    :
                                                    <img className="col-2" src="/assets/media/img/bank/mb-ar-up.png" />
                                                }
                                            </div>
                                        </div>
                                    </a>
                                        
                                    <Collapse isOpen={this.state.expandedBni} style={{paddingTop:"10px"}}>
                                        <div className="step-wrapper-odd">
                                            <div className="row">
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.NAME}</b></label>
                                                    <label className="control-label col-8 bold">{this.state.fullName}</label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.BANK_NAME}</b></label>
                                                    <label className="control-label col-8 bold">{this.state.bankBni.BankName}</label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.ACCOUNT_NUMBER}</b></label>
                                                    <label className="control-label col-8 bold">
                                                        {this.state.bankBni.AccountNumber} &nbsp;
                                                        <CopyToClipboard text={this.state.bankBni.AccountNumber} onCopy={() => this.setState({ copied: true})}>
                                                            <i className="fa fa-copy is-link"></i>
                                                        </CopyToClipboard>
                                                        &nbsp; {this.state.copied && <span>{strings.COPIED}</span>}
                                                    </label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.ESTIMATED_TIME}</b></label>
                                                    {this.state.activeTab === 'ib' ?
                                                        <label className="control-label col-8 bold">{strings.FIVE_HOURS}</label>
                                                        :<label className="control-label col-8 bold">{strings.FIVE_MINUTES}</label>
                                                    }
                                                </div>
                                            </div>
                                            {this.state.activeTab === 'mobile' &&
                                                <MobileAppGuide
                                                    fullName={this.state.fullName}
                                                    bankName={this.state.bankBni.bankName}
                                                    accountNumber={this.state.bankBni.accountNumber}
                                                />
                                            }
                                            {this.state.activeTab === 'ib' &&
                                                <InternetBankingGuide
                                                    fullName={this.state.fullName}
                                                    bankName={this.state.bankBni.bankName}
                                                    accountNumber={this.state.bankBni.accountNumber}
                                                />
                                            }
                                        </div>
                                    </Collapse>
                                </div>
                                <div className="col-12" style={{padding:"0px"}}>
                                    <a href="#" onClick={() => this.expandeToogleBri()}>
                                        <div className="col-12">
                                            <div className="row" style={{borderBottom:"1px solid #b8b8b87a", margin:"10px 0px", paddingBottom:"20px"}}>
                                                <img className="col-3" src="/assets/media/img/bank/BRI.png"  />
                                                <div className="col-7"></div>
                                                {this.state.expandedBri == false ?
                                                    <img className="col-2" src="/assets/media/img/bank/mb-ar-down.png" />
                                                    :
                                                    <img className="col-2" src="/assets/media/img/bank/mb-ar-up.png" />
                                                }
                                            </div>
                                        </div>
                                    </a>

                                    <Collapse isOpen={this.state.expandedBri} style={{paddingTop:"10px"}}>
                                        <div className="step-wrapper-odd">
                                            <div className="row">
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.NAME}</b></label>
                                                    <label className="control-label col-8 bold">{this.state.fullName}</label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.BANK_NAME}</b></label>
                                                    <label className="control-label col-8 bold">{this.state.bankBri.BankName}</label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.ACCOUNT_NUMBER}</b></label>
                                                    <label className="control-label col-8 bold">
                                                        {this.state.bankBri.AccountNumber} &nbsp;
                                                        <CopyToClipboard text={this.state.bankBri.AccountNumber} onCopy={() => this.setState({ copied: true})}>
                                                            <i className="fa fa-copy is-link"></i>
                                                        </CopyToClipboard>
                                                        &nbsp; {this.state.copied && <span>{strings.COPIED}</span>}
                                                    </label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.ESTIMATED_TIME}</b></label>
                                                    {this.state.activeTab === 'ib' ?
                                                        <label className="control-label col-8 bold">{strings.FIVE_HOURS}</label>
                                                        :<label className="control-label col-8 bold">{strings.FIVE_MINUTES}</label>
                                                    }
                                                </div>
                                            </div>
                                            {this.state.activeTab === 'mobile' &&
                                                <MobileAppGuide
                                                    fullName={this.state.fullName}
                                                    bankName={this.state.bankBri.bankName}
                                                    accountNumber={this.state.bankBri.accountNumber}
                                                />
                                            }
                                            {this.state.activeTab === 'ib' &&
                                                <InternetBankingGuide
                                                    fullName={this.state.fullName}
                                                    bankName={this.state.bankBri.bankName}
                                                    accountNumber={this.state.bankBri.accountNumber}
                                                />
                                            }
                                        </div>
                                    </Collapse>
                                </div>
                                <div className="col-12" style={{padding:"0px"}}>
                                    <a href="#" onClick={() => this.expandeTooglemandiri()}>
                                        <div className="col-12">
                                            <div className="row" style={{borderBottom:"1px solid #b8b8b87a", margin:"10px 0px", paddingBottom:"20px"}}>
                                                <img className="col-3" src="/assets/media/img/bank/Mandiri.png"  />
                                                <div className="col-7"></div>
                                                {this.state.expandedMandiri == false ?
                                                    <img className="col-2" src="/assets/media/img/bank/mb-ar-down.png" />
                                                    :
                                                    <img className="col-2" src="/assets/media/img/bank/mb-ar-up.png" />
                                                }
                                            </div>
                                        </div>
                                    </a>

                                    <Collapse isOpen={this.state.expandedMandiri} style={{paddingTop:"10px"}}>
                                        <div className="step-wrapper-odd">
                                            <div className="row">
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.NAME}</b></label>
                                                    <label className="control-label col-8 bold">{this.state.fullName}</label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.BANK_NAME}</b></label>
                                                    <label className="control-label col-8 bold">{this.state.bankMandiri.BankName}</label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.ACCOUNT_NUMBER}</b></label>
                                                    <label className="control-label col-8 bold">
                                                        {this.state.bankMandiri.AccountNumber} &nbsp;
                                                        <CopyToClipboard text={this.state.bankMandiri.AccountNumber} onCopy={() => this.setState({ copied: true})}>
                                                            <i className="fa fa-copy is-link"></i>
                                                        </CopyToClipboard>
                                                        &nbsp; {this.state.copied && <span>{strings.COPIED}</span>}
                                                    </label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.ESTIMATED_TIME}</b></label>
                                                    {this.state.activeTab === 'ib' ?
                                                        <label className="control-label col-8 bold">{strings.FIVE_HOURS}</label>
                                                        :<label className="control-label col-8 bold">{strings.FIVE_MINUTES}</label>
                                                    }
                                                </div>
                                            </div>
                                            {this.state.activeTab === 'mobile' &&
                                                <MobileAppGuide
                                                    fullName={this.state.fullName}
                                                    bankName={this.state.bankMandiri.bankName}
                                                    accountNumber={this.state.bankMandiri.accountNumber}
                                                />
                                            }
                                            {this.state.activeTab === 'ib' &&
                                                <InternetBankingGuide
                                                    fullName={this.state.fullName}
                                                    bankName={this.state.bankMandiri.bankName}
                                                    accountNumber={this.state.bankMandiri.accountNumber}
                                                />
                                            }
                                        </div>
                                    </Collapse>
                                </div>

                                <div className="col-12" style={{padding:"0px"}}>
                                    <a href="#" onClick={() => this.expandeTooglePermata()}>
                                        <div className="col-12">
                                            <div className="row" style={{borderBottom:"1px solid #b8b8b87a", margin:"10px 0px", paddingBottom:"20px"}}>
                                                <img className="col-3" src="/assets/media/img/bank/Permata.png"  />
                                                <div className="col-7"></div>
                                                {this.state.expandedPermata == false ?
                                                    <img className="col-2" src="/assets/media/img/bank/mb-ar-down.png" />
                                                    :
                                                    <img className="col-2" src="/assets/media/img/bank/mb-ar-up.png" />
                                                }
                                            </div>
                                        </div>
                                    </a>

                                    <Collapse isOpen={this.state.expandedPermata} style={{paddingTop:"10px"}}>
                                        <div className="step-wrapper-odd">
                                            <div className="row">
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.NAME}</b></label>
                                                    <label className="control-label col-8 bold">{this.state.fullName}</label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.BANK_NAME}</b></label>
                                                    <label className="control-label col-8 bold">{this.state.bankPermata.BankName}</label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.ACCOUNT_NUMBER}</b></label>
                                                    <label className="control-label col-8 bold">
                                                        {this.state.bankPermata.AccountNumber} &nbsp;
                                                        <CopyToClipboard text={this.state.bankPermata.AccountNumber} onCopy={() => this.setState({ copied: true})}>
                                                            <i className="fa fa-copy is-link"></i>
                                                        </CopyToClipboard>
                                                        &nbsp; {this.state.copied && <span>{strings.COPIED}</span>}
                                                    </label>
                                                </div>
                                                <div className="col-12">
                                                    <label className="control-label col-4"><b>{strings.ESTIMATED_TIME}</b></label>
                                                    {this.state.activeTab === 'ib' ?
                                                        <label className="control-label col-8 bold">{strings.FIVE_HOURS}</label>
                                                        :<label className="control-label col-8 bold">{strings.FIVE_MINUTES}</label>
                                                    }
                                                </div>
                                            </div>
                                            {this.state.activeTab === 'mobile' &&
                                                <MobileAppGuide
                                                    fullName={this.state.fullName}
                                                    bankName={this.state.bankPermata.bankName}
                                                    accountNumber={this.state.bankPermata.accountNumber}
                                                />
                                            }
                                            {this.state.activeTab === 'ib' &&
                                                <InternetBankingGuide
                                                    fullName={this.state.fullName}
                                                    bankName={this.state.bankPermata.bankName}
                                                    accountNumber={this.state.bankPermata.accountNumber}
                                                />
                                            }
                                        </div>
                                    </Collapse>
                                </div>
                            </div>

                            {/*{this.state.activeTab === 'ib' &&*/}
                                {/*<div style={{textAlign: 'center'}}>{strings.COMING_SOON}</div>*/}
                            {/*}*/}

                            {/* {this.state.activeTab === 'mobile' &&
                            <MobileAppGuide
                                fullName={this.state.fullName}
                                bankName={this.state.bankName}
                                accountNumber={this.state.accountNumber}
                            />} */}

                        </div>}

                    </ModalBody>
                </Modal>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        language: state.language
    }
}

const mapDispatchToProps = dispatch => ({
    fetchBankDepositAccounts: actions.fetchBankDepositAccounts
})

export default connect(mapStateToProps, mapDispatchToProps, null)(DepositModal)
