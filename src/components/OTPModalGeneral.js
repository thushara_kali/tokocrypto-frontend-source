import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import actions from '../core/actions/client/otp';
import { Loader } from './tcLoader';
import NetworkError from '../errors/networkError';
import { NotificationManager } from 'react-notifications';
import classnames from 'classnames';
import amountFormatter from '../helpers/amountFormatter';
import history from '../history';
import cryptoFormatter from '../helpers/cryptoFormatter';
import LocalizedStrings from 'react-localization';
import enDictionary from '../languages/en.json';
import idDictionary from '../languages/id.json';
import javaDictionary from '../languages/java.json';
import sundaDictionary from '../languages/sunda.json';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class OTPModalGeneral extends Component {

  constructor() {
    super();
    this.state = {
      method: "email",
      token: "",
      maskedToken: "",
      verifyRequested: false,
      loading: false,
      loadingOTP:false,
      loadingResend: false,
      OTPSent: false
    }
  }

  componentWillReceiveProps(nextProps) {
        if(this.props.show === false) {
            this.setState({
            token: "",
            maskedToken: "",
            verifyRequested: false,
            loading: false,
            loadingOTP:false,
            loadingResend: false,
            OTPSent: false
            });
        }
      if(nextProps.show === true && this.state.verifyRequested === false){
          // on open
          
          this.requestVerification();
      }
  }

  getToken() {
    let username = this.props.user.username
    let clientId = this.props.user.pool.clientId
    let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
    return token
  }

  onSetLanguage(){
    strings.setLanguage(this.props.language);
  }

  changeMethod(method) {
    if (method === 'sms') {
      this.setState({
        maskedToken: "",
        method: method
      });
    } else {
      this.setState({
        method: method
      });
    }

  }

  requestVerification(loadingResend = false) {

    this.setState({ loading: true});

    let data = {
      type: this.state.method
    }

    this.setState({ loadingOTP: true, loadingResend, OTPSent: false, verifyRequested: true });

    if(loadingResend === false) {
        this.setState({ token: ""});
    }

    actions.requestOTP(this.getToken(), data).then(res => {
      let response = res.data;
      let OTPSent = false;
      if(loadingResend) {
          // is resend
        OTPSent = true;
      }

      

      if(this.props.method === 'email') {
        NotificationManager.success('OTP sent.');
      }

      if (this.state.method === 'misscall') {
        this.setState({
          maskedToken: response.data.MaskedToken,
          otpId: response.data.OtpId,
          verifyRequested: true,
          method: response.data.Type,
          loadingResend: false,
          OTPSent
        });
      } else {
        this.setState({
          otpId: response.data.OtpId,
          verifyRequested: true,
          loadingOTP: false,
          method: response.data.Type,
          loadingResend: false,
          OTPSent
        });
      }

      this.setState({ loading: false});

    }).catch(err => {
      console.error(err);
      this.setState({ loading: false, loadingResend: false });
      NetworkError(err);
    });
  }

  completeVerification() {
    if (this.state.token.length > 0) {
        this.setState({
            loading: true
        });

        actions.clientMfaVerify(this.state.otpId, this.state.token).then(res => {
            this.props.completeVerification(this.state.otpId, this.state.token);
            if(!this.props.preventClose) {
                this.props.toggleShow();
            }
            // this.setState({
            //     loading: false
            // });
            // NotificationManager.success("Verification success");
        }).catch(err => {
          NetworkError(err);
          this.setState({
              loading: false,
              invalidToken: true
          });
          // NotificationManager.error("Invalid verification code");
          // this.requestVerification();
        })

    }
  }

  close(){
    this.props.toggleShow()
    setTimeout(() => {
        this.setState({
            method: "email",
            token: "",
            maskedToken: "",
            verifyRequested: false,
            loading: false,
            loadingOTP:false,
            invalidToken: false
          });
    }, 50);

  }

  gotoAccounts(){
    this.close();
    if(this.props.summary) {
        history.push('/accounts?currency='+this.props.summary.currency.toUpperCase());
    } else {
        history.push('/accounts?currency=IDR');
    }

  }

  componentWillMount() {
    this.onSetLanguage();
  }

  render() {
    return (
      <div>
        <Modal isOpen={this.props.show} toggle={() => this.close()} className={this.props.className}>
          <ModalHeader toggle={() => this.close()}>
            { this.props.transactionComplete? 'Success' : 'Verification' }
          </ModalHeader>
          <ModalBody className="tc-modal-body">
            <div className="row">
              <div className="col-md-12">
                <div className="form">
                  <br />
                  <div className="form-group">
                    {
                      this.props.method === 'email' &&
                      <p> {strings.YOU_WILL_EMAIL_VERIFICATION_TOKEN}.</p>
                    }
                    {
                      this.props.method === 'ga' &&
                      <p> {strings.ENTER_VERIFICATION_GOOGLE_AUTHENTICATOR_APP}.</p>
                    }
                  </div>
                  <br />
                  <div className="form-group">
                      <input disabled={this.state.loadingOTP} type="text" className={classnames({ "form-control": true, "is-invalid": this.state.invalidToken })} value={this.state.token} onChange={(e) => this.setState({ token: e.target.value })} placeholder={strings.YOUR_VERIFICATION_TOKEN} />
                  </div>
                  {  !this.state.loadingResend && !this.state.OTPSent && this.props.method === 'email' && <a className="float-right is-link" onClick={() => this.requestVerification(true)}>
                      {strings.RESEND_OTP}
                    </a> }
                  {  this.state.loadingResend && this.props.method === 'email' && <a className="float-right is-link" href="#">
                      {strings.SENDING_OTP}...
                  </a>}
                  {  this.state.OTPSent && this.props.method === 'email' && <a className="float-right is-link" onClick={() => this.requestVerification(true)}>
                      {strings.OTP_SENT}. {strings.CLICK_TO_RESEND}
                  </a>}
                </div>
              </div>
            </div>
          </ModalBody>
          <ModalFooter className="tc-modal-footer">
                {this.state.loading ? <div className="mx-auto"><Loader type="line-scale" active={true} className="text-center" /></div> :
                <button disabled={this.state.token.length === 0} className="btn-block btn btn-blue" onClick={() => this.completeVerification()}>{strings.VERIFY}</button>}
            </ModalFooter>
        </Modal>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    user: state.cognito.user,
    kyc: state.kyc,
    language: state.language
  };
};

export default connect(mapStateToProps, null)(OTPModalGeneral);
