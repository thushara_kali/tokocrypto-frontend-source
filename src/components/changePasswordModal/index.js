import React, { Component } from 'react';
import classnames from 'classnames';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import { getUserAttributes, updateAttributes } from '../../core/react-cognito/attributes.js'
import { changePassword } from '../../core/react-cognito/utils.js'
import { NotificationManager } from 'react-notifications'
import GeneralOTP from '../../components/OTPModalGeneral';
import PasswordChecker from '../../components/passwordCheckerSimple';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import NetworkError from '../../errors/networkError';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class ChangePasswordModal extends Component {

    constructor(){
        super();
        this.state = {
            currentPassword: '',
            newPassword: '',
            repeatNewPassword: '',
            showOTP: false,
            help: true,
            passwordValid: false
        }

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            obj[fieldName] = event.target.value
            this.setState(obj)
        }
    }

    toggleShow() {
        let currentOTP = this.state.showOTP;

        if(currentOTP === true) {
            this.setState({
                showOTP: !this.state.showOTP,
                help: false
            });
        } else {
            this.setState({
                showOTP: !this.state.showOTP
            });
        }
    }

    onSetLanguage(){
		strings.setLanguage(this.props.language);
	}

    doUpdatePassword() {
        this.setState({
            loading: true
        });

        
	    

        if (this.state.currentPassword.length > 0) {
            changePassword(this.props.user, this.state.currentPassword, this.state.newPassword).then(res => {
                this.setState({
                    loading: false,
                    showOTP: false
                });
                NotificationManager.success('Password updated');
                this.close();
            })
                .catch(err => {
                    this.setState({
                        loading: false,
                        showOTP: false
                    });
                    let message = 'Unexpected error';
                    if(err.toString().length !== 0) {
                        message = err.toString();
                    }
                    
                    let errorDetails = {
                        type: 'COMPONENTS: ChangePasswordModal | FUNC: doUpdatePassword',
                        url: 'react-cognito'
                    }
                    NetworkError(err, errorDetails);

                    // NotificationManager.error(message);
                    this.close();
                })
        }
    }

    onUpdatePassword() {
        this.toggleShow();
    }

    onBlurHelp(e) {
        
        // 
        if(!e.target.classList.contains('keep-help')){
            this.setState({
                help: false
            });
        } else {
            this.setState({
                help: true
            });
        }
    }

    forceOpenHelpText(){
        this.setState({
            help: true
        });
    }

    close() {
    	this.setState({
		    newPassword: ''
	    });
    	this.props.toggleShow();
    }

    componentWillMount() {
		this.onSetLanguage();
	}

    render(){

        let invalidPassword = this.state.newPassword.length !== 0 && (this.state.newPassword !== this.state.repeatNewPassword) && this.state.repeatNewPassword.length !== 0

        return (
            <div>
                <Modal size={"lg"} isOpen={this.props.show} toggle={() => this.close()} className={"modal-dialog-centered"} style={{ justifyContent: "center" }}size="lg">
                    <ModalHeader toggle={() => this.close()}>
                            {strings.CHANGE_PASSWORD}
                    </ModalHeader>
                    <ModalBody onMouseUp={(e) => this.onBlurHelp(e)} style={{ width: "500px"}}>
                        <div className="form-group">
                            <label className="control-label" >{strings.CURRENT_PASSWORD}</label>
                            <input type="password" onChange={this.handleChange('currentPassword')} className="form-control" />
                        </div>
                        <div className="form-group">
                            <label className="control-label" >{strings.NEW_PASSWORD}</label>
                            <PasswordChecker forceOpenHelpText={() => this.forceOpenHelpText()} help={this.state.help} value={this.state.newPassword} onChange={this.handleChange('newPassword')} setValid={(valid) => this.setState({ passwordValid: valid})}/>
                        </div>
                        <div className="form-group">
                            <label className="control-label" >{strings.REPEAT_NEW_PASSWORD}</label>
                            <input type="password" onChange={this.handleChange('repeatNewPassword')} className={classnames({
                                'form-control': true,
                                'is-invalid': invalidPassword
                            })} />
                            <div className="invalid-feedback">
                                {strings.REPEAT_PASSWORD_NOT_MATCH}
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button disabled={this.state.loading} color="secondary" onClick={() => this.close()}>{strings.CANCEL}</Button>
                        {
                            !this.state.loading?
                            <Button disabled={!this.state.passwordValid || invalidPassword || this.state.currentPassword.length === 0 || this.state.newPassword === 0 || this.state.repeatNewPassword.length === 0} color="primary" onClick={() => this.onUpdatePassword()}>{strings.CHANGE_PASSWORD}</Button> :
                            <Button disabled={true} color="primary">{strings.CHANGING_PASSWORD} ...</Button>
                        }
                    </ModalFooter>
                </Modal>

                <GeneralOTP preventClose={true} completeVerification={() => this.doUpdatePassword()} show={this.state.showOTP} method={this.props.method} toggleShow={() => this.toggleShow()}/>
            </div>

        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        language: state.language
    };
};

export default connect(mapStateToProps, null) (ChangePasswordModal);
