import React, {Component} from 'react';

export class Loader extends Component {
    render() {
        return (
            <img
                style={{
                    height: this.props.size,
                    width: this.props.size  
                }}
                src="/assets/loader.gif"
            />
        )
    }
}

Loader.defaultProps = {
    size: 50
}
  