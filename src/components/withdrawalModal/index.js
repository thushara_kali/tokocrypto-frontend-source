import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import bankActions from '../../core/actions/client/banks';
import withdrawActions from '../../core/actions/client/withdraw';
import NetworkError from '../../errors/networkError';
import OTPModal from '../OTPModal';
import history from '../../history';
import { NotificationManager } from 'react-notifications';
import amountFormatter from '../../helpers/amountFormatter';
// import Cleave from 'cleave.js/react';
import Cleave from '../../components/customCleave';
import Select from 'react-select'
import _ from 'lodash';
import numeral from 'numeral';
import OTPActions from '../../core/actions/client/otp';
import classnames from 'classnames';
import { Loader } from '../../components/tcLoader';
import { getUserAttributes, updateAttributes } from '../../core/react-cognito/attributes.js';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

const banksList = [
    'BCA', 
    'GOPAY',
    'OVO',
    'LINK AJA',
    'DANA',
    'ARTAJASA PEMBAYARAN ELEK. (RTGS)',
    'BANGKOK BANK PUBLIC CO.LTD',
    'BANK INDEX SELINDO',
    'BANK INDONESIA',
    'BANK INDONESIA KP JAKARTA',
    'BANK OF AMERICA NA',
    'BANK OF CHINA (HONG KONG) LIMITED',
    'BI CABANG AMBON,AMB',
    'BI CABANG BALIKPAPAN,BLP',
    'BI CABANG BANDA ACEH,BDA',
    'BI CABANG BANDAR LAMPUNG,BDL',
    'BI CABANG BANDUNG,BDG',
    'BI CABANG BANJARMASIN',
    'BI CABANG BATAM,BTM',
    'BI CABANG BENGKULU',
    'BI CABANG CIREBON,CRB',
    'BI CABANG DENPASAR,BALI',
    'BI CABANG JAMBI',
    'BI CABANG JAYAPURA',
    'BI CABANG JEMBER,JBR',
    'BI CABANG KEDIRI,KDR',
    'BI CABANG KENDARI,KDI',
    'BI CABANG KUPANG,KPG',
    'BI CABANG LHOKSEUMAWE,LSM',
    'BI CABANG MALANG,MLG',
    'BI CABANG MANADO,MDO',
    'BI CABANG MATARAM,CKA',
    'BI CABANG MEDAN,MDN',
    'BI CABANG PADANG,PDG',
    'BI CABANG PALANGKARAYA',
    'BI CABANG PALEMBANG,PLG',
    'BI CABANG PALU,PLS',
    'BI CABANG PEKAN BARU,PKB',
    'BI CABANG PONTIANAK,PTK',
    'BI CABANG SAMARINDA,SMD',
    'BI CABANG SEMARANG,SMG',
    'BI CABANG SIBOLGA',
    'BI CABANG SOLO,SLO',
    'BI CABANG SURABAYA,SBY',
    'BI CABANG TEGAL',
    'BI CABANG TERNATE',
    'BI CABANG UJUNG PANDANG',
    'BI CABANG YOGYAKARTA,YOG',
    'BI PURWOKERTO,PWT',
    'BI TASIKMALAYA,TSM',
    'BPD DKI SYARIAH',
    'BPD JAMBI SYARIAH',
    'BPD JATENG SYARIAH',
    'BPD JATIM SYARIAH',
    'BPD KALBAR SYARIAH',
    'BPD KALSEL SYARIAH',
    'BPD KALTIM & KALTARA SYARIAH',
    'BPD RIAU SYARIAH',
    'BPD SULSELBAR SYARIAH',
    'BPD SUMBAR SYARIAH',
    'BPD SUMSEL BABEL SYARIAH',
    'BPD SUMUT SYARIAH',
    'BPD YOGYAKARTA SYARIAH',
    'BTN SYARIAH',
    'CIMB NIAGA SYARIAH',
    'CITIBANK, NA',
    'DANAMON SYARIAH',
    'DEUTSCHE BANK AG.',
    'FINNET INDONESIA (RTGS)',
    'INDONESIA EXIMBANK (RTGS)',
    'JALIN PEMBAYARAN NUSANTARA (JALIN)',
    'KC JPMORGAN CHASE BANK, N.A',
    'KSEI (RTGS)',
    'KSEI(REKSADANA)',
    'MAYBANK INDONESIA SYARIAH',
    'MUFG Bank, Ltd.',
    'OCBC NISP SYARIAH',
    'PERMATA SYARIAH',
    'PT ALTO NETWORK (PT ALTO)',
    'PT BANK BNI SYARIAH',
    'PT BANK CAPITAL INDONESIA',
    'PT RINTIS SEJAHTERA (PT RINTIS)',
    'PT. BANK ACEH SYARIAH',
    'PT. BANK AGRIS',
    'PT. BANK AMAR INDONESIA',
    'PT. BANK ANZ INDONESIA',
    'PT. BANK ARTHA GRAHA INTERNASIONAL, TBK',
    'PT. BANK ARTOS INDONESIA',
    'PT. BANK BCA SYARIAH',
    'PT. BANK BISNIS INTERNASIONAL',
    'PT. BANK BNP PARIBAS INDONESIA',
    'PT. BANK BUKOPIN Tbk.',
    'PT. BANK BUMI ARTA',
    'PT. BANK CHINA CONSTRUCTION BANK INDONESIA, TBK',
    'PT. BANK CIMB NIAGA TBK',
    'PT. BANK COMMONWEALTH',
    'PT. BANK CTBC INDONESIA',
    'PT. BANK DANAMON INDONESIA Tbk.',
    'PT. BANK DBS INDONESIA',
    'PT. BANK DINAR INDONESIA',
    'PT. BANK DKI',
    'PT. BANK FAMA INTERNATIONAL',
    'PT. BANK GANESHA',
    'PT. BANK HARDA INTERNATIONAL',
    'PT. BANK HSBC INDONESIA',
    'PT. BANK ICBC INDONESIA',
    'PT. BANK INA PERDANA',
    'PT. BANK JABAR BANTEN SYARIAH',
    'PT. BANK JASA JAKARTA',
    'PT. BANK JTRUST INDONESIA, TBK',
    'PT. BANK KEB HANA INDONESIA',
    'PT. BANK KESEJAHTERAAN EKONOMI',
    'PT. BANK MANDIRI (PERSERO) TBK',
    'PT. BANK MANDIRI TASPEN',
    'PT. BANK MASPION',
    'PT. BANK MAYAPADA Tbk.',
    'PT. BANK MAYBANK INDONESIA Tbk.',
    'PT. BANK MAYBANK SYARIAH INDONESIA',
    'PT. BANK MAYORA',
    'PT. BANK MEGA SYARIAH',
    'PT. BANK MEGA Tbk.',
    'PT. BANK MESTIKA DHARMA',
    'PT. BANK MITRANIAGA',
    'PT. BANK MIZUHO INDONESIA',
    'PT. BANK MNC INTERNASIONAL, TBK',
    'PT. BANK MUAMALAT INDONESIA',
    'PT. BANK MULTI ARTA SENTOSA',
    'PT. BANK NATIONALNOBU',
    'PT. BANK NEGARA INDONESIA (PERSERO)',
    'PT. BANK NUSANTARA PARAHYANGAN',
    'PT. BANK OCBC NISP, Tbk.',
    'PT. BANK OF INDIA INDONESIA, TBK',
    'PT. BANK OKE INDONESIA',
    'PT. BANK PANIN DUBAI SYARIAH',
    'PT. BANK PEMBANGUNAN DAERAH  JABAR DAN BANTEN',
    'PT. BANK PEMBANGUNAN DAERAH  JATIM',
    'PT. BANK PEMBANGUNAN DAERAH  SULUT',
    'PT. BANK PEMBANGUNAN DAERAH BALI',
    'PT. BANK PEMBANGUNAN DAERAH BANTEN',
    'PT. BANK PEMBANGUNAN DAERAH DIY',
    'PT. BANK PEMBANGUNAN DAERAH JAWA TENGAH',
    'PT. BANK PEMBANGUNAN DAERAH KALSEL',
    'PT. BANK PEMBANGUNAN DAERAH KALTIM DAN KALTARA',
    'PT. BANK PEMBANGUNAN DAERAH NUSA TENGGARAT BARAT S',
    'PT. BANK PEMBANGUNAN DAERAH RIAU KEPRI',
    'PT. BANK PEMBANGUNAN DAERAH SUMATERA BARAT',
    'PT. BANK PEMBANGUNAN DAERAH SUMUT',
    'PT. BANK PERMATA,TBK',
    'PT. BANK QNB INDONESIA,TBK',
    'PT. BANK RABOBANK INTERNATIONAL INDONESIA',
    'PT. BANK RAKYAT INDONESIA (PERSERO)',
    'PT. BANK RAKYAT INDONESIA AGRONIAGA, TBK',
    'PT. BANK RESONA PERDANIA',
    'PT. BANK ROYAL INDONESIA',
    'PT. BANK SAHABAT SAMPOERNA',
    'PT. BANK SBI INDONESIA',
    'PT. BANK SHINHAN INDONESIA',
    'PT. BANK SINARMAS',
    'PT. BANK SULSELBAR',
    'PT. BANK SYARIAH BRI',
    'PT. BANK SYARIAH BUKOPIN',
    'PT. BANK SYARIAH MANDIRI Tbk.',
    'PT. BANK TABUNGAN NEGARA (PERSERO)',
    'PT. BANK TABUNGAN PENSIUNAN NASIONAL SYARIAH',
    'PT. BANK UOB INDONESIA',
    'PT. BANK VICTORIA INTERNATIONAL',
    'PT. BANK VICTORIA SYARIAH',
    'PT. BANK WOORI SAUDARA INDONESIA 1906,TBK',
    'PT. BANK YUDHA BHAKTI',
    'PT. BPD BENGKULU',
    'PT. BPD KALIMANTAN TENGAH',
    'PT. BPD SULAWESI TENGAH',
    'PT. BPD SULAWESI TENGGARA',
    'PT. BPD SUMSEL DAN BABEL',
    'PT. Bank BTPN, Tbk.',
    'PT. PANIN BANK Tbk.',
    'PT. PRIMA MASTER BANK',
    'PT.BANK PEMBANGUNAN DAERAH JAMBI',
    'PT.BANK PEMBANGUNAN DAERAH KALBAR',
    'PT.BANK PEMBANGUNAN DAERAH LAMPUNG',
    'PT.BANK PEMBANGUNAN DAERAH MALUKU DAN MALUKU UTARA',
    'PT.BANK PEMBANGUNAN DAERAH NTT',
    'PT.BANK PEMBANGUNAN DAERAH PAPUA',
    'SINARMAS SYARIAH',
    'STANDARD CHARTERED BANK'
]; //Nicholas' change. Added requested banks

const cityList = [ 
    'AMBON',
    'BANDA ACEH',
    'BANDAR LAMPUNG',
    'BANDUNG',
    'BANJARMASIN',
    'BATAM',
    'BENGKULU',
    'DENPASAR',
    'JAKARTA',
    'JAMBI',
    'JAYAPURA',
    'KAB.   BARU',
    'KAB.  ACEH BARAT',
    'KAB.  ACEH BARAT DAYA',
    'KAB.  ACEH BESAR',
    'KAB.  ACEH JAYA',
    'KAB.  ACEH SELATAN',
    'KAB.  ACEH SINGKIL',
    'KAB.  ACEH TAMIANG',
    'KAB.  ACEH TENGAH',
    'KAB.  ACEH TENGGARA',
    'KAB.  ACEH TIMUR',
    'KAB.  ACEH UTARA',
    'KAB.  AGAM',
    'KAB.  ALOR',
    'KAB.  ANAMBAS',
    'KAB.  ASAHAN',
    'KAB.  ASMAT',
    'KAB.  BADUNG',
    'KAB.  BALANGAN',
    'KAB.  BANDUNG',
    'KAB.  BANDUNG BARAT',
    'KAB.  BANGGAI',
    'KAB.  BANGKA',
    'KAB.  BANGKA BARAT',
    'KAB.  BANGKA SELATAN',
    'KAB.  BANGKA TENGAH',
    'KAB.  BANGKALAN',
    'KAB.  BANGLI',
    'KAB.  BANJAR',
    'KAB.  BANJARNEGARA',
    'KAB.  BANTAENG',
    'KAB.  BANTUL',
    'KAB.  BANYUASIN',
    'KAB.  BANYUMAS',
    'KAB.  BANYUWANGI',
    'KAB.  BARITO KUALA',
    'KAB.  BARITO SELATAN',
    'KAB.  BARITO TIMUR',
    'KAB.  BARITO UTARA',
    'KAB.  BARRU',
    'KAB.  BATANG',
    'KAB.  BATANGHARI',
    'KAB.  BATU BARA',
    'KAB.  BEKASI',
    'KAB.  BELITUNG',
    'KAB.  BELITUNG TIMUR',
    'KAB.  BELU',
    'KAB.  BENER MERIAH',
    'KAB.  BENGKALIS',
    'KAB.  BENGKAYANG',
    'KAB.  BENGKULU SELATAN',
    'KAB.  BENGKULU TENGAH',
    'KAB.  BENGKULU UTARA',
    'KAB.  BERAU',
    'KAB.  BIAK NUMFOR',
    'KAB.  BIMA',
    'KAB.  BINTAN (D/H  KEPULAUAN RIAU)',
    'KAB.  BIREUEN',
    'KAB.  BLITAR',
    'KAB.  BLORA',
    'KAB.  BOALEMO',
    'KAB.  BOGOR',
    'KAB.  BOJONEGORO',
    'KAB.  BOLAANG MONGONDOW',
    'KAB.  BOLAANG MONGONDOW SELATAN',
    'KAB.  BOLAANG MONGONDOW TIMUR',
    'KAB.  BOLAANG MONGONDOW UTARA',
    'KAB.  BOMBANA',
    'KAB.  BONDOWOSO',
    'KAB.  BONE',
    'KAB.  BONEBOLANGO',
    'KAB.  BOVEN DIGOEL',
    'KAB.  BOYOLALI',
    'KAB.  BREBES',
    'KAB.  BULELENG',
    'KAB.  BULUKUMBA',
    'KAB.  BULUNGAN',
    'KAB.  BUNGO',
    'KAB.  BUOL',
    'KAB.  BURU SELATAN',
    'KAB.  BUTON',
    'KAB.  BUTON UTARA',
    'KAB.  CIAMIS',
    'KAB.  CIANJUR',
    'KAB.  CILACAP',
    'KAB.  CIREBON',
    'KAB.  DAIRI',
    'KAB.  DEIYAI',
    'KAB.  DELI SERDANG',
    'KAB.  DEMAK',
    'KAB.  DHARMASRAYA',
    'KAB.  DOGIYAI',
    'KAB.  DOMPU',
    'KAB.  DONGGALA',
    'KAB.  EMPAT LAWANG',
    'KAB.  ENDE',
    'KAB.  ENREKANG',
    'KAB.  FLORES TIMUR',
    'KAB.  GARUT',
    'KAB.  GAYO LUES',
    'KAB.  GIANYAR',
    'KAB.  GORONTALO',
    'KAB.  GORONTALO UTARA',
    'KAB.  GOWA',
    'KAB.  GRESIK',
    'KAB.  GROBOGAN',
    'KAB.  GUNUNG KIDUL',
    'KAB.  GUNUNG MAS',
    'KAB.  HALMAHERA BARAT',
    'KAB.  HALMAHERA SELATAN',
    'KAB.  HALMAHERA TENGAH',
    'KAB.  HALMAHERA TIMUR',
    'KAB.  HALMAHERA UTARA',
    'KAB.  HULU SUNGAI SELATAN',
    'KAB.  HULU SUNGAI TENGAH',
    'KAB.  HULU SUNGAI UTARA',
    'KAB.  HUMBANG HASUNDUTAN',
    'KAB.  INDRAGIRI HILIR',
    'KAB.  INDRAGIRI HULU',
    'KAB.  INDRAMAYU',
    'KAB.  INTAN JAYA',
    'KAB.  JAYAPURA',
    'KAB.  JAYAWIJAYA',
    'KAB.  JEMBER',
    'KAB.  JEMBRANA',
    'KAB.  JENEPONTO',
    'KAB.  JEPARA',
    'KAB.  JOMBANG',
    'KAB.  KAMPAR',
    'KAB.  KAPUAS',
    'KAB.  KAPUAS HULU',
    'KAB.  KARANGANYAR',
    'KAB.  KARANGASEM',
    'KAB.  KARAWANG',
    'KAB.  KARIMUN',
    'KAB.  KARO',
    'KAB.  KATINGAN',
    'KAB.  KAUR',
    'KAB.  KAYONG UTARA',
    'KAB.  KEBUMEN',
    'KAB.  KEDIRI',
    'KAB.  KEEROM',
    'KAB.  KENDAL',
    'KAB.  KEPAHIANG',
    'KAB.  KEPULAUAN ARU',
    'KAB.  KEPULAUAN MENTAWAI',
    'KAB.  KEPULAUAN MERANTI',
    'KAB.  KEPULAUAN SANGIHE',
    'KAB.  KEPULAUAN SITARO',
    'KAB.  KEPULAUAN SULA',
    'KAB.  KEPULAUAN TALAUD',
    'KAB.  KERINCI',
    'KAB.  KETAPANG',
    'KAB.  KLATEN',
    'KAB.  KLUNGKUNG',
    'KAB.  KOLAKA',
    'KAB.  KOLAKA UTARA',
    'KAB.  KONAWE',
    'KAB.  KONAWE SELATAN',
    'KAB.  KONAWE UTARA',
    'KAB.  KUANTAN SINGINGI',
    'KAB.  KUBU RAYA',
    'KAB.  KUDUS',
    'KAB.  KULON PROGO',
    'KAB.  KUNINGAN',
    'KAB.  KUPANG',
    ',KAB.  KUTAI BARAT',
    'KAB.  KUTAI KARTANEGARA',
    'KAB.  KUTAI TIMUR',
    'KAB.  LABUHAN BATU',
    'KAB.  LABUHANBATU SELATAN',
    'KAB.  LABUHANBATU UTARA',
    'KAB.  LAHAT',
    'KAB.  LAMANDAU',
    'KAB.  LAMONGAN',
    'KAB.  LAMPUNG BARAT',
    'KAB.  LAMPUNG SELATAN',
    'KAB.  LAMPUNG TENGAH',
    'KAB.  LAMPUNG TIMUR',
    'KAB.  LAMPUNG UTARA',
    'KAB.  LANDAK',
    'KAB.  LANGKAT',
    'KAB.  LANNY JAYA',
    'KAB.  LEBAK',
    'KAB.  LEBONG',
    'KAB.  LEMBATA',
    'KAB.  LIMAPULUH KOTO',
    'KAB.  LINGGA',
    'KAB.  LOMBOK BARAT',
    'KAB.  LOMBOK TENGAH',
    'KAB.  LOMBOK TIMUR',
    'KAB.  LOMBOK UTARA',
    'KAB.  LUMAJANG',
    'KAB.  LUWU',
    'KAB.  LUWU TIMUR (D/H LUWU SELATAN)',
    'KAB.  LUWU UTARA',
    'KAB.  MADIUN',
    'KAB.  MAGELANG',
    'KAB.  MAGETAN',
    'KAB.  MAJALENGKA',
    'KAB.  MALANG',
    'KAB.  MALINAU',
    'KAB.  MALUKU BARAT DAYA',
    'KAB.  MALUKU TENGAH',
    'KAB.  MALUKU TENGGARA',
    'KAB.  MALUKU TENGGARA BARAT',
    'KAB.  MAMBERAMO RAYA',
    'KAB.  MAMBERAMO TENGAH',
    'KAB.  MANDAILING NATAL',
    'KAB.  MANGGARAI',
    'KAB.  MANGGARAI BARAT',
    'KAB.  MANGGARAI TIMUR',
    'KAB.  MAPPI',
    'KAB.  MAROS',
    'KAB.  MELAWI',
    'KAB.  MERANGIN',
    'KAB.  MERAUKE',
    'KAB.  MESUJI',
    'KAB.  MIMIKA',
    'KAB.  MINAHASA',
    'KAB.  MINAHASA SELATAN',
    'KAB.  MINAHASA TENGGARA',
    'KAB.  MINAHASA UTARA',
    'KAB.  MOJOKERTO',
    'KAB.  MOROWALI',
    'KAB.  MUARA ENIM',
    'KAB.  MUARO JAMBI',
    'KAB.  MUKOMUKO',
    'KAB.  MUNA',
    'KAB.  MURUNG RAYA',
    'KAB.  MUSI BANYUASIN',
    'KAB.  MUSI RAWAS',
    'KAB.  NABIRE',
    'KAB.  NAGAN RAYA',
    'KAB.  NAGEKEO',
    'KAB.  NATUNA',
    'KAB.  NDUGA',
    'KAB.  NGADA',
    'KAB.  NGANJUK',
    'KAB.  NGAWI',
    'KAB.  NIAS',
    'KAB.  NIAS BARAT',
    'KAB.  NIAS SELATAN',
    'KAB.  NIAS UTARA',
    'KAB.  NUNUKAN',
    'KAB.  OGAN ILIR',
    'KAB.  OGAN KOMERING ILIR',
    'KAB.  OGAN KOMERING ULU',
    'KAB.  OGAN KOMERING ULU SELATAN',
    'KAB.  OGAN KOMERING ULU TIMUR',
    'KAB.  PACITAN',
    'KAB.  PADANG LAWAS',
    'KAB.  PADANG LAWAS UTARA',
    'KAB.  PADANG PARIAMAN',
    'KAB.  PAKPAK BHARAT',
    'KAB.  PAMEKASAN',
    'KAB.  PANDEGLANG',
    'KAB.  PANGKAJENE KEPULAUAN',
    'KAB.  PANIAI',
    'KAB.  PARIGI MOUTONG',
    'KAB.  PASAMAN',
    'KAB.  PASAMAN BARAT',
    'KAB.  PASIR',
    'KAB.  PASURUAN',
    'KAB.  PATI',
    'KAB.  PEGUNUNGAN BINTANG',
    'KAB.  PEKALONGAN',
    'KAB.  PELALAWAN',
    'KAB.  PEMALANG',
    'KAB.  PENAJAM PASER UTARA',
    'KAB.  PESAWARAN',
    'KAB.  PESISIR SELATAN',
    'KAB.  PIDIE',
    'KAB.  PIDIE JAYA',
    'KAB.  PINRANG',
    'KAB.  POHUWATO',
    'KAB.  PONOROGO',
    'KAB.  PONTIANAK',
    'KAB.  POSO',
    'KAB.  PRINGSEWU',
    'KAB.  PROBOLINGGO',
    'KAB.  PULANG PISAU',
    'KAB.  PULAU MOROTAI',
    'KAB.  PUNCAK',
    'KAB.  PUNCAK JAYA',
    'KAB.  PURBALINGGA',
    'KAB.  PURWAKARTA',
    'KAB.  PURWOREJO',
    'KAB.  REJANG LEBONG',
    'KAB.  REMBANG',
    'KAB.  ROKAN HILIR',
    'KAB.  ROKAN HULU',
    'KAB.  ROTE NDAO',
    'KAB.  SABU RAIJUA',
    'KAB.  SAMBAS',
    'KAB.  SAMOSIR',
    'KAB.  SAMPANG',
    'KAB.  SANGGAU',
    'KAB.  SARMI',
    'KAB.  SAROLANGUN',
    'KAB.  SEKADAU',
    'KAB.  SELAYAR',
    'KAB.  SELUMA',
    'KAB.  SEMARANG',
    'KAB.  SERAM BAGIAN BARAT',
    'KAB.  SERAM BAGIAN TIMUR',
    'KAB.  SERANG',
    'KAB.  SERDANG BEDAGAI',
    'KAB.  SERUYAN',
    'KAB.  SIAK',
    'KAB.  SIDENRENG RAPPANG',
    'KAB.  SIDOARJO',
    'KAB.  SIGI',
    'KAB.  SIJUNJUNG',
    'KAB.  SIKKA',
    'KAB.  SIMALUNGUN',
    'KAB.  SIMEULUE',
    'KAB.  SINJAI',
    'KAB.  SINTANG',
    'KAB.  SITUBONDO',
    'KAB.  SLEMAN',
    'KAB.  SOLOK',
    'KAB.  SOLOK SELATAN',
    'KAB.  SOPPENG (D/H WATANSOPPENG)',
    'KAB.  SRAGEN',
    'KAB.  SUBANG',
    'KAB.  SUKABUMI',
    'KAB.  SUKAMARA',
    'KAB.  SUKOHARJO',
    'KAB.  SUMBA BARAT',
    'KAB.  SUMBA BARAT DAYA',
    'KAB.  SUMBA TENGAH',
    'KAB.  SUMBA TIMUR',
    'KAB.  SUMBAWA',
    'KAB.  SUMBAWA BARAT',
    'KAB.  SUMEDANG',
    'KAB.  SUMENEP',
    'KAB.  SUPIORI',
    'KAB.  TABALONG',
    'KAB.  TABANAN',
    'KAB.  TAKALAR',
    'KAB.  TANA TIDUNG',
    'KAB.  TANA TORAJA',
    'KAB.  TANAH DATAR',
    'KAB.  TANAH LAUT',
    'KAB.  TANGERANG',
    'KAB.  TANGGAMUS',
    'KAB.  TANJUNG JABUNG BARAT',
    'KAB.  TANJUNG JABUNG TIMUR',
    'KAB.  TAPANULI SELATAN',
    'KAB.  TAPANULI TENGAH',
    'KAB.  TAPANULI UTARA',
    'KAB.  TAPIN',
    'KAB.  TASIKMALAYA',
    'KAB.  TEBO',
    'KAB.  TEGAL',
    'KAB.  TEMANGGUNG',
    'KAB.  TIMOR-TENGAH SELATAN',
    'KAB.  TIMOR-TENGAH UTARA',
    'KAB.  TOBA SAMOSIR',
    'KAB.  TOJO UNA-UNA',
    'KAB.  TOLI-TOLI',
    'KAB.  TOLIKARA',
    'KAB.  TORAJA UTARA',
    'KAB.  TRENGGALEK',
    'KAB.  TUBAN',
    'KAB.  TULANG BAWANG',
    'KAB.  TULANG BAWANG BARAT',
    'KAB.  TULUNGAGUNG',
    'KAB.  WAJO',
    'KAB.  WAKATOBI',
    'KAB.  WARINGIN BARAT',
    'KAB.  WARINGIN TIMUR',
    'KAB.  WAROPEN',
    'KAB.  WAY KANAN',
    'KAB.  WONOGIRI',
    'KAB.  WONOSOBO',
    'KAB.  YAHUKIMO',
    'KAB.  YALIMO',
    'KAB.  YAPEN-WAROPEN',
    'KAB. BANGGAI KEPULAUAN',
    'KAB. KAB BURU',
    'KAB. TANAH BUMBU',
    'KENDARI',
    'KOTA  AMBON',
    'KOTA  BALIKPAPAN',
    'KOTA  BANDA ACEH',
    'KOTA  BANDAR LAMPUNG',
    'KOTA  BANDUNG',
    'KOTA  BANJAR',
    'KOTA  BANJARBARU',
    'KOTA  BANJARMASIN',
    'KOTA  BATAM',
    'KOTA  BATU',
    'KOTA  BAU-BAU',
    'KOTA  BEKASI',
    'KOTA  BENGKULU',
    'KOTA  BINJAI',
    'KOTA  BITUNG',
    'KOTA  BLITAR',
    'KOTA  BOGOR',
    'KOTA  BONTANG',
    'KOTA  BUKITTINGGI',
    'KOTA  CILEGON',
    'KOTA  CIMAHI',
    'KOTA  CIREBON',
    'KOTA  DENPASAR',
    'KOTA  DEPOK',
    'KOTA  DUMAI',
    'KOTA  GORONTALO',
    'KOTA  GUNUNG SITOLI',
    'KOTA  JAKARTA BARAT',
    'KOTA  JAKARTA PUSAT',
    'KOTA  JAKARTA SELATAN',
    'KOTA  JAKARTA TIMUR',
    'KOTA  JAKARTA UTARA',
    'KOTA  JAMBI',
    'KOTA  JAYAPURA',
    'KOTA  KEDIRI',
    'KOTA  KENDARI',
    'KOTA  KUPANG',
    'KOTA  LANGSA',
    'KOTA  LHOKSEUMAWE',
    'KOTA  LUBUKLINGGAU',
    'KOTA  MADIUN',
    'KOTA  MAGELANG',
    'KOTA  MAKASSAR',
    'KOTA  MALANG',
    'KOTA  MANADO',
    'KOTA  MATARAM',
    'KOTA  MEDAN',
    'KOTA  METRO',
    'KOTA  MOBAGU',
    'KOTA  MOJOKERTO',
    'KOTA  PADANG',
    'KOTA  PADANG PANJANG',
    'KOTA  PADANG SIDEMPUAN',
    'KOTA  PAGAR ALAM',
    'KOTA  PALANGKARAYA',
    'KOTA  PALEMBANG',
    'KOTA  PALOPO',
    'KOTA  PALU',
    'KOTA  PARE-PARE',
    'KOTA  PARIAMAN',
    'KOTA  PASURUAN',
    'KOTA  PAYAKUMBUH',
    'KOTA  PEKALONGAN',
    'KOTA  PEKANBARU',
    'KOTA  PEMATANG SIANTAR',
    'KOTA  PONTIANAK',
    'KOTA  PRABUMULIH',
    'KOTA  PROBOLINGGO',
    'KOTA  SABANG',
    'KOTA  SALATIGA',
    'KOTA  SAMARINDA',
    'KOTA  SAWAHLUNTO',
    'KOTA  SEMARANG',
    'KOTA  SERANG',
    'KOTA  SIBOLGA',
    'KOTA  SINGKAWANG',
    'KOTA  SOLOK',
    'KOTA  SUBULUSSALAM',
    'KOTA  SUKABUMI',
    'KOTA  SUNGAI PENUH',
    'KOTA  SURABAYA',
    'KOTA  SURAKARTA/SOLO',
    'KOTA  TANGERANG',
    'KOTA  TANGERANG SELATAN',
    'KOTA  TANJUNG BALAI',
    'KOTA  TANJUNG PINANG',
    'KOTA  TARAKAN',
    'KOTA  TASIKMALAYA',
    'KOTA  TEBING TINGGI',
    'KOTA  TEGAL',
    'KOTA  TERNATE',
    'KOTA  TIDORE KEPULAUAN',
    'KOTA  TUAL',
    'KOTA  YOGYAKARTA',
    'KOTA . BIMA',
    'KOTA . TOMOHON',
    'KOTA BALIKPAPAN',
    'KOTA BANYUWANGI',
    'KOTA BATURAJA',
    'KOTA BLITAR',
    'KOTA BOGOR',
    'KOTA CILACAP',
    'KOTA CIREBON',
    'KOTA JEMBER',
    'KOTA KARAWANG',
    'KOTA KEDIRI',
    'KOTA KISARAN',
    'KOTA LANGSA',
    'KOTA LHOKSEUMAWE',
    'KOTA LUMAJANG',
    'KOTA MADIUN',
    'KOTA MALANG',
    'KOTA MUARA BUNGO',
    'KOTA PRABUMULIH',
    'KOTA PROBOLINGGO',
    'KOTA PURWOKERTO',
    'KOTA RANTAU PRAPAT',
    'KOTA SERANG',
    'KOTA SITUBONDO',
    'KOTA SOLO',
    'KOTA SORONG',
    'KOTA TANJUNG PINANG',
    'KOTA TASIKMALAYA',
    'KOTA TERNATE',
    'KOTA TULUNGAGUNG',
    'KOTA WATAMPONE',
    'KUPANG',
    'KAB. FAK-FAK',
    'KAB. KAIMANA',
    'KAB. MAJENE',
    'KAB. MAMASA',
    'KAB. MAMUJU UTARA',
    'KAB. MANOKWARI',
    'KAB. MAYBRAT',
    'KAB. POLEWALI MANDAR',
    'KAB. RAJA AMPAT',
    'KAB. SORONG',
    'KAB. SORONG SELATAN',
    'KAB. TELUK BINTUNI',
    'KAB. TELUK WONDAMA',
    'KAB. TEMBRAUW',
    'KOTA MAMUJU',
    'KOTA SORONG',
    'MAKASSAR',
    'MATARAM',
    'MEDAN',
    'MENADO',
    'PADANG',
    'PALANGKARAYA',
    'PALEMBANG',
    'PALU',
    'PANGKAL PINANG',
    'PEKANBARU',
    'PONTIANAK',
    'SAMARINDA',
    'SEMARANG',
    'SURABAYA',
    'WIL. KEPULAUAN SERIBU',
    'YOGYAKARTA'
]; //Nicholas' change. Added city list


class WithdrawalModal extends Component {

    constructor() { 
        super();
        this.state = {
            banks: [],
            bankNames: [],
            selectedIndex: null,
            slide: 'banks', // banks, amounts. progress, add, conrifm
            amount: "",
            showOTP: false,
            loading: false,
            banksList: [],
            cityList: [], 
            label: "",
            bankName: "",
            bankAddress: "",
            accountNumber: "",
            formFilled: false,
            summary: {
                bank: '',
                accountNumber: '',
                totalAmount: 0,
                fee: 0
            },
            resident: false,
            citizen: false,
            accountName: '',
            beneficiaryCategory: 'COMPANY',
            city: '',
            otpMethod: 'email',
            transactionComplete: false
        } //Nicholas' change. Added city list variable

        this.handleChange = (fieldName) => (event) => {
            let obj = Object.assign({})
            if (fieldName === 'amount') {
                obj[fieldName] = event.target.rawValue;
            } else {
                obj[fieldName] = event.target.value
            }

            this.setState(obj)
        }
    }

    getToken() {
        let username = this.props.user.username
        let clientId = this.props.user.pool.clientId
        let token = this.props.user.storage['CognitoIdentityServiceProvider.' + clientId + '.' + username + '.idToken']
        return token
    }

    onSetLanguage() {
        strings.setLanguage(this.props.language);
    }

    deleteBank() {
        let bank = this.state.banks[this.state.selectedIndex];
        this.setState({ loading: true });

        bankActions.deleteBank(this.getToken(), bank.BankAccountId).then(res => {
            
            this.setState({ loading: false, selectedIndex: null });
            this.fetchBanks();
        }).catch(err => {
            this.setState({ loading: false });
            NetworkError(err);
        });
    }

    fetchUserProfile() {
        getUserAttributes(this.props.user).then(attr => {

            this.setState({
                accountName: attr.name
            })
        })
        .catch(err => {
            let details = {
                type: 'COMPONENTS: withdrawalModal | FUNC: fetchUserProfile',
                url: ''
            }
            NetworkError(err, details);
        })
    }

    fetchBanks() {
        this.setState({ loading: true })
        bankActions.fetchBanks(this.getToken()).then(res => {
            this.setState({ banks: res.data.data, loading: false, slide: 'banks' })
        })
        .catch(err => {
            this.setState({ loading: false })
            let details = {
                type: 'COMPONENTS: withdrawalModal | FUNC: fetchBanks',
                url: ''
            }
            NetworkError(err, details);
        })
    }

    selectBank(i) {
        this.setState({
            selectedIndex: i
        });
    }

    componentWillMount() {
        this.onSetLanguage();
    }

    renderBanks() {
        return this.state.banks.map((b, i) => {
            return (
                <tr key={b.BankAccountId}>
                    <td>
                        {/* <label className="m-checkbox m-checkbox--solid m-checkbox--single m-checkbox--brand">
                        <input onClick={() => this.selectBank(i)} checked={this.state.selectedIndex === i} type="checkbox" /><span></span>
                    </label> */}
                        <label className="m-radio m-radio--solid">
                            <input type="radio" onClick={() => this.selectBank(i)} checked={this.state.selectedIndex === i} />
                            <span></span>
                        </label>
                    </td>
                    <td>{b.AccountNumber}</td>
                    <td>
                        <span className="m-widget11__title">{b.BankName}</span>
                        {/* <span className="m-widget11__sub">{b.BankName}</span> */}
                    </td>
                    {/* <td className=" m--font-brand">{b.Label}</td> */}
                </tr>
            )
        });
    }

    toggleOTP() {

        let newStatus = !this.state.showOTP

        if (newStatus === false && this.state.transactionComplete === true) {
            this.setState({
                amount: 0,
                loading: false,
                slide: 'banks',
                selectedIndex: null,
                transactionComplete: false
            });
            this.props.toggleShow();
        }

        this.setState({
            showOTP: newStatus
        });
    }

    requestWithdrawal() {

        let remainingBalance = this.props.idrDailyLimit - this.props.idrUsageDayText;

        if (this.state.amount > this.props.availableBalanceIdr) {
            NotificationManager.warning(strings.WITHDRAWAL_AMOUNT_HIGHER_ACCOUNT_BALANCE)
        } else if (this.state.amount > remainingBalance) {
            NotificationManager.warning(strings.WITHDRAW_AMOUNT_HIGHER_DAILY_LIMIT_LEFT + remainingBalance)
        }
        else {
            let amounts = 0;

            if (typeof (this.state.amount) == 'string') {
                amounts = parseFloat(this.state.amount.replace(/,/g, ''))
            } else {
                amounts = this.state.amount;
            }

            this.setState({
                summary: {
                    bank: this.state.banks[this.state.selectedIndex].BankName,
                    accountNumber: this.state.banks[this.state.selectedIndex].AccountNumber,
                    totalAmount: amounts,
                    fee: 0
                }
            })

            setTimeout(() => {
                this.toggleOTP();
            }, 50)
        }
    }

    completeVerification(otpId, token) {
        this.doWithdraw(otpId, token);
    }

    createBank() {
        this.setState({ formFilled: true })
        let data = {
            "bankName": this.state.bankName,
            "accountNumber": this.state.accountNumber,
            "city": this.state.city
        }

        let valid =
            this.state.bankName.length > 0 &&
            this.state.accountNumber.length > 0 &&
            this.state.city.length > 0;

        if (valid) {
            this.setState({ loading: true })
            bankActions.createBank(this.getToken(), data).then(res => {
                this.setState({
                    sending: false,
                    formFilled: false
                })
                NotificationManager.success(strings.BANK_HAS_BEEN_CREATED)
                this.fetchBanks();
            }).catch(err => {
                NetworkError(err);
                // let message = strings.UNEXPECTED_ERROR;
                // if (err.response) {
                //     message = err.response.data.err_str;
                //     if (message === strings.BANK_ACCOUNT_ALREADY_THERE) {
                //         message = strings.BANK_ACCOUNT_ALREADY_ADDED_PREVIOSLY;
                //     }
                // }
                // NotificationManager.error(message)
                this.setState({
                    loading: false,
                    formFilled: false,
                    slide: 'banks'
                })
            })
        } else {
            this.setState({ sending: false })
        }
    }

    doWithdraw(otpId, token) {
        let amounts = 0;

        if (typeof (this.state.amount) == 'string') {
            amounts = parseFloat(this.state.amount.replace(/,/g, ''))
        } else {
            amounts = this.state.amount;
        }

        let data = {};

        if (otpId !== null && token !== null) {
            data = {
                otp: {
                    otpId: otpId,
                    token: Number(token)
                },
                withdraw: {
                    bankAccountId: this.state.banks[this.state.selectedIndex].BankAccountId,
                    amount: amounts
                }

            }
        } else {
            data = {
                withdraw: {
                    bankAccountId: this.state.banks[this.state.selectedIndex].BankAccountId,
                    amount: amounts
                }
            }
        }

        this.setState({
            loading: true
        });

        withdrawActions.withdraw(this.getToken(), data).then(res => {

            this.setState({
                loading: false,
                showOTP: true,
                transactionComplete: true
            });
            // this.toggleShow();
            // history.push('/');
            NotificationManager.success(strings.WITHDRAWAL_TRASNACTIONS_SUCCESS);
            if (this.props.onRefreshBalances) {
                this.props.onRefreshBalances('IDR');
            }
        }).catch(err => {
            this.setState({
                loading: false,
                slide: 'banks',
                showOTP: false,
                transactionComplete: false
            });
            this.toggleShow(true);
            // if (err.response) {
            //     NotificationManager.error(err.response.data.err);
            // } else {
            //     NetworkError(err);
            // }
            NetworkError(err);
            // this.props.toggleShow()
        })
    }

    componentDidMount() {
        let countriesData = [];
        // banksList.map(c => {
        //     countriesData.push({
        //         label: c,
        //         value: c
        //     });
        // });

        this.getBankList();

        this.fetchUserOTPSTatus();

        this.fetchBanks();
        this.fetchUserProfile();
        // this.setState({ banksList: countriesData});
    }

    toggleShow(clearAll = false) {

        let obj = {
            loading: false,
            slide: 'banks',
            selectedIndex: null,
            transactionComplete: false
        }

        if (clearAll) {
            obj.amount = 0;
        }

        this.setState(obj);
        setTimeout(() => {
            this.props.toggleShow()
        }, 50);
    }

    toggleBack() {
        this.setState({
            loading: false,
            slide: 'banks'
        });
        // setTimeout(() => {
        //     this.props.toggleShow()
        // },50);
    }

    fetchUserOTPSTatus() {
        OTPActions.getOTPStatus(this.getToken()).then(res => {
            

            let otpMethod = 'email';

            if (res.data.data.ga === true) {
                otpMethod = 'ga';
            }

            this.setState({
                otpMethod
            })
        }).catch(err => {
            
            NetworkError(err);
        });
    }

    getBankList() {
        bankActions.getBankNameList(this.getToken()).then(res => {
            // 

            let bankNames = [];

            res.data.data.forEach(b => {
                bankNames.push({
                    label: b.fullname,
                    value: b.fullname
                });
            });

            this.setState({
                bankNames
            })
        }).catch(err => {
            
            NetworkError(err);
        })
    }

    gotoAdd() {
        this.setState({
            slide: 'add',
            accountNumber: "",
            bankName: "",
            city: ""
        });
    }

    gotoAccounts() {
        this.toggleShow();
        history.push('/accounts?currency=IDR')
    }

    render() {

        let mappedBanksList = [];
        banksList.map(b => {
            mappedBanksList.push({ value: b, label: b })
        });

        let mappedCityList = []; //Nicholas' change. Same functionality as dropdown banks
        cityList.map(c => {
            mappedCityList.push({ value: c, label: c })
        });

        // 

        return (
            <div>
                <OTPModal transactionComplete={this.state.transactionComplete} method={this.state.otpMethod} summaryFiat={this.state.summary} show={this.state.showOTP} toggleShow={this.toggleOTP.bind(this)} completeVerification={this.completeVerification.bind(this)} />
                <Modal isOpen={this.props.show} toggle={() => this.toggleShow(true)} className={"modal-dialog-centered mx-auto"} size="lg">
                    <ModalHeader toggle={() => this.toggleShow(true)}>
                        {this.state.slide === 'add' ?
                            strings.ADD_BANK : strings.WITHDRAW_INDONESIAN_RUPIAH
                        }
                    </ModalHeader>
                    <ModalBody className={classnames({ 'mx-auto tc-modal-body': true, 'deposit-modal-idr': this.state.slide !== 'add' })}>

                        {this.state.slide === 'add' &&
                            <div className="row">
                                <p style={{ fontWeight: 500, marginBottom: "20px", marginLeft: "18px" }}>{strings.PLEASE_CHECK}</p>
                                <div className="col-xl-12">
                                    <div className="m-form__section m-form__section--first">

                                        <div className="form-group m-form__group row">
                                            <label className="col-xl-4 col-lg-4 col-form-label">{strings.ACCOUNT_NAME}:</label>
                                            <div className="col-12">
                                                <input type="text" value={this.state.accountName} disabled style={{ marginBottom: "5px" }} className={classnames({ "form-control m-input": true })} />
                                                <div className="invalid-feedback">
                                                    {strings.THE_FIELD_IS_REQUIRED}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group m-form__group row">
                                            <label className="col-xl-4 col-lg-4 col-form-label">{strings.ACCOUNT_NUMBER}:</label>
                                            <div className="col-12">
                                                <input type="text" value={this.state.accountNumber} onChange={this.handleChange('accountNumber')} name="name" style={{ marginBottom: "5px" }} className={classnames({ "form-control m-input": true, "is-invalid": this.state.formFilled && this.state.accountNumber.length === 0 })} />
                                                <div className="invalid-feedback">
                                                    {strings.THE_FIELD_IS_REQUIRED}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group m-form__group row">
                                            <label className="col-xl-4 col-lg-4 col-form-label">{strings.BANK_NAME}:</label>
                                            <div className="col-12">
                                                {/* <input type="text" name="name" value={this.state.fullName} onChange={this.handleChange('fullName')} style={{marginBottom:"5px"}} className="form-control m-input" placeholder="Nick Stone"/> */}
                                                <Select
                                                    name="form-field-name"
                                                    style={{ marginBottom: "5px" }}
                                                    value={this.state.bankName}
                                                    options={mappedBanksList}
                                                    onChange={(val) => {
                                                        this.setState({ bankName: val.value })
                                                    }}
                                                />
                                                {/* <span className="m-form__help" style={{fontSize:"12px",color:"#7b7e8a"}}>Please select the Nationality</span> */}
                                            </div>
                                        </div>
                                        <div className="form-group m-form__group row">
                                            <label className="col-xl-4 col-lg-4 col-form-label">{strings.CITY}:</label>
                                            <div className="col-12">
                                                {/* <input type="text" name="name" value={this.state.fullName} onChange={this.handleChange('fullName')} style={{marginBottom:"5px"}} className="form-control m-input" placeholder="Nick Stone"/> */}
                                                <Select
                                                    name="form-field-name"
                                                    style={{ marginBottom: "5px" }}
                                                    value={this.state.city}
                                                    options={mappedCityList}
                                                    onChange={(val) => {
                                                        this.setState({ city: val.value })
                                                    }} 
                                                /> {/*Nicholas' change. Added dropdown city list function*/}
                                                {/* <span className="m-form__help" style={{fontSize:"12px",color:"#7b7e8a"}}>Please select the Nationality</span> */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }

                        {this.state.slide === 'banks' && <div>
                            <div className="row">
                                <div className="col-md-6">
                                    <div style={{ fontWeight: 500, marginBottom: "20px" }}>{strings.SELECT_BANK_ACCOUNT}</div>
                                </div>
                                <div className="col-md-6">
                                    <div className="row" style={{ width: "150%" }}>
                                        <button disabled={this.state.selectedIndex === null} style={{ marginRight: 10 }} onClick={() => this.deleteBank()} className={classnames({ "btn btn-outline-grey float-right": this.state.selectedIndex === null, "btn btn-outline-danger float-right": this.state.selectedIndex !== null })}>- {strings.REMOVE_BANK}</button>
                                        <button onClick={() => this.gotoAdd()} className="btn btn-blue float-right">+ {strings.ADD_NEW_BANK}</button>
                                    </div>
                                </div>
                            </div>
                            <div className="m-portlet__body">
                                <div className="m-portlet__body">
                                    <div className="tab-content">
                                        <div className="tab-pane active">
                                            <div className="m-widget11">
                                                <div className="table-responsive">
                                                    <table className="table">
                                                        <thead>
                                                            <tr>
                                                                <td className="m-widget11__label">#</td>
                                                                <td className="m-widget11__app">{strings.ACCOUNT_NUMBER}</td>
                                                                <td className="m-widget11__app">{strings.BANK_NAME}</td>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            {this.renderBanks()}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>}
                        {
                            this.state.slide === 'amounts' &&
                            <div className="m-portlet__body">
                                <div className="m-portlet__body">
                                    <div style={{ fontWeight: 500, marginBottom: "20px" }}>{strings.PLEASE_CHECK_DETAILS_AMOUNT_WITHDRAW}</div>
                                    <div className="m-portlet__body">
                                        <div className="m-widget13">
                                            <div className="m-widget13__item">
                                                <span className="m-widget13__desc txtalignleft m--align-right">
                                                    {strings.BANK}
                                                </span>
                                                <span className="m-widget13__text txtalignleft m-widget13__text-bolder">
                                                    {this.state.banks[this.state.selectedIndex].BankName}
                                                </span>
                                            </div>
                                            <div className="m-widget13__item">
                                                <span className="m-widget13__desc txtalignleft m--align-right">
                                                    {strings.ACCOUNT_NUMBER}
                                                </span>
                                                <span className="m-widget13__text txtalignleft m-widget13__text-bolder">
                                                    {this.state.banks[this.state.selectedIndex].AccountNumber}
                                                </span>
                                            </div>
                                            <div className="m-widget13__item" style={{ padding: "5px", marginTop: "10px" }}>
                                                <span className="m-widget13__desc txtalignleft m--align-right">
                                                    {strings.ACCOUNT_BALANCE}
                                                </span>
                                                <span className="m-widget13__text m-widget13__text-bolder">
                                                    {amountFormatter(this.props.accountBalanceIdr, this.props.currency)}
                                                </span>
                                            </div>
                                            <div className="m-widget13__item" style={{ padding: "5px", marginTop: "10px", backgroundColor: "#dcdcdc78" }}>
                                                <span className="m-widget13__desc txtalignleft m--align-right">
                                                    {strings.WITHDRAWAL_BALANCE}
                                                </span>
                                                <span className="m-widget13__text m-widget13__text-bolder">
                                                    {amountFormatter(this.props.availableBalanceIdr, this.props.currency)}
                                                </span>
                                            </div>
                                            <div style={{ fontWeight: 500, fontSize: "13px", marginTop: "24px" }}>
                                                <span>{strings.PLEASE_ENTER_AMOUNT_WITHDRAW}</span><span style={{ float: "right" }}></span>
                                                <div className="input-group" style={{ marginTop: "12px" }}>
                                                    <div className="input-group-append" style={{ padding: "-0.65rem 1.25rem", border: "1px solid rgba(0,0,0,.15)", borderRadius: ".25rem", backgroundColor: "#f4f5f8", color: "gray" }} >
                                                        <span className="input-group-text" id="basic-addon2">Rp</span></div>
                                                    {/* <input onChange={this.handleChange('amount')} type="number" className="form-control m-input" placeholder="" aria-describedby="basic-addon2" /> */}
                                                    <Cleave onChange={this.handleChange('amount')} type="text" className="form-control m-input" placeholder="" aria-describedby="basic-addon2" disabled={this.state.loading} />
                                                </div>
                                                <div style={{ color: this.state.amount < 500000 && (this.state.amount != "") ? 'red' : 'black' }}>{strings.MINIMUM_AMOUNT_IS} Rp500.000</div>
                                                {this.state.amount > (this.props.idrDailyLimit - this.props.idrUsageDayText) && <div style={{ color: 'red' }}>{strings.WITHDRAW_AMOUNT_EXCEED_DAILY_LIMIT}</div>}
                                                {this.state.amount > this.props.availableBalanceIdr && <div style={{ color: 'red' }}>{strings.INSUFFCIENT_WITHDRAWAL_BALANCE}</div>}
                                            </div>
                                            <div style={{ fontWeight: 500, fontSize: "13px", marginTop: "20px", marginBottom: "10px" }}>
                                                <span><span>{strings.WITHDRAWAL_DAILY_LIMIT}:</span>
                                                    <span style={{ float: "right" }}><span> Rp{amountFormatter(this.props.idrDailyLimit, this.props.currency)}</span>
                                                    </span></span></div>
                                            <div className="m--space-10"></div>
                                            <div className="progress m-progress--sm">
                                                <div className="progress-bar m--bg-green" role="progressbar" style={{ width: 100 - this.props.idrUsageDayPrecentage + "%" }} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <span className="m-widget24__change" style={{ fontSize: "12px" }}>
                                                {strings.REMAINING_DAILY_WITHDRAW_LIMIT}
                                            </span>
                                            <span className="m-widget24__number" style={{ float: "right", fontSize: "12px", marginTop: "2px" }}>
                                                Rp{amountFormatter((this.props.idrDailyLimit - this.props.idrUsageDayText), this.props.currency)} &nbsp;({100 - this.props.idrUsageDayPrecentage}%)
                                </span>
                                            <br />
                                            {this.state.loading ?
                                                <div style={{ marginLeft: "45%", marginTop: "5%" }}><Loader /></div> :
                                                <button disabled={Number(this.state.amount) > this.props.availableBalanceIdr || Number(this.state.amount) < 500000 || Number(this.state.amount) === 0 || (100 - this.props.idrUsageDayPrecentage) === 0 || (this.state.amount > (this.props.idrDailyLimit - this.props.idrUsageDayText))} onClick={() => this.requestWithdrawal()} style={{ marginTop: "25px", marginBottom: "20px" }} type="button" className="btn btn-blue btn-block" data-toggle="modal" data-target="#m_modal_3">{strings.REQUEST_WITHDRAWAL}</button>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }
                        {
                            this.state.slide === 'confirm' &&
                            <div>
                                <div className="m-portlet__body">
                                    <div className="m-widget13">
                                        <div className="m-widget13__item">
                                            <span className="m-widget13__desc txtalignleft m--align-right">
                                                {strings.BANK}
                                            </span>
                                            <span className="m-widget13__text txtalignleft m-widget13__text-bolder">
                                                {this.state.summary.bank}
                                            </span>
                                        </div>
                                        <div className="m-widget13__item">
                                            <span className="m-widget13__desc txtalignleft m--align-right">
                                                {strings.ACCOUNT_NUMBER}
                                            </span>
                                            <span className="m-widget13__text txtalignleft m-widget13__text-bolder">
                                                {this.state.summary.accountNumber}
                                            </span>
                                        </div>
                                        <div className="m-widget13__item">
                                            <span className="m-widget13__desc txtalignleft m--align-right">
                                                {strings.AMOUNT}
                                            </span>
                                            <span className="m-widget13__text txtalignleft m-widget13__text-bolder">
                                                Rp{amountFormatter(this.state.summary.totalAmount, this.props.currency)}
                                            </span>
                                        </div>
                                        {/* <div className="m-widget13__item">
									<span className="m-widget13__desc txtalignleft m--align-right">
										Withdrawal fee
									</span>
									<span className="m-widget13__text txtalignleft m-widget13__text-bolder">
                                    IDR { amountFormatter(this.state.summary.fee) }
									</span>
								</div> */}
                                        <div className="m-widget13__item" style={{ marginTop: "10px", backgroundColor: "#dcdcdc78" }}>
                                            <span className="m-widget13__desc" style={{ textAlign: "right" }}>
                                                {strings.YOU_WILL_RECEIVE}
                                            </span>
                                            <span className="m-widget13__text m-widget13__text-bolder">
                                                Rp{amountFormatter((this.state.summary.totalAmount - this.state.summary.fee), this.props.currency)}
                                            </span>
                                        </div>
                                        <br />
                                        <div className="">
                                            <label className="control-label col-12">
                                                {strings.KEEP_TRACK_TRANSACTION_STATUS_UNDER_HISOTRY} <a href="#" onClick={() => this.gotoAccounts()}> {strings.ACCOUNTS} </a> {strings.TAB}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }
                    </ModalBody>
                    <ModalFooter className="tc-modal-footer">
                        {this.state.slide === 'add' && !this.state.loading ?
                            <Button disabled={this.state.loading} color="secondary" onClick={() => this.toggleBack()}>{strings.BACK}</Button> : null}
                        {this.state.slide === 'banks' && !this.state.loading && <button disabled={this.state.loading || this.state.selectedIndex === null} className="btn btn-blue" onClick={() => this.setState({ slide: 'amounts' })}>{strings.NEXT}</button>}
                        {this.state.slide === 'add' && !this.state.loading && <button disabled={this.state.loading} className="btn btn-blue" onClick={() => this.createBank()}>
                            {strings.ADD}
                        </button>}
                        {this.state.loading && this.state.slide === 'banks' && <div style={{ marginLeft: "45%" }}><Loader /></div>}
                        {this.state.loading && this.state.slide === 'add' && <div style={{ marginLeft: "45%" }}><Loader /></div>}
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.cognito.user,
        kyc: state.kyc,
        language: state.language
    };
};

export default connect(mapStateToProps, null)(WithdrawalModal);
