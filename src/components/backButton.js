import React, { Component } from 'react'
import history from '../history.js'

class BackButton extends Component {

  backAction() {
    if (this.props.back) {
      history.goBack()
    } else {
      history.replace('/')
    }
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-12">
          <button onClick={() => this.backAction()} className="btn btn-back pull-right">Back</button>
        </div>
      </div>
    )
  }

}

export default BackButton