import React, { Component } from 'react';
import actions from '../../core/actions/client/home';
import _ from 'lodash';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import { connect } from 'react-redux';
import fontawesome from '@fortawesome/fontawesome';
import {faArrowUp, faArrowDown} from '@fortawesome/fontawesome-free-solid';
import NetworkError from '../../errors/networkError';

fontawesome.library.add(faArrowUp,faArrowDown)

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

function getSymbolFromURL() {
    const regex = new RegExp('[\\?&]symbol=([^&#]*)');
    const results = regex.exec(window.location.search);

    return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

class DepthChart extends Component {

    constructor(){
        super();
        this.state = {
            liveCurrencyPair: "BTCIDR",
            liveType: "hours",
            liveRates: [],
			selected: "depth",
			selectedOption: [],
			historyString: "",

            depthData : [],
            depthDataProcessed : []
        }
    }

	componentWillMount() {
		strings.setLanguage(this.props.language);
	}

    get_data_depth(){
        let data = {
            "currencyPair":this.state.liveCurrencyPair,
            "resolution":"5"
           }
        actions.getDepthTrading(data).then(res => {
            
            
            this.setState({
                depthData: res.data.result
            })

            this.processDepthData(res.data.result);

        }).catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: deptChart | FUNC: get_data_depth',
                url: 'client/rates/getTradingViewNew'
            }
            NetworkError(err, errorDetails);
        });
    }

    processDepthData(data){
        
        function processData(list, type, desc) {
            
            // Convert to data points
            for(var i = 0; i < list.length; i++) {
                list[i] = {
                    value: Number(list[i][0]),
                    volume: Number(list[i][1]),
                }
            }
            

            // Sort list just in case
            list.sort(function(a, b) {
                if (a.value > b.value) {
                return 1;
                }
                else if (a.value < b.value) {
                return -1;
                }
                else {
                return 0;
                }
            });

            // Calculate cummulative volume
            if (desc) {
                for(var i = list.length - 1; i >= 0; i--) {
                    if (i < (list.length - 1)) {
                        list[i].totalvolume = list[i+1].totalvolume + list[i].volume;
                    }
                    else {
                        list[i].totalvolume = list[i].volume;
                    }
                    var dp = {};
                    dp["value"] = list[i].value;
                    dp[type + "volume"] = list[i].volume;
                    dp[type + "totalvolume"] = list[i].totalvolume;                    
                    res.unshift(dp);
                }
            }
            else {
                for(var i = 0; i < list.length; i++) {
                    if (i > 0) {
                        list[i].totalvolume = list[i-1].totalvolume + list[i].volume;
                    }
                    else {
                        list[i].totalvolume = list[i].volume;
                    }
                    var dp = {};
                    dp["value"] = list[i].value;
                    dp[type + "volume"] = list[i].volume;
                    dp[type + "totalvolume"] = list[i].totalvolume;
                    res.push(dp);
                }
            }
        }

        var res = [];

        processData(data.bids, "bids", true);
        processData(data.asks, "asks", false);
        
        this.setState({
            depthDataProcessed: res
        })
        
    }

	do_depth_chart(){
        let index_idr = getSymbolFromURL().indexOf("IDR");
        let currency = getSymbolFromURL().substring(0,index_idr).toLowerCase();
        var chart;
        let url = "";
        if(process.env.REACT_APP_ENV === 'dev' || process.env.REACT_APP_ENV === 'local')
        {
            url = "https://service-dev.tokocrypto.com/client2/orderbook/";
        }else if(process.env.REACT_APP_ENV === 'demo')
        {
            url = "https://service-demo.tokocrypto.com/client2/orderbook/";
        }else if(process.env.REACT_APP_ENV === 'production')
        {
            url = "https://service.tokocrypto.com/client2/orderbook/";
        }
		setTimeout(()=>{
			chart = window.am4core.create(document.getElementById('chartdiv'), window.am4charts.XYChart);
            // chart.dataSource.url = "https://service-dev.tokocrypto.com/rates/getTradingViewNew";
            // chart.dataSource.url = "https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_ETH&depth=50";
            chart.dataSource.url = url+currency;
            chart.dataSource.reloadFrequency = 30000;
			chart.dataSource.adapter.add("parsedData", function(data) {
                
                

			  // Function to process (sort and calculate cummulative volume)
			  function processData(list, type, desc) {

			    // Convert to data points
			    for(var i = 0; i < list.length; i++) {
                    list[i] = {
                        value: Number(list[i][0]),
                        volume: Number(list[i][1]),
			      }
			    }

			    // Sort list just in case
			    list.sort(function(a, b) {
			      if (a.value > b.value) {
			        return 1;
			      }
			      else if (a.value < b.value) {
			        return -1;
			      }
			      else {
			        return 0;
			      }
                });                

			    // Calculate cummulative volume
			    if (desc) {
			      for(var i = list.length - 1; i >= 0; i--) {
			        if (i < (list.length - 1)) {
			          list[i].totalvolume = list[i+1].totalvolume + list[i].volume;
			        }
			        else {
			          list[i].totalvolume = list[i].volume;
			        }
			        var dp = {};
			        dp["value"] = list[i].value;
			        dp[type + "volume"] = list[i].volume;
			        dp[type + "totalvolume"] = list[i].totalvolume;
			        res.unshift(dp);
			      }
			    }
			    else {
			      for(var i = 0; i < list.length; i++) {
			        if (i > 0) {
			          list[i].totalvolume = list[i-1].totalvolume + list[i].volume;
			        }
			        else {
			          list[i].totalvolume = list[i].volume;
			        }
			        var dp = {};
			        dp["value"] = list[i].value;
			        dp[type + "volume"] = list[i].volume;
			        dp[type + "totalvolume"] = list[i].totalvolume;
			        res.push(dp);
			      }
			    }

			  }

			  // Init
              var res = [];
              
              
			  processData(data.bids, "bids", true);
              processData(data.asks, "asks", false);
            

			  return res;
            });

			// Set up precision for numbers
			chart.numberFormatter.numberFormat = "#,###.####";
            
			// Create axes
			var xAxis = chart.xAxes.push(new window.am4charts.CategoryAxis());
			xAxis.dataFields.category = "value";
			//xAxis.renderer.grid.template.location = 0;
			xAxis.renderer.minGridDistance = 100;
			xAxis.title.text = "Price ("+currency.toUpperCase()+"/IDR)";

			var yAxis = chart.yAxes.push(new window.am4charts.ValueAxis());
			yAxis.title.text = "Volume";

            chart.mouseWheelBehavior = 'zoomXY';

			// Create series
			var series = chart.series.push(new window.am4charts.StepLineSeries());
			series.dataFields.categoryX = "value";
			series.dataFields.valueY = "bidstotalvolume";
			series.strokeWidth = 2;
			series.stroke = window.am4core.color("#1aa64a");
			series.fill = series.stroke;
			series.fillOpacity = 0.1;
			series.tooltipText = "Ask: [bold]{categoryX}[/]\nTotal volume: [bold]{valueY}[/]\nVolume: [bold]{bidvolume}[/]"

			var series2 = chart.series.push(new window.am4charts.StepLineSeries());
			series2.dataFields.categoryX = "value";
			series2.dataFields.valueY = "askstotalvolume";
			series2.strokeWidth = 2;
			series2.stroke = window.am4core.color("#f89927");
			series2.fill = series2.stroke;
			series2.fillOpacity = 0.1;
			series2.tooltipText = "Ask: [bold]{categoryX}[/]\nTotal volume: [bold]{valueY}[/]\nVolume: [bold]{asksvolume}[/]"

			var series3 = chart.series.push(new window.am4charts.ColumnSeries());
			series3.dataFields.categoryX = "value";
			series3.dataFields.valueY = "bidsvolume";
			series3.strokeWidth = 0;
			series3.fill = window.am4core.color("#000");
			series3.fillOpacity = 0.2;

			var series4 = chart.series.push(new window.am4charts.ColumnSeries());
			series4.dataFields.categoryX = "value";
			series4.dataFields.valueY = "asksvolume";
			series4.strokeWidth = 0;
			series4.fill = window.am4core.color("#000");
			series4.fillOpacity = 0.2;

			// Add cursor
			chart.cursor = new window.am4charts.XYCursor();

			return chart;
		},300)
		
		// return;

		// Add data

	}

    render(){

        let currentPrice = 0;
        let monthlyChange = 0;
        let monthlyChangeAmount = 0;
        let rangeMax = 0;
        let rangeMin = 0;
        let currencyPair;

        if(this.props.currencyPairData.hasOwnProperty('BTCIDR')){
            currentPrice = this.props.currencyPairData[this.props.currency.toUpperCase()+'IDR']['24H'].rates.price;
            monthlyChangeAmount = this.props.currencyPairData[this.props.currency.toUpperCase()+'IDR']['24H'].rates['last24hour'];

            rangeMax = this.props.currencyPairData[this.props.currency.toUpperCase()+'IDR']['24H'].rates['maxPrice'];
            rangeMin = this.props.currencyPairData[this.props.currency.toUpperCase()+'IDR']['24H'].rates['minPrice'];

            monthlyChange = ((monthlyChangeAmount / currentPrice) * 100).toFixed(2) ;

            if(currentPrice < monthlyChangeAmount){
                monthlyChange *= -1;
            }
        }

        // dummy test

        return (
            <div className="col-md-12">
				<div className="m-portlet m-portlet--full-height dashboard-chart-data-container">
					<div className="m-portlet__body" style={{padding:"0px"}}>
                        <div className="tab-content" style={{padding:"2.2rem"}}>
                            <div className="tab-pane active" id="m_widget4_tab1_content">
                                {this.do_depth_chart()}
								<div id="chartdiv" style={{width: '100%', height: '500px'}} value="tes">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
        )
    }

}

const mapStateToProps = (state) => {
	return {
		language: state.language
	};
};

export default connect(mapStateToProps, null)(DepthChart);
