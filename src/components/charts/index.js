import React, { Component } from 'react';
import actions from '../../core/actions/client/home';
import classnames from 'classnames';
import {GoogleCharts} from '../../googleCharts';
import _ from 'lodash';
import numeral from 'numeral';
import moment from 'moment';
import amountFormatter from '../../helpers/amountFormatter';
import cryptoFormatter from '../../helpers/cryptoFormatter';
import ReactHighstock from 'react-highcharts/ReactHighstock';
import LocalizedStrings from 'react-localization';
import enDictionary from '../../languages/en.json';
import idDictionary from '../../languages/id.json';
import javaDictionary from '../../languages/java.json';
import sundaDictionary from '../../languages/sunda.json';
import { connect } from 'react-redux';
import history from '../../history';
import { TVChartAdvancedContainer } from '../TVChartContainer/fullAdavancedChart';
import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import {faArrowUp, faArrowDown} from '@fortawesome/fontawesome-free-solid';
import currencyFormatter from '../../helpers/currencyFormatter';
import Select from 'react-select';
import NetworkError from '../../errors/networkError';

fontawesome.library.add(faArrowUp,faArrowDown)

let strings = new LocalizedStrings({
    en: enDictionary,
    id: idDictionary,
    sunda: sundaDictionary,
    java: javaDictionary
});

class HistoricalChart extends Component {

    constructor(){
        super();
        this.state = {
            liveCurrencyPair: "BTCIDR",
            liveType: "hours",
            liveRates: [],
			selected: "history",
			selectedOption: [],
			historyString: "",

            o: 0,
            h: 0,
            l: 0,
            c: 0,
            rising: false,
            currentBTC: 0,
            currentETH: 0,
            changesPercentage: 0,
            changesAmount: 0,
            changesLastMonth: 0,
            currentVolume: 0,

	        config: {
		        title: {
			        text: null
		        },
		        rangeSelector: {
			        inputPosition: {
				        align: 'left',
				        x: 0,
				        y: 0
			        },
			        buttons: [{
				        type: 'day',
				        count: 1,
				        text: '1d'
			        },{
				        type: 'day',
				        count: 3,
				        text: '3d'
			        },{
				        type: 'ytd',
				        text: 'All'
			        }]
		        },
		        chart: {
			        type: 'area',
			        zoomType: "x"
		        },
		        series: [{
			        data: [],
			        color: '#007EA4'
		        }],
		        plotOptions: {
			        area: {
				        animation: false
			        }
		        }
	        }
        }
    }

	componentWillMount() {
		strings.setLanguage(this.props.language);
	}

    /**
     * TCDOC-021
     * make sure this function called after we get the data and state set
     * @param {*} series
     * series is the data from backend. will need to reformated if change the chart feature 
     */
    drawChart(series){
        let seriesData = series;

        

        GoogleCharts.load(() => {
            let ratesData = seriesData.values.map((r) => {

                let dateFormat = moment(r.x).format('MMM Do YYYY, h:mm:ss a');

                return [
                    r.x, r.close, ''+dateFormat+'\n'+numeral(r.close).format('0,0')
                ];

            });

            let dataTable = new GoogleCharts.api.visualization.DataTable();
            dataTable.addColumn('date', 'Date');
            // dataTable.addColumn('number', 'Low');
            // dataTable.addColumn('number', 'Open');
            dataTable.addColumn('number', 'Close');
            // dataTable.addColumn('number', 'High');
            dataTable.addColumn({type: 'string', role: 'tooltip'});
            dataTable.addRows(ratesData);

            // const data = GoogleCharts.api.visualization.arrayToDataTable(ratesData, true);

            // let max = _.maxBy(ratesData, (r) => r[4])[4];
            // let min = _.minBy(ratesData, (r) => r[1])[1];

            // let tick = 10;
            // let range = max - min;
            // let step = range / tick;
            // let tickRange = [min];

            // let current = min;
            // while(current < max ){
            //     current += step;
            //     tickRange.push(current);
            // }

            let options = {
                legend:'none',
                vAxis: {
                    gridlines: {
                        color: 'transparent',
                    },
                    format:'IDR #,###'
                    // textPosition: 'none'
                },
                hAxis: {
                    gridlines: {
                        color: 'transparent'
                    },
                    // textPosition: 'none'
                },
                bar: { groupWidth: '100%' },
                chartArea: {
                    top: 10,
                    bottom: 30,
                    right: 0,
                    left: 97, // set to 0 to hide vAxis
                    colors:['#FBAF5D']
                },
                tooltip: {isHtml: false},
                colors:['#FBAF5D']
                // vAxis: {
                //     ticks: tickRange
                // }
            };

            

            const candle_chart = new GoogleCharts.api.visualization.AreaChart(document.getElementById('chart_div'));
            candle_chart.draw(dataTable, options);
        });
    }

    getSummary(){
        actions.getChartSummary(this.props.currency+"IDR").then(res => {
            this.setState({
                changesPercentage: res.data.data.month.rates.changes.percentage,
                changesAmount: res.data.data.month.rates.price - res.data.data.month.rates.lastMonth,
                changesLastMonth: res.data.data.month.rates.lastMonth
            })
        }).catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: HistoricalChart | FUNC: getChartSummary',
                url: 'https://tfxf49mpq1.execute-api.us-east-1.amazonaws.com/dev/rates/changed/'+this.props.currency+"IDR"
            }
            NetworkError(err, errorDetails);
        });
    }

    componentDidMount(){
		if (this.state.selected === "history") {
            this.fetchTradingVolume();
		}
		
		let idTypes = [];
        idTypes = [
            {
                "value": "history",
                "label": (this.props.language === 'id') ? strings.HISTORICAL_DATA+ " "+this.props.currency+"/IDR" : this.props.currency+"/IDR "+strings.HISTORICAL_DATA
            },
            {
                "value": "depth",
                "label": "Depth Chart"
            }];
        this.setState({ selectedOption: idTypes });
    }
	do_depth_chart(){
		var chart;
		setTimeout(()=>{
			chart = window.am4core.create(document.getElementById('chartdiv'), window.am4charts.XYChart);
			
			chart.dataSource.url = "https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_ETH&depth=50";
			chart.dataSource.reloadFrequency = 30000;
			chart.dataSource.adapter.add("parsedData", function(data) {

			  // Function to process (sort and calculate cummulative volume)
			  function processData(list, type, desc) {

			    // Convert to data points
			    for(var i = 0; i < list.length; i++) {
			      list[i] = {
			        value: Number(list[i][0]),
			        volume: Number(list[i][1]),
			      }
			    }

			    // Sort list just in case
			    list.sort(function(a, b) {
			      if (a.value > b.value) {
			        return 1;
			      }
			      else if (a.value < b.value) {
			        return -1;
			      }
			      else {
			        return 0;
			      }
			    });

			    // Calculate cummulative volume
			    if (desc) {
			      for(var i = list.length - 1; i >= 0; i--) {
			        if (i < (list.length - 1)) {
			          list[i].totalvolume = list[i+1].totalvolume + list[i].volume;
			        }
			        else {
			          list[i].totalvolume = list[i].volume;
			        }
			        var dp = {};
			        dp["value"] = list[i].value;
			        dp[type + "volume"] = list[i].volume;
			        dp[type + "totalvolume"] = list[i].totalvolume;
			        res.unshift(dp);
			      }
			    }
			    else {
			      for(var i = 0; i < list.length; i++) {
			        if (i > 0) {
			          list[i].totalvolume = list[i-1].totalvolume + list[i].volume;
			        }
			        else {
			          list[i].totalvolume = list[i].volume;
			        }
			        var dp = {};
			        dp["value"] = list[i].value;
			        dp[type + "volume"] = list[i].volume;
			        dp[type + "totalvolume"] = list[i].totalvolume;
			        res.push(dp);
			      }
			    }

			  }

			  // Init
			  var res = [];
			  processData(data.bids, "bids", true);
			  processData(data.asks, "asks", false);

			  return res;
			});

			// Set up precision for numbers
			chart.numberFormatter.numberFormat = "#,###.####";

			// Create axes
			var xAxis = chart.xAxes.push(new window.am4charts.CategoryAxis());
			xAxis.dataFields.category = "value";
			//xAxis.renderer.grid.template.location = 0;
			xAxis.renderer.minGridDistance = 50;
			xAxis.title.text = "Price (BTC/ETH)";

			var yAxis = chart.yAxes.push(new window.am4charts.ValueAxis());
			yAxis.title.text = "Volume";

			// Create series
			var series = chart.series.push(new window.am4charts.StepLineSeries());
			series.dataFields.categoryX = "value";
			series.dataFields.valueY = "bidstotalvolume";
			series.strokeWidth = 2;
			series.stroke = window.am4core.color("#1aa64a");
			series.fill = series.stroke;
			series.fillOpacity = 0.1;
			series.tooltipText = "Ask: [bold]{categoryX}[/]\nTotal volume: [bold]{valueY}[/]\nVolume: [bold]{bidsvolume}[/]"

			var series2 = chart.series.push(new window.am4charts.StepLineSeries());
			series2.dataFields.categoryX = "value";
			series2.dataFields.valueY = "askstotalvolume";
			series2.strokeWidth = 2;
			series2.stroke = window.am4core.color("#f89927");
			series2.fill = series2.stroke;
			series2.fillOpacity = 0.1;
			series2.tooltipText = "Ask: [bold]{categoryX}[/]\nTotal volume: [bold]{valueY}[/]\nVolume: [bold]{asksvolume}[/]"

			var series3 = chart.series.push(new window.am4charts.ColumnSeries());
			series3.dataFields.categoryX = "value";
			series3.dataFields.valueY = "bidsvolume";
			series3.strokeWidth = 0;
			series3.fill = window.am4core.color("#000");
			series3.fillOpacity = 0.2;

			var series4 = chart.series.push(new window.am4charts.ColumnSeries());
			series4.dataFields.categoryX = "value";
			series4.dataFields.valueY = "asksvolume";
			series4.strokeWidth = 0;
			series4.fill = window.am4core.color("#000");
			series4.fillOpacity = 0.2;

			// Add cursor
			chart.cursor = new window.am4charts.XYCursor();

			return chart;
		},300)
		
		// return;

		// Add data

	}

    fetchTradingVolume() {
        actions.fetchTradingView(this.props.currency+"IDR").then( res => {
            if(res.data.v){
                if (res.data.v.length > 0) {
                    this.setState({
                        currentVolume: res.data.v[res.data.v.length - 1]
                    })
                }
            }
        }).catch(err => {
            let errorDetails = {
                type: 'COMPONENTS: HistoricalChart | FUNC: fetchTradingVolume',
                url: `client2/tradingview/history?symbol=${this.props.currency+"IDR"}&resolution=1D`
            }
            NetworkError(err, errorDetails);
        });
    }
    renderDataText = (props) => {
        return (
            <div style={{ textAlign: "left", flex: 1, alignContent: 'end' }}>
              <p style={{ ...styles.header, ...props.headerNameStyle }}>
                {" "}
                {props.headerName}
              </p>
              <p style={styles.price}>
                {props.price}
                {props.percentage ? (
                  props.percentage > 0 ? (
                    <span
                      style={{
                        ...styles.percentageMin,
                        ...styles.percentage,
                        ...props.style
                      }}
                    >
                      {" "}
                      {props.percentage} %
                    </span>
                  ) : (
                    <span
                      style={{
                        ...styles.percentageMin,
                        ...styles.percentage,
                        ...props.percentageStyle
                      }}
                    >
                      {" "}
                      {props.percentage} %
                    </span>
                  )
                ) : null}
              </p>
            </div>
        );
    }

    renderCoinPair = (props) => {
        return(
            <div style={styles.coinContainer}>
                {this.renderIconHeader(props.pair) }
                <p style={styles.coinPair}> {props.pair} / IDR </p>
            </div>
        )
    }

    renderIconHeader = (pair) => {
        switch (pair.toUpperCase()) {
            case 'BTC':
                return(
                    <div style={{marginRight: 10}}>
                        <i className="cc BTC" style={{ fontSize: "25px", color: "#F7931A" }}></i>
                    </div>
                )
            case 'ETH':
                return(
                    <div style={{marginRight: 10}}>
                        <i className="cc ETH-alt" style={{ fontSize: "25px", color: "#575962" }}></i>
                    </div>
                )
            case 'XRP':
                return(
                    <div style={{marginRight: 10}}>
                        <i className="cc XRP-alt" style={{ fontSize: "25px", color: "#0099d2" }}></i>
                    </div>
                )
            case 'DGX':
                return(
                    <div>
                        <img style={styles.coinIcon} className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/dgx_token.png" alt="" />
                    </div>
                )
            case 'GUSD':
                return(
                    <div>
                        <img style={styles.coinIcon} className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/gemini.png" alt="" />
                    </div>
                )
            case 'PAX':
                return(
                    <div>
                        <img style={styles.coinIcon} className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/pax.png" alt="" />
                    </div>
                )
            case 'TUSD':
                return(
                    <div>
                        <img style={styles.coinIcon} className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/tusd.png" alt="" />
                    </div>
                )
            case 'USDC':
                return(
                    <div>
                        <img style={styles.coinIcon} className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/usdc.png" alt="" />
                    </div>
                )
            case 'USDT':
                return(
                    <div>
                        <img style={styles.coinIcon} className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/usdt.png" alt="" />
                    </div>
                )
            case 'USDT':
                return(
                    <div>
                        <img style={styles.coinIcon} className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/ttc.png" alt="" />
                    </div>
                )
            case 'SWIPE':
                return(
                    <div>
                        <img style={styles.coinIcon} className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/swipe.png" alt="" />
                    </div>
                )
            case 'ZIL':
                return(
                    <div>
                        <img style={styles.coinIcon} className="custom-coin-image-dashboard-middle" src="../../assets/media/img/logo/zil.png" alt="" />
                    </div>
                )
        
            default:
                break;
        }
        
    }

    renderChartHeaderText = (props) => {
        return (
            <div style={{ textAlign: "left" }}>
                <div style={{ ...styles.header, ...props.headerNameStyle }}>
                    {props.headerName}
                </div>
                <div style={{ ...styles.price, ...props.priceStyle }}>
                    { props.headerName == '24H Volume' ? 
                        <div>{ currencyFormatter(amountFormatter(props.price, 2)) } </div>
                        :
                        props.headerName == '24H Change' ?
                        
                            props.price > 0 ? 
                                <span style={{ ...styles.percentagePlus, marginLeft: 0 }}>{ currencyFormatter(amountFormatter(props.price)) }</span>
                            :
                                <span style={{ ...styles.percentageMin, marginLeft: 0 }}>{ currencyFormatter(amountFormatter(props.price * -1)) }</span>
                        :
                        
                        currencyFormatter(amountFormatter(props.price))

                    }
                    {props.percentage ? (props.percentage > 0 ? (
                        <span
                        style={{
                            ...styles.percentagePlus,
                            ...styles.percentage,
                            ...props.style
                        }}
                        >
                        {props.percentage}%
                        </span>
                    ) : (
                        <span
                        style={{
                            ...styles.percentageMin,
                            ...styles.percentage,
                            ...props.percentageStyle
                        }}
                        >
                        {props.percentage}%
                        </span>
                    )
                    ) : null}
                </div>
            </div>
        )
    }
    renderChartHeader = (data) => {
        let {currentPrice, monthlyChange, rangeMin, rangeMax, currentVolume, monthlyChangeAmount} = data;
        
        return(
            <div className="row m-portlet__head chart__header padding-0 margin-0">
                <div className="col-xs-12 col-sm-12 col-md-2  coin-pair__container">
                    {this.renderCoinPair({ pair:this.props.currency })}
                </div>
                <div className="col-xs-12 col-sm-12 col-md-10 padding-0 margin-0">
                    {this.state.selected === "history" &&
                    
                    // <div className="chart-header col-md-12 col-sm-12" style={{ marginTop: "0px", padding: 0}}>
                    //     <div className="col-md-10 col-sm-12 chart__header__data__container">
                    //         <div className="col-md-4 col-sm-3 chart__header__data__last-price">
                    //             { this.renderChartHeaderText({headerName:"Last Price", price: currentPrice }) }
                    //         </div>
                    //         <div className="col-md-8 col-sm-9 chart__header__data__prices">
                    //             { this.renderChartHeaderText({headerName:"24H Change", price:monthlyChangeAmount, headerNameStyle: {fontSize: 9}, priceStyle: {fontSize: 11}, percentage: monthlyChange})}
                    //             { this.renderChartHeaderText({headerName:"24H Max", price:rangeMax, headerNameStyle: {fontSize: 9}, priceStyle: {fontSize: 11}})}
                    //             { this.renderChartHeaderText({headerName:"24H Min", price:rangeMin, headerNameStyle: {fontSize: 9}, priceStyle: {fontSize: 11}})}
                    //             { this.renderChartHeaderText({headerName:"24H Volume", price:currentVolume, headerNameStyle: {fontSize: 9}, priceStyle: {fontSize: 11}})}
                    //         </div>
                    //     </div>
                    //     <div className="col-md-2 dashboard-chart-data-lower-button" style={{ textAlign:"center", paddingLeft: 0}}>
                    //         <button onClick={() => window.open('/depth-chart?symbol='+this.props.currency+'IDR', '_blank')} style={{fontSize: '0.85vw', marginRight: 5, marginVertical: 0}} className="btn btn-outline-blue" >Depth Trading Charts</button>
                    //     </div>
                    // </div>

                    
                        <div className="row col-xs-12 col-sm-12 col-md-12 margin-0 padding-0">
                            <div className="row col-xs-12 col-sm-12 col-md-10 margin-0 padding-0">
                                <div className="col-xs-6 col-sm-6 col-md-3" style={{ display: 'flex', justifyContent: 'center', marginBottom: 5, alignItems: 'center', flex: 1}}> { this.renderChartHeaderText({headerName:"Last Price", price: currentPrice,headerNameStyle: {fontSize: 9}, priceStyle: {fontSize: 18} }) } </div>
                                <div className="col-xs-6 col-sm-6 col-md-2" style={{ display: 'flex', justifyContent: 'center', marginBottom: 5, alignItems: 'center', flex: 1}}> { this.renderChartHeaderText({headerName:"24H Change", price:monthlyChangeAmount, headerNameStyle: {fontSize: 9}, priceStyle: {fontSize: 11}, percentage: monthlyChange})} </div>
                                <div className="col-xs-4 col-sm-4 col-md-2" style={{ display: 'flex', justifyContent: 'center', marginBottom: 5, alignItems: 'center', flex: 1 }}> { this.renderChartHeaderText({headerName:"24H Max", price:rangeMax, headerNameStyle: {fontSize: 9}, priceStyle: {fontSize: 11}})} </div>
                                
                                <div className="col-xs-4 col-sm-4 col-md-2 mobile-24-min" style={{ display: 'flex', justifyContent: 'center', marginBottom: 5, alignItems: 'center', flex: 1 }}> { this.renderChartHeaderText({headerName:"24H Min", price:rangeMin, headerNameStyle: {fontSize: 9}, priceStyle: {fontSize: 11}})} </div>
                                <div className="col-xs-4 col-sm-4 col-md-2 col-offset-1 mobile-24-volume" style={{ display: 'flex', justifyContent: 'center', marginBottom: 5, alignItems: 'center', flex: 1 }}> { this.renderChartHeaderText({headerName:"24H Volume", price:currentVolume, headerNameStyle: {fontSize: 9}, priceStyle: {fontSize: 11}})} </div>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-md-2 padding-0 margin-0 padding--right-5">
                                <button className="btn btn-outline-blue" style={{ width: '100%',  whiteSpace: 'normal' }} onClick={() => window.open('/'+this.props.language+'/depth-chart?symbol='+this.props.currency+'IDR', '_blank')}><span> Depth Trading Charts </span> </button>
                            </div>
                        </div>
                
                    }
                </div>
            </div>
        )
    }
    render(){

        let currentPrice = 0;
        let monthlyChange = 0;
        let monthlyChangeAmount = 0;
        let rangeMax = 0;
        let rangeMin = 0;
        let currentVolume = this.state.currentVolume;
        let currencyPair;

        

        /* 
        ALERT: 
            Lines below caused error when selecting new coins for rate history.
            Consider for delete or changes.

        */
        if(this.props.currencyPairData.hasOwnProperty(this.props.currencyPair)){
            // currentPrice = this.props.currencyPairData[this.props.currency.toUpperCase()+'IDR']['24H'].rates.price;
            currentPrice = this.props.mostRecentConversion > 0 ? this.props.mostRecentConversion : this.props.currencyPairData[this.props.currency.toUpperCase()+'IDR']['24H'].rates.price;
            monthlyChangeAmount = this.props.currencyPairData[this.props.currency.toUpperCase()+'IDR']['24H'].rates['last24hour'];

            rangeMax = this.props.currencyPairData[this.props.currency.toUpperCase()+'IDR']['24H'].rates['maxPrice'];
            rangeMin = this.props.currencyPairData[this.props.currency.toUpperCase()+'IDR']['24H'].rates['minPrice'];

            monthlyChange = ((monthlyChangeAmount / currentPrice) * 100).toFixed(2) ;

            if(currentPrice < monthlyChangeAmount){
                monthlyChange *= -1;
            }
        }

        // dummy test

        return (
            <div className="col-md-12">
				<div className="m-portlet m-portlet--full-height dashboard-chart-data-container padding-0" style={{height:"calc(100vh - 90px)"}}>
				    { this.renderChartHeader({currentPrice, monthlyChange, rangeMin, rangeMax, currentVolume, monthlyChangeAmount}) }
					{this.state.selected === "depth" &&
					<div className="m-portlet__body" style={{padding:"0px"}}>
                        <div className="tab-content" style={{padding:"2.2rem"}}>
                            <div className="tab-pane active" id="m_widget4_tab1_content">
								{this.do_depth_chart()}
								<div id="chartdiv" style={{width: '100%', height: '500px'}} value="tes">

                                </div>
                            </div>
                        </div>
                    </div>}
                    
					{this.state.selected === "history" &&
                    <div className="m-portlet__body" style={{padding:"0px"}}>
                        <div className="tab-content">
                            <div className="tab-pane active" id="m_widget4_tab1_content">
                                <div>
                                {/*<div id="chart_div" style={{ height: "300px" }}></div>*/}
                                {/* <ReactHighstock config={this.state.config}/> */}
                                <TVChartAdvancedContainer language = {this.props.language} symbol={this.props.currencyPair}/>
                                </div>
                            </div>
                            <div className="tab-pane active" id="m_widget4_tab1_content">
                            </div>
                            <div className="tab-pane" id="m_widget4_tab2_content">
                                <div className="m-widget4">
                                </div>
                            </div>
                        </div>
                    </div>}
                </div>
			</div>
        )
    }

}

const mapStateToProps = (state) => {
	return {
		language: state.language
	};
};

export default connect(mapStateToProps, null)(HistoricalChart);


// Styles for chart's header
const styles = {
    container: {
      padding: "10px",
      backgroundColor: "",
      display: "flex",
      flex: 1,
      alignItems: 'end'
    },
    col4: {
      backgroundColor: "",
      flex: 1
    },
    header: {
      fontFamily: "Roboto",
      fontSize: 12,
      color: "#A1A1A1",
      fontWeight: 500,
      margin: 0
    },
    price: {
      fontFamily: "Montserrat",
      fontWeight: 600,
      fontSize: '1.5vw',
      margin: 0,
      display: 'inline-flex'
    },
    percentagePlus: {
      fontSize: 11,
      fontWeight: 400,
      marginLeft: 5,
      color: "#1aa74a"
    },
    percentageMin: {
      fontSize: 11,
      fontWeight: 400,
      marginLeft: 5,
      color: "#C4791C"
    },
    coinPair: {
      fontFamily: 'Montserrat',
      fontSize: '18px',
    //   backgroundColor: 'red',
      fontWeight: 600,
      margin: 0,
      textOverflow: 'ellipsis'
    },
    coinContainer: {
      flex: 1, 
    //   backgroundColor: 'blue',
      alignItems: 'center',
      flexDirection: 'row',
      display: 'flex'
    },
    info: {
      display: "flex", 
      flex: 1, 
      alignContent: 'end',
      backgroundColor: 'red',
    },
    percentage: {
        display: 'inline-flex'
    },
    coinIcon: {
        width: '27px',
        marginRight: '5px'
    }
  };