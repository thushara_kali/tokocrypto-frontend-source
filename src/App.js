import React, { Component } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
//import SidebarMenuRouters from './routers/routers.js';
import getMuiTheme from 'material-ui/styles/getMuiTheme'

import RoutesComponent from './routers/routesComponent.js'

import './static/css/App.css'
import './static/css/vendor-styles.css'
import 'react-notifications/lib/notifications.css'

import 'react-datepicker/dist/react-datepicker.css'
import localStorage from 'localStorage';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setLanguage } from './core/actions/client/language'
import homeActions from './core/actions/client/home'

const muiTheme = getMuiTheme({
	palette: {
		primary1Color: '#258df2',
		accent1Color: '#40c741',
	}
})

class App extends Component {

	componentWillMount() {
		let currentLanguage = localStorage.getItem('language');
		// 
		if(!currentLanguage) {
			localStorage.setItem('language', 'id');
		} else {
			// 
			// 
			this.props.actions.setLanguage(currentLanguage)
		}
	}

	render() {
		return (
			<div id="tc-root">
				<MuiThemeProvider muiTheme={muiTheme}>
					<div>
						<RoutesComponent/>

						{/*<Header />
            <SidebarMenuRouters />*/}
					</div>
				</MuiThemeProvider>
			</div>

		)
	}
}

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(Object.assign({}, { setLanguage: setLanguage }), dispatch)
});

export default connect(null, mapDispatchToProps)(App)
