export const MESSAGE_NOT_AUTHORIZED = "session expired will logout now";
export const MESSAGE_INTERNAL_SERVER_ERROR = "internal server error";
export const MESSAGE_BAD_REQUEST = "something errors please try again later";