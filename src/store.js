import { createStore } from 'redux';
import storage from 'redux-persist/lib/storage'
import allReducers from './core/reducers/client';

const Store = createStore(
    allReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)
export default Store;

