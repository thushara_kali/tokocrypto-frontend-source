# Your Favicon Package

This package was generated with [RealFaviconGenerator](https://realfavicongenerator.net/) [v0.16](https://realfavicongenerator.net/change_log#v0.16)

## Install instructions

To install this package:

Extract this package in the root of your web site. If your site is <code>http://www.example.com</code>, you should be able to access a file named <code>http://www.example.com/favicon.ico</code>.

Insert the following code in the `head` section of your pages:

    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon.png?v=69409bAEP5">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=69409bAEP5">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=69409bAEP5">
    <link rel="manifest" href="/site.webmanifest?v=69409bAEP5">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=69409bAEP5" color="#5bbad5">
    <link rel="shortcut icon" href="/favicon.ico?v=69409bAEP5">
    <meta name="apple-mobile-web-app-title" content="Tokocrypto">
    <meta name="application-name" content="Tokocrypto">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="theme-color" content="#ffffff">
    <meta content="Jual Beli Bitcoin dan Aset Digital Indonesia - Tokocrypto.com"
        property="og:title">
    <meta
        content="Pusat jual beli aset digital terpercaya di Indonesia seperti trading Bitcoin, Ethereum, Ripple & lainya berbasis Rupiah paling aman, cepat & mudah. Login sekarang!"
        property="og:description">
    <meta content="website" property="og:type">
    <meta content="https://tokocrypto.com/" property="og:url">
    <meta content="./../assets-website/images/logo/favicon.ico" property="og:image">
    <meta content="en_US" property="og:locale" data-hid="og:locale">
    <meta content="id_ID" property="og:locale:alternate" data-hid="og:locale:alternate-id-ID">

*Optional* - Check your favicon with the [favicon checker](https://realfavicongenerator.net/favicon_checker)