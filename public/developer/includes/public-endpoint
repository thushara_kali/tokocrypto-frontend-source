<h1 id='public-endpoint'>Public Endpoint</h1>
<p>All Public Endpoints use GET requests</p>
<h2 id='rates'>rates</h2>
<blockquote>
<p>Respon:</p>
</blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
  </span><span class="s2">"success"</span><span class="p">:</span><span class="w"> </span><span class="kc">true</span><span class="p">,</span><span class="w">
  </span><span class="s2">"message_code"</span><span class="p">:</span><span class="w"> </span><span class="s2">"get_rates_success"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"data"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
    </span><span class="p">{</span><span class="w">
      </span><span class="s2">"currencyPair"</span><span class="p">:</span><span class="w"> </span><span class="s2">"XRPIDR"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"buy"</span><span class="p">:</span><span class="w"> </span><span class="mi">7200</span><span class="p">,</span><span class="w">
      </span><span class="s2">"sell"</span><span class="p">:</span><span class="w"> </span><span class="mi">7100</span><span class="w">
    </span><span class="p">},</span><span class="w">
    </span><span class="p">{</span><span class="w">
      </span><span class="s2">"currencyPair"</span><span class="p">:</span><span class="w"> </span><span class="s2">"BTCIDR"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"buy"</span><span class="p">:</span><span class="w"> </span><span class="mi">102676100</span><span class="p">,</span><span class="w">
      </span><span class="s2">"sell"</span><span class="p">:</span><span class="w"> </span><span class="mi">101726900</span><span class="w">
    </span><span class="p">},</span><span class="w">
    </span><span class="p">{</span><span class="w">
      </span><span class="s2">"currencyPair"</span><span class="p">:</span><span class="w"> </span><span class="s2">"ETHIDR"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"buy"</span><span class="p">:</span><span class="w"> </span><span class="mi">3262000</span><span class="p">,</span><span class="w">
      </span><span class="s2">"sell"</span><span class="p">:</span><span class="w"> </span><span class="mi">3232500</span><span class="w">
    </span><span class="p">}</span><span class="w">
  </span><span class="p">]</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre><h3 id='http-request'>HTTP Request</h3>
<p><span class="http-method post">POST</span> <code>https://api.tokocrypto.com/v1/rates</code></p>

<p>Showing current rate for all available crypto</p>
<h2 id='ticker'>Ticker</h2>
<blockquote>
<p>Respon:</p>
</blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
  </span><span class="s2">"success"</span><span class="p">:</span><span class="w"> </span><span class="kc">true</span><span class="p">,</span><span class="w">
  </span><span class="s2">"message_code"</span><span class="p">:</span><span class="w"> </span><span class="s2">"get_ticker_success"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"data"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
    </span><span class="s2">"Name"</span><span class="p">:</span><span class="w"> </span><span class="s2">"XRPIDR"</span><span class="p">,</span><span class="w">
    </span><span class="s2">"Close"</span><span class="p">:</span><span class="w"> </span><span class="mi">5454</span><span class="p">,</span><span class="w">
    </span><span class="s2">"Low"</span><span class="p">:</span><span class="w"> </span><span class="mi">5428</span><span class="p">,</span><span class="w">
    </span><span class="s2">"High"</span><span class="p">:</span><span class="w"> </span><span class="mi">5513</span><span class="p">,</span><span class="w">
    </span><span class="s2">"Open"</span><span class="p">:</span><span class="w"> </span><span class="mi">5464</span><span class="p">,</span><span class="w">
    </span><span class="s2">"Volume"</span><span class="p">:</span><span class="w"> </span><span class="mi">251653454</span><span class="p">,</span><span class="w">
    </span><span class="s2">"Buy"</span><span class="p">:</span><span class="w"> </span><span class="mi">5559</span><span class="p">,</span><span class="w">
    </span><span class="s2">"Sell"</span><span class="p">:</span><span class="w"> </span><span class="mi">5348</span><span class="p">,</span><span class="w">
    </span><span class="s2">"DateTime"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2019-05-23"</span><span class="w">
  </span><span class="p">}</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre><h3 id='http-request-2'>HTTP Request</h3>
<p><span class="http-method get">GET</span> <code>https://api.tokocrypto.com/v1/rates/ticker?currencyPair=dgxidr&amp;type=days</code></p>

<p>The ticker is a high level overview of the state of the market. It shows you the current best bid and ask, as well as the last trade price. It also includes information such as daily volume and how much the price has moved over the last day.</p>

<table><thead>
<tr>
<th>Params</th>
<th>Description</th>
</tr>
</thead><tbody>
<tr>
<td>currencyPair</td>
<td>dgxidr -- <em>btcidr, xrpidr, ttcidr, gusdidr, usdtidr, tusdidr, paxidr, ethidr, dgxidr,usdcidr</em></td>
</tr>
<tr>
<td>type</td>
<td>days -- <em>there are 2 types, &quot;hours&quot; and &quot;days&quot;</em></td>
</tr>
</tbody></table>
<h2 id='orderbook'>Orderbook</h2>
<blockquote>
<p>Respon:</p>
</blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
  </span><span class="s2">"bids"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
    </span><span class="p">[</span><span class="w">
      </span><span class="mi">50000</span><span class="p">,</span><span class="w">
      </span><span class="mf">14.998</span><span class="w">
    </span><span class="p">],</span><span class="w">
    </span><span class="p">[</span><span class="w">
      </span><span class="mi">30000</span><span class="p">,</span><span class="w">
      </span><span class="mi">6</span><span class="w">
    </span><span class="p">],</span><span class="w">
    </span><span class="p">[</span><span class="w">
      </span><span class="mi">10000</span><span class="p">,</span><span class="w">
      </span><span class="mi">10</span><span class="w">
    </span><span class="p">],</span><span class="w">
    </span><span class="p">[</span><span class="w">
      </span><span class="mi">112</span><span class="p">,</span><span class="w">
      </span><span class="mi">1</span><span class="w">
    </span><span class="p">]</span><span class="w">
  </span><span class="p">],</span><span class="w">
  </span><span class="s2">"asks"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
    </span><span class="p">[</span><span class="w">
      </span><span class="mi">70000</span><span class="p">,</span><span class="w">
      </span><span class="mi">50</span><span class="w">
    </span><span class="p">],</span><span class="w">
    </span><span class="p">[</span><span class="w">
      </span><span class="mi">80000</span><span class="p">,</span><span class="w">
      </span><span class="mi">55</span><span class="w">
    </span><span class="p">],</span><span class="w">
    </span><span class="p">[</span><span class="w">
      </span><span class="mi">90000</span><span class="p">,</span><span class="w">
      </span><span class="mi">60</span><span class="w">
    </span><span class="p">]</span><span class="w">
  </span><span class="p">],</span><span class="w">
  </span><span class="s2">"timestamp"</span><span class="p">:</span><span class="w"> </span><span class="mi">1550676264034</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre><h3 id='http-request-3'>HTTP Request</h3>
<p><span class="http-method get">GET</span> <code>https://api.tokocrypto.com/v1/orderbook/btcidr</code></p>

<p>/v1/orderbook/{currencyPair}</p>

<p>btcidr, dgxidr, ethidr, xrpidr, ttcidr, tusdidr, paxidr, usdtidr, gusdidr, usdcidr [price, volume]</p>

<p>can add page for pagination example: <a href="https://api.tokocrypto.com/v1/orderbook/eth?page=0">https://api.tokocrypto.com/v1/orderbook/eth?page=0</a> *page is starting from 0</p>
<h2 id='get-all-tickers'>Get All Tickers</h2>
<blockquote>
<p>Respon:</p>
</blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
  </span><span class="s2">"success"</span><span class="p">:</span><span class="w"> </span><span class="kc">true</span><span class="p">,</span><span class="w">
  </span><span class="s2">"message_code"</span><span class="p">:</span><span class="w"> </span><span class="s2">"get_tickers_success"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"data"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
    </span><span class="p">{</span><span class="w">
      </span><span class="s2">"Name"</span><span class="p">:</span><span class="w"> </span><span class="s2">"BTCIDR"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"Close"</span><span class="p">:</span><span class="w"> </span><span class="mi">117000000</span><span class="p">,</span><span class="w">
      </span><span class="s2">"Low"</span><span class="p">:</span><span class="w"> </span><span class="mi">117000000</span><span class="p">,</span><span class="w">
      </span><span class="s2">"High"</span><span class="p">:</span><span class="w"> </span><span class="mi">123899500</span><span class="p">,</span><span class="w">
      </span><span class="s2">"Open"</span><span class="p">:</span><span class="w"> </span><span class="mi">123881000</span><span class="p">,</span><span class="w">
      </span><span class="s2">"Volume"</span><span class="p">:</span><span class="w"> </span><span class="mi">4808471</span><span class="p">,</span><span class="w">
      </span><span class="s2">"Buy"</span><span class="p">:</span><span class="w"> </span><span class="mi">117000000</span><span class="p">,</span><span class="w">
      </span><span class="s2">"Sell"</span><span class="p">:</span><span class="w"> </span><span class="mi">117000000</span><span class="p">,</span><span class="w">
      </span><span class="s2">"DateTime"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2019-06-17"</span><span class="w">
    </span><span class="p">},</span><span class="w">
    </span><span class="p">{</span><span class="w">
      </span><span class="s2">"Name"</span><span class="p">:</span><span class="w"> </span><span class="s2">"ETHIDR"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"Close"</span><span class="p">:</span><span class="w"> </span><span class="mi">4053840</span><span class="p">,</span><span class="w">
      </span><span class="s2">"Low"</span><span class="p">:</span><span class="w"> </span><span class="mi">3845500</span><span class="p">,</span><span class="w">
      </span><span class="s2">"High"</span><span class="p">:</span><span class="w"> </span><span class="mi">4417928</span><span class="p">,</span><span class="w">
      </span><span class="s2">"Open"</span><span class="p">:</span><span class="w"> </span><span class="mi">3916000</span><span class="p">,</span><span class="w">
      </span><span class="s2">"Volume"</span><span class="p">:</span><span class="w"> </span><span class="mi">119675</span><span class="p">,</span><span class="w">
      </span><span class="s2">"Buy"</span><span class="p">:</span><span class="w"> </span><span class="mi">4064000</span><span class="p">,</span><span class="w">
      </span><span class="s2">"Sell"</span><span class="p">:</span><span class="w"> </span><span class="mi">4043680</span><span class="p">,</span><span class="w">
      </span><span class="s2">"DateTime"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2019-06-17"</span><span class="w">
    </span><span class="p">},</span><span class="w">
    </span><span class="err">...</span><span class="w">
  </span><span class="p">]</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre><h3 id='http-request-4'>HTTP Request</h3>
<p><span class="http-method get">GET</span> <code>https://api.tokocrypto.com/v1/rates/tickers</code></p>
