import gulp from 'gulp';
import plumber from 'gulp-plumber';
import notify from 'gulp-notify';
import sass from 'gulp-sass';
import watch from 'gulp-watch';
import livereload from 'gulp-livereload';

import change from 'gulp-change';
import shell from 'gulp-shell';
import runInSequence from 'run-sequence';
import cleanCSS from 'gulp-clean-css';

gulp.task('minify-css', () => {
    return gulp.src('public/assets/css/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('public/assets/css/'));
});

gulp.task('styles', function () {
  return watch('./src/sass/**/*.scss', function () {
    gulp.src(['./src/sass/App.scss', './src/sass/vendor-styles.scss', './src/sass/bootstrap.scss'])
      .pipe(plumber({ errorHandler: notify.onError('Error: <%= error.message %>') }))
      .pipe(sass())
      .pipe(notify({
        title: 'Gulp',
        subtitle: 'success',
        message: 'Admin Sass Task',
        sound: 'Pop',
      }))
      .pipe(livereload())
      .pipe(gulp.dest('./src/static/css/'));
  });
});

gulp.task('watch', () => {
  livereload.listen();
  gulp.watch('./src/sass/**/*.scss', ['styles']);
});

gulp.task('default', ['styles']);

const DEV_ENDPOINT = 'https://service-dev.tokocrypto.com/client';
const DEMO_ENDPOINT = 'https://service-demo.tokocrypto.com/client';
const PRODUCTION_ENDPOINT = 'https://service.tokocrypto.com/client';

const DEV_MAIN_ENDPOINT = 'https://service-dev.tokocrypto.com/';
const DEMO_MAIN_ENDPOINT = 'https://service-demo.tokocrypto.com/';
const PRODUCTION_MAIN_ENDPOINT = 'https://service.tokocrypto.com/';

const DEV_SIGNING_URL = 'https://service-dev.tokocrypto.com/client/put-signed-url';
const DEMO_SIGNING_URL = 'https://service-demo.tokocrypto.com/client/put-signed-url';
const PRODUCTION_SIGNING_URL = 'https://service.tokocrypto.com/client/put-signed-url';

// ENV Management

function setDevEnviroment(content) {
    return content.replace(/REACT_APP_BASE_URL=.+/g, 'REACT_APP_BASE_URL='+DEV_ENDPOINT);
}

function setDevMainEnviroment(content) {
    return content.replace(/REACT_APP_BASE_MAIN_URL=.+/g, 'REACT_APP_BASE_MAIN_URL='+DEV_MAIN_ENDPOINT);
}

function setDevAppiroment(content) {
    return content.replace(/REACT_APP_ENV=.+/g, 'REACT_APP_ENV=dev');
}

function setLocalAppiroment(content) {
    return content.replace(/REACT_APP_ENV=.+/g, 'REACT_APP_ENV=local');
}

function setDevSigningURL(content) {
    return content.replace(/REACT_APP_SIGNING_URL=.+/g, 'REACT_APP_SIGNING_URL='+DEV_SIGNING_URL);
}

function setDemoEnviroment(content) {
    return content.replace(/REACT_APP_BASE_URL=.+/g, 'REACT_APP_BASE_URL='+DEMO_ENDPOINT);
}

function setDemoMainEnviroment(content) {
    return content.replace(/REACT_APP_BASE_MAIN_URL=.+/g, 'REACT_APP_BASE_MAIN_URL='+DEMO_MAIN_ENDPOINT);
}

function setDemoAppiroment(content) {
    return content.replace(/REACT_APP_ENV=.+/g, 'REACT_APP_ENV=demo');
}

function setDemoSigningURL(content) {
    return content.replace(/REACT_APP_SIGNING_URL=.+/g, 'REACT_APP_SIGNING_URL='+DEMO_SIGNING_URL);
}

function setProductionEnviroment(content) {
    return content.replace(/REACT_APP_BASE_URL=.+/g, 'REACT_APP_BASE_URL='+PRODUCTION_ENDPOINT);
}

function setProductionMainEnviroment(content) {
    return content.replace(/REACT_APP_BASE_MAIN_URL=.+/g, 'REACT_APP_BASE_MAIN_URL='+PRODUCTION_MAIN_ENDPOINT);
}

function setProductionAppiroment(content) {
    return content.replace(/REACT_APP_ENV=.+/g, 'REACT_APP_ENV=production');
}

function setProductionSigningURL(content) {
    return content.replace(/REACT_APP_SIGNING_URL=.+/g, 'REACT_APP_SIGNING_URL='+PRODUCTION_SIGNING_URL);
}

gulp.task('build', shell.task([
    'npm run build'
]));

// git tag management
gulp.task('tag:remove-development', shell.task([
    'git tag -d development',
    'git push origin :refs/tags/development'
]));

gulp.task('tag:set-dev', shell.task([
    'git tag -a development -m "development"'
]));

gulp.task('tag:dev', shell.task([
    'node_modules/.bin/gulp git:setuser',
    'node_modules/.bin/gulp tag:remove-development',
    'node_modules/.bin/gulp tag:set-dev',
    'node_modules/.bin/gulp tag:push'
]));

gulp.task('tag:remove-demo', shell.task([
    'git tag -d demo',
    'git push origin :refs/tags/demo'
]));

gulp.task('tag:set-demo', shell.task([
    'git tag -a demo -m "demo"'
]));

gulp.task('tag:demo', shell.task([
    'node_modules/.bin/gulp git:setuser',
    'node_modules/.bin/gulp tag:remove-demo',
    'node_modules/.bin/gulp tag:set-demo',
    'node_modules/.bin/gulp tag:push'
]));

gulp.task('git:setuser', shell.task([
    'git config --global user.name "Heru Joko"',
    'git config --global user.email "herujokoutomo@gmail.com"'
]));

gulp.task('tag:push', shell.task([
    'git push --tags'
]));

gulp.task('env:local', function() {
    return gulp.src('.env')
    .pipe(change(setDevEnviroment))
    .pipe(change(setDevMainEnviroment))
    .pipe(change(setLocalAppiroment))
    .pipe(change(setDevSigningURL))
    .pipe(gulp.dest('./'))
});

gulp.task('env:dev', function() {
    return gulp.src('.env')
        .pipe(change(setDevEnviroment))
        .pipe(change(setDevMainEnviroment))
        .pipe(change(setDevAppiroment))
        .pipe(change(setDevSigningURL))
        .pipe(gulp.dest('./'))
});

gulp.task('env:demo', function() {
    return gulp.src('.env')
        .pipe(change(setDemoEnviroment))
        .pipe(change(setDemoMainEnviroment))
        .pipe(change(setDemoAppiroment))
        .pipe(change(setDemoSigningURL))
        .pipe(gulp.dest('./'))
});

gulp.task('env:production', function() {
    return gulp.src('.env')
        .pipe(change(setProductionEnviroment))
        .pipe(change(setProductionMainEnviroment))
        .pipe(change(setProductionAppiroment))
        .pipe(change(setProductionSigningURL))
        .pipe(gulp.dest('./'))
});

gulp.task('build:dev', () => {
    runInSequence('env:dev', 'build', 'minify-css');
});

gulp.task('build:demo', () => {
    runInSequence('env:demo', 'build', 'minify-css');
});

gulp.task('build:production', () => {
    runInSequence('env:production', 'build', 'minify-css');
});
